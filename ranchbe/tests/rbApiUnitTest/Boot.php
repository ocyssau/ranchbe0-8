<?php 

//------------------------------------------------------------------------
//Initialization
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_WARNING);
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );

define('APPLICATION_PATH',
	str_replace ( '\\', '/', realpath ( dirname ( __FILE__ ) . '/../../application' ) )
);

define('APPLICATION_ENV','testing');

// Ensure library/ is on include_path
set_include_path ( implode ( PATH_SEPARATOR,
					array (	realpath ( APPLICATION_PATH . '/../library' ),
							realpath ( APPLICATION_PATH . '/../external' ),
							get_include_path () ) 
					) 
				);
				
require_once ('Zend/Config/Ini.php');
require_once ('Zend/Application.php');
require_once ('Zend/Session.php');
require_once ('Rb/Conf/Ranchbe.php');

// Installe le chargeur automatique de classes
require_once ('Zend/Loader/Autoloader.php');
$autoloader = Zend_Loader_Autoloader::getInstance();

$config = new Zend_Config_Ini ( APPLICATION_PATH . '/configs/rb.ini',
							APPLICATION_ENV , true);
Ranchbe::setConfig ($config);

$application = new Zend_Application ( APPLICATION_ENV, Ranchbe::getConfig() );
$application->bootstrap('php');
$application->bootstrap('adodb');
$application->bootstrap('galaxia');
$application->bootstrap('ranchbe');
$application->bootstrap('Translate');


//======================================================================

assert_options(ASSERT_ACTIVE,   true);
assert_options ( ASSERT_WARNING );
//assert_options ( ASSERT_CALLBACK , 'myAssertCallback');

//Init a user
$user = Rb_User::get(22);
Rb_User::setCurrentUser($user);

