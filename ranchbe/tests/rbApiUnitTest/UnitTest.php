<?php 

require('Boot.php');

function Test_ErrorStack(){
	$errorStack = new Rb_Error('RbTest');
	$errorStack->setLogger( Ranchbe::getLogger() );
	$errorStack->setLoglevel(Rb_Error::DEPRECATED);
	
	$errorStack->push(Rb_Error::ERROR, array('in'=>'hopla'), 'error test %in%' );
	$errorStack->push(Rb_Error::ERROR, array(), 'error test2' );
	Test_ErrorStack_2($errorStack);
}

function Test_ErrorStack_2($errorStack){
	$errorStack->push(Rb_Error::ERROR, array(), 'error test3' );
	$errorStack->error('%tr%!', array('tr'=>'Hoops'));
	$errorStack->warning('%tr%!', array('tr'=>'Hophophop'));
	$errorStack->notice('%tr%!', array('tr'=>'tststs'));
	//var_dump( $errorStack->getErrors() );
	//var_dump( $errorStack->getLastMessage() );
}


function Test_Creation(){
	//Create a project
	$project = new Rb_Project();
	$project->setName( 'TestPROJ' );
	$project->setNumber( uniqId('TestPROJ') );
	$project->setProperty('description', 'Pour tests');
	assert(
		$project->save(false, false)
	);
		
	$space_name = 'workitem';
	
	//Create a container
	$container = Rb_Container::get($space_name, 0);
	$container->setNumber( uniqId('TestCONTAINER') );
	$container->setName( 'TestCONTAINER' );
	$container->setProject( $project );
	assert(
		$container->save(false, false)
	);
	
	$document = Rb_Document::get($space_name, 0);
	$document->setContainer( $container );
	$document->setNumber( uniqId('TestDOC') );
	assert(
		$document->save(false, false)
	);
}



echo "==========================================\n";
echo 'include path: ' . get_include_path ()  . "\n";
echo "==========================================\n";
echo 'APPLICATION_PATH: ' . APPLICATION_PATH . "\n";

Test_ErrorStack();
Test_Creation();

