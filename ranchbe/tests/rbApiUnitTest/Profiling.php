<?php 
//session_start();

require('Boot.php');

function Profiling_Documents_Creation(){
	//Create a project
	$project = new Rb_Project();
	$project->setName( 'TestPROJ' );
	$project->setNumber( uniqId('TestPROJ') );
	$project->setProperty('description', 'Pour tests');
	assert($project->save(false, false));
		
	$space_name = 'workitem';
	
	//Create a container
	$container = Rb_Container::get($space_name, 0);
	$container->setNumber( uniqId('TestCONTAINER') );
	//$container->setName( 'TestCONTAINER' );
	$container->setProject( $project );
	assert($container->save(false, false));
	
	$startTime = microtime(true);
	$i=0;
	$loop = 10000;
	while($i < $loop){
		$number = uniqId('TestDOC');
		echo 'Loop : ' . $i . "\n";
		echo 'create document ' . $number . "\n";
		$document = Rb_Document::get($space_name, 0);
		$document->setContainer( $container );
		$document->setNumber( $number );
		$document->save(false, false);
		$i++;
	}
	$endTime = microtime(true);
	$executionTime = $endTime - $startTime;
	$executionTimeByUnit = $executionTime/$loop;
	echo 'In project'. $project->getNumber() . " \n";
	echo 'In container'. $container->getNumber() . " \n";
	echo "creation de $loop objects en $executionTime S \n";
	echo "soit $executionTimeByUnit S par loop \n";
}


echo "==========================================\n";
echo 'include path: ' . get_include_path ()  . "\n";
echo "==========================================\n";
echo 'APPLICATION_PATH: ' . APPLICATION_PATH . "\n";

Profiling_Documents_Creation();

