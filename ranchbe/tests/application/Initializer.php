<?php
class Initializer extends Zend_Controller_Plugin_Abstract
{
  public function __construct ($env = 'production')
  {
    // On indique l'endroit o� sont plac�s les contr�leurs
    Zend_Controller_Front::getInstance()->addControllerDirectory('../application/controllers');
 
    // On pr�pare une connexion avec le driver PDO MySQL
    switch($env) {
      case 'test':
        $dbname = 'votreapp_test';
        break;
      default:
        $dbname = 'votreapp';
    }
    $db = Zend_Db::factory('pdo_mysql', array(
       'host' => 'localhost' ,
        'username' => 'root' ,
        'password' => '' ,
        'dbname' => $dbname
    ));
 
    // On place l'instance de la connexion dans le registre
    Zend_Registry::set('db', $db);
  }
}


