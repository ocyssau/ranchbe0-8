<?php
require_once 'Zend/Test/PHPUnit/ControllerTestCase.php';
 
class IndexControllerTest extends Zend_Test_PHPUnit_ControllerTestCase 
{
  protected function setUp() 
  {
    // Code de mise en place
    $this->bootstrap = array($this, 'appBootstrap');		
    parent::setUp();	
  }
 
  protected function appBootstrap()
  {
    $this->frontController->registerPlugin(new Initializer('test'));
  }
 
  protected function tearDown() 
  {
    // Nettoyage
    parent::tearDown();
  }		
 
  public function testIndexAction() 
  {
    // Simule une requ�te vers la racine
    $this->dispatch('/');
    // On v�rifie le code de r�ponse HTTP
    $this->assertResponseCode(200, 'Code was ' . $this->_response->getHttpResponseCode());
    // Le nom du contr�leur est bien Index
    $this->assertController('index', 'Controller was ' . $this->_request->getControllerName());
    // Le nom de l'action est bien Index
    $this->assertAction('index', 'Action was ' . $this->_request->getActionName());
    // Teste le contenu de la r�ponse
    $this->assertQueryContentContains('h4', 'Pierre QUIMOUSSE');
  }
  
}




