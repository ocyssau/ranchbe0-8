<?php
require_once 'Zend/Loader.php';
 
// Installe le chargeur automatique de classes
Zend_Loader::registerAutoload();

// Charge la classe d'initialisation
require_once 'Initializer.php';
 
// R�cup�re une instance de controlleur principal
$frontController = Zend_Controller_Front::getInstance();
 
// Enregistre le plugin d'initialisation
$frontController->registerPlugin(new Initializer('testing'));
 
// Demande au controller de traiter la requ�te
$frontController->dispatch();


