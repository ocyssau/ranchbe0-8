<?php
// Important ! Affiche tous les messages d'erreurs
error_reporting(E_ALL);

// Installe le chargeur automatique de classes
require_once 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();

// Charge la bibliothèque PHPUnit
require_once 'PHPUnit/Framework/TestSuite.php';

// Charge notre classe d'initialisation
//require_once './application/Initializer.php';
/*
Zend_Controller_Front::getInstance()
	->addControllerDirectory('../application/controllers');
*/
// Charge le test du contrôleur IndexController

require_once '../application/controllers/document/IndexControllerTest.php';

// Mise en place d'une suite de test
class AllTests extends PHPUnit_Framework_TestSuite{
  
  public function __construct() {
    $this->setName('AllTests');
    $this->addTestSuite('Document_IndexControllerTest');
  }
  
  public static function suite() {
    return new self();
  }
  
}

