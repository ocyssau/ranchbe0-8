<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_WARNING);
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );

// Define path to application directory
define('APPLICATION_PATH',
	str_replace ( '\\', '/', realpath ( dirname ( __FILE__ ) . '/../../application' ) )
);
require( APPLICATION_PATH . '/configs/Boot.php');

require_once('RbService/Document.php');

//echo realpath ( APPLICATION_PATH . '/../library' );
//echo "\n\r";
//echo get_include_path ();die;

$application = new Zend_Application ( APPLICATION_ENV, Ranchbe::getConfig() );
$application->bootstrap('php');
$application->bootstrap('adodb');
$application->bootstrap('translate');
$application->bootstrap('galaxia');
$application->bootstrap('ranchbe');

$inputs[] =array(
			'file_name'=>'file1.txt',
			'space_name'=>'workitem'
);
$inputs[] =array(
			'file_name'=>'fichierTest_100.txt',
			'space_name'=>'workitem'
);

$DocumentS = new RbService_Document();

$output = $DocumentS->getFromFiles($inputs);
var_dump($output);

