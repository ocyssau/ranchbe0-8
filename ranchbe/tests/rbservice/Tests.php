<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_WARNING);
ini_set('display_errors', 1 );
ini_set('display_startup_errors', 1 );
ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);

assert_options( ASSERT_ACTIVE, 1);
assert_options( ASSERT_WARNING, 1 );
assert_options( ASSERT_BAIL, 0 );

/*
 Start session after definition of classes

 //http://bugs.php.net/bug.php?id=44267&thanks=3
 # Soap client
 $client = new SoapClient(NULL, array(
 "location" => "http://localhost/ranchbe/rbservice/server.php",
 "uri" => "urn:xmethodsTest",
 'trace' => 1));

 # SOAP requests
 try {
 $session = $client->login();
 $client->__setCookie('PSESSION', $session);
 print $client->incVar(); print "\n";
 print $client->incVar(); print "\n";
 print $client->incVar(); print "\n";
 print $client->incVar(); print "\n";
 print $client->incVar(); print "\n";
 } catch (SoapFault $sf) {
 echo $sf->getTrace();
 }
 die;
 */

/* Class for transmit user name and password to soap server
 *
 */
class Rbs_UserPass
{
	public $Username;
	public $Password;

	function __construct($Username, $Password)
	{
		$this->Username = $Username;
		$this->Password = $Password;
	}
}

/* Class for transmit user name and password to soap server
 *
 */
class Rbs_Identifier
{
	public $id;

	function __construct($id)
	{
		$this->id = $id;
	}
}


class Rbs_SoapClient extends SoapClient{

	/**
	 *
	 * @var array
	 */
	protected static $_registry = array();

	/**
	 *
	 * @var array
	 */
	protected static $_connexionFailed = false;

	/**
	 *
	 * @var unknown_type
	 */
	public static $passwd = '';

	/**
	 *
	 * @var unknown_type
	 */
	public static $userHandle = '';

	/**
	 *
	 * @var unknown_type
	 */
	public static $serverUrl = '';

	/**
	 *
	 * @var unknown_type
	 */
	public static $soapOptions = array();


	/**
	 *
	 * @param string $serverUrl
	 * @param string $serviceName = document|container|project|docfile|...
	 * @param string $userHandle
	 * @param string $passwd
	 * @param array $soapOptions
	 */
	public function __construct($serverUrl, $serviceName, $userHandle, $passwd, array $soapOptions){
		//Create a soap var from UserPass class
		$soapAuthenticator = new SoapVar(new Rbs_UserPass($userHandle, $passwd), SOAP_ENC_OBJECT, 'UserPass');
		$authHeader = new SoapHeader("http://schemas.xmlsoap.org/ws/2002/07/utility","headerAuthentify",$soapAuthenticator,false);
		//$soapIdentifier = new SoapVar(new Rbs_Identifier($id), SOAP_ENC_OBJECT, 'Identifier');
		//$idHeader = new SoapHeader("http://schemas.xmlsoap.org/ws/2002/07/utility","headerIdentify",$soapIdentifier,false);

		$wsdl = "$serverUrl/$serviceName/?wsdl";

		echo 'Try to connect to ' . $wsdl . "\n";
		if(self::$_connexionFailed == true){
			throw new exception('connexion failed'."\n");
		}
		parent::__construct($wsdl, $soapOptions);
		echo 'Successful connexion to ' . $wsdl . "\n";

		$this->__setSoapHeaders( array($authHeader) );
	}//End of method

	/**
	 *
	 * @param string $serviceName = document|container|project|docfile|...
	 */
	public static function singleton($serviceName){
		$serviceName = strtolower($serviceName);
		if(!self::$_registry[$serviceName]){
			try {
				$userHandle = self::$userHandle;
				$passwd = self::$passwd;
				$serverUrl = self::$serverUrl;
				$soapOptions = self::$soapOptions;
				self::$_registry[$serviceName] = new Rbs_SoapClient($serverUrl, $serviceName, $userHandle, $passwd, $soapOptions);
			}catch(Exception $e){
				echo 'Connexion to '.$serviceName.' failed ' . "\n";
				self::setConnexionFailed(true);
				throw $e;
			}
		}
		return self::$_registry[$serviceName];
	}//End of method

	/**
	 *
	 * @param string $serviceName = document|container|project|docfile|...
	 */
	public static function factory($serviceName){
		$serviceName = strtolower(basename($serviceName));
		try {
			$userHandle = self::$userHandle;
			$passwd = self::$passwd;
			$serverUrl = self::$serverUrl;
			$soapOptions = self::$soapOptions;
			$service = new Rbs_SoapClient($serverUrl, $serviceName, $userHandle, $passwd, $soapOptions);
		}catch(Exception $e){
			echo 'Connexion to '.$serviceName.' failed ' . "\n";
			throw $e;
		}
		return $service;
	}//End of method

	/**
	 *
	 */
	public static function setConnexionFailed($flag){
		self::$_connexionFailed = $flag;
	}//End of method

} //End of class


/**
 * 
 */
function rbs_test_list($container_id){
	echo __FUNCTION__ . ": \n";

	$serviceName = 'document/list';
	$documentService = Rbs_SoapClient::singleton($serviceName);
	
	$name = uniqId(__FUNCTION__ . '_doc001_');
	$number = $name;
	
	$file_name = $name . '.txt';
	$content = $file_name . ' v1.1';
	
	$inputs = array();
	$inputs['documents'][0]['container'] = array('id'=>$container_id, 'space_name'=>'workitem');
	$inputs['documents'][0]['properties'] = array('document_number'=>$number, 'document_name'=>$name);
	$inputs['documents'][0]['docfiles'][0] = array('index'=>0, 'role'=>0);
	$inputs['docfiles'][0]['container'] = array('id'=>$container_id, 'space_name'=>'workitem');
	$inputs['docfiles'][0]['properties'] = array('file_name'=>$file_name);
	$inputs['docfiles'][0]['fsdata'] = $file_name;
	$inputs['fsdatas'][$file_name]['file_name'] = $file_name;
	$inputs['fsdatas'][$file_name]['md5'] = md5($content);
	$inputs['fsdatas'][$file_name]['content'] = base64_encode($content);
	
	//var_dump($inputs);
	$outputs = $documentService->create($inputs);
	//var_dump($outputs);
	
	$inputs = array();
	$inputs[] =array(
				'file_name'=>$file_name,
				'space_name'=>'workitem');
	$outputs = $documentService->getFromFiles($inputs);
	
	//var_dump($outputs);
	
	assert($outputs['fsdatas'][$file_name]['file_name'] == $file_name);
	$docfile_id = $outputs['fsdatas'][$file_name]['docfile'];
	$document_id = $outputs['fsdatas'][$file_name]['document'];
	assert($outputs['documents'][$document_id]['properties']['document_number'] == $number);
	//assert($outputs['documents'][$document_id]['docfiles'][$docfile_id]['index'] == $docfile_id);
	assert($outputs['docfiles'][$docfile_id]['id'] == $docfile_id);
	assert($outputs['docfiles'][$docfile_id]['properties']['file_name'] == $file_name);
	//var_dump($outputs);die;
}

/**
 * 
 * @param $numbers
 * @param $container_id
 */
function rbs_test_create(array $numbers, $container_id){
	echo __FUNCTION__ . ": \n";

	$serviceName = 'document/list';
	$documentService = Rbs_SoapClient::singleton($serviceName);

	$number001 = $numbers[0];
	$number002 = $numbers[1];

	$inputs = array('documents'=>array(
                	0=>array(
                				'container'=>array('id'=>$container_id, 'space_name'=>'workitem'),
                				'properties'=>array('document_number'=>$number001, 'document_name'=>$number001),
                				'docfiles'=>array()),
                	1=>array(
                				'container'=>array('id'=>$container_id, 'space_name'=>'workitem'),
                				'properties'=>array('document_number'=>$number002, 'document_name'=>$number002),
                				'docfiles'=>array())));
	$outputs = $documentService->create($inputs);
	assert($outputs['documents'][1]['error_code'] == 0);
	assert($outputs['documents'][2]['error_code'] == 0);
	//assert($outputs['docfiles']);
	//var_dump($outputs);die;

	echo "Get from numbers: \n";
	$inputs = array(
		0=>array(
			'number'=>$number001,
			'space_name'=>'workitem'
			),
		1=>array(
			'number'=>$number002,
			'space_name'=>'workitem'
		));
	$outputs = $documentService->getFromNumbers($inputs);
	assert( count($outputs['documents']) == 2 );
	//var_dump($outputs);
	$i = 0;
	foreach($outputs['documents'] as $id=>$output){
	   //var_dump($output);
	   assert($output['container']['id'] == $container_id);
	   assert($output['container']['space_name'] == 'workitem');
	   assert($output['properties']['document_number'] == $numbers[$i]);
	   assert($output['properties']['document_name'] == $numbers[$i]);
	   $i++;
  }
	//var_dump($outputs);
}

/**
 * test of document API
 */
function rbs_test_document($container_id){
	echo __FUNCTION__ . ": \n";
	
	$serviceName = 'document';
	$client = Rbs_SoapClient::factory($serviceName);
	
	$space_name = 'workitem';
	
	try {
		//$session = $client->login();
		//$session = uniqId();
		//$client->__setCookie('PSESSION', $session);
		
		$number = uniqId('doc003');
		$name = __FUNCTION__ . '_doc003';
		
		$client->init( $space_name );
		$client->setFather( $container_id );
		$client->setNumber( $number );
		$client->setName( $name );
		
		//var_dump( $client->getNumber() );
		//var_dump( $client->getProperty('container_id') );
		
		assert($client->getNumber() == $number);
		assert($client->getName() == $name);
		assert($client->getProperty('container_id') == $container_id);
		
		$client->save();
		$id = $client->getId();
		//var_dump($id);
		
		assert($id > 0);
		
		$respons = $client->loadFromId($id, $space_name);
		$respons = $client->getId();
		assert($respons == $id);
		//var_dump($respons);
		$respons = $client->getProperties();
		assert($respons['document_number'] == $number);
		assert($respons['document_name'] == $name);
		assert($client->getNumber() == $number);
		assert($client->getName() == $name);
		assert($client->getProperty('container_id') == $container_id);
		//var_dump($respons);
		
		$document2 = Rbs_SoapClient::factory($serviceName);
		$number2 = uniqId('doc004');
		$name2 = __FUNCTION__ . '_doc004';
		$document2->init( $space_name );
		$document2->setFather( $container_id );
		$document2->setNumber( $number2 );
		$document2->setName( $name2 );
		$document2->save();
		
		assert($client->getNumber() == $number);
		assert($client->getName() == $name);
		assert($client->getProperty('container_id') == $container_id);
		
		assert($document2->getNumber() == $number2);
		assert($document2->getName() == $name2);
		assert($document2->getProperty('container_id') == $container_id);
		
	} catch (SoapFault $fault) {
		/*$fault = new SoapFault();
		$fault->getCode();
		$fault->getMessage();
		$fault->getTraceAsString();*/
		trigger_error("SOAP Fault: (faultcode: {$fault->getCode()}, faultstring: {$fault->getMessage()})", E_USER_ERROR);
	}
}

/**
 * Create a new reposit if necessary
 */
function rbs_create_reposit(){
	echo __FUNCTION__ . ": \n";
	
	$serviceName = 'reposit';
	$client = Rbs_SoapClient::factory($serviceName);
	$id = $client->getActiveReposit('workitem');
	if(!$id){
		$id = $client->quickCreate('workitem', 'reposit001');
	}
	assert($id > 0);
	
	var_dump($id);
}


/**
 * test of container API
 */
function rbs_test_container(){
	echo __FUNCTION__ . ": \n";
	
	$serviceName = 'container';
	$client = Rbs_SoapClient::factory($serviceName);
	
	$space_name = 'workitem';
	
	try {
		$number = uniqId('cont001_');
		$name = __FUNCTION__ . '_cont001';
		
		$client->init( $space_name );
		$client->setNumber( $number );
		$client->setName( $name );
		$client->save();
		
		var_dump( $client->getNumber() );
		var_dump( $client->getName() );
		assert($client->getNumber() == $number);
		assert($client->getName() == $name);
		
	} catch (SoapFault $fault) {
		trigger_error("SOAP Fault: (faultcode: {$fault->getCode()}, faultstring: {$fault->getMessage()})", E_USER_ERROR);
	}
}

/**
 * test of container API
 */
function rbs_test_container_create(){
	echo __FUNCTION__ . ": \n";
	
	$serviceName = 'container';
	$client = Rbs_SoapClient::factory($serviceName);
	
	$space_name = 'workitem';
	
	try {
		$number = uniqId(__FUNCTION__ . '_cont001_');
		$name = __FUNCTION__ . '_cont001';
		
		$client->init( $space_name );
		$client->setNumber( $number );
		$client->setName( $name );
		$client->save();
		
		var_dump( $client->getNumber() );
		var_dump( $client->getName() );
		assert($client->getNumber() == $number);
		assert($client->getName() == $name);
		$id = $client->getId();
		assert($id > 0);
		return $id;
	} catch (SoapFault $fault) {
		trigger_error("SOAP Fault: (faultcode: {$fault->getCode()}, faultstring: {$fault->getMessage()})", E_USER_ERROR);
	}
}


/**
 * test of container API
 */
function rbs_test_project(){
	echo __FUNCTION__ . ": \n";
	
	$serviceName = 'project';
	$client = Rbs_SoapClient::factory($serviceName);
	
	try {
		$number = uniqId('project001_');
		$name = __FUNCTION__ . '_proj001_' . $number;
		
		$client->init();
		$client->setNumber( $number );
		$client->setName( $name );
		$client->save();
		
		var_dump( $client->getNumber() );
		var_dump( $client->getName() );
		assert($client->getNumber() == $number);
		assert($client->getName() == $name);
		
	} catch (SoapFault $fault) {
		trigger_error("SOAP Fault: (faultcode: {$fault->getCode()}, faultstring: {$fault->getMessage()})", E_USER_ERROR);
	}
}

//------------------------------------------------------------------------------

/*
 $client = new SoapClient(NULL, array(
 "location" => "$serverUrl/$serviceName",
 "uri" => "urn:xmethodsTest",
 'trace' => 1));
 */


Rbs_SoapClient::$userHandle = 'admin';
Rbs_SoapClient::$passwd = 'admin00';
Rbs_SoapClient::$serverUrl = 'http://localhost/ranchbe/rbservice';
Rbs_SoapClient::$soapOptions = array(
								'trace' => true,
								'soap_version' => 2,
								'cache_wsdl' => false,
								'encoding' => "ISO-8859-1");


rbs_create_reposit();
$container_id = rbs_test_container_create();
if($container_id){
	rbs_test_list($container_id);
	rbs_test_create(array( uniqId('doc001_'), uniqId('doc002_') ), $container_id);
	rbs_test_project();
	rbs_test_document($container_id);
}


