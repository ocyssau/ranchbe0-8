SELECT
	COUNT(
		document_state
	) AS countstate , 
	owner,
	document_state,
	instanceId,
	activity_id,
	histo_order
FROM
  	workitem_documents_history LEFT OUTER JOIN galaxia_instances
	ON
		workitem_documents_history.instance_id = galaxia_instances.instanceId
WHERE 
    document_state NOT LIKE 'aborted'
  AND
  	action_name = 'ChangeState' 
	AND 
		status = 'completed'
GROUP BY
	document_state, instance_id
HAVING COUNT(document_state) > 1


SELECT
	histo_order
FROM
  	workitem_documents_history LEFT OUTER JOIN galaxia_instances
	ON
		workitem_documents_history.instance_id = galaxia_instances.instanceId
WHERE 
    document_state NOT LIKE 'aborted'
  AND
  	action_name = 'ChangeState' 
	AND 
		status = 'completed'
GROUP BY
	document_state, instance_id
HAVING COUNT(document_state) > 1

