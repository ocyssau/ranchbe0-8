
--
-- Base de donn�es: `ranchbe0-8`
--


--
-- Structure de la table `rbdb`
--
CREATE TABLE `db_version` (
  `version` int(2) NOT NULL,
  `compiled` int(4) NOT NULL
) ENGINE = MYISAM;


-- --------------------------------------------------------

--
-- Structure de la table `objects`
--
CREATE TABLE `objects` (
  `object_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `space_id` int(11) NOT NULL,
  PRIMARY KEY (`object_id`),
  KEY `INDEX_objects_1` (`space_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `objects_seq`
--
CREATE TABLE `objects_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB;
INSERT INTO `objects_seq` (`id`) VALUES (100);

-- --------------------------------------------------------

--
-- Structure de la table `objects_rel`
--
CREATE TABLE `objects_rel` (
  `link_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `ext_id` int(11) NOT NULL,
  PRIMARY KEY  (`link_id`),
  UNIQUE KEY `UNIQ_objects_rel_1` (`parent_id`,`child_id`, `ext_id`),
  KEY `INDEX_objects_rel_1` (`parent_id`),
  KEY `INDEX_objects_rel_2` (`child_id`),
  KEY `INDEX_objects_rel_3` (`ext_id`)
) ENGINE=InnoDB;


-- --------------------------------------------------------

--
-- Structure de la table `acl_rules`
--

CREATE TABLE `acl_rules` (
  `privilege` int(11)  NOT NULL,
  `resource_id` int(11) NOT NULL,
  `role_id` int(11)  NOT NULL,
  `rule` varchar(32)  NOT NULL,
  UNIQUE KEY `UC_resources_acl` (`resource_id`,`role_id`,`privilege`),
  KEY `INDEX_acl_privileges_1` (`resource_id`),
  KEY `INDEX_acl_privileges_2` (`role_id`),
  KEY `INDEX_acl_privileges_3` (`privilege`),
  KEY `INDEX_acl_privileges_4` (`rule`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `checkout_index`
--

CREATE TABLE `checkout_index` (
  `file_name` varchar(128) NOT NULL default '',
  `description` varchar(128) default NULL,
  `container_type` varchar(128) NOT NULL default '',
  `container_id` int(11) NOT NULL default '0',
  `container_number` varchar(128) NOT NULL default '',
  `check_out_by` int(11) NOT NULL default '0',
  `document_id` int(11) NOT NULL default '0'
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `indices`
--

CREATE TABLE `indices` (
  `indice_id` int(11) NOT NULL default '0',
  `indice_value` varchar(8) NOT NULL default '',
  PRIMARY KEY  (`indice_id`),
  UNIQUE KEY `UC_indice_value` (`indice_value`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `doctypes`
--

CREATE TABLE `doctypes` (
  `doctype_id` int(11) NOT NULL default '0',
  `doctype_number` varchar(64) NOT NULL default '',
  `doctype_description` varchar(128) default NULL,
  `can_be_composite` tinyint(4) NOT NULL default '1',
  `file_extension` varchar(32) default NULL,
  `file_type` varchar(128) default NULL,
  `icon` varchar(32) default NULL,
  `script_post_store` varchar(64) default NULL,
  `script_pre_store` varchar(64) default NULL,
  `script_post_update` varchar(64) default NULL,
  `script_pre_update` varchar(64) default NULL,
  `recognition_regexp` text,
  `visu_file_extension` varchar(32) default NULL,
  PRIMARY KEY  (`doctype_id`),
  UNIQUE KEY `UNIQ_doctypes_1` (`doctype_number`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_number` varchar(32) NOT NULL,
  `category_description` varchar(512) default NULL,
  `category_icon` varchar(32) default NULL,
  PRIMARY KEY  (`category_id`),
  UNIQUE KEY `UNIQ_categories_1` (`category_number`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_activities`
--

CREATE TABLE `galaxia_activities` (
  `activityId` int(14) NOT NULL auto_increment,
  `name` varchar(80) default NULL,
  `normalized_name` varchar(80) default NULL,
  `pId` int(14) NOT NULL default '0',
  `type` enum('start','end','split','switch','join','activity','standalone') default NULL,
  `isAutoRouted` char(1) default NULL,
  `flowNum` int(10) default NULL,
  `isInteractive` char(1) default NULL,
  `isAutomatic` char(1) default NULL,
  `isComment` char(1) default NULL,
  `lastModif` int(14) default NULL,
  `description` text,
  `expirationTime` int(6) unsigned NOT NULL default '0',
  PRIMARY KEY  (`activityId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_activity_roles`
--

CREATE TABLE `galaxia_activity_roles` (
  `activityId` int(14) NOT NULL default '0',
  `roleId` int(14) NOT NULL default '0',
  PRIMARY KEY  (`activityId`,`roleId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_instances`
--

CREATE TABLE `galaxia_instances` (
  `instanceId` int(14) NOT NULL auto_increment,
  `pId` int(14) NOT NULL default '0',
  `started` int(14) default NULL,
  `name` varchar(200) default 'No Name',
  `owner` varchar(200) default NULL,
  `nextActivity` int(14) default NULL,
  `nextUser` varchar(200) default NULL,
  `ended` int(14) default NULL,
  `status` enum('active','exception','aborted','completed') default NULL,
  `properties` longblob,
  PRIMARY KEY  (`instanceId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_instance_activities`
--

CREATE TABLE `galaxia_instance_activities` (
  `instanceId` int(14) NOT NULL default '0',
  `activityId` int(14) NOT NULL default '0',
  `started` int(14) NOT NULL default '0',
  `ended` int(14) NOT NULL default '0',
  `user` varchar(40) default NULL,
  `status` enum('running','completed') default NULL,
  PRIMARY KEY  (`instanceId`,`activityId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_instance_comments`
--

CREATE TABLE `galaxia_instance_comments` (
  `cId` int(14) NOT NULL auto_increment,
  `instanceId` int(14) NOT NULL default '0',
  `user` varchar(40) default NULL,
  `activityId` int(14) default NULL,
  `hash` varchar(32) default NULL,
  `title` varchar(250) default NULL,
  `comment` text,
  `activity` varchar(80) default NULL,
  `timestamp` int(14) default NULL,
  PRIMARY KEY  (`cId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_processes`
--

CREATE TABLE `galaxia_processes` (
  `pId` int(14) NOT NULL auto_increment,
  `name` varchar(80) default NULL,
  `isValid` char(1) default NULL,
  `isActive` char(1) default NULL,
  `version` varchar(12) default NULL,
  `description` text,
  `lastModif` int(14) default NULL,
  `normalized_name` varchar(80) default NULL,
  PRIMARY KEY  (`pId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_roles`
--

CREATE TABLE `galaxia_roles` (
  `roleId` int(14) NOT NULL auto_increment,
  `pId` int(14) NOT NULL default '0',
  `lastModif` int(14) default NULL,
  `name` varchar(80) default NULL,
  `description` text,
  PRIMARY KEY  (`roleId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_transitions`
--

CREATE TABLE `galaxia_transitions` (
  `pId` int(14) NOT NULL default '0',
  `actFromId` int(14) NOT NULL default '0',
  `actToId` int(14) NOT NULL default '0',
  PRIMARY KEY  (`actFromId`,`actToId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_user_roles`
--

CREATE TABLE `galaxia_user_roles` (
  `pId` int(14) NOT NULL default '0',
  `roleId` int(14) NOT NULL auto_increment,
  `user` varchar(40) NOT NULL default '',
  PRIMARY KEY  (`roleId`,`user`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_workitems`
--

CREATE TABLE `galaxia_workitems` (
  `itemId` int(14) NOT NULL auto_increment,
  `instanceId` int(14) NOT NULL default '0',
  `orderId` int(14) NOT NULL default '0',
  `activityId` int(14) NOT NULL default '0',
  `properties` longblob,
  `started` int(14) default NULL,
  `ended` int(14) default NULL,
  `user` varchar(40) default NULL,
  PRIMARY KEY  (`itemId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `rb_groups`
--

CREATE TABLE `rb_groups` (
  `group_id` int(11) NOT NULL,
  `group_define_name` varchar(32) default NULL,
  `group_description` varchar(64) default NULL,
  `is_active` tinyint(1) default '1',
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `rb_groups_uniq1` (`group_define_name`),
  KEY `INDEX_rb_groups_1` (`group_define_name`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `rb_groupusers`
--

CREATE TABLE `rb_groupusers` (
  `auth_user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  UNIQUE KEY `rb_groupusers_uniq1` (`auth_user_id`,`group_id`),
  KEY `INDEX_rb_groupusers_1` (`auth_user_id`),
  KEY `INDEX_rb_groupusers_2` (`group_id`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `rb_group_subgroups`
--

CREATE TABLE `rb_group_subgroups` (
  `group_id` int(11) NOT NULL,
  `subgroup_id` int(11) NOT NULL,
  UNIQUE KEY `rb_group_subgroups_uniq1` (`group_id`,`subgroup_id`),
  KEY `INDEX_rb_group_subgroups_1` (`group_id`),
  KEY `INDEX_rb_group_subgroups_2` (`subgroup_id`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `rb_users`
--

CREATE TABLE `rb_users` (
  `auth_user_id` int(11) NOT NULL,
  `handle` varchar(128) NOT NULL,
  `passwd` varchar(32) default NULL,
  `email` varchar(128) default NULL,
  `lastlogin` timestamp NULL default '0000-00-00 00:00:00',
  `is_active` tinyint(1) default '1',
  PRIMARY KEY  (`auth_user_id`),
  UNIQUE KEY `handle` (`handle`),
  UNIQUE KEY `auth_user_id_idx` (`auth_user_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `messu_archive`
--

CREATE TABLE `messu_archive` (
  `msgId` int(14) NOT NULL auto_increment,
  `user` varchar(40) NOT NULL default '',
  `user_from` varchar(40) NOT NULL default '',
  `user_to` text,
  `user_cc` text,
  `user_bcc` text,
  `subject` varchar(255) default NULL,
  `body` text,
  `hash` varchar(32) default NULL,
  `replyto_hash` varchar(32) default NULL,
  `date` int(14) default NULL,
  `isRead` char(1) default NULL,
  `isReplied` char(1) default NULL,
  `isFlagged` char(1) default NULL,
  `priority` int(2) default NULL,
  PRIMARY KEY  (`msgId`)
) ENGINE=InnoDB  AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Structure de la table `messu_messages`
--

CREATE TABLE `messu_messages` (
  `msgId` int(14) NOT NULL auto_increment,
  `user` varchar(40) NOT NULL default '',
  `user_from` varchar(200) NOT NULL default '',
  `user_to` text,
  `user_cc` text,
  `user_bcc` text,
  `subject` varchar(255) default NULL,
  `body` text,
  `hash` varchar(32) default NULL,
  `replyto_hash` varchar(32) default NULL,
  `date` int(14) default NULL,
  `isRead` char(1) default NULL,
  `isReplied` char(1) default NULL,
  `isFlagged` char(1) default NULL,
  `priority` int(2) default NULL,
  PRIMARY KEY  (`msgId`)
) ENGINE=InnoDB  AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Structure de la table `messu_sent`
--

CREATE TABLE `messu_sent` (
  `msgId` int(14) NOT NULL auto_increment,
  `user` varchar(40) NOT NULL default '',
  `user_from` varchar(40) NOT NULL default '',
  `user_to` text,
  `user_cc` text,
  `user_bcc` text,
  `subject` varchar(255) default NULL,
  `body` text,
  `hash` varchar(32) default NULL,
  `replyto_hash` varchar(32) default NULL,
  `date` int(14) default NULL,
  `isRead` char(1) default NULL,
  `isReplied` char(1) default NULL,
  `isFlagged` char(1) default NULL,
  `priority` int(2) default NULL,
  PRIMARY KEY  (`msgId`)
) ENGINE=InnoDB  AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

CREATE TABLE `messu_notification` (
  `user_id` int(11) NOT NULL,
  KEY  (`user_id`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `partners`
--

CREATE TABLE `partners` (
  `partner_id` int(11) NOT NULL,
  `partner_number` varchar(128) NOT NULL,
  `partner_type` enum('customer','supplier','staff') default NULL,
  `first_name` varchar(64) default NULL,
  `last_name` varchar(64) default NULL,
  `adress` varchar(512) default NULL,
  `city` varchar(64) default NULL,
  `zip_code` int(11) default NULL,
  `phone` varchar(64) default NULL,
  `cell_phone` varchar(64) default NULL,
  `mail` varchar(64) default NULL,
  `web_site` varchar(64) default NULL,
  `activity` varchar(64) default NULL,
  `company` varchar(64) default NULL,
  PRIMARY KEY  (`partner_id`),
  UNIQUE KEY `UC_partner_number` (`partner_number`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL,
  `project_number` varchar(128) NOT NULL,
  `project_description` varchar(512) default NULL,
  `project_state` varchar(32) NOT NULL default 'init',
  `project_indice_id` int(11) default NULL,
  `default_process_id` int(11) default NULL,
  `open_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `close_date` int(11) default NULL,
  PRIMARY KEY  (`project_id`),
  UNIQUE KEY `UC_project_number` (`project_number`),
  KEY `INDEX_projects_1` (`project_indice_id`),
  KEY `INDEX_projects_2` (`project_description`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `project_history`
--

CREATE TABLE `project_history` (
  `histo_order` int(11) NOT NULL,
  `action_name` varchar(32) NOT NULL,
  `action_by` varchar(32) NOT NULL,
  `action_date` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `project_number` varchar(16)  default NULL,
  `project_description` varchar(128) default NULL,
  `project_state` varchar(16)  default NULL,
  `project_indice_id` int(11) default NULL,
  `default_process_id` int(11) default NULL,
  `open_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `close_date` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `tiki_user_tasks`
--

CREATE TABLE `tiki_user_tasks` (
  `task_id` int(14) NOT NULL auto_increment,
  `last_version` int(4) NOT NULL default '0',
  `user` varchar(40) NOT NULL default '',
  `creator` varchar(200) NOT NULL default '',
  `public_for_group` int(11) default NULL,
  `rights_by_creator` char(1) default NULL,
  `created` int(14) NOT NULL default '0',
  `status` char(1) default NULL,
  `priority` int(2) default NULL,
  `completed` int(14) default NULL,
  `percentage` int(4) default NULL,
  PRIMARY KEY  (`task_id`),
  UNIQUE KEY `creator` (`creator`,`created`),
  UNIQUE KEY `creator_2` (`creator`,`created`)
) ENGINE=InnoDB  AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `tiki_user_tasks_history`
--

CREATE TABLE `tiki_user_tasks_history` (
  `belongs_to` int(14) NOT NULL default '0',
  `task_version` int(4) NOT NULL default '0',
  `title` varchar(250) NOT NULL default '',
  `description` text,
  `start` int(14) default NULL,
  `end` int(14) default NULL,
  `lasteditor` varchar(200) NOT NULL default '',
  `lastchanges` int(14) NOT NULL default '0',
  `priority` int(2) NOT NULL default '3',
  `completed` int(14) default NULL,
  `deleted` int(14) default NULL,
  `status` char(1) default NULL,
  `percentage` int(4) default NULL,
  `accepted_creator` char(1) default NULL,
  `accepted_user` char(1) default NULL,
  PRIMARY KEY  (`belongs_to`,`task_version`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `user_prefs`
--

CREATE TABLE `user_prefs` (
  `user_id` int(11) NOT NULL,
  `css_sheet` varchar(32)  NOT NULL default 'default',
  `lang` varchar(32)  NOT NULL default 'default',
  `long_date_format` varchar(32)  NOT NULL default 'default',
  `short_date_format` varchar(32)  NOT NULL default 'default',
  `date_input_method` varchar(16)  NOT NULL default 'default',
  `hour_format` varchar(32)  NOT NULL default 'default',
  `time_zone` varchar(32)  NOT NULL default 'default',
  `wildspace_path` varchar(128)  NOT NULL default 'default',
  `max_record` varchar(128)  NOT NULL default 'default',
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB;


-- ------------------------------------------------------------------------------
CREATE TABLE `metadata_dictionary` (
  `property_id` int(11) NOT NULL,
  `property_fieldname` varchar(32) NOT NULL default '',
  `property_name` varchar(32) NOT NULL default '',
  `property_description` varchar(128) NOT NULL default '',
  `property_type` varchar(32) NOT NULL default '',
  `property_length` int(11) default NULL,
  `extend_table` varchar(64) NOT NULL,
  `regex` varchar(255) default NULL,
  `return_name` int(1) NOT NULL default '0',
  `is_required` int(1) NOT NULL default '0',
  `is_multiple` int(1) NOT NULL default '0',
  `is_hide` int(1) NOT NULL default '0',
  `select_list` varchar(255) default NULL,
  `selectdb_where` varchar(255) default NULL,
  `selectdb_table` varchar(32) default NULL,
  `selectdb_field_for_value` varchar(32) default NULL,
  `selectdb_field_for_display` varchar(32) default NULL,
  `date_format` varchar(32) default NULL,
  `display_both` tinyint(1) NOT NULL default 0,
  PRIMARY KEY (`property_id`),
  UNIQUE KEY `UNIQ_metadata_dictionary_1` (`property_name`,`extend_table`),
  KEY `INDEX_metadata_dictionary_1` (`extend_table`),
  KEY `INDEX_metadata_dictionary_2` (`property_description`)
) ENGINE=InnoDB;


-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE TABLE `reposits` (
  `reposit_id` int(11) NOT NULL,
  `reposit_number` varchar(32) NOT NULL,
  `reposit_name` varchar(64) NOT NULL,
  `reposit_description` text,
  `reposit_type` int(1) default 1,
  `reposit_mode` int(1) default 1,
  `reposit_url` varchar(256) NOT NULL,
  `is_active` int(1) default 0,
  `priority` int(1) default 0,
  `open_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `space_id` int(11) default 0,
  PRIMARY KEY  (`reposit_id`),
  UNIQUE KEY `UNIQ_reposits_1` (`reposit_url`(256)),
  KEY `INDEX_reposits_1` (`reposit_number`),
  KEY `INDEX_reposits_2` (`is_active`),
  KEY `INDEX_reposits_3` (`priority`),
  KEY `INDEX_reposits_4` (`space_id`),
  KEY `INDEX_reposits_5` (`reposit_type`)
) ENGINE=InnoDB;


-- --------------------------------------------------------

--
-- Structure de la table `import_history`
--
CREATE TABLE `import_history` (
  `import_order` int(11) NOT NULL default '0',
  `container_id` int(11) NOT NULL default '0',
  `description` varchar(64) default NULL,
  `state` varchar(32) default NULL,
  `import_date` int(11) default NULL,
  `import_by` int(11) default NULL,
  `errors_report` text,
  `logfile` text,
  `listfile` text,
  `file_name` varchar(128) NOT NULL default '',
  `file_extension` varchar(32) default NULL,
  `file_mtime` int(11) default NULL,
  `file_size` int(11) default NULL,
  `file_md5` varchar(128) NOT NULL default '',
  PRIMARY KEY  (`import_order`)
) ENGINE=InnoDB;


-- --------------------------------------------------------

--
-- Structure de la table `tickets`
--

CREATE TABLE `tickets` (
  `ticket_id` text(40) NOT NULL,
  `expiration_date` int(11) NOT NULL,
  `for_object_id` int(11) NOT NULL,
  KEY `INDEX_tickets_1` (`ticket_id` (20), `for_object_id`),
  KEY `INDEX_tickets_2` (`expiration_date`)
) ENGINE=InnoDB;


--
-- Contraintes pour la table `import_history`
--
ALTER TABLE `import_history`
  ADD CONSTRAINT `FK_import_history_1` FOREIGN KEY (`import_order`)
  REFERENCES `objects` (`object_id`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `FK_import_history_2` FOREIGN KEY (`import_by`)
  REFERENCES `rb_users` (`auth_user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_import_history_3` FOREIGN KEY (`container_id`)
  REFERENCES `objects` (`object_id`) ON UPDATE CASCADE ON DELETE CASCADE;

--
-- Contraintes pour la table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `FK_projects_1` FOREIGN KEY (`project_id`)
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_projects_2` FOREIGN KEY (`project_indice_id`)
  REFERENCES `indices` (`indice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_projects_3` FOREIGN KEY (`default_process_id`)
  REFERENCES `galaxia_processes` (`pId`) ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `user_prefs`
  ADD CONSTRAINT `FK_user_prefs_1` FOREIGN KEY (`user_id`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `rb_groupusers`
  ADD CONSTRAINT `FK_rb_groupusers_1` FOREIGN KEY (`auth_user_id`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_rb_groupusers_2` FOREIGN KEY (`group_id`) 
  REFERENCES `rb_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `rb_group_subgroups`
  ADD CONSTRAINT `FK_rb_group_subgroups_1` FOREIGN KEY (`group_id`) 
  REFERENCES `rb_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_rb_group_subgroups_2` FOREIGN KEY (`subgroup_id`) 
  REFERENCES `rb_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `messu_notification` 
  ADD CONSTRAINT `FK_messu_notification_1` FOREIGN KEY (`user_id`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `doctypes` 
  ADD CONSTRAINT `FK_doctypes_1` FOREIGN KEY (`doctype_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `partners` 
  ADD CONSTRAINT `FK_partner_1` FOREIGN KEY (`partner_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `metadata_dictionary` 
  ADD CONSTRAINT `FK_metadata_dictionary_1` FOREIGN KEY (`property_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `objects_rel`
  ADD CONSTRAINT `FK_objects_rel_1` FOREIGN KEY (`parent_id`)
    REFERENCES `objects` (`object_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_objects_rel_2` FOREIGN KEY (`child_id`)
    REFERENCES `objects` (`object_id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `%container%_categories`
--
ALTER TABLE `categories` 
  ADD CONSTRAINT `FK_categories_1` FOREIGN KEY (`category_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reposits`
--
ALTER TABLE `reposits`
  ADD CONSTRAINT `FK_reposits_1` FOREIGN KEY (`open_by`)
  REFERENCES `rb_users` (`auth_user_id`) ON UPDATE CASCADE;


-- --------------------------------------------------------

--
-- Structure de la table `bookshops`
--
CREATE TABLE `bookshops` (
  `bookshop_id` int(11) NOT NULL default '0',
  `bookshop_number` varchar(128) NOT NULL,
  `bookshop_name` varchar(128) default '',
  `bookshop_state` varchar(32) NOT NULL default 'init',
  `bookshop_description` varchar(512) default NULL,
  `file_only` tinyint(1) NOT NULL default '0',
  `bookshop_version` int(11) default NULL,
  `bookshop_iteration` int(11) default NULL,
  `project_id` int(11) default NULL,
  `default_process_id` int(11) default NULL,
  `default_read_id` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `owned_by` int(11) default NULL,
  PRIMARY KEY (`bookshop_id`),
  UNIQUE KEY `UC_bookshop_number` (`bookshop_number`),
  KEY `INDEX_bookshops_1` (`bookshop_version`),
  KEY `INDEX_bookshops_2` (`project_id`),
  KEY `INDEX_bookshops_3` (`bookshop_description`),
  KEY `INDEX_bookshops_4` (`file_only`),
  KEY `INDEX_bookshops_5` (`default_process_id`),
  KEY `INDEX_bookshops_6` (`default_read_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_alias`
--

CREATE TABLE `bookshop_alias` (
  `alias_id` int(11) NOT NULL,
  `bookshop_id` int(11) NOT NULL,
  `bookshop_number` varchar(16) NOT NULL,
  `bookshop_description` varchar(128) default NULL,
  PRIMARY KEY  (`alias_id`),
  UNIQUE KEY `UNIQ_bookshop_alias` (`alias_id`,`bookshop_id`),
  KEY `INDEX_bookshop_alias_1` (`bookshop_id`),
  KEY `INDEX_bookshop_alias_2` (`bookshop_number`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_doccomments`
--

CREATE TABLE `bookshop_doccomments` (
  `comment_id` INT NOT NULL ,
  `document_id` INT NOT NULL ,
  `comment` TEXT NOT NULL ,
  `open_by` INT NOT NULL ,
  PRIMARY KEY  (`comment_id`),
  KEY `INDEX_bookshop_doccomments_1` (`document_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_documents`
--
CREATE TABLE `bookshop_documents` (
  `document_id` int(11) NOT NULL,
  `document_bid` CHAR(15) NOT NULL,
  `document_number` varchar(128)  NOT NULL,
  `document_name` varchar(256)  NOT NULL,
  `document_state` varchar(32)  NOT NULL default 'init',
  `document_access_code` int(11) NOT NULL default 0,
  `document_iteration` int(11) NOT NULL default 1,
  `document_version` int(11) NOT NULL default 1,
  `document_life_stage` int(1) NOT NULL default 1,
  `from_document` int(11) default NULL,
  `bookshop_id` int(11) NOT NULL,
  `default_process_id` int(11) default NULL,
  `instance_id` int(11) default NULL,
  `doctype_id` int(11) NOT NULL,
  `category_id` int(11) default NULL,
  `check_out_by` int(11) default NULL,
  `check_out_date` int(11) default NULL,
  `description` varchar(128)  default NULL,
  `update_date` int(11) default NULL,
  `update_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `owned_by` int(11) default NULL,
  PRIMARY KEY  (`document_id`),
  UNIQUE KEY `UNIQ_bookshop_documents_1` (`document_number`,`document_version`, `document_iteration`),
  UNIQUE KEY `UNIQ_bookshop_documents_2` (`document_bid`,`document_version`, `document_iteration`),
  KEY `INDEX_bookshop_documents_1` (`document_number`),
  KEY `INDEX_bookshop_documents_2` (`document_version`),
  KEY `INDEX_bookshop_documents_3` (`document_iteration`),
  KEY `INDEX_bookshop_documents_4` (`doctype_id`),
  KEY `INDEX_bookshop_documents_5` (`bookshop_id`),
  KEY `INDEX_bookshop_documents_6` (`category_id`),
  KEY `INDEX_bookshop_documents_7` (`document_state`),
  KEY `INDEX_bookshop_documents_8` (`description`),
  KEY `INDEX_bookshop_documents_9` (`from_document`),
  KEY `INDEX_bookshop_documents_10` (`check_out_by`),
  KEY `INDEX_bookshop_documents_11` (`update_by`),
  KEY `INDEX_bookshop_documents_12` (`open_by`),
  KEY `INDEX_bookshop_documents_13` (`document_bid`),
  KEY `INDEX_bookshop_documents_14` (`document_name`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_doc_files`
--
CREATE TABLE `bookshop_doc_files` (
  `file_id` int(11) NOT NULL,
  `file_bid` CHAR(15) NOT NULL,
  `document_id` int(11) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `reposit_id` int(11) NOT NULL,
  `file_version` int(11) NOT NULL default 1,
  `file_iteration` int(11) NOT NULL default 1,
  `file_access_code` int(11) NOT NULL default 0,
  `file_mode` int(1) NOT NULL default 0,
  `file_life_stage` int(1) NOT NULL default 1,
  `from_file` int(11) default NULL,
  `file_root_name` varchar(128) NOT NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) NOT NULL default 'init',
  `file_type` varchar(128) NOT NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_checkout_by` int(11) default NULL,
  `file_checkout_date` int(11) default NULL,
  `file_open_date` int(11) NOT NULL,
  `file_open_by` int(11) NOT NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `UNIQ_bookshop_doc_files_2` (`file_bid`, `file_version`, `file_iteration`),
  KEY `INDEX_bookshop_doc_files_1` (`document_id`),
  KEY `INDEX_bookshop_doc_files_2` (`file_name`),
  KEY `INDEX_bookshop_doc_files_3` (`file_root_name`),
  KEY `INDEX_bookshop_doc_files_4` (`file_extension`),
  KEY `INDEX_bookshop_doc_files_5` (`file_type`),
  KEY `INDEX_bookshop_doc_files_6` (`file_md5`),
  KEY `INDEX_bookshop_doc_files_7` (`file_checkout_by`),
  KEY `INDEX_bookshop_doc_files_8` (`file_name`, `file_iteration`),
  KEY `INDEX_bookshop_doc_files_9` (`reposit_id`)
) ENGINE=InnoDB;

CREATE TABLE `bookshop_doc_file_role` (
  `link_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `role_id` int(1) NOT NULL,
  PRIMARY KEY  (`link_id`),
  UNIQUE KEY `bookshop_doc_file_role_uniq1` (`file_id`,`role_id`)
) ENGINE=InnoDB;

CREATE TABLE `bookshop_files` (
  `file_id` int(11) NOT NULL,
  `file_bid` CHAR(15) NOT NULL,
  `bookshop_id` int(11) NOT NULL default '0',
  `import_order` int(11) default NULL,
  `file_name` varchar(128)  NOT NULL,
  `reposit_id` int(11) NOT NULL,
  `file_version` int(11) NOT NULL default 1,
  `file_iteration` int(11) NOT NULL default 1,
  `file_access_code` int(11) NOT NULL default 0,
  `file_life_stage` int(1) NOT NULL default 1,
  `file_root_name` varchar(128) NOT NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) NOT NULL default 'init',
  `file_type` varchar(128) NOT NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_open_date` int(11) NOT NULL,
  `file_open_by` int(11) NOT NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  `from_file` int(11) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `UNIQ_bookshop_files_2` (`file_bid`, `file_version`, `file_iteration`),
  KEY `INDEX_bookshop_files_1` (`bookshop_id`),
  KEY `INDEX_bookshop_files_2` (`file_name`),
  KEY `INDEX_bookshop_files_3` (`file_root_name`),
  KEY `INDEX_bookshop_files_4` (`file_extension`),
  KEY `INDEX_bookshop_files_5` (`file_type`),
  KEY `INDEX_bookshop_files_6` (`file_md5`),
  KEY `INDEX_bookshop_files_8` (`file_name`, `file_iteration`),
  KEY `INDEX_bookshop_files_9` (`reposit_id`),
  KEY `INDEX_bookshop_files_10` (`import_order`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_doc_rel`
--

CREATE TABLE `bookshop_doc_rel` (
  `dr_link_id` int(11) NOT NULL,
  `dr_document_id` int(11) default NULL,
  `dr_l_document_id` int(11) default NULL,
  `dr_access_code` int(11) NOT NULL default '15',
  PRIMARY KEY  (`dr_link_id`),
  UNIQUE KEY `bookshop_doc_rel_uniq1` (`dr_document_id`,`dr_l_document_id`),
  KEY `INDEX_bookshop_doc_rel_1` (`dr_document_id`),
  KEY `INDEX_bookshop_doc_rel_2` (`dr_l_document_id`)
) ENGINE=InnoDB;


-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE TABLE `bookshop_cont_notifications` (
  `notification_id` INT NOT NULL ,
  `user_id` INT NOT NULL ,
  `bookshop_id` INT NOT NULL ,
  `event` VARCHAR (64) NOT NULL ,
  `rb_condition` BLOB,
  PRIMARY KEY  (`notification_id`),
  KEY `INDEX_bookshop_cont_notifications_1` (`user_id`),
  KEY `INDEX_bookshop_cont_notifications_2` (`bookshop_id`),
  KEY `INDEX_bookshop_cont_notifications_3` (`event`),
  UNIQUE KEY `UNIQ_bookshop_cont_notifications_1` (`user_id`,`bookshop_id`,`event`)
) ENGINE=InnoDB;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE TABLE `bookshop_documents_extends` (
  `extend_id` int(11) NOT NULL,
  PRIMARY KEY (`extend_id`)
) ENGINE=InnoDB;

CREATE TABLE `bookshops_extends` (
  `extend_id` int(11) NOT NULL,
  PRIMARY KEY (`extend_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------
--
-- Structure de la table `bookshops_history`
--
CREATE TABLE `bookshops_history` (
  `histo_order` int(11) NOT NULL default '0',
  `action_name` varchar(32) default NULL,
  `action_by` varchar(32) default NULL,
  `action_date` int(11) default NULL,
  `bookshop_id` int(11) NOT NULL default '0',
  `bookshop_number` varchar(16) default NULL,
  `bookshop_state` varchar(16) default NULL,
  `bookshop_description` varchar(128) default NULL,
  `bookshop_version` int(11) default NULL,
  `project_id` int(11) default NULL,
  `default_read_id` text  default NULL,
  `default_process_id` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_files_history`
--
CREATE TABLE `bookshop_doc_files_history` (
  `histo_order` int(11) NOT NULL default 0,
  `action_name` varchar(32) NOT NULL default '',
  `action_by` varchar(64) NOT NULL default '',
  `action_date` int(11) NOT NULL default 0,
  `file_id` int(11) default NULL,
  `document_id` int(11) default NULL,
  `file_bid` CHAR(15) default NULL,
  `file_root_name` varchar(128) default NULL,
  `file_iteration` int(11) default NULL,
  `file_version` int(11) default NULL,
  `file_name` varchar(128) default NULL,
  `file_path` text default NULL,
  `file_access_code` int(11) default NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) default NULL,
  `file_type` varchar(128) default NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_open_date` int(11) default NULL,
  `file_open_by` int(11) default NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_documents_history`
--

CREATE TABLE `bookshop_documents_history` (
  `histo_order` int(11) NOT NULL default 0,
  `action_name` varchar(32) NOT NULL default '',
  `action_by` varchar(64) NOT NULL default '',
  `action_date` int(11) NOT NULL default 0,
  `document_id` int(11) default NULL,
  `bookshop_id` int(11) default NULL,
  `document_bid` CHAR(15)  default NULL,
  `document_number` varchar(128) default NULL,
  `document_state` varchar(32) default NULL,
  `document_access_code` int(11) default NULL,
  `document_iteration` int(11) default NULL,
  `document_version` int(11) default NULL,
  `description` varchar(128) default NULL,
  `document_life_stage` int(1) default NULL,
  `from_document` int(11) default NULL,
  `category_id` int(11) default NULL,
  `check_out_by` int(11) default NULL,
  `check_out_date` int(11) default NULL,
  `update_date` int(11) default NULL,
  `update_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `doctype_id` int(11) default NULL,
  `instance_id` int(11) default NULL,
  `activity_id` int(11) default NULL,
  `comment` varchar(255) default NULL,
  PRIMARY KEY  (`histo_order`),
  KEY `INDEX_bookshop_documents_history_1` (`document_number`),
  KEY `INDEX_bookshop_documents_history_2` (`document_version`),
  KEY `INDEX_bookshop_documents_history_3` (`document_iteration`),
  KEY `INDEX_bookshop_documents_history_5` (`bookshop_id`),
  KEY `INDEX_bookshop_documents_history_7` (`document_state`),
  KEY `INDEX_bookshop_documents_history_8` (`description`),
  KEY `INDEX_bookshop_documents_history_9` (`action_name`)
) ENGINE=InnoDB;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE ALGORITHM=TEMPTABLE VIEW `bookshop_alias_view` AS
SELECT
	`bookshops`.`bookshop_id` AS `bookshop_id`,
	`bookshop_alias`.`alias_id` AS `alias_id`,
	`bookshop_alias`.`bookshop_number` AS `bookshop_number`,
	`bookshop_alias`.`bookshop_description` AS `bookshop_description`,
	`bookshops`.`bookshop_state` AS `bookshop_state`,
	`bookshops`.`file_only` AS `file_only`,
	`bookshops`.`bookshop_version` AS `bookshop_version`,
	`bookshops`.`project_id` AS `project_id`,
	`bookshops`.`default_process_id` AS `default_process_id`,
	`bookshops`.`default_read_id` AS `default_read_id`,
	`bookshops`.`open_date` AS `open_date`,
	`bookshops`.`open_by` AS `open_by`,
	`bookshops`.`forseen_close_date` AS `forseen_close_date`,
	`bookshops`.`close_date` AS `close_date`,
	`bookshops`.`close_by` AS `close_by`
FROM `bookshop_alias`
LEFT JOIN `bookshops`
ON `bookshop_alias`.`bookshop_id` = `bookshops`.`bookshop_id`
UNION
SELECT
	`bookshops`.`bookshop_id` AS `bookshop_id`,
	'' AS `alias_id`,
	`bookshops`.`bookshop_number` AS `bookshop_number`,
	`bookshops`.`bookshop_description` AS `bookshop_description`,
	`bookshops`.`bookshop_state` AS `bookshop_state`,
	`bookshops`.`file_only` AS `file_only`,
	`bookshops`.`bookshop_version` AS `bookshop_version`,
	`bookshops`.`project_id` AS `project_id`,
	`bookshops`.`default_process_id` AS `default_process_id`,
	`bookshops`.`default_read_id` AS `default_read_id`,
	`bookshops`.`open_date` AS `open_date`,
	`bookshops`.`open_by` AS `open_by`,
	`bookshops`.`forseen_close_date` AS `forseen_close_date`,
	`bookshops`.`close_date` AS `close_date`,
	`bookshops`.`close_by` AS `close_by`
FROM `bookshops`
LEFT JOIN `bookshop_alias`
ON `bookshop_alias`.`bookshop_id` = `bookshops`.`bookshop_id`;

-- --------------------------------------------------------

--
-- Structure de la table `cadlibs`
--
CREATE TABLE `cadlibs` (
  `cadlib_id` int(11) NOT NULL default '0',
  `cadlib_number` varchar(128) NOT NULL,
  `cadlib_name` varchar(128) default '',
  `cadlib_state` varchar(32) NOT NULL default 'init',
  `cadlib_description` varchar(512) default NULL,
  `file_only` tinyint(1) NOT NULL default '0',
  `cadlib_version` int(11) default NULL,
  `cadlib_iteration` int(11) default NULL,
  `project_id` int(11) default NULL,
  `default_process_id` int(11) default NULL,
  `default_read_id` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `owned_by` int(11) default NULL,
  PRIMARY KEY (`cadlib_id`),
  UNIQUE KEY `UC_cadlib_number` (`cadlib_number`),
  KEY `INDEX_cadlibs_1` (`cadlib_version`),
  KEY `INDEX_cadlibs_2` (`project_id`),
  KEY `INDEX_cadlibs_3` (`cadlib_description`),
  KEY `INDEX_cadlibs_4` (`file_only`),
  KEY `INDEX_cadlibs_5` (`default_process_id`),
  KEY `INDEX_cadlibs_6` (`default_read_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_alias`
--

CREATE TABLE `cadlib_alias` (
  `alias_id` int(11) NOT NULL,
  `cadlib_id` int(11) NOT NULL,
  `cadlib_number` varchar(16) NOT NULL,
  `cadlib_description` varchar(128) default NULL,
  PRIMARY KEY  (`alias_id`),
  UNIQUE KEY `UNIQ_cadlib_alias` (`alias_id`,`cadlib_id`),
  KEY `INDEX_cadlib_alias_1` (`cadlib_id`),
  KEY `INDEX_cadlib_alias_2` (`cadlib_number`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_doccomments`
--

CREATE TABLE `cadlib_doccomments` (
  `comment_id` INT NOT NULL ,
  `document_id` INT NOT NULL ,
  `comment` TEXT NOT NULL ,
  `open_by` INT NOT NULL ,
  PRIMARY KEY  (`comment_id`),
  KEY `INDEX_cadlib_doccomments_1` (`document_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_documents`
--
CREATE TABLE `cadlib_documents` (
  `document_id` int(11) NOT NULL,
  `document_bid` CHAR(15) NOT NULL,
  `document_number` varchar(128)  NOT NULL,
  `document_name` varchar(256)  NOT NULL,
  `document_state` varchar(32)  NOT NULL default 'init',
  `document_access_code` int(11) NOT NULL default 0,
  `document_iteration` int(11) NOT NULL default 1,
  `document_version` int(11) NOT NULL default 1,
  `document_life_stage` int(1) NOT NULL default 1,
  `from_document` int(11) default NULL,
  `cadlib_id` int(11) NOT NULL,
  `default_process_id` int(11) default NULL,
  `instance_id` int(11) default NULL,
  `doctype_id` int(11) NOT NULL,
  `category_id` int(11) default NULL,
  `check_out_by` int(11) default NULL,
  `check_out_date` int(11) default NULL,
  `description` varchar(128)  default NULL,
  `update_date` int(11) default NULL,
  `update_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `owned_by` int(11) default NULL,
  PRIMARY KEY  (`document_id`),
  UNIQUE KEY `UNIQ_cadlib_documents_1` (`document_number`,`document_version`, `document_iteration`),
  UNIQUE KEY `UNIQ_cadlib_documents_2` (`document_bid`,`document_version`, `document_iteration`),
  KEY `INDEX_cadlib_documents_1` (`document_number`),
  KEY `INDEX_cadlib_documents_2` (`document_version`),
  KEY `INDEX_cadlib_documents_3` (`document_iteration`),
  KEY `INDEX_cadlib_documents_4` (`doctype_id`),
  KEY `INDEX_cadlib_documents_5` (`cadlib_id`),
  KEY `INDEX_cadlib_documents_6` (`category_id`),
  KEY `INDEX_cadlib_documents_7` (`document_state`),
  KEY `INDEX_cadlib_documents_8` (`description`),
  KEY `INDEX_cadlib_documents_9` (`from_document`),
  KEY `INDEX_cadlib_documents_10` (`check_out_by`),
  KEY `INDEX_cadlib_documents_11` (`update_by`),
  KEY `INDEX_cadlib_documents_12` (`open_by`),
  KEY `INDEX_cadlib_documents_13` (`document_bid`),
  KEY `INDEX_cadlib_documents_14` (`document_name`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_doc_files`
--
CREATE TABLE `cadlib_doc_files` (
  `file_id` int(11) NOT NULL,
  `file_bid` CHAR(15) NOT NULL,
  `document_id` int(11) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `reposit_id` int(11) NOT NULL,
  `file_version` int(11) NOT NULL default 1,
  `file_iteration` int(11) NOT NULL default 1,
  `file_access_code` int(11) NOT NULL default 0,
  `file_mode` int(1) NOT NULL default 0,
  `file_life_stage` int(1) NOT NULL default 1,
  `from_file` int(11) default NULL,
  `file_root_name` varchar(128) NOT NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) NOT NULL default 'init',
  `file_type` varchar(128) NOT NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_checkout_by` int(11) default NULL,
  `file_checkout_date` int(11) default NULL,
  `file_open_date` int(11) NOT NULL,
  `file_open_by` int(11) NOT NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `UNIQ_cadlib_doc_files_2` (`file_bid`, `file_version`, `file_iteration`),
  KEY `INDEX_cadlib_doc_files_1` (`document_id`),
  KEY `INDEX_cadlib_doc_files_2` (`file_name`),
  KEY `INDEX_cadlib_doc_files_3` (`file_root_name`),
  KEY `INDEX_cadlib_doc_files_4` (`file_extension`),
  KEY `INDEX_cadlib_doc_files_5` (`file_type`),
  KEY `INDEX_cadlib_doc_files_6` (`file_md5`),
  KEY `INDEX_cadlib_doc_files_7` (`file_checkout_by`),
  KEY `INDEX_cadlib_doc_files_8` (`file_name`, `file_iteration`),
  KEY `INDEX_cadlib_doc_files_9` (`reposit_id`)
) ENGINE=InnoDB;

CREATE TABLE `cadlib_doc_file_role` (
  `link_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `role_id` int(1) NOT NULL,
  PRIMARY KEY  (`link_id`),
  UNIQUE KEY `cadlib_doc_file_role_uniq1` (`file_id`,`role_id`)
) ENGINE=InnoDB;

CREATE TABLE `cadlib_files` (
  `file_id` int(11) NOT NULL,
  `file_bid` CHAR(15) NOT NULL,
  `cadlib_id` int(11) NOT NULL default '0',
  `import_order` int(11) default NULL,
  `file_name` varchar(128)  NOT NULL,
  `reposit_id` int(11) NOT NULL,
  `file_version` int(11) NOT NULL default 1,
  `file_iteration` int(11) NOT NULL default 1,
  `file_access_code` int(11) NOT NULL default 0,
  `file_life_stage` int(1) NOT NULL default 1,
  `file_root_name` varchar(128) NOT NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) NOT NULL default 'init',
  `file_type` varchar(128) NOT NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_open_date` int(11) NOT NULL,
  `file_open_by` int(11) NOT NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  `from_file` int(11) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `UNIQ_cadlib_files_2` (`file_bid`, `file_version`, `file_iteration`),
  KEY `INDEX_cadlib_files_1` (`cadlib_id`),
  KEY `INDEX_cadlib_files_2` (`file_name`),
  KEY `INDEX_cadlib_files_3` (`file_root_name`),
  KEY `INDEX_cadlib_files_4` (`file_extension`),
  KEY `INDEX_cadlib_files_5` (`file_type`),
  KEY `INDEX_cadlib_files_6` (`file_md5`),
  KEY `INDEX_cadlib_files_8` (`file_name`, `file_iteration`),
  KEY `INDEX_cadlib_files_9` (`reposit_id`),
  KEY `INDEX_cadlib_files_10` (`import_order`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_doc_rel`
--

CREATE TABLE `cadlib_doc_rel` (
  `dr_link_id` int(11) NOT NULL,
  `dr_document_id` int(11) default NULL,
  `dr_l_document_id` int(11) default NULL,
  `dr_access_code` int(11) NOT NULL default '15',
  PRIMARY KEY  (`dr_link_id`),
  UNIQUE KEY `cadlib_doc_rel_uniq1` (`dr_document_id`,`dr_l_document_id`),
  KEY `INDEX_cadlib_doc_rel_1` (`dr_document_id`),
  KEY `INDEX_cadlib_doc_rel_2` (`dr_l_document_id`)
) ENGINE=InnoDB;


-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE TABLE `cadlib_cont_notifications` (
  `notification_id` INT NOT NULL ,
  `user_id` INT NOT NULL ,
  `cadlib_id` INT NOT NULL ,
  `event` VARCHAR (64) NOT NULL ,
  `rb_condition` BLOB,
  PRIMARY KEY  (`notification_id`),
  KEY `INDEX_cadlib_cont_notifications_1` (`user_id`),
  KEY `INDEX_cadlib_cont_notifications_2` (`cadlib_id`),
  KEY `INDEX_cadlib_cont_notifications_3` (`event`),
  UNIQUE KEY `UNIQ_cadlib_cont_notifications_1` (`user_id`,`cadlib_id`,`event`)
) ENGINE=InnoDB;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE TABLE `cadlib_documents_extends` (
  `extend_id` int(11) NOT NULL,
  PRIMARY KEY (`extend_id`)
) ENGINE=InnoDB;

CREATE TABLE `cadlibs_extends` (
  `extend_id` int(11) NOT NULL,
  PRIMARY KEY (`extend_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------
--
-- Structure de la table `cadlibs_history`
--
CREATE TABLE `cadlibs_history` (
  `histo_order` int(11) NOT NULL default '0',
  `action_name` varchar(32) default NULL,
  `action_by` varchar(32) default NULL,
  `action_date` int(11) default NULL,
  `cadlib_id` int(11) NOT NULL default '0',
  `cadlib_number` varchar(16) default NULL,
  `cadlib_state` varchar(16) default NULL,
  `cadlib_description` varchar(128) default NULL,
  `cadlib_version` int(11) default NULL,
  `project_id` int(11) default NULL,
  `default_read_id` text  default NULL,
  `default_process_id` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_files_history`
--
CREATE TABLE `cadlib_doc_files_history` (
  `histo_order` int(11) NOT NULL default 0,
  `action_name` varchar(32) NOT NULL default '',
  `action_by` varchar(64) NOT NULL default '',
  `action_date` int(11) NOT NULL default 0,
  `file_id` int(11) default NULL,
  `document_id` int(11) default NULL,
  `file_bid` CHAR(15) default NULL,
  `file_root_name` varchar(128) default NULL,
  `file_iteration` int(11) default NULL,
  `file_version` int(11) default NULL,
  `file_name` varchar(128) default NULL,
  `file_path` text default NULL,
  `file_access_code` int(11) default NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) default NULL,
  `file_type` varchar(128) default NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_open_date` int(11) default NULL,
  `file_open_by` int(11) default NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_documents_history`
--

CREATE TABLE `cadlib_documents_history` (
  `histo_order` int(11) NOT NULL default 0,
  `action_name` varchar(32) NOT NULL default '',
  `action_by` varchar(64) NOT NULL default '',
  `action_date` int(11) NOT NULL default 0,
  `document_id` int(11) default NULL,
  `cadlib_id` int(11) default NULL,
  `document_bid` CHAR(15)  default NULL,
  `document_number` varchar(128) default NULL,
  `document_state` varchar(32) default NULL,
  `document_access_code` int(11) default NULL,
  `document_iteration` int(11) default NULL,
  `document_version` int(11) default NULL,
  `description` varchar(128) default NULL,
  `document_life_stage` int(1) default NULL,
  `from_document` int(11) default NULL,
  `category_id` int(11) default NULL,
  `check_out_by` int(11) default NULL,
  `check_out_date` int(11) default NULL,
  `update_date` int(11) default NULL,
  `update_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `doctype_id` int(11) default NULL,
  `instance_id` int(11) default NULL,
  `activity_id` int(11) default NULL,
  `comment` varchar(255) default NULL,
  PRIMARY KEY  (`histo_order`),
  KEY `INDEX_cadlib_documents_history_1` (`document_number`),
  KEY `INDEX_cadlib_documents_history_2` (`document_version`),
  KEY `INDEX_cadlib_documents_history_3` (`document_iteration`),
  KEY `INDEX_cadlib_documents_history_5` (`cadlib_id`),
  KEY `INDEX_cadlib_documents_history_7` (`document_state`),
  KEY `INDEX_cadlib_documents_history_8` (`description`),
  KEY `INDEX_cadlib_documents_history_9` (`action_name`)
) ENGINE=InnoDB;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE ALGORITHM=TEMPTABLE VIEW `cadlib_alias_view` AS
SELECT
	`cadlibs`.`cadlib_id` AS `cadlib_id`,
	`cadlib_alias`.`alias_id` AS `alias_id`,
	`cadlib_alias`.`cadlib_number` AS `cadlib_number`,
	`cadlib_alias`.`cadlib_description` AS `cadlib_description`,
	`cadlibs`.`cadlib_state` AS `cadlib_state`,
	`cadlibs`.`file_only` AS `file_only`,
	`cadlibs`.`cadlib_version` AS `cadlib_version`,
	`cadlibs`.`project_id` AS `project_id`,
	`cadlibs`.`default_process_id` AS `default_process_id`,
	`cadlibs`.`default_read_id` AS `default_read_id`,
	`cadlibs`.`open_date` AS `open_date`,
	`cadlibs`.`open_by` AS `open_by`,
	`cadlibs`.`forseen_close_date` AS `forseen_close_date`,
	`cadlibs`.`close_date` AS `close_date`,
	`cadlibs`.`close_by` AS `close_by`
FROM `cadlib_alias`
LEFT JOIN `cadlibs`
ON `cadlib_alias`.`cadlib_id` = `cadlibs`.`cadlib_id`
UNION
SELECT
	`cadlibs`.`cadlib_id` AS `cadlib_id`,
	'' AS `alias_id`,
	`cadlibs`.`cadlib_number` AS `cadlib_number`,
	`cadlibs`.`cadlib_description` AS `cadlib_description`,
	`cadlibs`.`cadlib_state` AS `cadlib_state`,
	`cadlibs`.`file_only` AS `file_only`,
	`cadlibs`.`cadlib_version` AS `cadlib_version`,
	`cadlibs`.`project_id` AS `project_id`,
	`cadlibs`.`default_process_id` AS `default_process_id`,
	`cadlibs`.`default_read_id` AS `default_read_id`,
	`cadlibs`.`open_date` AS `open_date`,
	`cadlibs`.`open_by` AS `open_by`,
	`cadlibs`.`forseen_close_date` AS `forseen_close_date`,
	`cadlibs`.`close_date` AS `close_date`,
	`cadlibs`.`close_by` AS `close_by`
FROM `cadlibs`
LEFT JOIN `cadlib_alias`
ON `cadlib_alias`.`cadlib_id` = `cadlibs`.`cadlib_id`;

-- --------------------------------------------------------

--
-- Structure de la table `mockups`
--
CREATE TABLE `mockups` (
  `mockup_id` int(11) NOT NULL default '0',
  `mockup_number` varchar(128) NOT NULL,
  `mockup_name` varchar(128) default '',
  `mockup_state` varchar(32) NOT NULL default 'init',
  `mockup_description` varchar(512) default NULL,
  `file_only` tinyint(1) NOT NULL default '0',
  `mockup_version` int(11) default NULL,
  `mockup_iteration` int(11) default NULL,
  `project_id` int(11) default NULL,
  `default_process_id` int(11) default NULL,
  `default_read_id` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `owned_by` int(11) default NULL,
  PRIMARY KEY (`mockup_id`),
  UNIQUE KEY `UC_mockup_number` (`mockup_number`),
  KEY `INDEX_mockups_1` (`mockup_version`),
  KEY `INDEX_mockups_2` (`project_id`),
  KEY `INDEX_mockups_3` (`mockup_description`),
  KEY `INDEX_mockups_4` (`file_only`),
  KEY `INDEX_mockups_5` (`default_process_id`),
  KEY `INDEX_mockups_6` (`default_read_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_alias`
--

CREATE TABLE `mockup_alias` (
  `alias_id` int(11) NOT NULL,
  `mockup_id` int(11) NOT NULL,
  `mockup_number` varchar(16) NOT NULL,
  `mockup_description` varchar(128) default NULL,
  PRIMARY KEY  (`alias_id`),
  UNIQUE KEY `UNIQ_mockup_alias` (`alias_id`,`mockup_id`),
  KEY `INDEX_mockup_alias_1` (`mockup_id`),
  KEY `INDEX_mockup_alias_2` (`mockup_number`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_doccomments`
--

CREATE TABLE `mockup_doccomments` (
  `comment_id` INT NOT NULL ,
  `document_id` INT NOT NULL ,
  `comment` TEXT NOT NULL ,
  `open_by` INT NOT NULL ,
  PRIMARY KEY  (`comment_id`),
  KEY `INDEX_mockup_doccomments_1` (`document_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_documents`
--
CREATE TABLE `mockup_documents` (
  `document_id` int(11) NOT NULL,
  `document_bid` CHAR(15) NOT NULL,
  `document_number` varchar(128)  NOT NULL,
  `document_name` varchar(256)  NOT NULL,
  `document_state` varchar(32)  NOT NULL default 'init',
  `document_access_code` int(11) NOT NULL default 0,
  `document_iteration` int(11) NOT NULL default 1,
  `document_version` int(11) NOT NULL default 1,
  `document_life_stage` int(1) NOT NULL default 1,
  `from_document` int(11) default NULL,
  `mockup_id` int(11) NOT NULL,
  `default_process_id` int(11) default NULL,
  `instance_id` int(11) default NULL,
  `doctype_id` int(11) NOT NULL,
  `category_id` int(11) default NULL,
  `check_out_by` int(11) default NULL,
  `check_out_date` int(11) default NULL,
  `description` varchar(128)  default NULL,
  `update_date` int(11) default NULL,
  `update_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `owned_by` int(11) default NULL,
  PRIMARY KEY  (`document_id`),
  UNIQUE KEY `UNIQ_mockup_documents_1` (`document_number`,`document_version`, `document_iteration`),
  UNIQUE KEY `UNIQ_mockup_documents_2` (`document_bid`,`document_version`, `document_iteration`),
  KEY `INDEX_mockup_documents_1` (`document_number`),
  KEY `INDEX_mockup_documents_2` (`document_version`),
  KEY `INDEX_mockup_documents_3` (`document_iteration`),
  KEY `INDEX_mockup_documents_4` (`doctype_id`),
  KEY `INDEX_mockup_documents_5` (`mockup_id`),
  KEY `INDEX_mockup_documents_6` (`category_id`),
  KEY `INDEX_mockup_documents_7` (`document_state`),
  KEY `INDEX_mockup_documents_8` (`description`),
  KEY `INDEX_mockup_documents_9` (`from_document`),
  KEY `INDEX_mockup_documents_10` (`check_out_by`),
  KEY `INDEX_mockup_documents_11` (`update_by`),
  KEY `INDEX_mockup_documents_12` (`open_by`),
  KEY `INDEX_mockup_documents_13` (`document_bid`),
  KEY `INDEX_mockup_documents_14` (`document_name`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_doc_files`
--
CREATE TABLE `mockup_doc_files` (
  `file_id` int(11) NOT NULL,
  `file_bid` CHAR(15) NOT NULL,
  `document_id` int(11) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `reposit_id` int(11) NOT NULL,
  `file_version` int(11) NOT NULL default 1,
  `file_iteration` int(11) NOT NULL default 1,
  `file_access_code` int(11) NOT NULL default 0,
  `file_mode` int(1) NOT NULL default 0,
  `file_life_stage` int(1) NOT NULL default 1,
  `from_file` int(11) default NULL,
  `file_root_name` varchar(128) NOT NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) NOT NULL default 'init',
  `file_type` varchar(128) NOT NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_checkout_by` int(11) default NULL,
  `file_checkout_date` int(11) default NULL,
  `file_open_date` int(11) NOT NULL,
  `file_open_by` int(11) NOT NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `UNIQ_mockup_doc_files_2` (`file_bid`, `file_version`, `file_iteration`),
  KEY `INDEX_mockup_doc_files_1` (`document_id`),
  KEY `INDEX_mockup_doc_files_2` (`file_name`),
  KEY `INDEX_mockup_doc_files_3` (`file_root_name`),
  KEY `INDEX_mockup_doc_files_4` (`file_extension`),
  KEY `INDEX_mockup_doc_files_5` (`file_type`),
  KEY `INDEX_mockup_doc_files_6` (`file_md5`),
  KEY `INDEX_mockup_doc_files_7` (`file_checkout_by`),
  KEY `INDEX_mockup_doc_files_8` (`file_name`, `file_iteration`),
  KEY `INDEX_mockup_doc_files_9` (`reposit_id`)
) ENGINE=InnoDB;

CREATE TABLE `mockup_doc_file_role` (
  `link_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `role_id` int(1) NOT NULL,
  PRIMARY KEY  (`link_id`),
  UNIQUE KEY `mockup_doc_file_role_uniq1` (`file_id`,`role_id`)
) ENGINE=InnoDB;

CREATE TABLE `mockup_files` (
  `file_id` int(11) NOT NULL,
  `file_bid` CHAR(15) NOT NULL,
  `mockup_id` int(11) NOT NULL default '0',
  `import_order` int(11) default NULL,
  `file_name` varchar(128)  NOT NULL,
  `reposit_id` int(11) NOT NULL,
  `file_version` int(11) NOT NULL default 1,
  `file_iteration` int(11) NOT NULL default 1,
  `file_access_code` int(11) NOT NULL default 0,
  `file_life_stage` int(1) NOT NULL default 1,
  `file_root_name` varchar(128) NOT NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) NOT NULL default 'init',
  `file_type` varchar(128) NOT NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_open_date` int(11) NOT NULL,
  `file_open_by` int(11) NOT NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  `from_file` int(11) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `UNIQ_mockup_files_2` (`file_bid`, `file_version`, `file_iteration`),
  KEY `INDEX_mockup_files_1` (`mockup_id`),
  KEY `INDEX_mockup_files_2` (`file_name`),
  KEY `INDEX_mockup_files_3` (`file_root_name`),
  KEY `INDEX_mockup_files_4` (`file_extension`),
  KEY `INDEX_mockup_files_5` (`file_type`),
  KEY `INDEX_mockup_files_6` (`file_md5`),
  KEY `INDEX_mockup_files_8` (`file_name`, `file_iteration`),
  KEY `INDEX_mockup_files_9` (`reposit_id`),
  KEY `INDEX_mockup_files_10` (`import_order`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_doc_rel`
--

CREATE TABLE `mockup_doc_rel` (
  `dr_link_id` int(11) NOT NULL,
  `dr_document_id` int(11) default NULL,
  `dr_l_document_id` int(11) default NULL,
  `dr_access_code` int(11) NOT NULL default '15',
  PRIMARY KEY  (`dr_link_id`),
  UNIQUE KEY `mockup_doc_rel_uniq1` (`dr_document_id`,`dr_l_document_id`),
  KEY `INDEX_mockup_doc_rel_1` (`dr_document_id`),
  KEY `INDEX_mockup_doc_rel_2` (`dr_l_document_id`)
) ENGINE=InnoDB;


-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE TABLE `mockup_cont_notifications` (
  `notification_id` INT NOT NULL ,
  `user_id` INT NOT NULL ,
  `mockup_id` INT NOT NULL ,
  `event` VARCHAR (64) NOT NULL ,
  `rb_condition` BLOB,
  PRIMARY KEY  (`notification_id`),
  KEY `INDEX_mockup_cont_notifications_1` (`user_id`),
  KEY `INDEX_mockup_cont_notifications_2` (`mockup_id`),
  KEY `INDEX_mockup_cont_notifications_3` (`event`),
  UNIQUE KEY `UNIQ_mockup_cont_notifications_1` (`user_id`,`mockup_id`,`event`)
) ENGINE=InnoDB;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE TABLE `mockup_documents_extends` (
  `extend_id` int(11) NOT NULL,
  PRIMARY KEY (`extend_id`)
) ENGINE=InnoDB;

CREATE TABLE `mockups_extends` (
  `extend_id` int(11) NOT NULL,
  PRIMARY KEY (`extend_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------
--
-- Structure de la table `mockups_history`
--
CREATE TABLE `mockups_history` (
  `histo_order` int(11) NOT NULL default '0',
  `action_name` varchar(32) default NULL,
  `action_by` varchar(32) default NULL,
  `action_date` int(11) default NULL,
  `mockup_id` int(11) NOT NULL default '0',
  `mockup_number` varchar(16) default NULL,
  `mockup_state` varchar(16) default NULL,
  `mockup_description` varchar(128) default NULL,
  `mockup_version` int(11) default NULL,
  `project_id` int(11) default NULL,
  `default_read_id` text  default NULL,
  `default_process_id` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_files_history`
--
CREATE TABLE `mockup_doc_files_history` (
  `histo_order` int(11) NOT NULL default 0,
  `action_name` varchar(32) NOT NULL default '',
  `action_by` varchar(64) NOT NULL default '',
  `action_date` int(11) NOT NULL default 0,
  `file_id` int(11) default NULL,
  `document_id` int(11) default NULL,
  `file_bid` CHAR(15) default NULL,
  `file_root_name` varchar(128) default NULL,
  `file_iteration` int(11) default NULL,
  `file_version` int(11) default NULL,
  `file_name` varchar(128) default NULL,
  `file_path` text default NULL,
  `file_access_code` int(11) default NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) default NULL,
  `file_type` varchar(128) default NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_open_date` int(11) default NULL,
  `file_open_by` int(11) default NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_documents_history`
--

CREATE TABLE `mockup_documents_history` (
  `histo_order` int(11) NOT NULL default 0,
  `action_name` varchar(32) NOT NULL default '',
  `action_by` varchar(64) NOT NULL default '',
  `action_date` int(11) NOT NULL default 0,
  `document_id` int(11) default NULL,
  `mockup_id` int(11) default NULL,
  `document_bid` CHAR(15)  default NULL,
  `document_number` varchar(128) default NULL,
  `document_state` varchar(32) default NULL,
  `document_access_code` int(11) default NULL,
  `document_iteration` int(11) default NULL,
  `document_version` int(11) default NULL,
  `description` varchar(128) default NULL,
  `document_life_stage` int(1) default NULL,
  `from_document` int(11) default NULL,
  `category_id` int(11) default NULL,
  `check_out_by` int(11) default NULL,
  `check_out_date` int(11) default NULL,
  `update_date` int(11) default NULL,
  `update_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `doctype_id` int(11) default NULL,
  `instance_id` int(11) default NULL,
  `activity_id` int(11) default NULL,
  `comment` varchar(255) default NULL,
  PRIMARY KEY  (`histo_order`),
  KEY `INDEX_mockup_documents_history_1` (`document_number`),
  KEY `INDEX_mockup_documents_history_2` (`document_version`),
  KEY `INDEX_mockup_documents_history_3` (`document_iteration`),
  KEY `INDEX_mockup_documents_history_5` (`mockup_id`),
  KEY `INDEX_mockup_documents_history_7` (`document_state`),
  KEY `INDEX_mockup_documents_history_8` (`description`),
  KEY `INDEX_mockup_documents_history_9` (`action_name`)
) ENGINE=InnoDB;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE ALGORITHM=TEMPTABLE VIEW `mockup_alias_view` AS
SELECT
	`mockups`.`mockup_id` AS `mockup_id`,
	`mockup_alias`.`alias_id` AS `alias_id`,
	`mockup_alias`.`mockup_number` AS `mockup_number`,
	`mockup_alias`.`mockup_description` AS `mockup_description`,
	`mockups`.`mockup_state` AS `mockup_state`,
	`mockups`.`file_only` AS `file_only`,
	`mockups`.`mockup_version` AS `mockup_version`,
	`mockups`.`project_id` AS `project_id`,
	`mockups`.`default_process_id` AS `default_process_id`,
	`mockups`.`default_read_id` AS `default_read_id`,
	`mockups`.`open_date` AS `open_date`,
	`mockups`.`open_by` AS `open_by`,
	`mockups`.`forseen_close_date` AS `forseen_close_date`,
	`mockups`.`close_date` AS `close_date`,
	`mockups`.`close_by` AS `close_by`
FROM `mockup_alias`
LEFT JOIN `mockups`
ON `mockup_alias`.`mockup_id` = `mockups`.`mockup_id`
UNION
SELECT
	`mockups`.`mockup_id` AS `mockup_id`,
	'' AS `alias_id`,
	`mockups`.`mockup_number` AS `mockup_number`,
	`mockups`.`mockup_description` AS `mockup_description`,
	`mockups`.`mockup_state` AS `mockup_state`,
	`mockups`.`file_only` AS `file_only`,
	`mockups`.`mockup_version` AS `mockup_version`,
	`mockups`.`project_id` AS `project_id`,
	`mockups`.`default_process_id` AS `default_process_id`,
	`mockups`.`default_read_id` AS `default_read_id`,
	`mockups`.`open_date` AS `open_date`,
	`mockups`.`open_by` AS `open_by`,
	`mockups`.`forseen_close_date` AS `forseen_close_date`,
	`mockups`.`close_date` AS `close_date`,
	`mockups`.`close_by` AS `close_by`
FROM `mockups`
LEFT JOIN `mockup_alias`
ON `mockup_alias`.`mockup_id` = `mockups`.`mockup_id`;

-- --------------------------------------------------------

--
-- Structure de la table `workitems`
--
CREATE TABLE `workitems` (
  `workitem_id` int(11) NOT NULL default '0',
  `workitem_number` varchar(128) NOT NULL,
  `workitem_name` varchar(128) default '',
  `workitem_state` varchar(32) NOT NULL default 'init',
  `workitem_description` varchar(512) default NULL,
  `file_only` tinyint(1) NOT NULL default '0',
  `workitem_version` int(11) default NULL,
  `workitem_iteration` int(11) default NULL,
  `project_id` int(11) default NULL,
  `default_process_id` int(11) default NULL,
  `default_read_id` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `owned_by` int(11) default NULL,
  PRIMARY KEY (`workitem_id`),
  UNIQUE KEY `UC_workitem_number` (`workitem_number`),
  KEY `INDEX_workitems_1` (`workitem_version`),
  KEY `INDEX_workitems_2` (`project_id`),
  KEY `INDEX_workitems_3` (`workitem_description`),
  KEY `INDEX_workitems_4` (`file_only`),
  KEY `INDEX_workitems_5` (`default_process_id`),
  KEY `INDEX_workitems_6` (`default_read_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_alias`
--

CREATE TABLE `workitem_alias` (
  `alias_id` int(11) NOT NULL,
  `workitem_id` int(11) NOT NULL,
  `workitem_number` varchar(16) NOT NULL,
  `workitem_description` varchar(128) default NULL,
  PRIMARY KEY  (`alias_id`),
  UNIQUE KEY `UNIQ_workitem_alias` (`alias_id`,`workitem_id`),
  KEY `INDEX_workitem_alias_1` (`workitem_id`),
  KEY `INDEX_workitem_alias_2` (`workitem_number`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_doccomments`
--

CREATE TABLE `workitem_doccomments` (
  `comment_id` INT NOT NULL ,
  `document_id` INT NOT NULL ,
  `comment` TEXT NOT NULL ,
  `open_by` INT NOT NULL ,
  PRIMARY KEY  (`comment_id`),
  KEY `INDEX_workitem_doccomments_1` (`document_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_documents`
--
CREATE TABLE `workitem_documents` (
  `document_id` int(11) NOT NULL,
  `document_bid` CHAR(15) NOT NULL,
  `document_number` varchar(128)  NOT NULL,
  `document_name` varchar(256)  NOT NULL,
  `document_state` varchar(32)  NOT NULL default 'init',
  `document_access_code` int(11) NOT NULL default 0,
  `document_iteration` int(11) NOT NULL default 1,
  `document_version` int(11) NOT NULL default 1,
  `document_life_stage` int(1) NOT NULL default 1,
  `from_document` int(11) default NULL,
  `workitem_id` int(11) NOT NULL,
  `default_process_id` int(11) default NULL,
  `instance_id` int(11) default NULL,
  `doctype_id` int(11) NOT NULL,
  `category_id` int(11) default NULL,
  `check_out_by` int(11) default NULL,
  `check_out_date` int(11) default NULL,
  `description` varchar(128)  default NULL,
  `update_date` int(11) default NULL,
  `update_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `owned_by` int(11) default NULL,
  PRIMARY KEY  (`document_id`),
  UNIQUE KEY `UNIQ_workitem_documents_1` (`document_number`,`document_version`, `document_iteration`),
  UNIQUE KEY `UNIQ_workitem_documents_2` (`document_bid`,`document_version`, `document_iteration`),
  KEY `INDEX_workitem_documents_1` (`document_number`),
  KEY `INDEX_workitem_documents_2` (`document_version`),
  KEY `INDEX_workitem_documents_3` (`document_iteration`),
  KEY `INDEX_workitem_documents_4` (`doctype_id`),
  KEY `INDEX_workitem_documents_5` (`workitem_id`),
  KEY `INDEX_workitem_documents_6` (`category_id`),
  KEY `INDEX_workitem_documents_7` (`document_state`),
  KEY `INDEX_workitem_documents_8` (`description`),
  KEY `INDEX_workitem_documents_9` (`from_document`),
  KEY `INDEX_workitem_documents_10` (`check_out_by`),
  KEY `INDEX_workitem_documents_11` (`update_by`),
  KEY `INDEX_workitem_documents_12` (`open_by`),
  KEY `INDEX_workitem_documents_13` (`document_bid`),
  KEY `INDEX_workitem_documents_14` (`document_name`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_doc_files`
--
CREATE TABLE `workitem_doc_files` (
  `file_id` int(11) NOT NULL,
  `file_bid` CHAR(15) NOT NULL,
  `document_id` int(11) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `reposit_id` int(11) NOT NULL,
  `file_version` int(11) NOT NULL default 1,
  `file_iteration` int(11) NOT NULL default 1,
  `file_access_code` int(11) NOT NULL default 0,
  `file_mode` int(1) NOT NULL default 0,
  `file_life_stage` int(1) NOT NULL default 1,
  `from_file` int(11) default NULL,
  `file_root_name` varchar(128) NOT NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) NOT NULL default 'init',
  `file_type` varchar(128) NOT NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_checkout_by` int(11) default NULL,
  `file_checkout_date` int(11) default NULL,
  `file_open_date` int(11) NOT NULL,
  `file_open_by` int(11) NOT NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `UNIQ_workitem_doc_files_2` (`file_bid`, `file_version`, `file_iteration`),
  KEY `INDEX_workitem_doc_files_1` (`document_id`),
  KEY `INDEX_workitem_doc_files_2` (`file_name`),
  KEY `INDEX_workitem_doc_files_3` (`file_root_name`),
  KEY `INDEX_workitem_doc_files_4` (`file_extension`),
  KEY `INDEX_workitem_doc_files_5` (`file_type`),
  KEY `INDEX_workitem_doc_files_6` (`file_md5`),
  KEY `INDEX_workitem_doc_files_7` (`file_checkout_by`),
  KEY `INDEX_workitem_doc_files_8` (`file_name`, `file_iteration`),
  KEY `INDEX_workitem_doc_files_9` (`reposit_id`)
) ENGINE=InnoDB;

CREATE TABLE `workitem_doc_file_role` (
  `link_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `role_id` int(1) NOT NULL,
  PRIMARY KEY  (`link_id`),
  UNIQUE KEY `workitem_doc_file_role_uniq1` (`file_id`,`role_id`)
) ENGINE=InnoDB;

CREATE TABLE `workitem_files` (
  `file_id` int(11) NOT NULL,
  `file_bid` CHAR(15) NOT NULL,
  `workitem_id` int(11) NOT NULL default '0',
  `import_order` int(11) default NULL,
  `file_name` varchar(128)  NOT NULL,
  `reposit_id` int(11) NOT NULL,
  `file_version` int(11) NOT NULL default 1,
  `file_iteration` int(11) NOT NULL default 1,
  `file_access_code` int(11) NOT NULL default 0,
  `file_life_stage` int(1) NOT NULL default 1,
  `file_root_name` varchar(128) NOT NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) NOT NULL default 'init',
  `file_type` varchar(128) NOT NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_open_date` int(11) NOT NULL,
  `file_open_by` int(11) NOT NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  `from_file` int(11) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `UNIQ_workitem_files_2` (`file_bid`, `file_version`, `file_iteration`),
  KEY `INDEX_workitem_files_1` (`workitem_id`),
  KEY `INDEX_workitem_files_2` (`file_name`),
  KEY `INDEX_workitem_files_3` (`file_root_name`),
  KEY `INDEX_workitem_files_4` (`file_extension`),
  KEY `INDEX_workitem_files_5` (`file_type`),
  KEY `INDEX_workitem_files_6` (`file_md5`),
  KEY `INDEX_workitem_files_8` (`file_name`, `file_iteration`),
  KEY `INDEX_workitem_files_9` (`reposit_id`),
  KEY `INDEX_workitem_files_10` (`import_order`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_doc_rel`
--

CREATE TABLE `workitem_doc_rel` (
  `dr_link_id` int(11) NOT NULL,
  `dr_document_id` int(11) default NULL,
  `dr_l_document_id` int(11) default NULL,
  `dr_access_code` int(11) NOT NULL default '15',
  PRIMARY KEY  (`dr_link_id`),
  UNIQUE KEY `workitem_doc_rel_uniq1` (`dr_document_id`,`dr_l_document_id`),
  KEY `INDEX_workitem_doc_rel_1` (`dr_document_id`),
  KEY `INDEX_workitem_doc_rel_2` (`dr_l_document_id`)
) ENGINE=InnoDB;


-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE TABLE `workitem_cont_notifications` (
  `notification_id` INT NOT NULL ,
  `user_id` INT NOT NULL ,
  `workitem_id` INT NOT NULL ,
  `event` VARCHAR (64) NOT NULL ,
  `rb_condition` BLOB,
  PRIMARY KEY  (`notification_id`),
  KEY `INDEX_workitem_cont_notifications_1` (`user_id`),
  KEY `INDEX_workitem_cont_notifications_2` (`workitem_id`),
  KEY `INDEX_workitem_cont_notifications_3` (`event`),
  UNIQUE KEY `UNIQ_workitem_cont_notifications_1` (`user_id`,`workitem_id`,`event`)
) ENGINE=InnoDB;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE TABLE `workitem_documents_extends` (
  `extend_id` int(11) NOT NULL,
  PRIMARY KEY (`extend_id`)
) ENGINE=InnoDB;

CREATE TABLE `workitems_extends` (
  `extend_id` int(11) NOT NULL,
  PRIMARY KEY (`extend_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------
--
-- Structure de la table `workitems_history`
--
CREATE TABLE `workitems_history` (
  `histo_order` int(11) NOT NULL default '0',
  `action_name` varchar(32) default NULL,
  `action_by` varchar(32) default NULL,
  `action_date` int(11) default NULL,
  `workitem_id` int(11) NOT NULL default '0',
  `workitem_number` varchar(16) default NULL,
  `workitem_state` varchar(16) default NULL,
  `workitem_description` varchar(128) default NULL,
  `workitem_version` int(11) default NULL,
  `project_id` int(11) default NULL,
  `default_read_id` text  default NULL,
  `default_process_id` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_files_history`
--
CREATE TABLE `workitem_doc_files_history` (
  `histo_order` int(11) NOT NULL default 0,
  `action_name` varchar(32) NOT NULL default '',
  `action_by` varchar(64) NOT NULL default '',
  `action_date` int(11) NOT NULL default 0,
  `file_id` int(11) default NULL,
  `document_id` int(11) default NULL,
  `file_bid` CHAR(15) default NULL,
  `file_root_name` varchar(128) default NULL,
  `file_iteration` int(11) default NULL,
  `file_version` int(11) default NULL,
  `file_name` varchar(128) default NULL,
  `file_path` text default NULL,
  `file_access_code` int(11) default NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) default NULL,
  `file_type` varchar(128) default NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_open_date` int(11) default NULL,
  `file_open_by` int(11) default NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_documents_history`
--

CREATE TABLE `workitem_documents_history` (
  `histo_order` int(11) NOT NULL default 0,
  `action_name` varchar(32) NOT NULL default '',
  `action_by` varchar(64) NOT NULL default '',
  `action_date` int(11) NOT NULL default 0,
  `document_id` int(11) default NULL,
  `workitem_id` int(11) default NULL,
  `document_bid` CHAR(15)  default NULL,
  `document_number` varchar(128) default NULL,
  `document_state` varchar(32) default NULL,
  `document_access_code` int(11) default NULL,
  `document_iteration` int(11) default NULL,
  `document_version` int(11) default NULL,
  `description` varchar(128) default NULL,
  `document_life_stage` int(1) default NULL,
  `from_document` int(11) default NULL,
  `category_id` int(11) default NULL,
  `check_out_by` int(11) default NULL,
  `check_out_date` int(11) default NULL,
  `update_date` int(11) default NULL,
  `update_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `doctype_id` int(11) default NULL,
  `instance_id` int(11) default NULL,
  `activity_id` int(11) default NULL,
  `comment` varchar(255) default NULL,
  PRIMARY KEY  (`histo_order`),
  KEY `INDEX_workitem_documents_history_1` (`document_number`),
  KEY `INDEX_workitem_documents_history_2` (`document_version`),
  KEY `INDEX_workitem_documents_history_3` (`document_iteration`),
  KEY `INDEX_workitem_documents_history_5` (`workitem_id`),
  KEY `INDEX_workitem_documents_history_7` (`document_state`),
  KEY `INDEX_workitem_documents_history_8` (`description`),
  KEY `INDEX_workitem_documents_history_9` (`action_name`)
) ENGINE=InnoDB;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE ALGORITHM=TEMPTABLE VIEW `workitem_alias_view` AS
SELECT
	`workitems`.`workitem_id` AS `workitem_id`,
	`workitem_alias`.`alias_id` AS `alias_id`,
	`workitem_alias`.`workitem_number` AS `workitem_number`,
	`workitem_alias`.`workitem_description` AS `workitem_description`,
	`workitems`.`workitem_state` AS `workitem_state`,
	`workitems`.`file_only` AS `file_only`,
	`workitems`.`workitem_version` AS `workitem_version`,
	`workitems`.`project_id` AS `project_id`,
	`workitems`.`default_process_id` AS `default_process_id`,
	`workitems`.`default_read_id` AS `default_read_id`,
	`workitems`.`open_date` AS `open_date`,
	`workitems`.`open_by` AS `open_by`,
	`workitems`.`forseen_close_date` AS `forseen_close_date`,
	`workitems`.`close_date` AS `close_date`,
	`workitems`.`close_by` AS `close_by`
FROM `workitem_alias`
LEFT JOIN `workitems`
ON `workitem_alias`.`workitem_id` = `workitems`.`workitem_id`
UNION
SELECT
	`workitems`.`workitem_id` AS `workitem_id`,
	'' AS `alias_id`,
	`workitems`.`workitem_number` AS `workitem_number`,
	`workitems`.`workitem_description` AS `workitem_description`,
	`workitems`.`workitem_state` AS `workitem_state`,
	`workitems`.`file_only` AS `file_only`,
	`workitems`.`workitem_version` AS `workitem_version`,
	`workitems`.`project_id` AS `project_id`,
	`workitems`.`default_process_id` AS `default_process_id`,
	`workitems`.`default_read_id` AS `default_read_id`,
	`workitems`.`open_date` AS `open_date`,
	`workitems`.`open_by` AS `open_by`,
	`workitems`.`forseen_close_date` AS `forseen_close_date`,
	`workitems`.`close_date` AS `close_date`,
	`workitems`.`close_by` AS `close_by`
FROM `workitems`
LEFT JOIN `workitem_alias`
ON `workitem_alias`.`workitem_id` = `workitems`.`workitem_id`;


--
-- Contraintes pour la table `bookshops`
--
ALTER TABLE `bookshops`
  ADD CONSTRAINT `FK_bookshops_1` FOREIGN KEY (`bookshop_version`)
  REFERENCES `indices` (`indice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshops_2` FOREIGN KEY (`project_id`)
  REFERENCES `projects` (`project_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshops_3` FOREIGN KEY (`default_process_id`)
  REFERENCES `galaxia_processes` (`pId`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshops_4` FOREIGN KEY (`bookshop_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshops_5` FOREIGN KEY (`default_read_id`)
  REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `bookshop_alias`
--
ALTER TABLE `bookshop_alias`
  ADD CONSTRAINT `FK_bookshop_alias_1` FOREIGN KEY (`bookshop_id`)
  REFERENCES `bookshops` (`bookshop_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `bookshop_documents`
--

ALTER TABLE `bookshop_documents`
  ADD CONSTRAINT `FK_bookshop_documents_2` FOREIGN KEY (`doctype_id`) 
  REFERENCES `doctypes` (`doctype_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshop_documents_3` FOREIGN KEY (`document_version`) 
  REFERENCES `indices` (`indice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshop_documents_4` FOREIGN KEY (`bookshop_id`) 
  REFERENCES `bookshops` (`bookshop_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshop_documents_5` FOREIGN KEY (`category_id`) 
  REFERENCES `categories` (`category_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshop_documents_6` FOREIGN KEY (`instance_id`) 
  REFERENCES `galaxia_instances` (`instanceId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshop_documents_7` FOREIGN KEY (`default_process_id`) 
  REFERENCES `galaxia_processes` (`pId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshop_documents_8` FOREIGN KEY (`open_by`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshop_documents_9` FOREIGN KEY (`update_by`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshop_documents_10` FOREIGN KEY (`owned_by`)
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshop_documents_11` FOREIGN KEY (`document_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `bookshop_doccomments`
  	ADD CONSTRAINT `FK_bookshop_doccomments_1` FOREIGN KEY (`document_id`)
  	REFERENCES `bookshop_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_doccomments_2` FOREIGN KEY (`open_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;


--
-- Contraintes pour la table `bookshop_doc_files`
--
ALTER TABLE `bookshop_doc_files` 
  	ADD CONSTRAINT `FK_bookshop_doc_files_1` FOREIGN KEY (`document_id`) 
  	REFERENCES `bookshop_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  	ADD CONSTRAINT `FK_bookshop_doc_files_2` FOREIGN KEY (`file_id`) 
  	REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  	ADD CONSTRAINT `FK_bookshop_doc_files_3` FOREIGN KEY (`reposit_id`) 
  	REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_doc_files_4` FOREIGN KEY (`file_open_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_doc_files_5` FOREIGN KEY (`file_update_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_doc_files_6` FOREIGN KEY (`file_checkout_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `bookshop_files`
--
ALTER TABLE `bookshop_files` 
	ADD CONSTRAINT `FK_bookshop_files_1` FOREIGN KEY (`bookshop_id`) 
	REFERENCES `bookshops` (`bookshop_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_files_2` FOREIGN KEY (`file_id`) 
	REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_files_3` FOREIGN KEY (`reposit_id`) 
	REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_files_4` FOREIGN KEY (`import_order`) 
	REFERENCES `import_history` (`import_order`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_files_5` FOREIGN KEY (`file_open_by`) 
	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_files_6` FOREIGN KEY (`file_update_by`) 
	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `bookshop_doc_rel`
--
ALTER TABLE `bookshop_doc_rel`
  ADD CONSTRAINT `FK_bookshop_doc_rel_1` FOREIGN KEY (`dr_document_id`) 
  REFERENCES `bookshop_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshop_doc_rel_2` FOREIGN KEY (`dr_l_document_id`) 
  REFERENCES `bookshop_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `bookshop_cont_notifications`
  ADD CONSTRAINT `FK_bookshop_cont_notifications_1` FOREIGN KEY (`bookshop_id`)
  REFERENCES `bookshops` (`bookshop_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bookshop_cont_notifications_2` FOREIGN KEY (`user_id`)
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `bookshop_doc_file_role`
  ADD CONSTRAINT `FK_bookshop_doc_file_role_1` FOREIGN KEY (`file_id`)
  REFERENCES `bookshop_doc_files` (`file_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `bookshop_documents_extends`
--
ALTER TABLE `bookshop_documents_extends` 
  ADD CONSTRAINT `FK_bookshop_documents_extends_1` FOREIGN KEY (`extend_id`) 
  REFERENCES `bookshop_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `bookshops_extends`
--
ALTER TABLE `bookshops_extends` 
  ADD CONSTRAINT `FK_bookshops_extends_1` FOREIGN KEY (`extend_id`) 
  REFERENCES `bookshops` (`bookshop_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `cadlibs`
--
ALTER TABLE `cadlibs`
  ADD CONSTRAINT `FK_cadlibs_1` FOREIGN KEY (`cadlib_version`)
  REFERENCES `indices` (`indice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlibs_2` FOREIGN KEY (`project_id`)
  REFERENCES `projects` (`project_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlibs_3` FOREIGN KEY (`default_process_id`)
  REFERENCES `galaxia_processes` (`pId`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlibs_4` FOREIGN KEY (`cadlib_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlibs_5` FOREIGN KEY (`default_read_id`)
  REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `cadlib_alias`
--
ALTER TABLE `cadlib_alias`
  ADD CONSTRAINT `FK_cadlib_alias_1` FOREIGN KEY (`cadlib_id`)
  REFERENCES `cadlibs` (`cadlib_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `cadlib_documents`
--

ALTER TABLE `cadlib_documents`
  ADD CONSTRAINT `FK_cadlib_documents_2` FOREIGN KEY (`doctype_id`) 
  REFERENCES `doctypes` (`doctype_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlib_documents_3` FOREIGN KEY (`document_version`) 
  REFERENCES `indices` (`indice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlib_documents_4` FOREIGN KEY (`cadlib_id`) 
  REFERENCES `cadlibs` (`cadlib_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlib_documents_5` FOREIGN KEY (`category_id`) 
  REFERENCES `categories` (`category_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlib_documents_6` FOREIGN KEY (`instance_id`) 
  REFERENCES `galaxia_instances` (`instanceId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlib_documents_7` FOREIGN KEY (`default_process_id`) 
  REFERENCES `galaxia_processes` (`pId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlib_documents_8` FOREIGN KEY (`open_by`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlib_documents_9` FOREIGN KEY (`update_by`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlib_documents_10` FOREIGN KEY (`owned_by`)
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlib_documents_11` FOREIGN KEY (`document_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cadlib_doccomments`
  	ADD CONSTRAINT `FK_cadlib_doccomments_1` FOREIGN KEY (`document_id`)
  	REFERENCES `cadlib_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_doccomments_2` FOREIGN KEY (`open_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;


--
-- Contraintes pour la table `cadlib_doc_files`
--
ALTER TABLE `cadlib_doc_files` 
  	ADD CONSTRAINT `FK_cadlib_doc_files_1` FOREIGN KEY (`document_id`) 
  	REFERENCES `cadlib_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  	ADD CONSTRAINT `FK_cadlib_doc_files_2` FOREIGN KEY (`file_id`) 
  	REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  	ADD CONSTRAINT `FK_cadlib_doc_files_3` FOREIGN KEY (`reposit_id`) 
  	REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_doc_files_4` FOREIGN KEY (`file_open_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_doc_files_5` FOREIGN KEY (`file_update_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_doc_files_6` FOREIGN KEY (`file_checkout_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `cadlib_files`
--
ALTER TABLE `cadlib_files` 
	ADD CONSTRAINT `FK_cadlib_files_1` FOREIGN KEY (`cadlib_id`) 
	REFERENCES `cadlibs` (`cadlib_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_files_2` FOREIGN KEY (`file_id`) 
	REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_files_3` FOREIGN KEY (`reposit_id`) 
	REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_files_4` FOREIGN KEY (`import_order`) 
	REFERENCES `import_history` (`import_order`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_files_5` FOREIGN KEY (`file_open_by`) 
	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_files_6` FOREIGN KEY (`file_update_by`) 
	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `cadlib_doc_rel`
--
ALTER TABLE `cadlib_doc_rel`
  ADD CONSTRAINT `FK_cadlib_doc_rel_1` FOREIGN KEY (`dr_document_id`) 
  REFERENCES `cadlib_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlib_doc_rel_2` FOREIGN KEY (`dr_l_document_id`) 
  REFERENCES `cadlib_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `cadlib_cont_notifications`
  ADD CONSTRAINT `FK_cadlib_cont_notifications_1` FOREIGN KEY (`cadlib_id`)
  REFERENCES `cadlibs` (`cadlib_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_cadlib_cont_notifications_2` FOREIGN KEY (`user_id`)
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `cadlib_doc_file_role`
  ADD CONSTRAINT `FK_cadlib_doc_file_role_1` FOREIGN KEY (`file_id`)
  REFERENCES `cadlib_doc_files` (`file_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `cadlib_documents_extends`
--
ALTER TABLE `cadlib_documents_extends` 
  ADD CONSTRAINT `FK_cadlib_documents_extends_1` FOREIGN KEY (`extend_id`) 
  REFERENCES `cadlib_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `cadlibs_extends`
--
ALTER TABLE `cadlibs_extends` 
  ADD CONSTRAINT `FK_cadlibs_extends_1` FOREIGN KEY (`extend_id`) 
  REFERENCES `cadlibs` (`cadlib_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `mockups`
--
ALTER TABLE `mockups`
  ADD CONSTRAINT `FK_mockups_1` FOREIGN KEY (`mockup_version`)
  REFERENCES `indices` (`indice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockups_2` FOREIGN KEY (`project_id`)
  REFERENCES `projects` (`project_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockups_3` FOREIGN KEY (`default_process_id`)
  REFERENCES `galaxia_processes` (`pId`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockups_4` FOREIGN KEY (`mockup_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockups_5` FOREIGN KEY (`default_read_id`)
  REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `mockup_alias`
--
ALTER TABLE `mockup_alias`
  ADD CONSTRAINT `FK_mockup_alias_1` FOREIGN KEY (`mockup_id`)
  REFERENCES `mockups` (`mockup_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `mockup_documents`
--

ALTER TABLE `mockup_documents`
  ADD CONSTRAINT `FK_mockup_documents_2` FOREIGN KEY (`doctype_id`) 
  REFERENCES `doctypes` (`doctype_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockup_documents_3` FOREIGN KEY (`document_version`) 
  REFERENCES `indices` (`indice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockup_documents_4` FOREIGN KEY (`mockup_id`) 
  REFERENCES `mockups` (`mockup_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockup_documents_5` FOREIGN KEY (`category_id`) 
  REFERENCES `categories` (`category_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockup_documents_6` FOREIGN KEY (`instance_id`) 
  REFERENCES `galaxia_instances` (`instanceId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockup_documents_7` FOREIGN KEY (`default_process_id`) 
  REFERENCES `galaxia_processes` (`pId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockup_documents_8` FOREIGN KEY (`open_by`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockup_documents_9` FOREIGN KEY (`update_by`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockup_documents_10` FOREIGN KEY (`owned_by`)
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockup_documents_11` FOREIGN KEY (`document_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `mockup_doccomments`
  	ADD CONSTRAINT `FK_mockup_doccomments_1` FOREIGN KEY (`document_id`)
  	REFERENCES `mockup_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_doccomments_2` FOREIGN KEY (`open_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;


--
-- Contraintes pour la table `mockup_doc_files`
--
ALTER TABLE `mockup_doc_files` 
  	ADD CONSTRAINT `FK_mockup_doc_files_1` FOREIGN KEY (`document_id`) 
  	REFERENCES `mockup_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  	ADD CONSTRAINT `FK_mockup_doc_files_2` FOREIGN KEY (`file_id`) 
  	REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  	ADD CONSTRAINT `FK_mockup_doc_files_3` FOREIGN KEY (`reposit_id`) 
  	REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_doc_files_4` FOREIGN KEY (`file_open_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_doc_files_5` FOREIGN KEY (`file_update_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_doc_files_6` FOREIGN KEY (`file_checkout_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `mockup_files`
--
ALTER TABLE `mockup_files` 
	ADD CONSTRAINT `FK_mockup_files_1` FOREIGN KEY (`mockup_id`) 
	REFERENCES `mockups` (`mockup_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_files_2` FOREIGN KEY (`file_id`) 
	REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_files_3` FOREIGN KEY (`reposit_id`) 
	REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_files_4` FOREIGN KEY (`import_order`) 
	REFERENCES `import_history` (`import_order`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_files_5` FOREIGN KEY (`file_open_by`) 
	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_files_6` FOREIGN KEY (`file_update_by`) 
	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `mockup_doc_rel`
--
ALTER TABLE `mockup_doc_rel`
  ADD CONSTRAINT `FK_mockup_doc_rel_1` FOREIGN KEY (`dr_document_id`) 
  REFERENCES `mockup_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockup_doc_rel_2` FOREIGN KEY (`dr_l_document_id`) 
  REFERENCES `mockup_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `mockup_cont_notifications`
  ADD CONSTRAINT `FK_mockup_cont_notifications_1` FOREIGN KEY (`mockup_id`)
  REFERENCES `mockups` (`mockup_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_mockup_cont_notifications_2` FOREIGN KEY (`user_id`)
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `mockup_doc_file_role`
  ADD CONSTRAINT `FK_mockup_doc_file_role_1` FOREIGN KEY (`file_id`)
  REFERENCES `mockup_doc_files` (`file_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `mockup_documents_extends`
--
ALTER TABLE `mockup_documents_extends` 
  ADD CONSTRAINT `FK_mockup_documents_extends_1` FOREIGN KEY (`extend_id`) 
  REFERENCES `mockup_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `mockups_extends`
--
ALTER TABLE `mockups_extends` 
  ADD CONSTRAINT `FK_mockups_extends_1` FOREIGN KEY (`extend_id`) 
  REFERENCES `mockups` (`mockup_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `workitems`
--
ALTER TABLE `workitems`
  ADD CONSTRAINT `FK_workitems_1` FOREIGN KEY (`workitem_version`)
  REFERENCES `indices` (`indice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitems_2` FOREIGN KEY (`project_id`)
  REFERENCES `projects` (`project_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitems_3` FOREIGN KEY (`default_process_id`)
  REFERENCES `galaxia_processes` (`pId`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitems_4` FOREIGN KEY (`workitem_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitems_5` FOREIGN KEY (`default_read_id`)
  REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `workitem_alias`
--
ALTER TABLE `workitem_alias`
  ADD CONSTRAINT `FK_workitem_alias_1` FOREIGN KEY (`workitem_id`)
  REFERENCES `workitems` (`workitem_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `workitem_documents`
--

ALTER TABLE `workitem_documents`
  ADD CONSTRAINT `FK_workitem_documents_2` FOREIGN KEY (`doctype_id`) 
  REFERENCES `doctypes` (`doctype_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitem_documents_3` FOREIGN KEY (`document_version`) 
  REFERENCES `indices` (`indice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitem_documents_4` FOREIGN KEY (`workitem_id`) 
  REFERENCES `workitems` (`workitem_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitem_documents_5` FOREIGN KEY (`category_id`) 
  REFERENCES `categories` (`category_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitem_documents_6` FOREIGN KEY (`instance_id`) 
  REFERENCES `galaxia_instances` (`instanceId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitem_documents_7` FOREIGN KEY (`default_process_id`) 
  REFERENCES `galaxia_processes` (`pId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitem_documents_8` FOREIGN KEY (`open_by`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitem_documents_9` FOREIGN KEY (`update_by`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitem_documents_10` FOREIGN KEY (`owned_by`)
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitem_documents_11` FOREIGN KEY (`document_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `workitem_doccomments`
  	ADD CONSTRAINT `FK_workitem_doccomments_1` FOREIGN KEY (`document_id`)
  	REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_doccomments_2` FOREIGN KEY (`open_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;


--
-- Contraintes pour la table `workitem_doc_files`
--
ALTER TABLE `workitem_doc_files` 
  	ADD CONSTRAINT `FK_workitem_doc_files_1` FOREIGN KEY (`document_id`) 
  	REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  	ADD CONSTRAINT `FK_workitem_doc_files_2` FOREIGN KEY (`file_id`) 
  	REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  	ADD CONSTRAINT `FK_workitem_doc_files_3` FOREIGN KEY (`reposit_id`) 
  	REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_doc_files_4` FOREIGN KEY (`file_open_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_doc_files_5` FOREIGN KEY (`file_update_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_doc_files_6` FOREIGN KEY (`file_checkout_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `workitem_files`
--
ALTER TABLE `workitem_files` 
	ADD CONSTRAINT `FK_workitem_files_1` FOREIGN KEY (`workitem_id`) 
	REFERENCES `workitems` (`workitem_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_files_2` FOREIGN KEY (`file_id`) 
	REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_files_3` FOREIGN KEY (`reposit_id`) 
	REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_files_4` FOREIGN KEY (`import_order`) 
	REFERENCES `import_history` (`import_order`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_files_5` FOREIGN KEY (`file_open_by`) 
	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_files_6` FOREIGN KEY (`file_update_by`) 
	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `workitem_doc_rel`
--
ALTER TABLE `workitem_doc_rel`
  ADD CONSTRAINT `FK_workitem_doc_rel_1` FOREIGN KEY (`dr_document_id`) 
  REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitem_doc_rel_2` FOREIGN KEY (`dr_l_document_id`) 
  REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `workitem_cont_notifications`
  ADD CONSTRAINT `FK_workitem_cont_notifications_1` FOREIGN KEY (`workitem_id`)
  REFERENCES `workitems` (`workitem_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_workitem_cont_notifications_2` FOREIGN KEY (`user_id`)
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `workitem_doc_file_role`
  ADD CONSTRAINT `FK_workitem_doc_file_role_1` FOREIGN KEY (`file_id`)
  REFERENCES `workitem_doc_files` (`file_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `workitem_documents_extends`
--
ALTER TABLE `workitem_documents_extends` 
  ADD CONSTRAINT `FK_workitem_documents_extends_1` FOREIGN KEY (`extend_id`) 
  REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `workitems_extends`
--
ALTER TABLE `workitems_extends` 
  ADD CONSTRAINT `FK_workitems_extends_1` FOREIGN KEY (`extend_id`) 
  REFERENCES `workitems` (`workitem_id`) ON DELETE CASCADE ON UPDATE CASCADE;


-- --------------------------DATAS-------------------------
INSERT INTO `db_version` (`version`, `compiled`) VALUES (15,1292762522);
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES
(30,110,0),
(31,110,0),
(32,110,0),
(33,110,0),
(34,110,0),
(35,110,0),
(36,110,0),
(37,110,0),
(38,110,0),
(39,110,0),
(40,110,0),
(41,110,0),
(42,110,0),
(43,110,0),
(44,110,0),
(45,110,0),
(46,110,0),
(47,110,0),
(48,110,0),
(49,110,0),
(50,110,0),
(51,110,0),
(52,110,0),
(53,110,0),
(54,110,0),
(55,110,0),
(56,110,0),
(57,110,0),
(58,110,0),
(59,110,0),
(60,110,0),
(61,110,0),
(62,110,0),
(63,110,0),
(64,110,0),
(65,110,0),
(66,110,0),
(67,110,0),
(68,110,0),
(69,110,0),
(70,110,0),
(71,110,0),
(72,110,0),
(73,110,0),
(74,110,0),
(75,110,0),
(76,110,0),
(77,110,0),
(78,110,0),
(79,110,0),
(80,110,0),
(81,110,0),
(82,110,0),
(83,110,0),
(84,110,0),
(85,110,0),
(86,110,0),
(87,110,0),
(88,110,0),
(89,110,0),
(90,110,0),
(91,110,0),
(92,110,0),
(93,110,0),
(94,110,0),
(95,110,0),
(96,110,0),
(97,110,0),
(98,110,0),
(99,110,0);
-- ------------------------------------------------------ 
-- 
INSERT INTO `rb_groups` (`group_id`, `group_define_name`, `group_description`, `is_active`) VALUES
(21, 'admins', 'Administrators', 1),
(23, 'adv_users', 'Advanced users', 1),
(24, 'users', 'Users', 1);

-- ------------------------------------------------------ 
INSERT INTO `doctypes` (`doctype_id`, `doctype_number`, `doctype_description`, `can_be_composite`, `script_post_store`, `script_pre_store`, `script_post_update`, `script_pre_update`, `recognition_regexp`, `file_extension`, `file_type`, `icon`) VALUES
(30, 'cao__drafting', 'MATRA Prelude drafting','0','','','','','', '.drw', 'file', ''),
(31, 'cao__dxf', 'AutoCAD','0','','','','','', '.dxf', 'file', 'dwf.gif'),
(32, 'cao__i-deas', 'SDRC I-deas','0','','','','','', '.unv', 'file', ''),
(33, 'cao__iges', 'Echange IGES','0','','','','','', '.igs', 'file', ''),
(34, 'cao__pro_eng', 'ProEngineer','0','','','','','', '.prt', 'file', ''),
(35, 'cao__set', 'CAO set','0','','','','','', '.set', 'file', ''),
(36, 'cao__sla', 'stereolithographie','0','','','','','', '.stl', 'file', ''),
(37, 'cao__step', 'Datas STEP','0','','','','','', '.step', 'file', 'stp.gif'),
(38, 'cao__vda', 'Surface','0','','','','','', '.vda', 'file', ''),
(39, 'text__pdf', 'Adobe Acrobat','0','','','','','', '.pdf', 'file', 'pdf.gif'),
(40, 'text__postscript', 'PostScript','0','','','','','', '.ps', 'file', 'ps.gif'),
(41, 'text__epostscript', 'Encapsulated postScript','0','','','','','', '.eps', 'file', 'eps.gif'),
(42, 'text__rtf', 'Rich text','0','','','','','', '.rtf', 'file', 'rtf.gif'),
(43, 'text__htm', 'HTML','0','','','','','', '.htm', 'file', 'html.gif'),
(44, 'text__html', 'HTML','0','','','','','', '.html', 'file', 'htm.gif'),
(45, 'text__plain', 'Plain text','0','','','','','', '.txt', 'file', 'txt.gif'),
(46, 'text__tsv', 'Tabulated separated','0','','','','','', '.tsv', 'file', 'tsv.gif'),
(47, 'text__csv', 'Comma separated','0','','','','','', '.csv', 'file', 'csv.gif'),
(48, 'text__msword', 'Microsoft office Word','0','','','','','', '.doc', 'file', 'doc.gif'),
(49, 'text__msxls', 'Microsoft office Excel','0','','','','','', '.xls', 'file', 'xls.gif'),
(50, 'text__msppt', 'Microsoft office Powerpoint','0','','','','','', '.ppt', 'file', 'ppt.gif'),
(51, 'text__oowriter', 'Open office writer','0','','','','','', '.odt', 'file', 'odt.gif'),
(52, 'text__oocalc', 'Open office calc','0','','','','','', '.ods', 'file', 'ods.gif'),
(53, 'text__ooimpress', 'Open office impress','0','','','','','', '.odp', 'file', 'odp.gif'),
(54, 'model__msword', 'Microsoft office model msword','0','','','','','', '.dot', 'file', 'dot.gif'),
(55, 'model__msxls', 'Microsoft office model msexcel','0','','','','','', '.xlt', 'file', 'xls.gif'),
(56, 'model__msppt', 'Microsoft office model msppt','0','','','','','', '.pot', 'file', 'pot.gif'),
(57, 'model__oowriter', 'Open office model writer','0','','','','','', '.ott', 'file', 'ott.gif'),
(58, 'model__oocalc', 'Open office model calc','0','','','','','', '.ots', 'file', 'ots.gif'),
(59, 'model__ooimpress', 'Open office model impress','0','','','','','', '.otp', 'file', 'otp.gif'),
(60, 'archive__gtar', 'Tar GNU','0','','','','','', '.gtar', 'file', 'gtar.gif'),
(61, 'archive__tar', 'Tar','0','','','','','', '.tar', 'file', 'tar.gif'),
(62, 'archive__zip', 'Compressed ZIP','0','','','','','', '.zip', 'file', 'zip.gif'),
(63, 'archive__gzip', 'Compressed GNU zip','0','','','','','', '.gzip', 'file', 'gzip.gif'),
(64, 'archive__gz', 'Compressed GNU gz','0','','','','','', '.gz', 'file', 'gz.gif'),
(65, 'archive__Z', 'Archive unix','0','','','','','', '.Z', 'file', 'Z.gif'),
(66, 'audio__basic', 'Audio basiques','0','','','','','', '.snd', 'file', 'snd.gif'),
(67, 'audio__x-aiff', 'Audio AIFF','0','','','','','', '.aif', 'file', 'aif.gif'),
(68, 'audio__x-wav', 'Audio Wave','0','','','','','', '.wav', 'file', 'wav.gif'),
(69, 'image__gif', 'Images gif','0','','','','','', '.gif', 'file', 'gif.gif'),
(70, 'image__jpeg', 'Images Jpeg','0','','','','','', '.jpg', 'file', 'jpg.gif'),
(71, 'image__tiff', 'Images Tiff','0','','','','','', '.tif', 'file', 'tif.gif'),
(72, 'image__png', 'Images png','0','','','','','', '.png', 'file', 'png.gif'),
(73, 'cao__solidwork', 'Part solidWork','0','','','','','', '.sdw', 'file', 'sdw.gif'),
(74, 'cao__cadds_zipadrawc4', 'Adraw cadds4x','0','','','','','', '_pd', 'adrawc4', 'cadds.gif'),
(75, 'cao__cadds_zipadrawc5', 'Adraw cadds5','0','','','','','', '_fd', 'adrawc5', 'cadds.gif'),
(76, 'cao__cadds4x', 'Part cadds4x','0','','','','','', '_pd', 'cadds4', 'cadds.gif'),
(77, 'cao__cadds5', 'Parts cadds5','0','','','','','', '_fd', 'cadds5', 'cadds.gif'),
(78, 'cao__cadds_adrawc4', 'Adraw cadds4x','0','','','','','', '.adrawc4', 'adrawc4', 'cadds.gif'),
(79, 'cao__cadds_adrawc5', 'Adraw cadds5','0','','','','','', '.adrawc5', 'adrawc5', 'cadds.gif'),
(80, 'cao__cadds_camu', 'Product cadds','0','','','','','', '_db', 'camu', 'cadds.gif'),
(81, 'cao__catpart', 'Part catia','0','','','','','', '.CATPart', 'file', 'CATPart.gif'),
(82, 'cao__catdrawing', 'Drawing catia','0','','','','','', '.CATDrawing', 'file', 'CATDrawing.gif'),
(83, 'cao__catproduct', 'Product catia','0','','','','','', '.CATProduct', 'file', 'CATProduct.gif'),
(84, 'cao__catmodel', 'Model catia','0','','','','','', '.model', 'file', 'model.gif'),
(85, 'cao__catalog', 'Catalog catia','0','','','','','', '.catalog', 'file', ''),
(86, 'cao__catanalysis', 'Analyse catia','0','','','','','', '.CATAnalysis', 'file', 'CATAnalysis.gif'),
(87, 'cao__catcatalog', 'Catalog catia','0','','','','','', '.CATCatalog', 'file', 'CATCatalog.gif'),
(88, 'cao__catmaterial', 'Material catia','0','','','','','', '.CATMaterial', 'file', 'CATMaterial.gif'),
(89, 'cao__catprocess', 'Process catia','0','','','','','', '.CATProcess', 'file', 'CATProcess.gif'),
(90, 'cao__catsetting', 'Setting catia','0','','','','','', '.CATSetting', 'file', 'CATSetting.gif'),
(91, 'cao__catscript', 'Script catia','0','','','','','', '.CATScript', 'file', 'CATScript.gif'),
(92, 'cao__pstree', 'Product ps','0','','','','','', '_ps', 'pstree', ''),
(93, 'cao__acad', 'AutoCADdwg','0','','','','','', '.dwg', 'file', 'dwg.gif'),
(94, 'nofile', 'Document without file','0','','','','','', '', 'nofile', '_default.gif');





-- ------------------------------------------------------ 
-- Password for admin = admin00
-- Password for user = user00
INSERT INTO `rb_users` (`auth_user_id`, `handle`, `passwd`, `email`, `lastlogin`, `is_active`) VALUES 
('22', 'admin', 'a5a30bc4c47888cd59c4e9df68d80242', NULL, '2000-01-01 00:00:00', 1),
('27', 'advuser', '0baf78e0dcadd5125fbb6ae50514b3e7', NULL, '2000-01-01 00:00:00', 1),
('28', 'user', '0baf78e0dcadd5125fbb6ae50514b3e7', NULL, '2000-01-01 00:00:00', 1);

-- For users
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (22,2,0), (27,2,0), (28,2,0);

-- For groups
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (21,3,0), (23,3,0), (24,3,0);

INSERT INTO `rb_groupusers` (`auth_user_id`, `group_id`) VALUES (22, 21),(27, 23),(28, 24);

-- ------------------------------------------------------ 
INSERT INTO `indices` (`indice_id`, `indice_value`) VALUES 
(1, 'A'),
(2, 'B'),
(3, 'C'),
(4, 'D'),
(5, 'E'),
(6, 'F'),
(7, 'G'),
(8, 'H'),
(9, 'I'),
(10, 'J'),
(11, 'K'),
(12, 'L'),
(13, 'M'),
(14, 'N'),
(15, 'P'),
(16, 'Q'),
(17, 'R'),
(18, 'S'),
(19, 'T'),
(20, 'U'),
(21, 'V'),
(22, 'W'),
(23, 'X'),
(24, 'Y'),
(25, 'Z'),
(26, 'AA'),
(27, 'AB'),
(28, 'AC'),
(29, 'AD'),
(30, 'AE');

-- Insert null values in activity role. If not, cant list the activity created until
-- role/activity link creation.
INSERT INTO `galaxia_activity_roles` (`activityId`, `roleId`) VALUES (0, 0);

-- Default ressource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (1,1,0);

-- Default project resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (2,1,0);

-- Default cadlib resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (3,1,10);

-- Default bookshop resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (4,1,15);

-- Default mockup resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (5,1,20);

-- Default workitem resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (6,1,25);

-- Default reposit resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (7,1,0);

-- Default read resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (8,1,0);

-- Default partner resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (9,1,0);




CREATE ALGORITHM = UNDEFINED VIEW contents AS 

SELECT document_id as id, document_number as name, description , 
        document_iteration as iteration , document_version as version,
        "bookshop" as space_name, "document" as class_name
FROM bookshop_documents

UNION ALL
SELECT document_id as id, document_number as name, description , 
        document_iteration as iteration , document_version as version,
        "cadlib" as space_name, "document" as class_name
FROM cadlib_documents

UNION ALL
SELECT document_id as id, document_number as name, description , 
        document_iteration as iteration , document_version as version,
        "mockup" as space_name, "document" as class_name
FROM mockup_documents

UNION ALL
SELECT document_id as id, document_number as name, description , 
        document_iteration as iteration , document_version as version,
        "workitem" as space_name, "document" as class_name
FROM workitem_documents


UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_version as version,
        "bookshop" as space_name, "docfile" as class_name
FROM bookshop_doc_files

UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_version as version,
        "cadlib" as space_name, "docfile" as class_name
FROM cadlib_doc_files

UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_version as version,
        "mockup" as space_name, "docfile" as class_name
FROM mockup_doc_files

UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_version as version,
        "workitem" as space_name, "docfile" as class_name
FROM workitem_doc_files


UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_iteration as version,
        "bookshop" as space_name, "recordfile" as class_name
FROM bookshop_files

UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_iteration as version,
        "cadlib" as space_name, "recordfile" as class_name
FROM cadlib_files

UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_iteration as version,
        "mockup" as space_name, "recordfile" as class_name
FROM mockup_files

UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_iteration as version,
        "workitem" as space_name, "recordfile" as class_name
FROM workitem_files;



