-- --------------------------------------------------------

--
-- Structure de la table `%container%s`
--
CREATE TABLE `%container%s` (
  `%container%_id` int(11) NOT NULL default '0',
  `%container%_number` varchar(128) NOT NULL,
  `%container%_name` varchar(128) default '',
  `%container%_state` varchar(32) NOT NULL default 'init',
  `%container%_description` varchar(512) default NULL,
  `file_only` tinyint(1) NOT NULL default '0',
  `%container%_version` int(11) default NULL,
  `%container%_iteration` int(11) default NULL,
  `project_id` int(11) default NULL,
  `default_process_id` int(11) default NULL,
  `default_read_id` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `owned_by` int(11) default NULL,
  PRIMARY KEY (`%container%_id`),
  UNIQUE KEY `UC_%container%_number` (`%container%_number`),
  KEY `INDEX_%container%s_1` (`%container%_version`),
  KEY `INDEX_%container%s_2` (`project_id`),
  KEY `INDEX_%container%s_3` (`%container%_description`),
  KEY `INDEX_%container%s_4` (`file_only`),
  KEY `INDEX_%container%s_5` (`default_process_id`),
  KEY `INDEX_%container%s_6` (`default_read_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `%container%_alias`
--

CREATE TABLE `%container%_alias` (
  `alias_id` int(11) NOT NULL,
  `%container%_id` int(11) NOT NULL,
  `%container%_number` varchar(16) NOT NULL,
  `%container%_description` varchar(128) default NULL,
  PRIMARY KEY  (`alias_id`),
  UNIQUE KEY `UNIQ_%container%_alias` (`alias_id`,`%container%_id`),
  KEY `INDEX_%container%_alias_1` (`%container%_id`),
  KEY `INDEX_%container%_alias_2` (`%container%_number`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `%container%_doccomments`
--

CREATE TABLE `%container%_doccomments` (
  `comment_id` INT NOT NULL ,
  `document_id` INT NOT NULL ,
  `comment` TEXT NOT NULL ,
  `open_by` INT NOT NULL ,
  PRIMARY KEY  (`comment_id`),
  KEY `INDEX_%container%_doccomments_1` (`document_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `%container%_documents`
--
CREATE TABLE `%container%_documents` (
  `document_id` int(11) NOT NULL,
  `document_bid` CHAR(15) NOT NULL,
  `document_number` varchar(128)  NOT NULL,
  `document_name` varchar(256)  NOT NULL,
  `document_state` varchar(32)  NOT NULL default 'init',
  `document_access_code` int(11) NOT NULL default 0,
  `document_iteration` int(11) NOT NULL default 1,
  `document_version` int(11) NOT NULL default 1,
  `document_life_stage` int(1) NOT NULL default 1,
  `from_document` int(11) default NULL,
  `%container%_id` int(11) NOT NULL,
  `default_process_id` int(11) default NULL,
  `instance_id` int(11) default NULL,
  `doctype_id` int(11) NOT NULL,
  `category_id` int(11) default NULL,
  `check_out_by` int(11) default NULL,
  `check_out_date` int(11) default NULL,
  `description` varchar(128)  default NULL,
  `update_date` int(11) default NULL,
  `update_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `owned_by` int(11) default NULL,
  PRIMARY KEY  (`document_id`),
  UNIQUE KEY `UNIQ_%container%_documents_1` (`document_number`,`document_version`, `document_iteration`),
  UNIQUE KEY `UNIQ_%container%_documents_2` (`document_bid`,`document_version`, `document_iteration`),
  KEY `INDEX_%container%_documents_1` (`document_number`),
  KEY `INDEX_%container%_documents_2` (`document_version`),
  KEY `INDEX_%container%_documents_3` (`document_iteration`),
  KEY `INDEX_%container%_documents_4` (`doctype_id`),
  KEY `INDEX_%container%_documents_5` (`%container%_id`),
  KEY `INDEX_%container%_documents_6` (`category_id`),
  KEY `INDEX_%container%_documents_7` (`document_state`),
  KEY `INDEX_%container%_documents_8` (`description`),
  KEY `INDEX_%container%_documents_9` (`from_document`),
  KEY `INDEX_%container%_documents_10` (`check_out_by`),
  KEY `INDEX_%container%_documents_11` (`update_by`),
  KEY `INDEX_%container%_documents_12` (`open_by`),
  KEY `INDEX_%container%_documents_13` (`document_bid`),
  KEY `INDEX_%container%_documents_14` (`document_name`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `%container%_doc_files`
--
CREATE TABLE `%container%_doc_files` (
  `file_id` int(11) NOT NULL,
  `file_bid` CHAR(15) NOT NULL,
  `document_id` int(11) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `reposit_id` int(11) NOT NULL,
  `file_version` int(11) NOT NULL default 1,
  `file_iteration` int(11) NOT NULL default 1,
  `file_access_code` int(11) NOT NULL default 0,
  `file_mode` int(1) NOT NULL default 0,
  `file_life_stage` int(1) NOT NULL default 1,
  `from_file` int(11) default NULL,
  `file_root_name` varchar(128) NOT NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) NOT NULL default 'init',
  `file_type` varchar(128) NOT NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_checkout_by` int(11) default NULL,
  `file_checkout_date` int(11) default NULL,
  `file_open_date` int(11) NOT NULL,
  `file_open_by` int(11) NOT NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `UNIQ_%container%_doc_files_2` (`file_bid`, `file_version`, `file_iteration`),
  KEY `INDEX_%container%_doc_files_1` (`document_id`),
  KEY `INDEX_%container%_doc_files_2` (`file_name`),
  KEY `INDEX_%container%_doc_files_3` (`file_root_name`),
  KEY `INDEX_%container%_doc_files_4` (`file_extension`),
  KEY `INDEX_%container%_doc_files_5` (`file_type`),
  KEY `INDEX_%container%_doc_files_6` (`file_md5`),
  KEY `INDEX_%container%_doc_files_7` (`file_checkout_by`),
  KEY `INDEX_%container%_doc_files_8` (`file_name`, `file_iteration`),
  KEY `INDEX_%container%_doc_files_9` (`reposit_id`)
) ENGINE=InnoDB;

CREATE TABLE `%container%_doc_file_role` (
  `link_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `role_id` int(1) NOT NULL,
  PRIMARY KEY  (`link_id`),
  UNIQUE KEY `%container%_doc_file_role_uniq1` (`file_id`,`role_id`)
) ENGINE=InnoDB;

CREATE TABLE `%container%_files` (
  `file_id` int(11) NOT NULL,
  `file_bid` CHAR(15) NOT NULL,
  `%container%_id` int(11) NOT NULL default '0',
  `import_order` int(11) default NULL,
  `file_name` varchar(128)  NOT NULL,
  `reposit_id` int(11) NOT NULL,
  `file_version` int(11) NOT NULL default 1,
  `file_iteration` int(11) NOT NULL default 1,
  `file_access_code` int(11) NOT NULL default 0,
  `file_life_stage` int(1) NOT NULL default 1,
  `file_root_name` varchar(128) NOT NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) NOT NULL default 'init',
  `file_type` varchar(128) NOT NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_open_date` int(11) NOT NULL,
  `file_open_by` int(11) NOT NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  `from_file` int(11) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `UNIQ_%container%_files_2` (`file_bid`, `file_version`, `file_iteration`),
  KEY `INDEX_%container%_files_1` (`%container%_id`),
  KEY `INDEX_%container%_files_2` (`file_name`),
  KEY `INDEX_%container%_files_3` (`file_root_name`),
  KEY `INDEX_%container%_files_4` (`file_extension`),
  KEY `INDEX_%container%_files_5` (`file_type`),
  KEY `INDEX_%container%_files_6` (`file_md5`),
  KEY `INDEX_%container%_files_8` (`file_name`, `file_iteration`),
  KEY `INDEX_%container%_files_9` (`reposit_id`),
  KEY `INDEX_%container%_files_10` (`import_order`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `%container%_doc_rel`
--

CREATE TABLE `%container%_doc_rel` (
  `dr_link_id` int(11) NOT NULL,
  `dr_document_id` int(11) default NULL,
  `dr_l_document_id` int(11) default NULL,
  `dr_access_code` int(11) NOT NULL default '15',
  PRIMARY KEY  (`dr_link_id`),
  UNIQUE KEY `%container%_doc_rel_uniq1` (`dr_document_id`,`dr_l_document_id`),
  KEY `INDEX_%container%_doc_rel_1` (`dr_document_id`),
  KEY `INDEX_%container%_doc_rel_2` (`dr_l_document_id`)
) ENGINE=InnoDB;


-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE TABLE `%container%_cont_notifications` (
  `notification_id` INT NOT NULL ,
  `user_id` INT NOT NULL ,
  `%container%_id` INT NOT NULL ,
  `event` VARCHAR (64) NOT NULL ,
  `rb_condition` BLOB,
  PRIMARY KEY  (`notification_id`),
  KEY `INDEX_%container%_cont_notifications_1` (`user_id`),
  KEY `INDEX_%container%_cont_notifications_2` (`%container%_id`),
  KEY `INDEX_%container%_cont_notifications_3` (`event`),
  UNIQUE KEY `UNIQ_%container%_cont_notifications_1` (`user_id`,`%container%_id`,`event`)
) ENGINE=InnoDB;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE TABLE `%container%_documents_extends` (
  `extend_id` int(11) NOT NULL,
  PRIMARY KEY (`extend_id`)
) ENGINE=InnoDB;

CREATE TABLE `%container%s_extends` (
  `extend_id` int(11) NOT NULL,
  PRIMARY KEY (`extend_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------
--
-- Structure de la table `%container%s_history`
--
CREATE TABLE `%container%s_history` (
  `histo_order` int(11) NOT NULL default '0',
  `action_name` varchar(32) default NULL,
  `action_by` varchar(32) default NULL,
  `action_date` int(11) default NULL,
  `%container%_id` int(11) NOT NULL default '0',
  `%container%_number` varchar(16) default NULL,
  `%container%_state` varchar(16) default NULL,
  `%container%_description` varchar(128) default NULL,
  `%container%_version` int(11) default NULL,
  `project_id` int(11) default NULL,
  `default_read_id` text  default NULL,
  `default_process_id` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `%container%_files_history`
--
CREATE TABLE `%container%_doc_files_history` (
  `histo_order` int(11) NOT NULL default 0,
  `action_name` varchar(32) NOT NULL default '',
  `action_by` varchar(64) NOT NULL default '',
  `action_date` int(11) NOT NULL default 0,
  `file_id` int(11) default NULL,
  `document_id` int(11) default NULL,
  `file_bid` CHAR(15) default NULL,
  `file_root_name` varchar(128) default NULL,
  `file_iteration` int(11) default NULL,
  `file_version` int(11) default NULL,
  `file_name` varchar(128) default NULL,
  `file_path` text default NULL,
  `file_access_code` int(11) default NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) default NULL,
  `file_type` varchar(128) default NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_open_date` int(11) default NULL,
  `file_open_by` int(11) default NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `%container%_documents_history`
--

CREATE TABLE `%container%_documents_history` (
  `histo_order` int(11) NOT NULL default 0,
  `action_name` varchar(32) NOT NULL default '',
  `action_by` varchar(64) NOT NULL default '',
  `action_date` int(11) NOT NULL default 0,
  `document_id` int(11) default NULL,
  `%container%_id` int(11) default NULL,
  `document_bid` CHAR(15)  default NULL,
  `document_number` varchar(128) default NULL,
  `document_state` varchar(32) default NULL,
  `document_access_code` int(11) default NULL,
  `document_iteration` int(11) default NULL,
  `document_version` int(11) default NULL,
  `description` varchar(128) default NULL,
  `document_life_stage` int(1) default NULL,
  `from_document` int(11) default NULL,
  `category_id` int(11) default NULL,
  `check_out_by` int(11) default NULL,
  `check_out_date` int(11) default NULL,
  `update_date` int(11) default NULL,
  `update_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `doctype_id` int(11) default NULL,
  `instance_id` int(11) default NULL,
  `activity_id` int(11) default NULL,
  `comment` varchar(255) default NULL,
  PRIMARY KEY  (`histo_order`),
  KEY `INDEX_%container%_documents_history_1` (`document_number`),
  KEY `INDEX_%container%_documents_history_2` (`document_version`),
  KEY `INDEX_%container%_documents_history_3` (`document_iteration`),
  KEY `INDEX_%container%_documents_history_5` (`%container%_id`),
  KEY `INDEX_%container%_documents_history_7` (`document_state`),
  KEY `INDEX_%container%_documents_history_8` (`description`),
  KEY `INDEX_%container%_documents_history_9` (`action_name`)
) ENGINE=InnoDB;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE ALGORITHM=TEMPTABLE VIEW `%container%_alias_view` AS
SELECT
	`%container%s`.`%container%_id` AS `%container%_id`,
	`%container%_alias`.`alias_id` AS `alias_id`,
	`%container%_alias`.`%container%_number` AS `%container%_number`,
	`%container%_alias`.`%container%_description` AS `%container%_description`,
	`%container%s`.`%container%_state` AS `%container%_state`,
	`%container%s`.`file_only` AS `file_only`,
	`%container%s`.`%container%_version` AS `%container%_version`,
	`%container%s`.`project_id` AS `project_id`,
	`%container%s`.`default_process_id` AS `default_process_id`,
	`%container%s`.`default_read_id` AS `default_read_id`,
	`%container%s`.`open_date` AS `open_date`,
	`%container%s`.`open_by` AS `open_by`,
	`%container%s`.`forseen_close_date` AS `forseen_close_date`,
	`%container%s`.`close_date` AS `close_date`,
	`%container%s`.`close_by` AS `close_by`
FROM `%container%_alias`
LEFT JOIN `%container%s`
ON `%container%_alias`.`%container%_id` = `%container%s`.`%container%_id`
UNION
SELECT
	`%container%s`.`%container%_id` AS `%container%_id`,
	'' AS `alias_id`,
	`%container%s`.`%container%_number` AS `%container%_number`,
	`%container%s`.`%container%_description` AS `%container%_description`,
	`%container%s`.`%container%_state` AS `%container%_state`,
	`%container%s`.`file_only` AS `file_only`,
	`%container%s`.`%container%_version` AS `%container%_version`,
	`%container%s`.`project_id` AS `project_id`,
	`%container%s`.`default_process_id` AS `default_process_id`,
	`%container%s`.`default_read_id` AS `default_read_id`,
	`%container%s`.`open_date` AS `open_date`,
	`%container%s`.`open_by` AS `open_by`,
	`%container%s`.`forseen_close_date` AS `forseen_close_date`,
	`%container%s`.`close_date` AS `close_date`,
	`%container%s`.`close_by` AS `close_by`
FROM `%container%s`
LEFT JOIN `%container%_alias`
ON `%container%_alias`.`%container%_id` = `%container%s`.`%container%_id`;

