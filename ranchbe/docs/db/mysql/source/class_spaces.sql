drop table spaces;
drop table class;

create table spaces(
id int(1),
name varchar(16) not null
);

create table class(
id int(2),
name varchar(16) not null
);

ALTER TABLE `spaces` ADD PRIMARY KEY(`id`);
ALTER TABLE `class` ADD PRIMARY KEY(`id`);

insert into spaces (id, name) values (1, 'bookshop');
insert into spaces (id, name) values (2, 'cadlib');
insert into spaces (id, name) values (3, 'mockup');
insert into spaces (id, name) values (4, 'workitem');

insert into class (id, name) values (1, 'project');
insert into class (id, name) values (2, 'container');
insert into class (id, name) values (3, 'document');
insert into class (id, name) values (4, 'docfile');
insert into class (id, name) values (5, 'recordfile');
insert into class (id, name) values (6, 'partner');
insert into class (id, name) values (7, 'doctype');
insert into class (id, name) values (8, 'process');
insert into class (id, name) values (9, 'process');



