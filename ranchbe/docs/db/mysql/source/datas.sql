
-- ------------------------------------------------------ 
-- Password for admin = admin00
-- Password for user = user00
INSERT INTO `rb_users` (`auth_user_id`, `handle`, `passwd`, `email`, `lastlogin`, `is_active`) VALUES 
('22', 'admin', 'a5a30bc4c47888cd59c4e9df68d80242', NULL, '2000-01-01 00:00:00', 1),
('27', 'advuser', '0baf78e0dcadd5125fbb6ae50514b3e7', NULL, '2000-01-01 00:00:00', 1),
('28', 'user', '0baf78e0dcadd5125fbb6ae50514b3e7', NULL, '2000-01-01 00:00:00', 1);

-- For users
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (22,2,0), (27,2,0), (28,2,0);

-- For groups
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (21,3,0), (23,3,0), (24,3,0);

INSERT INTO `rb_groupusers` (`auth_user_id`, `group_id`) VALUES (22, 21),(27, 23),(28, 24);

-- ------------------------------------------------------ 
INSERT INTO `indices` (`indice_id`, `indice_value`) VALUES 
(1, 'A'),
(2, 'B'),
(3, 'C'),
(4, 'D'),
(5, 'E'),
(6, 'F'),
(7, 'G'),
(8, 'H'),
(9, 'I'),
(10, 'J'),
(11, 'K'),
(12, 'L'),
(13, 'M'),
(14, 'N'),
(15, 'P'),
(16, 'Q'),
(17, 'R'),
(18, 'S'),
(19, 'T'),
(20, 'U'),
(21, 'V'),
(22, 'W'),
(23, 'X'),
(24, 'Y'),
(25, 'Z'),
(26, 'AA'),
(27, 'AB'),
(28, 'AC'),
(29, 'AD'),
(30, 'AE');

-- Insert null values in activity role. If not, cant list the activity created until
-- role/activity link creation.
INSERT INTO `galaxia_activity_roles` (`activityId`, `roleId`) VALUES (0, 0);

-- Default ressource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (1,1,0);

-- Default project resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (2,1,0);

-- Default cadlib resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (3,1,10);

-- Default bookshop resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (4,1,15);

-- Default mockup resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (5,1,20);

-- Default workitem resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (6,1,25);

-- Default reposit resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (7,1,0);

-- Default read resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (8,1,0);

-- Default partner resource
INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES (9,1,0);

