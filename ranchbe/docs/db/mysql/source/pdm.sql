CREATE TABLE `pdm_effectivity` (
  `effectivity_id` int(11) NOT NULL,
  `number` varchar(128) NULL,
  `name` varchar(128) NULL,
  `description` varchar(512) NULL,
  `usage_id` int(11) NULL,
  `usage_type` int(11) NULL,
  `cd_id` int(11) NULL,
  `start_date` int(11) NULL,
  `end_date` int(11) NULL,
  `lot_id` int(11) NULL,
  `lot_size` int(11) NULL,
  `snum_start_id` int(11) NULL,
  `snum_end_id` int(11) NULL,
  PRIMARY KEY  (`effectivity_id`),
  UNIQUE KEY `UNIQ_pdm_effectivity_1` (`number`),
  KEY `INDEX_pdm_effectivity_1` (`name`),
  KEY `INDEX_pdm_effectivity_2` (`description`),
  KEY `INDEX_pdm_effectivity_3` (`usage_id`),
  KEY `INDEX_pdm_effectivity_4` (`cd_id`),
  KEY `INDEX_pdm_effectivity_5` (`start_date`),
  KEY `INDEX_pdm_effectivity_6` (`end_date`),
  KEY `INDEX_pdm_effectivity_7` (`lot_id`),
  KEY `INDEX_pdm_effectivity_8` (`snum_start_id`),
  KEY `INDEX_pdm_effectivity_9` (`snum_end_id`)
);

CREATE TABLE `pdm_product` (
  `p_id` int(11) NOT NULL,
  `p_number` varchar(128) NOT NULL,
  `p_name` varchar(128) NULL,
  `p_description` varchar(512) NULL,
  PRIMARY KEY  (`p_id`),
  UNIQUE KEY `pdm_product_uniq1` (`p_number`),
  KEY `INDEX_pdm_product_1` (`p_name`),
  KEY `INDEX_pdm_product_2` (`p_description`)
);

CREATE TABLE `pdm_usage` (
  `usg_id` int(11) NOT NULL,
  `usg_number` varchar(128) NOT NULL,
  `usg_name` varchar(128) NULL,
  `usg_description` varchar(512) NULL,
  `usg_type_id` int(1) NOT NULL default 1,
  `parent_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  PRIMARY KEY  (`usg_id`),
  KEY(`usg_number`),
  INDEX(`usg_name`),
  INDEX(`child_id`),
  INDEX(`parent_id`)
);

CREATE TABLE `pdm_product_definition` (
  `pd_id` int(11) NOT NULL,
  `ct_id` int(11) NOT NULL,
  `pv_id` int(11) NOT NULL,
  `pd_description` varchar(512) NULL,
  PRIMARY KEY  (`pd_id`),
  UNIQUE KEY `UNIQ_pdm_product_definition_1` (`pv_id`,`ct_id`),
  INDEX(`pd_description`)
);

CREATE TABLE `pdm_product_version` (
  `pv_id` int(11) NOT NULL,
  `of_product_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL,
  `pv_description` varchar(512) NULL,
  `document_id` int(11) NULL,
  `document_space` varchar(16) NULL,
  PRIMARY KEY  (`pv_id`),
  UNIQUE KEY `pdm_product_version_uniq1` (`of_product_id`,`version_id`),
  KEY `INDEX_pdm_product_version_1` (`of_product_id`),
  KEY `INDEX_pdm_product_version_2` (`version_id`),
  KEY `INDEX_pdm_product_version_3` (`pv_description`)
);

CREATE TABLE `pdm_context_application` (
  `cta_id` int(11) NOT NULL,
  `application` varchar(128) NULL,
  PRIMARY KEY  (`cta_id`),
  KEY `INDEX_pdm_context_productdefinition_1` (`application`)
);
ALTER TABLE `pdm_context_application`
 ADD `name` VARCHAR( 128 ) NULL,
 ADD `life_cycle_stage` VARCHAR( 128 ) NULL,
 ADD INDEX `INDEX_pdm_context_productdefinition_2` ( `name` ),
 ADD INDEX `INDEX_pdm_context_productdefinition_3` ( `life_cycle_stage` );


CREATE TABLE `pdm_context_association` (
  `link_id` int(11) NOT NULL,
  `pd_id` int(11) NOT NULL,
  `act_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL default 0,
  PRIMARY KEY  (`link_id`),
  UNIQUE KEY `UNIQ_pdm_context_association_1` (`pd_id`,`act_id`),
  KEY `INDEX_pdm_context_association_1` (`pd_id`),
  KEY `INDEX_pdm_context_association_2` (`act_id`),
  KEY `INDEX_pdm_context_association_3` (`role_id`)
);

CREATE TABLE `pdm_context_role` (
  `role_id` int(11) NOT NULL,
  `name` varchar(128) NULL,
  `description` varchar(512) NULL,
  PRIMARY KEY  (`role_id`),
  KEY `INDEX_pdm_context_role_1` (`name`),
  KEY `INDEX_pdm_context_role_2` (`description`)
);

CREATE TABLE `pdm_configuration_concept` (
  `concept_id` int(11) NOT NULL,
  `number` varchar(128) NOT NULL,
  `name` varchar(128) NULL,
  `description` varchar(512) NULL,
  PRIMARY KEY  (`concept_id`),
  UNIQUE KEY `UNIQ_pdm_configuration_concept_1` (`number`),
  KEY `INDEX_pdm_configuration_concept_1` (`name`),
  KEY `INDEX_pdm_configuration_concept_2` (`description`)
);

CREATE TABLE `pdm_configuration_design` (
  `cd_id` int(11) NOT NULL,
  `name` varchar(128) NULL,
  `description` varchar(512) NULL,
  `ci_id` int(11) NOT NULL,
  `pv_id` int(11) NOT NULL,
  PRIMARY KEY  (`ci_id`),
  UNIQUE KEY  `UNIQ_pdm_configuration_design_1` (`ci_id`, `pv_id`),
  INDEX `INDEX_pdm_configuration_design_1` (`name`),
  INDEX `INDEX_pdm_configuration_design_2` (`ci_id`),
  INDEX `INDEX_pdm_configuration_design_3` (`pv_id`)
);

CREATE TABLE `pdm_configuration_item` (
  `ci_id` int(11) NOT NULL,
  `number` varchar(128) NULL,
  `name` varchar(128) NULL,
  `description` varchar(512) NULL,
  `concept_id` int(11) NOT NULL,
  `purpose` varchar(512) NULL,
  PRIMARY KEY  (`ci_id`),
  UNIQUE KEY  `UNIQ_pdm_configuration_item_1` (`number`),
  INDEX `INDEX_pdm_configuration_item_1` (`name`),
  INDEX `INDEX_pdm_configuration_item_2` (`concept_id`),
  INDEX `INDEX_pdm_configuration_item_3` (`purpose`)
);

CREATE ALGORITHM = UNDEFINED VIEW pdm_product_viewa AS 
SELECT 
pdm_product.p_id,
pdm_product_version.pv_id,
pdm_product_definition.pd_id,
pdm_product_definition.ct_id,
pdm_context_association.act_id,
pdm_product.p_number,
pdm_product.p_name,
pdm_product.p_description,
pdm_product_version.pv_description,
pdm_product_definition.pd_description,
pdm_product_version.version_id,
pdm_product_version.document_id,
pdm_product_version.document_space,
pdm_context_association.role_id as act_role_id
FROM pdm_product
INNER JOIN pdm_product_version ON 
pdm_product.p_id = pdm_product_version.of_product_id
INNER JOIN pdm_product_definition ON
pdm_product_version.pv_id = pdm_product_definition.pv_id
LEFT OUTER JOIN pdm_context_association ON
pdm_context_association.pd_id = pdm_product_definition.pd_id
;
