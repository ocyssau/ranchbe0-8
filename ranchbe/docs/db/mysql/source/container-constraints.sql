--
-- Contraintes pour la table `%container%s`
--
ALTER TABLE `%container%s`
  ADD CONSTRAINT `FK_%container%s_1` FOREIGN KEY (`%container%_version`)
  REFERENCES `indices` (`indice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%s_2` FOREIGN KEY (`project_id`)
  REFERENCES `projects` (`project_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%s_3` FOREIGN KEY (`default_process_id`)
  REFERENCES `galaxia_processes` (`pId`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%s_4` FOREIGN KEY (`%container%_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%s_5` FOREIGN KEY (`default_read_id`)
  REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `%container%_alias`
--
ALTER TABLE `%container%_alias`
  ADD CONSTRAINT `FK_%container%_alias_1` FOREIGN KEY (`%container%_id`)
  REFERENCES `%container%s` (`%container%_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `%container%_documents`
--

ALTER TABLE `%container%_documents`
  ADD CONSTRAINT `FK_%container%_documents_2` FOREIGN KEY (`doctype_id`) 
  REFERENCES `doctypes` (`doctype_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%_documents_3` FOREIGN KEY (`document_version`) 
  REFERENCES `indices` (`indice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%_documents_4` FOREIGN KEY (`%container%_id`) 
  REFERENCES `%container%s` (`%container%_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%_documents_5` FOREIGN KEY (`category_id`) 
  REFERENCES `categories` (`category_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%_documents_6` FOREIGN KEY (`instance_id`) 
  REFERENCES `galaxia_instances` (`instanceId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%_documents_7` FOREIGN KEY (`default_process_id`) 
  REFERENCES `galaxia_processes` (`pId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%_documents_8` FOREIGN KEY (`open_by`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%_documents_9` FOREIGN KEY (`update_by`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%_documents_10` FOREIGN KEY (`owned_by`)
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%_documents_11` FOREIGN KEY (`document_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `%container%_doccomments`
  	ADD CONSTRAINT `FK_%container%_doccomments_1` FOREIGN KEY (`document_id`)
  	REFERENCES `%container%_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_%container%_doccomments_2` FOREIGN KEY (`open_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;


--
-- Contraintes pour la table `%container%_doc_files`
--
ALTER TABLE `%container%_doc_files` 
  	ADD CONSTRAINT `FK_%container%_doc_files_1` FOREIGN KEY (`document_id`) 
  	REFERENCES `%container%_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  	ADD CONSTRAINT `FK_%container%_doc_files_2` FOREIGN KEY (`file_id`) 
  	REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  	ADD CONSTRAINT `FK_%container%_doc_files_3` FOREIGN KEY (`reposit_id`) 
  	REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_%container%_doc_files_4` FOREIGN KEY (`file_open_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_%container%_doc_files_5` FOREIGN KEY (`file_update_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_%container%_doc_files_6` FOREIGN KEY (`file_checkout_by`) 
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `%container%_files`
--
ALTER TABLE `%container%_files` 
	ADD CONSTRAINT `FK_%container%_files_1` FOREIGN KEY (`%container%_id`) 
	REFERENCES `%container%s` (`%container%_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_%container%_files_2` FOREIGN KEY (`file_id`) 
	REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_%container%_files_3` FOREIGN KEY (`reposit_id`) 
	REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_%container%_files_4` FOREIGN KEY (`import_order`) 
	REFERENCES `import_history` (`import_order`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_%container%_files_5` FOREIGN KEY (`file_open_by`) 
	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_%container%_files_6` FOREIGN KEY (`file_update_by`) 
	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `%container%_doc_rel`
--
ALTER TABLE `%container%_doc_rel`
  ADD CONSTRAINT `FK_%container%_doc_rel_1` FOREIGN KEY (`dr_document_id`) 
  REFERENCES `%container%_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%_doc_rel_2` FOREIGN KEY (`dr_l_document_id`) 
  REFERENCES `%container%_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `%container%_cont_notifications`
  ADD CONSTRAINT `FK_%container%_cont_notifications_1` FOREIGN KEY (`%container%_id`)
  REFERENCES `%container%s` (`%container%_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%_cont_notifications_2` FOREIGN KEY (`user_id`)
  	REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `%container%_doc_file_role`
  ADD CONSTRAINT `FK_%container%_doc_file_role_1` FOREIGN KEY (`file_id`)
  REFERENCES `%container%_doc_files` (`file_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `%container%_documents_extends`
--
ALTER TABLE `%container%_documents_extends` 
  ADD CONSTRAINT `FK_%container%_documents_extends_1` FOREIGN KEY (`extend_id`) 
  REFERENCES `%container%_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `%container%s_extends`
--
ALTER TABLE `%container%s_extends` 
  ADD CONSTRAINT `FK_%container%s_extends_1` FOREIGN KEY (`extend_id`) 
  REFERENCES `%container%s` (`%container%_id`) ON DELETE CASCADE ON UPDATE CASCADE;

