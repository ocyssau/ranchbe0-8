<?php

compile_sql_script('fr', '.');
compile_sql_script('en', '.');

function compile_sql_script($lang, $path){
  $result = fopen ( '../ranchbe_'.$lang.'.sql', 'w' );
  //$dbname = 'ranchbe0-8';
  
  /** this var existing since rb 0.8.0.2
   * @var integer
   */
  $dbVersion = 15; //since rb 0.8.0.5beta4
  
  /** this var existing since rb 0.8.0.2
   *  Time of the compilation of db
   * @var integer
   */
  $compiled = time();
  
  //$drop = "DROP DATABASE `$dbname`;";
  //$create = "CREATE DATABASE `$dbname` DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;";
  //$create .= "USE `$dbname`;";
  
  //
  $base = file_get_contents ( $path . '/base.sql' );
  $container_src = file_get_contents ( $path . '/container.sql' );
  $container_constraint_src = file_get_contents ( $path . '/container-constraints.sql' );
  //$container_project_relation = file_get_contents($path.'/container-project-relation.sql');
  
  $container = '';
  $container_constraint = '';
  foreach ( array ('bookshop', 'cadlib', 'mockup', 'workitem' ) as $space ) {
  	$container .= str_replace ( '%container%', $space, $container_src, $count );
  	$container_constraint .= str_replace ( '%container%', $space, $container_constraint_src, $count );
  }
  
  $data = "-- --------------------------DATAS-------------------------\n";
  
  $data .= "INSERT INTO `db_version` (`version`, `compiled`) VALUES ($dbVersion,$compiled);" . "\n";
  $data .= 'INSERT INTO `objects` (`object_id`, `class_id`, `space_id`) VALUES' . "\n";
  $i = 30;
  //for doctypes
  while ( $i < 99 ) {
  	$data .= "($i,110,0)," . "\n";
  	$i ++;
  }
  $data .= "(99,110,0);" . "\n"; //close with ";"
  
  
  $data .= file_get_contents ( $path . '/datas_'.$lang.'.sql' ) . "\n";
  $data .= file_get_contents ( $path . '/datas.sql' ) . "\n";
  
  //$pdm = file_get_contents($path.'/pdm.sql');
  
  $view_contents = file_get_contents ( $path . '/view_contents.sql' );
  //$view_objects_documents_docfiles = file_get_contents ( $path . '/view_objects_documents_docfiles.sql' );
  
  //fwrite ( $result, $drop . "\n" );
  fwrite ( $result, $create . "\n" );
  fwrite ( $result, $base . "\n" );
  fwrite ( $result, $container . "\n" );
  fwrite ( $result, $container_constraint . "\n" );
  //fwrite($result, $container_project_relation."\n");
  fwrite ( $result, $data . "\n" );
  fwrite ( $result, $view_contents );
  //fwrite ( $result, $view_objects_documents_docfiles );
  fclose ( $result );
}
