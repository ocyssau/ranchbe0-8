

CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW view_objects_documents_docfiles AS 
SELECT 
tobj.*,
talias.*,
tdocs.*,
tdocextends.*, 
tcategories.category_number,
tdoctypes.doctype_number,
tfiles.file_id,
tfiles.file_bid,
tfiles.reposit_id,
tfiles.file_version,
tfiles.file_iteration,
tfiles.file_name,
tfileroles.role_id as file_role_id,
checkouser.handle as check_out_by_name,
updateuser.handle as update_by_name,
openuser.handle as open_by_name
FROM objects AS tobj
LEFT OUTER JOIN `objects_alias` AS talias 
	ON tobj.object_id = talias.object_id
INNER JOIN `workitem_documents` AS tdocs 
	ON tobj.object_id = tdocs.document_id
LEFT OUTER JOIN `workitem_documents_extends` AS tdocextends 
	ON tdocextends.extend_id=tdocs.document_id
LEFT OUTER JOIN `workitem_doc_files` AS tfiles 
	ON tfiles.document_id=tdocs.document_id
LEFT OUTER JOIN `workitem_doc_file_role` AS tfileroles 
	ON tfileroles.file_id=tfiles.file_id
LEFT OUTER JOIN `categories` AS tcategories 
	ON tcategories.category_id=tdocs.category_id
LEFT OUTER JOIN `doctypes` AS tdoctypes 
	ON tdoctypes.doctype_id=tdocs.doctype_id
LEFT OUTER JOIN `rb_users` AS tusers 
	ON tusers.auth_user_id=tdocs.check_out_by
LEFT OUTER JOIN `rb_users` AS checkouser 
	ON checkouser.auth_user_id=tdocs.check_out_by
LEFT OUTER JOIN `rb_users` AS updateuser 
	ON updateuser.auth_user_id=tdocs.update_by
LEFT OUTER JOIN `rb_users` AS openuser 
	ON openuser.auth_user_id=tdocs.open_by
;


