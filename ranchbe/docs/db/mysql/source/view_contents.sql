
CREATE ALGORITHM = UNDEFINED VIEW contents AS 

SELECT document_id as id, document_number as name, description , 
        document_iteration as iteration , document_version as version,
        "bookshop" as space_name, "document" as class_name
FROM bookshop_documents

UNION ALL
SELECT document_id as id, document_number as name, description , 
        document_iteration as iteration , document_version as version,
        "cadlib" as space_name, "document" as class_name
FROM cadlib_documents

UNION ALL
SELECT document_id as id, document_number as name, description , 
        document_iteration as iteration , document_version as version,
        "mockup" as space_name, "document" as class_name
FROM mockup_documents

UNION ALL
SELECT document_id as id, document_number as name, description , 
        document_iteration as iteration , document_version as version,
        "workitem" as space_name, "document" as class_name
FROM workitem_documents


UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_version as version,
        "bookshop" as space_name, "docfile" as class_name
FROM bookshop_doc_files

UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_version as version,
        "cadlib" as space_name, "docfile" as class_name
FROM cadlib_doc_files

UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_version as version,
        "mockup" as space_name, "docfile" as class_name
FROM mockup_doc_files

UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_version as version,
        "workitem" as space_name, "docfile" as class_name
FROM workitem_doc_files


UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_iteration as version,
        "bookshop" as space_name, "recordfile" as class_name
FROM bookshop_files

UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_iteration as version,
        "cadlib" as space_name, "recordfile" as class_name
FROM cadlib_files

UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_iteration as version,
        "mockup" as space_name, "recordfile" as class_name
FROM mockup_files

UNION ALL
SELECT file_id as id, file_name as name, file_root_name as description,
        file_iteration as iteration , file_iteration as version,
        "workitem" as space_name, "recordfile" as class_name
FROM workitem_files;



