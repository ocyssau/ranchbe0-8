--
-- Base de donn�es: `ranchbe0-8`
--


--
-- Structure de la table `rbdb`
--
CREATE TABLE `db_version` (
  `version` int(2) NOT NULL,
  `compiled` int(4) NOT NULL
) ENGINE = MYISAM;


-- --------------------------------------------------------

--
-- Structure de la table `objects`
--
CREATE TABLE `objects` (
  `object_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `space_id` int(11) NOT NULL,
  PRIMARY KEY (`object_id`),
  KEY `INDEX_objects_1` (`space_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `objects_seq`
--
CREATE TABLE `objects_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB;
INSERT INTO `objects_seq` (`id`) VALUES (100);

-- --------------------------------------------------------

--
-- Structure de la table `objects_rel`
--
CREATE TABLE `objects_rel` (
  `link_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `ext_id` int(11) NOT NULL,
  PRIMARY KEY  (`link_id`),
  UNIQUE KEY `UNIQ_objects_rel_1` (`parent_id`,`child_id`, `ext_id`),
  KEY `INDEX_objects_rel_1` (`parent_id`),
  KEY `INDEX_objects_rel_2` (`child_id`),
  KEY `INDEX_objects_rel_3` (`ext_id`)
) ENGINE=InnoDB;


-- --------------------------------------------------------

--
-- Structure de la table `acl_rules`
--

CREATE TABLE `acl_rules` (
  `privilege` int(11)  NOT NULL,
  `resource_id` int(11) NOT NULL,
  `role_id` int(11)  NOT NULL,
  `rule` varchar(32)  NOT NULL,
  UNIQUE KEY `UC_resources_acl` (`resource_id`,`role_id`,`privilege`),
  KEY `INDEX_acl_privileges_1` (`resource_id`),
  KEY `INDEX_acl_privileges_2` (`role_id`),
  KEY `INDEX_acl_privileges_3` (`privilege`),
  KEY `INDEX_acl_privileges_4` (`rule`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `checkout_index`
--

CREATE TABLE `checkout_index` (
  `file_name` varchar(128) NOT NULL default '',
  `description` varchar(128) default NULL,
  `container_type` varchar(128) NOT NULL default '',
  `container_id` int(11) NOT NULL default '0',
  `container_number` varchar(128) NOT NULL default '',
  `check_out_by` int(11) NOT NULL default '0',
  `document_id` int(11) NOT NULL default '0'
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `indices`
--

CREATE TABLE `indices` (
  `indice_id` int(11) NOT NULL default '0',
  `indice_value` varchar(8) NOT NULL default '',
  PRIMARY KEY  (`indice_id`),
  UNIQUE KEY `UC_indice_value` (`indice_value`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `doctypes`
--

CREATE TABLE `doctypes` (
  `doctype_id` int(11) NOT NULL default '0',
  `doctype_number` varchar(64) NOT NULL default '',
  `doctype_description` varchar(128) default NULL,
  `can_be_composite` tinyint(4) NOT NULL default '1',
  `file_extension` varchar(32) default NULL,
  `file_type` varchar(128) default NULL,
  `icon` varchar(32) default NULL,
  `script_post_store` varchar(64) default NULL,
  `script_pre_store` varchar(64) default NULL,
  `script_post_update` varchar(64) default NULL,
  `script_pre_update` varchar(64) default NULL,
  `recognition_regexp` text,
  `visu_file_extension` varchar(32) default NULL,
  PRIMARY KEY  (`doctype_id`),
  UNIQUE KEY `UNIQ_doctypes_1` (`doctype_number`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_number` varchar(32) NOT NULL,
  `category_description` varchar(512) default NULL,
  `category_icon` varchar(32) default NULL,
  PRIMARY KEY  (`category_id`),
  UNIQUE KEY `UNIQ_categories_1` (`category_number`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_activities`
--

CREATE TABLE `galaxia_activities` (
  `activityId` int(14) NOT NULL auto_increment,
  `name` varchar(80) default NULL,
  `normalized_name` varchar(80) default NULL,
  `pId` int(14) NOT NULL default '0',
  `type` enum('start','end','split','switch','join','activity','standalone') default NULL,
  `isAutoRouted` char(1) default NULL,
  `flowNum` int(10) default NULL,
  `isInteractive` char(1) default NULL,
  `isAutomatic` char(1) default NULL,
  `isComment` char(1) default NULL,
  `lastModif` int(14) default NULL,
  `description` text,
  `expirationTime` int(6) unsigned NOT NULL default '0',
  PRIMARY KEY  (`activityId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_activity_roles`
--

CREATE TABLE `galaxia_activity_roles` (
  `activityId` int(14) NOT NULL default '0',
  `roleId` int(14) NOT NULL default '0',
  PRIMARY KEY  (`activityId`,`roleId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_instances`
--

CREATE TABLE `galaxia_instances` (
  `instanceId` int(14) NOT NULL auto_increment,
  `pId` int(14) NOT NULL default '0',
  `started` int(14) default NULL,
  `name` varchar(200) default 'No Name',
  `owner` varchar(200) default NULL,
  `nextActivity` int(14) default NULL,
  `nextUser` varchar(200) default NULL,
  `ended` int(14) default NULL,
  `status` enum('active','exception','aborted','completed') default NULL,
  `properties` longblob,
  PRIMARY KEY  (`instanceId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_instance_activities`
--

CREATE TABLE `galaxia_instance_activities` (
  `instanceId` int(14) NOT NULL default '0',
  `activityId` int(14) NOT NULL default '0',
  `started` int(14) NOT NULL default '0',
  `ended` int(14) NOT NULL default '0',
  `user` varchar(40) default NULL,
  `status` enum('running','completed') default NULL,
  PRIMARY KEY  (`instanceId`,`activityId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_instance_comments`
--

CREATE TABLE `galaxia_instance_comments` (
  `cId` int(14) NOT NULL auto_increment,
  `instanceId` int(14) NOT NULL default '0',
  `user` varchar(40) default NULL,
  `activityId` int(14) default NULL,
  `hash` varchar(32) default NULL,
  `title` varchar(250) default NULL,
  `comment` text,
  `activity` varchar(80) default NULL,
  `timestamp` int(14) default NULL,
  PRIMARY KEY  (`cId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_processes`
--

CREATE TABLE `galaxia_processes` (
  `pId` int(14) NOT NULL auto_increment,
  `name` varchar(80) default NULL,
  `isValid` char(1) default NULL,
  `isActive` char(1) default NULL,
  `version` varchar(12) default NULL,
  `description` text,
  `lastModif` int(14) default NULL,
  `normalized_name` varchar(80) default NULL,
  PRIMARY KEY  (`pId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_roles`
--

CREATE TABLE `galaxia_roles` (
  `roleId` int(14) NOT NULL auto_increment,
  `pId` int(14) NOT NULL default '0',
  `lastModif` int(14) default NULL,
  `name` varchar(80) default NULL,
  `description` text,
  PRIMARY KEY  (`roleId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_transitions`
--

CREATE TABLE `galaxia_transitions` (
  `pId` int(14) NOT NULL default '0',
  `actFromId` int(14) NOT NULL default '0',
  `actToId` int(14) NOT NULL default '0',
  PRIMARY KEY  (`actFromId`,`actToId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_user_roles`
--

CREATE TABLE `galaxia_user_roles` (
  `pId` int(14) NOT NULL default '0',
  `roleId` int(14) NOT NULL auto_increment,
  `user` varchar(40) NOT NULL default '',
  PRIMARY KEY  (`roleId`,`user`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_workitems`
--

CREATE TABLE `galaxia_workitems` (
  `itemId` int(14) NOT NULL auto_increment,
  `instanceId` int(14) NOT NULL default '0',
  `orderId` int(14) NOT NULL default '0',
  `activityId` int(14) NOT NULL default '0',
  `properties` longblob,
  `started` int(14) default NULL,
  `ended` int(14) default NULL,
  `user` varchar(40) default NULL,
  PRIMARY KEY  (`itemId`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `rb_groups`
--

CREATE TABLE `rb_groups` (
  `group_id` int(11) NOT NULL,
  `group_define_name` varchar(32) default NULL,
  `group_description` varchar(64) default NULL,
  `is_active` tinyint(1) default '1',
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `rb_groups_uniq1` (`group_define_name`),
  KEY `INDEX_rb_groups_1` (`group_define_name`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `rb_groupusers`
--

CREATE TABLE `rb_groupusers` (
  `auth_user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  UNIQUE KEY `rb_groupusers_uniq1` (`auth_user_id`,`group_id`),
  KEY `INDEX_rb_groupusers_1` (`auth_user_id`),
  KEY `INDEX_rb_groupusers_2` (`group_id`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `rb_group_subgroups`
--

CREATE TABLE `rb_group_subgroups` (
  `group_id` int(11) NOT NULL,
  `subgroup_id` int(11) NOT NULL,
  UNIQUE KEY `rb_group_subgroups_uniq1` (`group_id`,`subgroup_id`),
  KEY `INDEX_rb_group_subgroups_1` (`group_id`),
  KEY `INDEX_rb_group_subgroups_2` (`subgroup_id`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `rb_users`
--

CREATE TABLE `rb_users` (
  `auth_user_id` int(11) NOT NULL,
  `handle` varchar(128) NOT NULL,
  `passwd` varchar(32) default NULL,
  `email` varchar(128) default NULL,
  `lastlogin` timestamp NULL default '0000-00-00 00:00:00',
  `is_active` tinyint(1) default '1',
  PRIMARY KEY  (`auth_user_id`),
  UNIQUE KEY `handle` (`handle`),
  UNIQUE KEY `auth_user_id_idx` (`auth_user_id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `messu_archive`
--

CREATE TABLE `messu_archive` (
  `msgId` int(14) NOT NULL auto_increment,
  `user` varchar(40) NOT NULL default '',
  `user_from` varchar(40) NOT NULL default '',
  `user_to` text,
  `user_cc` text,
  `user_bcc` text,
  `subject` varchar(255) default NULL,
  `body` text,
  `hash` varchar(32) default NULL,
  `replyto_hash` varchar(32) default NULL,
  `date` int(14) default NULL,
  `isRead` char(1) default NULL,
  `isReplied` char(1) default NULL,
  `isFlagged` char(1) default NULL,
  `priority` int(2) default NULL,
  PRIMARY KEY  (`msgId`)
) ENGINE=InnoDB  AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Structure de la table `messu_messages`
--

CREATE TABLE `messu_messages` (
  `msgId` int(14) NOT NULL auto_increment,
  `user` varchar(40) NOT NULL default '',
  `user_from` varchar(200) NOT NULL default '',
  `user_to` text,
  `user_cc` text,
  `user_bcc` text,
  `subject` varchar(255) default NULL,
  `body` text,
  `hash` varchar(32) default NULL,
  `replyto_hash` varchar(32) default NULL,
  `date` int(14) default NULL,
  `isRead` char(1) default NULL,
  `isReplied` char(1) default NULL,
  `isFlagged` char(1) default NULL,
  `priority` int(2) default NULL,
  PRIMARY KEY  (`msgId`)
) ENGINE=InnoDB  AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Structure de la table `messu_sent`
--

CREATE TABLE `messu_sent` (
  `msgId` int(14) NOT NULL auto_increment,
  `user` varchar(40) NOT NULL default '',
  `user_from` varchar(40) NOT NULL default '',
  `user_to` text,
  `user_cc` text,
  `user_bcc` text,
  `subject` varchar(255) default NULL,
  `body` text,
  `hash` varchar(32) default NULL,
  `replyto_hash` varchar(32) default NULL,
  `date` int(14) default NULL,
  `isRead` char(1) default NULL,
  `isReplied` char(1) default NULL,
  `isFlagged` char(1) default NULL,
  `priority` int(2) default NULL,
  PRIMARY KEY  (`msgId`)
) ENGINE=InnoDB  AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

CREATE TABLE `messu_notification` (
  `user_id` int(11) NOT NULL,
  KEY  (`user_id`)
) ENGINE=InnoDB ;

-- --------------------------------------------------------

--
-- Structure de la table `partners`
--

CREATE TABLE `partners` (
  `partner_id` int(11) NOT NULL,
  `partner_number` varchar(128) NOT NULL,
  `partner_type` enum('customer','supplier','staff') default NULL,
  `first_name` varchar(64) default NULL,
  `last_name` varchar(64) default NULL,
  `adress` varchar(512) default NULL,
  `city` varchar(64) default NULL,
  `zip_code` int(11) default NULL,
  `phone` varchar(64) default NULL,
  `cell_phone` varchar(64) default NULL,
  `mail` varchar(64) default NULL,
  `web_site` varchar(64) default NULL,
  `activity` varchar(64) default NULL,
  `company` varchar(64) default NULL,
  PRIMARY KEY  (`partner_id`),
  UNIQUE KEY `UC_partner_number` (`partner_number`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL,
  `project_number` varchar(128) NOT NULL,
  `project_description` varchar(512) default NULL,
  `project_state` varchar(32) NOT NULL default 'init',
  `project_indice_id` int(11) default NULL,
  `default_process_id` int(11) default NULL,
  `open_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `close_date` int(11) default NULL,
  PRIMARY KEY  (`project_id`),
  UNIQUE KEY `UC_project_number` (`project_number`),
  KEY `INDEX_projects_1` (`project_indice_id`),
  KEY `INDEX_projects_2` (`project_description`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `project_history`
--

CREATE TABLE `project_history` (
  `histo_order` int(11) NOT NULL,
  `action_name` varchar(32) NOT NULL,
  `action_by` varchar(32) NOT NULL,
  `action_date` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `project_number` varchar(16)  default NULL,
  `project_description` varchar(128) default NULL,
  `project_state` varchar(16)  default NULL,
  `project_indice_id` int(11) default NULL,
  `default_process_id` int(11) default NULL,
  `open_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `close_date` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `tiki_user_tasks`
--

CREATE TABLE `tiki_user_tasks` (
  `task_id` int(14) NOT NULL auto_increment,
  `last_version` int(4) NOT NULL default '0',
  `user` varchar(40) NOT NULL default '',
  `creator` varchar(200) NOT NULL default '',
  `public_for_group` int(11) default NULL,
  `rights_by_creator` char(1) default NULL,
  `created` int(14) NOT NULL default '0',
  `status` char(1) default NULL,
  `priority` int(2) default NULL,
  `completed` int(14) default NULL,
  `percentage` int(4) default NULL,
  PRIMARY KEY  (`task_id`),
  UNIQUE KEY `creator` (`creator`,`created`),
  UNIQUE KEY `creator_2` (`creator`,`created`)
) ENGINE=InnoDB  AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `tiki_user_tasks_history`
--

CREATE TABLE `tiki_user_tasks_history` (
  `belongs_to` int(14) NOT NULL default '0',
  `task_version` int(4) NOT NULL default '0',
  `title` varchar(250) NOT NULL default '',
  `description` text,
  `start` int(14) default NULL,
  `end` int(14) default NULL,
  `lasteditor` varchar(200) NOT NULL default '',
  `lastchanges` int(14) NOT NULL default '0',
  `priority` int(2) NOT NULL default '3',
  `completed` int(14) default NULL,
  `deleted` int(14) default NULL,
  `status` char(1) default NULL,
  `percentage` int(4) default NULL,
  `accepted_creator` char(1) default NULL,
  `accepted_user` char(1) default NULL,
  PRIMARY KEY  (`belongs_to`,`task_version`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `user_prefs`
--

CREATE TABLE `user_prefs` (
  `user_id` int(11) NOT NULL,
  `css_sheet` varchar(32)  NOT NULL default 'default',
  `lang` varchar(32)  NOT NULL default 'default',
  `long_date_format` varchar(32)  NOT NULL default 'default',
  `short_date_format` varchar(32)  NOT NULL default 'default',
  `date_input_method` varchar(16)  NOT NULL default 'default',
  `hour_format` varchar(32)  NOT NULL default 'default',
  `time_zone` varchar(32)  NOT NULL default 'default',
  `wildspace_path` varchar(128)  NOT NULL default 'default',
  `max_record` varchar(128)  NOT NULL default 'default',
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB;


-- ------------------------------------------------------------------------------
CREATE TABLE `metadata_dictionary` (
  `property_id` int(11) NOT NULL,
  `property_fieldname` varchar(32) NOT NULL default '',
  `property_name` varchar(32) NOT NULL default '',
  `property_description` varchar(128) NOT NULL default '',
  `property_type` varchar(32) NOT NULL default '',
  `property_length` int(11) default NULL,
  `extend_table` varchar(64) NOT NULL,
  `regex` varchar(255) default NULL,
  `return_name` int(1) NOT NULL default '0',
  `is_required` int(1) NOT NULL default '0',
  `is_multiple` int(1) NOT NULL default '0',
  `is_hide` int(1) NOT NULL default '0',
  `select_list` varchar(255) default NULL,
  `selectdb_where` varchar(255) default NULL,
  `selectdb_table` varchar(32) default NULL,
  `selectdb_field_for_value` varchar(32) default NULL,
  `selectdb_field_for_display` varchar(32) default NULL,
  `date_format` varchar(32) default NULL,
  `display_both` tinyint(1) NOT NULL default 0,
  PRIMARY KEY (`property_id`),
  UNIQUE KEY `UNIQ_metadata_dictionary_1` (`property_name`,`extend_table`),
  KEY `INDEX_metadata_dictionary_1` (`extend_table`),
  KEY `INDEX_metadata_dictionary_2` (`property_description`)
) ENGINE=InnoDB;


-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
CREATE TABLE `reposits` (
  `reposit_id` int(11) NOT NULL,
  `reposit_number` varchar(32) NOT NULL,
  `reposit_name` varchar(64) NOT NULL,
  `reposit_description` text,
  `reposit_type` int(1) default 1,
  `reposit_mode` int(1) default 1,
  `reposit_url` varchar(256) NOT NULL,
  `is_active` int(1) default 0,
  `priority` int(1) default 0,
  `open_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `space_id` int(11) default 0,
  PRIMARY KEY  (`reposit_id`),
  UNIQUE KEY `UNIQ_reposits_1` (`reposit_url`(256)),
  KEY `INDEX_reposits_1` (`reposit_number`),
  KEY `INDEX_reposits_2` (`is_active`),
  KEY `INDEX_reposits_3` (`priority`),
  KEY `INDEX_reposits_4` (`space_id`),
  KEY `INDEX_reposits_5` (`reposit_type`)
) ENGINE=InnoDB;


-- --------------------------------------------------------

--
-- Structure de la table `import_history`
--
CREATE TABLE `import_history` (
  `import_order` int(11) NOT NULL default '0',
  `container_id` int(11) NOT NULL default '0',
  `description` varchar(64) default NULL,
  `state` varchar(32) default NULL,
  `import_date` int(11) default NULL,
  `import_by` int(11) default NULL,
  `errors_report` text,
  `logfile` text,
  `listfile` text,
  `file_name` varchar(128) NOT NULL default '',
  `file_extension` varchar(32) default NULL,
  `file_mtime` int(11) default NULL,
  `file_size` int(11) default NULL,
  `file_md5` varchar(128) NOT NULL default '',
  PRIMARY KEY  (`import_order`)
) ENGINE=InnoDB;


-- --------------------------------------------------------

--
-- Structure de la table `tickets`
--

CREATE TABLE `tickets` (
  `ticket_id` text(40) NOT NULL,
  `expiration_date` int(11) NOT NULL,
  `for_object_id` int(11) NOT NULL,
  KEY `INDEX_tickets_1` (`ticket_id` (20), `for_object_id`),
  KEY `INDEX_tickets_2` (`expiration_date`)
) ENGINE=InnoDB;


--
-- Contraintes pour la table `import_history`
--
ALTER TABLE `import_history`
  ADD CONSTRAINT `FK_import_history_1` FOREIGN KEY (`import_order`)
  REFERENCES `objects` (`object_id`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `FK_import_history_2` FOREIGN KEY (`import_by`)
  REFERENCES `rb_users` (`auth_user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_import_history_3` FOREIGN KEY (`container_id`)
  REFERENCES `objects` (`object_id`) ON UPDATE CASCADE ON DELETE CASCADE;

--
-- Contraintes pour la table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `FK_projects_1` FOREIGN KEY (`project_id`)
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_projects_2` FOREIGN KEY (`project_indice_id`)
  REFERENCES `indices` (`indice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_projects_3` FOREIGN KEY (`default_process_id`)
  REFERENCES `galaxia_processes` (`pId`) ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `user_prefs`
  ADD CONSTRAINT `FK_user_prefs_1` FOREIGN KEY (`user_id`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `rb_groupusers`
  ADD CONSTRAINT `FK_rb_groupusers_1` FOREIGN KEY (`auth_user_id`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_rb_groupusers_2` FOREIGN KEY (`group_id`) 
  REFERENCES `rb_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `rb_group_subgroups`
  ADD CONSTRAINT `FK_rb_group_subgroups_1` FOREIGN KEY (`group_id`) 
  REFERENCES `rb_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_rb_group_subgroups_2` FOREIGN KEY (`subgroup_id`) 
  REFERENCES `rb_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `messu_notification` 
  ADD CONSTRAINT `FK_messu_notification_1` FOREIGN KEY (`user_id`) 
  REFERENCES `rb_users` (`auth_user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `doctypes` 
  ADD CONSTRAINT `FK_doctypes_1` FOREIGN KEY (`doctype_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `partners` 
  ADD CONSTRAINT `FK_partner_1` FOREIGN KEY (`partner_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `metadata_dictionary` 
  ADD CONSTRAINT `FK_metadata_dictionary_1` FOREIGN KEY (`property_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `objects_rel`
  ADD CONSTRAINT `FK_objects_rel_1` FOREIGN KEY (`parent_id`)
    REFERENCES `objects` (`object_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_objects_rel_2` FOREIGN KEY (`child_id`)
    REFERENCES `objects` (`object_id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `%container%_categories`
--
ALTER TABLE `categories` 
  ADD CONSTRAINT `FK_categories_1` FOREIGN KEY (`category_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reposits`
--
ALTER TABLE `reposits`
  ADD CONSTRAINT `FK_reposits_1` FOREIGN KEY (`open_by`)
  REFERENCES `rb_users` (`auth_user_id`) ON UPDATE CASCADE;

