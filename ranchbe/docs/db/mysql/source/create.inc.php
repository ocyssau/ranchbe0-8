<?php

function create_db($lang, $dbname, $connect, $drop){
	echo 'try to create db ' . $dbname  . "\n\r";
	
	if($drop){
		echo 'delete db ' . $dbname . "\n\r";
		$sql = 'DROP DATABASE IF EXISTS `'.$dbname.'`;';
		mysql_query($sql) or die('SQL Error!' . $sql . "\n\r" . mysql_error());
	}
	
	$sql = 'CREATE DATABASE IF NOT EXISTS `'.$dbname.'` DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;';
	mysql_query($sql) or die('SQL Error!' . $sql . "\n\r" . mysql_error());
	
	mysql_select_db($dbname)or die (mysql_error());
	
	$dbFile = "../ranchbe_$lang.sql";
	if ( file_exists($dbFile) ) {
	 	$file = fopen($dbFile, 'r');
	  	$data='';
	  	$i = 0;
		while(!feof($file)){
		    $var=fread($file,1);
		    if ($var!=';'){
		    	$data.= $var;
		    }else{
		    	$data.= $var;
		    	mysql_query( $data ) or die( mysql_error() );
		    	$data = '';
		    }
		    if($i > 100) {
		    	echo '.';
		    	$i = 0;
		    }
		    $i++;
		}		
	}else{
		echo "The data base file $dbFile dont exist";
		die;
	}
	
	echo "Success \n\r";

}

function init_rbdbuser($dbname, $dbuser, $dbpasswd, $host='%'){
	$sql = "CREATE USER '$dbuser'@'$host' IDENTIFIED BY '$dbpasswd'";
	if(!mysql_query($sql)){
		echo mysql_error() ."\n\r";
		echo "User is probably existing \n\r";
	}
		
	$sql =  'GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, ALTER, INDEX, DROP, CREATE TEMPORARY TABLES, CREATE VIEW, SHOW VIEW, EXECUTE ';
	$sql .= "ON `$dbname` . * TO '$dbuser'@'$host';";
	mysql_query($sql) or die( 'SQL Error!' . $sql . "\n\r" . mysql_error() );
}

function init_consultdbuser($dbname, $dbuser, $dbpasswd, $host='%'){
	$sql = "CREATE USER '$dbuser'@'$host' IDENTIFIED BY '$dbpasswd'";
	if(!mysql_query($sql)){
		echo mysql_error() ."\n\r";
		echo "User is probably existing \n\r";
	}
	
	$sql = 'GRANT SELECT ';
	$sql .= "ON `$dbname` . * TO '$dbuser'@'$host';";
	mysql_query($sql) or die( 'SQL Error!' . $sql . "\n\r" . mysql_error() );
	
}
