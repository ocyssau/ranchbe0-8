SELECT workitem_doc_files_iterations.file_id, workitem_doc_files_iterations.father_id 
 FROM workitem_doc_files_iterations
 LEFT JOIN workitem_doc_files
 ON workitem_doc_files.file_id = workitem_doc_files_iterations.father_id 
 WHERE workitem_doc_files.file_id IS NULL;

