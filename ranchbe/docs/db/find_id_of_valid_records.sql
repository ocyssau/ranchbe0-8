SELECT file_id,file_access_code
FROM   workitem_doc_files
WHERE file_name = 'Derive.CATPart'
GROUP  BY file_name
HAVING file_access_code = MAX(file_access_code)

SELECT file_id,MAX(file_access_code) AS MAC
FROM   workitem_doc_files
WHERE file_name = 'Derive.CATPart'
GROUP  BY file_name
HAVING file_access_code = MAX(file_access_code)

SELECT file_id,file_access_code
FROM   workitem_doc_files
WHERE file_name = 'Derive.CATPart'
GROUP  BY file_access_code
HAVING MIN(file_access_code)

SELECT DISTINCT file_id,file_access_code
FROM   workitem_doc_files
WHERE file_name = 'Derive.CATPart'
HAVING MIN(file_access_code)

SELECT DISTINCT file_id,file_access_code
FROM   workitem_doc_files
WHERE file_name = 'Derive.CATPart'


retrouver le id d'un docfile dont le nom = nom1 et dont le code d'acc�s est aussi petit
que possible.

OPTION 1: Avec sous requ�te :

SELECT file_id
FROM   workitem_doc_files
WHERE file_name = 'Derive.CATPart'
AND
file_access_code =
(SELECT MAX(file_access_code) AS MAC
FROM   workitem_doc_files
WHERE file_name = 'Derive.CATPart'
GROUP  BY file_name)


OPTION 2 : PLUS LENT AVEC IN

SELECT file_id,file_access_code FROM workitem_doc_files
WHERE (file_access_code, file_name) IN
(SELECT MIN(file_access_code), file_name
FROM workitem_doc_files
WHERE file_name = 'Derive.CATPart'
GROUP BY file_name)




