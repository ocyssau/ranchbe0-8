UPDATE workitem_doc_files
SET file_path = REPLACE(`file_path`, '/indices/', '/__indices/');

UPDATE bookshop_doc_files
SET file_path = REPLACE(`file_path`, '/indices/', '/__indices/');

UPDATE cadlib_doc_files
SET file_path = REPLACE(`file_path`, '/indices/', '/__indices/');

UPDATE mockup_doc_files
SET file_path = REPLACE(`file_path`, '/indices/', '/__indices/');
