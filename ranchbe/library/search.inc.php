<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Ranchbe is distributed in the hope that it will be useful,                |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('conf/ranchbe_setup.php');//ranchBE configuration

function combine($array , $string){
	foreach( $array as $val){
		$res[] = "$string" . "$val";
	}
	return $res;
}

function rcombine($array , $ctype='workitems', $replace='cont'){ //Add the as close ex :'mockup_number as container_number'
	foreach( $array as $val){
		$as = str_replace($ctype , $replace , $val);
		$res[] = "$val" . ' as ' .$replace.'_'.$as;
	}
	return $res;
}

function array_replace($search , $replace , $in_array){
	foreach( $in_array as $val){
		$res[] = str_replace($search, $replace , $val);
	}
	return $res;
}

//-----------------------------------------------------------------------------

function search($params){
	/*Parameters to set :
	 $params['doctype_id'] //array
	 $params['container_type'] //array
	 $params['document_number'] //varchar
	 $params['designation'] //varchar
	 $params['document_state'] //array
	 $params['document_indice_id'] //array
	 $params['document_version'] //array
	 $params['open_by'] //array
	 $params['update_by'] //array
	 $params['check_out_by'] //array
	 $params['open_date_min'] //int
	 $params['open_date_max'] //int
	 $params['update_date_min'] //int
	 $params['update_date_max'] //int
	 $params['check_out_date_min'] //int
	 $params['check_out_date_max'] //int

	 $params['select'] //array
	 $params['selectd'] //array to define fields to display for docuemts
	 $params['selectc'] //array to define fields to display for containers
	 $params[offset]     = ;
	 $params[maxRecords] = ;
	 $params[sort_field] = ;
	 $params[sort_order] = ;
	 $params['container_type']

	 /*
	 echo '<pre>';
	 var_dump($params);
	 echo '</pre>';
	 */

	$offset = $params['offset'];
	$numrows = $params['numrows'];
	$sort_field = $params['sort_field'];
	$sort_order = $params['sort_order'];
	$selectd  = $params['selectd'];
	$selectc  = $params['selectc'];
	$container_type = $params['container_type'];

	unset($params['offset']);
	unset($params['numrows']);
	unset($params['sort_field']);
	unset($params['sort_order']);
	unset($params['submit']);
	unset($params['container_type']);
	unset($params['selectd']);
	unset($params['selectc']);

	global $Manager;

	//Construct the where close for dates
	if(!empty($params['open_date_min']) && $params['open_date_min_sel'])
	$whereClose[] = '(doc.open_date > '.$params['open_date_min'].')';
	if(!empty($params['open_date_max']) && $params['open_date_max_sel'])
	$whereClose[] = '(doc.open_date > '.$params['open_date_max'].')';

	if(!empty($params['update_date_min']) && $params['update_date_min_sel'])
	$whereClose[] = '(doc.update_date > '.$params['update_date_min'].')';
	if(!empty($params['update_date_max']) && $params['update_date_max_sel'])
	$whereClose[] = '(doc.update_date > '.$params['update_date_max'].')';

	if(!empty($params['checkout_date_min']) && $params['checkout_date_min_sel'])
	$whereClose[] = '(doc.check_out_date > '.$params['checkout_date_min'].')';
	if(!empty($params['checkout_date_max']) && $params['checkout_date_max_sel'])
	$whereClose[] = '(doc.check_out_date > '.$params['checkout_date_max'].')';

	unset($params['open_date_min']);
	unset($params['open_date_max']);
	unset($params['update_date_min']);
	unset($params['update_date_max']);
	unset($params['checkout_date_min']);
	unset($params['checkout_date_max']);

	unset($params['open_date_min_sel']);
	unset($params['open_date_max_sel']);
	unset($params['update_date_min_sel']);
	unset($params['update_date_max_sel']);
	unset($params['checkout_date_min_sel']);
	unset($params['checkout_date_max_sel']);

	foreach($params as $field_name=>$field_value){
		if (is_array($field_value)){
			$Where = '';
			foreach($field_value as $val){
				if(!empty($val))
				$Where[] = "doc.$field_name LIKE '%$val%'";
			}
			if(!empty($Where))
			$whereClose[] = '(' .implode(' OR ', $Where) . ')';
		}else{
			if (!empty($field_name) && !empty($field_value)){
				$whereClose[] = "(doc.$field_name LIKE '%$field_value%')";
			}
		}
	}
	//var_dump($whereClose);return false;

	//Construct the where close
	if (!empty( $whereClose ) ) {
		$whereClose = ' WHERE ' . implode(' AND ', $whereClose);
	}
	//var_dump($whereClose);//return false;

	//Construct the select close definition for document
	if (!empty($selectd)){
		if(!in_array('document_number' , $selectd)) $selectd[] = 'document_number'; //Add the default fields to display.
		if(!in_array('doctype_id' , $selectd)) $selectd[] = 'doctype_id'; //Add the default fields to display. Necessary for display icon
		if(!in_array('document_id' , $selectd)) $selectd[] = 'document_id'; //Add the default fields to display. Necessary for display document
	}else{
		$selectd[] = 'document_number';
		$selectd[] = 'doctype_id';
		$selectd[] = 'document_id';
	}
	$select = $selectd;
	$selectd = combine($selectd , 'doc.');
	$selectdClose = implode(' , ' , $selectd);
	//var_dump($selectdClose);return false;

	//Construct the select close definition for container
	if (!empty($selectc)){
		if(!in_array('%CONT%_number' , $selectc)) $selectc[] = '%CONT%_number';
		if(!in_array('container_type' , $selectc)) $selectc[] = 'container_type'; //Add the default fields to display. Necessary for display document
	}else{
		$selectc[] = '%CONT%_number';
		$selectc[] = 'container_type';
	}
	//var_dump($selectc);return false; //= array(1){[0]=>string(13)"%CONT%_number"}

	//Construct a query for each container type...
	if(!is_array($container_type))
	$container_type = array('cadlib','bookshop','mockup','workitem');
	foreach($container_type as $ctype){
		$tmpselectc = array_replace('%CONT%' , $ctype , $selectc); //output exemple: array(1){[0]=> string(13) "cadlib_number"}//define the number field name for the container type ctype
		$tmpselectc = rcombine($tmpselectc , $ctype); //output exemple: array(1) { [0]=>  string(33) "cadlib_number as cont_cont_number" }//Add an aliase as 'cont_number' to number field name
		$tmpselectc = combine($tmpselectc , 'cont.'); //output exemple: array(1) { [0]=>  string(38) "cont.cadlib_number as cont_cont_number" }//Add the 'cont.' suffix
		$selectcClose = implode(' , ' , $tmpselectc);
		$container_id = $ctype.'_id';
		$containers = $ctype.'s as cont';
		$container_doc = $ctype.'_documents as doc';
		$query[] = 'SELECT '.$selectdClose.' , '.$selectcClose. ' FROM ' .$container_doc. ' JOIN ' .$containers. ' ON cont.' .$container_id. ' = doc.' .$container_id . $whereClose;
	}
	foreach( $selectc as $val){
		$val = str_replace('%CONT%', 'cont' , $val);
		$selectcc[] = 'cont_'.$val;
	}
	$select = array_merge($select , $selectcc);
	$smarty->assign('select' , $select);

	//...and UNION it
	$query = implode(' UNION ', $query);
	$query .= ' ORDER BY document_number ASC';

	//Define limit and offset
	if (isset($numrows) && !empty($numrows) && ($numrows <= 1000))
	$limit = $numrows;
	else $limit = '1000';
	if (!isset($offset))
	$offset = '0';

	//echo $query;

	if(!$Rset = Ranchbe::getDb()->SelectLimit( $query , ($limit + 1) , $offset) ){
		print 'error in query: '.Ranchbe::getDb()->ErrorMsg().'<br />';
		echo $query . '<br />';
		return false;
	}else{
		$CountOutput = $Rset->RecordCount();
		if($CountOutput >= $limit){
			global $search_return;
			$search_return = 'The max return of rows is reached('.$limit.'). Please, review your request.';
		}
		while ($row = $Rset->FetchRow()) {
			$All[] = $row;
		}

		/*$Rset = $Rset->GetArray(); //To transform result in array;
		 foreach($All as $key=>$document){
		 //$All["$key"]['doctype_id_id'] = $document['doctype_id'];
		 //$All["$key"]['doctype_id'] = doctype($document['doctype_id']);
		 $All["$key"]['cont_cont_indice_id'] = container_indice($document['cont_cont_indice_id']);
		 $All["$key"]['cont_open_date'] = Rb_Date::formatDate($document['cont_open_date']);
		 $All["$key"]['cont_close_date'] = Rb_Date::formatDate($document['cont_close_date']);
		 $All["$key"]['cont_forseen_close_date'] = Rb_Date::formatDate($document['cont_forseen_close_date']);
		 $All["$key"]['cont_open_by'] = username($document['cont_open_by']);
		 $All["$key"]['cont_close_by'] = username($document['cont_close_by']);
		 $All["$key"]['cont_default_process_id'] = process($document['cont_default_process_id']);
		 $All["$key"]['document_indice_id'] = document_indice($document['document_indice_id']);
		 $All["$key"]['category_id'] = category($document['category_id'] , $document['cont_cont_id']);
		 $All["$key"]['check_out_by'] = username($document['check_out_by']);
		 $All["$key"]['check_out_date'] = Rb_Date::formatDate($document['check_out_date']);
		 $All["$key"]['update_by'] = username($document['update_by']);
		 $All["$key"]['open_date'] = Rb_Date::formatDate($document['open_date']);
		 $All["$key"]['update_date'] = Rb_Date::formatDate($document['update_date']);
		 $All["$key"]['open_by'] = username($document['open_by']);
		 //$All["$key"]['doctype_id'] = typeName($document['doctype_id']);
		 //$All["$key"]['document_indice_id'] = indiceName($document['document_indice_id']);
		 //$All["$key"]['category_id'] = categoryName($document['category_id'] , $document['cont_cont_id']);
		 //$All["$key"]['check_out_by'] = userName($document['check_out_by']);
		 //$All["$key"]['check_out_date'] = Rb_Date::formatDate($document['check_out_date']);
		 //$All["$key"]['update_by'] = userName($document['update_by']);
		 //$All["$key"]['open_date'] = Rb_Date::formatDate($document['open_date']);
		 //$All["$key"]['update_date'] = Rb_Date::formatDate($document['update_date']);
		 //$All["$key"]['open_by'] = userName($document['open_by']);
		 }*/

		return $All;

	}

} //End of function search
