<?php

function category($category_id='0')
{
  global $Manager; 
  $TABLE = $Manager->OBJECT_TYPE.'_categories';  
  $query = "SELECT category_number FROM $TABLE WHERE category_id = '$category_id'";
  if(!$number = Ranchbe::getDb()->GetOne($query)){
    return 'undefined';
  }else{
    return $number;
  }
}

//------------------------------------------------------------------------------
function document_indice($indice_id='0')
{
  $query = "SELECT indice_value FROM document_indice WHERE document_indice_id = '$indice_id'";
  if(!$number = Ranchbe::getDb()->GetOne($query)){
    return 'undefined';
  }else{
    return $number;
  }
}

//------------------------------------------------------------------------------
function process($process_id='0')
{
  $query = "SELECT name FROM galaxia_processes WHERE pId = '$process_id'";
  if(!$number = Ranchbe::getDb()->GetOne($query)){
    return 'undefined';
  }else{
    return $number;
  }
}

//------------------------------------------------------------------------------
function project($project_id='0')
{
  $query = "SELECT project_number FROM projects WHERE project_id = '$project_id'";
  if(!$number = Ranchbe::getDb()->GetOne($query)){
    return 'undefined';
  }else{
    return $number;
  }
}

//------------------------------------------------------------------------------
function supplier($supplier_id='0')
{
  $query = "SELECT partner_number FROM partners WHERE partner_id = '$supplier_id'";
  if(!$number = Ranchbe::getDb()->GetOne($query)){
    return 'undefined';
  }else{
    return $number;
  }
}

//------------------------------------------------------------------------------
function type($type_id='0')
{
  $query = "SELECT doctype_number FROM doctypes WHERE doctype_id = '$type_id'";
  if(!$number = Ranchbe::getDb()->GetOne($query)){
    return 'undefined';
  }else{
    return $number;
  }
}

//------------------------------------------------------------------------------
function username($user_id='0')
{
  include_once './lib/userslib.php';
  $userlib = new UsersLib();
  $username = $userlib->get_user_name($user_id);
	return $username;
}

//------------------------------------------------------------------------------
function container($container_id='0')
{
  global $Manager;
  $map_id = $Manager->OBJECT_TYPE.'_id';
  if($container_id !== 0){
    $query = "SELECT $Manager->FIELDS_MAP_NUM FROM $Manager->OBJECT_TABLE WHERE $map_id = '$container_id'";
    if(!$number = Ranchbe::getDb()->GetOne($query)){
      return 'undefined';
    }else{
      return $number;
    }
  }
}


?>
