<?php

class ZendRb_Controller_Plugin_DojoLayer extends Zend_Controller_Plugin_Abstract{
	protected $_build;
	protected $_layerScript;
	protected $_profileScript;
	protected $_view;
	
	public function setView( $view ){
		$this->_view = $view;
	}
	
	public function setLayerScript( $script ){
		$this->_layerScript = $script;
	}
	
	public function setProfileScript( $script ){
		$this->_profileScript = $script;
	}
	
	public function getLayerScript( ){
		return $this->_layerScript;
	}
	
	public function dispatchLoopShutdown(){
		$this->generateDojoLayer();
	}

	public function getBuild(){
		/*
		$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper(
                  'ViewRenderer'
                  );
        $viewRenderer->view;
        */
		if (null === $this->_build) {
			$this->_build = new Zend_Dojo_BuildLayer(array(
				'view'      => $this->_view,
				'layerName' => 'custom.main',
				'consumeOnLoad' => true,
				'consumeJavascript' => true,
			));
		}
		return $this->_build;
	}

	public function generateDojoLayer(){
		$build = $this->getBuild();
		$profile   = $build->generateBuildProfile();
		if (!is_dir(dirname($this->_profileScript))) {
			mkdir( dirname($this->_profileScript) );
		}
		file_put_contents($this->_profileScript, $profile);
		$layerContents = $build->generateLayerScript();
		if (!is_dir(dirname($this->_layerScript))) {
			mkdir( dirname($this->_layerScript) );
		}
		file_put_contents($this->_layerScript, $layerContents);
	}

}


