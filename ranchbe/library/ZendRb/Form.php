<?php

class ZendRb_Form extends Zend_Dojo_Form{
	/**
	 * (non-PHPdoc)
	 * @see external/Zend/Zend_Form#init()
	 */
	public function init(){
		$this->addPrefixPath('ZendRb_Form_Element_',
								'ZendRb/Form/Element/', 
								'element');
		
		$this->addPrefixPath('ZendRb_Form_Decorator_',
								'ZendRb/Form/Decorator/', 
								'decorator');
		
		$this->addElementPrefixPath('ZendRb_Form_Decorator_',
		                            'ZendRb/Form/Decorator/',
		                            'decorator');
		
		//Decorator: Loop on elements
		//$this->addDecorator('FormElements');
		//Decorator: default render
		//$this->addDecorator('HtmlTag', array('tag' => 'dl', 'class' => 'rb_form'));
		//Decorator: default decorator
		//$this->addDecorator('DijitForm');
		//$this->getView();
		//$script="document.unonload=window.parent.opener.location.reload();";
        //$this->getView()->headScript()->appendScript($script);
		$this->setMethod('post');
		$this->setAttrib('id', 'editform');
		$this->setAttrib('name', 'editform');
		$this->addElement('SubmitButton', 'save', array(
											'label' => tra('Save'), 
											'order'=>1000));
		
		/*
		$this->setElementDecorators(array(
			    array('ViewHelper'),
			    array('Errors'),
			    array('Description', array('tag' => 'p', 'class' => 'description')),
				array('HtmlTag' , array('tag'=>'td')),
				));
		*/
	}

	/** Add a cancel button
	 * 
	 * @return self
	 */
	public function addCancel(){
		$this->addElement('SubmitButton', 'cancel', array(
											'label' => tra('Cancel'), 
											'order'=>1010));
		return $this;
	}
}

