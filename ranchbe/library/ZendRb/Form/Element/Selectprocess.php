<?php
class ZendRb_Form_Element_Selectprocess extends ZendRb_Form_Element_Multiselect{

	/**
	 * 
	 * @param Rb_Process $source
	 * @return ZendRb_Form_Element_SelectProcess
	 */
	public function setSource(){
		require_once('Galaxia/Monitor/Process.php');
		$processMonitor = new Galaxia_Monitor_Process ( Ranchbe::getDb() );
		$processList = $processMonitor->monitor_list_processes ( 0, 9999, 'lastModif_desc', '', $options ['selectdb_where'] );
		$processSelectSet = array();
		return $this->setOptionsList($processList['data'], 'pId', 'normalized_name');
	} //End of method

}//End of class


