<?php
class ZendRb_Form_Element_Selectread extends ZendRb_Form_Element_Multiselect{

	/**
	 * 
	 * @param Rb_Space $source
	 * @return ZendRb_Form_Element_SelectRead
	 */
	public function setSource(Rb_Space $source = null){
		$id_key = Rb_Reposit_Read::get ()->getDao ()->getFieldName ( 'id' );
		$number_key = Rb_Reposit_Read::get ()->getDao ()->getFieldName ( 'number' );
		$p = array ('sort_field' => $number_key, 
					'sort_order' => 'ASC', 
					'select' => array ($id_key, $number_key, 'reposit_name' ),
					'exact_find'=>  array ('reposit_type' => 2),
		);
		if($source){
			$p['exact_find']['space_id'] = $source->getId();
		}
		$optionsList = Rb_Reposit_Read::get ()->getAll ( $p );
		return $this->setOptionsList($optionsList, $id_key, $number_key);
	}
}


