<?php

/** Submit with default decorator for rb
 * 
 * @author Olivier CYSSAU
 *
 */
class ZendRb_Form_Element_Submit extends Zend_Form_Element_Submit{
	/**
	 * (non-PHPdoc)
	 * @see external/Zend/Form/Element/Zend_Form_Element_Submit#loadDefaultDecorators()
	 */
    public function loadDefaultDecorators(){
        if ($this->loadDefaultDecoratorsIsDisabled()) {
            return;
        }

        $decorators = $this->getDecorators();
        if (empty($decorators)) {
            //$this->addDecorator('Tooltip');
            $this->addDecorator('ViewHelper');
            //$this->addDecorator('DtDdWrapper');
        }
    }
}
