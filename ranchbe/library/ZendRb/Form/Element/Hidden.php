<?php
/**
 * This hidden element reset decorator to strict minimum
 * to not display hidden element in form
 *
 */
class ZendRb_Form_Element_Hidden extends Zend_Form_Element_Hidden
{
    /**
     * Use formHidden view helper by default
     * @var string
     */
    public $helper = 'formHidden';
    
    public function init(){
    	$this->setDisableLoadDefaultDecorators(true);
    	$this->setDecorators(array('ViewHelper'));
    }
}
