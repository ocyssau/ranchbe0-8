<?php
/**
 * This hidden element reset decorator to strict minimum
 * to not display hidden element in form
 *
 */
class ZendRb_Form_Element_Multihidden extends Zend_Form_Element_Multi
{
    /**
     * Use formHidden view helper by default
     * @var string
     */
    public $helper = 'formHidden';
    
    /**
     * (non-PHPdoc)
     * @see external/Zend/Form/Zend_Form_Element#init()
     */
    public function init(){
    	$this->setDisableLoadDefaultDecorators(true);
    	//$this->setDecorators(array('ViewHelper'));
    }

	/**
	 * (non-PHPdoc)
	 * @see external/Zend/Form/Zend_Form_Element#render($view)
	 */
    public function render(Zend_View_Interface $view = null)
    {
    	$view = $this->getView();
        if (null === $view) {
            require_once 'Zend/Form/Decorator/Exception.php';
            throw new Zend_Form_Decorator_Exception('ViewHelper decorator cannot render without a registered view object');
        }
    	$helperObject = $view->getHelper($this->helper);
    	$content = array();
    	foreach($this->getMultiOptions() as $value){
    		$content[] = $helperObject->formHidden($this->getName().'[]', $value, array('id'=>$this->getName().'-'.$value));
    	}
        return implode(' ', $content);
    }
    
	/**
	 * (non-PHPdoc)
	 * @see external/Zend/Form/Element/Zend_Form_Element_Multi#isValid($value, $context)
	 */
    public function isValid($value, $context = null){
    	return true;
    }
    
}
