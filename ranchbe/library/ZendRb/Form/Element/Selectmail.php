<?php
class ZendRb_Form_Element_Selectmail extends ZendRb_Form_Element_Multiselect{
	
	/**
	 * (non-PHPdoc)
	 * @see external/Zend/Form/Zend_Form_Element#init()
	 */	
	public function init(){
		//$this->setRegexp('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}');
	}
	
	/**
	 * @param
	 * @return ZendRb_Form_Element_SelectUser
	 */
	public function setSource(){
		$list = Ranchbe::getAuthAdapter ()->getUsers ();
		return $this->setOptionsList($list, 'email', 'email');
	}
}


