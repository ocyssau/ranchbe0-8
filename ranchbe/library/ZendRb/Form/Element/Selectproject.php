<?php
class ZendRb_Form_Element_Selectproject extends ZendRb_Form_Element_Multiselect{

	/**
	 * 
	 * @param Rb_Project $source
	 * @return ZendRb_Form_Element_SelectProject
	 */
	public function setSource(){
		return parent::setSource(Rb_Project::get());
	}

}
