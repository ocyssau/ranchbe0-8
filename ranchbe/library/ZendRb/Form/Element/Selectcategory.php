<?php
class ZendRb_Form_Element_Selectcategory extends ZendRb_Form_Element_Multiselect{

	/**
	 * 
	 * @param Rb_Object_Permanent $source
	 * @return ZendRb_Form_Element_SelectCategory
	 */
	public function setSource(Rb_Object_Permanent $source){
		$options = array();
		if (is_a ( $source, 'Rb_Container' )) {
			$categories = Rb_Container_Relation_Category::get ()->getCategories ( $source->getId (), $options );
		} else if (is_a ( $source, 'Rb_Document' )) {
			$categories = Rb_Doctype_Relation_Category::get ()->getCategories ( $source->getProperty ( 'doctype_id' ), $options );
		} else if (is_a ( $source, 'Rb_Doctype' )) {
			$categories = Rb_Doctype_Relation_Category::get ()->getCategories ( $source->getId (), $options );
		} else if (is_a ( $source, 'Rb_Category' )) {
			$categories = $source->getAll ( $options );
		} else
			$categories = array ();
		return $this->setOptionsList($categories, 'category_id', 'category_number');
	}
	
}


