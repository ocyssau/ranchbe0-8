<?php
/**
 * This extend class of Zend_Form_Element_Select is equivalent to Zend_Form_Element_Multiselect
 * with capability to set the return type to array just be setMultiple(true|false) use
 * There is too method to simplify creation of select element from Rb_Permanent_Object
 */
//Zend_Form_Element_Select
//Zend_Dojo_Form_Element_ComboBox
//Zend_Dojo_Form_Element_FilteringSelect
class ZendRb_Form_Element_Multiselect extends Zend_Dojo_Form_Element_ComboBox{
	
	/** FilteringSelect | ComboBox Type
	 *	 FilteringSelect : limit input to list
	 *	 Combobox : autocompletion but with free input possibility
	 * 
	 * @var string
	 * 
	 */
	public $helper = 'FilteringSelect';
	
	/**
	 * 
	 * @var unknown_type
	 */
	protected $_registerInArrayValidator = true;
	
	/**
	 * If true, select return the name of object, else return his id
	 *
	 * @var boolean
	 */
	protected $_returnName = false;

	/**
	 * If true, display return value and display value in display value
	 *
	 * @var boolean
	 */
	protected $_displayBoth = false;
	
	/**
	 * If true, display apply advMultiselect decorator
	 *
	 * @var boolean
	 */
	protected $_advSelect = false;
	
    /**
     * 'multiple' attribute
     * @var string
     */
    public $multiple = 'multiple';

    /**
     * Multiselect is an array of values by default
     * @var bool
     */
    protected $_isArray = true;
	
	
	/**
	 * (non-PHPdoc)
	 * @see external/Zend/Form/Zend_Form_Element#init()
	 */	
	public function init(){
	}

	/** If set to true, return the input name and switch to ComboBox type
	 * Else, return the id and swtich to FilteringSelect type
	 * 
	 * @param boolean $return
	 * @return ZendRb_Form_Element_Multiselect
	 */
	public function setReturnName($return){
		$this->_returnName = (int) $return;
		if($this->_returnName){
			$this->helper = 'ComboBox';
			$_registerInArrayValidator = false;
		}else{
			$this->helper = 'FilteringSelect';
			$_registerInArrayValidator = true;
		}
		return $this;
	}

	/**
	 * 
	 * @param boolean $return
	 * @return ZendRb_Form_Element_Multiselect
	 */
	public function setAdvSelect($bool){
		$this->_advSelect = (int) $bool;
		if($bool){
			$this->setDecorators(array(
							'Advmultiselect', 
							'Errors', 
							array('HtmlTag', array()), 
							array('Label', array()),
							));
		}
		return $this;
	}
	
	/**
	 * 
	 * 
	 * @param boolean $return
	 * @return ZendRb_Form_Element_Multiselect
	 */
	public function setDisplayBoth($display){
		$this->_displayBoth = (int) $display;
		return $this;
	}
	
	/**
	 * 
	 * @param Rb_Permanent_Object $source
	 * @return ZendRb_Form_Element_Multiselect
	 */
	public function setSource(Rb_Object_Permanent $source){
		$id_key = $source->getDao ()->getFieldName ( 'id' );
		$number_key = $source->getDao ()->getFieldName ( 'number' );
		$p = array (
			'sort_field' => $number_key, 
			'sort_order' => 'ASC', 
			'select' => array (	$number_key, $id_key ) );
		return $this->setOptionsList($source->getAll($p), $id_key, $number_key);
		//trigger_error('this method is not implemented', E_WARNING);
	}
	
	/**
	 * Create options list from array extract from source. Carefull to call this method after
	 * setAllowEmpty.
	 * 
	 * @param array
	 * @return ZendRb_Form_Element_Multiselect
	 */
	public function setOptionsList(array $optionsList, $id_key, $number_key){
		if($this->getAllowEmpty() == true) $this->addMultiOption(NULL, '');
		foreach ( $optionsList as $value ) {
			if($this->_displayBoth){
				$displayOption = $value [$id_key] .'-'. $value [$number_key];
			}else{
				$displayOption = $value [$number_key];
			}
			if ( $this->_returnName ){
				$this->addMultiOption($value [$number_key], $displayOption);
			}else{
				$this->addMultiOption($value [$id_key], $displayOption);
			}
		}
		return $this;
	}

	/**
	 * 
	 * 
	 * @param boolean
	 * @return ZendRb_Form_Element_Multiselect
	 */
	public function setMultiple($isMutliple){
		if($isMutliple){
			$this->setAttrib('multiple', 'multiple');
			$this->_isArray = true;
			$this->multiple = 'multiple';
		}else{
			$this->_isArray = false;
			$this->multiple = '';
		}
		return $this;
	}
	
}


