<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

/**
 * This class is a librairy of functions to create and init form element
 *
 *
 *
 */
class ZendRb_Form_Element_Creator{

	//------------------------------------------------------------------------------
	/**
	 *
	 * @param array   $field = array(
	 ['property_name']=>string
	 ['property_description']=>string
	 ['property_type']=>string
	 ['field_regex']=>string
	 ['is_required']=>string
	 ['is_multiple']=>string
	 ['property_length']=>string
	 ['return_name']=>string
	 ['field_list']=>string
	 ['selectdb_where']=>string
	 ['field_regex_message']=>string
	 ['default_value']=>string
	 ['sort_order']=>string
	 ['sort_field']=>string
	 ['table_name']=>string
	 ['field_for_value']=>string
	 ['field_for_display']=>string
	 ['date_format']=>string
	 ['date_language']=>string / default 'fr'
	 ['display_both']=>bool / default false
	 ['disabled']=>string
	 * @param unknown_type $form
	 * @param unknown_type $validation
	 */
	function getElement(array $field) {
		if (! isset ( $field ['property_type'] )) return false;
		if (! isset ( $field ['property_fieldname'] ))return false;
		if (! isset ( $field ['property_name'] )) $field ['property_name'] = $field ['property_fieldname'];
		if (empty ( $field ['field_id'] )) 
			$field ['field_id'] = $field ['property_fieldname'];

		switch ($field ['property_type']) {
			case 'text' :
				if (!$field ['property_length']) $field ['property_length'] = 20;
				$element = new Zend_Form_Element_Text( $field ['property_fieldname'] );
				$element->setLabel($field ['property_name'])
						->setRequired($field ['is_required'])
						->setValue($field ['default_value'])
						->setAttrib('id', $field ['field_id'])
						->setAttrib('size', $field ['property_length'])
						->setAttrib('maxlength', $field ['property_length']);
				if($field ['disabled']) $element->setAttrib('disabled', 'disabled');
				if($field ['field_regex']){
					$element->addValidator('Zend_Validate_Regex', array(
											$field ['field_regex']))
							->addErrorMessage($field ['field_regex_message']);
				}
				
				/*
				 $form->addRule ( $field ['property_fieldname'],
						tra ( 'should be less than or equal to' ) . ' ' .
						$field ['property_length'] . ' characters', 'maxlength',
						$field ['property_length'], $validation );
					if (! empty ( $field ['field_regex'] )) { //Add rule with regex
						if (empty ( $field ['field_regex_message'] ))
						$field ['field_regex_message'] = tra ( 'invalid format' );
					$form->addRule ( $field ['property_fieldname'], 
						$field ['field_regex_message'], 
						'regex', '/' . $field ['field_regex'] . '/', $validation );
					$form->addRule ( $field ['property_fieldname'], 
						tra ( 'is required' ), 
						'required', null, $validation );
					*/
				break;

			case 'long_text' :
				if (!$field ['property_length']) $field ['property_length'] = 20;
				$element = new Zend_Form_Element_Textarea( $field ['property_fieldname'] );
				$element->setLabel($field ['property_name'])
						->setRequired($field ['is_required'])
						->setValue($field ['default_value'])
						->setAttrib('id', $field ['field_id'])
						->setAttrib('cols', $field ['property_length'])
						->setAttrib('rows', 8);
				if($field ['disabled']) $element->setAttrib('disabled', 'disabled');
				
				/*
				 if (! empty ( $field ['field_regex'] )) //Add rule with regex
				 if (empty ( $field ['field_regex_message'] ))
					$field ['field_regex_message'] = tra ( 'invalid format' );
					$form->addRule ( $field ['property_fieldname'], $field ['field_regex_message'], 'regex', '/' . $field ['field_regex'] . '/m', $validation );
					if ($field ['is_required']) //Add rule with require option
					$form->addRule ( $field ['property_fieldname'], tra ( 'is required' ), 'required', null, $validation );
					*/
				break;

			case 'html_area' :
				if (!$field ['property_length']) $field ['property_length'] = 40;
				$element = new Zend_Dojo_Form_Element_Editor( $field ['property_fieldname'] );
				$element->setLabel($field ['property_name'])
						->setRequired($field ['is_required'])
						->setValue($field ['default_value'])
						->setAttrib('id', $field ['field_id']);
				//->setAttrib('cols', $field ['property_length'])
				//->setAttrib('rows', 20);
				if($field ['disabled']) $element->setAttrib('disabled', 'disabled');
				/*
				 require_once ('HTML/QuickForm/htmlarea.php');
				 $options = array ('url' => 'lib/htmlarea/',
				 'lang' => Ranchbe::getConfig()->resources->translate->lang,
				 'cssURL' => 'style/default/htmlarea.css',
				 $field ['disabled'] );
				 $attributes = array ('rows' => $field ['property_length'], 'cols' => 80 );
				 $config = array ('width' => '500px', 'height' => '400px', 'imgURL' => 'lib/htmlarea/images/', 'popupURL' => 'lib/htmlarea/popups/' );
				 */
				break;

			case 'integer' :
				if (!$field ['property_length']) $field ['property_length'] = 11;
				$element = new Zend_Dojo_Form_Element_NumberTextBox( $field ['property_fieldname'] );
				$element->setLabel($field ['property_name'])
						->setRequired($field ['is_required'])
						->setValue($field ['default_value'])
						->setAttrib('id', $field ['field_id'])
						->setConstraint(array(
				                        'min' => -9999999999,
				                        'max' => +9999999999,
				                        'places' => 0,
				));
				if($field ['disabled']) $element->setAttrib('disabled', 'disabled');
				$element->addValidator('Zend_Validate_Int', array(
										$field ['field_regex']));					
				/*
				 if ($field ['is_required']) //Add rule with require option
				 $form->addRule ( $field ['property_fieldname'], tra ( 'is required' ), 'required', null, $validation );
				 $form->addRule ( $field ['property_fieldname'], tra ( 'should be numeric' ), 'numeric', null, $validation );
				 */
				break;

			case 'decimal' :
				if (!$field ['property_length']) $field ['property_length'] = 11;
				$element = new Zend_Dojo_Form_Element_NumberTextBox( $field ['property_fieldname'] );
				$element->setLabel($field ['property_name'])
						->setRequired($field ['is_required'])
						->setValue($field ['default_value'])
						->setAttrib('id', $field ['field_id'])
						->setAttrib('size', $field ['property_length'])
						->setConstraint( array() );
				if($field ['disabled']) $element->setAttrib('disabled', 'disabled');
				/*
				 $form->addRule ( $field ['property_fieldname'], tra ( 'is required' ), 'required', null, $validation );
				 $form->addRule ( $field ['property_fieldname'], tra ( 'should be numeric' ), 'numeric', null, $validation );
				 */
				break;
					
			case 'partner' :
				if (!$field ['property_length']) $field ['property_length'] = 1;
				$element = new ZendRb_Form_Element_Selectpartner( $field ['property_fieldname'] );
				$element->setLabel($field ['property_name'])
						->setRequired($field ['is_required'])
						->setValue($field ['default_value'])
						->setAttrib('id', $field ['field_id'])
						->setAttrib('size', $field ['property_length'])
						->setReturnName($field['return_name'])
						->setDisplayBoth($field['display_both'])
						->setAdvSelect($field['adv_select'])
						->setMultiple($field['is_multiple']);
				if($field ['disabled']) $element->setAttrib('disabled', 'disabled');
				break;

			case 'doctype' :
				if (!$field ['property_length']) $field ['property_length'] = 1;
				$element = new ZendRb_Form_Element_Selectdoctype( $field ['property_fieldname'] );
				$element->setLabel($field ['property_name'])
						->setRequired($field ['is_required'])
						->setValue($field ['default_value'])
						->setAttrib('id', $field ['field_id'])
						->setAttrib('size', $field ['property_length'])
						->setReturnName($field['return_name'])
						->setDisplayBoth($field['display_both'])
						->setAdvSelect($field['adv_select'])
						->setMultiple($field['is_multiple']);
				if($field ['disabled']) $element->setAttrib('disabled', 'disabled');
				break;
					
			case 'indice' :
				if (!$field ['property_length']) $field ['property_length'] = 1;
				$element = new ZendRb_Form_Element_Selectindice( $field ['property_fieldname'] );
				$element->setLabel($field ['property_name'])
						->setRequired($field ['is_required'])
						->setValue($field ['default_value'])
						->setAttrib('id', $field ['field_id'])
						->setAttrib('size', $field ['property_length'])
						->setReturnName($field['return_name'])
						->setDisplayBoth($field['display_both'])
						->setAdvSelect($field['adv_select'])
						->setMultiple($field['is_multiple']);
				if($field ['disabled']) $element->setAttrib('disabled', 'disabled');
				break;
					
			case 'user' :
				if (!$field ['property_length']) $field ['property_length'] = 1;
				$element = new ZendRb_Form_Element_Selectuser( $field ['property_fieldname'] );
				$element->setLabel($field ['property_name'])
						->setRequired($field ['is_required'])
						->setValue($field ['default_value'])
						->setAttrib('id', $field ['field_id'])
						->setAttrib('size', $field ['property_length'])
						->setReturnName($field['return_name'])
						->setDisplayBoth($field['display_both'])
						->setAdvSelect($field['adv_select'])
						->setMultiple($field['is_multiple']);
				if($field ['disabled']) $element->setAttrib('disabled', 'disabled');
				break;

			case 'process' :
				if (!$field ['property_length']) $field ['property_length'] = 1;
				$element = new ZendRb_Form_Element_Selectprocess( $field ['property_fieldname'] );
				$element->setLabel($field ['property_name'])
						->setRequired($field ['is_required'])
						->setValue($field ['default_value'])
						->setAttrib('id', $field ['field_id'])
						->setAttrib('size', $field ['property_length'])
						->setReturnName($field['return_name'])
						->setDisplayBoth($field['display_both'])
						->setAdvSelect($field['adv_select'])
						->setMultiple($field['is_multiple']);
				if($field ['disabled']) $element->setAttrib('disabled', 'disabled');
				break;

			case 'select' :
				$list = explode ( '#', $field ['field_list'] );
				$list = array_combine ( $list, $list );
					
				if ($field ['is_multiple'] && isset ( $field ['default_value'] )) {
					$field ['default_value'] = explode ( '#', $field ['default_value'] );
				}
					
				if (!$field ['property_length']) $field ['property_length'] = 1;
				$element = new ZendRb_Form_Element_Multiselect( $field ['property_fieldname'] );
				$element->setLabel($field ['property_name'])
						->setRequired($field ['is_required'])
						->setValue($field ['default_value'])
						->setAttrib('id', $field ['field_id'])
						->setAttrib('size', $field ['property_length'])
						->setReturnName(false)
						->setDisplayBoth(false)
						->setAdvSelect($field['adv_select'])
						->setMultiple($field['is_multiple'])
						->setMultiOption($list);
				if($field ['disabled']) $element->setAttrib('disabled', 'disabled');
				break;

			case 'selectFromDB' :
				$pget ['sort_field'] = $field ['field_for_display'];
				$pget ['sort_order'] = 'ASC';
				$pget ['maxRecords'] = 10000;
				$pget ['select'] = array ($field ['field_for_value'], $field ['field_for_display'] );
				$dao = new Rb_Dao ( Ranchbe::getDb () );
				$dao->setTable ( $field ['table_name'] );
				$list = $dao->getAll ( $pget );
					
				if (!$field ['property_length']) $field ['property_length'] = 1;
				$element = new ZendRb_Form_Element_Multiselect( $field ['property_fieldname'] );
				$element->setLabel($field ['property_name'])
						->setRequired($field ['is_required'])
						->setValue($field ['default_value'])
						->setAttrib('id', $field ['field_id'])
						->setAttrib('size', $field ['property_length'])
						->setReturnName($field ['return_name'])
						->setDisplayBoth($field['display_both'])
						->setAdvSelect($field['adv_select'])
						->setMultiple($field['is_multiple'])
						->setOptionsList($list, $field ['field_for_value'], $field ['field_for_display']);
				if($field ['disabled']) $element->setAttrib('disabled', 'disabled');
				break;
					
			case 'liveSearch' :
				break;
					
			case 'date' :
				$element = new ZendRb_Form_Element_Date( $field ['property_fieldname'] );
				$default_date = Rb_Date::formatDate($field ['default_value'], 'dojo' );
				$element->setLabel($field ['property_name'])
						->setRequired($field ['is_required'])
						->setValue($default_date)
						->setAttrib('id', $field ['field_id']);
				if($field ['disabled']) $element->setAttrib('disabled', 'disabled');
				break;
			default :
		} //End of switch
		return $element;

	} // End of method

} //End of class
