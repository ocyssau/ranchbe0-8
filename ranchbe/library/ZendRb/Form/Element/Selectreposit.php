<?php
class ZendRb_Form_Element_Selectreposit extends ZendRb_Form_Element_Multiselect{

	/**
	 * 
	 * @param Rb_Reposit $source
	 * @return ZendRb_Form_Element_SelectReposit
	 */
	public function setSource($source = null){
		$id_key = Rb_Reposit::get ()->getDao ()->getFieldName ( 'id' );
		$number_key = Rb_Reposit::get ()->getDao ()->getFieldName ( 'number' );
		$p = array ('sort_field' => $number_key, 
					'sort_order' => 'ASC', 
					'select' => array ($id_key, $number_key, 'reposit_name' ),
					'exact_find'=>  array ('reposit_type' => 1 ),
		);
		$optionsList = Rb_Reposit::get ()->getAll ( $p );
		return $this->setOptionsList($optionsList, $id_key, $number_key);
	}
}


