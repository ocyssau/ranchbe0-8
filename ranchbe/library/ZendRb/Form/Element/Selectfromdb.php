<?php
class ZendRb_Form_Element_Selectfromdb extends ZendRb_Form_Element_Multiselect{

	/**
	 * @param array $source('field_for_value', 'field_for_display', 'table_name' )
	 * @return ZendRb_Form_Element_SelectUser
	 */
	public function setSource(array $source){
		$pget ['sort_field'] = $source ['field_for_display'];
		$pget ['sort_order'] = 'ASC';
		$pget ['maxRecords'] = 10000;
		$pget ['select'] = array ($source ['field_for_value'], $source ['field_for_display'] );
		$dao = new Rb_Dao ( Ranchbe::getDb () );
		$dao->setTable ( $source ['table_name'] );
		$list = $dao->getAll ( $pget );
		return $this->setOptionsList($list, $source ['field_for_value'], $source ['field_for_display']);
	}
}


