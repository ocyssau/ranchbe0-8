<?php
class ZendRb_Form_Element_Selectuser extends ZendRb_Form_Element_Multiselect{

	/**
	 * @param
	 * @return ZendRb_Form_Element_SelectUser
	 */
	public function setSource(){
		$list = Ranchbe::getAuthAdapter ()->getUsers ();
		return $this->setOptionsList($list, 'auth_user_id', 'handle');
	}
}


