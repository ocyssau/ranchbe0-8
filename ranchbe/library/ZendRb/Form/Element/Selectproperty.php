<?php
class ZendRb_Form_Element_Selectproperty extends ZendRb_Form_Element_Multiselect{

	/**
	 * 
	 * @param Rb_Dao_Abstract $source
	 * @return ZendRb_Form_Element_SelectProperty
	 */
	public function setSource(Rb_Dao_Abstract $dao){
		$list = Rb_Metadata_Dictionary::get ()->getMetadatas ( $dao, array (
						'select' => array (
									'property_id', 
									'property_name', 
									'property_description' ) ) );
		return $this->setOptionsList($list, 'property_id', 'property_name');
	}
	
}


