<?php
class ZendRb_Form_Element_Selectpartner extends ZendRb_Form_Element_Multiselect{

	/**
	 * 
	 * @return ZendRb_Form_Element_SelectPartner
	 */
	public function setSource(){
		$p = array();
		$p ['select'] = array ('partner_id', 'partner_number' );
		if (! empty ( $p ['selectdb_where'] ))
			$p ['where'] = array ($p ['selectdb_where'] );
		$p ['sort_order'] = 'ASC';
		$p ['sort_field'] = 'partner_number';
		$list = Rb_Partner::get ()->getAll ( $p );
		return $this->setOptionsList($list, 'partner_id', 'partner_number');
	}
	
}


