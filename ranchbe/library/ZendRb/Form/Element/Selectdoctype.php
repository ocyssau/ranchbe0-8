<?php
class ZendRb_Form_Element_Selectdoctype extends ZendRb_Form_Element_Multiselect{

	/**
	 * @param Rb_Doctype
	 * @return ZendRb_Form_Element_SelectDoctype
	 */
	public function setSource(){
		$p = array();
		if (! empty ( $options ['selectdb_where'] ))
			$p ['where'] [] = $options ['selectdb_where'];
		$p ['select'] = array ('doctype_id', 'doctype_number' );
		$p ['sort_order'] = 'ASC';
		$p ['sort_field'] = 'doctype_number';
		$list = Rb_Doctype::get()->getAll ( $p );
		return $this->setOptionsList($list, 'partner_id', 'partner_number');
	}
}


