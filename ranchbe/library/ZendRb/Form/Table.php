<?php

class ZendRb_Form_Table extends Zend_Dojo_Form
{
	/**
	 * (non-PHPdoc)
	 * @see external/Zend/Zend_Form#init()
	 */
	public function init(){
		$this->addPrefixPath('ZendRb_Form_Element_',
								'ZendRb/Form/Element/', 
								'element');
		
		$this->addPrefixPath('ZendRb_Form_Decorator_',
								'ZendRb/Form/Decorator/', 
								'decorator');
		
		$this->addElementPrefixPath('ZendRb_Form_Decorator_',
		                            'ZendRb/Form/Decorator/',
		                            'decorator');
		
		//DECORATORS :
		//Loop on elements
		$this->addDecorator('FormElements');
		
		//Add td tag on elements
		//$this->addDecorator('FormInTableElements');
		
		//Add table and tr tag
		//$this->addDecorator('Formintable');
		
		//default render
		$this->addDecorator('HtmlTag', array('tag' => 'table', 'class' => 'Rb_Form'));
		
		//default decorator
		$this->addDecorator('DijitForm');

		$this->setMethod('post');
		$this->setAttrib('id', 'editform');
		$this->setAttrib('name', 'editform');
		$this->addElement('submit', 'save', array(
											'label' => 'Save', 
											'order'=>1000));
		/*
		$this->setElementDecorators(array(
			    array('ViewHelper'),
			    array('Errors'),
			    array('Description', array('tag' => 'p', 'class' => 'description')),
				array('HtmlTag' , array('tag'=>'td')),
				));
		*/

	}
}

