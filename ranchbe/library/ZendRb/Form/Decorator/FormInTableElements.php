<?php
class ZendRb_Form_Decorator_FormInTableElements extends Zend_Form_Decorator_Abstract
{
	/**
	 * Render form elements
	 *
	 * @param  string $content
	 * @return string
	 */
	public function render($content)
	{
		$form    = $this->getElement();
		if ((!$form instanceof Zend_Form) && (!$form instanceof Zend_Form_DisplayGroup)) {
			return $content;
		}

		$items          = array();
		$separator      = $this->getSeparator();
		foreach ($form as $item) {
			if( is_a($item, 'Zend_Form_SubForm') ){
				$items[] = '<tr>' . $item->render() . '</tr>';
			}else{
				$items[] = '<td>' . $item->render() . '</td>';
			}
		}
        return implode($separator, $items);
	}
}
