<?php
/**
 * Copyright (c) 2010, Olivier Cyssau <olivier.cyssau@acensea.com>
 * This scripts is build on work of Laurent Laville as described here :
 * @category  HTML
 * @package   HTML_QuickForm_advmultiselect
 * @author    Laurent Laville <pear@laurent-laville.org>
 * @copyright 2005-2008 Laurent Laville
 * @license   http://www.opensource.org/licenses/bsd-license.php  BSD
 * @version   CVS: $Id: advmultiselect.php,v 1.1 2009/06/05 17:21:44 ranchbe Exp $
 * @link      http://pear.php.net/package/HTML_QuickForm_advmultiselect
 * 
 * You must respect initial licence of package HTML_QuickForm_advmultiselect
 * 
 * Any questions or feedback about this scripts must be send to Olivier CYSSAU.
 * 
 */

/**
 * Copyright (c) 2005-2008, Laurent Laville <pear@laurent-laville.org>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the authors nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * PHP versions 5.3
 *
 */


/**
 * Decorator for Zend_Form_Element that emulate a multi-select.
 *
 * The Form_Decorator_Advmultiselect package adds an element to the
 * Zend_Form_Decorator that is two select boxes next to each other
 * emulating a multi-select.
 * 
 * How to use :
 *	$element = new Zend_Form_Element_Multiselect('element_name');
 *	$element->setLabel('Element label')
 *			->setMultiOptions(array('option1','option2','option3'))
 *			->setDecorators(array('Advmultiselect'));
 *	echo $element->render();
 *
 * @author    Laurent Laville <pear@laurent-laville.org> Olivier CYSSAU <olivier.cyssau@acensea.com>
 * @copyright 2005-2010 Laurent Laville, Olivier CYSSAU
 * @license   http://www.opensource.org/licenses/bsd-license.php  BSD
 * @version
 * @link
 * @since
 */
class ZendRb_Form_Decorator_Advmultiselect extends Zend_Form_Decorator_Abstract{
	
	/**
	 * Id of the left select box
	 * This id is different of element id
	 * 
	 * @var string
	 */
	protected $_leftBoxId = '';
	
	/**
	 * Name of the left select box
	 * This name is different of element name
	 * 
	 * @var string
	 */
	protected $_leftBoxName = '';
	
	/**
	 * Name of the right select box
	 * This name is different of element name
	 * 
	 * @var string
	 */
	protected $_rightBoxName = '';
	
	/**
	 * Id of the right select box
	 * This id is different of element id
	 * 
	 * @var string
	 */
	protected $_rightBoxId = '';
	
	
	/**
	 * Options for select
	 * 
	 * @var array
	 */
	protected $_options = array();

	/**
	 * Initials options in left box
	 * 
	 * @var array
	 */
	protected $_leftoptions = array();
	
	/**
	 * Initials options in right box
	 * 
	 * @var array
	 */
	protected $_rightoptions = array();
	
	/**
	 * Initials selected options
	 * 
	 * @var array
	 */
	protected $_selectedOptions = array();
	
	
	/**
	 * Render a box
	 *
	 * @return string
	 */
	protected static function _renderBox($thisBoxId, $thisBoxName, 
								  $opositeBoxId, $opositeBoxName,
								  $elementId, $elementName,
								  array $options, array $selected = array()){
		$html = "<select id=\"$thisBoxId\"";
		$html .= " ondblclick=\"qfamsMoveSelection('$elementName', this.form.elements['$thisBoxName']";
		$html .= ",this.form.elements['$opositeBoxName']";
		$html .= ",this.form.elements['$elementName']";
		$html .= ",'add', 'none')\"";
		$html .= ' multiple="multiple"';
		$html .= " name=\"$thisBoxName\"";
		$html .= 'style="width: 300px;" size="5">';
		$html .= self::_renderSelectOptions($options, $selected);
		$html .= '</select>';
		return $html;
	}
	
	/**
	 * Generate select options from array
	 *
	 * @return string
	 */
	protected static function _renderSelectOptions(array $options, array $selected){
		$html = '';
		foreach($options as $key=>$option){
			if (is_array($option)){
				$html .= "<optgroup label=\"$key\">";
				$html .= self::_renderSelectOptions($option, $selected);
				$html .= '</optgroup>';
				continue;
			}
			$attrib = '';
			if (in_array($key, $selected, true)){
				$attrib = 'selected="selected"';
			}
			$html .= "<option label=\"$option\" value=\"$key\" $attrib>$option</option>";
		}
		return $html;
	}
	
	
	/**
	 * Render the left box select list
	 *
	 * @return string
	 */
	protected function _renderLeftBox(){
		//var_dump($this->_options, $this->_selectedOptions);die;
		if(!is_array($this->_selectedOptions)) $this->_selectedOptions = array($this->_selectedOptions);
		$options = array_diff_key($this->_options, array_flip($this->_selectedOptions));
		return $this->_renderBox($this->_leftBoxId, $this->_leftBoxName, 
								$this->_rightBoxId, $this->_rightBoxName,
								$this->_elementId, $this->_elementName,
								$options);
	}
	
	
	/**
	 * Render the right box select list
	 *
	 * @return string
	 */
	protected function _renderRightBox(){
		$options = array_intersect_key($this->_options, array_flip($this->_selectedOptions));
		return $this->_renderBox($this->_rightBoxId, $this->_rightBoxName, 
								$this->_leftBoxId, $this->_leftBoxName,
								$this->_elementId, $this->_elementName,
								$options);
	}
	
	/**
	 * Render select field, this field is use to return value in $POST var
	 * it is the base element. It is hidden
	 *
	 * @return string
	 */
	protected function _renderSelect(){
		$elementId =& $this->_elementId;
		$elementName =& $this->_elementName;
		$html = "<select multiple=\"multiple\" name=\"$elementName\" id=\"$elementId\"";
		$html .= ' style="overflow: hidden; visibility: hidden; width: 1px; height: 0pt;"';
		$html .= ' size="1">';
		$html .= self::_renderSelectOptions($this->_options, $this->_selectedOptions);
		$html .= '</select>';
		return $html;
	}

	/**
	 * Render select field, this field is use to return value in $POST var
	 * @param array $options add=true|false, remove=true|false, 
	 * 							all=true|false, none=true|false
	 * 							toggle=true|false
	 * 							
	 * @return string
	 */
	protected function _renderButtons(array $options){
		$elementId =& $this->_elementId;
		$elementName =& $this->_elementName;
		$leftBoxId =& $this->_leftBoxId;
		$leftBoxName =& $this->_leftBoxName;
		$rightBoxId =& $this->_rightBoxId;
		$rightBoxName =& $this->_rightBoxName;
		
		$commonhtml = '<input class="inputCommand" type="button"';
		$commonhtml .= " onclick=\"qfamsMoveSelection('$elementId', this.form.elements['$leftBoxName']";
		$commonhtml .= ",this.form.elements['$rightBoxName'], this.form.elements['$elementName']";
		
		$sort = 'none';
		
		//Add button
		if($options['add']){
			$html .= $commonhtml;
			$html .= ",'add', '$sort'); return false;\"";
			$html .= 'value="Add >>" name="add"/>';
			$html .= "\n";
		}
		//Remove button
		if($options['remove']){
			$html .= $commonhtml;
			$html .= ",'remove', '$sort'); return false;\"";
			$html .= 'value="<< Remove" name="remove"/>';
		}
		$html .= '<br />';
		//Add all button
		if($options['all']){
			$html .= $commonhtml;
			$html .= ",'all', '$sort'); return false;\"";
			$html .= 'value="All>" name="all"/>';
			$html .= "\n";
		}
		//Remove all button
		if($options['none']){
			$html .= $commonhtml;
			$html .= ",'none', '$sort'); return false;\"";
			$html .= 'value="None<" name="none"/>';
			$html .= "\n";
		}
		$html .= '<br />';
		//Add toggle button
		if($options['toggle']){
			$html .= $commonhtml;
			$html .= ",'toggle', '$sort'); return false;\"";
			$html .= 'value=">Toggle<" name="toggle"/>';
			$html .= "\n";
		}
		return $html;
	}
	
	/**
	 * Render form elements
	 *
	 * @param  string $content
	 * @return string
	 */
	public function render($content){
		//var_dump($content);die;
		//return 'toto';
		
		$element = $this->getElement();
		$this->_elementId = $element->getId();
		$this->_elementName = $element->getName().'[]';
		$this->_leftBoxId = $element->getId() . '-f';
		$this->_leftBoxName = $element->getName() . '-f[]';
		$this->_rightBoxId = $element->getId() . '-t';
		$this->_rightBoxName = $element->getName() . '-t[]';
		if($element->getValue())
        	$this->_selectedOptions = $element->getValue();
        $attribs       	= $element->getAttribs();
		$this->_options = $attribs['options'];
		
        $view = $element->getView();
        if (null === $view) {
            require_once 'Zend/Form/Decorator/Exception.php';
            throw new Zend_Form_Decorator_Exception('ViewHelper decorator cannot render without a registered view object');
        }
		
        if (method_exists($element, 'getMultiOptions')) {
            $element->getMultiOptions();
        }
		
        $separator = $element->getSeparator(); //'<br />'
        
        //var_dump($value, $attribs, $separator);die;
        $buttons = array('add'=>true, 'remove'=>true, 'all'=>true, 'none'=>true, 'toggle'=>false);
        
        $html = ''; 
        $html .= '<table class="pool"><tbody>'; 
        $html .= '<tr><th>Available</th><th>&nbsp;</th><th>Selected</th></tr><tr>';
        $html .= '<td valign="top">'.$this->_renderLeftBox().'</td>';
        $html .= '<td valign="top" ALIGN="center">'.$this->_renderButtons($buttons).'</td>';
        $html .= '<td valign="top">'.$this->_renderRightBox().'</td>';
        $html .= '</tr></tbody></table>'; 
        $html .= $this->_renderSelect();
        return $html;
	}

} //End of class

