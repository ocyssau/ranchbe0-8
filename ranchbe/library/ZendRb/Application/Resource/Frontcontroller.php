<?php
class ZendRb_Application_Resource_Frontcontroller extends Zend_Application_Resource_Frontcontroller
{
    public function getFrontController()
    {
    	if (null === $this->_front) {
            $this->_front = Zend_Controller_Front::getInstance();
            //$this->_front->setRouter(new Zend_Controller_Router_Rewrite());
			$this->_front->getRouter()->addRoute('container',
			    new Zend_Controller_Router_Route('container/:space',
                                     array('controller' => 'index',
                                           'module' => 'container',
                                           'action' => 'index')));
        }
        return $this->_front;
    }
}
