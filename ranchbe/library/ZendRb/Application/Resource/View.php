<?php

class ZendRb_Application_Resource_View
    extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
    	$view = new Zend_View($this->getOptions());
        Zend_Dojo::enableView($view);

        $view->doctype('XHTML1_STRICT');
        $view->headTitle()->setSeparator(' - ')->append('My Site');
        $view->headMeta()->appendHttpEquiv('Content-Type',
                                           'text/html; charset=utf-8');

        $view->dojo()->setDjConfigOption('parseOnLoad', true)
                     ->setLocalPath('/js/dojo/dojo.js')
                     ->registerModulePath('../spindle', 'spindle')
                     ->addStylesheetModule('spindle.themes.spindle')
                     ->requireModule('spindle.main')
                     ->disable();

        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper(
                            'ViewRenderer'
                        );
        $viewRenderer->setView($view);

        return $view;
    }
}
