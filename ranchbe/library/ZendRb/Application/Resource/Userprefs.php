<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

class ZendRb_Application_Resource_Userprefs extends Zend_Application_Resource_ResourceAbstract {
	public function init() {
		$preferences = Rb_User::getCurrentUser ()->getPreference()->getUserPreferences();
		$config = Ranchbe::getConfig ();
		if ($preferences ['css_sheet'] && $preferences ['css_sheet'] != 'default') {
			$config->resources->smarty->stylesheet->user = $preferences ['css_sheet'];
		}
		if ($preferences ['lang'] && $preferences ['lang'] != 'default') {
			$config->resources->adodb->lang = $preferences ['lang'];
			$config->resources->smarty->lang = $preferences ['lang'];
			$config->resources->translate->lang = $preferences ['lang'];
		}else{
			$config->resources->adodb->lang = $config->resources->translate->lang;
			$config->resources->smarty->lang = $config->resources->translate->lang;
		}
		if ($preferences ['long_date_format'] && $preferences ['long_date_format'] != 'default') {
			$config->date->format->long = $preferences ['long_date_format'];
		}
		if ($preferences ['short_date_format'] && $preferences ['short_date_format'] != 'default') {
			$config->date->format->short = $preferences ['short_date_format'];
		}
		if ($preferences ['date_input_method'] && $preferences ['date_input_method'] != 'default') {
			$config->date->input_method = $preferences ['date_input_method'];
		}
		if ($preferences ['charset'] && $preferences ['charset'] != 'default') {
			$config->phpSettings->default_charset = $preferences ['charset'];
			ini_set ( 'default_charset', $preferences ['charset'] );
		}
		if ($preferences ['time_zone'] && $preferences ['time_zone'] != 'default') {
			$config->phpSettings->default_charset = $preferences ['time_zone'];
			ini_set ( 'date.timezone', $preferences ['time_zone'] );
		}
		if ($preferences ['max_record'] && $preferences ['max_record'] != 'default') {
		}
		//Zend_Debug::dump($config->toArray());die;
		$this->getBootstrap ()->setOptions ($config->toArray());
		//$config->setReadOnly();
	}
}

