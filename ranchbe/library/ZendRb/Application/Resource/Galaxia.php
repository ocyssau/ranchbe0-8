<?php

/**
 * Resource for settings Galaxia workflow engine
 *
 * @uses       Zend_Application_Resource_ResourceAbstract
 * @category   ZendRb
 * @package    Zend_Application
 * @subpackage Resource
 */
class ZendRb_Application_Resource_Galaxia extends Zend_Application_Resource_ResourceAbstract {
	
	/**
	 * Defined by Zend_Application_Resource_Resource
	 *
	 * @return Zend_Layout
	 */
	public function init() {
		$options = $this->getOptions ();
		// Common prefix used for all database table names, e.g. galaxia_
		define ( 'GALAXIA_TABLE_PREFIX', $options['table_prefix'] );
		
		// Directory containing the Galaxia library, e.g. lib/Galaxia
		define ( 'GALAXIA_LIBRARY', $options['library_path'] );
		
		// Directory where the Galaxia processes will be stored, e.g. lib/Galaxia/processes
		// Note: this directory must be writeable by the webserver !
		define ( 'GALAXIA_PROCESSES', $options['process_path'] );
		
		// Directory where a *copy* of the Galaxia activity templates will be stored, e.g. templates
		// Define as '' if you don't want to copy templates elsewhere
		define ( 'GALAXIA_TEMPLATES', $options['template_path'] );
		
		// Default header to be added to new activity templates
		define ( 'GALAXIA_TEMPLATE_HEADER', $options['template_header'] );
		
		// File where the ProcessManager logs for Galaxia will be saved, e.g. lib/Galaxia/log/pm.log
		// Define as '' if you don't want to use logging
		// Note: this file must be writeable by the webserver !
		define ( 'GALAXIA_LOGFILE', $options['logfile'] );
		
		// Directory containing the GraphViz 'dot' and 'neato' programs, in case
		// your webserver can't find them via its PATH environment variable
		define ( 'GRAPHVIZ_BIN_DIR', $options['graphviz_bin_dir'] );
	}
}
