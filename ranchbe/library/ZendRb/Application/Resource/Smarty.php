<?php

class ZendRb_Application_Resource_Smarty
    extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
		$options = $this->getOptions();
		require_once('ZendRb/View.php');
		$view = new ZendRb_View($options);
		$viewhelper = new Zend_Controller_Action_Helper_ViewRenderer($view);
		$viewhelper->setViewSuffix($options['view_suffix']);
		$viewhelper->setViewScriptPathSpec($options['view_script_path_spec']);
		
		/*
		$front = Zend_Controller_Front::getInstance();
		$DojoLayerPlugin = new ZendRb_Controller_Plugin_DojoLayer();
		$DojoLayerPlugin->setView( $view );
		$DojoLayerPlugin->setLayerScript( APPLICATION_PATH . '/../public/js/custom/main.js' );
		$DojoLayerPlugin->setProfileScript( APPLICATION_PATH . '/../public/jsdev/profile.js' );
		$front->registerPlugin( $DojoLayerPlugin );
		*/
		
		//Set dojo
        Zend_Dojo::enableView($view);
        $view->dojo()->setDjConfigOption('parseOnLoad', true)
                     ->setLocalPath(Ranchbe::getConfig()->js->baseurl.'dojo/dojo.js')
                     ->addStylesheetModule('dijit.themes.tundra')
                     ->addLayer(Ranchbe::getConfig()->js->baseurl.'dojo/rbdojo.js')
                     ->disable();

		$doctypeHelper = new Zend_View_Helper_Doctype();
		$doctypeHelper->doctype($options['doctype']);

		//<meta http-equiv="Content-Script-Type" content="text/javascript" />
		
		$view->headLink()
			->headLink ( array ('rel' => 'shortcut icon', 
								'href' => ROOT_URL.$options['favicon'] ), 'PREPEND' )
			->headLink ( array ('rel' => 'icon', 
								'href' => ROOT_URL.$options['favicon'] ), 'PREPEND' );

		$view->headLink()->offsetSetStylesheet ( 'default', ROOT_URL.$options['stylesheet']['default'] );
		
		//if(is_file(Ranchbe::getPublicPath().'/'.$options['stylesheet'][Ranchbe::getBrowser()])){
		//var_dump(Ranchbe::getBrowser(), ROOT_URL.$options['stylesheet'][Ranchbe::getBrowser()]);die;
			$view->headLink()->offsetSetStylesheet (Ranchbe::getBrowser(), ROOT_URL.$options['stylesheet'][Ranchbe::getBrowser()] );
		//}
		$view->headLink()->offsetSetStylesheet ( 'user', ROOT_URL.$options['stylesheet']['user'] );
		
		// param�trage des mots cl�s
		$view->headMeta ()->appendName ( 'keywords', $options['keyword'] )
			->appendHttpEquiv ( 'Content-Type', $options['content_type'].'; '.$options['encoding'] )
		  	->appendHttpEquiv ( 'Content-Language', $options['lang'] );
		// d�sactiver la mise en cache c�t� client
			//->appendHttpEquiv ( 'expires', 'Wed, 26 Feb 1997 01:01:01 GMT' )
			//->appendHttpEquiv ( 'pragma', 'no-cache' )
			//->appendHttpEquiv ( 'Cache-Control', 'no-cache' );
		
		// placer celui-ci � un offset particulier pour s'assurer
		// de le charger en dernier
		//$view->headScript()->offsetSetFile(100, '/js/myfuncs.js');
  		
		//$view->headScript()->appendFile(Ranchbe::getConfig()->js->baseurl.'lib.js');
		$view->headScript()->offsetSetFile(100, Ranchbe::getConfig()->js->baseurl.'lib.js');
		$view->headScript()->appendFile(Ranchbe::getConfig()->js->baseurl.'overlib.js');
		
		// mais dans tous les cas, le script de base prototype
		// doit �tre charg� en premier :
		//$view->headScript()->prependFile('/js/prototype.js');
		
		$view->headTitle ( $options['headtitle'] )
			 ->headTitle ()->setSeparator ( ' -- ' );
		
		//$view->getEngine()->assign_by_ref('navigation',$view->navigation());
		//var_dump($view->navigation());
		
		//Zend_Debug::dump($view->getScriptPaths());die;
		//Zend_Debug::dump($view->getHelperPaths());die;
		//Zend_Debug::dump($view->getFilterPaths());die;
		
		Zend_Controller_Action_HelperBroker::addHelper($viewhelper);
		
		$view->ranchbe_version = Ranchbe::RANCHBE_VER; //Assign variable to smarty
	    $view->ranchbe_build = Ranchbe::RANCHBE_BUILD; //Assign variable to smarty
	    $view->ranchbe_copyright = Ranchbe::RANCHBE_COPYRIGHT; //Assign variable to smarty
	    $view->logo = ROOT_URL . $options['logo']; //Assign variable to smarty
	    $view->root_url = ROOT_URL; //Assign variable to smarty
	    
		//Assign value of the version to smarty
		/*
	    $preference =& rb_user::getCurrentUser()->getPreference();
	    $view->css_sheet = $preference->getPreference('css_sheet'); //Assign variable to smarty
	    $view->charset = $preference->getPreference('charset'); //Assign variable to smarty
	    $view->shortcut_icon = SHORTCUT_ICON; //Assign variable to smarty
	    $view->allow_user_prefs = ALLOW_USER_PREFS; //Assign variable to smarty
	    $view->lang = $preference->getPreference('lang'); //Assign variable to smarty
	    $view->current_user_name = rb_user::getCurrentUser()->getUsername();
	    $view->current_user_id = rb_user::getCurrentUser()->getId();
	    */
		//$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper ( 'ViewRenderer' );
        //$viewRenderer->setView($view);
	    
	    return $view;
    }
}
