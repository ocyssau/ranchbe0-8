<?php

class ZendRb_Application_Resource_Client extends Zend_Application_Resource_ResourceAbstract {
	public function init() {
		$this->_initClient ();
	}
	
	//------------------------------------------------------------------------
	protected function _initClient() {
		//$application = $this->getBootstrap()->getApplication();
		//$this->setOptions(array('toto'=>'tutu'));
		$options = $this->getOptions ();

		//Check the client browser
		if($options['force_browser']){
			$valid = false;
			foreach($options['force_browser'] as $browser){
				if(stristr ( $_SERVER ['HTTP_USER_AGENT'], $browser ) ){
					$valid = true;
					Ranchbe::setBrowser($browser);
					break;
				}
			}
			if( !$valid ){
				print 'Sorry, for the moment you can not use this browser '.$_SERVER ['HTTP_USER_AGENT'];
				print 'Your browser must be one of this list :<ul>';
				foreach($options['force_browser'] as $browser){
					print '<li>'.$browser.'</li>';
				}
				print '</ul>';
				die ();
			}
		}
		
		if (stristr ( $_SERVER ['HTTP_USER_AGENT'], 'windows' )) {
			$options ['clientOs'] = 'WINDOWS';
			$options ['eol'] = '\r\n';
		} else if (stristr ( $_SERVER ['HTTP_USER_AGENT'], 'macintosh' ) || stristr ( $_SERVER ['HTTP_USER_AGENT'], 'mac_powerpc' )) {
			$options ['clientOs'] = 'MACINTOSH';
			$options ['eol'] = '\r';
		} else {
			$options ['clientOs'] = 'UNIX';
			$options ['eol'] = '\n';
		}
		define ( 'CLIENT_OS', $options ['clientOs'] );
		
		$this->setOptions ( $options );
	}
	
	//------------------------------------------------------------------------
	public function getClientOs() {
		$application = $this->getBootstrap ()->getApplication ();
		if ($application->getOption ( 'clientOs' ) === null) {
			self::_initClient ();
		}
		return $application->getOption ( 'clientOs' );
	}
	
	//------------------------------------------------------------------------
	public function getEol() {
		$application = $this->getBootstrap ()->getApplication ();
		if ($application->getOption ( 'eol' ) === null) {
			self::_initClient ();
		}
		return $application->getOption ( 'eol' );
	}
	
	//------------------------------------------------------------------------
	public function getPathMapping() {
		$application = $this->getBootstrap ()->getApplication ();
		switch ($application->getOption ( 'clientOs' )) {
			// Windows
			case 'WINDOWS' :
				$map = $application->getOption ( 'pathmapping.windows' );
				$path = $application->getOption ( 'path.read' );
				break;
			// Mac
			case 'MACINTOSH' :
				$map = $application->getOption ( 'pathmapping.mac' );
				$path = $application->getOption ( 'path.read' );
				break;
			// Unix
			case 'UNIX' :
				$map = $application->getOption ( 'pathmapping.unix' );
				$path = $application->getOption ( 'path.read' );
				break;
		}
		$_path_mapping = array ($path ['bookshop'] => $map ['bookshop'], $path ['cadlib'] => $map ['cadlib'], $path ['mockup'] => $map ['mockup'], $path ['workitem'] => $map ['workitem'], $path ['wildspace'] => $map ['wildspace'] );
		return $_path_mapping;
	}
}
