<?php

class ZendRb_Application_Resource_Php
extends Zend_Application_Resource_ResourceAbstract
{
	public function init()
	{
		$include_path = array();
		foreach(explode(PATH_SEPARATOR,get_include_path()) as $pathelement){
			if($pathelement){
				$include_path[] = realpath($pathelement);
			}
		}
		set_include_path(implode(PATH_SEPARATOR, $include_path));
		
		//minimal options for session
		//ini_set('session.auto_start', 0);
		ini_set('session.use_cookies', 1);
		ini_set('session.use_only_cookies', 1);
		
		
		/**
		 * To use xdebug, install module for php from http://www.xdebug.org
		 * Add this line to end of php ini file :
		 * [XDebug]
		 zend_extension_ts=[path to dll or module for unix] example: "C:\wamp\bin\php\php5.2.5\ext\php_xdebug-2.0.3-5.2.5.dll"
		 xdebug.remote_enable=true
		 xdebug.remote_host=127.0.0.1
		 xdebug.remote_port=9000
		 xdebug.remote_handler=dbgp
		 xdebug.profiler_enable=0 // 1 to enable profiler
		 xdebug.profiler_output_dir=[path to dir where store profiler logfile] example "c:\tmp\xdebug\"
		 xdebug.auto_trace=1
		 xdebug.trace_output_dir=[path to dir where store logfile] example "c:\tmp\xdebug\"
		 *
		 */
		ini_set('xdebug.show_local_vars', 1);
		ini_set('xdebug.var_display_max_data', 512);
		//number of array elements or object properties that xdebug displays
		ini_set('xdebug.var_display_max_children', 128);
		//three nested levels of array elements and object relations are displayed
		ini_set('xdebug.var_display_max_depth', 15);
		//ini_set('xdebug.max_nesting_level', 100); //three nested levels of array elements and object relations are displayed
		//Display undefined var too
		ini_set('xdebug.dump_undefined', 'on');
		//Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals();
		ini_set('xdebug.dump.POST', '*');
		//Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals();
		ini_set('xdebug.dump.GET', '*');
		//controls whether Xdebug should collect the parameters passed to functions when a function call is recorded in either the function trace or the stack trace
		ini_set('xdebug.collect_params' , 4);
		//controls whether Xdebug should write the return value of function calls to the trace files.
		ini_set('xdebug.collect_return' , 1);
		ini_set('xdebug.show_exception_trace' , 'on');
		//1: computer readable format / 0: shows a human readable indented trace file with: time index, memory usage, memory delta (if the setting xdebug.show_mem_delta is enabled), level, function name, function parameters (if the setting xdebug.collect_params is enabled, filename and line number.
		ini_set('xdebug.trace_format' , 0);
	}
}
