<?php

function tra($content) {
	return ZendRb_Application_Resource_Translate::tra ( $content );
}

class ZendRb_Application_Resource_Translate extends Zend_Application_Resource_ResourceAbstract {
	
	protected static $_language = 'en';
	protected static $_traductions = false;
	protected static $_debug = false;
	protected static $_debugFile = false;
	
	public function init() {
		$options = $this->getOptions ();
		self::_setLang ( $options ['lang'] );
		self::_setPath ( $options ['path'] );
		if($options ['debug']){
			self::_setDebug ( $options ['debug']['enable'] );
			self::_setDebugPath ( $options ['debug']['path'] );
		}
		return $this;
	}
	
	public static function tra($content) {
		if ($content) {
			if (isset ( self::$_traductions [$content] )) {
				return self::$_traductions [$content];
			} else {
				if (self::$_debug) {
					$handle = fopen (self::$_debugFile, 'a' );
					fwrite ( $handle, $content . PHP_EOL );
					fclose ( $handle );
				}
				return $content;
			}
		}
	} //end of method

	public static function getLang() {
		return self::$_language;
	} //end of method
	
	protected static function _setLang($language) {
		self::$_language = $language;
	} //end of method

	protected static function _setPath($path) {
		$path = str_ireplace('%lang%', self::$_language, $path);
		require_once ($path);
		self::$_traductions =& $lang;
	} //end of method

	protected static function _setDebugPath($path) {
		$path = str_ireplace('%lang%', self::$_language, $path);
		self::$_debugFile = $path;
	} //end of method

	protected static function _setDebug($debug) {
		self::$_debug = (bool) $debug;
	} //end of method
}
