<?php

require_once ('Rb/Conf/Ranchbe.php');

class ZendRb_Application_Resource_Ranchbe extends Zend_Application_Resource_ResourceAbstract {
	public function init() {
		if (ini_get ( 'register_globals' ) == true) {
			print 'Before use RanchBE you must set Register_global php directive to off in php.ini file.
		      You can too set this directive in .htaccess file. See php documentation.';
		}
		Ranchbe::checkDirectories();
		foreach(Ranchbe::getConfig ()->path->reposit as $path){
			Rb_Filesystem::setAuthorized(realpath($path));
		}
		foreach(Ranchbe::getConfig ()->path->read as $path){
			Rb_Filesystem::setAuthorized(realpath($path));
		}
		foreach(Ranchbe::getConfig ()->path->scripts as $path){
			Rb_Filesystem::setAuthorized(realpath($path));
		}
		if(is_array(Ranchbe::getConfig ()->path->authorized))
		foreach(Ranchbe::getConfig ()->path->authorized as $path){
			Rb_Filesystem::setAuthorized(realpath($path));
		}
		if(!defined('DEFAULT_TRASH_DIR'))
			define('DEFAULT_TRASH_DIR', Ranchbe::getConfig ()->path->reposit->trash);
		
		Rb_Filesystem::setAuthorized(DEFAULT_TRASH_DIR);
			
		if(!defined('LOG'))
			define('LOG', 3);
		
		if(!defined('ERROR'))
			define('ERROR', 1);
	}

	public function desactivate() {
		// Display the template
		//self::initView ();
		//self::getSmarty ()->assign ( 'isDesctivated_message', DESACTIVATE_MESSAGE ); //Assign variable to smarty
		//self::getSmarty ()->assign ( 'accueilTab', 'active' );
		//self::getSmarty ()->assign ( 'mid', 'isDesactivated.tpl' );
		//self::getSmarty ()->display ( 'ranchbe.tpl' );
		die ();
	}
}

