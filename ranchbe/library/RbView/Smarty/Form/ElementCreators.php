<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once ('HTML/QuickForm.php'); //Librairy to easily create forms
require_once ('HTML/QuickForm/Renderer/ArraySmarty.php'); //Lib to use Smarty with QuickForm

function ArraytoHidden($array, &$form, $keyname = '') {
	unset ( $array ['tab'], $array ['kt_language'], $array ['tz_offset'], $array ['username'], $array ['ml_id'], $array ['PHPSESSID'] );
	$html = '';
	foreach ( $array as $key => $val ) {
		if (is_array ( $val )) {
			$html .= ArraytoHidden ( $val, $form, $key . '[]' );
		} else {
			if (! empty ( $keyname ))
				$key = $keyname;
			$form->addElement ( 'hidden', $key, $val );
		}
	}
}

/**
 * 
 * @param array $list
 * @param array $params
 * @param unknown_type $form
 * @param unknown_type $validation
 */
function construct_select(Array $list, Array $params, &$form, $validation = 'client') {
	/*
	  $params = array(
	            'selectdb_where',
	            'property_name',
	            'property_description'
	            'default_value',
	            'is_multiple',
	            'is_required',
	            'adv_select',
	            'display_both',
	            'disabled',
	            'field_id',
	            'onChange',
	            'onClick',
	  );
  */
	
	//$params['display_both'] = true;
	//$params['adv_select'] = true;
	

	if (empty ( $params ['field_id'] ))
		$params ['field_id'] = $params ['property_name'];
	
	if (! isset ( $params ['property_description'] ))
		$params ['property_description'] = tra ( $params ['property_name'] );
	
	if (! isset ( $params ['property_fieldname'] ))
		$params ['property_fieldname'] = $params ['property_name'];
		
	//Construct array for selection set
	if ($params ['adv_select'] == 0)
		$SelectSet [NULL] = ''; //Leave a blank option for default none selected. Not useful for advanced select
	

	if (is_array ( $list )) {
		foreach ( $list as $key => $values )
			if ($params ['display_both'])
				$SelectSet [$key] = $key . '-' . $values;
			else
				$SelectSet [$key] = $values;
	} else
		$SelectSet = array ();
	
	if (! empty ( $params ['default_value'] ))
		$form->setDefaults ( array ($params ['property_fieldname'] => $params ['default_value'] ) );
	
	if ($params ['adv_select'] == 1) {
		$form->removeAttribute ( 'name' ); // XHTML compliance
		//Construct object for advanced select
		require_once ('HTML/QuickForm/advmultiselect.php');
		$select = & $form->addElement ( 'advmultiselect', $params ['property_fieldname'], tra ( $params ['property_description'] ), $SelectSet, array ('size' => 5, 'class' => 'pool', 'style' => 'width:300px;' ) );
		
		$select->setLabel ( array (tra ( $params ['property_description'] ), 'Available', 'Selected' ) );
		
		$select->setButtonAttributes ( 'add', array ('value' => 'Add >>', 'class' => 'inputCommand' ) );
		$select->setButtonAttributes ( 'remove', array ('value' => '<< Remove', 'class' => 'inputCommand' ) );
	} else {
		//Construct object for normal select
		$attributes = array ();
		$attributes ['id'] = $params ['field_id'];
		if (! empty ( $params ['disabled'] ))
			$attributes [] = $params ['disabled'];
		if (! empty ( $params ['onChange'] ))
			$attributes ['onChange'] = $params ['onChange'];
		if (! empty ( $params ['onClick'] ))
			$attributes ['onClick'] = $params ['onClick'];
		
		$select = & $form->addElement ( 'select', $params ['property_fieldname'], $params ['property_description'], $SelectSet, $attributes );
		
		//Set the size
		if (isset ( $params ['property_length'] ))
			$select->setSize ( $params ['property_length'] );
		else
			$select->setSize ( 5 );
			
		//Enable multi selection
		if ($params ['is_multiple'])
			$select->setMultiple ( true );
		else
			$select->setMultiple ( false );
	}
	
	//Add required rule
	if ($params ['is_required'])
		$form->addRule ( $params ['property_fieldname'], tra ( 'is required' ), 'required', $validation );
} // End of method

//------------------------------------------------------------------------------
function construct_select_partner($params, &$form, $validation = 'client') {
	$params ['select'] = array ('partner_id', 'partner_number' );
	if (! empty ( $params ['selectdb_where'] ))
		$params ['where'] = array ($params ['selectdb_where'] );
	$params ['sort_order'] = 'ASC';
	$params ['sort_field'] = 'partner_number';
	$partnerList = Rb_Partner::get ()->getAll ( $params );
	$partnerSelectSet = array();
	//Construct array for set partner select options
	if (is_array ( $partnerList )) {
		if (! $params ['return_name'])
			foreach ( $partnerList as $key => $values ) {
				$partnerSelectSet [$values ['partner_id']] = $values ['partner_number'];
			}
		else
			foreach ( $partnerList as $key => $values ) {
				$partnerSelectSet [$values ['partner_number']] = $values ['partner_number'];
			}
	} else
		$partnerSelectSet = array ();
	construct_select ( $partnerSelectSet, $params, $form, $validation );
} // End of method


//------------------------------------------------------------------------------
function construct_select_doctype(Array $params, &$form, $validation = 'client') {
	$p = array();
	if (! empty ( $params ['selectdb_where'] ))
		$p ['where'] [] = $params ['selectdb_where'];
	$p ['select'] = array ('doctype_id', 'doctype_number' );
	$p ['sort_order'] = 'ASC';
	$p ['sort_field'] = 'doctype_number';
	$doctypes = Rb_Doctype::get()->getAll ( $p );
	$doctypeSelectSet = array();
	if (! isset ( $params ['return_name'] ))
		$params ['return_name'] = true;
	//Construct array for set partner select options
	if (! $params ['return_name']) {
		foreach ( $doctypes as $key => $values ) {
			$doctypeSelectSet [$values ['doctype_id']] = $values ['doctype_number'];
		}
	} else {
		foreach ( $doctypes as $key => $values ) {
			$doctypeSelectSet [$values ['doctype_number']] = $values ['doctype_number'];
		}
	}
	construct_select ( $doctypeSelectSet, $params, $form, $validation );
} // End of method


//------------------------------------------------------------------------------
function construct_select_indice($params, &$form, $validation = 'client') {
	//Get list of indice
	$indice_list = Rb_Indice::singleton ()->getIndices ( 0 );
	if (empty ( $params ['default_value'] ))
		$params ['default_value'] = 1;
	construct_select ( $indice_list, $params, $form, $validation );
} // End of method


//------------------------------------------------------------------------------
function construct_select_user($params, &$form, $validation = 'client') {
	$list = Ranchbe::getAuthAdapter ()->getUsers ();
	$SelectSet = array();
	//Construct array for set partner select options
	if (! $params ['return_name']) {
		foreach ( $list as $key => $values ) {
			$SelectSet [$values ['auth_user_id']] = $values ['handle'];
		}
	} else {
		foreach ( $list as $key => $values ) {
			$SelectSet [$values ['handle']] = $values ['handle'];
		}
	}	
	construct_select ( $SelectSet, $params, $form, $validation );
} // End of method


//------------------------------------------------------------------------------
function construct_select_process(Array $params, &$form, $validation = 'client') {
	$processMonitor = new Galaxia_Monitor_Process ( Ranchbe::getDb() );
	$processList = $processMonitor->monitor_list_processes ( 0, 9999, 'lastModif_desc', '', $params ['selectdb_where'] );
	$processSelectSet = array();
	if (! isset ( $params ['return_name'] ))
		$params ['return_name'] = false;
	if (! $params ['return_name'])
		if (is_array ( $processList ['data'] ))
			foreach ( $processList ['data'] as $pid => $values ) {
				$processSelectSet [$pid] = $values ['name'] . ' ' . $values ['version'];
			}
		else if (is_array ( $processList ['data'] ))
			foreach ( $processList ['data'] as $pid => $values ) {
				$processSelectSet [$values ['name'] . '_' . $values ['version']] = $values ['name'] . ' ' . $values ['version'];
			}
	construct_select ( $processSelectSet, $params, $form, $validation );
} // End of method


//------------------------------------------------------------------------------
function construct_select_category($params, &$form, Rb_Object_Permanent &$object, $validation = 'client') {
	if (! isset ( $params ['return_name'] ))
		$params ['return_name'] = false;
	//Get list of categories
	if (is_a ( $object, 'Rb_Container' )) {
		$categories = Rb_Container_Relation_Category::get ()->getCategories ( $object->getId (), $params );
	} else if (is_a ( $object, 'Rb_Document' )) {
		$categories = Rb_Doctype_Relation_Category::get ()->getCategories ( $object->getProperty ( 'doctype_id' ), $params );
	} else if (is_a ( $object, 'Rb_Doctype' )) {
		$categories = Rb_Doctype_Relation_Category::get ()->getCategories ( $object->getId (), $params );
	} else if (is_a ( $object, 'Rb_Category' )) {
		$categories = $object->getAll ( $params );
	} else
		$categories = array ();
	$selectedCat = array ();
	if (! $params ['return_name'])
		foreach ( $categories as $value ) { //Rewrite result array for quickform convenance
			$selectedCat [$value ['category_id']] = $value ['category_number'];
		}
	else
		foreach ( $categories as $value ) { //Rewrite result array for quickform convenance
			$selectedCat [$value ['category_number']] = $value ['category_number'];
		}
	construct_select ( $selectedCat, $params, $form, $validation );
} // End of method


//------------------------------------------------------------------------------
function construct_select_container(Array $params, &$form, Rb_Container &$container, $validation = 'client') {
	if (! isset ( $params ['return_name'] ))
		$params ['return_name'] = false;
	$p = array (
		'sort_field' => $container->getDao ()->getFieldName ( 'number' ), 
		'sort_order' => 'ASC', 
		'select' => array (	$container->getDao ()->getFieldName ( 'number' ), 
							$container->getDao ()->getFieldName ( 'id' ) ) );
	$selectedCont = array();
	$containers = $container->getAll ( $p );
	foreach ( $containers as $value ) { //Rewrite result array for quickform convenance
		if ($params ['return_name'])
			$selectedCont [$value [$container->getDao ()->getFieldName ( 'number' )]] = $value [$container->getDao ()->getFieldName ( 'number' )];
		else
			$selectedCont [$value [$container->getDao ()->getFieldName ( 'id' )]] = $value [$container->getDao ()->getFieldName ( 'number' )];
	}
	construct_select ( $selectedCont, $params, $form, $validation );

} // End of method


//------------------------------------------------------------------------------
function construct_select_project(Array $params, &$form, Rb_Project &$project, $validation = 'client') {
	if (! isset ( $params ['return_name'] ))
		$params ['return_name'] = false;
	$p = array ('sort_field' => 'project_number', 
				'sort_order' => 'ASC', 
				'select' => array ('project_number', 'project_id' ) );
	$projects = $project->getAll ( $p );
	$selectedSet = array();
	if (! $params ['return_name'])
		//Rewrite result array for quickform convenance
		foreach ( $projects as $value ) {
			$selectedSet [$value ['project_id']] = $value ['project_number'];
		}
	else
		foreach ( $projects as $value ) {
			$selectedSet [$value ['project_number']] = $value ['project_number'];
		}
	construct_select ( $selectedSet, $params, $form, $validation );
} // End of method


//------------------------------------------------------------------------------
function construct_select_reposit($params, &$form, $validation = 'client') {
	return _construct_select_reposit ( 1, $params, $form, $validation );
} // End of method


//------------------------------------------------------------------------------
function construct_select_read($params, &$form, $validation = 'client') {
	return _construct_select_reposit ( 2, $params, $form, $validation );
} // End of method


//------------------------------------------------------------------------------
function _construct_select_reposit($type, $params, &$form, $validation) {
	if (! isset ( $params ['return_name'] ))
		$params ['return_name'] = false;
	$p = array ('sort_field' => 'reposit_number', 
				'sort_order' => 'ASC', 
				'select' => array ('reposit_id', 'reposit_number', 'reposit_name' ) );
	if ($type)
		$p ['exact_find'] = array ('reposit_type' => $type );
	$selectedSet = array();
	$reposits = Rb_Reposit::get ()->getAll ( $p );
	if (! $params ['return_name'])
		foreach ( $reposits as $value ) {
			$selectedSet [$value ['reposit_id']] = $value ['reposit_number'] . '(<i>' . $value ['reposit_name'] . '</i>)';
		}
	else
		foreach ( $projects as $value ) {
			$selectedSet [$value ['reposit_number']] = $value ['reposit_number'] . '(<i>' . $value ['reposit_name'] . '</i>)';
		}
	construct_select ( $selectedSet, $params, $form, $validation );
} // End of method


//------------------------------------------------------------------------------
function construct_select_propset($params, &$form, Rb_Propset &$propset, $validation = 'client') {
	if (! isset ( $params ['return_name'] ))
		$params ['return_name'] = false;
	$list = $propset->GetPropset ();
	$selectedSet = array();
	if (! $params ['return_name'])
		foreach ( $list as $value ) {
			$selectedSet [$value ['propset_id']] = $value ['propset_name'];
		}
	else
		foreach ( $list as $value ) {
			$selectedSet [$value ['propset_name']] = $value ['propset_name'];
		}
	construct_select ( $selectedSet, $params, $form, $validation );
} // End of method


//------------------------------------------------------------------------------
function construct_select_property($params, &$form, Rb_Dao_Abstract &$dao, $validation = 'client') {
	if (! isset ( $params ['return_name'] ))
		$params ['return_name'] = false;
	$selectedSet = array();
	$list = Rb_Metadata_Dictionary::get ()->getMetadatas ( $dao, array ('select' => array ('property_id', 'property_name', 'property_description' ) ) );
	if (! $params ['return_name'])
		foreach ( $list as $value ) {
			$selectedSet [$value ['property_id']] = $value ['property_name'] . ' (' . $value ['property_description'] . ')';
		}
	else
		foreach ( $list as $value ) {
			$selectedSet [$value ['property_name']] = $value ['property_name'] . ' (' . $value ['property_description'] . ')';
		}
	construct_select ( $selectedSet, $params, $form, $validation );
} // End of method


//------------------------------------------------------------------------------
/**
 * 
 * @param unknown_type   $params = array(
						    ['property_name']=>string
						    ['property_description']=>string
						    ['is_required']=>string
						    ['default_value']=>string
						    ['date_format']=>string / default 'dMY'
						    ['date_language']=>string / default 'fr'
 * @param unknown_type $form
 * @param unknown_type $validation
 */
function construct_select_date(Array $params, &$form, $validation = 'client') {
	if (Ranchbe::getConfig ()->date->input_method === 'jscalendar') { 
		//Use or not the jscalendar script for select date.
		require_once ('HTML/QuickForm/jscalendar.php'); //Lib to create date selection
		if (isset ( $params ['date_format'] )) {
			$optionsjsc ['daFormat'] = $params ['date_format']; //Displayed date format. '%Y/%m/%d' by default
		}
		if (isset ( $params ['daFormat'] ))
			$optionsjsc ['daFormat'] = $params ['daFormat']; //Displayed date
		else
			$optionsjsc ['daFormat'] = Ranchbe::getConfig ()->date->format->long; //Displayed date
		if (isset ( $params ['ifFormat'] ))
			$optionsjsc ['ifFormat'] = $params ['ifFormat']; //Format of return date. Timestamp by default
		if (isset ( $params ['displayArea'] ))
			$optionsjsc ['displayArea'] = $params ['displayArea'];
		if (isset ( $params ['button'] ))
			$optionsjsc ['button'] = $params ['button'];
		if (isset ( $params ['showsTime'] ))
			$optionsjsc ['showsTime'] = $params ['showsTime'];
		
		if (isset ( $params ['default_value'] )) {
			$optionsjsc ['date'] = $params ['default_value'];
			$attributes ['value'] = $params ['default_value'];
		}
		
		//If ifFormat=%s (to return timestamp), jscalendar return always to day date and ignore previous selected date when select a date
		//So we must set a format for ifFormat and create an hidden field to return timestamp.
		$optionsjsc ['timestamp'] = true; //to return timestamp in an hidden field
		$optionsjsc ['ifFormat'] = '%Y/%m/%d %H:%M:%S'; //Format of return date. Timestamp by default

		if (! isset ( $params ['property_description'] ))
			$params ['property_description'] = $params ['property_name'];
		
		if (! isset ( $params ['property_fieldname'] ))
			$params ['property_fieldname'] = $params ['property_name'];
		
		$attributes ['size'] = $params ['size'];
		
		$calendar = & $form->addElement ( 'jscalendar', $params ['property_fieldname'], tra ( $params ['property_description'] ), $optionsjsc, $attributes );
		$calendar->basePath = Ranchbe::getConfig()->js->baseurl.'jscalendar/';
		$calendar->lang = Ranchbe::getConfig()->resources->translate->lang;
		
		if ($params ['is_required'])
			$form->addRule ( $params ['property_fieldname'], tra ( 'is required' ), 'required', null, $validation );
		
		if (isset ( $params ['showsTime'] ))
			$calendar->_config ['daFormat'] = 
				Ranchbe::getConfig()->date->format->long;
	
	} else {
		
		//Option to manage display of a date input
		if (! isset ( $params ['language'] ))
			$params ['language'] = 
				Ranchbe::getConfig()->resources->translate->lang;
		if (! isset ( $params ['date_format'] ))
			$params ['format'] = 'dMY';
		else
			$params ['format'] = $params ['date_format'];
		if (! isset ( $params ['default_value'] ))
			$params ['default_value'] = time ();
		if (! isset ( $params ['property_description'] ))
			$params ['property_description'] = $params ['property_name'];
		if (! isset ( $params ['property_fieldname'] ))
			$params ['property_fieldname'] = $params ['property_name'];
			
		//Option to manage display of a date input
		$options = array ('language' => $params ['language'], 'format' => $params ['format'], 'minYear' => date ( 'Y' ), 'maxYear' => date ( 'Y', time () + 20 * 365 * 24 * 3600 ) );
		
		$form->setConstants ( array (
			$params ['property_fieldname'] => $params ['default_value'] ) );
		
		$form->addElement ( 'date', $params ['property_fieldname'], tra ( $params ['property_description'] ), $options );
		
		//Add required rule
		if ($params ['is_required'])
			$form->addRule ( $params ['property_fieldname'], tra ( 'is required' ), 'required', null, $validation );
	}
} // End of method


//------------------------------------------------------------------------------
/**
 * 
 * @param array   $field = array(
				    ['property_name']=>string
				    ['property_description']=>string
				    ['property_type']=>string
				    ['field_regex']=>string
				    ['is_required']=>string
				    ['is_multiple']=>string
				    ['property_length']=>string
				    ['return_name']=>string
				    ['field_list']=>string
				    ['selectdb_where']=>string
				    ['field_regex_message']=>string
				    ['default_value']=>string
				    ['sort_order']=>string
				    ['sort_field']=>string
				    ['table_name']=>string
				    ['field_for_value']=>string
				    ['field_for_display']=>string
				    ['date_format']=>string
				    ['date_language']=>string / default 'fr'
				    ['display_both']=>bool / default false
				    ['disabled']=>string
 * @param unknown_type $form
 * @param unknown_type $validation
 */
function construct_element(Array $field, &$form, $validation = 'client') {
	if (! isset ( $field ['property_type'] ))
		return false;
	if (! isset ( $field ['property_fieldname'] ))
		return false;
	if (! isset ( $field ['property_name'] ))
		$field ['property_name'] = $field ['property_fieldname'];
	if ($field ['property_description'])
		$field ['property_name'] = $field ['property_name'] . '<br /><i>' . $field ['property_description'] . '</i>';
	if (empty ( $field ['field_id'] ))
		$field ['field_id'] = $field ['property_fieldname'];
	
	switch ($field ['property_type']) {
		case 'text' :
			if (! isset ( $field ['property_length'] ) || is_null ( $field ['property_length'] ))
				$field ['property_length'] = 20;
			$form->addElement ( 'text', $field ['property_fieldname'], $field ['property_name'], array ('value' => $field ['default_value'], 'size' => $field ['property_length'], 'maxlength' => $field ['property_length'], $field ['disabled'], 'id' => $field ['field_id'] ) );
			$form->addRule ( $field ['property_fieldname'], tra ( 'should be less than or equal to' ) . ' ' . $field ['property_length'] . ' characters', 'maxlength', $field ['property_length'], $validation );
			if (! empty ( $field ['field_regex'] )) { //Add rule with regex
				if (empty ( $field ['field_regex_message'] ))
					$field ['field_regex_message'] = tra ( 'invalid format' );
				$form->addRule ( $field ['property_fieldname'], $field ['field_regex_message'], 'regex', '/' . $field ['field_regex'] . '/', $validation );
			}
			if ($field ['is_required']) { //Add rule with require option
				$form->addRule ( $field ['property_fieldname'], tra ( 'is required' ), 'required', null, $validation );
			}
			break;
		
		case 'long_text' :
			if (! isset ( $field ['property_length'] ))
				$field ['property_length'] = 20;
			$form->addElement ( 'textarea', $field ['property_fieldname'], $field ['property_name'], array ('value' => $field ['default_value'], 'cols' => $field ['property_length'], $field ['disabled'], 'id' => $field ['field_id'] ) );
			if (! empty ( $field ['field_regex'] )) //Add rule with regex
				if (empty ( $field ['field_regex_message'] ))
					$field ['field_regex_message'] = tra ( 'invalid format' );
			$form->addRule ( $field ['property_fieldname'], $field ['field_regex_message'], 'regex', '/' . $field ['field_regex'] . '/m', $validation );
			if ($field ['is_required']) //Add rule with require option
				$form->addRule ( $field ['property_fieldname'], tra ( 'is required' ), 'required', null, $validation );
			break;
		
		case 'html_area' :
			//http://www.nabble.com/HTML-QuickForm-elments-for-HTMLArea-(xinha),-FCKeditor-and-jscalendar-td4523180.html
			if (! isset ( $field ['property_length'] )) //set default value
				$field ['property_length'] = 20; //for number of lines, col is fixed to 80
			
			require_once ('HTML/QuickForm/htmlarea.php');
			$options = array ('url' => 'lib/htmlarea/',
								'lang' => Ranchbe::getConfig()->resources->translate->lang,
								'cssURL' => 'style/default/htmlarea.css',
								$field ['disabled'] );
			$attributes = array ('rows' => $field ['property_length'], 'cols' => 80 );
			$config = array ('width' => '500px', 'height' => '400px', 'imgURL' => 'lib/htmlarea/images/', 'popupURL' => 'lib/htmlarea/popups/' );
			
			$form->addElement ( 'htmlarea', $field ['property_fieldname'], $field ['property_name'], $options, $attributes );
			$form->setDefaults ( array ($field ['property_fieldname'] => $field ['default_value'] ) );
			
			$htmlarea = &$form->getElement ( $field ['property_fieldname'] );
			$htmlarea->setURL ( 'lib/htmlarea/' );
			$htmlarea->setLang ( Ranchbe::getConfig()->resources->translate->lang );
			$htmlarea->setConfig ( $config );
			$htmlarea->setConfig ( 'statusBar', true );
			$htmlarea->hideButton ( 'popupeditor' );
			
			//$htmlarea->registerPlugin('CSS', $css_options);
			//$htmlarea->registerPlugin('TableOperations');
			
			break;
		
		case 'partner' :
			construct_select_partner ( $field, $form );
			break;
		
		case 'doctype' :
			construct_select_doctype ( $field, $form );
			break;
		
		case 'indice' :
			construct_select_indice ( $field, $form );
			break;
		
		case 'user' :
			construct_select_user ( $field, $form );
			break;
		
		case 'process' :
			construct_select_process ( $field, $form );
			break;
		
		case 'date' :
			construct_select_date ( $field, $form );
			break;
		
		case 'integer' :
			if (! isset ( $field ['property_length'] ))
				$field ['property_length'] = 20;
			$form->addElement ( 'text', $field ['property_fieldname'], $field ['property_name'], array ('value' => $field ['default_value'], 'size' => $field ['property_length'], 'id' => $field ['field_id'] ) );
			if ($field ['is_required']) //Add rule with require option
				$form->addRule ( $field ['property_fieldname'], tra ( 'is required' ), 'required', null, $validation );
			$form->addRule ( $field ['property_fieldname'], tra ( 'should be numeric' ), 'numeric', null, $validation );
			break;
		
		case 'decimal' :
			if (! isset ( $field ['property_length'] ))
				$field ['property_length'] = 20;
			$form->addElement ( 'text', $field ['property_fieldname'], $field ['property_name'], array ('value' => $field ['default_value'], 'size' => $field ['property_length'], $field ['disabled'], 'id' => $field ['field_id'] ) );
			if ($field ['is_required']) //Add rule with require option
				$form->addRule ( $field ['property_fieldname'], tra ( 'is required' ), 'required', null, $validation );
			$form->addRule ( $field ['property_fieldname'], tra ( 'should be numeric' ), 'numeric', null, $validation );
			break;
		
		case 'select' :
			$list = explode ( '#', $field ['field_list'] );
			$list = array_combine ( $list, $list );
			construct_select ( $list, $field, $form );
			break;
		
		case 'selectFromDB' :
			$pget ['sort_field'] = $field ['field_for_display'];
			$pget ['sort_order'] = 'ASC';
			$pget ['maxRecords'] = 10000;
			$pget ['select'] = array ($field ['field_for_value'], $field ['field_for_display'] );
			//$list = $Manager->Get($field['table_name'] , $pget , 'all' , false);
			$dao = new Rb_Dao ( Ranchbe::getDb () );
			$dao->setTable ( $field ['table_name'] );
			$list = $dao->getAll ( $pget );
			
			//Construct array for set select options
			if (! $field ['return_name']) {
				foreach ( $list as $key => $values ) {
					$SelectSet [$values [$field ['field_for_value']]] = $values [$field ['field_for_display']];
				}
			} else {
				foreach ( $list as $key => $values ) {
					$SelectSet [$values [$field ['field_for_display']]] = $values [$field ['field_for_display']];
				}
			}
			
			construct_select ( $SelectSet, $field, $form );
			break;
		
		case 'liveSearch' :
			require_once ('HTML/QuickForm/livesearch_select.php');
			$form->addElement ( 
				'livesearch_select', 
				$field ['property_fieldname'], 
				tra ( $field ['property_description'] ), 
				array ('elementId' => 'qflssearch', //element id, name must be same that method in .class.php
						//'callback' => array('Test', 'getTestName'),//callback function to retrieve value from ID selection
						//'dbh' => Ranchbe::getDb(),//optional handler for callback function
						'style' => '', //optional class for style not set or '' ==> default
						'divstyle' => '', //optional class for style not set or '' ==> default
						'ulstyle' => '', //optional class for style not set or '' ==> default
						'listyle' => '', //optional class for style not set or '' ==> default
						'searchZeroLength' => 1, //enable the search request with 0 length keyword(Default 0)
						'buffer' => 350, //set the interval single buffer send time (ms)
						'printStyle' => 1, //anything != 0 will render css inline(Default 1), 0 => the default style will not be rendered, you should put it in your style.css(XHTML fix)
						'autoComplete' => 1, //if 0 the autocomplete attribute will not be set. Default not set;
						'autoserverPath' => '', //path to auto_server.php file with trailing slash NOTE: check path for all files included in autoserver.php
						'query' => 'SELECT ' . $field ['field_for_display'] . ', ' . $field ['field_for_value'] . ' FROM ' . $field ['table_name'] . ' WHERE ' . $field ['field_for_display'] . " LIKE '%search_term%%'" . ' ORDER BY ' . $field ['field_for_display'] . ' ASC' ), array ('size' => $field ['property_length'], $field ['disabled'] ) );
			break;
		default :
			break;
	} //End of switch
	
	//@todo : revoir ca qui fait double emploie avec les operation du select...
	//Set the default value of the field from field['default_value']
	// -- if its a multiple field, explode the string value into array
	if ($field ['is_multiple'] && isset ( $field ['default_value'] )) {
		$field ['default_value'] = explode ( '#', $field ['default_value'] );
	}
	
	if (isset ( $field ['default_value'] ))
		$form->setDefaults ( array ($field ['property_fieldname'] => $field ['default_value'] ) );
	else
		$form->setDefaults ( array ($field ['property_fieldname'] => '' ) );

} // End of method

