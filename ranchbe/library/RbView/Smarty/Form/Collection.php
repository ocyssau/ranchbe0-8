<?php
require_once('HTML/QuickForm.php'); //Librairy to easily create forms

class RbView_Smarty_Form_Collection extends HTML_QuickForm{

	protected $_subforms = array();

	public $_headerTemplate =
      "\n\t<tr>\n\t\t<td style=\"white-space: nowrap; background-color: #CCCCCC;\" align=\"left\" valign=\"top\" colspan=\"2\"><b>{header}</b></td>\n\t</tr>";

	public $_elementTemplate =
      "\n\t<tr>\n\t\t<td align=\"right\" valign=\"top\">
      <!-- BEGIN required --><span style=\"color: #ff0000\">*</span>
      <!-- END required -->
      <b>{label}</b></td>\n\t\t<td valign=\"top\" align=\"left\">
      <!-- BEGIN error --><span style=\"color: #ff0000\">{error}</span><br />
      <!-- END error -->\t{element}</td>\n\t</tr>";

	public $_formTemplate =
      "\n<form{attributes}>\n<div>\n{hidden}<table class=\"normal\">\n{content}\n</table>\n</div>\n</form>";

	public $_requiredNoteTemplate =
      "\n\t<tr>\n\t\t<td></td>\n\t<td align=\"left\" valign=\"top\">{requiredNote}</td>\n\t</tr>";

	public function __construct($formName='', $method='post', $action='', $target='', $attributes=null, $trackSubmit = false){
		parent::HTML_QuickForm($formName, $method, $action, $target, $attributes, $trackSubmit);
	}

	//-------------------------------------------------------------------------
	/** Add a subform to this collection
	 * 
	 * @param $subform
	 */
	public function add(&$subform){
		if (is_object($subform) && is_subclass_of($subform, 'HTML_QuickForm')) {
			$this->_subforms[] =& $subform;
			return $this->_subforms;
		}else{
			return false;
		}
	} //End of method

	//-------------------------------------------------------------------------
	/** Returns a reference to default renderer object
	 * 
	 * @return object a default renderer object
	 * 
	 */
	public function defaultRenderer()
	{
		if( !isset($this->renderer) ){
			require_once('HTML/QuickForm/Renderer/Default.php');
			$this->renderer = new HTML_QuickForm_Renderer_Default();
		}
		$this->renderer->setFormTemplate($this->_formTemplate);
		$this->renderer->setHeaderTemplate($this->_headerTemplate);
		$this->renderer->setElementTemplate($this->_elementTemplate);
		$this->renderer->setRequiredNoteTemplate($this->_requiredNoteTemplate);
		return $this->renderer;
		
	} // end func defaultRenderer

	//-------------------------------------------------------------------------
	/** Accepts a renderer
	 * 
	 * @param HTML_QuickForm_Renderer     An HTML_QuickForm_Renderer object
	 * @return void
	 */
	function accept(HTML_QuickForm_Renderer $renderer)
	{
		$renderer->startForm($this);
		foreach($this->_subforms as $subform){
			$renderer->_html .= $subform->toHtml();
		}
		foreach (array_keys($this->_elements) as $key) {
			$element =& $this->_elements[$key];
			$elementName = $element->getName();
			$required    = ($this->isElementRequired($elementName) && !$element->isFrozen());
			$error       = $this->getElementError($elementName);
			$element->accept($renderer, $required, $error);
		}
		$renderer->finishForm($this);
	} // end func accept

	//-------------------------------------------------------------------------
	/*! \brief freeze all subform
	 *
	 */
	function freeze($elementList=null){
		foreach($this->_subforms as $subform){
			$subform->freeze($elementList);
		}
		parent::freeze($elementList);
		return true;
	} //End of method

	//-------------------------------------------------------------------------
	/*! \brief Returns a reference to the subform
	 * @param     string     $element    Element name
	 * @return    object     reference to element
	 */
	function getSubform()
	{
		return $this->_subforms;
	} // end func getElement

} //End of class

