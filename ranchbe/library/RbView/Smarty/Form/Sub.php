<?php
require_once('HTML/QuickForm.php'); //Librairy to easily create forms

class RbView_Smarty_Form_Sub extends HTML_QuickForm{

function __construct($formName='', $method='post', $action='', $target='', $attributes=null, $trackSubmit = false){
  parent::HTML_QuickForm($formName, $method, $action, $target, $attributes, $trackSubmit);
  $this->defaultRenderer()->setFormTemplate("\n{hidden}\n{content}\n"); //redefinis le template pour ne pas afficher les balises <form></form>

} //End of method

} //End of class

