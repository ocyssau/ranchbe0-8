<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Smarty process modifier plugin
 *
 * Type:     modifier<br>
 * Name:     access_code<br>
 * Purpose:  replace access code by a human text<br>
 * Input: $access_code<br>
 */
function smarty_modifier_access_code($access_code='0'){
  switch($access_code){
  case 0:
    return '<font color="green">'.tra('<b>F</b>ree').'</font>';
    break;
  case 1:
    return '<font color="OrangeRed">'.tra('<b>C</b>heckout').'</font>';
    break;
  case 5:
    return '<font color="Blue">'.tra('In<b>W</b>orkflow').'</font>';
    break;
  case 10:
    return '<font color="Orange">'.tra('<b>V</b>alidate').'</font>';
    break;
  case 11:
    return '<font color="red">'.tra('<b>L</b>ocked').'</font>';
    break;
  case 12:
    return '<font color="red">'.tra('<b>M</b>arked to suppress').'</font>';
    break;
  case 15:
    return '<font color="red">'.tra('<b>H</b>istoric').'</font>';
    break;
  default:
    return '(Code='.$access_code.')';
    break;
  }
}//End of function
