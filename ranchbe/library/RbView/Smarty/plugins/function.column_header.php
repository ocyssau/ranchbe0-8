<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {column_header} function plugin
 *
 * Type:     function<br>
 * Name:     column_header<br>
 * Purpose:  make a header for table list with link to activate or not the sort
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_column_header($params, &$smarty){
  $request =& Zend_Controller_Front::getInstance()->getRequest();
  $url = $request->getModuleName().'/'.$request->getControllerName().'/'.$request->getActionName();

  if( $params['sort_order'] == 'INV' ){
    if( $request->getParam('sort_order') == 'ASC' ) $params['sort_order'] = 'DESC';
    else $params['sort_order'] = 'ASC';
  }

  $title = $params['title'];
  unset($params['title']);
  if( !$title )
    $title = $params['sort_field'];
  
  if( is_array($params) ){
    $p = '';
    foreach($params as $name=>$val) {
      $url .= '/'.$name.'/'.$val;
  }}

  $html = '<a class="tableheading" href="'.$url.'">'.tra($title).'</a>';

  if($params['sort_field'] == $request->getParam('sort_field')){
    if( $params['sort_order'] == 'ASC'){
      $html .= '<img src="./img/icons/desc.png">';
    }else if( $params['sort_order'] == 'DESC'){
      $html .= '<img src="./img/icons/asc.png">';
    }
  }

  return $html;
} //End of function

