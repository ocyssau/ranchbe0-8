<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {product_action_button} function plugin
 *
 * Type:     function<br>
 * Name:     product_action_button<br>
 * Purpose:  return buttons for products action
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_product_action_button($params, &$smarty)
{
  $html = '';
  include_once('GUI/productaction.php');

  if($params['inForm'])
    $html = RbView_Action_Document::initJsForm();
  else
    $html = RbView_Action_Document::initJs();
  
  if($params['ifSuccessAction'] && $params['ifSuccessController'] 
                                && $params['ifSuccessModule']){
    $ifSuccessForward = '/ifSuccessModule/'.$params['ifSuccessModule'].
                        '/ifSuccessController/'.$params['ifSuccessController'].
                        '/ifSuccessAction/'.$params['ifSuccessAction'];
  }

  if($params['ifFailedModule'] && $params['ifFailedController'] 
                                && $params['ifFailedAction']){
    $ifFailedForward =  '/ifFailedModule/'.$params['ifFailedModule'].
                        '/ifFailedController/'.$params['ifFailedController'].
                        '/ifFailedAction/'.$params['ifFailedAction'];
  }

  if(!$params['infos']) $params['infos'] = array();

  foreach(GUI_productaction::$action_def as $actionId=>$class){
    $actionHtml = '';
    $action = GUI_productaction::get($actionId, $params['infos'],
                          $params['form'], $params['ticket'],
                          $ifSuccessForward, $ifFailedForward,
                          $params['displayText']);
    if($action->isActive && $params['mode'] == 'button'){
      $actionHtml = $action->toButton();
    }else if($action->isActive){
      $actionHtml = $action->toHref();
    }

    if($action->isActive && $params['mode'] == 'list'){
      $html .= '<li>'.$actionHtml.'</li>';
    }else{
      $html .= $actionHtml;
    }
  }

  return $html;

} //End of function

