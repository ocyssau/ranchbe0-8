<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * Type:     function<br>
 * Name:     reposit_action_button<br>
 * Purpose:  return buttons for reposit action
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_reposit_action_button($params, &$smarty) {
	if($params['mode'] == 'main'){
		if($params['reposit_type'] == 1){
			$menu = new RbView_Menu_Reposit_Main($params, 'Form');
		}else{
			$menu = new RbView_Menu_RepositRead_Main($params, 'Form');
		}
		return $menu->render();
	}else{
		if($params['reposit_type'] == 1){
			$menu = new RbView_Menu_Reposit_Contextual($params, 'Href');
		}else{
			$menu = new RbView_Menu_RepositRead_Contextual($params, 'Href');
		}
		return $menu->render();
	}
} //End of function

