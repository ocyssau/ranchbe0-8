<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {enable_dojo} function plugin
 *
 * Type:     function<br>
 * Purpose:  init header for dojo
 */

function smarty_function_enable_dojo($params, &$smarty){
	if( !$params['view']->dojo()->isEnabled() ) {
		$params['view']->dojo()->enable();
	}
	return $params['view']->dojo();
	/*
	if($params['view']->dojo()->isEnabled()) {
		return $params['view']->dojo();
	}
	*/
} //End of function

