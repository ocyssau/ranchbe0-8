<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Purpose:  replace space id by space name<br>
 * Input: $type<br>
 */
function smarty_modifier_space($id){
  switch($id){
  	case 10:
  		return tra('bookshop');
  		break;
  	case 15:
  		return tra('cadlib');
  		break;
  	case 20:
  		return tra('mockup');
  		break;
  	case 25:
  		return tra('workitem');
  		break;
  	default:
  		return tra('undefined');
  }
}
