<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     modifier.tr.php
 * Type:     modifier
 * Name:     tr
 * Purpose:  translate a block of text
 * 
 * See too block.tr.php
 * -------------------------------------------------------------
 */

function smarty_modifier_tr($text) {
	if ($text) return tra ( $text );
	return '';
}
