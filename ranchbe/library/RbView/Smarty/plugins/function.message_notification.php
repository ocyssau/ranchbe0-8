<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {message_notification} function plugin
 *
 * Type:     function<br>
 * Purpose:  display a alert of new message if necessary
 */
//require_once('core/message/notification.php');

function smarty_function_message_notification($params, &$smarty){
  $count_message = Rb_Message_Notification::hasMessage( Rb_User::getCurrentUser()->getId() );
  if( $count_message ){
	    $smarty->assign('message_notification', 
	                    sprintf( tra('you have %s new messages'), $count_message) );
	    $smarty->display('messu/notification.tpl');
  }
} //End of function


