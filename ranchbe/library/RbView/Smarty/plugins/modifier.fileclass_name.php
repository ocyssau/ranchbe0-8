<?php
/**
 * Ranchbe plugin
 */

/**
 * Type:     modifier<br>
 * Purpose:  replace file_class id by his name<br>
 * @author   Olivier Cyssau
 * @version  1.0
 */
function smarty_modifier_fileclass_name($int=0){
  switch($int){
    case(0):
      return 'MAIN';
      break;
    case(1):
      return 'VISU';
      break;
    case(2):
      return 'PICTURE';
      break;
    case(3):
      return 'ANNEX';
      break;
    default:
      return 'UNKNOW';
  }
}
