<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {get_myspaceTabs} function plugin
 *
 * Type:     function<br>
 * Name:     get_myspaceTabs<br>
 * Purpose:  genereate tabs for myspace
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_get_myspaceTabs($params, &$smarty)
{
  $context =& Ranchbe::getContext();
var_dump($params);
  if( $context->getProperty('container_id') ){
    if($params['documentManager'])
    echo '<li class="'.$params['documentManager'].'"><a href="documentManager/index/get">'
          .$context->getProperty('container_number').'</a></li>';

    if($params['recordfileManager'])
    echo '<li class="'.$params['recordfileManager'].'"><a href="recordfileManager/index/get">'
          .$context->getProperty('container_number').'</a></li>';
  
    if($params['DisplayDocfileTab'])
    echo '<li class="'.$params['DisplayDocfileTab'].'"><a href="docfileManager/index/index/space/'
          .$context->getProperty('space').'/container_id/'
          .$context->getProperty('container_id').'">'
          .$context->getProperty('container_number').'_Files</a></li>';
  }
  echo '<li class="'.$params['wildspaceTab'].'"><a href="wildspace">'.tra('Wildspace').'</a></li>';

  if( $context->getProperty('container_id') ){
    echo '<li class="'.$params['containerConsultTab'].'"><a href="context/consult/index">'.tra('Consultation').'</a></li>';
  }

} //End of function
?>
