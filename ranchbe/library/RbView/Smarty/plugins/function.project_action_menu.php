<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {project_action_button} function plugin
 *
 * Type:     function<br>
 * Name:     project_action_button<br>
 * Purpose:  return buttons for project action
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_project_action_menu(array $params, &$smarty){
	if($params['mode'] == 'main'){
		$menu = new RbView_Menu_Project_Main($params, 'Form');
		return $menu->render();
	}else{
		$menu = new RbView_Menu_Project_Contextual($params, 'Href');
		return $menu->render();
	}
}

