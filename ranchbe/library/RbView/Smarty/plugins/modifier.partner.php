<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Type:     modifier<br>
 * Purpose:  replace partner id by the partner name<br>
 * Input: $partner_id<br>
 */
function smarty_modifier_partner($partner_id='0'){
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('function','filter_select');
  return smarty_filter_select_partner($partner_id);
}
