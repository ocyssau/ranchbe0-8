<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Purpose:  replace reposit id by the reposit number<br>
 * Input: $id<br>
 */
function smarty_modifier_reposit($id='0'){
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('function','filter_select');
  return smarty_filter_select_reposit($id);
}
