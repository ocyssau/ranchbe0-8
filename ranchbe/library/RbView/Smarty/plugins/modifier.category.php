<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Type:     modifier<br>
 * Purpose:  replace category id by the category name<br>
 * Input: $category_id<br>
 */
function smarty_modifier_category($category_id='0'){
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('function','filter_select');
  return smarty_filter_select_category($category_id);
}

