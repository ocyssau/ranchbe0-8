<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Smarty username modifier plugin
 *
 * Type:     modifier<br>
 * Name:     username<br>
 * Purpose:  replace user id by the user name<br>
 * Input: user_id<br>
 */
function smarty_modifier_username($user_id='0'){
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('function','filter_select');
  return smarty_filter_select_username($user_id);
}
