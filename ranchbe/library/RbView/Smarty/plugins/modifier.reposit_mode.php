<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Purpose:  replace reposit mode by mode name<br>
 * Input: $type<br>
 */
function smarty_modifier_reposit_mode($mode){
  switch($mode){
  	case 1:
  		return tra('by symlink');
  		break;
  	case 2:
  		return tra('by copy');
  		break;
  	case 3:
  		return tra('by hardlink');
  		break;
  	default:
  		return tra('undefined');
  }
}
