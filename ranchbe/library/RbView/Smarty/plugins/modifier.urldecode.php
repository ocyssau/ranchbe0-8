<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */
/**
 * RanchBE urldecode modifier plugin
 *
 * Type:     modifier<br>
 * Purpose:  decode a url string endoce with urlencode php function<br>
 * Input:<br>
 *         - string: input encode url string
 * @return string|void
 */
function smarty_modifier_urldecode($string){
    if(!empty($string)) {
        return urldecode($string);
    } else {
        return;
    }
}
