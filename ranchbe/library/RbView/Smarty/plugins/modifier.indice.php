<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Name:     indice<br>
 * Purpose:  replace indice id by indice name<br>
 * Input: $indice_id<br>
 */
function smarty_modifier_indice($indice_id='0'){
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('function','filter_select');
  return smarty_filter_select_indice($indice_id);
}
