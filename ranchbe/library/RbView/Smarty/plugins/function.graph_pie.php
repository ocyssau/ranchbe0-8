<?php
/**
 * Ranchbe plugin
 *


/**
 * Ranchbe {file_icon} function plugin
 *
 * Type:     function<br>
 * Name:     graph_pie<br>
 * Date:     Mar 30, 2007<br>
 * Purpose:  Display pie graph from data array<br>
 * Input:<br>
 *         - values = array of values
 *         - legend = array of legend
 *         - title  = title of graph!
 *
 * Examples: {graph_pie values=$values legend=$legend title=$title}
 * @author   Olivier Cyssau
 * @version  1.0
 */

function smarty_function_graph_pie( $params ){

  if (!isset ($params['values']) )
    $params['values'] = '';

  if (!isset ($params['legend']) )
    $params['legend'] = '';
  
  if (!isset ($params['title']) )
    $params['title'] = 'No title';

  require_once "Artichow/Pie.class.php";
  
  $graph = new Graph(400, 250);
  //$graph->setAntiAliasing(true);
  
  $graph->title->set($Titre);
  
  $plot = new Pie($Valeur, Pie::EARTH);
  $plot->setLegend($Legend);
  
  $plot->setCenter(0.4, 0.55);
  $plot->setSize(0.7, 0.6);
  $plot->set3D(10);
  
  $plot->legend->setPosition(1.3);
  $plot->legend->shadow->setSize(3);
  
  $graph->add($plot);
  
  return $graph->draw();

}
?>
