<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {getMenu} function plugin
 *
 * Type:     function<br>
 * Name:     getMenu<br>
 * Purpose:  genereate menu
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_get_menu($params, &$smarty) {
	echo RbView_Menu::get ()->toHtml ();
} //End of function

