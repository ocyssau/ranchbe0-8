<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {container_action_button} function plugin
 *
 * Type:     function<br>
 * Name:     container_action_button<br>
 * Purpose:  return buttons for container action
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_container_action_menu(array $params, &$smarty){
	if($params['mode'] == 'main'){
		$menu = new RbView_Menu_Container_Main($params, 'Form');
		return $menu->render();
	}else{
		if($params['isalias']){
			$menu = new RbView_Menu_Container_Alias_Contextual($params, 'Href');
		}else{
			$menu = new RbView_Menu_Container_Contextual($params, 'Href');
		}
		return $menu->render();
	}
}

