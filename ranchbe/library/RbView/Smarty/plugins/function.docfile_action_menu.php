<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {docfile_action_button} function plugin
 *
 * Type:     function<br>
 * Name:     docfile_action_button<br>
 * Purpose:  return buttons for docfile action
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_docfile_action_menu(array $params, &$smarty){
	if($params['mode'] == 'main'){
		$menu = new RbView_Menu_Docfile_Main($params, 'Form');
		return $menu->render();
	}else{
		$menu = new RbView_Menu_Docfile_Contextual($params, 'Href');
		return $menu->render();
	}
}

