<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Purpose:  replace container id by the container number<br>
 * Input: container_id<br>
 */
function smarty_modifier_container($id='0'){
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('function','filter_select');
  return smarty_filter_select_container($id);
}
