<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Purpose:  replace privilege id by the privilege name<br>
 * Input: privilege_id<br>
 */
function smarty_modifier_privilege_name($id='0'){
  return Rb_Acl::getPrivilegeName($id);
}
