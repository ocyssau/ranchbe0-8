<?php

function smarty_function_sameurl($params, &$smarty)
{
  $request =& Zend_Controller_Front::getInstance()->getRequest();
  $url = $request->getModuleName().'/'.$request->getControllerName().'/'.$request->getActionName();

  if( $params['sort_order'] == 'INV' ){
    if( $request->getParam('sort_order') == 'ASC' ) $params['sort_order'] = 'DESC';
    else $params['sort_order'] = 'ASC';
  }
  
  if( is_array($params) ){
    $p = '';
    foreach($params as $name=>$val) {
      $url .= '/'.$name.'/'.$val;
  }}
  
  return $url;
}
