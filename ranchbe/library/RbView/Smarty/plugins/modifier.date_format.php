<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Purpose:  format datestamps via strftime<br>
 * Input:<br>
 *         - string: input date string
 *         - format: long|short|hour
 * @return string|void
 */
function smarty_modifier_date_format($string, $format = 'long') {
	switch(strtolower($format)){
		case('long'):
			$format = Ranchbe::getConfig ()->date->format->long;
			break;
		case('short'):
			$format = Ranchbe::getConfig ()->date->format->short;
			break;
		case('hour'):
			$format = Ranchbe::getConfig ()->date->format->hour;
			break;
	}
	
	if ($string != '') {
		return strftime ( $format, $string );
	} else {
		return;
	}
}

