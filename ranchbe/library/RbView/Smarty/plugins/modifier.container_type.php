<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Purpose:  replace container type by is name<br>
 * Input: type<br>
 */
function smarty_modifier_container_type($type){
	if($type == 1){
		return tra('file manager');
	}else if($type == 0){
		return tra('doc manager');
	}
}
