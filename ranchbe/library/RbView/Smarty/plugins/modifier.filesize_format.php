<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Purpose:  format filesize for human<br>
 * Input:<br>
 */
function smarty_modifier_filesize_format($int='0'){
  if($int == 0) $format = "";
	else if($int <= 1024)				$format = $int." oct";
	else if($int <= (10*1024))			$format = sprintf ("%.2f k%s",($int/1024),"o");
	else if($int <= (100*1024))			$format = sprintf ("%.1f k%s",($int/1024),"o");
	else if($int <= (1024*1024))			$format = sprintf ("%d k%s",($int/1024),"o");
	else if($int <= (10*1024*1024))		$format = sprintf ("%.2f M%s",($int/(1024*1024)),"o");
	else if($int <= (100*1024*1024))		$format = sprintf ("%.1f M%s",($int/(1024*1024)),"o");
	else $format = sprintf ("%d M%s",($int/(1024*1024)),"o");
	return $format;
}
