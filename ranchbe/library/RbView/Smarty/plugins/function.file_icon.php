<?php
/**
 * Ranchbe plugin
 */


/**
 * Ranchbe {file_icon} function plugin
 *
 * Type:     function<br>
 * Name:     file_icon<br>
 * Date:     Mar 30, 2007<br>
 * Purpose:  display icon in accordance to file extension<br>
 * Input:<br>
 *         - extension = extension of file
 *         - icondir = directory where are store files icons, default = DEFAULT_FILE_ICONS_DIR
 *         - icontype = extension for icons files, default = .png
 *
 * Examples: {file_icon extension=".pdf"}
 * Output:   pdf.gif
 * @author   Olivier Cyssau
 * @version  1.0
 */

function smarty_function_file_icon( $params )
{
  if ($params['extension'])
    $extension = trim($params['extension'] , ".");
  else return 'no icon';

  if (!$params['icontype'] )
    $params['icontype'] = '.' . trim(Ranchbe::getConfig()->icons->file->type, '.');

  $iconfullurl = Ranchbe::getConfig()->icons->file->url .'/'. $extension . $params['icontype'];
  $iconfilepath = Ranchbe::getConfig()->icons->file->path .'/'. $extension . $params['icontype'];
  if ( ! is_file($iconfilepath) ) 
  	$iconfullurl = Ranchbe::getConfig()->icons->file->url .'/_default.gif';
  return '<img border="0" alt="no icon" src="' . "$iconfullurl" . '" />';
}

