<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Name:     yesorno<br>
 * Purpose:  replace O and 1 by yes or no<br>
 * Input: $val<br>
 */
function smarty_modifier_yesorno($val='0'){
  if ($val == 0) return tra('no');
  if ($val == 1) return tra('yes');
  return 'undefined';
}
