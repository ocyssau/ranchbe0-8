<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 *
 * Type:     function<br>
 * Name:     get_visu<br>
 * Purpose:  return visualisation of the document_id of space
 * @param array
 * @param Smarty
 * @return string
 * Examples: {get_visu document_id=$value space=$space_name}
 */
function smarty_function_get_visu($params, &$smarty){
  if(!$params['space']) return '';

  //require_once('core/document/viewer.php');
  $viewer = new Rb_Document_Viewer(Rb_Document::get($params['space'], $params['document_id']));
  
  $html = $viewer->displayVisu();//Display visu of document
  if(!$html)
    $html = $viewer->displayPicture();//Display image of document
  if($html){
    RbView_Flood::setTicket($viewer->file);
    $html .= '<a href="getFile.php?file='.$viewer->file.'&file_mime_type='.$viewer->mimetype.'">'
             .tra('Download visualisation file').'</a>';
    return $html;
  }
  return tra('No visualisation file');
  
} //End of function


