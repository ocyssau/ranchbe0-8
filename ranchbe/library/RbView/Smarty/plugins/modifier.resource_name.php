<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Purpose:  replace resource id by the resource name<br>
 * Input: resource_id<br>
 */
function smarty_modifier_resource_name($id='0'){
  return $id;
}
