<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {partner_action_menu} function plugin
 *
 * Type:     function<br>
 * Name:     partner_action_menu<br>
 * Purpose:  return buttons for partner action
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_partner_action_menu(array $params, &$smarty){
	if($params['mode'] == 'main'){
		$menu = new RbView_Menu_Partner_Main($params, 'Form');
		return $menu->render();
	}else{
		$menu = new RbView_Menu_Partner_Contextual($params, 'Href');
		return $menu->render();
	}
}

