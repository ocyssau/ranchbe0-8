<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Purpose:  replace project id by the project number<br>
 * Input: project_id<br>
 */
function smarty_modifier_project($project_id='0'){
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('function','filter_select');
  return smarty_filter_select_project($project_id);
}
