<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     block.translate.php
 * Type:     block
 * Name:     translate
 * Purpose:  translate a block of text
 * -------------------------------------------------------------
 */

function smarty_block_tr($params, $content, &$smarty) {
	if ($content) {
		echo tra ( $content );
	}
}
