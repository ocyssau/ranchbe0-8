<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Purpose:  replace process id by the process name<br>
 * Input: $process_id<br>
 */
function smarty_modifier_process($process_id='0'){
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('function','filter_select');
  return smarty_filter_select_process($process_id);
}
