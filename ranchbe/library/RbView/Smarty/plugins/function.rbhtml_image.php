<?php
function smarty_function_rbhtml_image($params, &$smarty) {
	$alt = '';
	$file = '';
	$height = '';
	$width = '';
	$extra = '';
	$prefix = '';
	$suffix = '';
	$usemap = '';
	$id = '';
	$path_prefix = '';
	foreach ( $params as $_key => $_val ) {
		switch ($_key) {
			case 'file' :
			case 'height' :
			case 'width' :
			case 'dpi' :
			case 'basedir' :
				$$_key = $_val;
				break;
			
			case 'alt' :
				if (! is_array ( $_val )) {
					$$_key = $_val;
				} else {
					$smarty->trigger_error ( "html_image: extra attribute '$_key' cannot be an array", E_USER_NOTICE );
				}
				break;
			
			case 'link' :
			case 'href' :
				$prefix = '<a href="' . $_val . '">';
				$suffix = '</a>';
				break;
			
			case 'map' :
				$procmapName = 'procmap_' . $id;
				$suffix .= '<map name="' . $procmapName . '">' . $_val . '</map>';
				$extra .= 'usemap="#' . $procmapName . '"';
				break;
			
			default :
				if (! is_array ( $_val )) {
					$extra .= ' ' . $_key . '="' . $_val . '"';
				} else {
					$smarty->trigger_error ( "html_image: extra attribute '$_key' cannot be an array", E_USER_NOTICE );
				}
				break;
		}
	}
	
	if (empty ( $file )) {
		$smarty->trigger_error ( "html_image: missing 'file' parameter", E_USER_NOTICE );
		return;
	}
	
	if (! isset ( $params ['width'] ) || ! isset ( $params ['height'] )) {
		if (! $_image_data = @getimagesize ( $file )) {
			if (! file_exists ( $file )) {
				$smarty->trigger_error ( "html_image: unable to find '$file'", E_USER_NOTICE );
				return;
			} else if (! is_readable ( $_image_path )) {
				$smarty->trigger_error ( "html_image: unable to read '$file'", E_USER_NOTICE );
				return;
			} else {
				$smarty->trigger_error ( "html_image: '$file' is not a valid image file", E_USER_NOTICE );
				return;
			}
		}
		if (! isset ( $params ['width'] )) {
			$width = $_image_data [0];
		}
		if (! isset ( $params ['height'] )) {
			$height = $_image_data [1];
		}
	}
	
	if (isset ( $params ['dpi'] )) {
		$dpi_default = 96;
		$_resize = $dpi_default / $params ['dpi'];
		$width = round ( $width * $_resize );
		$height = round ( $height * $_resize );
	}
    RbView_Flood::setTicket($file);
	return $prefix . '<img src="./getFile.php?file=' . $file . '" alt="' . $alt . '" width="' . $width . '" height="' . $height . '"' . $extra . ' />' . $suffix;
}



