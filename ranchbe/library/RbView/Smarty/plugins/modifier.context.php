<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Purpose:  replace context application id id by his name<br>
 * Input: cta_id<br>
 */
function smarty_modifier_context($cta_id='0'){
  global $cache_usualName;
  if( isset($cache_usualName['cta'][$cta_id]) ) {
    return $cache_usualName['cta'][$cta_id];
  }

  $query = "SELECT name FROM pdm_context_application WHERE cta_id = '$cta_id'";

  if(!$cache_usualName['cta'][$cta_id] = Ranchbe::getDb()->GetOne($query)){
    return $cache_usualName['cta'][$cta_id] = tra('undefined');
  }else{
    return $cache_usualName['cta'][$cta_id];
  }
}

