<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {get_context_containers} function plugin
 *
 * Type:     function<br>
 * Name:     get_context_container_tab<br>
 * Purpose:  return list of containers in context
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_get_context_containers($params, &$smarty)
{
  $context =& Ranchbe::getContext();
  //generate list of container
  if ( $context->getProperty( 'links' ) )
  foreach ( $context->getProperty( 'links' ) as $space_name=>$objects ){
    if( is_array($objects) )
    foreach($objects as $object){
      if (!($object['object_class'] == 'bookshop' || 
            $object['object_class'] == 'cadlib' || 
            $object['object_class'] == 'mockup' || 
            $object['object_class'] == 'workitem')) continue;
      if($params['activate_container_id'] == $object[$object['object_class'].'_id'] && $params['activate_space'] == $object['object_class']) {
        echo '<li class="active"><a href="context/consult/consult/space/'.$object['object_class'].'/container_id/'.$object[$object['object_class'].'_id'].'">'.$object[$object['object_class'].'_number'].'</a></li>';
      }else{
        echo '<li class=""><a href="context/consult/consult/space/'.$object['object_class'].'/container_id/'.$object[$object['object_class'].'_id'].'">'.$object[$object['object_class'].'_number'].'</a></li>';
      }
    }
  }// End of foreach

} //End of function
?>
