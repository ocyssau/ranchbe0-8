<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 *
 * Type:     function<br>
 * Name:     get_thumbnail<br>
 * Purpose:  return img html to display thumbnail of document
 * @param array
 * @param Smarty
 * @return string
 * Examples: {get_thumbnail document_id=$id space=$space_name}
 */
function smarty_function_get_thumbnail($params, &$smarty){
  switch($params['space']){
    case 'bookshop':
    case 'cadlib':
    case 'mockup':
    case 'workitem':
      break;
    default:
      return '<!--bad space name-->';
  }

  $thumbnailsDir = Ranchbe::getConfig()->thumbnails->path;
  $thumbnailsUrl = Ranchbe::getConfig()->thumbnails->url;
  $thumbnailsExtension = '.' . trim(Ranchbe::getConfig()->thumbnails->extension, '.');
  
  $img_path = $thumbnailsDir.'/'.$params['space'].'/'
              .$params['document_id']
              .$thumbnailsExtension;

  if( is_file($img_path) ){
    $img_url =  $thumbnailsUrl.'/'.$params['space'].'/'
                .$params['document_id']
                .$thumbnailsExtension;
    return '<img border="0" src="'.$img_url.'" />';
  }else{
    return '<!--none thumbs-->';
  }
} //End of function

