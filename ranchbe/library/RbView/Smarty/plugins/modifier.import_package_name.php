<?php
/**
 * Ranchbe plugin
 * @package Ranchbe
 */

/**
 * Ranchbe process modifier plugin
 *
 * Purpose:  replace import_order by the package name<br>
 * Input: $import_order<br>
 */
function smarty_modifier_import_package_name($import_order='0'){
  global $cache_usualName;
  if( isset($cache_usualName['import_package_name'][$import_order]) ) {
    return $cache_usualName['import_package_name'][$import_order];
  }

  $TABLE = 'import_history';
  $query = 'SELECT file_name FROM '.$TABLE.' WHERE import_order = \''.$import_order.'\'';
  if(!$cache_usualName['import_package_name'][$import_order] = Ranchbe::getDb()->GetOne($query))
    return $cache_usualName['import_package_name'][$import_order] = tra('undefined');
  return $cache_usualName['import_package_name'][$import_order];
}
