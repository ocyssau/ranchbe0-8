<?php
/**
 * Ranchbe plugin
 */

/**
 * Ranchbe {doctype_icon} function plugin
 *
 * Type:     function<br>
 * Name:     doctype_icon<br>
 * Date:     Mar 30, 2007<br>
 * Purpose:  display icon for doctype<br>
 * Input:<br>
 *         - extension = extension of file
 *         - icondir = directory where are store files icons, default = DEFAULT_FILE_ICONS_DIR
 *         - icontype = extension for icons files, default = .png
 *
 * @author   Olivier Cyssau
 * @version  1.0
 */

function smarty_function_doctype_icon($params) {
	if (! $params ['doctype_id'] && ! $params['icon'])
		return 'no icon';
	if($params ['doctype_id']){
		if (! $params ['icontype'])
			$params ['icontype'] = '.' . trim(Ranchbe::getConfig ()->icons->doctype->type, '.');
		$iconfullurl = Ranchbe::getConfig ()->icons->doctype->compiled->url 
								. '/' . $params ['doctype_id'] . $params ['icontype'];
		$iconfilepath = realpath(Ranchbe::getConfig ()->icons->doctype->compiled->path 
								. '/' . $params ['doctype_id'] . $params ['icontype']);
	}else if($params ['icon']){
		$iconfullurl = Ranchbe::getConfig ()->icons->doctype->source->url 
								. '/' . $params ['icon'];
		$iconfilepath = realpath(Ranchbe::getConfig ()->icons->doctype->source->path 
								. '/' . $params ['icon']);
	}
	if (! is_file ( $iconfilepath ))
		return 'no icon';
	return '<img border="0" alt="not found" src="' . "$iconfullurl" . '" />';
}

