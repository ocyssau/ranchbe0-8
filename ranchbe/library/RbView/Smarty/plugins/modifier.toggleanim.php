<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 *
 * Type:     modifier<br>
 * Purpose:  replace container id by the container number<br>
 * Input: container_id<br>
 */
function smarty_modifier_toggleanim($content, $hide=true){
	$init = 'none';
	
	$untoggleText = tra('Hide the filters');
	$toggleText = tra('Display the filters');
	
	$html= '';
	$html .= '<div class="hautboxScroll" style="text-align:left;"';
	$html .= "onclick=\"toggleAnim('animDiv', '$toggleText', '$untoggleText', 'imageFiltre', 'labelFiltre', '$init');\" >";
	$html .= '<img id="imageFiltre" style="margin-top: 0px;" />';
	$html .= '&nbsp;&nbsp;';
	$html .= '<span id="labelFiltre" style="color:#08639C;" >';
	$html .= '</span></div>';
	$html .= '<div id="animDiv" class="boxScroll" >';
	$html .= $content;
	$html .= '</div>';
	
	$js = '<script type="text/javascript">';
	if($hide) $init = 'hide';
	else $init = 'show';
	$js .= "toggleAnim('animDiv', '$toggleText', '$untoggleText', 'imageFiltre', 'labelFiltre', '$init');";
	$js .= '</script>';	
	
  	return $html . $js;
}

