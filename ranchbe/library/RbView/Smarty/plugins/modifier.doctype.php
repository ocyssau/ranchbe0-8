<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Smarty process modifier plugin
 *
 * Type:     modifier<br>
 * Name:     type<br>
 * Purpose:  replace type id by the type name<br>
 * Input: $type_id<br>
 */
function smarty_modifier_doctype($doctype_id='0'){
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('function','filter_select');
  return smarty_filter_select_doctype($doctype_id);
}
