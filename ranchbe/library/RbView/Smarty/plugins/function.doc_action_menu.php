<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {doc_action_button} function plugin
 *
 * Type:     function<br>
 * Name:     doc_action_button<br>
 * Purpose:  return buttons for document action
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_doc_action_menu(array $params, &$smarty){
	if($params['mode'] == 'main'){
		$menu = new RbView_Menu_Document_Main($params, 'Form');
		return $menu->render();
	}else{
		$menu = new RbView_Menu_Document_Contextual($params, 'Href');
		return $menu->render();
	}
}

