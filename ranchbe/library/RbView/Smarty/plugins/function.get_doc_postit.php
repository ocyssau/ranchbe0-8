<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {get_doc_postit} function plugin
 *
 * Type:     function<br>
 * Name:     get_doc_postit<br>
 * Purpose:  make a pop window if document is commented
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_get_doc_postit($params, &$smarty)
{
  $ocomment = new Rb_Document_Comment( Rb_Space::get($params['space']) );
  $comments = $ocomment->get( $params['document_id'] );
  if( !is_array($comments[0]) ) return false;

/*
  //$div_id = 'postit_'.$params['document_id'];
  $div_id = 'postit_';

  //$html .='<script type=\'text/javascript\' src="js/postit.js"></script>';
  //$html .='<script type=\'text/javascript\' src="html_ajax_server.php?client=all&stub=all&space='.$odocument->space->getName().'"></script>';
  //$html .= '<script src="js/scriptaculous/prototype.js" type="text/javascript"></script>';
  //$html .= '<script src="js/scriptaculous/scriptaculous.js" type="text/javascript"></script>';

  $html .='<script type=\'text/javascript\'>
            var Remote = new rb_ajax_postit(Callback);</script>';
  $html .= '<a onclick="return Remote.get(\''.$params['document_id'].'\')">
            <img src="img/icons/comment/comment.png" title="postit"  alt="postit" /></a>';


  $html .= '
  <script language=\'Javascript\' type=\'text/javascript\'>
  <!--
  function closeMbox(id){
  element = document.getElementById(id);
  element.style.visibility = \'hidden\';
  return true;
  }
  //-->
  </script>
  ';

  $html .= '<div id="'.$div_id.'" style="display:none;visibility:hidden;background:yellow; border:1px solid #333;cursor: move;min-width=150px;max-width:800px;overflow:auto;padding:0px"></div>';
  $html .= '<script type="text/javascript">new Draggable(\''.$div_id.'\');</script>';

  return $html;
*/

  $html .= '<a onclick="return overlib(';
  $html .= '\'';
  require_once $smarty->_get_plugin_filepath('modifier', 'username');
  foreach($comments as $comment ){
    $html .= '<i>By '.smarty_modifier_username($comment['open_by']).'</i><br />';
    $comment['comment'] = str_replace("\r\n", '<br />',$comment['comment']);
    $comment['comment'] = str_replace("\n", '<br />',$comment['comment']);
    $comment['comment'] = str_replace("\r", '<br />',$comment['comment']);
    $comment['comment'] = str_replace('\'', '\\\'',$comment['comment']);
    $html .= iconv('UTF-8', "ISO-8859-1//TRANSLIT", $comment['comment'] ); //convert $rawdata in ISO-8859-1
    $html .= ' <a href=\\\'document/index/delComment/comment_id/'.
                $comment['comment_id'].'/space/'.$params['space'].
                '/ifSuccessModule/'.$params['ifSuccessModule'].
                '/ifSuccessController/'.$params['ifSuccessController'].
                '/ifSuccessAction/'.$params['ifSuccessAction'].
                '/document_id/'.$params['document_id'].
                '\\\'>Delete</a>';
    $html .= '<hr />';
  }
  $html .= '\'';
  $html .= ',STICKY,CAPTION,\'postit\',FGCOLOR,\'yellow\',CLOSECLICK);" onmouseout="nd();">';
  $html .= '<img src="img/icons/comment/comment.png" title="postit"  alt="postit" />';
  $html .= '</a>';

  //var_dump($comments[0]['comment']);
  //var_dump($params);
  //var_dump($html);
  return $html;

} //End of function
