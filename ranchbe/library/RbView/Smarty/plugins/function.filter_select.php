<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Smarty plugin
 *
 * Type:     function<br>
 * Name:     filter_select<br>
 * Purpose:  select the correct filter to display normalized name from the id<br>
 * Input: $id of the object, and $type of the object<br>
 * type = [bookshop, cadlib, doctype, workitem, mockup, project, process, partner, nofilter] 
 * if type = nofilter return the value of id without translation 
 * Examples: {filter_select id=$partner_id type=$type}
 * @author   Olivier Cyssau
 * @version  1.0
 */

function smarty_filter_select_partner($id){
  global $cache_usualName;
  if( isset($cache_usualName['partner'][$id]) ) {
    return $cache_usualName['partner'][$id];
  }
  if(is_numeric($id)){
    $query = "SELECT partner_number FROM partners WHERE partner_id = '$id'";
    if(!$val=Ranchbe::getDb()->CacheGetOne(3600,$query)){
      return $cache_usualName['supplier'][$id] = $id;
    }else{
      return $cache_usualName['supplier'][$id] = $val;
    }
  }else return $id;
}

function smarty_filter_select_doctype($id){
  global $cache_usualName;
  if( isset($cache_usualName['doctype'][$id]) ) {
    return $cache_usualName['doctype'][$id];
  }
  if(is_numeric($id)){
    $query = "SELECT doctype_number FROM doctypes WHERE doctype_id = '$id'";
    if(!$val=Ranchbe::getDb()->CacheGetOne(3600,$query)){
      return $cache_usualName['doctype'][$id] = $id;
    }else{
      return $cache_usualName['doctype'][$id] = $val;
    }
  }else return $id;
} //End

function smarty_filter_select_indice($id){
  return Rb_Indice::singleton()->getName($id);
} // End

function smarty_filter_select_username($id){
  global $cache_usualName;
  if( isset($cache_usualName['username'][$id]) ) {
    return $cache_usualName['username'][$id];
  }

  $query = "SELECT handle FROM rb_users WHERE auth_user_id = '$id'";
  if(!$val=Ranchbe::getDb()->CacheGetOne(3600,$query)){
    return $cache_usualName['username'][$id] = $id;
  }else{
    return $cache_usualName['username'][$id] = $val;
  }
} // End

function smarty_filter_select_groupname($id){
  global $cache_usualName;
  if( isset($cache_usualName['groupname'][$id]) ) {
    return $cache_usualName['groupname'][$id];
  }

  if(is_numeric($id)){
    $query = "SELECT group_define_name FROM rb_groups WHERE group_id = '$id'";
    if(!$val=Ranchbe::getDb()->CacheGetOne(3600,$query)){
      return $cache_usualName['groupname'][$id] = $id;
    }else{
      return $cache_usualName['groupname'][$id]=$val;
    }
  }else return $id;
}

function smarty_filter_select_category($id){
  global $cache_usualName;
  if( isset($cache_usualName['category'][$id]) ) {
    return $cache_usualName['category'][$id];
  }

  if(is_numeric($id)){
    $query = "SELECT category_number FROM categories WHERE category_id = '$id'";
    if(!$val=Ranchbe::getDb()->CacheGetOne(3600,$query)){
      return $cache_usualName['category'][$id] = $id;
    }else{
      return $cache_usualName['category'][$id] = $val;
    }
  }else return $id;
}

function smarty_filter_select_process($id){
  global $cache_usualName;
  if( isset($cache_usualName['process'][$id]) ) {
    return $cache_usualName['process'][$id];
  }

  if(is_numeric($id)){
    $query = "SELECT normalized_name FROM galaxia_processes WHERE pId = '$id'";
    if(!$val=Ranchbe::getDb()->CacheGetOne(3600,$query)){
      return $cache_usualName['process'][$id] = $id;
    }else{
      return $cache_usualName['process'][$id]=$val;
    }
  }else return $id;
}

function smarty_filter_select_reposit($id){
  global $cache_usualName;
  if( isset($cache_usualName['reposit'][$id]) ) {
    return $cache_usualName['reposit'][$id];
  }

  if(is_numeric($id)){
    $query = "SELECT reposit_number FROM reposits WHERE reposit_id = '$id'";
    if(!$val=Ranchbe::getDb()->CacheGetOne(3600,$query)){
      return $cache_usualName['reposit'][$id] = $id;
    }else{
      return $cache_usualName['reposit'][$id]=$val;
    }
  }else return $id;
}

function smarty_filter_select_project($id){
  global $cache_usualName;
  if( isset($cache_usualName['project'][$id]) ) {
    return $cache_usualName['project'][$id];
  }

  if(is_numeric($id)){
    $query = "SELECT project_number FROM projects WHERE project_id = '$id'";
    if(!$val=Ranchbe::getDb()->CacheGetOne(3600,$query)){
      return $cache_usualName['project'][$id] = $id;
    }else{
      return $cache_usualName['project'][$id]=$val;
    }
  }else return $id;
}

function smarty_filter_select_container($id){
  global $cache_usualName;
  if( isset($cache_usualName['container'][$id]) ) {
    return $cache_usualName['container'][$id];
  }

  if(is_numeric($id)){
    $query = "SELECT space_id FROM objects WHERE object_id = '$id'";
    $space_id = Ranchbe::getDb()->CacheGetOne(3600,$query);
    if(!$space_id) return $cache_usualName['container'][$id] = $id;
    $table = Rb_Space::get($space_id)->CONT_TABLE;
    $fid = Rb_Space::get($space_id)->CONT_FIELDS_MAP_ID;
    $fname = Rb_Space::get($space_id)->CONT_FIELDS_MAP_NUM;
    $query = "SELECT $fname FROM $table WHERE $fid = '$id'";
    $val=Ranchbe::getDb()->CacheGetOne(3600,$query);
    if(!$val){
      return $cache_usualName['container'][$id] = $id;
    }else{
      return $cache_usualName['container'][$id]=$val;
    }
  }else return $id;
}

function smarty_function_filter_select($params){
  if (!isset ($params['id']) )
    return '';

  if (!isset($params['type']) && !isset($params['property_name']))
    return '';

  if (!isset($params['type']) && isset($params['property_name'])){
    $params['type'] = $params['property_name'];
  }

  switch ( $params['type'] ) {
  case 'doctype':
  case 'doctype_id':
    return smarty_filter_select_doctype($params['id']);
    break;
  case 'indice':
  case (in_array($params['type'],array('cont_cont_indice_id','bookshop_version','cadlib_version','workitem_version','mockup_version','project_indice_id'))):
    return smarty_filter_select_indice($params['id']);
    break;
  case 'document_version':
    return $params['id'];
    break;
  case 'category_id':
  case 'category': 
    return smarty_filter_select_category($params['id']);
    break;
  case 'date': 
  case (in_array($params['type'],array('cont_open_date','open_date','cont_close_date','close_date','cont_forseen_close_date','forseen_close_date','update_date','check_out_date'))):
    require_once Ranchbe::getSmarty()->_get_plugin_filepath('modifier','date_format');
    return smarty_modifier_date_format($params['id']);
    break;
  case 'user': 
  case (in_array($params['type'],array('cont_open_by','cont_close_by','close_by','close_by','check_out_by','open_by'))):
    return smarty_filter_select_username($params['id']);
    break;
  case 'process': 
  case (in_array($params['type'],array('cont_default_process_id','default_process_id'))):
    return smarty_filter_select_process($params['id']);
    break;
  /*
  case 'container': 
  case (in_array($params['type'],array('bookshop_id','mockup_id','cadlib_id','workitem_id','project_id'))):
    return container($params['id']);
    break;
  case 'workitem':
    return container($params['id']);
    break;
  case 'mockup':
    return container($params['id']);
    break;
  case 'cadlib': 
    return container($params['id']);
    break;
  case 'bookshop': 
    return container($params['id']);
    break;
  */
  case 'project':
    return smarty_filter_select_project($params['id']);
    break;
  case 'partner':
    return smarty_filter_select_partner($params['id']);
    break;
  default:
    return $params['id'];
  } //End of switch types

} //End of function

