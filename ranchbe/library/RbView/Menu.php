<?php

class RbView_Menu {
	
	protected static $_instance;
	protected $_view;
	
	//----------------------------------------------------------------------------
	protected function __construct() {
		$this->_view = Zend_Controller_Front::getInstance ()->getParam ( 'bootstrap' )->getResource ( 'Smarty' );
	} //End of method
	

	//----------------------------------------------------------------------------
	public static function get($config = array()) {
		if (! self::$_instance) {
			if (! $config) {
				$config = Ranchbe::getConfig ()->menu->main->toArray ();
			}
			self::$_instance = new self ( );
			self::$_instance->init ( $config );
		}
		return self::$_instance;
	} //End of method
	

	//----------------------------------------------------------------------------
	public function init($config = array()) {
		new RbView_Tab ( 'homeTab', $this->_view->baseUrl ( '/home' ), tra ( 'Welcome' ) );
		new RbView_Tab ( 'projectsTab', $this->_view->baseUrl ( '/project' ), tra ( 'Projects' ) );
		new RbView_Tab ( 'workitemTab', $this->_view->baseUrl ( '/container/workitem' ), tra ( 'WorkItems' ) );
		
		if ($_SESSION ['myspace'] ['activePage'])
			new RbView_Tab ( 'mySpaceTab', $_SESSION ['myspace'] ['activePage'], tra ( 'My space' ) );
		else
			new RbView_Tab ( 'mySpaceTab', $this->_view->baseUrl ( '/wildspace' ), tra ( 'My space' ) );
		
		if ($config ['pdm'])
			new RbView_Tab ( 'pdmTab', $this->_view->baseUrl ( '/pdm/index' ), tra ( 'Pdm' ) );
		
		if ($config ['mockup'])
			new RbView_Tab ( 'mockupTab', $this->_view->baseUrl ( '/container/mockup' ), tra ( 'Mockups' ) );
		
		if ($config ['cadlib'])
			new RbView_Tab ( 'cadlibTab', $this->_view->baseUrl ( '/container/cadlib' ), tra ( 'Cadlibs' ) );
		
		if ($config ['bookshop'])
			new RbView_Tab ( 'bookshopTab', $this->_view->baseUrl ( '/container/bookshop' ), tra ( 'Bookshops' ) );
		
		if ($config ['partner'])
			new RbView_Tab ( 'partnersTab', $this->_view->baseUrl ( '/partner/index/get' ), tra ( 'Partners' ) );
		
		if ($config ['admin'])
			new RbView_Tab ( 'adminTab', $this->_view->baseUrl ( '/admin/index/index' ), tra ( 'Admin' ) );
		
		return $this;
	} //End of method
	

	//----------------------------------------------------------------------------
	public function getMySpace() {
		$context = & Ranchbe::getContext ();
		$mySpace = & RbView_Tab::get ( 'mySpaceTab' );
		$container_id = $context->getProperty ( 'container_id' );
		
		if ($container_id) {
			$container = & Rb_Container::get ( $context->getProperty ( 'space_name' ), $context->getProperty ( 'container_id' ) );
			if ($container->isFileOnly ()) {
				$mySpace->addTab ( 'documentTab', $this->_view->baseUrl ( '/content/get/recordfile' ), $context->getProperty ( 'container_number' ) );
			} else {
				$mySpace->addTab ( 'documentTab', $this->_view->baseUrl ( '/content/get/document' ), $context->getProperty ( 'container_number' ) );
			}
			if ($_SESSION ['DisplayDocfileTab'])
				$mySpace->addTab ( 'documentfileTab', $this->_view->baseUrl ( '/content/get/docfile' ), $context->getProperty ( 'container_number' ) . '_' . tra ( 'Files' ) );
		}
		
		$mySpace->addTab ( 'wildspaceTab', $this->_view->baseUrl ( '/wildspace' ), tra ( 'Wildspace' ) );
		
		if ($container_id) {
			$mySpace->addTab ( 'consultTab', $this->_view->baseUrl ( '/context/consult/index' ), tra ( 'Consultation' ) );
		}
		
		return $this;
	} //End of method
	

	//----------------------------------------------------------------------------
	public function getConsult() {
		$context = & Ranchbe::getContext ();
		$myConsult = & RbView_Tab::get ( 'consultTab' );
		$container_id = $context->getProperty ( 'container_id' );
		//generate list of container
		if ($context->getProperty ( 'links' ))
			foreach ( $context->getProperty ( 'links' ) as $class_id => $objects ) {
				if (! ($class_id == 20 || 
						$class_id == 21 || 
						$class_id == 22 || 
						$class_id == 23))
					continue;
				if (is_array ( $objects ))
					foreach ( $objects as $object ) {
						switch($class_id){
							case 20:
								$name = 'bookshop_number';
								$id = 'bookshop_id';
								$space_name = 'bookshop';
								break;
							case 21:
								$name = 'cadlib_number';
								$id = 'cadlib_id';
								$space_name = 'cadlib';
								break;
							case 22:
								$name = 'mockup_number';
								$id = 'mockup_id';
								$space_name = 'mockup';
								break;
							case 23:
								$name = 'workitem_number';
								$id = 'workitem_id';
								$space_name = 'workitem';
								break;
						}
						$myConsult->addTab ( $object [$name] . 'Tab', 
							$this->_view->baseUrl ( '/context/consult/consult/space/' ) 
							. $space_name 
							. '/container_id/' 
							. $object [$id], 
							$object [$name] );
					}
			} // End of foreach
	} //End of method
	

	//----------------------------------------------------------------------------
	public function getAdmin() {
		RbView_Tab::get ( 'adminTab' )
			->addTab ( 'doctypesTab', $this->_view->baseUrl ( 'doctype/index/index' ), tra ( 'Doctypes' ) )
			->addTab ( 'repositTab', $this->_view->baseUrl ( 'reposit/index/index' ), tra ( 'Reposits' ) )
			->addTab ( 'readrepositTab', $this->_view->baseUrl ( 'reposit/read/index' ), tra ( 'Read reposits' ) )
			->addTab ( 'workflowTab', $this->_view->baseUrl ( 'galaxia/processes/index' ), tra ( 'Workflow' ) )
			->addTab ( 'usersTab', $this->_view->baseUrl ( 'user/adminusers/get' ), tra ( 'Users' ) )
			->addTab ( 'toolsTab', $this->_view->baseUrl ( 'tools/index/index' ), tra ( 'Tools' ) );
		return $this;
	} //End of method
	

	//----------------------------------------------------------------------------
	public function getPdm() {
		RbView_Tab::get ( 'pdmTab' )
			->addTab ( 'contextTab', $this->_view->baseUrl ( 'pdm/context/index' ), tra ( 'Context' ) )
			->addTab ( 'productsTab', $this->_view->baseUrl ( 'pdm/products/index' ), tra ( 'Product' ) )
			->addTab ( 'effectivityTab', $this->_view->baseUrl ( 'pdm/effectivity/index' ), tra ( 'Effectivity' ) );
		return $this;
	} //End of method
	

	//----------------------------------------------------------------------------
	public function toHtml() {
		return RbView_Tab::toHtml ();
	} //End of method


} //End of class
