<?php

abstract class RbView_Menu_Item_Abstract {
	protected $_name;
	
	//----------------------------------------------------------------------------
	public function setName($name) {
		$this->_name = $name;
		return $this;
	}

	//----------------------------------------------------------------------------
	public function getName() {
		return $this->_name;
	}
	
	
	//----------------------------------------------------------------------------
	public abstract function render();
	

} //End of class

