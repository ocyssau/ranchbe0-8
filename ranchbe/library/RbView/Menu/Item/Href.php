<?php

class RbView_Menu_Item_Href extends RbView_Menu_Item_Item{
	
	//----------------------------------------------------------------------------
	/**
	 * 
	 */
	protected function _getUrl() {
		$this->_url = $this->_core_props['module'] . '/' . $this->_core_props['controller'] . '/' . $this->_core_props['action'];
		
		if ( $this->_url_params )
		foreach ( $this->_url_params as $key => $val ) {
			if ($val) $this->_url .= '/' . $key . '/' . $val;
		}
		
		if($this->_core_props['successAction'] && $this->_core_props['successController']
									&& $this->_core_props['successModule']){
			$this->_url .= '/ifSuccessModule/'.$this->_core_props['successModule'].
	                        '/ifSuccessController/'.$this->_core_props['successController'].
	                        '/ifSuccessAction/'.$this->_core_props['successAction'];
		}
		
		if($this->_core_props['failedModule'] && $this->_core_props['failedController']
									&& $this->_core_props['failedAction']){
			$this->_url .= '/ifFailedModule/'.$this->_core_props['failedModule'].
	                        '/ifFailedController/'.$this->_core_props['failedController'].
	                        '/ifFailedAction/'.$this->_core_props['failedAction'];
		}
		
		return $this->_url;
	} //End of method
	

	//----------------------------------------------------------------------------
	/**
	 * 
	 */
	protected function _getOnclick() {
		if ($this->_onclick) {
			$onClick = $this->_onclick;
		} else {
			if ($this->_ispopup) {
				$onClick = 'popupP(\'' . $this->_url . '\',\'' . $this->_label . '\',' . $this->_popupx . ',' . $this->_popupy . ');return false;';
			} else {
				$onClick = 'document.location.href=\'' . $this->_url . '\';return false;';
			}
		}
		
		if($this->_confirmation){
			$onClick = "if(confirm('$this->_confirmation'))".'{'.$onClick.'} else {return false;}';
		}
		
		$onClick = 'onClick="' . $onClick . '"';
		
		return $onClick;
	} //End of method
	
	
	//----------------------------------------------------------------------------
	public function render(){
		if( $this->hasChilds() ){
			$html .= "<div dojoType='dijit.PopupMenuItem'>";
			if($this->_label){
				$html .= "<span>";
				if ( $this->_icon_url ) {
					$html .= '<img border="0" src="' . $this->_icon_url . '"/>';
				}
				$html .= $this->_label . "</span>";
			}
			$html .= "<div dojoType='dijit.Menu'>";
			foreach($this->getChilds() as $child){
				$html .= $child->render();
			}
			$html .= '</div></div>';
		}else{
			if (! $this->_url) {
				$this->_getUrl();
			}
			
			$onClick = $this->_getOnclick ();
			$html = "<div dojoType='dijit.MenuItem' title=\"$this->_label\" $onClick >";
			
			//var_dump($html);die;
			
			if ( $this->_icon_url ) {
				$html .= '<img border="0" " src="' . $this->_icon_url . '"/>';
			}
			
			if ( $this->_displayText ) {
				$html .= $this->_label;
			}
			
			$html .= '</div>';
		}
		
		//return preg_replace('!\s+!', '', $html);
		return $html;
	}
	

} //End of class

