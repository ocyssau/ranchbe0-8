<?php

class RbView_Menu_Item_Form extends RbView_Menu_Item_Item{
	protected $_formid = 'checkform';
	
	//----------------------------------------------------------------------------
	public function setFormid($id){
		$this->_formid = $id;
		return $this;
	}
	
	//----------------------------------------------------------------------------
	public function getFormid(){
		return $this->_formid;
	}
	
	
	//----------------------------------------------------------------------------
	/**
	 * 
	 */
	protected function _getUrl() {
		$this->_url = ROOT_URL . $this->_core_props['module'] . '/' . $this->_core_props['controller'] . '/' . $this->_core_props['action'];
		
		if ( $this->_url_params )
		foreach ( $this->_url_params as $key => $val ) {
			if ($val) $this->_url .= '/' . $key . '/' . $val;
		}
		
		return $this->_url;
	} //End of method
	
	
	//----------------------------------------------------------------------------
	/**
	 * 
	 */
	protected function _getOnclick() {
		if ($this->_onclick) {
			$onClick = $this->_onclick;
		} else {
			$onClick = "document.$this->_formid.action='$this->_url';";
			if ($this->_ispopup) {
				$onClick .= "pop_it(document.$this->_formid, $this->_popupx, $this->_popupy);";
			} else {
				$onClick .= "pop_no(document.$this->_formid);";
			}
		}
		
		if($this->_confirmation){
			$onClick = "if(confirm('$this->_confirmation'))".'{'.$onClick.'} else {return false;}';
		}
		
		$onClick = 'onClick="' . $onClick . '"';
		return $onClick .'"';
	} //End of method
	

	//----------------------------------------------------------------------------
	public function render(){
		if( $this->hasChilds() ){
			$html .= "<div dojoType='dijit.PopupMenuItem'>";
			if($this->_label){
				$html .= "<span>";
				if ( $this->_icon_url ) {
					$html .= '<img border="0" src="' . $this->_icon_url . '"/>';
				}
				$html .= $this->_label . "</span>";
			}
			$html .= "<div dojoType='dijit.Menu'>";
			foreach($this->getChilds() as $child){
				$html .= $child->render();
			}
			$html .= '</div></div>';
		}else{
			if (! $this->_url) {
				$this->_getUrl();
			}
			
			$onClick = $this->_getOnclick ();
			$html = "<div dojoType='dijit.MenuItem' title=\"$this->_label\" $onClick >";
			
			if ( $this->_icon_url ) {
				$html .= '<img border="0" " src="' . $this->_icon_url . '"/>';
			}
			
			if ( $this->_displayText ) {
				$html .= $this->_label;
			}
			
			$html .= '</div>';
		}
		
		return $html;
	}
	

} //End of class

