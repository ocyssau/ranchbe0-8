<?php
abstract class RbView_Menu_Item_Item extends RbView_Menu_Item_Abstract{
	
	protected $_childs = array();
	protected $_url; //url of item
	protected $_label; //displayed name
	protected $_icon_url;
	protected $_isActive = false;
	protected $_core_props = array();
	protected $_ispopup = false;
	protected $_popupy;
	protected $_popupx;
	protected $_onclick;
	protected $_displayText= true;
	protected $_url_params = array();
	protected $_confirmation; //string
		
	
	//----------------------------------------------------------------------------
	/**
	 * 
	 * @param array|string $options
	 */
	public function __construct($options = null) {
		if( is_array($options) ) $this->setOptions($options);
		if( is_string($options) ) $this->setName($options);
	}
	
	//----------------------------------------------------------------------------
	/**
	 * 
	 */
	protected abstract function _getUrl();
	
	//----------------------------------------------------------------------------
	/**
     * Set object state from options array
     *
     * @param  array $options
     * @return RbView_Menu_Item
     */
    public function setOptions(array $options){
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                // Setter exists; use it
                $this->$method($value);
            } else {
                // Assume it's metadata
                $this->setProperty($key, $value);
            }
        }
        return $this;
    }
    
	//----------------------------------------------------------------------------
    /**
     * Set element attribute
     *
     * @param  string $name
     * @param  mixed $value
     * @return RbView_Menu_Item
     */
    public function setProperty($name, $value){
        $name = (string) $name;
        if (null === $value) {
            unset( $this->_core_props[$name] );
        } else {
            $this->_core_props[$name] = $value;
        }
        return $this;
    }
	
	//----------------------------------------------------------------------------
	public function addChild(RbView_Menu_Item_Abstract $item){
		$this->_childs[$item->getName()] = $item;
		return $this;
	}
	
	//----------------------------------------------------------------------------
	public function setChilds( array $items){
		$this->_childs = $items;
		return $this;
	}
	
	//----------------------------------------------------------------------------
	public function hasChilds(){
		if( $this->_childs ) return true;
		else return false;
	}
	
	//----------------------------------------------------------------------------
	public function getChilds(){
		return $this->_childs;
	}
	
	//----------------------------------------------------------------------------
	public function getChild($name){
		if($this->_childs[$name]) return $this->_childs[$name];
		else return false;
	}
	
	//----------------------------------------------------------------------------
	public function getUrl() {
		if( !$this->_url ) $this->_getUrl();
		return $this->_url;
	}

	//----------------------------------------------------------------------------
	public function setUrl($url) {
		$this->_url = $url;
		return $this;
	}

	//----------------------------------------------------------------------------
	public function setController($name) {
		$this->_core_props['controller'] = $name;
		return $this;
	}

	//----------------------------------------------------------------------------
	public function setModule($name) {
		$this->_core_props['module'] = $name;
		return $this;
	}

	//----------------------------------------------------------------------------
	public function setAction($name) {
		$this->_core_props['action'] = $name;
		return $this;
	}

	//----------------------------------------------------------------------------
	public function setUrlParams($name, $value) {
		$this->_url_params[$name] = $value;
		return $this;
	}
	
	//----------------------------------------------------------------------------
	public function setSuccessController($name) {
		if($name) $this->_core_props['successController'] = (string) $name;
		return $this;
	}

	//----------------------------------------------------------------------------
	public function setSuccessModule($name) {
		if($name) $this->_core_props['successModule'] = (string) $name;
		return $this;
	}

	//----------------------------------------------------------------------------
	public function setSuccessAction($name) {
		if($name) $this->_core_props['successAction'] = (string) $name;
		return $this;
	}

	//----------------------------------------------------------------------------
	public function setFailedController($name) {
		if($name) $this->_core_props['failedController'] = (string) $name;
		return $this;
	}

	//----------------------------------------------------------------------------
	public function setFailedModule($name) {
		if($name) $this->_core_props['failedModule'] = (string) $name;
		return $this;
	}

	//----------------------------------------------------------------------------
	public function setFailedAction($name) {
		if($name) $this->_core_props['failedAction'] = (string) $name;
		return $this;
	}
	
	//----------------------------------------------------------------------------
	public function setIcon($url) {
		$this->_icon_url = $url;
		return $this;
	}
	
	//----------------------------------------------------------------------------
	public function setLabel( $value ) {
		$this->_label = (string) $value;
		$this->_alt = $this->_label;
		return $this;
	}
	
	//----------------------------------------------------------------------------
	public function getLabel() {
		return $this->_label;
	}

	//----------------------------------------------------------------------------
	/**
	 * 
	 * @param array|boolean $dim = array('x'=>integer, 'y'=>integer)
	 */
	public function setPopup( $dim ) {
		if( is_array($dim) ){
			if( $dim['x'] && $dim['y'] ){
				$this->_ispopup = true;
				$this->_popupx = (int) $dim['x'];
				$this->_popupy = (int) $dim['y'];
				return $this;
			}
		}
		$this->_ispopup = false;
		unset($this->_popupx);
		unset($this->_popupy);
		return $this;
	}

	//----------------------------------------------------------------------------
	public function setOnclick($string) {
		//$this->_onclick =  strtr($string, array('\\'=>'\\\\',"'"=>"\\'",'"'=>'\\"',"\r"=>'\\r',"\n"=>'\\n','</'=>'<\/'));
		$this->_onclick = $string;
		return $this;
	}
	
	//----------------------------------------------------------------------------
	public function setConfirmation($string) {
		$this->_confirmation = $string;
		return $this;
	}
	
	
	//----------------------------------------------------------------------------
	public function setDisplayText($bool) {
		$this->_displayText =  (boolean) $bool;
		return $this;
	}

	//----------------------------------------------------------------------------
	public function isActive() {
		return $this->_isActive;
	}
	
	//----------------------------------------------------------------------------
	/**
	 * 
	 */
	protected abstract function _getOnclick();

} //End of class

