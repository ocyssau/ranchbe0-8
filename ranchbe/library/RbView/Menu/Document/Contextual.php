<?php

/** 
 * 
 * @author Olivier CYSSAU
 *
 */
class RbView_Menu_Document_Contextual extends RbView_Menu_Document_Abstract{

	
	/**
	 * 
	 * @param array $params
	 * @param string $class
	 */
	public function __construct(array $params, $class = 'Href'){
		
		$class = 'RbView_Menu_Item_' . ucfirst($class);
		$docinfos = $params['infos'];
		
		$subMenuAdmin = new $class('subMenuAdmin');
		$subMenuAdmin->setLabel( tra('Admin') );
		if ($docinfos ['document_access_code'] < 12)
			$subMenuAdmin->addChild($this->_getElement('resetdoctype', $params, $class));
		$subMenuAdmin->addChild($this->_getElement('updatethumbnail', $params, $class));
		$subMenuAdmin->addChild($this->_getElement('updateread', $params, $class));
		$subMenuAdmin->addChild($this->_getElement('export', $params, $class));
		if ($docinfos ['document_access_code'] == 0) {
			$subMenuAdmin->addChild($this->_getElement('freeze', $params, $class));
		}
		if ($docinfos ['document_access_code'] < 15) {
			$subMenuAdmin->addChild($this->_getElement('convert', $params, $class));
		}
		
		$subMenuEdit = new $class('subMenuEdit');
		$subMenuEdit->setLabel( tra('Edit') );
		if ($docinfos ['document_access_code'] >= 0 && $docinfos ['document_access_code'] < 10)
			$subMenuEdit->addChild($this->_getElement('editDoc', $params, $class));
		if ($docinfos ['document_access_code'] == 0)
			$subMenuEdit->addChild($this->_getElement('lockdoc', $params, $class));
		if ($docinfos ['document_access_code'] > 0 && $docinfos ['document_access_code'] < 15)
			$subMenuEdit->addChild($this->_getElement('unLockDoc', $params, $class));
		if ($docinfos ['document_access_code'] == 0)
			$subMenuEdit->addChild($this->_getElement('marktosuppress', $params, $class));
		if ($docinfos ['document_access_code'] == 12)
			$subMenuEdit->addChild($this->_getElement('unmarktosuppress', $params, $class));
		if ($docinfos ['document_access_code'] == 0 || $docinfos ['document_access_code'] == 12)
			$subMenuEdit->addChild($this->_getElement('suppressdoc', $params, $class));
		if ($docinfos ['document_access_code'] == 0){
			$subMenuEdit->addChild($this->_getElement('movedocument', $params, $class));
			$subMenuEdit->addChild($this->_getElement('copydocument', $params, $class));
			$subMenuEdit->addChild($this->_getElement('assocfile', $params, $class));
			$subMenuEdit->addChild($this->_getElement('assocattachment', $params, $class));
		}
		$subMenuEdit->addChild($this->_getElement('addcomment', $params, $class));
		
		$subMenuSend = new $class('subMenuSend');
		$subMenuSend->setLabel( tra('Send To') );
		$subMenuSend->addChild($this->_getElement('sendbymail', $params, $class))
					->addChild($this->_getElement('mailto', $params, $class));
		
		$this->setLabel( tra('action') );
		if ($docinfos ['document_access_code'] == 0) {
			$this->addChild($this->_getElement('checkout', $params, $class));
		}
		if ($docinfos ['check_out_by'] == Rb_User::getCurrentUser ()->getId () && $docinfos ['document_access_code'] >= 1 && $docinfos ['document_access_code'] <= 4) {
			$this->addChild($this->_getElement('checkin', $params, $class));
			$this->addChild($this->_getElement('updateDoc', $params, $class));
			$this->addChild($this->_getElement('resetdoc', $params, $class));
		}
		if ($docinfos ['document_access_code'] == 0 || ($docinfos ['document_access_code'] >= 5 && $docinfos ['document_access_code'] <= 9)) {
			$this->addChild($this->_getElement('changestate', $params, $class));
		}
		$this->addChild($this->_getElement('createversion', $params, $class));
		$this->addChild($this->_getElement('putinws', $params, $class));
		$this->addChild(new RbView_Menu_Item_Separator('separator1'));
		$this->addChild($this->_getElement('getHistory', $params, $class));
		$this->addChild($subMenuEdit);
		$this->addChild($subMenuSend);
		$this->addChild(new RbView_Menu_Item_Separator('separator2'));
		$this->addChild($subMenuAdmin);
		
		return $this;

	}
	
	
	/**
	 * 
	 * @param RbView_Menu_Element $item
	 * @param array $params
	 */
	protected function _initOptions(RbView_Menu_Item_Abstract $item, array $params){
		$item->setDisplayText($params['displayText'])
				->setSuccessAction($params['ifSuccessAction'])
				->setSuccessController($params['ifSuccessController'])
				->setSuccessModule($params['ifSuccessModule'])
				->setFailedAction($params['ifFailedAction'])
				->setFailedController($params['ifFailedController'])
				->setFailedModule($params['ifFailedModule'])
				->setUrlParams('space', $params['space'])
				->setUrlParams('ticket', $params['ticket'])
				->setUrlParams('document_id', $params['infos']['document_id']);
		return $item;
	}
	
	
	} //End of class
