<?php

/** Proxy to RbView_Menu_Menu with extension for generate menu of document actions
 * 
 * @author Administrateur
 *
 */
class RbView_Menu_Document_Main extends RbView_Menu_Document_Abstract{

	/**
	 * 
	 * @param array $params
	 * @param string $class
	 */
	public function __construct(array $params, $class = 'Form'){
		
		$class = 'RbView_Menu_Item_' . ucfirst($class);
		
		$subMenuAdmin = new $class('subMenuAdmin');
		$subMenuAdmin->setLabel( tra('Admin') );
		$subMenuAdmin->addChild($this->_getElement('resetdoctype', $params, $class))
					->addChild($this->_getElement('updatethumbnail', $params, $class))
					->addChild($this->_getElement('updateread', $params, $class))
					->addChild($this->_getElement('export', $params, $class))
					->addChild($this->_getElement('freeze', $params, $class))
					->addChild($this->_getElement('convert', $params, $class));
		
		$subMenuEdit = new $class('subMenuEdit');
		$subMenuEdit->setLabel( tra('Edit') );
		$subMenuEdit->addChild($this->_getElement('editDoc', $params, $class))
					->addChild($this->_getElement('lockdoc', $params, $class))
					->addChild($this->_getElement('unLockDoc', $params, $class))
					->addChild($this->_getElement('marktosuppress', $params, $class))
					->addChild($this->_getElement('unmarktosuppress', $params, $class))
					->addChild($this->_getElement('suppressdoc', $params, $class))
					->addChild($this->_getElement('movedocument', $params, $class))
					->addChild($this->_getElement('copydocument', $params, $class));
		
		$subMenuSend = new $class('subMenuSend');
		$subMenuSend->setLabel( tra('Send To') );
		$subMenuSend->addChild($this->_getElement('sendbymail', $params, $class))
					->addChild($this->_getElement('mailto', $params, $class));
		
		//$this->setLabel( tra('action') );
		$this->addChild($this->_getElement('createDoc', $params, $class))
			->addChild($this->_getElement('checkout', $params, $class))
			->addChild($this->_getElement('checkin', $params, $class))
			->addChild($this->_getElement('updateDoc', $params, $class))
			->addChild($this->_getElement('resetdoc', $params, $class))
			->addChild($this->_getElement('changestate', $params, $class))
			->addChild($this->_getElement('createversion', $params, $class))
			->addChild($this->_getElement('putinws', $params, $class))
			->addChild(new RbView_Menu_Item_Separator('separator1'))
			->addChild($this->_getElement('getHistory', $params, $class))
			->addChild($subMenuEdit)
			->addChild($subMenuSend)
			->addChild(new RbView_Menu_Item_Separator('separator2'))
			->addChild($subMenuAdmin)
			->addChild($this->_getElement('getlistfile', $params, $class))
			->addChild($this->_getElement('refresh', $params, $class))
			;
		return $this;
		
	}
	
	
	/**
	 * 
	 * @param RbView_Menu_Element $item
	 * @param array $params
	 */
	protected function _initOptions(RbView_Menu_Item_Abstract $item, array $params){
		$item->setDisplayText($params['displayText'])
				->setSuccessAction($params['ifSuccessAction'])
				->setSuccessController($params['ifSuccessController'])
				->setSuccessModule($params['ifSuccessModule'])
				->setFailedAction($params['ifFailedAction'])
				->setFailedController($params['ifFailedController'])
				->setFailedModule($params['ifFailedModule'])
				->setFormid($params['formId']);
		return $item;
	}
	
	public function getJs() {
		return;
	} //End of method
	
	
	
	} //End of class
