<?php

/**
 * 
 * @author Olivier CYSSAU
 *
 */
abstract class RbView_Menu_Document_Abstract extends RbView_Menu_Abstract{
	
	protected abstract function _initOptions(RbView_Menu_Item_Abstract $item, array $params);
	
	/**
	 * 
	 * @param array $params
	 * @param string $class
	 */	
	protected function _getElement($name, array $params, $class = 'Item'){
		
		$element = $this->_initOptions(new $class($name), $params);
		
		switch($name){
			case 'getHistory':
			$element->setLabel( tra ( 'Get history' ) )
						->setAction('get')
						->setController('history')
						->setModule('document')
						->setPopup(array('x'=>600, 'y'=>1200))
						->setIcon('img/icons/history.png');
			break;
			case 'editDoc':
			$element->setLabel( tra ( 'Edit' ) )
					->setAction('index')
					->setController('edit')
					->setModule('document')
					->setUrlParams('init', 1)
					->setPopup(array('x'=>700, 'y'=>600))
					->setIcon('img/icons/edit.png');
			break;
			case 'createDoc':
			$element->setLabel( tra ( 'Create' ) )
					->setAction('index')
					->setController('create')
					->setModule('document')
					->setUrlParams('init', 1)
					->setPopup(array('x'=>700, 'y'=>600))
					->setIcon('img/add.gif');
			break;
			case 'changestate':
			$element->setLabel( tra ( 'Change state' ) )
						->setAction('changestate')
						->setController('changestate')
						->setModule('document')
						->setIcon('img/icons/document/document_gear.png');
			break;
			case 'createversion':
			$element->setLabel( tra ( 'Create version' ) )
						->setAction('createversion')
						->setController('index')
						->setModule('document')
						->setIcon('img/icons/document/document_upindice.png');
			break;
			case 'addcomment':
			$element->setLabel( tra ( 'Add a postit' ) )
						->setAction('addcomment')
						->setController('index')
						->setModule('document')
						->setIcon('img/icons/comment/comment_add.png')
						->setOnclick("InputPostit('Input comment', '".$element->getUrl()."');return false;");
			break;
			case 'checkin':
			$element->setLabel( tra ( 'CheckIn' ) )
					->setAction('checkindoc')
					->setController('index')
					->setModule('document')
					->setIcon('img/icons/document/document_in.png');
			break;
			case 'checkout':
			$element->setLabel( tra ( 'CheckOut' ) )
					->setAction('checkoutdoc')
					->setController('index')
					->setModule('document')
					->setIcon('img/icons/document/document_out.png');
			break;
			case 'updateDoc':
			$element->setLabel( tra ( 'Update' ) )
					->setAction('updatedoc')
					->setController('index')
					->setModule('document')
					->setIcon('img/icons/document/document_refresh.png');
			break;
			case 'resetdoc':
			$element->setLabel( tra ( 'Reset' ) )
					->setAction('resetdoc')
					->setController('index')
					->setModule('document')
					->setIcon('img/icons/document/document_cancel.png');
			break;
			case 'lockdoc':
			$element->setLabel( tra ( 'Lock' ) )
					->setAction('lock')
					->setController('index')
					->setModule('document')
					->setConfirmation(tra('Are you sure?'))
					->setIcon('img/icons/lock.png');
			break;
			case 'unLockDoc':			
			$element->setLabel( tra ( 'Unlock' ) )
					->setAction('unLockDoc')
					->setController('index')
					->setModule('document')
					->setIcon('img/icons/lock_open.png');
			break;
			case 'assocfile':
			$element->setLabel( tra ( 'Associate a file' ) )
					->setAction('assocfile')
					->setController('index')
					->setModule('document')
					->setIcon('img/icons/file/file_attach.png');
			break;
			case 'assocattachment':
			$element->setLabel( tra ( 'Associate a visualisation' ) )
							->setAction('assocattachment')
							->setController('index')
							->setModule('document')
							->setIcon('img/icons/file/file_attach.png');
			break;
			case 'putinws':
			$element->setLabel( tra ( 'Put in wildspace' ) )
					->setAction('putinws')
					->setController('index')
					->setModule('document')
					->setIcon('img/icons/document/document_read.png');
			break;
			case 'resetdoctype':
			$element->setLabel( tra ( 'Recalculate the doctype' ) )
						->setAction('resetdoctype')
						->setController('index')
						->setModule('document')
						->setIcon('img/icons/doctype/doctype_reset.png');
				
			break;
			case 'convert':
			$element->setLabel( tra ( 'Convert' ) )
					->setAction('convert')
					->setController('index')
					->setModule('document')
					->setIcon('img/icons/document/document_convert.png');
			break;
			case 'unmarktosuppress':
			$element->setLabel( tra ( 'Unmark to suppress' ) )
							->setAction('unmarktosuppress')
							->setController('index')
							->setModule('document')
							->setIcon('img/icons/document/unMarkToSuppress.png');
			break;
			case 'marktosuppress':
			$element->setLabel( tra ( 'Mark to suppress' ) )
							->setAction('marktosuppress')
							->setController('index')
							->setModule('document')
							->setConfirmation(tra('Are you sure?'))
							->setIcon('img/icons/document/markToSuppress.png');
			break;
			case 'suppressdoc':
			$element->setLabel( tra ( 'Suppress' ) )
						->setAction('suppressdoc')
						->setController('index')
						->setModule('document')
						->setIcon('img/icons/trash.png');
			break;
			case 'movedocument':
			$element->setLabel( tra ( 'Move' ) )
						->setAction('movedocument')
						->setController('index')
						->setModule('document')
						->setPopup( array('x'=>450, 'y'=>800))
						->setIcon('img/icons/document/document_move.png');
			break;
			case 'copydocument':
			$element->setLabel( tra ( 'Copy' ) )
						->setAction('copydocument')
						->setController('index')
						->setModule('document')
						->setPopup( array('x'=>600, 'y'=>900) )
						->setIcon('img/icons/document/document_copy.png');
			break;
			case 'freeze':
			$element->setLabel( tra ( 'Freeze' ) )
					->setAction('freeze')
					->setController('index')
					->setModule('document')
					->setIcon('img/icons/document/document_freeze.png');
			break;
			case 'updatethumbnail':
			$element->setLabel( tra ( 'Update thumbnail' ) )
							->setAction('updatethumbnail')
							->setController('index')
							->setModule('document')
							->setIcon('img/icons/document/updateThumb.png');
			break;
			case 'updateread':
			$element->setLabel( tra ( 'Update read' ) )
						->setAction('updateread')
						->setController('index')
						->setModule('document')
						->setIcon('img/icons/document/updateRead.png');

			break;
			case 'export':
			$element->setLabel( tra ( 'Export' ) )
					->setAction('export')
					->setController('index')
					->setModule('document')
					->setIcon('img/icons/export.gif');
			break;
			case 'sendbymail':
			$element->setLabel( tra ( 'Send by mail with ranchbe' ) )
						->setAction('sendbymail')
						->setController('index')
						->setModule('document')
						->setIcon('img/icons/mail/go.png');
			break;
			case 'mailto':
			$element->setLabel( tra ( 'Send by mail with your mail application' ) )
					->setAction('mailto')
					->setController('index')
					->setModule('document')
					->setIcon('img/icons/mail/go.png');
			break;
			case 'getlistfile':
			$element->setLabel( tra ( 'Download listing' ) )
					->setAction('getlistfile')
					->setController('index')
					->setModule('document')
					->setIcon('img/icons/page_white_excel.png');
			break;
			case 'refresh':
			$element->setLabel( tra ( 'Refresh' ) )
					->setAction('document')
					->setController('get')
					->setModule('content')
					->setIcon('img/icons/refresh.png');
			default:
				return false;
				break;
		}
		
		return $element;
		
	} //End of method

	} //End of class
