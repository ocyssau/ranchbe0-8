<?php

/** 
 * 
 * @author Olivier CYSSAU
 *
 */
class RbView_Menu_Docfile_Main extends RbView_Menu_Abstract{

	/**
	 * 
	 * @param array $params
	 * @param string $class
	 */
	public function __construct(array $params, $class = 'Form'){
		
		$class = 'RbView_Menu_Item_' . ucfirst($class);
		
		$putinws = new $class('putinws');
		$this->_initOptions($putinws, $params);
		$putinws->setLabel( tra ( 'Put in wildspace' ) )
					->setAction('putinws')
					->setController('index')
					->setModule('docfile')
					->setIcon('img/icons/document/document_read.png');

		$setrole = new $class('setrole');
		$this->_initOptions($setrole, $params);
		$setrole->setLabel( tra ( 'Set docfile role' ) )
					->setAction('setrole')
					->setController('index')
					->setModule('docfile')
					->setIcon('img/icons/docfile/role.png');
					
		$reset = new $class('reset');
		$this->_initOptions($reset, $params);
		$reset->setLabel( tra ( 'Reset' ) )
					->setAction('reset')
					->setController('index')
					->setModule('docfile')
					->setIcon('img/icons/document/document_cancel.png');
					

		$refresh = new $class('refresh');
		$this->_initOptions($refresh, $params);
		$refresh->setLabel( tra ( 'Refresh' ) )
					->setAction($params['ifSuccessAction'])
					->setController($params['ifSuccessController'])
					->setModule($params['ifSuccessModule'])
					->setIcon('img/icons/refresh.png');
		    
		$suppress = new $class('suppress');
		$this->_initOptions($suppress, $params);
		$suppress->setLabel( tra ( 'Suppress' ) )
					->setAction('suppress')
					->setController('index')
					->setModule('docfile')
					->setConfirmation( tra('Do you want really suppress this file'))
					->setIcon('img/icons/trash.png');
					
		$this->addChild($setrole);
		$this->addChild($reset);
		$this->addChild($putinws);
		$this->addChild($suppress);
		$this->addChild(new RbView_Menu_Item_Separator('separator1'));
		$this->addChild($refresh);
		
		return $this;
		
	}
	
	/**
	 * 
	 * @param RbView_Menu_Element $item
	 * @param array $params
	 */
	protected function _initOptions(RbView_Menu_Item_Abstract $item, array $params){
		$item->setDisplayText($params['displayText'])
				->setSuccessAction($params['ifSuccessAction'])
				->setSuccessController($params['ifSuccessController'])
				->setSuccessModule($params['ifSuccessModule'])
				->setFailedAction($params['ifFailedAction'])
				->setFailedController($params['ifFailedController'])
				->setFailedModule($params['ifFailedModule'])
				->setFormid($params['formId']);
		return $item;
	}
	
	
	} //End of class
