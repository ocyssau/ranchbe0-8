<?php

/** 
 * 
 * @author Olivier CYSSAU
 *
 */
class RbView_Menu_Project_Contextual extends RbView_Menu_Abstract{

	/**
	 * 
	 * @param array $params
	 * @param string $class
	 */
	public function __construct(array $params, $class = 'Href'){
		
		$class = 'RbView_Menu_Item_' . ucfirst($class);
		
		$this->setLabel( tra('action') );
		
		$subMenuLinks = new $class('subMenuLinks');
		$subMenuLinks->setLabel( tra('Links') );
		$subMenuLinks->addChild($this->_getElement('getdoctypes', $params, $class));
		$subMenuLinks->addChild($this->_getElement('getcadlibs', $params, $class));
		$subMenuLinks->addChild($this->_getElement('getbookshops', $params, $class));
		$subMenuLinks->addChild($this->_getElement('getmockups', $params, $class));
		
		$this->addChild($subMenuLinks);
		$this->addChild($this->_getElement('putinws', $params, $class));
		$this->addChild(new RbView_Menu_Item_Separator('separator1'));
		$this->addChild($this->_getElement('getPerms', $params, $class));
		$this->addChild($this->_getElement('edit', $params, $class));
		$this->addChild($this->_getElement('suppress', $params, $class));
		$this->addChild(new RbView_Menu_Item_Separator('separator2'));
		$this->addChild($this->_getElement('getHistory', $params, $class));
		
		return $this;

	}
	
	/**
	 * 
	 * @param RbView_Menu_Element $item
	 * @param array $params
	 */
	protected function _initOptions(RbView_Menu_Item_Abstract $item, array $params){
		$item->setDisplayText($params['displayText'])
				->setSuccessAction($params['ifSuccessAction'])
				->setSuccessController($params['ifSuccessController'])
				->setSuccessModule($params['ifSuccessModule'])
				->setFailedAction($params['ifFailedAction'])
				->setFailedController($params['ifFailedController'])
				->setFailedModule($params['ifFailedModule'])
				->setUrlParams('space', $params['space'])
				->setUrlParams('ticket', $params['ticket'])
				->setUrlParams('project_id', $params['infos']['project_id']);
		return $item;
	}
	
	/**
	 * 
	 * @param array $params
	 * @param string $class
	 */	
	protected function _getElement($name, array $params, $class){
		
		$element = $this->_initOptions(new $class($name), $params);
		
		switch($name){
			case 'getdoctypes':
				$element->setLabel( tra ( 'Doctypes' ) )
						->setAction('getdoctypes')
						->setController('links')
						->setModule('project')
						->setPopup(array('x'=>900, 'y'=>900))
						->setIcon('img/icons/link_edit.png');
				break;
			case 'getmockups':
				$element->setLabel( tra ( 'Mockups' ) )
						->setAction('getmockups')
						->setController('links')
						->setModule('project')
						->setPopup(array('x'=>900, 'y'=>900))
						->setIcon('img/icons/link_edit.png');
				break;
			case 'getcadlibs':
				$element->setLabel( tra ( 'Cadlibs' ) )
						->setAction('getcadlibs')
						->setController('links')
						->setModule('project')
						->setPopup(array('x'=>900, 'y'=>900))
						->setIcon('img/icons/link_edit.png');
				break;
			case 'getbookshops':
				$element->setLabel( tra ( 'Bookshops' ) )
						->setAction('getbookshops')
						->setController('links')
						->setModule('project')
						->setPopup(array('x'=>900, 'y'=>900))
						->setIcon('img/icons/link_edit.png');
				break;
			case 'getPerms':
				$element->setLabel( tra ( 'permissions' ) )
						->setAction('get')
						->setController('perms')
						->setModule('project')
						->setPopup(array('x'=>550, 'y'=>650))
						->setUrlParams('object_id', $params['infos']['project_id'])
						->setIcon('img/icons/key.png');
				break;
			case 'suppress':
				$element->setLabel( tra ( 'Suppress project' ) )
						->setAction('suppress')
						->setController('index')
						->setModule('project')
						->setConfirmation( tra('Do you want really suppress') .' '. $params['infos']['project_number'])
						->setIcon('img/icons/trash.png');
				break;
			case 'edit':
				$element->setLabel( tra ( 'edit' ) )
						->setAction('edit')
						->setController('index')
						->setModule('project')
						->setPopup(array('x'=>600, 'y'=>600))
						->setIcon('img/icons/edit.png');
				break;
			case 'getHistory':
				$element->setLabel( tra ( 'Get history' ) )
						->setAction('get')
						->setController('history')
						->setModule('project')
						->setPopup(array('x'=>1180, 'y'=>1180))
						->setIcon('img/icons/history.png');
				break;
			default:
				return false;
				break;
		}
		return $element;
	}
	
	
	} //End of class
