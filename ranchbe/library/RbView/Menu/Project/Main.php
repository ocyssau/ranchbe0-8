<?php

/** Proxy to RbView_Menu_Menu with extension for generate menu of document actions
 * 
 * @author Administrateur
 *
 */
class RbView_Menu_Project_Main extends RbView_Menu_Abstract{

	/**
	 * 
	 * @param array $params
	 * @param string $class
	 */
	public function __construct(array $params, $class = 'Form'){
		
		$class = 'RbView_Menu_Item_' . ucfirst($class);
		
		$refresh = new $class('refresh');
		$this->_initOptions($refresh, $params);
		$refresh->setLabel( tra ( 'Refresh' ) )
					->setAction('index')
					->setController('index')
					->setModule('project')
					->setIcon('img/icons/refresh.png');
		
		$suppress = new $class('suppress');
		$this->_initOptions($suppress, $params);
		$suppress->setLabel( tra ( 'Suppress' ) )
					->setAction('suppress')
					->setController('index')
					->setModule('project')
					->setConfirmation( tra('Do you want really suppress'))
					->setIcon('img/icons/trash.png');
		
		$this->addChild($suppress);
		$this->addChild(new RbView_Menu_Item_Separator('separator1'));
		$this->addChild($refresh);
		
		return $this;
		
	}
	
	
	/**
	 * 
	 * @param RbView_Menu_Element $item
	 * @param array $params
	 */
	protected function _initOptions(RbView_Menu_Item_Abstract $item, array $params){
		$item->setDisplayText($params['displayText'])
				->setSuccessAction($params['ifSuccessAction'])
				->setSuccessController($params['ifSuccessController'])
				->setSuccessModule($params['ifSuccessModule'])
				->setFailedAction($params['ifFailedAction'])
				->setFailedController($params['ifFailedController'])
				->setFailedModule($params['ifFailedModule'])
				->setFormid($params['formId']);
		return $item;
	}
	
	
	} //End of class
