<?php

/** 
 * 
 * @author Olivier CYSSAU
 *
 */
class RbView_Menu_Container_Contextual extends RbView_Menu_Abstract{

	/**
	 * 
	 * @param array $params
	 * @param string $class
	 */
	public function __construct(array $params, $class = 'Href'){
		
		$class = 'RbView_Menu_Item_' . ucfirst($class);
		
		$this->setLabel( tra('action') );
		
		$subMenuLinks = new $class('subMenuLinks');
		$subMenuLinks->setLabel( tra('Links') );
		
		$subMenuLinks->addChild($this->_getElement('getdoctypes', $params, $class));
		$subMenuLinks->addChild($this->_getElement('getmetadatas', $params, $class));
		$subMenuLinks->addChild($this->_getElement('getcategories', $params, $class));
		$subMenuLinks->addChild($this->_getElement('getreads', $params, $class));
		
		$this->addChild($subMenuLinks);
		
		$this->addChild(new RbView_Menu_Item_Separator('separator1'));
		$this->addChild($this->_getElement('getPerms', $params, $class));
		$this->addChild($this->_getElement('edit', $params, $class));
		$this->addChild($this->_getElement('notification', $params, $class));
		$this->addChild($this->_getElement('suppress', $params, $class));
		$this->addChild(new RbView_Menu_Item_Separator('separator2'));
		$this->addChild($this->_getElement('stats', $params, $class));
		$this->addChild($this->_getElement('getHistory', $params, $class));
		
		return $this;

	}
	
	
	/**
	 * 
	 * @param RbView_Menu_Element $item
	 * @param array $params
	 */
	protected function _initOptions(RbView_Menu_Item_Abstract $item, array $params){
		$item->setDisplayText($params['displayText'])
				->setSuccessAction($params['ifSuccessAction'])
				->setSuccessController($params['ifSuccessController'])
				->setSuccessModule($params['ifSuccessModule'])
				->setFailedAction($params['ifFailedAction'])
				->setFailedController($params['ifFailedController'])
				->setFailedModule($params['ifFailedModule'])
				->setUrlParams('space', $params['space'])
				->setUrlParams('ticket', $params['ticket'])
				->setUrlParams('container_id', $params['infos'][$params['space'].'_id']);
		return $item;
	}
	
	
	/**
	 *
	 * @param array $params
	 * @param string $class
	 */
	protected function _getElement($name, array $params, $class){

		$element = $this->_initOptions(new $class($name), $params);

		switch($name){
			case 'getdoctypes':
				$element->setLabel( tra ( 'Doctypes' ) )
						->setAction('getdoctypes')
						->setController('links')
						->setModule('container')
						->setPopup(array('x'=>900, 'y'=>900))
						->setIcon('img/icons/link_edit.png');
						break;
			case 'getmetadatas':
				$element->setLabel( tra ( 'Metadatas' ) )
						->setAction('getmetadatas')
						->setController('links')
						->setModule('container')
						->setPopup(array('x'=>900, 'y'=>900))
						->setIcon('img/icons/link_edit.png');
						break;
			case 'getcategories':
				$element->setLabel( tra ( 'Categories' ) )
						->setAction('getcategories')
						->setController('links')
						->setModule('container')
						->setPopup(array('x'=>900, 'y'=>900))
						->setIcon('img/icons/link_edit.png');
						break;
			case 'getreads':
				$element->setLabel( tra ( 'Reads' ) )
						->setAction('getreads')
						->setController('links')
						->setModule('container')
						->setPopup(array('x'=>900, 'y'=>900))
						->setIcon('img/icons/link_edit.png');
						break;
			case 'getPerms':
				$element->setLabel( tra ( 'permissions' ) )
						->setAction('get')
						->setController('perms')
						->setModule('container')
						->setPopup(array('x'=>550, 'y'=>650))
						->setUrlParams('object_id', $params['infos'][$params['space'].'_id'])
						->setIcon('img/icons/key.png');
						break;
			case 'notification':
				$element->setLabel( tra ( 'Notification' ) )
						->setAction('get')
						->setController('notification')
						->setModule('container')
						->setPopup(array('x'=>900, 'y'=>900))
						->setIcon('img/icons/container/notification.png');
						break;
			case 'suppress':
				$element->setLabel( tra ( 'Suppress' ) )
						->setAction('suppress')
						->setController('index')
						->setModule('container')
						->setConfirmation( tra('Do you want really suppress') .' '. $params['infos'][$params['space'].'_number'] )
						->setIcon('img/icons/trash.png');
						break;
			case 'edit':
				$element->setLabel( tra ( 'edit' ) )
						->setAction('edit')
						->setController('index')
						->setModule('container')
						->setPopup(array('x'=>900, 'y'=>600))
						->setIcon('img/icons/edit.png');
						break;
			case 'stats':
				$element->setLabel( tra ( 'Statistics' ) )
						->setAction('get')
						->setController('stats')
						->setModule('container')
						->setIcon('img/icons/statistics.png');
						break;
			case 'getHistory':
				$element->setLabel( tra ( 'Get history' ) )
						->setAction('get')
						->setController('history')
						->setModule('container')
						->setPopup(array('x'=>1180, 'y'=>1180))
						->setIcon('img/icons/history.png');
						break;
		}
		
		return $element;
			
	}


} //End of class
