<?php

/** 
 * 
 * @author Olivier CYSSAU
 *
 */
class RbView_Menu_Container_Alias_Contextual extends RbView_Menu_Container_Contextual{

	/**
	 * 
	 * @param RbView_Menu_Item_Abstract $item
	 * @param array $params
	 */
	protected function _initOptions(RbView_Menu_Item_Abstract $item, array $params){
		$item->setDisplayText($params['displayText'])
				->setSuccessAction($params['ifSuccessAction'])
				->setSuccessController($params['ifSuccessController'])
				->setSuccessModule($params['ifSuccessModule'])
				->setFailedAction($params['ifFailedAction'])
				->setFailedController($params['ifFailedController'])
				->setFailedModule($params['ifFailedModule'])
				->setUrlParams('space', $params['space'])
				->setUrlParams('ticket', $params['ticket'])
				->setUrlParams('alias_id', $params['infos']['alias_id']);
		return $item;
	}
	
	/**
	 *
	 * @param array $params
	 * @param string $class
	 */
	protected function _getElement($name, array $params, $class){

		switch($name){
			case 'suppress':
				$element = $this->_initOptions(new $class($name), $params);
				$element->setLabel( tra ( 'Suppress' ) )
						->setAction('suppress')
						->setController('alias')
						->setModule('container')
						->setConfirmation( tra('Do you want really suppress') .' '. $params['infos'][$params['space'].'_number'] )
						->setIcon('img/icons/trash.png');
						break;
			case 'edit':
				$element = $this->_initOptions(new $class($name), $params);
				$element->setLabel( tra ( 'edit' ) )
						->setAction('edit')
						->setController('alias')
						->setModule('container')
						->setPopup(array('x'=>900, 'y'=>600))
						->setIcon('img/icons/edit.png');
						break;
			default:
				return parent::_getElement($name, $params, $class);
		}
		
		return $element;
			
	}


} //End of class
