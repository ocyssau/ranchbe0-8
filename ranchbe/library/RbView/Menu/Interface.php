<?php

interface RbView_Menu_Interface{
	public function addChild($item);
	public function hasChilds();
	public function getChilds();
	public function getChild($name);
	public function setName($value);
	public function getName();
	public function setLabel($value);
	public function getLabel();
	public function setIcon($url);
	public function render();
}
