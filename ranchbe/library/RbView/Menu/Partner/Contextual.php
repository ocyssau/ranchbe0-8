<?php

/** 
 * 
 * @author Olivier CYSSAU
 *
 */
class RbView_Menu_Partner_Contextual extends RbView_Menu_Abstract{

	/**
	 * 
	 * @param array $params
	 * @param string $class
	 */
	public function __construct(array $params, $class = 'Form'){
		
		$class = 'RbView_Menu_Item_' . ucfirst($class);
		
		$edit = new $class('edit');
		$this->_initOptions($edit, $params);
		$edit->setLabel( tra ( 'edit' ) )
					->setAction('edit')
					->setController('index')
					->setModule('partner')
					->setPopup(array('x'=>600, 'y'=>600))
					->setIcon('img/icons/edit.png');
    
		$refresh = new $class('refresh');
		$this->_initOptions($refresh, $params);
		$refresh->setLabel( tra ( 'Refresh' ) )
					->setAction($params['ifSuccessAction'])
					->setController($params['ifSuccessController'])
					->setModule($params['ifSuccessModule'])
					->setIcon('img/icons/refresh.png');
		    
		$suppress = new $class('suppress');
		$this->_initOptions($suppress, $params);
		$suppress->setLabel( tra ( 'Suppress' ) )
					->setAction('suppress')
					->setController('index')
					->setModule('partner')
					->setConfirmation( tra('Do you want really suppress') .' '. $params['infos']['partner_number'])
					->setIcon('img/icons/trash.png');
					
		$this->addChild($edit);
		$this->addChild($suppress);
		$this->addChild(new RbView_Menu_Item_Separator('separator1'));
		$this->addChild($refresh);
		
		return $this;
		
	}
	
	/**
	 * 
	 * @param RbView_Menu_Element $item
	 * @param array $params
	 */
	protected function _initOptions(RbView_Menu_Item_Abstract $item, array $params){
		$item->setDisplayText($params['displayText'])
				->setSuccessAction($params['ifSuccessAction'])
				->setSuccessController($params['ifSuccessController'])
				->setSuccessModule($params['ifSuccessModule'])
				->setFailedAction($params['ifFailedAction'])
				->setFailedController($params['ifFailedController'])
				->setFailedModule($params['ifFailedModule'])
				->setUrlParams('space', $params['space'])
				->setUrlParams('ticket', $params['ticket'])
				->setUrlParams('partner_id', $params['infos']['partner_id']);
		return $item;
	}
	
	
	} //End of class
