<?php

/** 
 * 
 * @author Olivier CYSSAU
 *
 */
class RbView_Menu_RepositRead_Main extends RbView_Menu_Abstract{

	/**
	 * 
	 * @param array $params
	 * @param string $class
	 */
	public function __construct(array $params, $class = 'Href'){
		
		$class = 'RbView_Menu_Item_' . ucfirst($class);
		
		$this->setLabel( tra('action') );
		
		$this->addChild($this->_getElement('edit', $params, $class));
		$this->addChild($this->_getElement('rebuild', $params, $class));
		$this->addChild(new RbView_Menu_Item_Separator('separator1'));
		$this->addChild($this->_getElement('suppress', $params, $class));
		
		return $this;

	}
	
	
	/**
	 * 
	 * @param RbView_Menu_Element $item
	 * @param array $params
	 */
	protected function _initOptions(RbView_Menu_Item_Abstract $item, array $params){
		$item->setDisplayText($params['displayText'])
				->setSuccessAction($params['ifSuccessAction'])
				->setSuccessController($params['ifSuccessController'])
				->setSuccessModule($params['ifSuccessModule'])
				->setFailedAction($params['ifFailedAction'])
				->setFailedController($params['ifFailedController'])
				->setFailedModule($params['ifFailedModule'])
				->setFormid($params['formId']);
		return $item;
	}
	
	/**
	 * 
	 * @param array $params
	 * @param string $class
	 */	
	protected function _getElement($name, array $params, $class){
		
		$element = $this->_initOptions(new $class($name), $params);

		switch($name){
			case 'edit':
				$element->setLabel( tra ( 'Edit' ) )
						->setAction('edit')
						->setController('read')
						->setModule('reposit')
						->setPopup(array('x'=>300, 'y'=>500))
						->setIcon('img/icons/reposit/edit.png');
				break;
			case 'suppress':
				$element->setLabel( tra ( 'Suppress' ) )
						->setAction('suppress')
						->setController('read')
						->setModule('reposit')
						->setConfirmation( tra('Do you want really suppress') )
						->setIcon('img/icons/reposit/delete.png');
				break;
			case 'rebuild':
				$element->setLabel( tra ( 'Rebuild read reposit' ) )
						->setAction('rebuild')
						->setController('read')
						->setModule('reposit')
						->setIcon('img/icons/reposit/rebuild.png');
				break;
			default:
				return false;
				break;
		}
		return $element;
	}
	
	
	} //End of class
