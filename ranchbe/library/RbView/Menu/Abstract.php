<?php

/**
 * 
 * @author Olivier CYSSAU
 *
 */
class RbView_Menu_Abstract implements RbView_Menu_Interface{
	protected $_childs = array();
	protected $_name; //name of menu
	protected $_label; //displayed name
	protected $_icon_url; //name of menu
	
	
	//----------------------------------------------------------------------------
	public function __construct($options) {
		if( is_array($options) ) {
			$this->setName( $options['name'] );
			$this->setLabel( $options['label'] );
			$this->setIcon( $options['icon'] );
		}
		if( is_string($options) ) {
			$this->setName($options);
		}
	} //End of method
	
	//----------------------------------------------------------------------------
	public function addChild($item){
		if( is_a($item, 'RbView_Menu_Item_Abstract')  ){
			$this->_childs[$item->getName()] = $item;
		}
		return $this;
	}
	
	//----------------------------------------------------------------------------
	public function hasChilds(){
		if( $this->_childs ) return true;
		else return false;
	}
	
	//----------------------------------------------------------------------------
	public function getChilds(){
		return $this->_childs;
	}
	
	//----------------------------------------------------------------------------
	public function getChild($name){
		if($this->_childs[$name]) return $this->_childs[$name];
		else return false;
	}
	
	//----------------------------------------------------------------------------
	public function setName($name) {
		$this->_name = $name;
		return $this;
	}

	//----------------------------------------------------------------------------
	public function getName() {
		return $this->_name;
	}
	
	//----------------------------------------------------------------------------
	public function setLabel( $value ) {
		$this->_label = (string) $value;
		return $this;
	}
	
	//----------------------------------------------------------------------------
	public function getLabel() {
		return $this->_label;
	}

	//----------------------------------------------------------------------------
	public function setIcon($url) {
		$this->_icon_url = $url;
		return $this;
	}
	
	//----------------------------------------------------------------------------
	public function render(){
		//var_dump( $this->getChilds() );die;
		$html = '';
	    $html .= '<div class="cacher"  dojoType=\'dijit.form.DropDownButton\' iconClass=\'noteIcon\' onLoad="load(this.id);">';
		$html .= "<span>";
		if ( $this->_icon_url ) {
			$html .= '<img border="0" src="' . $this->_icon_url . '"/>';
		}
		$html .= $this->_label . "</span>";
	    $html .= '<div dojoType=\'dijit.Menu\' >';
		
		if( $this->hasChilds() ){
			foreach($this->getChilds() as $child){
				$html .= $child->render();
			}
		}
		
		$html .= '</div></div>';
		return $html;
	}
	

} //End of class
