<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


class RbView_Flood {
	protected static $_ticket = '';
	protected static $_ticketStack = array ();
	protected static $_isValid = false; //true to keep validity after supression of session ticket
	

	//------------------------------------------------------------------------
	/**
	 * Anti-Flood V3. Condition existance du ticket en session. Tous les tickets 
	 * sont conservés en session jusqu'a leur utilisation.
	 * Inspired by principle described in
	 *   http://www.developpez.net/forums/archive/index.php/t-104783.html
	 * If session jeton is egal to jeton record in form, generate a new jeton.
	 * session_register('jeton');
	 * 
	 * @param string
	 * @return boolean
	 *   
	 */
	public static function checkFlood($ticket) {
		if (self::$_isValid)
			return true;
		if (Ranchbe::getConfig ()->flood->check == false)
			return true;
			//Zend_Debug::dump($_SESSION);

		$ticketStack = & self::$_ticketStack;
		
		//Return array key of the ticket or false if not a valid ticket
		$ticketKey = array_search ( $ticket, $ticketStack );
		
		if ($ticketKey) {
			unset ( $ticketStack [$ticketKey] ); // Suppress ticket in session
			self::setTicket (); // generate a new ticket
			self::$_isValid = true; //keep validity
			return true;
		} else {
			self::setTicket (); // generate a new ticket
			return false;
		}
	
	} //End of check Flood
	

	//------------------------------------------------------------------------
	/** Check time interval between 2 requests
	 * 
	 * @return boolean
	 */
	public static function checkTime() {
		//init time tracker
		if (! $_SESSION ['time_tracker']) {
			$_SESSION ['time_tracker'] = time ();
			return true;
		}
		
		//init Flood tracker
		if (! $_SESSION ['flood_tracker']) {
			$_SESSION ['flood_tracker'] = 1;
			return true;
		}
		
		if ((time () - $_SESSION ['time_tracker']) < Ranchbe::getConfig ()->flood->time_interval) { // If the page is recall after time < to one second
			$_SESSION ['flood_tracker'] ++;
			if ($_SESSION ['flood_tracker'] > Ranchbe::getConfig ()->flood->max_request_number) {
				print '<b>You can not doing ' 
					. Ranchbe::getConfig ()->flood->max_request_number 
					. ' requests in ' . Ranchbe::getConfig ()->flood->time_interval 
					. ' seconds.<br />Now, wait ' 
					. Ranchbe::getConfig ()->flood->wait_time 
					. ' seconds and refresh this page...</b>';
				$_SESSION ['time_tracker'] = time () + Ranchbe::getConfig ()->flood->wait_time;
				die ();
			}
		} else {
			$_SESSION ['flood_tracker'] = 1;
			$_SESSION ['time_tracker'] = time ();
			return true;
		}
	
	} //End of methode
	

	//------------------------------------------------------------------------
	/** Generate the ticket. If $string is set, generate the ticket from this string
	 *  md5 encode, else, generate a unpredictible random string
	 *  
	 * @param String String to encode with md5
	 * @return String Ticket
	 *
	 */
	public static function setTicket($string = '') {
		//Get the ticket stack from session
		self::_initStack ();
		$ticketStack = & self::$_ticketStack;
		//generate a random string if none string
		if (! $string) {
			self::$_ticket = md5 ( uniqid ( mt_rand (), true ) );
		} else {
			self::$_ticket = md5 ( $string );
			//check if ticket is yet in stack
			if (array_search ( self::$_ticket, $_SESSION ['ticket_gf'] ) !== false) {
				Ranchbe::getSmarty ()->assign ( 'ticket', self::$_ticket );
				return self::$_ticket;
			}
		}
		//add ticket to stack begining
		array_unshift ( $ticketStack, self::$_ticket );
		//assign ticket to view
		Ranchbe::getSmarty ()->assign ( 'ticket', self::$_ticket );
		//clean older ticket
		if (count ( $ticketStack ) > Ranchbe::getConfig ()->flood->stack->max_size) {
			array_pop ( $ticketStack );
		}
		return self::$_ticket;
	} //End of methode
	

	//------------------------------------------------------------------------
	/** Return ticket for current request
	 * 
	 * @return string
	 */
	public static function getTicket() {
		return self::$_ticket;
	} //End of methode
	

	//------------------------------------------------------------------------
	/** Init the ticketStack
	 * 
	 * @return unknown_type
	 */
	protected static function _initStack() {
		if (! self::$_ticketStack) {
			if (! $_SESSION ['ticket_gf'])
				$_SESSION ['ticket_gf'] = array ();
			self::$_ticketStack =& $_SESSION ['ticket_gf'];
		}
	} //End of methode


} //End of class
