<?php
//=======================================================================================================
//Definition of action href
//=======================================================================================================
class RbView_Action{

public $module; //string
public $controller; //string
public $action; //string
public $action_url; //string
public $icon_url; //string
public $title; //string
public $alt; //string
public $page_id; //string
public $space; //string
public $isPopup; //bool
public $popupx; //int
public $popupy; //int
public $displayText; //bool
public $html; //string
public $isActive; //bool
public $ifSuccessForward='default'; //string
public $ifFailedForward='default'; //string
public $form_name='checkform'; //string

public static $action_def = array();

protected function _actionUrl(){
  $this->action_url = BASE_URL.'/'.$this->module.'/'.$this->controller.'/'.$this->action;
  if($this->form_id) $inForm = true; else $inForm = false;
  if( is_array($this->query) && !$inForm )
  foreach($this->query as $key=>$val){
    if($val)
      $this->action_url .= '/'.$key.'/'.$val;
  }
  if( $this->ifSuccessForward != 'default' && $this->ifSuccessForward ){
    $this->action_url .= $this->ifSuccessForward;
  }
  if( $this->ifFailedForward != 'default' && $this->ifFailedForward ){
    $this->action_url .= $this->ifFailedForward;
  }
  return $this->action_url;
} //End of method

protected function _actionHref(){
  if( $this->onClick ){
    $href = 'onClick="'.$this->onClick.'" ';
  }else{
    if ( $this->isPopup ){
      $href = 'onclick="javascript:popupP(\''.$this->action_url.'\',\''.
                                                  $this->title.'\','.
                                                  $this->popupx.','.
                                                  $this->popupy.');return false;"';
    }else{
      $href = '\'onclick="document.location.href=\''.$this->action_url.'\';return false;"';
    }
  }
  return $href;
} //End of method

protected function _actionForm(){
  if( $this->onClick ){
    $href = 'onClick="'.$this->onClick.'" ';
  }else{
    $href = '\'onclick="document.'.$this->form_id.'.action=\''.$this->action_url.'\'; ';
  }

  if ( $this->isPopup ){
    $href .= 'pop_it('.$this->form_id.', '.$this->popupx.' , '.$this->popupy.');';
  }else{
    $href .= 'pop_no('.$this->form_id.');';
  }

  return $href;
} //End of method

//------------------------------------------------------------------------------
public function toHref(){
  if(!$this->action_url){
    $this->_actionUrl();
  }
  $html = '<a href="" title="'.$this->title.'" ';
  $html .= $this->_actionHref();
  $html .= '>';
  if( !empty($this->icon_url) ){
    $html .= '<img border="0" title="'.$this->title.'" alt="'.$this->alt.'" src="'.$this->icon_url.'"/>';
  }
  if( $this->displayText ){
    $html .= $this->title;
  }
  $html .= '</a>';
  //return preg_replace('!\s+!', '', $html);
  return $html;
} //End of method

//------------------------------------------------------------------------------
public function toButton(){  
  if(!$this->action_url){
    $this->_actionUrl();
  }
  $html = '<button class="multi-submit" type="submit" name="'.$this->action.'"'.
            'id="'.$this->action.'" value="'.$this->action.'" title="'.$this->title.
            '"';
  if ( $this->form_id ){
    $html .= $this->_actionForm();
  }else{
    $html .= $this->_actionHref();
  }
  $html .= '">';
  $html .= '<img class="icon" src="'.$this->icon_url.'" title="'.$this->title.'" width="16" height="16" />';
  if( $this->displayText ){
    $html .= '<small>'.$this->title.'</small>';
  }
  $html .= '</button>';
  return $html;
} //End of method

public static function initJsForm(){
  $js1 = "
  <script language='Javascript' type='text/javascript'>
  <!---->
  </script>";
  return $js1;
} //End of method

public static function initJs(){
  $js1 = "
  <script language='Javascript' type='text/javascript'>
  <!---->
  </script>";
  return $js1;
} //End of method
} //End of class
