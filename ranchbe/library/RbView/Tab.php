<?php

class RbView_Tab {
	
	protected $_name; //name of tab
	protected $_url; //url of link
	protected $_label; //displayed name
	protected $_isActive = false;
	protected $_father; //tab object. Its the tab wich activate the bar when is clicked
	protected $_level = 0; //level 0 for main bar and +1 for each sub-bar
	protected static $_instances = array (); //registry of tabs
	protected static $_structure = array (); //array of menu structure

	//----------------------------------------------------------------------------
	public function __construct($tabname, $url, $label, RbView_Tab &$father = null) {
		$this->_name = $tabname;
		if ($_SESSION ['tabs'] [$tabname] ['activePage'])
			$this->_url = $_SESSION ['tabs'] [$tabname] ['activePage'];
		else
			$this->_url = $url;
		$this->_label = $label;
		if (is_a ( $father , 'RbView_Tab' )) {
			$this->_father = & $father;
			$this->_level = $this->_father->getLevel () + 1;
		}
		self::$_instances [$this->_name] = & $this;
		self::$_structure [$this->_level] [] = $this->_name;
	}
	
	//----------------------------------------------------------------------------
	public static function get($tabname) {
		if (self::$_instances [$tabname])
			return self::$_instances [$tabname];
		else
			return false;
	}
	
	//----------------------------------------------------------------------------
	public function addTab($tabname, $url, $label) {
		new RbView_Tab ( $tabname, $url, $label, $this );
		return $this;
	} //End of method
	
	//----------------------------------------------------------------------------
	public function activate() {
		if ($this->_father) {
			$this->_father->setUrl ( $this->_url );
			$this->_father->activate ();
			$_SESSION ['tabs'] 
					  [$this->_father->getName ()] 
					  ['activePage'] = $this->_url;
		}
		$this->_isActive = true;
		return $this;
	}
	
	//----------------------------------------------------------------------------
	public function getLevel() {
		return $this->_level;
	}
	
	//----------------------------------------------------------------------------
	public function getName() {
		return $this->_name;
	}
	
	//----------------------------------------------------------------------------
	public function getUrl() {
		return $this->_url;
	}
	
	//----------------------------------------------------------------------------
	public function setUrl($url) {
		$this->_url = $url;
		return $this;
	}
	
	//----------------------------------------------------------------------------
	public function getLabel() {
		return $this->_label;
	}
	
	//----------------------------------------------------------------------------
	public function isActive() {
		return $this->_isActive;
	}
	
	//----------------------------------------------------------------------------
	public static function toHtml() {
		$html = '';
		foreach ( self::$_structure as $level => $tabsname ) {
			$html .= '<ul id="tabnav">';
			foreach ( $tabsname as $tabname ) {
				$class = '';
				$tab = RbView_Tab::get ( $tabname );
				if ($tab->isActive ())
					$class = 'active';
				$html .= '<li class="' . $class 
					  . '"><a href="' 
					  . $tab->getUrl () 
					  . '">' 
					  . $tab->getLabel () 
					  . '</a></li>';
			}
			$html .= '</ul>';
		}
		return $html;
	} //End of method

} //End of class

