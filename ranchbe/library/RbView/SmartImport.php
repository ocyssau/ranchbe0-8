
<?php
class RbView_SmartImport extends RbView_SmartStore{

	//----------------------------------------------------------
	function __construct($space_name){
		$this->space_name = $space_name;
		$this->space =& Rb_Space::get($this->space_name);
	}//End of method

	//-----------------------------------------------------------------
	//Genere une ligne de formulaire dans le cas du store multiple
	//Le principe est de renommer chaque champ du formulaire (y compris les champs issues des metadonnees crees par les utilisateurs)
	//et d'y ajouter '[$i]' ou $i est un numero incrementale issue de la boucle de parcours de $_REQUEST['checked'].
	//Le formulaire retourne alors des variables tableaux. La description[1] correspondra au fichier[1] etc.
	public function setForm($loop_id, $docaction=false, $validate=false, $request=array(), $isFrozen = false ){
		$this->form = new RbView_Smarty_Form_Sub('form_'.$loop_id, 'post');
		$this->isFrozen = $isFrozen;

		/*
		 there is a serious bug with quickform : the submit value are reused as default value for form field.
		 in this case file_name is submit and its a array where key are not synchro with $loop_id. so when create form
		 for loop_id = 1 the file_name default value is set from submit value file_name[1] wich is not the file_name for current loop
		 AND (its the real bug)
		 the call to setDefaults is ignored, so its impossible to force the default value to other things that the submit value.
		 Solutions :
		 $this->form->_submitValues = array();
		 OR
		 be carfull to always synchronize submit values with the form fields
		 OR
		 dont use integer for loop_id
		 */

		//$this->form->_submitValues = array_merge($this->form->_submitValues, $request);
		//var_dump($this->form->_submitValues);

		$defaultRender =& $this->form->defaultRenderer();
		$defaultRender->setFormTemplate($this->subformTemplate);
		$defaultRender->setHeaderTemplate($this->subheaderTemplate);
		$defaultRender->setElementTemplate($this->subelementTemplate);

		$this->form->_requiredNote = '';
		$this->loop_id = $loop_id;

		if( count($this->actions) == 1 ){
			if ( isset($this->actions['ignore']) ){
				$disabled = 'DISABLED';
				$readonly = 'READONLY';
			}
		}

		$mask = Ranchbe::getConfig()->document->mask;
		if($this->file_name){
			$this->form->addElement('text', 'file_name['.$loop_id.']', tra('File') ,
			array('readonly', 'size'=>32));
			$this->form->addRule('file_name['.$loop_id.']',
	                  'This file name is not valid', 'regex', "/$mask/" , 'server');
		}

		$this->form->addElement('text', 'document_number['.$loop_id.']', tra('document_number'),
		array('size'=>32) );

		$this->form->addRule('document_number['.$loop_id.']',
	              'This document name is not valid', 'regex', "/$mask/" , 'server');

		$this->form->addElement('hidden', 'loop_id['.$loop_id.']', $loop_id);

		$description = $request['description'];
		if( !$description )
		$description = 'undefined';

		$this->form->setDefaults(array(
	      'document_number['.$loop_id.']' => $request['document_number'],
	      'description['.$loop_id.']' => $description,
	      'category_id['.$loop_id.']' => $request['category_id'],
	      'file_name['.$loop_id.']' => $this->file_name
		));

		//Add select category
		$params = array(
				'is_multiple' => false,
				'property_name' => 'category_id['.$loop_id.']',
				'property_description' => tra('Category').' <br /><a href="#" onClick="javascript:setValue(\'category_id['.$loop_id.']\');
				getElementById(\'step\').value=\'refresh\';
				getElementById(\'form\').submit();
				return false;">'.tra('same for all').'</a>',
				'property_length' => '1',
				'return_name' => false,
				'default_value' => $this->document->getProperty('category_id'),
				'disabled' => $disabled,
				'onChange'=>'javascript:getElementById(\'step\').value=\'refresh\';getElementById(\'form\').submit();return false;'
				);
	construct_select_category($params , $this->form , $this->document->getFather(), 'server');

	//Add select document indice
	$params = array(
		'is_multiple' => false,
		'property_name' => 'document_version['.$loop_id.']',
		'property_description' => tra('document_version').' <br /><a href="#" onClick="javascript:setValue(\'document_version['.$loop_id.']\');return false;">'.tra('same for all').'</a>',
		'property_length' => '1',
		'is_required' => true,
		'default_value' => $request['document_version'],
		'disabled' => $disabled,
	);
	construct_select_indice($params, $this->form, 'server');

	/*
	 if( array_key_exists( 'upgrade_doc', $this->actions ) || array_key_exists( 'create_doc', $this->actions ) ){
	 construct_select_indice($params , $this->form);
	 }else{
	 $this->form->addElement('select', $params['property_name'], $params['property_description'], array(), array($disabled) );
	 }
	 */

	$label = tra('description').' <br />
	<a href="#" onClick="javascript:setValue(\'description['.$loop_id.']\');return false;">'.tra('same for all').'</a>';
	$this->form->addElement('textarea', 'description['.$loop_id.']' , $label ,
	array('rows'=>2, 'cols'=>32, $disabled, $readonly, 'id'=>'description['.$loop_id.']') );
	$this->form->addRule('description['.$loop_id.']', tra('is required'), 'required');

	//Get fields for custom metadata
	$optionalFields = array();
	if( $this->document->getProperty('category_id') ){
		$categoryMetadataLink = new Rb_Category_Relation_Metadata();
		$optionalFields = $categoryMetadataLink->getMetadatas( $request['category_id'] );
	}else{
		//--Get the metadatas from the container links
		$optionalFields =Rb_Container_Relation_Docmetadata::get()
							->getMetadatas( $this->document->getFather()->getId() );
	}

	if(is_array($optionalFields))
	foreach($optionalFields as $field){
		$field['default_value'] = $request[$field['property_name']];
		$this->fields[] = $field['property_name'];
		$field['property_name'] = $field['property_name'] . '['.$loop_id.']';
		$field['disabled'] = $disabled;
		if($field['property_type'] != 'date' && $field['property_type'] != 'liveSearch') //the js script to copy properties dont operate with date and livesearch fields
		$field['property_description'] = $field['property_description'].' <br /><a href="#" onClick="javascript:setValue(\''.$field['property_name'].'\');return false;">'.tra('same for all').'</a>';
		construct_element($field , $this->form, 'server');
	}

	//Create hidden field to retrieve initial action list
	foreach($this->actions as $action=>$description){
		$this->form->addElement('hidden', 'actions['.$loop_id.']['.$action.']', $description);
	}

	if( empty($docaction) ){ //To set the default value of select
		$tmp = array_keys($this->actions);
		$docaction = $tmp[0];
		unset($tmp);
	}

	//Add select action to perform
	if( is_array(tra('action_definition')) )
	$this->action_definition = tra('action_definition'); //Get array from traduction file if traduction exists. Be careful to order of actions in this array. The first element fund is used as default value.
	if( is_array($this->actions) )
	$this->actions = array_intersect_key($this->action_definition , $this->actions);

	if($docaction)
	$this->default_action = $docaction;

	$params = array(
	'is_multiple' => false,
	'property_name' => 'docaction['.$loop_id.']',
	'property_description' => tra('Action'),
	'property_length' => '1',
	'return_name' => false,
	'default_value' => $this->default_action, //Default value is the first key of the action array
	'disabled' => $disabled,
	);
	construct_select($this->actions , $params , $this->form);

	//Apply new filters to the element values
	$this->form->applyFilter('__ALL__', 'trim');

	foreach($this->fields as $property_name){
		$this->form->addElement('hidden', 'fields['.$loop_id.'][]', $property_name);
	}

	if($validate){
		if($docaction != 'create_doc')
		unset($this->fields['document_version']); //prevent change of indice by this way
		$this->_initDocumentProperties($this->fields, $this->loop_id, $request);
		$this->_validateForm($docaction);
	}

	$this->_errorsToElement();
	$this->_feedbackToElement();

	return $this->form;

	} //End of function

	//-----------------------------------------------------------------
	//---------- Set document properties from form input
	/*overload this method because properties are directly send in no assoc array whereas its a associative array in parent
	*/
	protected function _initDocumentProperties($properties_name, $loop_id, $request=array() ){
		foreach($properties_name as $property_name){
			$val = $request[$property_name];
			$val = $this->_normalizeProperty($property_name,
			$val,
			$this->form);
			$this->document->setProperty($property_name, $val);
		}
	} //End of function

} //End of class
