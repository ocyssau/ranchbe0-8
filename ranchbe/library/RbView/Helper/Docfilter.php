<?php

/**
 * @see Zend_Controller_Action_Helper_Abstract
 */

class RbView_Helper_Docfilter extends RbView_Helper_Filter{

	protected $usr; /*!< Detailed description after the member */
	protected $need_with = array(); /*!< Some request needs to define tables joins, set this requests here */


  //------------------------------------------------------------------------------
  /*! \brief
  */
  public function init(){
    $this->setTemplate('document/searchBar.tpl');
    $this->options_def = array_merge($this->options_def, array(
      'sort_field'=>'document_number',
      'sort_order'=>'ASC',
      'displayHistory'=>0,
      'onlyMy'=>0,
      'checkByMe'=>0,
      'displayThumbs'=>0,
      'find_document_number'=>'',
      'find_document_description'=>'',
      'find_doctype'=>'',
      'find_document_access_code'=>'',
      'find_document_state'=>'',
      'find_category'=>'',
      'f_adv_search_cb'=>0, //checkbox to activate advanced search
      'f_action_user_name'=>array(),
      'f_action_field'=>array(),
      'f_dateAndTime_cb'=>0, //checkbox to activate date and time search
      'f_check_out_date'=>'', //int timestamp
      'f_check_out_date_sel'=>0, //checkbox to activate this option
      'f_check_out_date_cond'=>'', // > or <
      'f_update_date'=>'', //int timestamp
      'f_update_date_sel'=>0,
      'f_update_date_cond'=>'', // > or <
      'f_open_date'=>'', //int timestamp
      'f_open_date_sel'=>0,
      'f_open_date_cond'=>'', // > or <
      //'f_close_date'=>'', //int timestamp
      //'f_close_date_sel'=>0,
      //'f_close_date_cond'=>'', // > or <
      //'f_fsclose_date'=>'', //int timestamp
      //'f_fsclose_date_sel'=>0,
      //'f_fsclose_date_cond'=>'', // > or <
      )
    );
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  public function fetchForm( &$smarty , Rb_Container &$container){
    $this->smarty =& $smarty;
    $this->smarty->assign_by_ref('filter_options',$this->options);
    $this->smarty->assign('date_elements', array('check_out_date','update_date','open_date') ); //Defined element for select date search
    $this->smarty->assign('user_elements', array('check_out_by','update_by','open_by') ); //Defined element for select action search
    $this->smarty->assign('SPACE_NAME', $container->getSpace()->getName()); //Defined element for select action search
    
    //Get list for generate category select
    $categories = Rb_Container_Relation_Category::get()
                      ->getCategories( $container->getId(),
                                       array('sort_field'=>'category_number',
                                             'sort_order'=>'ASC',
                                             'select'=>array('category_id',
                                                             'category_number'
                                             ),
                                            )
                                     );
    $catsel = array();
    foreach ($categories as $value ){ //Rewrite result array for quickform convenance
      $selectedCat[$value['category_id']] = $value['category_number'];
      $catsel[] = (int) $value['category_id']; //to inject in select of metadatas on next...
    }
    $this->smarty->assign('category_list', $selectedCat);

    //Try to get metadata from categories linked to container
    //  get too categories linked to container
    $catsel[] = $container->getId();

    $optionalFields = Rb_Category_Relation_Metadata::get()
                          ->getMetadatas($catsel,
                          array('sort_field'=>'property_name',
                                'sort_order'=>'ASC',
                                'select'=>array('property_id',
                                                'property_name',
                                                'property_fieldname',
                                                'property_description',
                                                'property_type'
                                                )
                               )
                          );

    if(is_array($optionalFields)){
      $this->smarty->assign('optionalFields',$optionalFields);
      foreach($optionalFields as $val)
        $all_field[0][$val['property_fieldname']] = $val['property_name'];
      $this->smarty->assign('find_elements', $all_field[0]);
    }

    Ranchbe::getSmarty()->assign_by_ref('user_list', 
                                        Ranchbe::getAuthAdapter()->getUsers() );

    return $this->smarty->fetch($this->template);
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_displayHistory(){
    return;
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_onlyMy(){
    if($this->options['onlyMy'] == 1){
      $where = '`check_out_by` = '. Rb_User::getCurrentUser()->getId() .' OR '.
      '`update_by` = '. Rb_User::getCurrentUser()->getId() .' OR '.
      '`open_by` = '. Rb_User::getCurrentUser()->getId()
      ;
      $this->search->setWhere( $where );
    }
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_checkByMe(){
    if($this->options['checkByMe'] == 1){
      $this->search->setFind( Rb_User::getCurrentUser()->getId(), 'check_out_by', 'exact');
    }
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_displayThumbs(){
    return;
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_find_document_number(){
    if( !empty($this->options['find_document_number']) ){
      $this->search->setFind( $this->options['find_document_number'], 'document_number', 'anywhere');
    }
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_find_document_description(){
    if( !empty($this->options['find_document_description']) ){
      $this->search->setFind( $this->options['find_document_description'], 'description', 'anywhere');
    }
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_find_doctype(){
    if( !empty($this->options['find_doctype']) ){
      $this->search->setWith( 'doctypes', 'doctype_id', 'INNER' );
      $this->search->setFind( $this->options['find_doctype'], 'doctype_number', 'anywhere', 'doctypes' );
    }
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_find_document_access_code(){
    if( !empty($this->options['find_document_access_code']) ){
      if ($this->options['find_document_access_code'] == 'free') //because 0 value create problem!!
        $this->search->setFind( 0, 'document_access_code', 'exact' );
      else
        $this->search->setFind( $this->options['find_document_access_code'], 'document_access_code', 'exact' );
    }
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_find_document_state(){
    if(!empty( $this->options['find_document_state'] )){
      $this->search->setFind( $this->options['find_document_state'], 'document_state', 'anywhere' );
    }
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_find_category(){
    if(!empty( $this->options['find_category'] )){
      $this->search->setFind( $this->options['find_category'], 'category_id', 'exact' );
    }
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_find(){
    if($this->options['f_adv_search_cb'] != 1) return;
    if( !empty($this->options['find']) && !empty($this->options['find_field']) )
      $this->search->setFind( $this->options['find'],
                              $this->options['find_field'],
                              'anywhere',
                              Rb_Object_Extend::getExtendTable($this->search->getDao())
                            );
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_f_action_user_name(){
    if($this->options['f_adv_search_cb'] != 1) return;
    if( is_array($this->options['f_action_user_name']) && count($this->options['f_action_user_name']) > 0 && !empty($this->options['f_action_field']) )
      foreach($this->options['f_action_user_name'] as $username){
        $this->search->setFind( $username, $this->options['f_action_field'], 'anywhere' );
      }
    else if( !empty($this->options['f_action_user_name']) && !empty($this->options['f_action_field']) ){
      $this->search->setFind( $this->options['f_action_user_name'], $this->options['f_action_field'], 'anywhere' );
    }
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_f_check_out_date(){
    if($this->options['f_adv_search_cb'] != 1 || $this->options['f_dateAndTime_cb'] != 1) return;
    if($this->options['f_check_out_date_sel'] != 1) return;
    if( !empty($this->options['f_check_out_date']) )
      $this->search->setFind( $this->options['f_check_out_date'], 'check_out_date', 'exact' );
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_f_update_date(){
    if($this->options['f_adv_search_cb'] != 1 || $this->options['f_dateAndTime_cb'] != 1) return;
    if($this->options['f_update_date_sel'] != 1) return;
    if( !empty($this->options['f_update_date']) ){
      $this->search->setWhere( 'update_date '.$this->options['f_check_out_date_cond'].' '.$this->options['f_update_date']);
    }
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_f_open_date(){
    if($this->options['f_adv_search_cb'] != 1 || $this->options['f_dateAndTime_cb'] != 1) return;
    if($this->options['f_open_date_sel'] != 1) return;
    if( !empty($this->options['f_open_date']) )
      $this->search->setFind( $this->options['f_open_date'], 'open_date', 'exact' );
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_f_close_date(){
    if($this->options['f_adv_search_cb'] != 1 || $this->options['f_dateAndTime_cb'] != 1) return;
    if($this->options['f_close_date_sel'] != 1) return;
    if( !empty($this->options['f_close_date']) )
      $this->search->setFind( $this->options['f_close_date'], 'close_date', 'exact' );
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_f_fsclose_date(){
    if($this->options['f_adv_search_cb'] != 1 || $this->options['f_dateAndTime_cb'] != 1) return;
    if($this->options['f_fsclose_date_sel'] != 1) return;
    if( !empty($this->options['f_fsclose_date']) )
      $this->search->setFind( $this->options['f_fsclose_date'], 'forseen_close_date', 'exact' );
  } //End of method

} //End of class
