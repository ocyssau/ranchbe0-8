<?php

/**
 * @see Zend_Controller_Action_Helper_Abstract
 */
  
class RbView_Helper_Filefilter extends RbView_Helper_Filter{
  
  //------------------------------------------------------------------------------
  /*! \brief
  */
  public function init(){
    $this->setTemplate('file/searchBar.tpl');
    $this->options_def = array(
        'find_file_name'=>'',
        'find_file_type'=>'',
        'displayMd5'=>false,
        'sort_field'=>'file_name',
        'sort_order'=>'ASC',
        'exact_find'=>'',
        'exact_find_field'=>array(), //name of db field where search values of find
    );
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_find_file_name(){
    if( !empty($this->options['find_file_name']) )
      $this->search->setFind( $this->options['find_file_name'], 'file_name', 'anywhere' );
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_find_file_type(){
    if( !empty($this->options['find_file_type']) )
      $this->search->setFind( $this->options['find_file_type'], 'file_extension', 'anywhere' );
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_displayMd5(){
    if( !empty($this->options['displayMd5']) )
      $this->search->set('displayMd5', $this->options['displayMd5']);
  } //End of method

} //End of class
