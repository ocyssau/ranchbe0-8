<?php

/**
 * @see Zend_Controller_Action_Helper_Abstract
 */

class RbView_Helper_Histofilter extends RbView_Helper_Filter{

	//------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/RbView/Helper/RbView_Helper_Filter#init()
	 */
	public function init(){
		$this->setTemplate('history/searchBar.tpl');
		$this->options_def = array_merge($this->options_def, array(
										      'sort_field'=>'histo_order',
										      'sort_order'=>'DESC',
											));
	} //End of method

	//------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/RbView/Helper/RbView_Helper_Filter#fetchForm($smarty)
	 */
	public function fetchForm( &$smarty ){
		$this->smarty =& $smarty;
		$this->smarty->assign_by_ref('filter_options',$this->options);
		$this->smarty->assign('find_elements', $this->find_elements);
		$this->smarty->assign('formurl', $this->formurl);
		return $this->smarty->fetch($this->template);
	} //End of method

	//------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/RbView/Helper/RbView_Helper_Filter#finish_find()
	 */
	protected function finish_find(){
		if( !empty($this->options['find']) && !empty($this->options['find_field']) )
		$this->search->setFind( $this->options['find'], $this->options['find_field'], 'anywhere' );
		if( empty($this->options['find']) )
		unset($this->options['find_field']);
	} //End of method

} //End of class
