<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

class RbView_Helper_Pagination{
	protected $_filter; /*!< object GUI_helper_filter */
	protected $_count; /*!<  */
	protected $_numrows; /*!<  */
	protected $_prev_offset; /*!<  */
	protected $_next_offset; /*!<  */
	protected $_actual_page=1; /*!<  */
	protected $_cant_pages=1; /*!<  */
	protected $_sameurl=false;
	protected $_template='pagination.tpl';

	//------------------------------------------------------------------------------
	/*! \brief Brief description.
	 *         Brief description continued.
	 *
	 *  Detailed description starts here.
	 \param var1 (integer)
	 */
	public function __construct(RbView_Helper_Filter &$filter, $template=false){
		$this->_filter =& $filter;
		if($template){
			$this->setTemplate( $template );
		}else{
			$this->setTemplate( $this->_template );
		}
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	 */
	public function fetchForm( &$smarty ){
		$smarty->assign('sameurl',$this->_sameurl);
		$smarty->assign('offset',$this->_offset);
		$smarty->assign('prev_offset',$this->_prev_offset);
		$smarty->assign('next_offset',$this->_next_offset);
		$smarty->assign('actual_page',$this->_actual_page);
		$smarty->assign('cant_pages', $this->_cant_pages);
		$smarty->assign('numrows',$this->_numrows);
		$smarty->assign('prev_pages', range( 1 , $this->_actual_page , 1 ) );
		return $smarty->fetch($this->_template);
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	 */
	public function setTemplate( $template ){
		$path = & Ranchbe::getConfig ()->resources->smarty->scriptPath;
		$this->_template = $path . $template;
		return $this->_template;
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	 */
	public function getTemplate(){
		return $this->_template;
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	 */
	public function setPagination($count){
		$this->_count = $count;
		$this->_numrows = $this->_filter->getOption('numrows');
		$this->_offset = $this->_filter->getOption('offset');
		if($this->_numrows != 0){
			//Process the pagination

			$this->_cant_pages = ($this->_count / $this->_numrows); //Number of find line / Number of max rows to display
			$this->_actual_page = 1 + ($this->_offset / $this->_numrows); //Actuall page number

			if ($this->_cant_pages == 1) {
				$this->_next_offset = $this->_offset + $this->_numrows; //if cant_page is = 1 we supppose that there are other items
			}
			if ($this->_offset > 0) {
				$this->_prev_offset = $this->_offset - $this->_numrows; //if offset is > 0 we suppose that they are previous page
			} else {
				$this->_prev_offset = -1;
			}

			if ( !is_int(1 + ($this->_offset / $this->_numrows) )) { //if the page number is a decimal, there is probably a problem...
				$this->_actual_page = 'pagination error';
				$this->_offset = 0;
				$this->_next_offset = $this->_numrows;
				$this->_prev_offset = 0;
			}
		}
		return $this;
	} //End of method
	
	//------------------------------------------------------------------------------
	/*! \brief
	 */
	public function setRequest(Zend_Controller_Request_Abstract &$request){
		$this->_sameurl = 	$request->getModuleName().'/'.
							$request->getControllerName().'/'.
							$request->getActionName();
	} //End of method
	
	
} //End of class
