<?php

/**
 * @see Zend_Controller_Action_Helper_Abstract
 */

class RbView_Helper_Containerfilter extends RbView_Helper_Filter{

	protected $usr; /*!< Detailed description after the member */

	//------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/RbView/Helper/RbView_Helper_Filter#init()
	 */
	public function init(){
		$this->setTemplate('container/searchBar.tpl');
		$this->options_def = array_merge($this->options_def, array(
      'sort_field'=>'',
      'sort_order'=>'',
      'f_adv_search_cb'=>0, //checkbox to activate advanced search
      'find_number'=>'',
      'find_description'=>'',
      'find_project'=>'',
      'f_action_user_name'=>array(),
      'f_action_field'=>array(),
      'f_dateAndTime_cb'=>0, //checkbox to activate date and time search
      'f_open_date'=>'', //int timestamp
      'f_open_date_sel'=>0,
      'f_open_date_cond'=>'', // > or <
      'f_fsclose_date'=>'', //int timestamp
      'f_fsclose_date_sel'=>0,
      'f_fsclose_date_cond'=>'', // > or <
		//'f_close_date'=>'', //int timestamp
		//'f_close_date_sel'=>0,
		//'f_close_date_cond'=>'', // > or <
		)
		);
	} //End of method

	//------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/RbView/Helper/RbView_Helper_Filter#fetchForm($smarty)
	 */
	public function fetchForm( &$smarty , Rb_Container &$container){
		$this->smarty =& $smarty;
		$this->smarty->assign_by_ref('filter_options',$this->options);
		$this->smarty->assign('date_elements', array('open_date','fs_close_date') ); //Defined element for select date search
		$this->smarty->assign('user_elements', array('open_by') ); //Defined element for select action search

		$optionalFields = $container->getMetadatas();
		$this->smarty->assign('optionalFields', $optionalFields);
		if(is_array($optionalFields))
		foreach($optionalFields as $val){
			$find_elements[$val['property_fieldname']] = $val['property_name'];
			$params['select'][] = $val['property_name'];
		}

		//Define select option for "find"
		$this->smarty->assign( 'find_elements', $find_elements );

		//Set list of user for action fields
		$this->smarty->assign_by_ref('user_list',
		Ranchbe::getAuthAdapter()->getUsers() );

		//Set list of projects
		$this->smarty->assign_by_ref('project_list',
		Rb_Project::get()->getAll() );

		$this->smarty->assign( 'formurl', $this->formurl );
		return $this->smarty->fetch($this->template);
	} //End of method

	//------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/RbView/Helper/RbView_Helper_Filter#finish_find()
	 */
	protected function finish_find(){
		if($this->options['f_adv_search_cb'] != 1) return;
		//parent::finish_find();
		if( !empty($this->options['find']) && !empty($this->options['find_field']) )
		$this->search->setFind( $this->options['find'],
								$this->options['find_field'],
                              	'anywhere',
					Rb_Object_Extend::getExtendTable($this->search->getDao())
					);
	} //End of method

	//------------------------------------------------------------------------------
	/**
	 * 
	 * @return void
	 */
	protected function finish_find_project(){
		if( !empty($this->options['find_project']) ){
			$this->search->setFind( $this->options['find_project'], 'project_id', 'exact' );
		}
	} //End of method

	//------------------------------------------------------------------------------
	/**
	 * 
	 * @return void
	 */
	protected function finish_f_action_user_name(){
		if($this->options['f_adv_search_cb'] != 1) return;
		if( is_array($this->options['f_action_user_name']) && count($this->options['f_action_user_name']) > 0 && !empty($this->options['f_action_field']) )
		foreach($this->options['f_action_user_name'] as $username){
			$this->search->setFind( $username, $this->options['f_action_field'], 'anywhere' );
		}
		else if( !empty($this->options['f_action_user_name']) && !empty($this->options['f_action_field']) ){
			$this->search->setFind( $this->options['f_action_user_name'], $this->options['f_action_field'], 'anywhere' );
		}
		if( empty($this->options['f_action_user_name']) )
		unset($this->options['f_action_field']);
	} //End of method

	//------------------------------------------------------------------------------
	/**
	 * 
	 * @return void
	 */
	protected function finish_f_open_date(){
		if($this->options['f_adv_search_cb'] != 1 || $this->options['f_dateAndTime_cb'] != 1) return;
		if($this->options['f_open_date_sel'] != 1) return;
		if( !empty($this->options['f_open_date']) )
		$this->search->setFind( $this->options['f_open_date'], 'open_date', 'exact' );
	} //End of method

	//------------------------------------------------------------------------------
	/**
	 * 
	 * @return void
	 */
	protected function finish_fs_close_date(){
		if($this->options['f_adv_search_cb'] != 1 || $this->options['f_dateAndTime_cb'] != 1) return;
		if($this->options['f_close_date_sel'] != 1) return;
		if( !empty($this->options['f_close_date']) )
		$this->search->setFind( $this->options['f_close_date'], 'close_date', 'exact' );
	} //End of method

} //End of class
