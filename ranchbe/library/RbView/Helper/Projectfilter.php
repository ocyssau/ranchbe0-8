<?php

/**
 * @see Zend_Controller_Action_Helper_Abstract
 */
  
  class RbView_Helper_Projectfilter extends RbView_Helper_Filter{
  
  protected $template = 'projectManager/searchBar.tpl'; /*!< path to template file */

  //------------------------------------------------------------------------------
  /*! \brief
  */
  public function init(){
    $this->options_def = array_merge($this->options_def, array(
      'f_adv_search_cb'=>0, //checkbox to activate advanced search
      'f_action_user_name'=>array(),
      'f_action_field'=>array(),
      'f_dateAndTime_cb'=>0, //checkbox to activate date and time search
      'f_open_date'=>'', //int timestamp
      'f_open_date_sel'=>0,
      'f_open_date_cond'=>'', // > or <
      'f_fsclose_date'=>'', //int timestamp
      'f_fsclose_date_sel'=>0,
      'f_fsclose_date_cond'=>'', // > or <
      //'f_close_date'=>'', //int timestamp
      //'f_close_date_sel'=>0,
      //'f_close_date_cond'=>'', // > or <
      )
    );
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_find(){
    if($this->options['f_adv_search_cb'] != 1) return;
    parent::finish_find();
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_f_action_user_name(){
    if($this->options['f_adv_search_cb'] != 1) return;
    if( is_array($this->options['f_action_user_name']) && count($this->options['f_action_user_name']) > 0 && !empty($this->options['f_action_field']) )
      foreach($this->options['f_action_user_name'] as $username){
        $this->search->setFind( $username, $this->options['f_action_field'], 'anywhere' );
      }
    else if( !empty($this->options['f_action_user_name']) && !empty($this->options['f_action_field']) ){
      $this->search->setFind( $this->options['f_action_user_name'], $this->options['f_action_field'], 'anywhere' );
    }
    if( empty($this->options['f_action_user_name']) )
      unset($this->options['f_action_field']);
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_f_open_date(){
    if($this->options['f_adv_search_cb'] != 1 || $this->options['f_dateAndTime_cb'] != 1) return;
    if($this->options['f_open_date_sel'] != 1) return;
    if( !empty($this->options['f_open_date']) )
      $this->search->setFind( $this->options['f_open_date'], 'open_date', 'exact' );
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_fs_close_date(){
    if($this->options['f_adv_search_cb'] != 1 || $this->options['f_dateAndTime_cb'] != 1) return;
    if($this->options['f_close_date_sel'] != 1) return;
    if( !empty($this->options['f_close_date']) )
      $this->search->setFind( $this->options['f_close_date'], 'close_date', 'exact' );
  } //End of method

} //End of class
