<?php

/**
 * @see Zend_Controller_Action_Helper_Abstract
 */

class RbView_Helper_GalaxiaAdminProcessFilter extends RbView_Helper_Filter{

  protected $wheres = array();
 
  /*define name of user input fields*/
  protected $options_def = array(
        'numrows'=>50,
        'offset'=>0,
        'sort_field'=>'',
        'sort_order'=>'',
        'find'=>'',
        'filter_name'=>'',
        'filter_active'=>'',
  );

  //------------------------------------------------------------------------------
  /*! \brief
  */
  public function init(){
    $this->setTemplate('galaxia/searchBar.tpl');
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief post treatment to send values to objects
  */
  protected function filter_find(){
    if( !empty($this->options['filter_find']) )
    	$find = $this->options['filter_find'];
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief post treatment to send values to objects
  */
  protected function filter_name(){
    if( !empty($this->options['filter_name']) )
  		$this->wheres[] = " name='" . $this->options['filter_name'] . "'";
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief post treatment to send values to objects
  */
  protected function filter_active(){
    if( !empty($this->options['filter_active']) )
  		$this->wheres[] = " isActive='" . $this->options['filter_active'] . "'";
  } //End of method

} //End of class
