<?php

/**
 * @see Zend_Controller_Action_Helper_Abstract
 */
//require_once 'Zend/Controller/Action/Helper/Abstract.php';


class RbView_Helper_Filter {

	protected $error_stack; /*!< PEAR_ErrorStack error manager */
	protected $page_id; /*!< string identify the session space name */
	protected $session; /*!< object Zend_Session_Namespace */
	protected $request; /*!< object zend_controller_request */
	protected $search; /*!< object class/search */
	protected $options; /*!< array of options*/
	protected $smarty; /*!< object smarty */
	protected $template = 'default/searchBar.tpl'; /*!< path to template file */
	protected $find_elements = array (); /*!< element to display in the select find_field options */
	protected $date_elements = array (); /*!< element to display in the select find_field options */

	/*define name of user input fields*/
	protected $options_def = array ('numrows' => 50, 
									'offset' => 0, 
									'sort_field' => '', 
									'sort_order' => '', 
									'find' => '', 
									'find_field' => array (), //name of db field where search values of find
									'exact_find' => '', 'exact_find_field' => array (), //name of db field where search values of find
									'find_number' => '', 'find_description' => '' );

	protected $need_with = array (); /*!< Some request needs to define tables joins, set this requests here */

	//------------------------------------------------------------------------------
	/**
	 * 
	 * @param Zend_Controller_Request_Abstract $request
	 * @param Rb_Search_Interface $search
	 * @param string $page_id
	 * @param string $formurl
	 * @return void
	 */
	public function __construct(Zend_Controller_Request_Abstract &$request,
								Rb_Search_Interface &$search,
								$page_id = 'default', $formurl = '#') {
		$this->page_id = $page_id . '_filters';
		$this->session = new Zend_Session_Namespace ( $this->page_id );
		$this->request = & $request;
		$this->search = & $search;
		$this->setUrl ( $formurl );
		$this->init ();
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Return search object
	 * 
	 * @return Rb_Search_Interface
	 */
	public function &getSearch() {
		return $this->search;
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Return the parameters in array to passthru to basic::getQueryOption method
	 * 
	 * @return array
	 */
	public function getParams() {
		return $this->search->getParams ();
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Init the object. call by constructor to init the childs
	 * 
	 * @return void
	 */
	public function init() {
		$this->setTemplate ( $this->template );
		return;
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Generate html code of the form from template
	 * 
	 * @param Smarty $smarty
	 * @return string
	 */
	public function fetchForm(Smarty &$smarty) {
		$this->smarty = & $smarty;
		$this->smarty->assign ( 'date_elements', $this->date_elements ); //Defined element for select date search
		$this->smarty->assign ( 'find_elements', $this->find_elements ); //Defined element for select action search
		$this->smarty->assign ( 'user_elements', $this->user_elements ); //Defined element for select action search
		$this->smarty->assign_by_ref ( 'filter_options', $this->options );
		if ($this->user_elements) {
			Ranchbe::getSmarty ()->assign_by_ref ( 'user_list', Ranchbe::getAuthAdapter ()->getUsers ( array ('sort_field' => 'handle', 'sort_order' => 'ASC', 'offset' => 0, 'limit' => 9999 ) ) );
		}
		return $this->smarty->fetch ( $this->template );
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Init the template file to use
	 * 
	 * @param string $template 	name of template file from base path config parameter resources.smarty.scriptPath
	 * @return string			full path to template
	 */
	public function setTemplate($template) {
		$path = & Ranchbe::getConfig ()->resources->smarty->scriptPath;
		$this->template = $path . $template;
		return $this->template;
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Return the template file
	 * 
	 * @return string
	 */
	public function getTemplate() {
		return $this->template;
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Destroy session var to reset filter
	 * 
	 * @return void
	 */
	public function reset() {
		$this->session->unsetAll ();
		$this->request->setParams ( $this->options_def );
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Set all sql options
	 * 
	 * @return array
	 */
	protected function setOptions() {
		if ($this->request->getParam ( 'resetf' ))
		$this->reset ();
		$request_params = $this->request->getParams ();
		foreach ( $this->options_def as $option_name => $default ) {
			if ($request_params ['activatefilter']) {
				if ($request_params [$option_name]) {
					$this->_setOption ( $option_name, $request_params [$option_name] );
				} else {
					$this->_setOption ( $option_name, $default );
				}
			} else if ($this->session->$option_name) {
				$this->_setOption ( $option_name, $this->session->$option_name );
			} else
			$this->_setOption ( $option_name, $default );
		}
		$this->setOrder ();
		$this->setPagination ();
		$this->save ();
		return $this->options;
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Init order option from request parameters
	 * 
	 * @return void
	 */
	private function setOrder() {
		if ($this->request->getParam ( 'sort_field' ))
		$this->_setOption ( 'sort_field', $this->request->getParam ( 'sort_field' ) );
		if ($this->request->getParam ( 'sort_order' ))
		$this->_setOption ( 'sort_order', $this->request->getParam ( 'sort_order' ) );
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Init pagination option (offset/numrows) from request parameters
	 * 
	 * @return void
	 */
	public function setPagination() {
		if ($this->request->getParam ( 'offset' ) || $this->request->getParam ( 'offset' ) === '0')
		$this->_setOption ( 'offset', $this->request->getParam ( 'offset' ) );
		if ($this->request->getParam ( 'numrows' ))
		$this->_setOption ( 'numrows', $this->request->getParam ( 'numrows' ) );
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Set where sql option from array
	 * 
	 * @param array $array
	 * @return void
	 */
	public function setFindElements(array $array) {
		$this->find_elements = $array;
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Set date elements from array
	 * 
	 * @param array $array
	 * @return void
	 */
	public function setDateElements(array $array) {
		$this->date_elements = $array;
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Set user elements from array
	 * 
	 * @param array $array
	 * @return void
	 */
		public function setUserElements(array $array) {
		$this->user_elements = $array;
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Get options $name
	 * 
	 * @param string $name
	 * @return mixed
	 */
	public function getOption($name) {
		return $this->options [$name];
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Set option $name with value $val
	 * 
	 * @param string $name
	 * @param mixed $val
	 * @return void
	 */
	protected function _setOption($name, $val) {
		$this->options [$name] = $val;
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Set url for form action
	 * 
	 * @param string $formurl
	 * @return void
	 */
	public function setUrl($formurl) {
		$this->formurl = $formurl;
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Set default options values
	 * 
	 * @param string $name
	 * @param mixed $val
	 * @return void
	 */
	public function setDefault($name, $val) {
		$this->options_def [$name] = $val;
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * 
	 * @param Rb_Dao_Abstract $dao
	 * @return void
	 */
	public function setWithExtends(Rb_Dao_Abstract $dao) {
		$this->search->setWith ( Rb_Object_Extend::getExtendTable ( $dao ), 
									array (
										'col1' => $dao->getKey ( 'primary' ), 
										'col2' => 'extend_id' ), 
										'LEFT OUTER' );
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Save option in session
	 * 
	 * @return bool
	 */
	protected function save() {
		foreach ( $this->options as $option_name => $val ) {
			$this->session->$option_name = $val;
		}
		return true;
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * 
	 * @return void
	 */
	public function finish() {
		$this->setOptions ();
		$this->search->setOrder ( $this->options ['sort_field'], $this->options ['sort_order'] );
		$this->search->setLimit ( $this->options ['numrows'] );
		$this->search->setOffset ( $this->options ['offset'] );
		foreach ( $this->options as $option_name => $val ) {
			$method = 'finish_' . $option_name;
			if (method_exists ( $this, $method )) {
				$this->$method ();
			} else {
				$this->finish_element ();
			}
		}
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Post treatment to send values to objects
	 * 
	 * @return void
	 */
	protected function finish_element() {
		return;
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Post treatment to send values to objects
	 * 
	 * @return void
	 */
	protected function finish_find() {
		if (! empty ( $this->options ['find'] ) && ! empty ( $this->options ['find_field'] ))
		$this->search->setFind ( $this->options ['find'], $this->options ['find_field'], 'anywhere' );
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Post treatment to send values to objects
	 * 
	 * @return void
	 */
	protected function finish_exact_find() {
		if (! empty ( $this->options ['exact_find'] ) && ! empty ( $this->options ['exact_find_field'] ))
		$this->search->setFind ( $this->options ['exact_find'], $this->options ['exact_find_field'], 'exact' );
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Post treatment to send values to objects
	 * 
	 * @return void
	 */
	protected function finish_find_number() {
		if (! empty ( $this->options ['find_number'] )) {
			$this->search->setFind ( $this->options ['find_number'], $this->search->getDao ()->getFieldName ( 'number' ), 'anywhere' );
		}
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * Post treatment to send values to objects
	 * 
	 * @return void
	 */
	protected function finish_find_description() {
		if (! empty ( $this->options ['find_description'] )) {
			$this->search->setFind ( $this->options ['find_description'], $this->search->getDao ()->getFieldName ( 'description' ), 'anywhere' );
		}
	} //End of method


} //End of class
