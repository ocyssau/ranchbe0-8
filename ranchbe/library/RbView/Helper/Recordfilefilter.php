<?php

/**
 * @see Zend_Controller_Action_Helper_Abstract
 */

class RbView_Helper_Recordfilefilter extends RbView_Helper_Filter{

	protected $usr; /*!< Detailed description after the member */

	//------------------------------------------------------------------------------
	/*! \brief
	*/
	public function init(){
		$this->options_def = array_merge($this->options_def, array(
      'sort_field'=>'file_name',
      'sort_order'=>'ASC',
      'find_file_name'=>'',
      'find_file_type'=>'',
      'find_import_order'=>'',
      'f_adv_search_cb'=>0, //checkbox to activate advanced search
      'f_action_user_name'=>array(),
      'f_action_field'=>array(),
      'f_dateAndTime_cb'=>0, //checkbox to activate date and time search
      'f_check_out_date'=>'', //int timestamp
      'f_check_out_date_sel'=>0, //checkbox to activate this option
      'f_check_out_date_cond'=>'', // > or <
      'f_update_date'=>'', //int timestamp
      'f_update_date_sel'=>0,
      'f_update_date_cond'=>'', // > or <
      'f_open_date'=>'', //int timestamp
      'f_open_date_sel'=>0,
      'f_open_date_cond'=>'', // > or <
		)
		);
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	*/
	public function fetchForm( &$smarty , Rb_Container &$container){
		$smarty->assign_by_ref('filter_options',$this->options);
		$smarty->assign('date_elements', array('file_open_date','file_update_date') ); //Defined element for select date search
		$smarty->assign('user_elements', array('file_open_by','file_update_by') ); //Defined element for select action search
		$smarty->assign('find_elements',
							array (
					              'file_path' => 'file_path',
					              'file_access_code' => 'file_access_code',
					              'file_state' => 'file_state',
					              'file_iteration' => 'file_iteration',
					              'file_type' => 'file_type',
					              'file_size' => 'file_size',
					              'file_md5' => 'file_md5',
							));

		//Get list of users for generate user select in advanced serachBar
		$smarty->assign_by_ref('user_list',
			Ranchbe::getAuthAdapter()->getUsers(array(
										'sort_field' => 'handle',
										'sort_order' => 'ASC',
										'offset' => 0,
										'limit' => 9999,
			)));
		return $smarty->fetch($this->template);
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	*/
	protected function finish_find_file_name(){
		if( !empty($this->options['find_file_name']) ){
			$this->search->setFind( $this->options['find_file_name'], 'file_root_name', 'anywhere');
		}
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	*/
	protected function finish_find_file_type(){
		if( !empty($this->options['find_file_type']) ){
			$this->search->setFind( $this->options['find_file_type'], 'file_extension', 'anywhere');
		}
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	*/
	protected function finish_find_import_order(){
		if( !empty($this->options['find_import_order']) )
		$this->search->setFind( $this->options['find_import_order'], 'import_order', 'exact' );
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	*/
	protected function finish_find(){
		if($this->options['f_adv_search_cb'] != 1) return;
		if( !empty($this->options['find']) && !empty($this->options['find_field']) )
		$this->search->setFind( $this->options['find'], $this->options['find_field'], 'anywhere' );
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	*/
	protected function finish_f_action_user_name(){
		if($this->options['f_adv_search_cb'] != 1) return;
		if( is_array($this->options['f_action_user_name']) && count($this->options['f_action_user_name']) > 0 && !empty($this->options['f_action_field']) )
		foreach($this->options['f_action_user_name'] as $username){
			$this->search->setFind( $username, $this->options['f_action_field'], 'anywhere' );
		}
		else if( !empty($this->options['f_action_user_name']) && !empty($this->options['f_action_field']) ){
			$this->search->setFind( $this->options['f_action_user_name'], $this->options['f_action_field'], 'anywhere' );
		}
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	*/
	protected function finish_f_update_date(){
		if($this->options['f_adv_search_cb'] != 1 || $this->options['f_dateAndTime_cb'] != 1) return;
		if($this->options['f_update_date_sel'] != 1) return;
		if( !empty($this->options['f_update_date']) ){
			$this->search->setWhere( 'update_date '.$this->options['f_check_out_date_cond'].' '.$this->options['f_update_date']);
		}
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	*/
	protected function finish_f_open_date(){
		if($this->options['f_adv_search_cb'] != 1 || $this->options['f_dateAndTime_cb'] != 1) return;
		if($this->options['f_open_date_sel'] != 1) return;
		if( !empty($this->options['f_open_date']) )
		$this->search->setFind( $this->options['f_open_date'], 'open_date', 'exact' );
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	*/
	protected function finish_f_close_date(){
		if($this->options['f_adv_search_cb'] != 1 || $this->options['f_dateAndTime_cb'] != 1) return;
		if($this->options['f_close_date_sel'] != 1) return;
		if( !empty($this->options['f_close_date']) )
		$this->search->setFind( $this->options['f_close_date'], 'close_date', 'exact' );
	} //End of method

	//------------------------------------------------------------------------------
	/*! \brief
	*/
	protected function finish_f_fsclose_date(){
		if($this->options['f_adv_search_cb'] != 1 || $this->options['f_dateAndTime_cb'] != 1) return;
		if($this->options['f_fsclose_date_sel'] != 1) return;
		if( !empty($this->options['f_fsclose_date']) )
		$this->search->setFind( $this->options['f_fsclose_date'], 'forseen_close_date', 'exact' );
	} //End of method

} //End of class
