<?php

/**
 * @see Zend_Controller_Action_Helper_Abstract
 */
class RbView_Helper_Repositfilter extends RbView_Helper_Filter{

	//------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/RbView/Helper/RbView_Helper_Filter#init()
	 */
	public function init(){
		$this->setTemplate('reposit/searchBar.tpl');
		$this->options_def = array_merge($this->options_def, array(
		      'f_adv_search_cb'=>0, //checkbox to activate advanced search
		      'find_number'=>'',
		      'find_space'=>25,
		));
		$this->find_elements = array(
									'reposit_name',
									'reposit_description',
									'reposit_url',
									);
	} //End of method


	//------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/RbView/Helper/RbView_Helper_Filter#fetchForm($smarty)
	 */
	public function fetchForm( &$smarty , Rb_Reposit &$reposit){
		$this->smarty =& $smarty;
		$this->smarty->assign_by_ref('filter_options',$this->options);

		//Define select option for "find"
		$this->smarty->assign( 'find_elements', $this->find_elements );

		$this->smarty->assign( 'formurl', $this->formurl );
		return $this->smarty->fetch($this->template);
	} //End of method

	
	//------------------------------------------------------------------------------
	/**
	 * Post treatment to send values to objects
	 * 
	 * @return void
	 */
	protected function finish_find_space() {
		if (! empty ( $this->options ['find_space'] )) {
			$this->search->setFind ( $this->options ['find_space'], 'space_id', 'exact' );
		}
	} //End of method
	
	
	//------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/RbView/Helper/RbView_Helper_Filter#finish_find()
	 */
	protected function finish_find(){
		if($this->options['f_adv_search_cb'] != 1) return;
		if( !empty($this->options['find']) && !empty($this->options['find_field']) )
		$this->search->setFind( $this->options['find'],
								$this->options['find_field'],
                              	'anywhere',
			Rb_Object_Extend::getExtendTable($this->search->getDao()
			));
	} //End of method
	
	
} //End of class
