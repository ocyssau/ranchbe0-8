<?php

//Redefine the function array_intersect_key for php<5.1.0
if (!function_exists('array_intersect_key'))
{
  function array_intersect_key($isec, $keys)
  {
    $argc = func_num_args();
    if ($argc > 2)
    {
      for ($i = 1; !empty($isec) && $i < $argc; $i++)
      {
        $arr = func_get_arg($i);
        foreach (array_keys($isec) as $key)
        {
          if (!isset($arr[$key]))
          {
            unset($isec[$key]);
          }
        }
      }
      return $isec;
    }
    else
    {
      $res = array();
      foreach (array_keys($isec) as $key)
      {
        if (isset($keys[$key]))
        {
          $res[$key] = $isec[$key];
        }
      }
      return $res;
    }
  }
}

class RbView_Helper_Changestate{

protected $_feedbacks = array();
protected $_errors = array();

protected $doc_properties = array();
protected $document; //object document
protected $workflow; //object docflow
protected $instance; //object instance
protected $iid; //integer

protected static $_previous=array(); //array()

//public $subformTemplate = "<table class=\"normal\"><tr>\n{hidden}\n{content}\n</tr></table>";
public $subformTemplate = "<tr class=odd>\n{hidden}\n{content}\n</tr>";

public $subheaderTemplate = "<h1>{header}</h1>";

public $subelementTemplate = 
    "<td NOWRAP>{label}
      <!-- BEGIN required -->
          <font color=red>*</font>
      <!-- END required -->
      <!-- BEGIN error -->
          <font color=red><i><b><br />{error}</b></i></font>
      <!-- END error -->
      <br />
      {element}
    </td>";

public $feedbackTemplate = "<font color=green>{feedback}</font><br />";

public $errorTemplate = "<font color=red><b>{error}</b></font><br />";

public $error_stack;

//----------------------------------------------------------
/*
*
*/
function __construct(Rb_Workflow_Docflow &$workflow){
  $this->error_stack =& Ranchbe::getError();
  $this->workflow =& $workflow;
  
  $this->document =& $this->workflow->getDocument();
  //init the instance from the document properties
  $this->instance =& $workflow->initInstance();
  $this->iid = $this->instance->getInstanceId();

  $this->_feedbacks = array();
  $this->_errors = array();
  $this->_isFrozen = false;
}//End of method

//-----------------------------------------------------------------
//Genere une ligne de formulaire dans le cas du store multiple
public function &setForm($loop_id, $docaction=NULL, $validate=false){
  $smarty =& Ranchbe::getSmarty();

  $this->form = new RbView_Smarty_Form_Sub('form_'.$loop_id, 'post');

  $defaultRender =& $this->form->defaultRenderer();
  $defaultRender->setFormTemplate($this->subformTemplate);
  $defaultRender->setHeaderTemplate($this->subheaderTemplate);
  $defaultRender->setElementTemplate($this->subelementTemplate);

  $this->form->_requiredNote = '';
  $this->loop_id = $loop_id;

  if( $this->iid ){ //If a workflow instance is running

    $ins_info = $this->workflow->getInstanceInfos();

    // Get the process from the instance and set information
    $this->workflow->initProcess($ins_info['pId']); //init the process
    $proc_info = $this->workflow->getProcessInfos();

    //Check if process is identical to previous document process : to ensure coherence between documents
    if( self::$_previous['pId'] ){
      if( self::$_previous['pId'] != $proc_info['pId']){
        $msg = 'document %element% is rejected : have not same process that other selected document';
        $e = $this->document->getProperty('document_number');
        $this->_errors[] = sprintf( tra($msg), $e );
      }
    }

    //Get infos about activities
    $acts = $this->workflow->getInstanceActivityInfo();
    $running_activity_id = $acts['running']['activityId']; //Set this var to check coherence in next step...

  }else{  // If no instance link get the process and display activities

    $this->workflow->initProcess(); //init the process

    //Check if this item has access free
    $accessCode = $this->document->checkAccess();
    if(!($accessCode == 0 || ( $accessCode > 1 && $accessCode <= 5 ) ) ){
      $this->_errors[] = tra('This item is not free');
    }//Else no access is required for current instance

    //Check if there is a process linked to doctype, container, project
    $default_process_id = $this->document->getProcessId();
    if( !$default_process_id ){
      $this->_errors[] = tra('no default process is find');
    }

    //Get infos about the selected process
    $proc_info = $this->workflow->getProcessInfos();

    //check if process is valid
    if( !$this->workflow->getProcess()->isValid() ){
      $this->_errors[] = tra('This process is not activated');
    }

    //Get infos about activities
    $acts = $this->workflow->getInstanceActivityInfo();
    $running_activity_id = 0; //Set this var to check coherence in next step...

  } //End of else

  //Check if activity is identical to previous selected document process : to ensure coherence between selected documents
  if( self::$_previous['activityId'] ){
    if( self::$_previous['activityId'] !== $running_activity_id ){
      $msg = 'document %s have not same state that %s. it is not selected';
      $e1 = $this->document->getProperty('document_number');
      $e2 = $this->session->current['document_number'];
      $this->_errors[] = sprintf( tra($msg), $e1, $e2);
    }
  }

  $this->_feedbackToElement();

  if($this->_errors){
    $font_color = 'red';
  }else{
    $font_color = 'green';
    $this->form->addElement('hidden', 'document_id['.$this->loop_id.']', $this->document->getId() );
    self::$_previous['pId'] = $proc_info['pId'];
    self::$_previous['activityId'] = $running_activity_id;
  }

  //Display document infos
  $html = '<b><font color='.$font_color.'>'.$this->document->getProperty('normalized_name').'</font></b><br />'.
            '<i>'.$this->document->getProperty('description').'</i><br />'.
            $this->document->getLockName($this->document->getProperty('access_code')).' '.
            '<b>'.$this->document->getProperty('state').'</b>';
  $this->form->addElement('static', '', $html );

  //Display infos about process
  $html = $proc_info['name'] .'-'. $proc_info['version'] .
          ' ( '. $proc_info['description'] .' )';
  $this->form->addElement('static', '', $html );

  $date_format = Ranchbe::getConfig()->date->format->long;
  //Display infos about running activity
  if($running_activity_id){
    $html =
    '<i>'.tra('Activity').'</i>: '.$acts['running']['name'].'('.$acts['running']['description'].')<br />'.
    '<i>'.tra('Type').'</i>: '.$acts['running']['type'].'<br />'.
    '<i>'.tra('Started').'</i>: '.strftime($date_format, $acts['running']['started']).'<br />'.
    '<i>'.tra('Status').'</i>: '.$acts['running']['actstatus'].'<br />'.
    '<i>'.tra('User').'</i>: '.$acts['running']['user'].'<br />'
    ;
  }else{
    $html = 'none';
  }
  $this->form->addElement('static', '', $html );

  //Display infos about running instance
  //var_dump( $ins_info );
  if($ins_info){
    $html =
    '<i>'.tra('Instance').'</i>: '.$ins_info['name'].'('.$ins_info['description'].')<br />'.
    '<i>'.tra('Created').'</i>: '.strftime($date_format, $ins_info['started']).'<br />'.
    '<i>'.tra('Status').'</i>: '.$ins_info['status'].'<br />'.
    '<i>'.tra('Owner').'</i>: '.$ins_info['owner'].'<br />'.
    '<i>'.tra('Next user').'</i>: '.$ins_info['nextUser'].'<br />'
    ;
  }else{
    $html = 'none';
  }
  $this->form->addElement('static', '', $html );

  return $this->form;

} //End of function

//-----------------------------------------------------------------
// generate html for feedback
protected function _feedbackToElement(){

  if($this->_errors){
    foreach($this->_errors as $error){
      $this->form->addElement('hidden', 'errors['.$this->loop_id.'][]', $error);
      $error_text .= str_replace('{error}', $error, $this->errorTemplate);
    }
    $semaphore = '<font color="red" size="1"><b>ERROR :</b></font>
                          <img border="0" alt="FATAL" src="img/error30.png" />';
    $this->form->addElement('static', '', $semaphore.' '.$error_text);
  }else{
    foreach($this->_feedbacks as $feedback){
      $this->form->addElement('hidden', 'feedbacks['.$this->loop_id.'][]', $feedback);
      $feedback_text .= str_replace('{feedback}', $feedback, $this->feedbackTemplate);
    }
    $semaphore = '<font color="green" size="1"><b>SELECTED</b></font>
                              <img border="0" alt="OK" src="img/accept.png" />';
    $this->form->addElement('static', '', $semaphore.' '.$feedback_text);
  }

} //End of function

//-----------------------------------------------------------------
public function getErrors(){
  return $this->_errors;
} //End of function

//-----------------------------------------------------------------
public function getFeedbacks(){
  return $this->_feedbacks;
} //End of function

//-----------------------------------------------------------------
public function getDocflow(){
  return $this->workflow;
} //End of function

} //End of class

