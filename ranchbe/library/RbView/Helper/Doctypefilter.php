<?php

/**
 * @see Zend_Controller_Action_Helper_Abstract
 */

class RbView_Helper_Doctypefilter extends RbView_Helper_Filter{

  //------------------------------------------------------------------------------
  /*! \brief
  */
  public function init(){
    $this->setTemplate('doctypeManager/searchBar.tpl');
    $this->options_def = array_merge($this->options_def, array(
      'sort_field'=>'doctype_number',
      'sort_order'=>'ASC',
      'find_number'=>'',
      )
    );
  } //End of method

  //------------------------------------------------------------------------------
  /*! \brief
  */
  protected function finish_find_number(){
    if( !empty($this->options['find_number']) )
      $this->search->setFind( $this->options['find_number'], 'doctype_number', 'anywhere' );
  } //End of method

} //End of class
