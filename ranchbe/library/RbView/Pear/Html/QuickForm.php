<?php
require_once ('HTML/QuickForm.php'); //Librairy to easily create forms

class RbView_Pear_Html_QuickForm extends HTML_QuickForm {
	
	protected $width = 'auto';
	
	function __construct($formName = '', $method = 'post', $action = '', $target = '', $attributes = null, $trackSubmit = false) {
		parent::HTML_QuickForm ( $formName, $method, $action, $target, $attributes, $trackSubmit );
		
		$defaultRender = & $this->defaultRenderer ();
		$defaultRender->setElementTemplate ( '<tr>
    <td class="formcolor">
      <!-- BEGIN required --><span style="color: #ff0000">*</span><!-- END required -->
      {label}
    </td>
    <td class="formcolor">
      <!-- BEGIN error --><span style="color: #ff0000">{error}</span><br /><!-- END error -->
      {element}
    </td>
  </tr>' );
		
		$defaultRender->setHeaderTemplate ( '<tr>
    <td colspan=2 style="align=center">
      <h1>{header}</h1>
    </td>
  </tr>' );
	
	} //End of method
	

	//to set the width of form. You must precise unit :px, em, ...
	function setWidth($width) {
		$this->width = $width;
	} //End of method
	

	function toHtml($in_data = null) {
		if (! is_null ( $in_data )) {
			$this->addElement ( 'html', $in_data );
		}
		
		$this->defaultRenderer ()->setFormTemplate ( '<div id="tiki-center" style="width:' . $this->width . ';">
			  <form{attributes}>
			    <table class="normal">
			      {content}
			    </table>
			  </form>
			  </div>
			  </body>
			  ' );
		
		$this->accept ( $this->defaultRenderer () );
		return $this->defaultRenderer ()->toHtml ();
	} // end func toHtml


} //End of class
