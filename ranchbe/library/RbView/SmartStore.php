<?php

class RbView_SmartStore extends RbView_SmartStoreForm{
	protected $document; //object document
	protected $container; //object container
	protected $space; //object space
	protected $action_definition = array(
              'create_doc' => 'Create a new document',
              'update_doc_file' => 'Update the document and files',
              'update_doc' => 'Update the document',
              'update_file' => 'Update the file only',
              'add_file' => 'Add the file to current document',
              'ignore' => 'Ignore',
              'upgrade_doc' => 'Create a new indice',
	);
	protected $actions = array('ignore'=>'Ignore'); //action apply to this document
	protected $default_action = 'ignore';
	protected $file; //full path to file for update
	protected $file_name; //name of file
	protected $file_id; //id of docfile to update
	protected $indice_id; //id of indice

	//List of default document properties to set from form input
	protected $fields  = array('document_number', 'description',
	                              'category_id', 'document_version');

	//----------------------------------------------------------
	/*
	*
	*/
	function __construct($space_name){
		$this->space_name = $space_name;
		$this->space =& Rb_Space::get($this->space_name);
	}//End of method

	//----------------------------------------------------------
	protected function _update_doc(){
		if(!$this->document->save()){
			$this->errors[] = 'Can not update the document = '.$this->document->getNumber();
			return false;
		}else{
			$this->feedbacks[] = 'the document ='.$this->document->getNumber().' has been updated.';
			return true;
		}
	} //End of function

	//----------------------------------------------------------
	protected function _update_doc_file(){
		if( $this->update_doc() )
		if( $this->update_file() )
		return true;
	} //End of function

	//----------------------------------------------------------
	protected function _update_file(){
		if( $this->docfile ){
			$this->feedbacks[] = 'update_file : '. $this->docfile->getNumber();
			//---------- Check if file exist in wildspace
			if($this->data->isExisting() ){ //-----update the file
				$this->document->checkOut(false); //Checkout without files
				if($this->docfile->checkOutFile(false)){
					if( $this->document->checkInAndRelease() ){ //replace document and files
						$this->feedbacks[] = $this->file_name.' has been updated.';
					}else{
						$this->errors[] = 'Can not checkin the file ='. $this->file_name;
						return false;
					}
				}else{
					$this->errors[] = 'Can not checkout the file ='. $this->file_name;
					return false;
				}
			}else{
				$this->errors[] = 'The file '.$this->file_name.' is not in your Wildspace ';
				return false;
			}
		}
		return true;
	} //End of function

	//----------------------------------------------------------
	protected function _add_file(){
		$this->_create_doc();
	} //End of function

	//----------------------------------------------------------
	/* \brief! Create a new document
	*/
	protected function _create_doc(){
		$fsdata = new Rb_Fsdata($this->file);
		if($fsdata){ //Add file to doc
			if( $this->document->ifExist($this->document->getNumber()) )
			{ //Dont believe the id give by step 1. The document_id might have changed after an indice upgrade,
				//$this->document =& Rb_Document::get($this->space_name, $this->document_id );
				//$this->document->setName( $this->report['properties']['document_number'] );
				$this->feedbacks[] = 'add_file : '. $this->file_name .' to '. $this->document->getNumber();
				//$docfile = new Rb_Docfile($this->space);
				//$this->docfile->setFsdata($fsdata);
				if( $this->document->associateDocfile($this->docfile , true) ){
					$this->feedbacks[] = 'the file '.$this->file_name.' has been associated to document '.$this->document->getNumber();
				}else{
					$this->errors[] = 'The file '.$this->file_name.' can not be associated to the document';
					return false;
				}
			}else{ //Create the doc
				//$this->document->setName( $this->report['properties']['document_number'] );
				$this->feedbacks[] = 'create_doc : '.$this->document->getNumber();
				/*
				 $this->docfile = new Rb_Docfile($this->space);
				 $this->docfile->setFsdata($fsdata);
				 $this->document->setDocfile($docfile);
				 $this->document->setContainer($this->container);
				 foreach($this->report['properties'] as $key=>$val){
				 $this->document->setProperty($key, $val);
				 }
				 */
				if( $this->document->store(false) )
				$this->feedbacks[] = 'The doc '.$this->document->getNumber().' has been created';
				else{
					$this->errors[] = 'Can not create the doc '.$this->document->getNumber();
					return false;
				}
			} //End of create doc
		}else{
			$this->errors[] = 'The file '.$this->file_name.' is not in your Wildspace';
			return false;
		}

		return true;

	} //End of function

	//----------------------------------------------------------
	/* Create a new indice and update the files if specifed
	*/
	protected function _upgrade_doc(){
		if(Ranchbe::checkPerm( 'document_create' , $this->document , false) === false){
			$this->feedbacks[] = 'You have not permission to upgrade the document '.$this->document->getNumber();
			break;
		}
		$this->feedbacks[] = 'Try to upgrade document '.$this->document->getNumber();
		//Check access
		$accessCode = $this->document->checkAccess();
		if($accessCode == 0){
			//Lock the current indice
			if( !$this->document->lockDocument(11, true) ){
				$this->errors[] = 'Can not lock the document '.$this->document->getNumber();
				break;
			}
			//Lock access to files too
			$docfiles =& $this->document->getDocfiles();
			if(is_array($docfiles))
			foreach($docfiles as $docfile){
				$docfile->lockFile(11, true);
			}
			$this->feedbacks[] = 'the document '.$this->document->getNumber().' has been locked.';
		}else{
			if($accessCode == 1){
				$this->errors[] = 'This document is checkout';
				break;
			}
		}

		//Create the new indice
		if( $this->document_version > $this->document->getProperty('document_version') )
		$new_indice_id = $this->document_version;
		else
		$new_indice_id = NULL; // Let createVersion method choose new indice
		if( $new_doc_id = $this->document->createVersion($new_indice_id, $this->container->getId()) ){
			$this->feedbacks[] = 'the document ID='.$new_doc_id.' has been created.';
			$onewDocument =& Rb_Document::get($this->space->getNumber(), $new_doc_id);
			//Update the new indice with metadata to import
			/*
			if( !$onewDocument->updateDocInfos($this->report['properties']) ){
			$this->errors[] = 'Can not update document data='.$accessCode;
			}else{
			$this->feedbacks[] = 'the document ID='.$new_doc_id.' has been updated.';
			}
			*/
			//----------------- Update the files
			//Get docfiles
			if($file_id = Rb_Docfile::get($this->space_name)->getFileId( $this->file_name , $new_doc_id ) ){ //- Get the id of file from new document
				$odocfile =& Rb_Docfile::get($this->space_name, $file_id);
				//$onewDocument->getDocfile();
				//$odocfile->init($file_id);
				$odata = Rb_Fsdata::_dataFactory($this->file);
				if($odata){ //-----update the file
					$onewDocument->checkOut(false); //Checkout document without files
					if($odocfile->checkOutFile(false)){ //Checkout file but without copy of file in wildspace
						if( $onewDocument->checkInAndRelease() ){ //checkin files
							$this->feedbacks[] = $this->file_name.' has been updated.';
						}else{
							$this->errors[] = 'Can not checkin the file ='. $this->file_name;
						}
					}else{
						$this->errors[] = 'Can not checkout the file ='. $this->file_name;
					}
				}else{
					$this->errors[] = 'The file '.$this->file_name.' is not in your Wildspace ';
				}
			}else $this->errors[] = 'Can not update the new indice';
		}else $this->errors[] = 'Can not create the new indice';
		return true;
	} //End of function

	//-----------------------------------------------------------------
	//Genere une ligne de formulaire dans le cas du store multiple
	//Le principe est de renommer chaque champ du formulaire (y compris les champs issues des metadonnees crees par les utilisateurs)
	//et d'y ajouter '[$i]' ou $i est un numero incrementale issue de la boucle de parcours de $_REQUEST['checked'].
	//Le formulaire retourne alors des variables tableaux. La description[1] correspondra au fichier[1] etc.
	//function setCreateDocForm(HTML_QuickForm &$form_collection, $loop_id, $docaction=NULL){
	public function setForm($loop_id, $docaction=false, $validate=false, $request=array() ){
		$this->form = new RbView_Smarty_Form_Sub('form_'.$loop_id, 'post');
		$this->isFrozen = $request['isFrozen'];

		/*
		 there is a serious bug with quickform : the submit value are reused as default value for form field.
		 in this case file_name is submit and its a array where key are not synchro with $loop_id. so when create form
		 for loop_id = 1 the file_name default value is set from submit value file_name[1] wich is not the file_name for current loop
		 AND (its the real bug)
		 the call to setDefaults is ignored, so its impossible to force the default value to other things that the submit value.
		 Solutions :
		 $this->form->_submitValues = array();
		 OR
		 be carfull to always synchronize submit values with the form fields
		 OR
		 dont use integer for loop_id
		 */

		//$this->form->_submitValues = array_merge($this->form->_submitValues, $request);
		//var_dump($this->form->_submitValues);

		$defaultRender =& $this->form->defaultRenderer();
		$defaultRender->setFormTemplate($this->subformTemplate);
		$defaultRender->setHeaderTemplate($this->subheaderTemplate);
		$defaultRender->setElementTemplate($this->subelementTemplate);

		$this->form->_requiredNote = '';
		$this->loop_id = $loop_id;

		if( count($this->actions) == 1 ){
			if ( isset($this->actions['ignore']) ){
				$disabled = 'DISABLED';
				$readonly = 'READONLY';
				$this->form->addElement('text', 'file_name['.$loop_id.']', 'File',
				array('readonly', 'size'=>32));
				$this->form->addElement('text', 'document_number['.$loop_id.']', 'Document_number',
				array('readonly', 'size'=>32));
				$this->form->setDefaults(array(
		          'document_number['.$loop_id.']' => $this->document->getNumber(),
		          'description['.$loop_id.']' => $this->document->getProperty('description'),
		          'file_name['.$loop_id.']' => $this->file_name,
				));
				$this->_errorsToElement();
				return $this->form;
			}
		}

		$mask = Ranchbe::getConfig()->document->mask;
		if($this->file_name){
			$this->form->addElement('text', 'file_name['.$loop_id.']', tra('File') ,
			array('readonly', 'size'=>32));
			$this->form->addRule('file_name['.$loop_id.']',
                  'This file name is not valid', 'regex', "/$mask/" , 'server');
		}

		$this->form->addElement('text', 'document_number['.$loop_id.']', tra('document_number'),
		array('readonly', 'size'=>32) );

		$this->form->addRule('document_number['.$loop_id.']',
              'This document name is not valid', 'regex', "/$mask/" , 'server');

		$this->form->addElement('hidden', 'loop_id['.$loop_id.']', $loop_id);
		$this->form->addElement('hidden', 'document_id['.$loop_id.']', $this->document_id);
		$this->form->addElement('hidden', 'file_id['.$loop_id.']', $this->file_id);

		$description = $this->document->getProperty('description');
		if( !$description )
		$description = 'undefined';

		$this->form->setDefaults(array(
	      'document_number['.$loop_id.']' => $this->document->getNumber(),
	      'description['.$loop_id.']' => $description,
	      'category_id['.$loop_id.']' => $this->document->getProperty('category_id'),
	      'file_name['.$loop_id.']' => $this->file_name
		));

		//Add select category
		$params = array(
              'is_multiple' => false,
              'property_name' => 'category_id['.$loop_id.']',
              'property_description' => tra('Category').' <br /><a href="#" onClick="javascript:setValue(\'category_id['.$loop_id.']\');'.
					                    'getElementById(\'redisplay\').value=1;'.
					                    'getElementById(\'step\').value=\'validate\';'.
					                    'getElementById(\'form\').submit();'.
					                    'return false;">'.
										tra('same for all').'</a>',
              'property_length' => '1',
              'return_name' => false,
              'default_value' => $this->document->getProperty('category_id'),
              'disabled' => $disabled,
              'onChange'=>'javascript:getElementById(\'redisplay\').value=1;'.
                                      'getElementById(\'step\').value=\'validate\';'.
                                      'getElementById(\'form\').submit();'.
                                      'return false;'
          );
          construct_select_category($params , $this->form , $this->document->getFather(), 'server');

          //Add select document indice
          $params = array(
              'is_multiple' => false,
              'property_name' => 'document_version['.$loop_id.']',
              'property_description' => tra('document_version').' <br /><a href="#" onClick="javascript:setValue(\'document_version['.$loop_id.']\');return false;">'.tra('same for all').'</a>',
              'property_length' => '1',
              'is_required' => true,
              'default_value' => $this->document->getProperty('document_version'),
              'disabled' => $disabled,
          );
          construct_select_indice($params, $this->form, 'server');

          /*
           if( array_key_exists( 'upgrade_doc', $this->actions ) || array_key_exists( 'create_doc', $this->actions ) ){
           construct_select_indice($params , $this->form);
           }else{
           $this->form->addElement('select', $params['property_name'], $params['property_description'], array(), array($disabled) );
           }
           */

          $label = tra('Description').' <br /><a href="#" onClick="javascript:setValue(\'description['.$loop_id.']\');return false;">'.tra('same for all').'</a>';
          $this->form->addElement('textarea', 'description['.$loop_id.']' , $label , array(
				          'rows'=>2, 
				          'cols'=>32, 
				          $disabled,
				          $readonly,
				          'id'=>'description['.$loop_id.']') );
          $this->form->addRule('description['.$loop_id.']', tra('is required'), 'required');

          //Get fields for custom metadata
          $optionalFields = array();
          if( $this->document->getProperty('category_id') ){
          	$optionalFields = Rb_Category_Relation_Metadata::get()
          	->getMetadatas( $this->document->getProperty('category_id') );
          }else{
          	//--Get the metadatas from the container links
          	$optionalFields = Rb_Container_Relation_Docmetadata::get()->
          	getMetadatas( $this->document->getProperty('container_id') );
          }

          if(is_array($optionalFields))
          foreach($optionalFields as $field){
          	$field['default_value'] = $this->document->getProperty($field['property_fieldname']);
          	$this->fields[] = $field['property_fieldname'];
          	$field['property_fieldname'] = $field['property_fieldname'] . '['.$loop_id.']';
          	$field['disabled'] = $disabled;
          	if($field['property_type'] != 'date' && $field['property_type'] != 'liveSearch') //the js script to copy properties dont operate with date and livesearch fields
          	$field['property_description'] =
          	$field['property_description']
          	.' <br /><a href="#" onClick="javascript:setValue(\''
          	.$field['property_fieldname'].'\');return false;">'
          	.tra('same for all').'</a>';
          	construct_element($field , $this->form, 'server');
          }

          //Create hidden field to retrieve initial action list
          foreach($this->actions as $action=>$description){
          	$this->form->addElement('hidden', 'actions['.$loop_id.']['.$action.']', $description);
          }

          //Add select action to perform
          if( is_array(tra('action_definition')) )
          $this->action_definition = tra('action_definition'); //Get array from traduction file if traduction exists. Be careful to order of actions in this array. The first element fund is used as default value.
          if( is_array($this->actions) )
          $this->actions = array_intersect_key($this->action_definition , $this->actions);

          if($docaction)
          $this->default_action = $docaction;

          $params = array(
              'is_multiple' => false,
              'property_name' => 'docaction['.$loop_id.']',
              'property_description' => tra('Action'),
              'property_length' => '1',
              'return_name' => false,
              'default_value' => $this->default_action, //Default value is the first key of the action array
              'disabled' => $disabled,
          );
          construct_select($this->actions , $params , $this->form);

          //Apply new filters to the element values
          $this->form->applyFilter('__ALL__', 'trim');

          foreach($this->fields as $property_name){
          	$this->form->addElement('hidden', 'fields['.$loop_id.'][]', $property_name);
          }

          if($validate){
          	if($docaction != 'create_doc')
          	unset($this->fields['document_version']); //prevent change of indice by this way
          	$this->_initDocumentProperties($this->fields, $this->loop_id, $request);
          	$this->_validateForm($docaction);
          }

          $this->_errorsToElement();
          $this->_feedbackToElement();

          return $this->form;

	} //End of function

	//-----------------------------------------------------------------
	// validate the form
	protected function _validateForm($docaction){
		if ( $this->isFrozen ){
			$this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
			$this->form->freeze();
			return true;
		}

		if ( $this->form->validate() ){
			switch($docaction){
				case "ignore": //Ignore
					return true;
					break;
				case "update_doc": //Update the document only
					if ( $this->_update_doc() ){  //call function
						$this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
						$this->form->freeze(); //and freeze it
						return true;
					}
					break;
				case "update_doc_file": //Update the document and the file if specified
					if ( $this->_update_doc_file() ){  //call function
						$this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
						$this->form->freeze(); //and freeze it
						return true;
					}
					break;
				case "update_file": //Update the associated file only
					if ( $this->_update_file() ){  //call function
						$this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
						$this->form->freeze(); //and freeze it
						return true;
					}
					break;
				case "add_file": //Add the file to the document
					if ( $this->_add_file() ){  //call function
						$this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
						$this->form->freeze(); //and freeze it
						return true;
					}
					break;
				case "create_doc": //Create a new document
					if ( $this->_create_doc() ){  //call function
						//if ( true ){
						$this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
						$this->form->freeze(); //and freeze it
						return true;
						}
						break;
				case "upgrade_doc": //Create a new indice and update the files if specifed
					if ( $this->_upgrade_doc() ){  //call function
						$this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
						$this->form->freeze(); //and freeze it
						return true;
					}
					break;
					} //End of switch
			}
			return false;

		} //End of function

		//-----------------------------------------------------------------
		public function setIndice($id){
			$this->indice_id = $id;
		}

		//-----------------------------------------------------------------
		/* transform return array in string and date array in timestamp
		* $name name of property
		* $val value of property
		* $form HTML_Quickform from where comes property
		* return normalized value
		*/
		protected function _normalizeProperty($name, $val, &$form){
			if(is_array($val)){
				if( $form->GetElementType($name.'['.$this->loop_id.']') == 'date'){
					$val = mktime($val['h'], $val['m'], $val['s'], $val['M'], $val['d'], $val['Y']);
				}else{
					$val = implode('#' , $val);
				}
			}else{
				if($name == 'category_id' && empty($val) )
				$val = null;
			}
			return $val;
		} //End of function
		
		//-----------------------------------------------------------------
		//---------- Set document properties from form input
		protected function _initDocumentProperties($properties_name, $loop_id, $request=array() ){
			foreach($properties_name as $property_name){
				$val = $request[$property_name];
				$val = $this->_normalizeProperty($property_name,
				$val[$loop_id],
				$this->form);
				$this->document->setProperty($property_name, $val);
			}
		} //End of function
		
		//---------------------------------------------------------------------
		/** Check if file exist in path
		 * 
		 * @param unknown_type $file_name
		 * @param unknown_type $path
		 * @return Rb_Fsdata|false
		 */
		public function testFile($file_name, $path){
			$data = new Rb_Fsdata($path.'/'.$file_name);
			if( !$data->isExisting() ){ //file dont exist so return error message to user
				$this->errors[] = $file_name.' is not in your Wildspace.';
				$this->actions['ignore'] = 'Ignore';
				return false;
			}else{
				$this->file_name = $data->getProperty('file_name');
				$this->file = $data->getProperty('file');
				return $this->data =& $data;
			}
		} //End of method
		
		//---------------------------------------------------------------------
		//test is file exist
		//$data Rb_Fsdata | string Fsdata of file or file name
		public function isDocfile($data){
			if (is_a($data, 'Rb_Fsdata')){
				$file_name = $data->getProperty('file_name');
			}else if(is_string($data)){
				$file_name = trim($data);
			}else{
				return trigger_error('Bad type for parameter $data', E_USER_ERROR);
			}
			
			if($file_id = Rb_Docfile::getFileId($this->space_name, $file_name) ){ //- The file is recorded in database
				$this->docfile =& Rb_Docfile::get($this->space_name, $file_id);
				$this->document =& $this->docfile->getFather();
				
				$this->errors[] = 'file '.$file_name . ' exist in this database. ID=' . $file_id;
				$this->errors[] = 'file '.$file_name . ' is linked to document ' .
								$this->document->getName().' ID='.$this->document->getId().
                              	' in container '.$this->document->getFather()->getNumber();
				
				//Compare md5
				if( $this->docfile->getProperty('file_md5') == $data->getProperty('file_md5') ){
					$equal_md5 = true;
				}
				
				if($this->document->checkAccess() != 0){
					$this->errors[] = 'The document '.$this->document->getNumber().' is not free ';
				}else if( $this->docfile->checkAccess() != 0 ){
					$this->errors[] = 'file '.$file_name . ' is not free ';
				}else{
					if( $equal_md5 ){
						$this->errors[] = 'files '.$file_name.' from wildspace and from vault are strictly identicals';
						$this->actions['update_doc'] = 'Update the document '.$this->document->getNumber();
						//$this->default_action = 'update_doc';
					}else{
						$this->actions['update_doc_file'] = 'Update the document '.$this->document->getNumber().' and files';
						$this->actions['update_file'] = 'Update the file '.$file_name.' only';
						$this->actions['upgrade_doc'] = 'Create a new indice of '.$this->document->getNumber();
						//$this->default_action = 'update_doc_file';
					}
				}
				return true;
			}else{
				return false;
			}
		} //End of method
		
		//---------------------------------------------------------------------
		//test if $doc_name is a existing document. If is, init document
		public function isDocument($doc_name){
			if( $document_id = Rb_Document::get($this->space_name)->ifExist( $doc_name ) ){
				$this->document =& Rb_Document::get($this->space_name, $document_id );
				$this->errors[] = 'The document '.$this->document->getNumber() .
                                  ' exist in this database. ID=' . $document_id;
				if($this->document->checkAccess() !== 0){
					$this->errors[] = 'The document '.$this->document->getNumber() . ' is not free ';
				}else{
					$this->actions['add_file'] = 'add_file';
					$this->actions['upgrade_doc'] = 'Create a new indice of '.$this->document->getNumber();
				}
				return true;
			}else{
				return false;
			}
		} //End of method
		
		//---------------------------------------------------------------------
		//init a new document and a new docfile
		public function initNewDocument(Rb_Container &$container, $fsdata = false, $document_number = false){
			$this->document = new Rb_Document( $this->space , 0);
			$this->document->setContainer( $container );
			if($fsdata){
				$this->docfile = new Rb_Docfile( $this->space , 0);
				$this->docfile->setFsdata( $fsdata );
				$this->document->setDocfile( $this->docfile, 'main' );
			}
			if( $document_number ){
				$this->document->setProperty( 'document_number', $document_number );
			}else if($fsdata){
				$this->document->setProperty( 'document_number', $fsdata->getProperty('doc_name') );
			}
			$this->actions['create_doc'] = 'Create a new document';
			$this->default_action = 'create_doc';
		} //End of method
		
		//---------------------------------------------------------------------
		//Check the doctype for the new document
		public function setDoctype(){
			if($this->data){
				$doctype =& $this->document->setDocType($this->data->getProperty('file_extension'),
				$this->data->getProperty('file_type'));
			}else{
				$doctype =& $this->document->setDocType('','nofile');
			}

			if(!$doctype){
				$this->errors[] = 'This document has a bad doctype';
				$this->actions = array('ignore'=>'Ignore'); //Reinit all actions
				return false;
			}else{
				$this->feedbacks[] = 'Reconize doctype : '.$doctype->getProperty('doctype_number');
				return $doctype;
			}
		} //End of method
		
		//---------------------------------------------------------------------
		/** Set category for document with doctype
		 * 
		 * @param Rb_Doctype $doctype
		 * @param integer $category_id
		 * @return void
		 */
		public function setCategory(&$doctype, $category_id = false){
			//Get the category
			if($category_id){
				$this->document->setProperty( 'category_id', $category_id );
			}else if(is_a($doctype, 'Rb_Doctype')){
				$categories = Rb_Container_Relation_Doctype_Category::get()
							->getCategories($this->document->getProperty('container_id'),$doctype->getId());
				$this->document->setProperty('category_id', $categories[0]['category_id']);
			}
		} //End of method
		
		//---------------------------------------------------------------------
		/**
		 * 
		 */
		public function getDocument(){
			return $this->document;
		} //End of method
		
		//---------------------------------------------------------------------
		public function getDocfile(){
			return $this->docfile;
		} //End of method
		
		//---------------------------------------------------------------------
		/**
		 * @return array
		 */
		public function getActions(){
			return $this->actions;
		} //End of method
		
		//---------------------------------------------------------------------
		/**
		 * @return array
		 */
		public function getErrors(){
			return $this->errors;
		} //End of method
		
} //End of class

