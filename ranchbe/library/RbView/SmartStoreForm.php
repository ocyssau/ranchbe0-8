<?php
class RbView_SmartStoreForm{
	//protected $doc_properties = array();
	protected $feedbacks = array();
	protected $errors = array();
	public $subformTemplate = "<table><tr>\n{hidden}\n{content}\n</tr></table>";
	public $subheaderTemplate = "<h1>{header}</h1>";
	public $subelementTemplate = "<td NOWRAP><i>{label}<!-- BEGIN required --><font color=red>*</font><!-- END required --></i>
      <!-- BEGIN error --><font color=red><i><b><br />{error}</b></i></font><!-- END error --><br />
      {element}
      </td>";
	public $feedbackTemplate = "<font color=green>{feedback}</font><br />";
	public $errorTemplate = "<font color=red><b>{error}</b></font><br />";
	protected $form; //RbView_Smarty_Form_Sub instance
	protected $loop_id; //(int)

	//List of default document properties to set from form input
	protected $fields  = array('document_number', 'description',
                              'category_id', 'document_version');

	function __construct($space_name){
		$this->space_name = $space_name;
		$this->space =& Rb_Space::get($this->space_name);
	}//End of method

	function setForm( $loop_id, $docaction=false, $validate=false, $request=array() ){
		$this->form = new RbView_Smarty_Form_Sub('form_'.$loop_id, 'post');

		$defaultRender =& $this->form->defaultRenderer();
		$defaultRender->setFormTemplate($this->subformTemplate);
		$defaultRender->setHeaderTemplate($this->subheaderTemplate);
		$defaultRender->setElementTemplate($this->subelementTemplate);

		$this->form->_requiredNote = '';
		$this->loop_id = $loop_id;

		$this->_errorsToElement();
		$this->_feedbackToElement();

		return $this->form;
	} //End of function

	//-----------------------------------------------------------------
	// generate html for errors
	protected function _errorsToElement(){
		foreach($this->errors as $error){
			$this->form->addElement('hidden', 'errors['.$this->loop_id.'][]', $error);
			$error_text .= str_replace('{error}', $error, $this->errorTemplate);
		}
		$this->form->addElement('static', '', $error_text);
	} //End of function

	//-----------------------------------------------------------------
	// generate html for feedback
	protected function _feedbackToElement(){
		foreach($this->feedbacks as $feedback){
			$this->form->addElement('hidden', 'feedbacks['.$this->loop_id.'][]', $feedback);
			$feedback_text .= str_replace('{feedback}', $feedback, $this->feedbackTemplate);
		}
		$this->form->addElement('static', '', $feedback_text);
	} //End of function

	public function setError($message){
		$this->errors[] = $message;
	} //End of method

	public function setFeedback($message){
		$this->feedbacks[] = $message;
	} //End of method

	public function setAction($action){
		$this->actions[] = $action;
	} //End of method

	public function setPropertyToUpdate($property_name){
		$this->fields[] = $property_name;
	} //End of method
} //End of class

