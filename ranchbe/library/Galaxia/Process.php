<?php


require_once('Galaxia/Base.php');


/**
 * This class representes the process that is being executed when an activity
 * is executed. You can access this class methods using $process from any activity.
 * No need to instantiate a new object.
 *
 */
class Galaxia_Process extends Galaxia_Base {
	/**
	 * 
	 * @var string
	 */
	public $name;
	
	/**
	 * 
	 * @var string
	 */
	public $description;
	
	/**
	 * 
	 * @var integer
	 */
	public $version;
	
	/**
	 * 
	 * @var string
	 */
	public $normalizedName;
	
	/**
	 * 
	 * @var integer
	 */
	public $pId = 0;
	
	/**
	 * 
	 * @var string	n|y
	 */
	public $isValid = 'n'; //Add by ranchbe
	
	/**
	 * 
	 * @var string	n|y
	 */
	public $isActive = 'n'; //Add by ranchbe

	
	/**
	 * 
	 * @param ADOConnection	$db
	 * @return void
	 */
	public function __construct(ADOConnection $db) {
		$this->db = $db;
	}

	/**
	 * Loads a process form the database
	 * 
	 * 
	 * @param $pId
	 * @return unknown_type
	 */
	function getProcess($pId) {
		$query = "select * from `".GALAXIA_TABLE_PREFIX."processes` where `pId`=?";
		$result = $this->query( $query, array($pId) );
		if(!$result->numRows()){
			return false;
		} 
		$res = $result->fetchRow();
		
		$this->name = $res['name'];
		$this->description = $res['description'];
		$this->normalizedName = $res['normalized_name'];
		$this->version = $res['version'];
		$this->pId = $res['pId'];
		$this->isValid = $res['isValid']; //Add by ranchbe
		$this->isActive = $res['isActive']; //Add by ranchbe
	}

	/**
	 * Gets the normalized name of the process
	 * 
	 * @return string
	 */
	function getNormalizedName() {
		return $this->normalizedName;
	}

	/**
	 * Gets the process name
	 * @return string
	 */
	function getName() {
		return $this->name;
	}

	/**
	 * Gets the process version
	 * 
	 * @return integer
	 */
	function getVersion() {
		return $this->version;
	}

	/**
	 * Gets information about an activity in this process by name,
	 * e.g. 
	 * <code>
	 * $actinfo = $process->getActivityByName('Approve CD Request');
	 *  if ($actinfo) {
	 * $some_url = 'tiki-g-run_activity.php?activityId=' . $actinfo['activityId'];
	 * }
	 * </code>
	 * 
	 * @param $actname
	 * @return unknown_type
	 */
	function getActivityByName($actname) {
		// Get the activity data
		$query = "select * from `".GALAXIA_TABLE_PREFIX."activities` where `pId`=? and `name`=?";
		$pId = $this->pId;
		$result = $this->query($query,array($pId,$actname));
		if(!$result->numRows()){
			return false;
		}
		$res = $result->fetchRow();
		return $res;
	}

	/**
	 * Add by RanchBE
	 * return true if process is valid and active
	 * 
	 * @return boolean
	 */
	function isValid() {
		if($this->isValid == 'y' && $this->isActive == 'y'){
			return true;
		}
		else{
			return false;	
		}
	}

}
