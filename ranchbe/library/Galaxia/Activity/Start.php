<?php

require_once('Galaxia/Activity.php');

/**
 * 
 * This class handles activities of type 'start'
 *
 */
class Galaxia_Activity_Start extends Galaxia_Activity {
	
	/**
	 * 
	 * @param $db
	 * @return void
	 */
	function __construct($db)
	{
		$this->setDb($db);
	}
}


