<?php

require_once('Galaxia/Activity.php');

/**
 * This class handles activities of type 'activity'
 *
 */
class Galaxia_Activity_Activity extends Galaxia_Activity {
	
	/**
	 * 
	 * @param $db
	 * @return void
	 */
	public function __construct($db) {
		$this->setDb($db);
	}
	
}


