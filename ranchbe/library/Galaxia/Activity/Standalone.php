<?php

require_once('Galaxia/Activity.php');

/**
 * 
 * This class handles activities of type 'standalone'
 *
 */
class Galaxia_Activity_Standalone extends Galaxia_Activity {
	
	/**
	 * 
	 * @param $db
	 * @return void
	 */
	function __construct($db)
	{
		$this->setDb($db);
	}
}


