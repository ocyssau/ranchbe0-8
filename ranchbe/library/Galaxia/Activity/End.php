<?php

require_once('Galaxia/Activity.php');


/**
 * 
 * This class handles activities of type 'end'
 *
 */
class Galaxia_Activity_End extends Galaxia_Activity {
	
	/**
	 * 
	 * @param $db
	 * @return void
	 */
	function __construct($db)
	{
		$this->setDb($db);
	}
	
	
}


