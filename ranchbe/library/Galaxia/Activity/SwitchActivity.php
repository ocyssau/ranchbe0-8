<?php

require_once('Galaxia/Activity.php');

/**
 * 
 * This class handles activities of type 'switch'
 *
 */
class Galaxia_Activity_SwitchActivity extends Galaxia_Activity {
	
	/**
	 * 
	 * @param $db
	 * @return unknown_type
	 */
	function __construct($db)
	{
		$this->setDb($db);
	}
}

