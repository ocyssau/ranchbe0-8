<?php


require_once('Galaxia/Observer.php');

/**
 * Log
 * 
 *
 */
class Galaxia_Observer_Logger extends Galaxia_Observer {
	
	/**
	 * 
	 * @var string
	 */
	protected $_filename;

	
	/**
	 * 
	 * @param 	string	$filename
	 * @return 	void
	 */
	function __construct($filename) {
		$this->_filename = $filename;
		$fp = fopen($this->_filename,"a+");
		if(!$fp) {
			trigger_error("Galaxia_Observer_Logger cannot append to log file: ".$this->filename, E_USER_WARNING);
		}
		if($fp) {
			fclose($fp);
		}

	}

	/**
	 * (non-PHPdoc)
	 * @see library/Galaxia/Galaxia_Observer#notify($event, $msg)
	 */
	function notify($event,$msg) {
		// Add a line to the log file.
		$fp = fopen($this->_filename,"a");
		$date = date("[d/m/Y h:i:s]");
		$msg=trim($msg);
		fputs($fp,$date." ".$msg."\n");
		fclose($fp);
	}
}
