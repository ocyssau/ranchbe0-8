<?php
//include_once(GALAXIA_LIBRARY.'/src/common/Galaxia_Base.php');
//!! Abstract class representing the base of the API
//! An abstract class representing the API base
/*!
 This class is derived by all the API classes so they get the
 database connection, database methods and the Observable interface.
 */
class Galaxia_Manager_Base extends Galaxia_Base {

	// Constructor receiving an ADODB database object.
	function __construct($db)
	{
		if(!$db) {
			die("Invalid db object passed to Galaxia_Manager_Base constructor");
		}
		$this->db = $db;
	}

} //end of class
