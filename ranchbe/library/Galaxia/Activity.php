<?php


require_once ('Galaxia/Base.php');


/**
 * Abstract class representing activities
 *
 *
 * This class represents activities, and must be derived for
 * each activity type supported in the system. Derived activities extending this
 * class can be found in the activities subfolder.
 * This class is observable.
 * 
 *
 */
class Galaxia_Activity extends Galaxia_Base {

	/**
	 *
	 * @var string
	 */
	protected $name;

	/**
	 *
	 * @var string
	 */
	protected $normalizedName;

	/**
	 *
	 * @var string
	 */
	protected $description;

	/**
	 *
	 * @var string
	 */
	protected $isInteractive;

	/**
	 *
	 * @var boolean
	 */
	protected $isAutoRouted;

	/**
	 * Add by ranchbe
	 * @var boolean
	 */
	protected $isAutomatic;

	/**
	 * Add by ranchbe
	 * @var boolean
	 */
	protected $isComment;

	/**
	 *
	 * @var array
	 */
	protected $roles = array();

	/**
	 *
	 * @var array
	 */
	protected $outbound = array();

	/**
	 *
	 * @var array
	 */
	protected $inbound = array();

	/**
	 *
	 * @var integer
	 */
	protected $pId;

	/**
	 *
	 * @var integer
	 */
	protected $activityId;

	/**
	 *
	 * @var unknown_type
	 */
	protected $type;

	/**
	 *
	 * @var integer
	 */
	protected $expirationTime = 0;


	function setDb($db) {
		$this->db = $db;
	}

	function __construct($db) {
		$this->db = $db;
		$this->type = 'base';
	}

	/**
	 * Factory method returning an activity of the desired type
	 * loading the information from the database.
	 *
	 * @param integer
	 * @return Galaxia_Activity
	 */
	function getActivity($activityId) {
		$query = "select * from `" . GALAXIA_TABLE_PREFIX . "activities` where `activityId`=?";
		$result = $this->query ( $query, array ($activityId ) );
		if (! $result->numRows ())
		return false;
		$res = $result->fetchRow ();
		switch ($res ['type']) {
			case 'start' :
				$act = new Galaxia_Activity_Start ( $this->db );
				break;
			case 'end' :
				$act = new Galaxia_Activity_End ( $this->db );
				break;
			case 'join' :
				$act = new Galaxia_Activity_Join ( $this->db );
				break;
			case 'split' :
				$act = new Galaxia_Activity_Split ( $this->db );
				break;
			case 'standalone' :
				$act = new Galaxia_Activity_Standalone ( $this->db );
				break;
			case 'switch' :
				$act = new Galaxia_Activity_SwitchActivity ( $this->db );
				break;
			case 'activity' :
				$act = new Galaxia_Activity_Activity ( $this->db );
				break;
			default :
				trigger_error ( 'Unknown activity type:' . $res ['type'], E_USER_WARNING );
		}

		$act->setName ( $res ['name'] );
		$act->setProcessId ( $res ['pId'] );
		$act->setNormalizedName ( $res ['normalized_name'] );
		$act->setDescription ( $res ['description'] );
		$act->setIsInteractive ( $res ['isInteractive'] );
		$act->setIsAutoRouted ( $res ['isAutoRouted'] );
		$act->setIsAutomatic ( $res ['isAutomatic'] );
		$act->setIsComment ( $res ['isComment'] );
		$act->setActivityId ( $res ['activityId'] );
		$act->setType ( $res ['type'] );

		//Now get forward transitions


		//Now get backward transitions


		//Now get roles
		$query = "select `roleId` from `" . GALAXIA_TABLE_PREFIX . "activity_roles` where `activityId`=?";
		$result = $this->query ( $query, array ($res ['activityId'] ) );
		while ( $res = $result->fetchRow () ) {
			$this->roles [] = $res ['roleId'];
		}
		$act->setRoles ( $this->roles );
		return $act;
	}

	/**
	 * Returns an Array of roleIds for the given user
	 *
	 * @param $user
	 * @return integer		The role id
	 */
	function getUserRoles($user) {
		$query = "select `roleId` from `" . GALAXIA_TABLE_PREFIX . "user_roles` where `user`=?";
		$result = $this->query ( $query, array ($user ) );
		$ret = Array ();
		while ( $res = $result->fetchRow () ) {
			$ret [] = $res ['roleId'];
		}
		return $ret;
	}

	/**
	 * Returns an Array of asociative arrays with roleId and name for the given user
	 * @return array		array(roleId=>'', name=>'')
	 */
	function getActivityRoleNames() {
		$aid = $this->activityId;
		$query = "select gr.`roleId`, `name` from `" . GALAXIA_TABLE_PREFIX . "activity_roles` gar, `" . GALAXIA_TABLE_PREFIX . "roles` gr where gar.`roleId`=gr.`roleId` and gar.`activityId`=?";
		$result = $this->query ( $query, array ($aid ) );
		$ret = Array ();
		while ( $res = $result->fetchRow () ) {
			$ret [] = $res;
		}
		return $ret;
	}


	/**
	 * Returns the normalized name for the activity
	 * @return string
	 */
	function getNormalizedName() {
		return $this->normalizedName;
	}

	/**
	 * Sets normalized name for the activity
	 *
	 * @param 	string	$name
	 * @return void
	 */
	function setNormalizedName($name) {
		$this->normalizedName = $name;
	}

	/**
	 *  Sets the name for the activity
	 * @param 	string	$name
	 * @return void
	 */
	function setName($name) {
		$this->name = $name;
	}

	/**
	 * Gets the activity name
	 * @return string
	 */
	function getName() {
		return $this->name;
	}

	/** 
	 * Sets the activity description 
	 * @param 	string	$desc
	 * @return void
	 */
	function setDescription($desc) {
		$this->description = $desc;
	}

	/** 
	 * Gets the activity description 
	 * @return string
	 */
	function getDescription() {
		return $this->description;
	}

	/**
	 *  Sets the type for the activity.
	 *  this does NOT allow you to change the actual type 
	 *  @param 	string	$type
	 *  @return void
	 */
	function setType($type) {
		$this->type = $type;
	}

	/** 
	 * Gets the activity type
	 * @return string
	 */
	function getType() {
		return $this->type;
	}

	/**
	 *  Sets if the activity is interactive 
	 *  @param 	boolean		$is
	 *  @return void
	 */
	function setIsInteractive($is) {
		$this->isInteractive = $is;
	}

	/**
	 *  Returns if the activity is interactive 
	 *  @return boolean
	 */
	function isInteractive() {
		return $this->isInteractive == 'y';
	}

	/**
	 *  Sets if the activity is auto-routed
	 *  @param 	boolean		$is
	 *  @return void
	 */
	function setIsAutoRouted($is) {
		$this->isAutoRouted = $is;
	}

	/**
	 *  Gets if the activity is auto routed 
	 *  @return boolean
	 */
	function isAutoRouted() {
		return $this->isAutoRouted == 'y';
	}

	/**
	 *  Sets if the activity is automaticly executed 
	 *  @return void
	 */
	function setIsAutomatic($is) {
		$this->isAutomatic = $is;
	}

	/** 
	 * Gets if the activity is automaticly executed 
	 *  @return boolean
	 */
	function isAutomatic() {
		return $this->isAutomatic == 'y';
	}

	/** 
	 * Sets if the activity is commented 
	 */
	function setIsComment($is) {
		$this->isComment = $is;
	}

	/** 
	 * Gets if the activity is commented 
	 *  @return boolean
	 */
	function isComment() {
		return $this->isComment == 'y';
	}

	/** 
	 * Sets the processId for this activity 
	 */
	function setProcessId($pid) {
		$this->pId = $pid;
	}

	/** 
	 * Gets the processId for this activity
	 *  @return integer
	 */
	function getProcessId() {
		return $this->pId;
	}

	/** 
	 * Gets the activityId 
	 *  @return integer
	 */
	function getActivityId() {
		return $this->activityId;
	}

	/** 
	 * Sets the activityId 
	 */
	function setActivityId($id) {
		$this->activityId = $id;
	}

	/** 
	 * Gets array with roleIds asociated to this activity 
	 *  @return array
	 */
	function getRoles() {
		return $this->roles;
	}

	/** 
	 * Sets roles for this activities, shoule receive an array of roleIds 
	 *  @param array
	 *  @return void
	 */
	function setRoles($roles) {
		$this->roles = $roles;
	}

	/**
	 * Checks if a user has a certain role (by name) for this activity,
	 * e.g. $isadmin = $activity->checkUserRole($user,'admin'); 
	 * 
	 *  @param string
	 *  @param string
	 *  @return integer
	 * 
	 */
	function checkUserRole($user, $rolename) {
		$aid = $this->activityId;
		return $this->getOne ( "select count(*) from `" . GALAXIA_TABLE_PREFIX . "activity_roles` gar, `" . GALAXIA_TABLE_PREFIX . "user_roles` gur, `" . GALAXIA_TABLE_PREFIX . "roles` gr where gar.`roleId`=gr.`roleId` and gur.`roleId`=gr.`roleId` and gar.`activityId`=? and gur.`user`=? and gr.`name`=?", array ($aid, $user, $rolename ) );
	}

}

