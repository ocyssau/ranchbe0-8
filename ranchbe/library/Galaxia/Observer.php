<?php


/**
 * Galaxia_Observer
 * An abstract class implementing observer objects.
 * 
 * Methods to override: notify($event, $msg)
 * This implements the Galaxia_Observer design pattern defining the Galaxia_Observer class.
 * Galaxia_Observer objects can be "attached" to Observable objects to listen for
 * a specific event.
 * Example:
 * <code>
 * $log = new Logger($logfile); //Logger extends Galaxia_Observer
 * $foo = new Foo(); //Foo extends Observable
 * $foo->attach('moo',$log); //Now $log observers 'moo' events in $foo class
 * // of
 * $foo->attach_all($log); // Same but all events are listened
 * </code>
 */
abstract class Galaxia_Observer {
	
	/**
	 * This will be assigned by an observable object when attaching.
	 * @var string
	 */
	public $_observerId = '';

	/**
	 * @param 	string	$event
	 * @param 	string	$msg
	 * @return 	void
	 */
	public function notify($event, $msg) {
		// do something...
	}
	
	
}

