<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+
/*
$input = array(
			'documents'=>array(
				docindex=>array(		//docindex is a integer | string
					'id'=>integer,
					'container'=>array('id'=>integer, space_name=>string),
					'properties'=>array(),
					'docfiles'=>array(
						0=>array(
							'index'=>dfindex,
							'role'=>0
						),
				),
			),
			'docfiles'=>array(
				dfindex=>array(			//dfindex is a integer | string
					'id'=>integer,
					'container'=>array('id'=>integer, space_name=>string),
					'properties'=>array(),
					'fsdata'=>array(),		//array of fsindex
				),
			),
			'fsdatas'=>array(
				fsindex=>array(			//fsindex is a integer | string
					'file_name'=>string,
					'md5'=>string,
					'content'=>base64,
				)
			),
);
*/
class RbService_Input{
}

class RbService_Output{
	/**
	 * 
	 * @var integer
	 */
	public $error_code;
	
	/**
	 * 
	 * @var string
	 */
	public $error_msg;
	
	/**
	 * 
	 * @var mixed
	 */
	public $index;
}


/** Superclass for all services
 * 
 * 
 * TODO Create type for input and output to replace array type. Array are not traductible for other language that PHP
 */
class RbService_Abstract{
	
	/**
	 * 
	 * @var boolean
	 */
	protected $_passed = false;

	/**
	 * 
	 * @var integer
	 */
	protected $_id;
	
	
	//---------------------------------------------------------------------
	/**
	 * Return the current user id, name and a flag to indicate that the user is authenticated
	 * 
	 * @return array
	 * 		array('id'=>integer, 'name'=>string, 'validate'=>boolean)
	 * 
	 */
	public function getCurrentUser() {
		return array(
			'id'=>Rb_User::getCurrentUser()->getId(),
			'name'=>Rb_User::getCurrentUser()->getName(),
			'validate'=>$this->_passed,
			'wildspace'=>Rb_User::getCurrentUser()->getWildspace()->getPath(),
			'prefs'=>Rb_User::getCurrentUser()->getPreference()->getUserPreferences()
		);
	} //End of method
	
    /**
     * Authenticate user from header parameters
     * 
     * 
     * @param string $username
     * @param string $password
	 * @return void
     */
    public function headerAuthentify( $username, $password ){
    	$myAdapter =& Ranchbe::getAuthAdapter ();
    	$myAdapter->setIdentity ( $username )
				  ->setPassword ( $password );
		$auth =& Zend_Auth::getInstance ();
		$result = $auth->authenticate ( $myAdapter );
		//Rb_User::setCurrentUser(null); //Why? Seem that CurrentUser is set in previous Ranchbe::getAuthAdapter function to anonymous???
		if ($result->isValid ()) {
			/*
			$_currentuser = new Rb_User( $myAdapter->getId () );
			$_currentuser->setUsername( $username );
			$_currentuser->setProperty( $myAdapter->getEmail () );
			Rb_User::setCurrentUser($_currentuser);
			*/
			
			/*Ce qui suit utilise les sessions, hors RbService n'utilise pas les sessions
			 * 
			*/
            $auth->setStorage(new Zend_Auth_Storage_NonPersistent());
			$auth->getStorage ()->write ( array (
									'user_id' => $myAdapter->getId (), 
									'user_name' => $username, 
									'email' => $myAdapter->getEmail (),
									'lastlogin' => time () )
			);
			
    		$this->_passed = true;
		} else {
    		$this->_passed = false;
			throw new SoapFault('Server', tra("Authenticate failed. You must provide a username and password in soap header. Consult RbService documentation.") );
		}
    } //End of method
    
    /**
     * 
     * @param integer $id
	 * @return void
     */
    public function headerIdentify( $id ){
    	$this->_id = $id;
    } //End of method
    
	/**
	 * @param integer
	 * @param string
	 * @param array
	 */
    /*
	public function __call( $id, $method, array $params = array() ){
		//$obj = new $class($id, $params['space_name']);
		//return $obj->$method;
	}
    */
	
    /**
     * Just for testing, return not interesting datas
     * 
     * 
	 * @return array
	 * 		array('info'=>string)
     */
    public function getServerInfo(){
		return array('info' => 'ok');
    }
    
    
    
    /** For test only
     * 
     * @return integer
     */
    /*
	public function login() {
		return session_id();
	}
	*/

    /** For test only
     * 
     * @return integer
     */
    /*
	public function incVar() {
		$this->var++;
		return $this->var;
	}
    */
    
	
} //End of class

