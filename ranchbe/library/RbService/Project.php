<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('RbService/Base.php');

/** Service to projects.
 * 
 * Projects are organization unit to the top level used to create specifics configuration.
 * 
 */
class RbService_Project extends RbService_Base{
	
	/**
	 * 
	 * @var Rb_Project
	 */
	protected $_project;
	
	/** Load object from his id
	 * 
	 * @param integer	id of object
	 * @return integer
	 */
	public function loadFromId($id){
		try{
			$this->_project = Rb_Project::get($id);
			$this->_proxied =& $this->_project;
			return $this->_project->getId();
		}catch(Exception $e){
			throw new SoapFault('Server', $e->getMessage());
		}
	}
	
	/** Init a new object
	 * 
	 * @return void
	 */
	public function init(){
		try{
			$this->_project = Rb_Project::get(0);
			$this->_proxied =& $this->_project;
		}catch(Exception $e){
			throw new SoapFault('Server', $e->getMessage());
		}
	}
	
} //End of class


