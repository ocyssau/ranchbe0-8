<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('RbService/Abstract.php');

/** Service of docfiles.
 * 
 * Docfiles are files attached to documents.
 * 
 */
class RbService_Docfile_List extends RbService_Abstract{

	//---------------------------------------------------------------------
	/** Check if file is recorded in database
	 * 	Return the file_id
	 * 
	 * @param string $file_name
	 * @param string $space_name
	 * @return integer
	 */
	public function fileExist($file_name, $space_name) {
		$file_id = Rb_Docfile::getFileId($space_name, $file_name);
		return $file_id;
	} //End of method
	
	/**
	 * 
	 * @param string $space_name
	 * @param array $filter			
	 * 	array(find|exact_find=>(array(AND|OR=>array('where'=>'what'))));
	 *  		find : use the passthrough %
	 *  		exact_find : to create a search on a plain value (a index key for example)
	 *  		AND : associate term by a AND opertor
	 *  		OR : associate terms by a OR opertor
	 *  		where : name of column to query
	 *  		what : value to search. Accept the passthrough %
	 * 
	 * @return array
	 */
	public function getList( $space_name, $filters ) {
		
		$docfile =& Rb_Docfile::get ( $space_name );

		if(!$docfile) {
			$errors = Ranchbe::getError()->getErrors(true);
			throw new SoapFault( $errors[0]['level'], $errors[0]['message'] );
		}
		
		/*
		$container = Rb_Container::get($space_name, $container_id);
		if(!$container) {
			throw new SoapFault(RBS_SERVER, "Container $container_id is not existing");
		}
		
		if (! Ranchbe::checkPerm ( 'document_get', $container, false )) {
			throw new SoapFault(RBS_SERVER, "You are not authorized to access to this request");
		}
		
		$search = new Rb_Search_Db ( $container->getDao () );
		$search->setSelect(array(
			'file_id', 
			'file_name',
			'file_access_code',
			'file_iteration',
			'file_version',
			'checkout_by',
			'uuser.handle As update_by',
			'couser.handle As check_out_by',
		));
		$search->setWith ( 'rb_users as uuser', 
									array (
										'col1' => 'update_by', 
										'col2' => 'auth_user_id' ), 
										'LEFT OUTER' );
		$search->setWith ( 'rb_users as couser', 
									array (
										'col1' => 'check_out_by', 
										'col2' => 'authuser_id' ), 
										'LEFT OUTER' );
		
		*/
		
		return $docfile->getAll( $filters );
	} //End of method
		
	
} //End of class
