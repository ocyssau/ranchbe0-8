<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('RbService/Base.php');

/** Service to containers.
 * 
 * Containers are basic elements to create a ranking
 * 
 */
class RbService_Reposit extends RbService_Base{
	
	/**
	 * 
	 * @var Rb_Container
	 */
	protected $_reposit;

	/** Init a new container
	 * 
	 * @param string
	 * @return void
	 */
	public function init($space_name){
		try{
			$this->_reposit = Rb_Reposit::get(0);
			$this->_proxied =& $this->_reposit;
		}catch(Exception $e){
			throw new SoapFault('Server', $e->getMessage());
		}
	}
	
	/** Get id of active reposit
	 * 
	 * @param string
	 * @return integer
	 */
	public function getActiveReposit($space_name){
		$space = Rb_Space::get($space_name);
		$ar = Rb_Reposit::getActiveReposit($space);
		if($ar){
			return $ar->getId();
		}else{
			return 0;
		}
	}
	
	
	/**
	 * 
	 * @param boolean
	 * @return void
	 */
	public function setActive($bool) {
		$this->_reposit->setActive($bool);
	}
	
	
	/** 
	 * 
	 * @param string
	 * @param string
	 * @return integer
	 */
	public function quickCreate($space_name, $name){
		$reposit = new Rb_Reposit (0);
		$reposit->setProperty('description', $name);
		$name = str_replace(' ', '_', $name);
		$name = str_replace('/', '', $name);
		$name = str_replace('\\', '', $name);
		$reposit->setNumber($name);
		$dir = uniqId();
		$reposit->setProperty('url', Ranchbe::getConfig()->path->reposit->workitem . '/' . $dir);
		
		$reposit->setProperty('space_id', Rb_Space::get($space_name)->getId() );
		$reposit->setProperty('type', Rb_Reposit::TYPE_REPOSIT);
		$reposit->setActive(true);
		$reposit->save();
		$this->_reposit = $reposit;
		$this->_proxied =& $this->_reposit;
		return $reposit->getId();
	}
	
	
} //End of class

