<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

/**
 * Input structure for all services :
 *
 $input = array(
	 'documents'=>array(
		 docindex=>array(		//docindex is a integer | string
			 'id'=>integer,
			 'container'=>array('id'=>integer, space_name=>string),
			 'properties'=>array(),
			 'docfiles'=>array(
				 0=>array(
					 'index'=>dfindex,
					 'role'=>0
				 ),
			 ),
		 ),
		 'docfiles'=>array(
			 dfindex=>array(			//dfindex is a integer | string
				 'id'=>integer,
				 'container'=>array('id'=>integer, space_name=>string),
				 'properties'=>array(),
				 'fsdata'=>array(),		//array of fsindex
			 ),
		 ),
		 'fsdatas'=>array(
			 fsindex=>array(			//fsindex is a integer | string
				 'file_name'=>string,
				 'md5'=>string,
				 'content'=>base64,
			 )
		 ),
 ));


 */

require_once('RbService/Abstract.php');
class RbService_Document_List extends RbService_Abstract{

	/**
	 *
	 * @var Rb_Wildspace
	 */
	private $_wildspace;

	//---------------------------------------------------------------------
	/**
	 * @return Rb_Wildspace
	 */
	protected function _getWildspace() {
		if(!$this->_wildspace){
			$this->_wildspace = Rb_User::getCurrentUser()->getWildspace();
			//$wpath = $this->_wildspace->getPath() .'/_rbGtkTemp/';
			//$this->_wildspace->setPath( $wpath );
		}
		return $this->_wildspace;
	} //End of method

	/**
	 * @param string $method name of method to call
	 * @param array $input
	 *		$input = array(
	 *					'documents'=>array(
	 *						(integer)=>array(
	 *							'id'=>(integer),
	 *							'container'=>array(space_name=>(string)),
	 *							'params'=>array('state'=>(string)),
	 *						),
	 *					),
	 *		);
	 * @param boolean
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>(integer),
	 *								'error_msg'=>(string),
	 *								'index'=>(mixed),
	 *								))
	 */
	protected function _proxy($method, $inputs, $save = false){
		$output = array();
		$i = 0;
		foreach ( $inputs['documents'] as $key=>$input ) {
			$i++;
			$output[$i]['index'] = $key;
			$output[$i]['error_code'] = 3;
			$output[$i]['error_msg'] = 'unknow error';
				
			$document_id = $input['id'];
			$space_name = $input['container']['space_name'];
				
			$document =& Rb_Document::get ( $space_name, $document_id );
			$output['documents'][$i]['return'] = call_user_func(array($document, $method), $input['params']);
			if($save){
				$ok = $document->save();
				if($ok){
					$output['documents'][$i]['error_code'] = 0;
					$output['documents'][$i]['error_msg'] = '';
				}else{
					$output['documents'][$i]['error_code'] = 1;
					$output['documents'][$i]['error_msg'] = Ranchbe::getError()->getErrors(true);
				}
			}else{
				$output['documents'][$i]['error_code'] = 0;
				$output['documents'][$i]['error_msg'] = '';
			}
		}

		/*
		 if (!Ranchbe::checkPerm ( 'document_suppress', $document, false )) {
			throw new SoapFault(RBS_SERVER, "You are not authorized to access to this request");
			}

			$output[$i]['document']['properties'] = $document->getProperties();
			foreach($document->getDocfiles() as $docfile){
			$output[$i]['document']['docfiles'][]['properties'] = $docfile->getProperties();
			}
			*/

		return $output;
	}//End of method


	/**
	 *
	 * @param array $input
	 *		$input = array(
	 *					'documents'=>array(
	 *						(integer)=>array(
	 *							'id'=>(integer),
	 *							'container'=>array(space_name=>(string)),
	 *							'params'=>array('state'=>(string)),
	 *						),
	 *					),
	 *		);
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>(integer),
	 *								'error_msg'=>(string),
	 *								'index'=>(mixed),
	 *								))
	 */
	function changeState(array $inputs){
		return $this->_proxy(__METHOD__, $inputs, true);
	}//End of method

	/**
	 *
	 * @param array $input
	 *		$input = array(
	 *					'documents'=>array(
	 *						(integer)=>array(
	 *							'id'=>(integer),
	 *							'container'=>array(space_name=>(string)),
	 *							'params'=>array('access_code'=>(string)),
	 *						),
	 *					),
	 *		);
	 *
	 * The value of the access code permit or deny action:<ul>
	 *  <li>0: Modification of Document is permit.</li>
	 *  <li>1: Modification of Document is in progress. The checkOut is deny. You can only do checkIn.</li>
	 *  <li>5: A workflow process is in progress. Modification is deny. You can only activate activities.</li>
	 *  <li>10: The Document is lock.You can only upgrade his indice.</li>
	 *  <li>11: Document is lock. You can only unlock it or upgrade his indice.</li>
	 *  <li>15: Indice is upgraded. You can only archive it.</li>
	 *  <li>20: The Document is archived.</li>
	 *</ul>
	 *
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>(integer),
	 *								'error_msg'=>(string),
	 *								'index'=>(mixed),
	 *								))
	 */
	function lockDocument(array $inputs){
		return $this->_proxy(__METHOD__, $inputs, true);
	}//End of method

	/**
	 *
	 * @param array $input
	 *		$input = array(
	 *					'documents'=>array(
	 *						(integer) docindex=>array(		//docindex is a integer | string
	 *							'id'=>integer,
	 *							'container'=>array(space_name=>string),
	 *						),
	 *					),
	 *		);
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>(integer),
	 *								'error_msg'=>(string),
	 *								'index'=>(mixed),
	 *								))
	 */
	function unlockDocument(array $inputs){
		return $this->_proxy(__METHOD__, $inputs, true);
	}//End of method


	//---------------------------------------------------------------------
	/**
	 * @param array $input
	 *		$input = array(
	 *					'documents'=>array(
	 *						docindex=>array(		//docindex is a integer | string
	 *							'id'=>integer,
	 *							'container'=>array(space_name=>string, id=>integer),
	 *							'properties'=>array(),
	 *						),
	 *					),
	 *		);
	 *
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>(integer),
	 *								'error_msg'=>(string),
	 *								'index'=>(mixed),
	 *								))
	 */
	public function edit(array $inputs) {
		$output = array();

		foreach ( $inputs['documents'] as $key=>$input ) {
			$isEdited = false;
				
			$output['documents'][$key] = array(
								'error_code'=>3, 
								'error_msg'=>'unknow error', 
								'index'=>$key);
				
			$space_name = $input['container']['space_name'];
			$container_id = (int) $input['container']['id'];
				
			$document_id = (int) $input['id'];
			$document =& Rb_Document::get($space_name, $document_id);
				
			if($document->description != $input['properties']['description']
			&& isset($input['properties']['description']) ){
				$document->description = $input['properties']['description'];
				$isEdited = true;
			}

			if($document->name != $input['properties']['document_name']
			&& isset($input['properties']['document_name']) ){
				$document->name = $input['properties']['document_name'];
				$isEdited = true;
			}
				
			if($document->category_id != $input['properties']['category_id']
			&& isset($input['properties']['category_id']) ){
				$document->category_id = $input['properties']['category_id'];
				$isEdited = true;
			}
				
				
			if($isEdited){
				$ok = $document->save();
			}else{
				$ok = true;
			}
			if($ok){
				$output['documents'][$key]['error_code'] = 0;
				$output['documents'][$key]['error_msg'] = $isEdited;
			}else{
				$output['documents'][$key]['error_code'] = 20;
				$output['documents'][$key]['error_msg'] = Ranchbe::getError()->getErrors(true);
			}
		} //End of foreach

		return $output;

	} //End of method

	//---------------------------------------------------------------------
	/**
	 * Suppress a list of document
	 * Return errors, error message, suppressed document properties and each suppressed docfiles properties in a array
	 *
	 * @param array	$inputs
	 * 				array(0=>array(	'document'=>array('id'=>integer),
	 * 								'space_name'=>string)
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>(integer),
	 *								'error_msg'=>(string),
	 *								'index'=>(mixed),
	 *								))
	 */
	public function suppress(array $inputs) {
		return $this->_proxy(__METHOD__, $inputs, false);
	} //End of method


	//---------------------------------------------------------------------
	/** Get list of document in container
	 *
	 * @param integer $container_id
	 * @param string $space_name	name of space to query
	 * @param array $filter
	 * 	array(find|exact_find=>(array(AND|OR=>array('where'=>'what'))));
	 *  		find : use the passthrough %
	 *  		exact_find : to create a search on a plain value (a index key for example)
	 *  		AND : associate term by a AND opertor
	 *  		OR : associate terms by a OR opertor
	 *  		where : name of column to query
	 *  		what : value to search. Accept the passthrough %
	 *
	 * @return array
	 * 				= array(0=>array('property_name'=>'property_value'))
	 *
	 */
	public function getList( $container_id, $space_name, $filter ) {

		$document = Rb_Document::get( $space_name );

		if(!$document) {
			$errors = Ranchbe::getError()->getErrors(true);
			throw new SoapFault( $errors[0]['level'], $errors[0]['message'] );
			//throw new SoapFault(RBS_SERVER, "Can not get document from space $space_name");
		}

		if($container_id > 0){
			$container = Rb_Container::get($space_name, $container_id);
			if(!$container) {
				throw new SoapFault(RBS_SERVER, "Container $container_id is not existing");
			}
	
			if (! Ranchbe::checkPerm ( 'document_get', $container, false )) {
				throw new SoapFault(RBS_SERVER, "You are not authorized to access to this request");
			}
		}

		$displayHistory = true;
		$search = new Rb_Search_Db ( $container->getDao () );
		$search->setSelect(array(
			'document_id', 
			'document_number',
			'description',
			'document_access_code',
			'document_iteration',
			'document_version',
			'check_out_by',
			'uuser.handle As update_by',
			'couser.handle As check_out_by',
		));
		$search->setWith ( 'rb_users as uuser',
		array (
										'col1' => 'update_by', 
										'col2' => 'auth_user_id' ), 
										'LEFT OUTER' );
		$search->setWith ( 'rb_users as couser',
		array (
										'col1' => 'check_out_by', 
										'col2' => 'auth_user_id' ), 
										'LEFT OUTER' );
			
		$params = $search->getParams();
		if($filter){
			$params = array_merge($params, $filter);
		}

		return $document->getDocuments( $params, $displayHistory, $container_id);
	} //End of method


	//---------------------------------------------------------------------
	/** Get father of a list of documents
	 *
	 * @param array $input
	 *		$input = array(
	 *					'documents'=>array(
	 *						docindex=>array(		//docindex is a integer | string
	 *							'id'=>integer,
	 *						),
	 *						'container'=>array(space_name=>string),
	 *					),
	 *		);
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>(integer),
	 *								'error_msg'=>(string),
	 *								'index'=>(mixed),
	 *								))
	 *
	 */
	public function getContainer( array $input ) {
		return $this->_proxy(__METHOD__, $inputs, false);
	} //End of method

	/** Return docfiles definition for each document of the input list.
	 *
	 * @param array $input
	 *		$input = array(
	 *					'documents'=>array(
	 *						(integer)=>array(
	 *							'id'=>(integer),
	 *							'container'=>array(space_name=>(string)),
	 *						),
	 *					),
	 *		);
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>(integer),
	 *								'error_msg'=>(string),
	 *								'index'=>(mixed),
	 *								'return'=>array(),
	 *								))
	 *
	 */
	function getDocfiles( array $input ){
		return $this->_proxy(__METHOD__, $inputs, false);
	}

	//---------------------------------------------------------------------
	/** Checkout a list of documents
	 *
	 * Note that if $input[documents][docindex][docfiles] is a empty array, then all
	 * docfiles of the document will be checkouted
	 *
	 * @param array		list of documents to checkout
	 *		$input = array(
	 *					'documents'=>array(
	 *						docindex=>array(		//docindex is a integer | string
	 *							'id'=>integer,
	 *							'container'=>array(space_name=>string),
	 *							'docfiles'=>array(
	 *								0=>array(
	 *									'index'=>dfindex, //id of docfile to checkout
	 *								),
	 *						),
	 *					'docfile'=>array(
	 *						dfindex=>array(		//docindex is a integer | string
	 *							'id'=>integer,
	 *							'properties'=>array('file_name'=>string),
	 *						),
	 *					),
	 *
	 * @return array
	 *		$output = array(
	 *				'docfiles'=>array(
	 *					dfindex=>array(			//dfindex is a integer | string
	 *						'id'=>integer,
	 *						'container'=>array('id'=>integer, space_name=>string),
	 *						'properties'=>array(),
	 *						'fsdata'=>fsindex
	 *					),
	 *				),
	 *				'fsdatas'=>array(
	 *					fsindex=>array(			//fsindex is a integer | string
	 *						'file_name'=>string,
	 *						'content'=>string,
	 *						'md5'=>string,
	 *					)
	 *				),
	 *	);
	 *
	 *
	 *
	 */
	public function checkout( array $inputs ) {
		$output = array();
		$i = 0;
		foreach ( $inputs['documents'] as $key=>$input ) {
			$i++;
			$output['documents'][$i] = array(
								'error_code'=>3, 
								'error_msg'=>'unknow error', 
								'index'=>$key);
				
			$space_name = $input['container']['space_name'];
			$document_id = $input['id'];
			$document = & Rb_Document::get ( $space_name, $document_id );
			$docfiles = $input['docfiles'];
				
			if (!Ranchbe::checkPerm ( 'document_edit', $document, false )) {
				$output['documents'][$i]['error_code'] = 9;
				$output['documents'][$i]['error_msg'] = "Bad permission: You are not authorized to process this request";
				continue;
			}
				
			if(!empty($input['docfiles'])){
				$withFiles = array();
				foreach($input['docfiles'] as $dfInput){
					$dfIndex = $dfInput['index'];
					$docfile_id = $inputs['docfiles'][$dfIndex]['id'];
					$withFiles[] = (int) $docfile_id;
				}
			}else{
				$withFiles = true;
			}
			$ok = $document->checkOut ( true , 'replace', false, $withFiles);
			if(!$ok){
				$output['documents'][$i]['error_code'] = 13;
				$output['documents'][$i]['error_msg'] = Ranchbe::getError()->getErrors(false);
				continue;
			}
				
			$docfiles = $document->getDocfiles ();
			if ( is_array ( $docfiles ) ){
				foreach ( $docfiles as &$docfile ) {
					if (!$docfile->getFsdata()){
						continue;
					}
					if( $docfile->checkAccess() !== 1 ){
						continue;
					}
					$output['docfiles'][$docfile->getId()] = array(
						'id'=>$docfile->getId(),
						'properties'=>$docfile->getProperties(),
						'container'=>array(
								'space_name'=>$docfile->getSpace()->getName()
					),
						'fsdata'=>$docfile->getId() //the key in $output['fsdatas'] array
					);
					$output['fsdatas'][$docfile->getId()] = array(
						'content' => base64_encode(file_get_contents($docfile->file)),
						'md5' => $docfile->file_md5,
						'file_name' => $docfile->file_name,
					);
				} //End of foreach
			}
		} //End of foreach

		return $output;

	} //End of method

	//---------------------------------------------------------------------
	/** Cancel checkout a list of documents
	 *
	 * @param array	$inputs
	 * 				array(0=>array(	'document'=>array('id'=>integer),
	 * 								'space_name'=>string)
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>(integer),
	 *								'error_msg'=>(string),
	 *								'index'=>(mixed),
	 *								))
	 */
	public function cancelCheckout( array $inputs ) {
		$output = array();
		$i = -1;
		foreach ( $inputs as $key=>$input ) {
			$i++;
			$output[$i]['index'] = $key;
			$output[$i]['error_code'] = 3;
			$output[$i]['error_msg'] = 'unknow error';
				
			$document_id = $input['document']['id'];
			$space_name = $input['space_name'];
				
			$document = & Rb_Document::get ( $space_name, $document_id );
			$ok = $document->cancelCheckOut ();
			if($ok){
				$output[$i]['error_code'] = 0;
				$output[$i]['error_msg'] = '';
			}else{
				$output[$i]['error_code'] = 1;
				$output[$i]['error_msg'] = Ranchbe::getError()->getErrors(true);
			}
		}

		return $output;
	} //End of method


	//---------------------------------------------------------------------
	/** Get documents and docfiles from files list
	 * 	The index of the input array is same for index of array $output['fsdatas']
	 *  Return the docfile properties if a docfile is associated to the file_name
	 *  Return the document properties of document parent of docfile
	 *  	or if the file must be associated to the document.
	 *  A document is to be associate to a file_name
	 *  	if the file_name string is a substring of document number.
	 *
	 *
	 * @param	array	$inputs
	 *	$inputs = array(
	 *			0=>array(
	 *				'file_name'=>string,
	 *				'space_name'=>string
	 *			),
	 *	);
	 *
	 * @return array	$output
	 *	$output = array(
	 *				'documents'=>array(
	 *					0=>array(
	 *						'id'=>integer,
	 *						'container'=>array('id'=>integer, space_name=>string),
	 *						'properties'=>array(),
	 *						'docfiles'=>array(
	 *							0=>array(
	 *								'index'=>dfindex,
	 *								'role'=>0
	 *							),
	 *					),
	 *				),
	 *				'docfiles'=>array(
	 *					0=>array(
	 *						'id'=>integer,
	 *						'container'=>array('id'=>integer, space_name=>string),
	 *						'properties'=>array(),
	 *						'fsdata'=>string //fsindex
	 *					),
	 *				),
	 *				'fsdatas'=>array(
	 *					0=>array(
	 *						'file_name'=>string,
	 *						'docfile'=>dfindex,
	 *						'document'=>docindex,
	 *						'todo'=>string,
	 *					)
	 *				),
	 *	);
	 *
	 * todo is a enum of string :
	 * 			create  : need to create a new document
	 * 			iterate : need to iterate a existing document
	 * 			associate : need to be associate to a existing document
	 *
	 */
	public function getFromFiles($inputs) {
		$tmp_list = array();
		$output = array();

		foreach ( $inputs as $input ) {
			$space_name = $input['space_name'];
			$file_name = basename( $input['file_name'] );
			$output['fsdatas'][$file_name]['file_name'] = $file_name;
			$document_number = Rb_File::sGetRoot($file_name);
			
			//---------- Check if file is recorded in database and try to init document and docfile
			$file_id = Rb_Docfile::getFileId($space_name, $file_name);
				
			//---------- Check if document is recorded in database and try to init document and docfile
			if ($file_id < 1){
				$document_id = Rb_Document::get($space_name)->ifExist( $document_number );
			}
			
			//---------- if is not a docfile or document init a new document and a new docfile
			//---------- Check the doctype for the new document
			if($file_id > 0){
				$docfile = Rb_Docfile::get($space_name, $file_id);
				$document =& $docfile->getFather();
				$document_id = $document->getid();
				$output['fsdatas'][$file_name]['todo'] = 'iterate';
				$output['fsdatas'][$file_name]['docfile'] = $file_id;
				$output['fsdatas'][$file_name]['document'] = $document->getId();
			}else if($document_id > 0){
				$docfile = false;
				$document = Rb_Document::get($space_name, $document_id);
				$document_id = $document->getid();
				$output['fsdatas'][$file_name]['todo'] = 'associate';
				$output['fsdatas'][$file_name]['docfile'] = false;
				$output['fsdatas'][$file_name]['document'] = $document_id;
			}else{
				$docfile = false;
				$document = false;
				$output['fsdatas'][$file_name]['todo'] = 'create';
				$output['fsdatas'][$file_name]['docfile'] = false;
				$output['fsdatas'][$file_name]['document'] = false;
			}
			
			if($document && !$output['documents'][$document_id]) {
				$container =& $document->getContainer();
				$output['documents'][$document_id] = array(
											'id'=>$document->getId(),
											'container'=>array('id'=>$container->getId(), 
															   'space_name'=>$space_name ),
											'properties'=>$document->getProperties(),
				);
			}
			if($docfile && !$output['documents'][$document_id]['docfiles'][$docfile->getId()]) {
				$roles = Rb_Docfile_Role::getRoles($docfile);
				$output['documents'][$document_id]['docfiles'][$docfile->getId()] = array(
									'index'=>$docfile->getId(),
									'role'=>$roles,
				);
			}
			if($docfile && !$output['docfiles'][$docfile->getId()]) {
				$output['docfiles'][$docfile->getId()] = array(
									'id'=>$docfile->getId(),
									'properties'=>$docfile->getProperties(),
									'fsdata'=>$file_name,
				);
			}
				
		} //End of foreach

		return $output;

	} //End of method

	
	//---------------------------------------------------------------------
	/** Get documents and docfiles from document number
	 *
	 * @param	array	$inputs
	 *	$inputs = array(
	 *			0=>array(
	 *				'number'=>string,
	 *				'space_name'=>string
	 *			),
	 *	);
	 *
	 * @return array	$output
	 *	$output = array(
	 *				'documents'=>array(
	 *					0=>array(
	 *						'id'=>integer,
	 *						'container'=>array('id'=>integer, space_name=>string),
	 *						'properties'=>array(),
	 *						'docfiles'=>array(
	 *							0=>array(
	 *								'index'=>dfindex,
	 *								'role'=>0
	 *							),
	 *					),
	 *				),
	 *				'docfiles'=>array(
	 *					0=>array(
	 *						'id'=>integer,
	 *						'container'=>array('id'=>integer, space_name=>string),
	 *						'properties'=>array(),
	 *						'fsdata'=>string //fsindex
	 *					),
	 *				),
	 *				'fsdatas'=>array(
	 *					0=>array(
	 *						'file_name'=>string,
	 *						'docfile'=>dfindex,
	 *						'document'=>docindex,
	 *						'todo'=>string,
	 *					)
	 *				),
	 *	);
	 *
	 *
	 */
	public function getFromNumbers($inputs) {
		$tmp_list = array();
		$output = array();
		
		foreach ( $inputs as $input ) {
			$space_name = $input['space_name'];
			$document_number = $input['number'];
			
			//---------- Check if document is recorded in database and try to init document and docfile
			$document_id = Rb_Document::get($space_name)->ifExist( $document_number );
				
			if($document_id > 0) {
				$document = Rb_Document::get($space_name, $document_id);
				$container =& $document->getContainer();
				$output['documents'][$document_id] = array(
											'id'=>$document->getId(),
											'container'=>array('id'=>$container->getId(), 
															   'space_name'=>$space_name ),
											'properties'=>$document->getProperties(),
				);
			}
		} //End of foreach
		
		return $output;
	} //End of method
	
	
	//---------------------------------------------------------------------
	/** Create documents from a list a files
	 *
	 * @param array $input
	 *		$input = array(
	 *					'documents'=>array(
	 *						docindex=>array(		//docindex is a integer | string
	 *							'id'=>integer,
	 *							'container'=>array('id'=>integer, space_name=>string),
	 *							'properties'=>array('document_number'=>string, 'document_name'=>string),
	 *							'docfiles'=>array(
	 *								0=>array(
	 *									'index'=>dfindex,
	 *									'role'=>0
	 *								),
	 *						),
	 *					),
	 *					'docfiles'=>array(
	 *						dfindex=>array(			//dfindex is a integer | string
	 *							'id'=>integer,
	 *							'container'=>array('id'=>integer, space_name=>string),
	 *							'properties'=>array('file_name'=>string),
	 *							'fsdata'=>string, //fsindex
	 *						),
	 *					),
	 *					'fsdatas'=>array(
	 *						fsindex=>array(			//fsindex is a integer | string
	 *							'file_name'=>string,
	 *							'md5'=>string,
	 *							'content'=>base64,
	 *						)
	 *					),
	 *		);
	 * @param boolean	if true, keep document checkouted
	 * @param boolean	if true, suppress file of wildspace after store
	 *
	 * @return array
	 * 			array('docfiles'=>
	 *				array(0=>array( 'error_code'=>error_code,
	 *								'error_msg'=>string,
	 *								'index'=>$key)),
	 *				'documents'=>
	 *				array(0=>array( 'error_code'=>error_code,
	 *								'error_msg'=>string,
	 *								'index'=>$key))
	 *
	 * error_code is a enum of integer :
	 * 			0 : no error
	 * 			1 : internal error to Rb library
	 * 			2 : error in service
	 * 			3 : warning in service
	 *
	 *
	 */
	public function create($inputs, $keepco = false, $suppressAfterStore = true) {
		$tmp_list = array(); //local index to detect doublons in documents to create list
		$output = array();

		$wildspace =& $this->_getWildspace();

		$i=0;
		foreach ( $inputs['documents'] as $key=>$input ) {
			$i++;
			//init output
			$output['documents'][$i] = array(
								'error_code'=>3, 
								'error_msg'=>'unknow error', 
								'index'=>$key);
			
			foreach($input['docfiles'] as $dfInput){
				$output['docfiles'][$dfInput['index']] = array(
					'error_code'=>&$output['documents'][$i]['error_code'], 
					'error_msg'=>&$output['documents'][$i]['error_msg'], 
					'index'=>$dfInput['index']);
			}
				
			$space_name = $input['container']['space_name'];
			$container_id = (int) $input['container']['id'];
			$docProperties = $input['properties'];
			$category_id = $docProperties['category_id'];
				
			$container = Rb_Container::get($space_name, $container_id);
			if(!$container || $container->getId() == 0) {
				$output['documents'][$i]['error_code'] = 11;
				$output['documents'][$i]['error_msg'] = "Container $container_id is not existing";
				continue;
			}
				
			foreach($input['docfiles'] as $dfInput){
				if($dfInput['role'] == 0){
					$maindfIndex = $dfInput['index'];
					break;
				}
			}
			$mainfile_name = basename($inputs['docfiles'][$maindfIndex]['properties']['file_name']);
			$document_number = trim($docProperties['document_number']);
			
			if(!$document_number){
				$document_number = Rb_File::sGetRoot( $mainfile_name );
			}
			if(!$document_number){
				$output['documents'][$i]['error_code'] = 10;
				$output['documents'][$i]['error_msg'] = "None number is set for this document";
				continue;
			}
			
			//---------- Check double documents
			if (in_array ( $document_number, $tmp_list )) {
				$output[$i]['error_code'] = 100;
				$output[$i]['error_msg'] = "Document number $document_number is not unique in this package";
				continue;
			} else {
				$tmp_list [] = $document_number;
			}
				
			//---------- Check if document is recorded in database and try to init document and docfile
			/*
			 $document_id = Rb_Document::get($space_name)->ifExist( $document_number );
			 if( $document_id > 0 ){
				$output['documents'][$i]['error_code'] = 12;
				$output['documents'][$i]['error_msg'] = 'This document is yet recorded';
				continue;
				}
				*/
			
			//---------- Create document
			if($mainfile_name){
				$file_extension = Rb_File::sGetExtension($mainfile_name);
				$file_type = 'file';
			}else{
				$file_extension = '';
				$file_type = 'nofile';
			}
			
			//$space = Rb_Space::get($space_name);
			$document = new Rb_Document( Rb_Space::get($space_name), 0 );
			foreach($input['properties'] as $pname=>$pvalue){
				$document->$pname = $pvalue;
			}
			$document->setContainer( $container );
			$document->setNumber($document_number);
			$doctype = $document->setDocType($file_extension, $file_type);
			//---------- Set the category
			if($category_id){
				$document->setProperty( 'category_id', $category_id );
			}else if(is_a($doctype, 'Rb_Doctype')){
				$categories = Rb_Container_Relation_Doctype_Category::get()
								->getCategories( $container_id, $doctype->getId() );
				$document->setProperty('category_id', $categories[0]['category_id']);
			}
			
			$ok = $document->store(false, false, $keepco);
			if(!$ok){
				$output['documents'][$i]['error_code'] = 15;
				$output['documents'][$i]['error_msg'] = Ranchbe::getError()->getErrors(true);
				continue;
			}else{
				$output['documents'][$i]['error_code'] = 0;
				$output['documents'][$i]['error_msg'] = Ranchbe::getError()->getErrors(true);
			}
			
			//---------- Create docfiles
			$output['docfiles'] = array();
			foreach($input['docfiles'] as $dfLink){
				//init output
				$output['docfiles'][$dfLink['index']] = array(
						'error_code'=>3, 
						'error_msg'=>'', 
						'index'=>$dfLink['index']);

				$dfInput = $inputs['docfiles'][$dfLink['index']];
				$fsInput = $inputs['fsdatas'][$dfInput['fsdata']];
				//Put datas in a temp file
				$file_name = $dfInput['properties']['file_name'];
				$fullPath = $wildspace->getPath() .'/'. $file_name;
				file_put_contents($fullPath, base64_decode($fsInput['content']));
				$fsdata = new Rb_Fsdata($fullPath);
				//Check md5 for validate transmission
				if($fsdata->getProperty('file_md5') != $fsInput['md5']){
					$output['docfiles'][$dfLink['index']]['error_code'] = 13;
					$output['docfiles'][$dfLink['index']]['error_msg'] = "Transmission error on file $file_name";
					continue;
				}
				$role = $dfLink['role'];
				$docfile = Rb_Docfile::get($space_name, 0);
				$docfile->setFsdata($fsdata);
				$document->setDocfile($docfile, $role);
				$ok = $docfile->storeFile($suppressAfterStore, $keepco);
				if(!$ok){
					$output['docfiles'][$dfLink['index']]['error_code'] = 16;
					$output['docfiles'][$dfLink['index']]['error_msg'] = Ranchbe::getError()->getErrors(true);
				}else{
					$output['docfiles'][$dfLink['index']]['error_code'] = 0;
					$output['docfiles'][$dfLink['index']]['error_msg'] = '';
				}
			}
		} //End of foreach

		return $output;

	} //End of method

	//---------------------------------------------------------------------
	/** Associate a list of files to a existing document
	 *
	 * Return a array with error_code, error_msg and index.
	 * Index is the key of the input array concerned by this output
	 *
	 *
	 * @param array $input
	 *		$input = array(
	 *					'documents'=>array(
	 *						docindex=>array(		//docindex is a integer | string
	 *							'id'=>integer,
	 *							'container'=>array('id'=>integer, space_name=>string),
	 *							'properties'=>array('document_number'=>string, 'document_name'=>string),
	 *							'docfiles'=>array(
	 *								0=>array(
	 *									'index'=>dfindex,
	 *									'role'=>0
	 *								),
	 *						),
	 *					),
	 *					'docfiles'=>array(
	 *						dfindex=>array(			//dfindex is a integer | string
	 *							'id'=>integer,
	 *							'container'=>array('id'=>integer, space_name=>string),
	 *							'properties'=>array('file_name'=>string),
	 *							'fsdata'=>string, //fsindex
	 *						),
	 *					),
	 *					'fsdatas'=>array(
	 *						fsindex=>array(			//fsindex is a integer | string
	 *							'file_name'=>string,
	 *							'md5'=>string,
	 *							'content'=>base64,
	 *						)
	 *					),
	 *		);
	 * @param boolean	if true, keep document checkouted
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>error_code,
	 *								'error_msg'=>string,
	 *								'index'=>$key))
	 *
	 */
	public function associate($inputs, $keepco, $suppressAfterStore = true) {
		$tmp_list = array();
		$output = array();
		
		$wildspace =& $this->_getWildspace();
		
		$i=0;
		foreach ( $inputs['documents'] as $key=>$input ) {
			$i++;
			$output['documents'][$i] = array(
								'error_code'=>3, 
								'error_msg'=>'unknow error', 
								'index'=>$key);
			foreach($input['docfiles'] as $dfInput){
				$output['docfiles'][$dfInput['index']] = array(
					'error_code'=>&$output['documents'][$i]['error_code'], 
					'error_msg'=>&$output['documents'][$i]['error_msg'], 
					'index'=>$dfInput['index']);
			}
			
			$space_name = $input['container']['space_name'];
			$container_id = (int) $input['container']['id'];
			
			$document_id = (int) $input['id'];
			$document =& Rb_Document::get($space_name, $document_id);
			
			foreach($input['docfiles'] as $dfLink){
				//init output
				$output['docfiles'][$dfLink['index']] = array(
						'error_code'=>3, 
						'error_msg'=>'', 
						'index'=>$dfLink['index']);
				
				$dfInput = $inputs['docfiles'][$dfLink['index']];
				$fsInput = $inputs['fsdatas'][$dfInput['fsdata']];
				//Put datas in a temp file
				$file_name = $dfInput['properties']['file_name'];
				$fullPath = $wildspace->getPath() .'/'. $file_name;
				file_put_contents($fullPath, base64_decode($fsInput['content']));
				$fsdata = new Rb_Fsdata($fullPath);
				//Check md5 for validate transmission
				if($fsdata->getProperty('file_md5') != $fsInput['md5']){
					$output['docfiles'][$dfLink['index']]['error_code'] = 13;
					$output['docfiles'][$dfLink['index']]['error_msg'] = "Transmission error on file $file_name";
				}else{
					$output['docfiles'][$dfLink['index']]['error_code'] = 0;
					$output['docfiles'][$dfLink['index']]['error_msg'] = '';
				}
				
				$role = $dfLink['role'];
				$docfile = Rb_Docfile::get($space_name, 0);
				$docfile->setFsdata($fsdata);
				$document->setDocfile($docfile, $role);
				$ok = $docfile->storeFile($suppressAfterStore, $keepco);
				if(!$ok){
					$output['docfiles'][$dfLink['index']]['error_code'] = 16;
					$output['docfiles'][$dfLink['index']]['error_msg'] = Ranchbe::getError()->getErrors(true);
				}else{
					$output['docfiles'][$dfLink['index']]['error_code'] = 0;
					$output['docfiles'][$dfLink['index']]['error_msg'] = '';
				}
			} //End of foreach
		} //End of foreach
		
		return $output;
		
	} //End of method


	//---------------------------------------------------------------------
	/** Checkin a list a files
	 *
	 * Return a array with error_code, error_msg and index.
	 * Index is the key of the input array concerned by this output
	 *
	 * @param array $input
	 *		$input = array(
	 *					'documents'=>array(
	 *						docindex=>array(		//docindex is a integer | string
	 *							'id'=>integer,
	 *							'container'=>array('id'=>integer, space_name=>string),
	 *							'properties'=>array('document_number'=>string, 'document_name'=>string),
	 *							'docfiles'=>array(
	 *								0=>array(
	 *									'index'=>dfindex,
	 *									'role'=>0
	 *								),
	 *						),
	 *					),
	 *					'docfiles'=>array(
	 *						dfindex=>array(			//dfindex is a integer | string
	 *							'id'=>integer,
	 *							'container'=>array('id'=>integer, space_name=>string),
	 *							'properties'=>array('file_name'=>string),
	 *							'fsdata'=>string, //fsindex
	 *						),
	 *					),
	 *					'fsdatas'=>array(
	 *						fsindex=>array(			//fsindex is a integer | string
	 *							'file_name'=>string,
	 *							'md5'=>string,
	 *							'content'=>base64,
	 *						)
	 *					),
	 *		);
	 * @param boolean	if true, keep document checkouted
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>error_code,
	 *								'error_msg'=>string,
	 *								'index'=>$key))
	 *
	 *
	 * error_code is a enum of integer :
	 * 			0 : no error
	 * 			1 : internal error to Rb library
	 * 			2 : error in service
	 *
	 */
	public function checkin(array $inputs, $keepco = false) {
		$tmp_list = array();
		$output = array();

		$wildspace =& $this->_getWildspace();

		$i=0;
		foreach ( $inputs['documents'] as $key=>$input ) {
			$i++;
			$output['documents'][$i] = array(
								'error_code'=>3, 
								'error_msg'=>'unknow error', 
								'index'=>$key);
			foreach($input['docfiles'] as $dfInput){
				$output['docfiles'][$dfInput['index']] = array(
					'error_code'=>&$output['documents'][$i]['error_code'], 
					'error_msg'=>&$output['documents'][$i]['error_msg'], 
					'index'=>$dfInput['index']);
			}
				
			$space_name = $input['container']['space_name'];
			$container_id = (int) $input['container']['id'];
				
			$document_id = (int) $input['id'];
			$document =& Rb_Document::get($space_name, $document_id);
			$description = $input['properties']['description'];
			if($description){
				$document->setProperty('description', $description);
			}
				
			$withFiles = array();
			$output['docfiles'] = array();
			$iterate = false;
			foreach($input['docfiles'] as $dfLink){
				//init output
				$output['docfiles'][$dfLink['index']] = array(
						'error_code'=>3, 
						'error_msg'=>'', 
						'index'=>$dfLink['index']);

				$dfInput = $inputs['docfiles'][$dfLink['index']];
				$fsInput = $inputs['fsdatas'][$dfInput['fsdata']];
				//Put datas in a temp file
				$file_name = $dfInput['properties']['file_name'];
				$fullPath = $wildspace->getPath() .'/'. $file_name;
				file_put_contents($fullPath, base64_decode($fsInput['content']));
				$fsdata = new Rb_Fsdata($fullPath);
				//Check md5 for validate transmission
				if($fsdata->getProperty('file_md5') != $fsInput['md5']){
					$output['docfiles'][$dfLink['index']]['error_code'] = 13;
					$output['docfiles'][$dfLink['index']]['error_msg'] = "Transmission error on file $file_name";
					continue;
				}
				$withFiles[] = $dfInput['id'];
				$docfile =& Rb_Docfile::get($space_name, $dfInput['id']);
				$dfIteration = $docfile->checkInFile(!$keepco, true, true);
				if($dfIteration){
					$output['docfiles'][$dfLink['index']]['error_code'] = 0;
					$output['docfiles'][$dfLink['index']]['error_msg'] = '';
					if($dfIteration->iteration != $docfile->iteration){
						$iterate = true;
					}
				}else{
					$output['docfiles'][$dfLink['index']]['error_code'] = 15;
					$output['docfiles'][$dfLink['index']]['error_msg'] = Ranchbe::getError()->getErrors(true);
				}
			} //End of foreach
				
			//if at least one file has change, increment iteration
			if($iterate){
				$document->setProperty('iteration_id', $document->getProperty('iteration_id')+1);
			}
				
			if($keepco){
				$ok = $document->checkInAndKeep($withFiles);
			}else{
				$ok = $document->checkInAndRelease($withFiles);
			}
				
			if($ok){
				$output['documents'][$i] = array('error_code'=>0,
									'error_msg'=>'', 
									'debug'=>$withFiles, 
									'index'=>$key);
			}else{
				$output['documents'][$i] = array('error_code'=>100,
									'error_msg'=>Ranchbe::getError()->getErrors(true),
									'index'=>$key);
			}
		} //End of foreach

		return $output;

	} //End of method


	//---------------------------------------------------------------------
	/** Checkout and checkin document of a list of file
	 *
	 * Return a array with error_code, error_msg, docfile id, document id and
	 * index. Index is the key of the input array concerned by this output
	 *
	 * @param array $input
	 *		$input = array(
	 *					'documents'=>array(
	 *						docindex=>array(		//docindex is a integer | string
	 *							'id'=>integer,
	 *							'container'=>array('id'=>integer, space_name=>string),
	 *							'properties'=>array('document_number'=>string, 'document_name'=>string),
	 *							'docfiles'=>array(
	 *								0=>array(
	 *									'index'=>dfindex,
	 *									'role'=>0
	 *								),
	 *						),
	 *					),
	 *					'docfiles'=>array(
	 *						dfindex=>array(			//dfindex is a integer | string
	 *							'id'=>integer,
	 *							'container'=>array('id'=>integer, space_name=>string),
	 *							'properties'=>array('file_name'=>string),
	 *							'fsdata'=>string, //fsindex
	 *						),
	 *					),
	 *					'fsdatas'=>array(
	 *						fsindex=>array(			//fsindex is a integer | string
	 *							'file_name'=>string,
	 *							'md5'=>string,
	 *							'content'=>base64,
	 *						)
	 *					),
	 *		);
	 * @param boolean	if true, keep document checkouted
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>error_code,
	 *								'error_msg'=>string,
	 *								'index'=>$key))
	 *
	 *
	 *
	 * error_code is a enum of integer :
	 * 			0 : no error
	 * 			3 : unknow error
	 * 			9 : Bad permissions
	 * 			12 : Error when checkout document and files
	 * 			13 : Error on file transfert
	 * 			15 : Error when checkin docfile
	 * 			20 : Error when checkin document
	 *
	 */
	public function checkoutin( array $inputs, $keepco = false) {
		$tmp_list = array();
		$output = array();

		$wildspace =& $this->_getWildspace();

		$i = 0;
		foreach ( $inputs['documents'] as $key=>$input ) {
			$i++;
			$output['documents'][$i] = array(
								'error_code'=>3, 
								'error_msg'=>'unknow error', 
								'index'=>$key);
			foreach($input['docfiles'] as $dfInput){
				$output['docfiles'][$dfInput['index']] = array(
					'error_code'=>&$output['documents'][$i]['error_code'], 
					'error_msg'=>&$output['documents'][$i]['error_msg'], 
					'index'=>$dfInput['index']);
			}
			Ranchbe::getError()->flush();
				
			$space_name = $input['container']['space_name'];
			$container_id = (int) $input['container']['id'];
				
			$document_id = (int) $input['id'];
			$document =& Rb_Document::get($space_name, $document_id);
				
			$description = $input['properties']['description'];
			if($description){
				$document->setProperty('description', $description);
			}
				
			//===============================================================
			//checkout file and document
			//===============================================================
			if (!Ranchbe::checkPerm ( 'document_edit', $document, false )) {
				$output['documents'][$i]['error_code'] = 9;
				$output['documents'][$i]['error_msg'] = "Bad permission: You are not authorized to process this request";
				continue;
			}
				
			$withFiles = array();
			foreach($input['docfiles'] as $dfInput){
				$withFiles[] = (int) $dfInput['id'];
			}
			$ok = $document->checkOut ( true , 'replace', false, $withFiles);
			if(!$ok){
				$output['documents'][$i]['error_code'] = 12;
				$output['documents'][$i]['error_msg'] = Ranchbe::getError()->getErrors(false);
				continue;
			}
				
			//===============================================================
			//checkin file and document
			//===============================================================
			$withFiles = array();
			$iterate = false;
			foreach($input['docfiles'] as $dfLink){
				//init output
				$output['docfiles'][$dfLink['index']] = array(
						'error_code'=>3, 
						'error_msg'=>'', 
						'index'=>$dfLink['index']);

				$dfInput = $inputs['docfiles'][$dfLink['index']];
				$fsInput = $inputs['fsdatas'][$dfInput['fsdata']];
				//Put datas in a temp file
				$file_name = $dfInput['properties']['file_name'];
				$fullPath = $wildspace->getPath() .'/'. $file_name;
				file_put_contents($fullPath, base64_decode($fsInput['content']));
				$fsdata = new Rb_Fsdata($fullPath);
				//Check md5 for validate transmission
				if($fsdata->getProperty('file_md5') != $fsInput['md5']){
					$output['docfiles'][$dfLink['index']]['error_code'] = 13;
					$output['docfiles'][$dfLink['index']]['error_msg'] = "Transmission error on file $file_name";
					continue;
				}
				$withFiles[] = $dfInput['id'];
				$docfile =& Rb_Docfile::get($space_name, $dfInput['id']);
				$dfIteration = $docfile->checkInFile(!$keepco, true, true);
				if($dfIteration){
					$output['docfiles'][$dfLink['index']]['error_code'] = 0;
					$output['docfiles'][$dfLink['index']]['error_msg'] = '';
					if($dfIteration->iteration != $docfile->iteration){
						$iterate = true;
					}
				}else{
					$output['docfiles'][$dfLink['index']]['error_code'] = 15;
					$output['docfiles'][$dfLink['index']]['error_msg'] = Ranchbe::getError()->getErrors(true);
				}

			} //End of foreach
				
			//if at least one file has change, increment iteration
			if($iterate){
				$document->setProperty('iteration_id', $document->getProperty('iteration_id')+1);
			}

			if($keepco){
				$ok = $document->checkInAndKeep($withFiles);
			}else{
				$ok = $document->checkInAndRelease($withFiles);
			}
				
			if($ok){
				$output['documents'][$i] = array('error_code'=>0,
									'error_msg'=>'', 
									'debug'=>$withFiles, 
									'index'=>$key);
			}else{
				$output['documents'][$i] = array('error_code'=>20,
									'error_msg'=>Ranchbe::getError()->getErrors(true),
									'index'=>$key);
			}
		}//End of foreach

		return $output;

	} //End of method

} //End of class
