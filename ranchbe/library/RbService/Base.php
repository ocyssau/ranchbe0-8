<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

/**
 * Input structure for all services :
 *
 $input = array(
	 'documents'=>array(
		 docindex=>array(		//docindex is a integer | string
			 'id'=>integer,
			 'container'=>array('id'=>integer, space_name=>string),
			 'properties'=>array(),
			 'docfiles'=>array(
				 0=>array(
					 'index'=>dfindex,
					 'role'=>0
				 ),
			 ),
		 ),
		 'docfiles'=>array(
			 dfindex=>array(			//dfindex is a integer | string
				 'id'=>integer,
				 'container'=>array('id'=>integer, space_name=>string),
				 'properties'=>array(),
				 'fsdata'=>array(),		//array of fsindex
			 ),
		 ),
		 'fsdatas'=>array(
			 fsindex=>array(			//fsindex is a integer | string
				 'file_name'=>string,
				 'md5'=>string,
				 'content'=>base64,
			 )
		 ),
 ));
 */

require_once('RbService/Abstract.php');

/*
require_once('Rb/Acl/Resource/Interface.php');
require_once('Rb/Observable/Interface.php');
require_once('Rb/Object/Root.php');
require_once('Rb/Observable.php');
require_once('Rb/Document.php');
require_once('Rb/Acl/Resource/Interface.php');
require_once('Rb/Object/Interface/Related.php');
require_once('Rb/Object/Permanent.php');
require_once('Rb/Object/Acl.php');
require_once('Rb/Space.php');
*/
require_once('Rb/Space.php');
//require_once('Rb/Document.php');

/** 
 * Abstract base class for all proxies class to ranchbe objects
 * 
 * Because SOAP use serialize to create permanence of object, the ranchbe class
 * must implement a wakeup method to recreate stream to db connexion (and others stream if needs)
 * Dont forget : a stream can never be unserialize
 * 
 */
class RbService_Base extends RbService_Abstract{
	
	/**
	 * 
	 * @var Rb_Object_Permanent
	 */
	protected $_proxied;
	
	/**
	 * 
	 * @return integer
	 */
	public function getId(){
		return $this->_proxied->getId();
	}
	
	//----------------------------------------------------------
	/**
	 * 
	 * @param string $name
	 * @return string
	 */
	public function getProperty($name){
		return $this->_proxied->getProperty($name);
	}//End of method
	
	//----------------------------------------------------------
	/**
	 * 
	 * @return array
	 */
	public function getProperties(){
		return $this->_proxied->getProperties();
	}//End of method
	
	
	//-------------------------------------------------------
	/** 
	 * 
	 * @return string
	 */
	public function getNumber(){
		return $this->_proxied->getNumber();
	}//End of method
	
	//-------------------------------------------------------
	/** 
	 * 
	 * @return string
	 */
	public function getName(){
		return $this->_proxied->getName();
	}//End of method
	
	//-------------------------------------------------------------------------
	/**
	 * @return integer
	 */
	function getFather(){
		return $this->_proxied->getContainer()->getId();
	}//End of method
	
	//----------------------------------------------------------
	/**
	 * @param string $name
	 * @param string $name
	 * @return boolean
	 */
	public function setProperty($name, $value){
		$this->_proxied->setProperty($name, $value);
		return true;
	}//End of method
	
	//--------------------------------------------------------------------
	/** 
	 * 
	 * @param string
	 * @return void
	 */
	function setNumber($number){
		return $this->_proxied->setNumber($number);
	}//End of method
	
	//--------------------------------------------------------------------
	/** 
	 * 
	 * @param string
	 * @return void
	 */
	function setName($name){
		return $this->_proxied->setName($name);
	}//End of method

	//---------------------------------------------------------------------------
	/** Remove a Document
	 * This method suppress the record of the Document
	 * and suppress files and iterations files.
	 * If versions exists, suppress the current version and restore the previous version.
	 * 
	 * @return boolean
	 * 
	 */
	public function suppress(){
		return $this->_proxied->suppress();
	}//End of method


	//--------------------------------------------------------------------
	/** 
	 * 
	 * @return integer	document_id
	 */
	function save(){
		$ok = $this->_proxied->save(true, true);
		if($ok < 1){
			$msg = Ranchbe::getError()->getLastMessage();
			throw new SoapFault('Server', 'error during saving : ' . $msg );
		}
		return $this->_proxied->getId();
	}//End of method
	
	//----------------------------------------------------------
	/**
	 * @param string $name
	 * @return boolean
	 */
	function unsetProperty($name){
		$this->_proxied->unsetProperty($name);
		return true;
	}//End of method
	
}//End of class
