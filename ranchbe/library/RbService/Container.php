<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('RbService/Base.php');

/** Service to containers.
 * 
 * Containers are basic elements to create a ranking
 * 
 */
class RbService_Container extends RbService_Base{
	
	/**
	 * 
	 * @var Rb_Container
	 */
	protected $_container;

	/** Init a new container
	 * 
	 * @param string	space name
	 * @return void
	 */
	public function init($space_name){
		try{
			$this->_container = Rb_Container::get($space_name, 0);
			$this->_proxied =& $this->_container;
		}catch(Exception $e){
			throw new SoapFault('Server', $e->getMessage());
		}
	}
	
	/** Test if is a file manager or document manager Container type
	 * 
	 * @return Boolean
	 */
	public function isFileOnly(){
		return $this->_container->getProperty('file_only');
	}//End of method

	/**
	 * Get all doctypes linked to Container
	 * Return false if error or an array with the doctype properties
	 * 
	 * @param String $extension limit the result to doctype with extension $extension.
	 * @param String $type limit the result to doctype with type $type.
	 * @return Array|Bool
	 */
	public function getDoctypes($extension=0 , $type=0){
		return $this->_container->getDoctypes($extension , $type);
	}//End of method
	
	/** helper method to get metadatas
	 * Get all metadatas linked to Container
	 * Return a array with properties of the metadatas
	 * 
	 * @param Array $params
	 * @return Array
	 */
	public function getMetadatas( array $params=array() ){
		return $this->_container->getMetadatas($params);
	}//End of method
	
	/**
	 * Check if there is at least one doctype linked to Container
	 * Return false or true
	 * 
	 * @return Bool
	 */
	public function hasDoctype(){
		return $this->_container->hasDoctype();
	}//End of method
	
} //End of class

