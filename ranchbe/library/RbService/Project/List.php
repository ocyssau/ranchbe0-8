<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('RbService/Abstract.php');

/** Service to projects.
 * 
 * Projects are organization unit to the top level used to create specifics configuration.
 * 
 */
class RbService_Project_List extends RbService_Abstract{
	
	//---------------------------------------------------------------------
	/** Edit 
	 * 
	 * @param array $inputs
	 *		$input = array(
	 *					array(
     *						'project'=>array(
     *							'id'=>integer,
     *							'properties'=>array('project_number'=>string, 'project_name'=>string),
     *						),
	 *					),
	 *		);
	 * 
	 * @return array
	 * 			array(
	 *				  0=>array( 'error_code'=>error_code, 
	 *							'error_msg'=>string, 
	 *							'index'=>$key)),
	 * 
	 * error_code is a enum of integer :
	 * 			0 : no error
	 * 			1 : internal error to Rb library
	 * 			2 : error in service
	 * 			3 : warning in service
	 * 
	 * 
	 */
	public function edit(array $inputs) {
		$output = array();
		
		$i=0;
		foreach ( $inputs as $key=>$input ) {
			$i++;
			$isEdited = false;
			
			$output[$i] = array(
							'error_code'=>3, 
							'error_msg'=>'unknow error', 
							'index'=>$key);
			
			$properties = $input['project']['properties'];
			$project_id = (int) $input['project']['id'];
			
			$project = Rb_Project::get ( $project_id );
			if(!$project){
				$output[$i]['error_code'] = 3;
				$output[$i]['error_msg'] = 'Project is not existing';
			}
			
			foreach ( $properties as $property => $value ) {
				$project->setProperty ( $property, $value );
			}
			$ok = $project->save ();
			if(!$ok){
				$output[$i]['error_code'] = 1;
				$output[$i]['error_msg'] = Ranchbe::getError()->getErrors(true);
			}else{
				$output[$i]['error_code'] = 0;
				$output[$i]['error_msg'] = '';
			}
		}
		return $output;
	} //End of method
		
	//---------------------------------------------------------------------
	/** Create projects from a list
	 * 
	 * @param array $inputs
	 *		$input = array(
	 *					array(
     *						'project'=>array(
     *							'id'=>integer,
     *							'properties'=>array('project_number'=>string, 'project_name'=>string),
     *						),
	 *					),
	 *		);
	 * 
	 * @return array
	 * 			array(
	 *				  0=>array( 'error_code'=>error_code, 
	 *							'error_msg'=>string, 
	 *							'index'=>$key)),
	 * 
	 * error_code is a enum of integer :
	 * 			0 : no error
	 * 			1 : internal error to Rb library
	 * 			2 : error in service
	 * 			3 : warning in service
	 * 
	 * 
	 */
	public function create(array $inputs) {
		$output = array();
		
		$i=0;
		foreach ( $inputs as $key=>$input ) {
			$i++;
			//init output
			$output[$i] = array(
								'error_code'=>3, 
								'error_msg'=>'unknow error', 
								'index'=>$key);
			
			$properties = $input['project']['properties'];
			
			$project = new Rb_Project; //Create a new instance
			
			foreach ( $properties as $property => $value ) {
				$project->setProperty ( $property, $value );
			}
			$ok = $project->save ();
			if(!$ok){
				$output[$i]['error_code'] = 1;
				$output[$i]['error_msg'] = Ranchbe::getError()->getErrors(true);
			}else{
				$output[$i]['error_code'] = 0;
				$output[$i]['error_msg'] = '';
			}
		} //End of foreach
		return $output;
	} //End of method
		
	//---------------------------------------------------------------------
	/** Suppress projects from a list
	 * 
	 * @param array $inputs
	 *		$input = array(
	 *					array(
     *						'project'=>array(
     *							'id'=>integer,
     *						),
	 *					),
	 *		);
	 * 
	 * @return array
	 * 			array(
	 *				  0=>array( 'error_code'=>error_code, 
	 *							'error_msg'=>string, 
	 *							'index'=>$key)),
	 * 
	 * error_code is a enum of integer :
	 * 			0 : no error
	 * 			1 : internal error to Rb library
	 * 			2 : error in service
	 * 			3 : warning in service
	 * 
	 * 
	 */
	public function suppress($inputs) {
		$output = array();
		$i = 0;
		foreach ( $inputs as $key=>$input ) {
			$i++;
			$output[$i]['index'] = $key;
			$output[$i]['error_code'] = 3;
			$output[$i]['error_msg'] = 'unknow error';
			
			$project_id = $input['project']['id'];
			
			$project = Rb_Project::get ( $project_id );
			if(!$project){
				$output[$i]['error_code'] = 3;
				$output[$i]['error_msg'] = 'Container is not existing';
			}
			
			$ok = $project->suppress ();
			if($ok){
				$output[$i]['error_code'] = 0;
				$output[$i]['error_msg'] = '';
			}else{
				$output[$i]['error_code'] = 1;
				$output[$i]['error_msg'] = Ranchbe::getError()->getErrors(true);
			}
		}
		
		return $output;
	} //End of method
	
	//---------------------------------------------------------------------
	/** Get list of projects
	 *  In filter, use find to do a passthrough search (what=*'search'*), or exact_find
	 *  to do a strict search (what='search')
	 * 
	 * @param array $filter			
	 * 	array(find|exact_find=>(array(AND|OR=>array('where'=>'what'))));
	 *  		find : use the passthrough %
	 *  		exact_find : to create a search on a plain value (a index key for example)
	 *  		AND : associate term by a AND opertor
	 *  		OR : associate terms by a OR opertor
	 *  		where : name of column to query
	 *  		what : value to search. Accept the passthrough %
	 * 
	 * @return array
	 * 				= array(0=>array('property_name'=>'property_value'))
	 * 
	 */
	public function getList( $filters ) {
		$project =& Rb_Project::get();

		if (! Ranchbe::checkPerm ( 'get', $project, false )) {
			throw new SoapFault(RBS_SERVER, "You are not authorized to access to this request");
		}
		
		try {
			//$search = new Rb_Search_Db ( $project->getDao () );
			//$dao =& $project->getDao ();
			return $project->getAll ( $filters );
		}catch(Exception $e){
			throw new SoapFault(RBS_SERVER, $e->getTraceAsString() );
		}
	} //End of method
	
} //End of class


