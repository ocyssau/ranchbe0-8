<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

/**
 * Input structure for all services :
 *
 $input = array(
	 'documents'=>array(
		 docindex=>array(		//docindex is a integer | string
			 'id'=>integer,
			 'container'=>array('id'=>integer, space_name=>string),
			 'properties'=>array(),
			 'docfiles'=>array(
				 0=>array(
					 'index'=>dfindex,
					 'role'=>0
				 ),
			 ),
		 ),
		 'docfiles'=>array(
			 dfindex=>array(			//dfindex is a integer | string
				 'id'=>integer,
				 'container'=>array('id'=>integer, space_name=>string),
				 'properties'=>array(),
				 'fsdata'=>array(),		//array of fsindex
			 ),
		 ),
		 'fsdatas'=>array(
			 fsindex=>array(			//fsindex is a integer | string
				 'file_name'=>string,
				 'md5'=>string,
				 'content'=>base64,
			 )
		 ),
 ));
 */

require_once('RbService/Base.php');
require_once('Rb/Space.php');
require_once('Rb/Document.php');


/*
require_once('Rb/Acl/Resource/Interface.php');
require_once('Rb/Observable/Interface.php');
require_once('Rb/Object/Root.php');
require_once('Rb/Observable.php');
require_once('Rb/Document.php');
require_once('Rb/Acl/Resource/Interface.php');
require_once('Rb/Object/Interface/Related.php');
require_once('Rb/Object/Permanent.php');
require_once('Rb/Object/Acl.php');
require_once('Rb/Space.php');
*/

/** 
 * 
 */
class RbService_Document extends RbService_Base{
	
	/**
	 * 
	 * @var Rb_Document
	 */
	protected $_document;
	
	/** Load object from his id
	 * 
	 * @param integer	id of object
	 * @param string	space name
	 * @return integer
	 */
	public function loadFromId($id, $space_name){
		//return Rb_User::getCurrentUser();
		//return $storage = Zend_Auth::getInstance()->getStorage()->read();
		//Zend_Auth::getInstance()->getStorage();
		//require_once 'Zend/Auth/Storage/Session.php';
		//new Zend_Auth_Storage_Session();
		try{
			$this->_document = Rb_Document::get($space_name, $id);
			$this->_proxied =& $this->_document;
			return $this->_document->getId();
		}catch(Exception $e){
			throw new SoapFault('Server', $e->getMessage());
		}
	}
	
	/** Init a new document
	 * 
	 * @param string	space name
	 * @return void
	 */
	public function init($space_name){
		try{
			$this->_document = Rb_Document::get($space_name, 0);
			$this->_proxied =& $this->_document;
		}catch(Exception $e){
			throw new SoapFault('Server', $e->getMessage());
		}
	}
	
	//-------------------------------------------------------------------------
	/**
	 * @param integer
	 * @return boolean
	 */
	function setFather( $id ){
		$space_name = $this->_document->getSpaceName();
		$father = Rb_Container::get($space_name, $id);
		$this->_document->setContainer($father);
		return true;
	}//End of method
	

	//--------------------------------------------------------
	/** 
	 * Return doctype of current Document.
	 * 
	 * @return integer
	 */
	function getDoctype(){
		return $this->_document->getDoctype()->getId();
	}//End of method

	//--------------------------------------------------------
	/** 
	 * Get a docfile object from $this->docfiles property
	 * return a docfile object for class main, other type return array of docfile object
	 *
	 * if $role = main|mainvisu|mainpicture|mainannex return a docfile object
	 * if $role =  visu|picture|annex return a array of docfile object
	 *
	 * there is only one main docfile for a Document. May be have many picture, visu or annex docfile.
	 * Main visu, picture or annex is the first docfile of the role
	 * 
	 * @param String $role
	 * @return Array
	 */
	function getDocfile($role = 'main'){
		$docfiles = $this->_document->getDocfile($role);
		$res = array();
		foreach($docfiles as $docfile){
			$res[] = $docfile->getId();
		}
		return $res;
	}//End of method

	//-------------------------------------------------------------------------
	/** Try to get the default process linked to Document else process linked
	 *         to container and else the process of the context
	 * Return the process_id else return false
	 * 
	 * @return integer
	 *
	 */
	public function getProcessId(){
		return $this->_document->getProcessId();
	}//End of method

	//----------------------------------------------------------
	/** Get all metadatas of the Document from Document, container,
	 *         doctype, category
	 * Return a array with properties of the metadatas
	 * 
	 * @param array 	see Rb_Basic::getQueryOptions()
	 * @return array
	 */
	public function getMetadatas($params=array()){
		return $this->_document->getMetadatas($params);
	}//End of method
	
	//---------------------------------------------------------------------------
	/** 
	 * This method record the file $file from the wildspace in database
	 * and copy it in vault and associate it to Document $document_id
	 * if suppress = true, the file will be suppress of the wildspace
	 *
	 * Return Rb_Docfile or false
	 *
	 * See too associateDocfile
	 *
	 * @param string 	$file , path and file to associate
	 * @param boolean 	$suppress , true for suppress file of the wildspace after associate it
	 * @param integer 	$role, id of role for this docfile : see Rb_Dofile_Role
	 * @param integer 	$mode, mode of generation of this docfile: 0=generated by user, 1=generated by Ranchbe
			 when mode=1, the docfile will be never modified by user
	 * @return integer	docfile id or false
	 */
	function associateFile($file, $suppress = false, $role=0, $mode=0){
		$ret = $this->_document->associateFile($file, $suppress, $role, $mode);
		if(is_a('Rb_Docfile', $ret)){
			return $ret->getId();
		}else{
			return false;
		}
	}//End of method
	
	//-------------------------------------------------------
	/** Put files of a Document from the vault reposit directory to user wildspace and lock the Document.
	 * return true or false.
	 *
	 * CheckOut is use for modify a Document. The files are put in a directory where user has write access
	 * and the Document storing on the protected directory (the vault) is lock. Thus it is impossible that a other user modify it in the same time.
	 * 
	 * If option $checkoutFile is false, options $ifExistMode, $getFiles and $withFiles are ignored
	 * If option $getFiles is false, options $ifExistMode is ignored
	 * 
	 * 
	 * @param boolean	$checkoutFile 	if true checkout files of document
	 * @param string	$ifExistMode 	cancel|replace|keep
	 * @param boolean	$getFiles 		if true copy file in wildspace
	 * @param boolean | array
	 * 						$withFiles, if true, checkin all associated docfiles
	 * 						$withFiles, if array of docfiles id, checkin only this docfiles
	 * @return boolean
	 * 
	 */
	public function checkOut($checkoutFile = true, $ifExistMode='cancel', $getFiles = true, $withFiles = true ){
		return $this->_document->checkOut($checkoutFile, $ifExistMode, $getFiles, $withFiles);
	}//End of method

	//-------------------------------------------------------
	/**
	 * Unlock the Document after a checkOut without copy files from wildspace to vault
	 * 
	 * @return boolean
	 *
	 */
	public function cancelCheckOut(){
		return $this->_document->cancelCheckOut();
	}//End of method

	//-------------------------------------------------------
	/** ckeckin Document and releasing reservation
	 *
	 * @param boolean | array 
	 * 						$withFiles, if true, checkin associates docfiles
	 * 						$withFiles, if false, do not checkin associates docfiles
	 * 						$withFiles, array of docfiles id, checkin only this docfile
	 * @return boolean
	 */
	public function checkInAndRelease($withFiles = true){
		return $this->_document->checkInAndRelease($withFiles);
	}//End of method

	//-------------------------------------------------------
	/** ckeckin Document and keep reservation
	 * 
	 * @param boolean | array 
	 * 						$withFiles, if true, checkin associates docfiles
	 * 						$withFiles, if false, do not checkin associates docfiles
	 * 						$withFiles, array of docfiles id, checkin only this docfile
	 * @return boolean
	 *
	 */
	public function checkInAndKeep($withFiles = true){
		return $this->_document->checkInAndKeep($withFiles);
	}//End of method

	//-------------------------------------------------------
	/** This method can be used to check if a Document is free.
	 * Return integer, access code (0 for free without restriction , 100 if error).
	 * 
	 * @return integer		access code
	 *
	 */
	public function checkAccess(){
		return $this->_document->checkAccess();
	}//End of method

	//-------------------------------------------------------
	/** This method can be used to check if the uers can update a Document
	 * Return true if document is currently checkout by current user
	 * 
	 * @return integer		access code
	 *
	 */
	public function checkUpdateRight(){
		return $this->_document->checkUpdateRight();
	}//End of method

	//---------------------------------------------------------------------------
	/** 
	 * Change the state of a Document
	 *
	 * @param string 	New value of the state.
	 * @param boolean	If true, save in database.
	 * 
	 * @return boolean
	 * 
	 */
	function changeState($state='init', $save=true){
		return $this->_document->changeState($state, $save);
	}//End of method
	
	//---------------------------------------------------------------------------
	/** Copy Document to another Document with new name, new bid
	 *  Return Rb_Document or false.
	 * 
	 * @param integer 		$to_container, 	target container id
	 * @param string  		$to_number, 	number of the new Document to create.
	 * @param integer  		$to_version, 	number id of version for new Document.
	 * @param boolean  		$copyFiles, 	if true, copy files too and associate to new Document.
	 * @return Rb_Document | false
	 *
	 */
	public function copyDocumentToNumber($to_container_id,
											$to_number,
											$to_version,
											$copyFiles = true){
												
		$to_container = Rb_Container::get($this->_document->getSpace(), $to_container_id);
		if($to_container){
			return $this->_document->copyDocumentToNumber($to_container,$to_number,$to_version,$copyFiles);
		}
	}//End of method

	//-------------------------------------------------------
	/** Create a new Document version from Document
	 * 
	 * @param integer			attach version to this container id
	 * @param integer	 		version id to create
	 * @param boolean			if true, copy files too and associate to new Document
	 * @return Rb_Document
	 */
	public function copyDocumentToVersion($to_container_id,
											$version_id,
											$copyFiles = true){
		$to_container = Rb_Container::get($this->_document->getSpace(), $to_container_id);
		if($to_container){
			return $this->_document->copyDocumentToVersion($to_container,$version_id,$copyFiles);
		}
	}//End of method

	//---------------------------------------------------------------------------
	/** 
	 * Lock a Document with code.
	 *
	 * The value of the access code permit or deny action:<ul>
	 *  <li>0: Modification of Document is permit.</li>
	 *  <li>1: Modification of Document is in progress. The checkOut is deny. You can only do checkIn.</li>
	 *  <li>5: A workflow process is in progress. Modification is deny. You can only activate activities.</li>
	 *  <li>10: The Document is lock.You can only upgrade his indice.</li>
	 *  <li>11: Document is lock. You can only unlock it or upgrade his indice.</li>
	 *  <li>15: Indice is upgraded. You can only archive it.</li>
	 *  <li>20: The Document is archived.</li>
	 *</ul>
	 * @param integer		new access code.
	 * @param boolean		if true save lock in database, else not
	 * 
	 * @return boolean
	 *  
	 */
	function lockDocument($code = 1, $save = true){
		return $this->_document->lockDocument($code, $save);
	}//End of method
	
	//---------------------------------------------------------------------------
	/** 
	 * This method can be used to unlock a Document.
	 * 
	 * @param boolean	if true save lock in database, else not
	 * @return boolean	
	 *
	 */
	public function unLockDocument($save=true){
		return $this->_document->unLockDocument($save);
	}//End of method
	
	//---------------------------------------------------------------------------
	/** 
	 * Return comprehensive name from lock code
	 *
	 * @param integer
	 * @return String
	 *
	 */
	public function getLockName($code){
		return $this->_document->getLockName($code);
	}//End of method
	
	//-------------------------------------------------------
	/** This method can be used to get all the files associated to a Document
	 *  You can use too getDocfiles().
	 *  This method create a query on database on
	 *  each call and not set the variable $this->docfiles.
	 *  You can create special query with parameter in $params.
	 * 
	 * @param array $params parameters of the query. See parameters function of getQueryOptions()
	 * @return array | false
	 */
	public function getAssociatedFiles($params=array()){
		return $this->_document->getAssociatedFiles($params);
	}//End of method
	
	//---------------------------------------------------------------------------
	/** Move a Document from container to another.
	 *
	 * A Document can be move only between container of the same space.
	 * You can not move a Document of a bookshop container to a cadlib container.
	 * This method modify the Document record and move the associated files from the
	 * directory of the original container to the directory of the tager container.
	 * Please, consider that this method dont move iteration files and the previous indice Document.
	 *
	 * @param integer		target container id
	 * @return boolean
	 */
	public function moveDocument($to_container_id){
		$to_container = Rb_Container::get($this->_document->getSpace(), $to_container_id);
		if($to_container){
			return $this->_document->moveDocument($to_container);
		}
	}//End of method

	//---------------------------------------------------------------------------
	/** Link a instance of a process to the Document
	 * Return true or false.
	 *
	 * @param integer $instance_id, id of the workflow instance
	 * @param boolean $save, If true, save in database.
	 * @return boolean
	 */
	public function setInstance($instance_id, $save=true){
		return $this->_document->setInstance($instance_id, $save);
	}//End of method
	
	//--------------------------------------------------------------------
	/** 
	 * To recognize the type of Document
	 * 
	 * @param String $file_extension
	 * @param String $file_type
	 * @return integer	doctype id or false
	 */
	function setDocType($file_extension, $file_type){
		$ret = $this->_document->setDocType($file_extension, $file_type);
		if(is_a('Rb_Doctype', $ret)){
			return $ret->getId();
		}else{
			return false;
		}
	}//End of method

	//---------------------------------------------------------------------------
	/** Unlink a instance of a process from the Document
	 * Return true or false.
	 *
	 * Use when instance is terminate. If a terminate instance is linked to a Document
	 * is not possible to create and link a new instance to this Document
	 * 
	 * @param boolean $save, If true, save in database.
	 * @return boolean
	 */
	public function unsetInstance($save=true){
		return $this->_document->unsetInstance($save);
	}//End of method

	//-------------------------------------------------------
	/** View the Document file.
	 *
	 * This method send the content of the $file to the client browser.
	 * It send too the mimeType to help the browser
	 *   to choose the right application to use for open the file.
	 * If the mimetype = no_read, display a error.
	 * 
	 * @return boolean
	 * 
	 */
	public function viewDocument(){
	}//End of method

	//-------------------------------------------------------
	/** Copy associated files to the wildspace to consult it.
	 * 
	 * @return boolean
	 *
	 */
	public function putInWildspace(){
		return $this->_document->putInWildspace();
	}//End of method

}//End of class
