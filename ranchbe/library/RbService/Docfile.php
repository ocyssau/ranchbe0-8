<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('RbService/Base.php');

/** Service of docfiles.
 * 
 * Docfiles are files attached to documents.
 * 
 */
class RbService_Docfile extends RbService_Base{
	
	/** Load object from his id
	 * 
	 * @param integer	id of object
	 * @param string	space name
	 * @return integer
	 */
	public function loadFromId($id, $space_name){
		try{
			$this->_proxied = Rb_Docfile::get($space_name, $id);
			return $this->_proxied->getId();
		}catch(Exception $e){
			throw new SoapFault('Server', $e->getMessage());
		}
	}
	
	/** Init a new object
	 * 
	 * @param string	space name
	 * @return void
	 */
	public function init($space_name){
		try{
			$this->_proxied = Rb_Docfile::get($space_name, 0);
		}catch(Exception $e){
			throw new SoapFault('Server', $e->getMessage());
		}
	}
	
} //End of class
