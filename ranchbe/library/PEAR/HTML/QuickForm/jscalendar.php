<?php

/**
 * HTML QuickForm element for jsCalendar
 *
 * jsCalendar is a dynamic JavaScript/DHTML calendar which can be obtained from
 * http://www.dynarch.com/projects/calendar/
 *
 * Example of PHP code:
 * <code>
 * require_once 'HTML/QuickForm.php';
 * require_once 'HTML/QuickForm/jscalendar.php';
 * $form = new HTML_QuickForm('calForm');
 *
 * // View DynArch's DHTML Calendar Widget document for exhaustive list
 * // of options
 * $options = array(
 *     'ifFormat' => '%Y-%m-%d',    // Input field format
 * );
 * $calendar =& $form->addElement('jscalendar', 'date', 'Date', $options);
 *
 * // URL to where you installed calendar's JavaScript files on your web server
 * // (Path must end with trailing slash /)
 * $calendar->basePath = 'js_library/jscalendar/';
 *
 * // Continue with regular QuickForm setup and processing...
 * $form->display();
 * <code>
 *
 * @author       Philippe Jausions <Philippe.Jausions@11abacus.com>
 * @copyright    2006 11abacus
 * @license      LGPL
 * @link         http://www.dynarch.com/projects/calendar/
 * @package      HTML_QuickForm_jscalendar
 * @category     HTML
 */

/**
 * Required parent class
 */
require_once 'HTML/QuickForm.php';
require_once 'HTML/QuickForm/text.php';

/**
 * Replace PHP_EOL constant
 *
 * @category    PHP
 * @package     PHP_Compat
 * @link        http://php.net/reserved.constants.core
 * @author      Aidan Lister <aidan@php.net>
 * @version     $Revision: 1.2 $
 * @since       PHP 5.0.2
 */
if (!defined(PHP_EOL)) { //erreur corrige : suppression des quotes...
    switch (strtoupper(substr(PHP_OS, 0, 3))) {
        // Windows
        case 'WIN':
            define('PHP_EOL', "\r\n");
            break;

        // Mac
        case 'DAR':
            define('PHP_EOL', "\r");
            break;

        // Unix
        default:
            define('PHP_EOL', "\n");
    }
}

/**
 * HTML Quickform element for jsCalendar
 *
 * jsCalendar is a dynamic JavaScript/HTML calendar which can be obtained from
 * http://www.dynarch.com/projects/calendar/
 *
 * @author       Philippe Jausions <Philippe.Jausions@11abacus.com>
 * @copyright    2006 11abacus
 * @license      LGPL
 * @link         http://www.dynarch.com/projects/calendar/
 * @package      HTML_QuickForm_jscalendar
 * @category     HTML
 */
class HTML_QuickForm_jscalendar extends HTML_QuickForm_text
{
    /**
     * The path where to find the editor
     *
     * Use forward slashes / only.
     * The path must end with a trailing slash /
     *
     * @var string
     * @access public
     */
    //var $basePath = '/jscalendar/';
    var $basePath = 'lib/jscalendar/';

    /**
     * Language for the calendar
     *
     * @var string
     * @access public
     */
    var $lang = 'en';

    /**
     * Calendar theme
     *
     * @var string
     * @access public
     */
    var $theme = 'calendar-win2k-1';

    /**
     * Whether to use the version stripped of its comment
     *
     * @var boolean
     * @access public
     */
    var $stripped = false;

    /**
     * Configuration settings for the calendar
     *
     * @var array
     * @access private
     */
    /*
    var $_config = array(
        'ifFormat' => '%Y/%m/%d',
        'daFormat' => '%Y/%m/%d',
        );
    */
    var $_config = array(
        'displayArea'=> 'jsc_display', //Id of the span for display area
        'daFormat' => '%Y/%m/%d %H:%M:%S',
        'ifFormat'   => '%Y/%m/%d %H:%M:%S', //Display timestamp
        'button' => 'jsc__trigger'
        );

    /**
     * Setting of the csv option for the span field
     * @var string
     * @access private
     */
    var $span_style = 'style="background-color: #ff8; cursor: default;"
                        onmouseover="this.style.backgroundColor=#ff0;"
                        onmouseout="this.style.backgroundColor=#ff8;"';

    /**
     * Class constructor
     *
     * @param   string  jsCalendar instance name
     * @param   string  jsCalendar instance label
     * @param   array   Config settings for jsCalendar
     *                  (see "Calendar.setup in details" in "DHTML Calendar
     *                   Widget" online documentation)
     * @param   string  Attributes for the text input form field
     * @access  public
     */
    function HTML_QuickForm_jscalendar($elementName = null, $elementLabel = null, $options = array(), $attributes = null)
    {
        $this->updateAttributes(array('type' => 'text'));
        $this->HTML_QuickForm_text($elementName, $elementLabel, $attributes);
        $this->_persistantFreeze = true;
        $this->_type = 'jscalendar';

        if( !isset($options['button']) )
          $this->_config['button'] = 'jsc__trigger_'.$elementName;

        if( !isset($options['displayArea']) )
          $this->_config['displayArea'] = 'jsc_display_'.$elementName;

        if (is_array($options)) {
            $this->_config = array_merge($this->_config, $options);
        }
    }

    /**
     * Set config variable for jsCalendar
     *
     * @param mixed an associated array of configuration value
     *              or the name of key of config setting
     * @param mixed Value of config setting (only used when $key is a scalar)
     * @access public
     */
    function setConfig($key, $value = null)
    {
        if (is_array($key)) {
            foreach ($key as $k => $v) {
                $this->_config[$k] = $v;
            }
        } else {
            $this->_config[$key] = $value;
        }
    }

    /**
     * Returns the jsCalendar in HTML
     *
     * @access public
     * @return string
     */
    function toHtml()
    {
        if ($this->_flagFrozen) {
            return $this->getFrozenHtml();
        }
        $name = $this->getAttribute('name');
        $value = $this->getAttribute('value'); //Timestamp from database
        $this->updateAttributes(array('id' => $name));
        $html = '';
        if (!defined('HTML_QUICKFORM_JSCALENDAR_LOADED')) { //if the include are not yet declared
            // load jsCalendar
            if ($this->stripped) {
                $calendarFile = 'calendar_stripped.js';
                $calendarSetupFile = 'calendar-setup_stripped.js';
            } else {
                $calendarFile = 'calendar.js';
                $calendarSetupFile = 'calendar-setup.js';
            }

            $calendarLangFile = 'lang/calendar-' . $this->lang . '.js';

            $html = '<script type="text/javascript" src="'
                    . $this->basePath . $calendarFile . '"></script>' . PHP_EOL
                    . '<link href="'
                    . $this->basePath . $this->theme.'.css"' . ' rel="stylesheet" type="text/css">' . PHP_EOL
                    . '<script type="text/javascript" src="'
                    . $this->basePath . $calendarLangFile . '"></script>' . PHP_EOL
                    . '<script type="text/javascript" src="'
                    . $this->basePath . $calendarSetupFile . '"></script>' . PHP_EOL
                    ;
            define('HTML_QUICKFORM_JSCALENDAR_LOADED', true);
        }

        if(!empty($value)){
          $display = strftime( $this->_config['daFormat'] , $value);
          $this->_config['date'] = 'date('.strftime( '%Y, %m, %d, %H, %M, %S' , $value).')';
          //$this->_config['date'] = strftime( $this->_config['ifFormat'] , $value);
        }else $display = 'Click here';

        $html .= '<input type="hidden" id="'.$name.'" name="'.$name.'" value="'. $value .'"/>' . PHP_EOL;

        //$name = $name.'[3]';
        //var_dump($name);
        //Reform name to put in end the [n]
        if(preg_match( '/[[0-9]+]$/' , $name , $match)){
          $input_field = preg_replace( '/[[0-9]+]$/' , '' ,  $name );
          $input_field = $input_field.'_jscalendar'.$match[0];
        }else{
          $input_field = $name.'_jscalendar';
        }
        //var_dump($input_field);
        //var_dump($match);die;

        if(isset($this->_config['displayArea'])){
          $html .= '<input type="hidden" id="'.$input_field.'" name="'.$input_field.'" value="'. $value .'"/>' . PHP_EOL;
          $html .= '<span id="'.$this->_config['displayArea'].'"'.$this->span_style.'><i>'.$display.'</i></span>.</p>' . PHP_EOL;
        }else{
          $html .= '<input type="text" id="'.$input_field.'" name="'.$input_field.' "value="'. $value .'"/>' . PHP_EOL;
        }

        if(isset($this->_config['button'])){
          $html .= '<button id="'.$this->_config['button'].'">...</button>';
        }

        $setup = array(
         'inputField' => $input_field,
        );

        $options = array_merge($this->_config, $setup);
        
        $js_calendarUpdated_function = 'function calendarUpdated(cal) {' . PHP_EOL
                                    .'var element = document.getElementById("'.$name.'");' . PHP_EOL
                                    .'if (element) element.value = cal.date.print(\'%s\');' . PHP_EOL
                                    .'}' . PHP_EOL;

        $html .= '<script type="text/javascript">' . PHP_EOL
              .'Calendar.setup({' .$this->_jsSerialize($options).',onUpdate:calendarUpdated});' . PHP_EOL
              .$js_calendarUpdated_function . PHP_EOL
              .'</script>' . PHP_EOL;

        return $html;
    }

    /**
     * Returns the jsCalendar content in HTML
     *
     * @return string
     * @access public
     */
    function getFrozenHtml()
    {
        return $this->getValue();
    }

    /**
     * Serializes into JavaScript
     *
     * @param  array  $data
     * @return string
     * @access protected
     */
    function _jsSerialize($data)
    {
        $jstr = '';
        foreach ($data as $key => $val) {
            if (is_bool($val)) {
                $val = ($val) ? 'true' : 'false';

            } elseif (!is_numeric($val)) {
                $val = '"'.$val.'"';
            }
            if ($jstr) {
                $jstr .= ',';
            }
            $jstr .= '"' . $key . '":' . $val;
        }
        return $jstr;
    }

}

/**
 * Register the element
 */
HTML_QuickForm::registerElementType('jscalendar', 'HTML/QuickForm/jscalendar.php', 'HTML_QuickForm_jscalendar');

?>
