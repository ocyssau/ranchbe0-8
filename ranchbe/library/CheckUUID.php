<?php
//A contrib proposed by christian SELLES

function string4c($Catia_file,$name)
{
	// Extraction d'un chaine de caract�re sous format :
	// 4 Octets d'index pour la longueur de la chaine (peut�tre MSB ou LSB).
	// N Octets pour le contenu de la chaine.
	$Data=fread ($Catia_file, 4);
	$lenstr = ord(substr($Data,0,1))+ord(substr($Data,3,1));
	$debug[] = "<br>$name , lenstr=". $lenstr;
	$result=fread($Catia_file, $lenstr);
	$debug[] = " , str=" . $result;
	return $result;
}

function stringc($Catia_file,$name)
{
	// Extraction d'une chaine de caract�re sous format :
	// 1 Octet d'index pour la longueur de la chaine (peut�tre MSB ou LSB).
	// N Octets pour le contenu de la chaine.
	$lenstr = ord(fread ($Catia_file, 1));
	$debug[] = "<br>$name , lenstr=". $lenstr;
	$result=fread($Catia_file, $lenstr);
	$debug[] = " , str=". $result;
	return $result;
}

function uint($DataUint)
{
	// Extraction d'un double mot MSB
	$uinteger = ord(substr($DataUint,0,1))*256*256*256;
	$uinteger += ord(substr($DataUint,1,1))*256*256;
	$uinteger += ord(substr($DataUint,2,1))*256;
	$uinteger += ord(substr($DataUint,3,1));
	return $uinteger;
}

function bytes($Catia_file,$name,$lng)
{
	$str="";
	for ($id=0;$id<$lng;$id++) {
		$str.=sprintf("%02x",ord(fread($Catia_file, 1)));
	}
	$debug[] =  "<br>$name bytes=". $str;
	return $str;
}

function uuid($Catia_file)
{
	$l=0;
	$lenstr = ord(fread ($Catia_file, 1));
	$result=fread($Catia_file, $lenstr);
	for ($id=0;$id<$lenstr;$id++) {
		if (ord(substr($result,$id,1))==70) {
			$debug[] =  "<br>uuid , lenstr=". $lenstr . " , result=". $result;
			$str="";
			for ($id1=0;$id1<6;$id1++) {
				$str .= sprintf("%c",ord(fread($Catia_file, 1)));
			}
			$debug[] =  ", str=" . $str;
			if ($str=="FINJPL") {
				$l=$id-1;
				$debug[] =  ", l=" . $l;
			}
		}
	}
	return $l;
}

//---------------------------------------------------------------------------
function checkUUID($file){

	$Catia_file = fopen ($file, "rb");

	if ( $Catia_file )
	{
		$Entete = fread($Catia_file, 8);
		//    echo $Entete , "<br>";
		$BlocA=uint(fread ($Catia_file, 4));
		$BlocB=uint(fread ($Catia_file, 4));
		//	echo "BlochA = $BlocA , $BlocB = BlocB , somme = " ,$BlocA+$BlocB , "soit la taille du fichier<br>";
		$subrecordMatch = chr(0x44). chr(0x41) . chr(0x53) . chr (0x53);
		$subrecordMSB = chr(00) . chr(00) . chr(00) . chr(17) . $subrecordMatch;
		$subrecordLSB = chr(17) . chr(00) . chr(00) . chr(00) . $subrecordMatch;
		for ($index=0;$index<8;$index++) {
			fseek($Catia_file,$index);
			$debug[] = "<br> " . $index . "<br>";
			while (!feof($Catia_file))
			{
				$Data = fread ($Catia_file, 8);
				$len++ ;
				if ($subrecordMSB  == $Data) {
					$DataPos[++$num] = ftell($Catia_file);
					$debug[] =  "<br>MSB # " . $num . " , " . sprintf("%X",$DataPos[$num]) . " , DATA Found " . sprintf("%X",uint($Data)) . sprintf("%X",uint($subrecordMatch));
					$i=0;
				}
				if ($subrecordLSB == $Data) {
					$DataPos[++$num] = ftell($Catia_file);
					$debug[] =  "<br>LSB # " . $num . " , " . sprintf("%X",$DataPos[$num]) . " , DATA Found " . sprintf("%X",uint($Data)) . sprintf("%X",uint($subrecordMatch));
					$i=0;
				}
			}
			$debug[] = "Fin file " . sprintf("%X",ftell($Catia_file));
		}
		for ($index=1;$index<=$num;$index++) {
			# Positionnement du pointeur au d�but de la zone trouv�e
			fseek($Catia_file,$DataPos[$index]-8);
			$debug[] = "DataPos[$index] = " . $DataPos[$index]-8 . "<br>";
			# Lecture de 4 strings dont les longeurs sont d�finies sur 4 octets
			for ($i=1;$i<=4;$i++) {
				$null=string4c($Catia_file,"Loop");
				$debug[] = "$null ; string4c $i , ftell = " . ftell($Catia_file) . "<br>";
			}
			$null=bytes($Catia_file,"Short",2);
			$debug[] = "$null ; 2 bytes , ftell = " . ftell($Catia_file) . "<br>";
			# Lecture de 1 string dont la longeur est d�finie sur 1 octet
			#StoProp
			$null = stringc($Catia_file,"StoProp");
			$debug[] = "$null ; stringc , ftell = " . ftell($Catia_file) . "<br>";
			# Lecture de 7 bytes
			#StoPropVal
			$null = bytes($Catia_file,"StoPropVal",7);
			$debug[] = "$null ; 7 bytes , ftell = " . ftell($Catia_file). "<br>";

			$sum=0;
			$Data=fread ($Catia_file, 4);
			for ($id1=0;$id1<4;$id1++) { $sum+=ord(substr($Data,$id1,1));}
			$debug[] = "<br>StoPropVal index=" . $index . " , DataPos[index]=". sprintf("%u",$DataPos[$index]-8) . " , i=" . $i . " , sum=" . $sum;
			#######################################################
			if ($sum==12) {
				bytes($Catia_file,"StoPropVal1",5);
				#Unicode
				stringc($Catia_file,"Unicode");
				#Unicodeval
				bytes($Catia_file,"Unicodeval",7);#Catia
				$result = stringc($Catia_file,"Catia");
				$debug[] =  "<hr><b>$result </b>";
				#CatiaVal
				#========================================
				if ($result=="CATIA") {
					bytes($Catia_file,"CatiaVal",8);
					#Partname
					$report[$result]['InternalName'] = stringc($Catia_file,"Partname");
					$report[$result]['FileName'] = $file;
					#UUID
					//		  bytes($Catia_file,"UUID",uuid($Catia_file));
					# FINJPL
					//		stringc($Catia_file,"FINJPL");
				}
				#========================================

				else if ($result=="CATProduct") {
					//bytes($Catia_file,"CatiaVal",8);
					#Catproduct
					stringc($Catia_file,"Catproduct");
					#CatproductVal
					#    bytes($Catia_file,"CatproductVal",8);
					#UUID
					#print "UUID1="bytes("UUID",uuid());
					bytes($Catia_file,"UUID",21);
					$report['UUID'] = bytes($Catia_file,"UUID",16);
					$report['type'] = $result;
					//		  bytes($Catia_file,"UUID",uuid($Catia_file));
					# FINJPL
					//		  stringc($Catia_file,"FINJPL");
				}
				#========================================

				else if ($result=="CATDrawing") {
					bytes($Catia_file,"CatiaVal",8);
					#Catproduct
					stringc($Catia_file,"CATDrawing");
					#CatproductVal
					#    bytes($Catia_file,"CatproductVal",8);
					#UUID
					bytes($Catia_file,"UUID",21);
					$report['UUID'] = bytes($Catia_file,"UUID",16);
					$report['type'] = $result;
					//		  bytes($Catia_file,"UUID",uuid($Catia_file));
					# FINJPL
					//		  stringc($Catia_file,"FINJPL");
				}
				#========================================

				else if ($result=="CATPart") {
					bytes($Catia_file,"CatiaVal",8);
					#Catproduct
					stringc($Catia_file,"CATDrawing");
					#CatproductVal
					#    bytes($Catia_file,"CatproductVal",8);
					#UUID
					bytes($Catia_file,"Pr� UUID",21);
					$report['UUID'] = bytes($Catia_file,"UUID",16);
					$report['type'] = $result;
					//	  bytes($Catia_file,"After UUID",uuid($Catia_file));
					# FINJPL
					//		  stringc($Catia_file,"FINJPL");
				}
				#========================================
			}
		}
		fclose($Catia_file);
	}
	else
	{
		die( "fopen failed for $file" ) ;
	}

	//var_dump($debug);

	return $report;

} //End of  CheckUUID


