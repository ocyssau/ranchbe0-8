<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies Content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/**
 * this class is used to store all properties defined by project and container like :
 * containers links
 * doctypes
 * current space
 * current container
 * current project
 */
class Rb_Context {

	private $_session;                /*!< Zend_Session_Namespace object */
	private $_namespace = 'Context';  /*!< Name of the namespace */

	//------------------------------------------------------------------------------
	/**
	*
	* @return void
	*/
	public function __construct(){
		$this->_session = new Zend_Session_Namespace( $this->_namespace );
	} //End of method

	//-------------------------------------------------------------------------
	public function __set($name, $value){
		return $this->setProperty($name,$value);
	}

	//-------------------------------------------------------------------------
	public function __get($name){
		return $this->getProperty($name);
	}

	//-------------------------------------------------------------------------
	/**
	*
	* @return void
	*/
	public function init(){
		Zend_Session::namespaceUnset( $this->_namespace );
	} //End of method

	//-------------------------------------------------------------------------
	/**
	*
	* @param String $var
	* @param String $value
	* @return void
	*/
	public function setProperty($var, $value){
		$this->_session->$var = $value;
	} //End of method

	//-------------------------------------------------------------------------
	/**
	*
	* @param String $var
	* @return String
	*/
	public function getProperty($var){
		return $this->_session->$var;
	} //End of method

	//-------------------------------------------------------------------------
	/**
	*
	* @return String
	*/
	public function getProperties(){
		return $_SESSION[$this->_namespace];
	} //End of method

} //End of class

