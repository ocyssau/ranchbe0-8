<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/**
 * This class manage Container. A Container is a basic ranchbe component
 * that is content in followings spaces :
 *     workitem, bookshop, cadlib, mockup.
 * Container content documents or recordfiles
 *
 *
 */
abstract class Rb_Container extends Rb_Object_Acl implements Rb_Object_Interface_Related{

	protected $SPACE_NAME; //(String)

	protected $OBJECT_TABLE; //(String)Table where are stored the containers definitions
	protected $INDICE_TABLE; //(String)Table where are stored the Container indices

	//Fields:
	protected $FIELDS_MAP_ID; //(String)Name of the field of the primary key
	protected $FIELDS_MAP_NUM; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC; //(String)Name of the field where is store the description
	protected $FIELDS_MAP_STATE; //(String)Name of the field where is store the state
	protected $FIELDS_MAP_VERSION; //(String)Name of the field where is store the indices
	protected $FIELDS_MAP_FATHER; //(String)Name of the field where is store the id of the father

	protected $DOC_FILE_TABLE; //(String)
	protected $FILE_TABLE; //(String)
	protected $LINK_FILE_TABLE; //(String)
	protected $LINK_DOC_TABLE; //(String)
	protected $SPEC_FIELDS; //(String)
	protected $FIELDS; //(String)
	protected $FIELDS_MAPPING; //(String)

	//protected $DEFAULT_REPOSIT_DIR = ''; //(string)

	protected $core_props = array('file_only'=>false);

	protected $documents  = array(); //(Array Object document) documents of the Container
	protected $category   = false; //(Object category)
	//protected $doctype    = false; //(object rb_container_doctypeProcessLink)
	protected $doctypes   = array(); //(Array) definition of doctypes authorized for this Container
	protected $metadatas  = array(); //(Array) definition of extends metadatas of the Container
	protected $docfiles   = array(); //(Array recodfile) recordfile attached to this Container
	
	protected $_privileges = array(
                                'get',
                                'create',
                                'edit',
                                'suppress',
                                'admin', //links, history
                                'admin_users', //permissions
                                'document_get',
                                'document_create',
                                'document_edit', //checkin,checkout,assocfile, esit properties, mark to suppress
                                'document_suppress',
                                'document_unlock',
                                'document_edit_number', //change number of document or not
	); //(array)default privileges name

	protected static $_registry = array(); //(array) registry of constructed objects

	protected static $_extend_dao = array(); //array of objects Rb_Dao to access to extend table

	//-------------------------------------------------------------------------
	/** accesor to object
	 * Example :
	 * $Container = Rb_Container::get('bookshop', 1)->initDoc();
	 * If $id is < 0 then forced id to -1
	 * If $id = 0 then dont use the registry. This value is used bay not database saved object
	 *
	 * \param $space_name(string) name of space
	 * \param $container_id(integer) id of Container
	 */
	public static function get($space_name , $id = -1){
		if($id < 0) $id = -1; //forced to value -1
		//var_dump(self::$_registry[$space_name][$id]);die;
		if( !self::$_registry[$space_name][$id] ){
			switch($space_name){
				case(10):
					$space_name = 'bookshop';
				case('bookshop'):
					//require_once('core/Container/bookshop.php');
					if($id == 0)
					return new Rb_Container_Bookshop(0);
					else
					self::$_registry[$space_name][$id] = new Rb_Container_Bookshop($id);
					break;
				case(15):
					$space_name = 'cadlib';
				case('cadlib'):
					//require_once('core/Container/cadlib.php');
					if($id == 0)
					return new Rb_Container_Cadlib(0);
					else
					self::$_registry[$space_name][$id] = new Rb_Container_Cadlib($id);
					break;
				case(20):
					$space_name = 'mockup';
				case('mockup'):
					//require_once('core/Container/mockup.php');
					if($id == 0)
					return new Rb_Container_Mockup(0);
					else
					self::$_registry[$space_name][$id] = new Rb_Container_Mockup($id);
					break;
				case(25):
					$space_name = 'workitem';
				case('workitem'):
					//require_once('core/Container/workitem.php');
					if($id == 0)
					return new Rb_Container_Workitem(0);
					else
					self::$_registry[$space_name][$id] = new Rb_Container_Workitem($id);
					break;
				default:
					print 'WARNING Rb_Container::get > none space name defined';
					return false;
			}
		}
		return self::$_registry[$space_name][$id];
	}//End of method
	
	
	//-------------------------------------------------------------------------
	/** Get the default resource for current object
	 *
	 * @return Rb_Acl_Resource_Default
	 */
	public function getDefaultResource() {
		if (! Ranchbe::getAcl ()->has ( static::$_defaultResource_id ) ) {
			$defaultResource = new Rb_Acl_Resource_Default( static::$_defaultResource_id );
			$defaultResource->setPrivileges( $this->_privileges );
			Ranchbe::getAcl ()->add ( $defaultResource, Ranchbe::getAcl ()->getRootResource () );
		}else{
			$defaultResource = Ranchbe::getAcl ()->get( static::$_defaultResource_id );
		}
		return $defaultResource;
	} //End of method
	

	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Acl#_init($id)
	 */
	protected function _init($container_id){
		$this->_dao = new Rb_Dao( Ranchbe::getDb() );
		$this->_dao->setTable($this->OBJECT_TABLE);
		$this->_dao->setKey($this->FIELDS_MAP_ID, 'primary_key');
		$this->_dao->setKey($this->FIELDS_MAP_NUM, 'number');
		$this->_dao->setKey($this->FIELDS_MAP_NAME, 'name');
		$this->_dao->setKey($this->FIELDS_MAP_DESC, 'description');
		$this->_dao->setKey($this->FIELDS_MAP_STATE, 'state');
		$this->_dao->setKey($this->FIELDS_MAP_VERSION, 'version');
		$this->_dao->setKey($this->FIELDS_MAP_ITERATION, 'iteration');
		$this->_dao->setKey($this->FIELDS_MAP_FATHER, 'father');

		$this->core_props = array(
								$this->FIELDS_MAP_NUM => '',
								$this->FIELDS_MAP_NAME => '',
								$this->FIELDS_MAP_DESC => '',
								$this->FIELDS_MAP_STATE => 'init',
								$this->FIELDS_MAP_VERSION => 1,
								$this->FIELDS_MAP_ITERATION => 1,
								'default_process_id' => null,
								'open_date' => time(),
								'close_date' => null,
								'forseen_close_date' => time(),
								'close_by' => null,
								'open_by' => null,
								'owned_by' => null,
								'file_only' => false,
								'project_id' => null,
								'default_read_id' => null,
								);

		return parent::_init($container_id);
	}//End of method

	//----------------------------------------------------------
	/** Test if is a file manager or document manager Container type
	 * 
	 * @return Bool
	 */
	public function isFileOnly(){
		return $this->getProperty('file_only');
	}//End of method


	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#getProperty($property_name)
	 */
	public function getProperty($name){
		switch($name){
			case 'id':
			case 'container_id':
				return $this->_id;
				break;

			case 'name':
			case 'number':
			case 'container_number':
				$name = $this->FIELDS_MAP_NUM;
				break;

			case 'description':
			case 'container_description':
				$name = $this->FIELDS_MAP_DESC;
				break;

			case 'state':
			case 'container_state':
				$name = $this->FIELDS_MAP_STATE;
				break;

			case 'version':
			case 'container_version':
				$name = $this->FIELDS_MAP_VERSION;
				break;

			case 'read_id':
				$name = 'default_read_id';
				break;

		} //End of switch
		return $this->core_props[$name];
	}//End of method

	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#setProperty($name, $property_value)
	 */
	public function setProperty($name, $value){
		if($this->getId() < 0) return false; //Can not change property on object -1
		$this->isSaved = false;
		switch($name){
			case 'id':
			case 'container_id':
			case 'extend_id':
			case $this->FIELDS_MAP_ID:
				break; //this properties are not core properties

			case 'number':
			case 'container_number':
			case $this->FIELDS_MAP_NUM:
				$this->core_props[$this->FIELDS_MAP_NUM] = $value;
				break;

			case 'name':
			case 'container_name':
			case $this->FIELDS_MAP_NAME:
				$this->core_props[$this->FIELDS_MAP_NAME] = $value;
				break;

			case 'description':
			case 'container_description':
			case $this->FIELDS_MAP_DESC:
				$this->core_props[$this->FIELDS_MAP_DESC] = $value;
				break;

			case 'state':
			case 'container_state':
			case $this->FIELDS_MAP_STATE:
				$this->core_props[$this->FIELDS_MAP_STATE] = $value;
				break;

			case 'version':
			case 'container_version':
			case $this->FIELDS_MAP_VERSION:
				$this->core_props[$this->FIELDS_MAP_VERSION] = $value;
				break;

			case 'iteration':
			case 'container_iteration':
			case $this->FIELDS_MAP_ITERATION:
				$this->core_props[$this->FIELDS_MAP_ITERATION] = $value;
				break;

			case 'read_id':
			case 'default_read_id':
				if( $value ){
					$this->core_props['default_read_id'] = (int) $value;
				}else
				$this->core_props['default_read_id'] = null;
				break;

			case 'process_id':
			case 'default_process_id':
				if(!$value) $value=null; else $value=(int) $value;
				$this->core_props['default_process_id'] = $value;
				break;

			case 'open_date':
			case 'close_date':
			case 'forseen_close_date':
			case 'close_by':
			case 'open_by':
			case 'owned_by':
			case 'file_only':
			case 'project_id':
				$this->core_props[$name] = (int) $value;
				break;

			default:
				return false;
				break;
		} //End of switch

		return $this;

	}//End of method

	//--------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_create()
	 */
	protected function _create(){
		if( $this->_id !== 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}

		if ( !$this->getNumber() ){
			Ranchbe::getError()->push(Rb_Error::ERROR,
			array(),'number is not set');
			return false;
		}

		//Set current time
		$now = time();
		$this->setProperty('open_date', $now);
		$this->setProperty('update_date', $now);
		//Set current users
		$this->setProperty('open_by', Rb_User::getCurrentUser()->getId() );
		$this->setProperty('update_by', Rb_User::getCurrentUser()->getId() );
		$this->setProperty('state', 'init' );
		$this->setProperty('version', 1 );

		if ( !$this->getProperty('forseen_close_date') ){
			$this->setProperty('forseen_close_date',
				time() + Ranchbe::getConfig()->date->object->lifetime );
		}

		//Record object in database
		if( $this->_id = $this->_dao->create( $this->getProperties() ) ){
			$this->isSaved = true;
			self::$_registry[$this->space->getName()][$this->_id] =& $this;
		}else{
			Ranchbe::getError()->push(Rb_Error::ERROR, array( 'element'=>$this->number),
			tra('cant create the Container %element%') );
			return $this->_id = 0;
		}

		return $this->_id;

	}//End of method

	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_update()
	 */
	protected function _update(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}
		return $this->_dao->update( $this->getProperties() , $this->_id);
	}//End of method

	//--------------------------------------------------------------------
	/** This method can be used to get list of all Container.
	 * 	return a array or recordset if success, else return false.
	 * 
	 * @param Array $params
	 * @return Array|Bool
	 */
	public function getAll( $params = array() ){
		return $this->_dao->getAllBasic($params);
	}//End of method

	//--------------------------------------------------------
	/**
	 * This method can be used to get list of all document from a Container.
	 * return a array if success, else return false.
	 * 
	 * @param Array $params
	 * @param Bool $displayHistory
	 * @return Array|Bool
	 */
	public function getContents($params, $displayHistory=false){
		if( !$this->_id ){
			trigger_error('$this->_id is not set', E_USER_WARNING);
			return false;
		}
		
		if( $this->isFileOnly() ){
			$params['exact_find'][$this->FIELDS_MAP_ID] = $this->_id;
			return Rb_Recordfile::get($this->space->getName())->getAll( $params );
		}else{
			return Rb_Document::get($this->space->getName())
			->getDocuments( $params, $displayHistory, $this->_id );
		}
	}//End of method

	//--------------------------------------------------------------------
	/** This method can be used to get infos about a single Container.
	 * return a array if success, else return false.
	 * 
	 * @param Integer $container_id
	 * @param String  $space_name
	 * @param Array   $selectClose to define the sql select option
	 * @return Array
	 */
	public static function getInfos( $container_id, $space_name, array $selectClose=array()){
		return Rb_Container::get($space_name)->getDao()
							->getBasicInfos($container_id, $selectClose);
	}//End of method

	//---------------------------------------------------------------------------
	/**
	 * Get all doctypes linked to Container
	 * Return false if error or an array with the doctype properties
	 * 
	 * @param String $extension limit the result to doctype with extension $extension.
	 * @param String $type limit the result to doctype with type $type.
	 * @return Array|Bool
	 */
	public function getDoctypes($extension=0 , $type=0){
		if( !$this->_doctypes[$extension][$type] ){
			$this->_doctypes[$extension][$type] =
							Rb_Container_Relation_Doctype::get()
								->getDoctypes($extension,$type,$this->_id);
		}
		return $this->_doctypes[$extension][$type];
	}//End of method

	//----------------------------------------------------------
	/** helper method to get metadatas
	 * Get all metadatas linked to Container
	 * Return a array with properties of the metadatas
	 * 
	 * @param Array $params
	 * @return Array
	 */
	public function getMetadatas( array $params=array() ){
		return Rb_Metadata_Dictionary::get()->getMetadatas($this->getDao(), $params);
	}//End of method

	//----------------------------------------------------------
	/**
	 * Check if there is at least one doctype linked to Container
	 * Return false or true
	 * 
	 * @return Bool
	 */
	public function hasDoctype(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			trigger_error('$this->_id is not set', E_USER_WARNING);
			return false;
		}
		return Rb_Container_Relation_Doctype::get()->hasDoctypes($this->_id);
	}//End of method

	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Interface/Rb_Object_Interface_Related#getFather()
	 */
	public function getFather(){
		if( $this->father ) return $this->father;
		return false;
	}//End of method

	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Interface/Rb_Object_Interface_Related#setFather($father)
	 */
	public function setFather( &$father ){
		return $this->father =& $father;
	}//End of method

	//----------------------------------------------------------
	/** Define the father project of the Container.
	 * 
	 * @param Rb_Project $project
	 * @return Bool
	 */
	public function setProject(Rb_Project &$project){
		return false;
	}//End of method

	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#suppress()
	 */
	public function suppress(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id), tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}

		if($ret = $this->_dao->suppress($this->_id)){
			$this->_suppressPermanentObject();
			//Write history
			$this->writeHistory('Suppress');
			//clean registry
			unset( self::$_registry[$this->space->getName()][$this->_id] );
		}
		return $ret;
	}//End of method

}//End of class
