<?php

/**
 * Library of static method to manage date in RanchBE
 */
class Rb_Date{

	/**
	 *
	 * @param timestamp $timestamp on unix format
	 * @param string $model short|long or other key of config key date.format.$model
	 * @return string|false
	 */
	public static function formatDate($timestamp, $model = 'long') {
		//Take current Date en return the Date in lisible format
		if (! is_null ( $timestamp )) {
			$format = Ranchbe::getConfig()->date->format->$model;
			return strftime ( $format, $timestamp );
		} else
		return false;
	} //End

	/**
	 * 
	 * @param timestamp $TimeStamp
	 * @return string
	 */
	public static function getDateFromTS($TimeStamp) {
		$date ['d'] = date ( d, $TimeStamp );
		$date ['M'] = date ( M, $TimeStamp );
		$date ['Y'] = date ( Y, $TimeStamp );
		return $date;
	} //End
	
	/**
	 * 
	 * @param array $date
	 * @return timestamp
	 */
	public static function getTSFromDate(array $date) {
		//  [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]]
		$TimeStamp = mktime ( 0, 0, 0, $date ['M'], $date ['d'], $date ['Y'] );
		return $TimeStamp;
	} //End

	/**
	 * 
	 * @param string
	 * @return timestamp
	 */
	public static function makeTimestamp($string) {
		if (empty ( $string )) {
			$time = time (); // Use now
		} elseif (preg_match ( '/^\d{14}$/', $string )) {
			// it is mysql timestamp format of YYYYMMDDHHMMSS?
			$time = mktime ( substr ( $string, 8, 2 ), substr ( $string, 10, 2 ), substr ( $string, 12, 2 ), substr ( $string, 4, 2 ), substr ( $string, 6, 2 ), substr ( $string, 0, 4 ) );
		} elseif (is_numeric ( $string )) {
			$time = ( int ) $string; // it is a numeric string, we handle it as timestamp
		} else {
			// strtotime should handle it
			$time = strtotime ( $string );
			if ($time == - 1 || $time === false) {
				// strtotime() was not able to parse $string, use "now":
				$time = time ();
			}
		}
		return $time;
	}

} //End of class
