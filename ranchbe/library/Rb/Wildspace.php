<?php

/*! \brief Wildspace is the user working directory.
* Wildspace directory must be writable by the user. 
* The files to store in ranchbe must be copy in the Wildspace before.
*/
class Rb_Wildspace extends Rb_Directory {
	
	protected $_currentUserName;
	protected $_currentUserId;
	protected $_wildspace;
	
	function __construct(Rb_User &$user, $path=null) {
		$this->dbranchbe = & Ranchbe::getDb ();
		
		$this->_currentUserName = $user->getUsername ();
		$this->_currentUserId = $user->getId ();
		
		if(!$path){
			$wildspace_dir = Ranchbe::getConfig ()->path->reposit->wildspace;
			//construct the path to the personnal working directory for the current user.
			if (! empty ( $this->_currentUserName )) {
				$this->_wildspace = $wildspace_dir . '/' . $this->_currentUserName;
			} else {
				$this->_wildspace = $wildspace_dir . '/' . 'Anonymous';
			}
		}else{
			$this->_wildspace = $path;
		}
		
		if (! is_dir ( $this->_wildspace ))
			if (! mkdir ( $this->_wildspace, 0775, true )) {
				Ranchbe::getError()->push ( Rb_Error::ERROR, array ('element' => $this->_wildspace, 'debug' => array () ), 'can\'t create %element%' );
				die ();
			}
		
		$this->_wildspace = realpath($this->_wildspace);
	} //End of method
	
	//---------------------------------------------------------------------------
	/** Return the path of the Wildspace
	 * 
	 * @return string
	*/
	public function getPath() {
		return $this->_wildspace;
	} //End of method
	
	//---------------------------------------------------------------------------
	/** Set the path of the Wildspace
	 * 
	 * @param string $path
	*/
	function setPath($path) {
		if (! is_dir ( $path ))
			if (! mkdir ( $path, 0775, true )) {
				Ranchbe::getError()->push ( Rb_Error::ERROR, array ('path' => $path ), 'can\'t create %path%' );
			}
		$this->_wildspace = realpath($path);
	} //End of method
	
	//---------------------------------------------------------------------------
	/** Return the current user id
	 * 
	 * @return integer
	*/
	function getUserId() {
		return $this->_currentUserId;
	} //End of method
	

	//---------------------------------------------------------------------------
	/** Return the current user name
	 * 
	 * @return string
	*/
	function getUserName() {
		return $this->_currentUserName;
	} //End of method
	

	//---------------------------------------------------------------------------
	/** Suppress a file from the Wildspace.
	*
	* @param string $file name of the data to suppress.
	* @return boolean
	*/
	function suppressWildspaceFile($file) {
		$file = $this->_wildspace . '/' . $file;
		$odata = Rb_Fsdata::_dataFactory ( $file );
		return $odata->putInTrash ();
	} //End of method
	

	//-------------------------------------------------------
	/** Copy a upload file in Wildspace.
    *	Return true or false.
	*
	* Take the parameters of the uploaded file(from array $file) and create the file in the wildpspace.
	* @param array	$file :
			$file[name](string) name of the uploaded file
			$file[type](string) mime type of the file
			$file[tmp_name](string) temp name of the file on the server
			$file[error](integer) error code if error occured during transfert(0=no error)
			$file[size](integer) size of the file in octets
	* @param boolean $replace if true and if the file exist in the Wildspace, it will be replaced by the uploaded file.
	* 
	* @return Rb_File
	*/
	function uploadFile($file, $replace = false) {
		return Rb_File::uploadFile ( $file, $replace, $this->_wildspace );
	} //End of method


}//End of class

