<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

class Rb_Condition{
	protected $_condition = array(); //array of object Rb_Condition, subcondtion
	protected $_tests = array(); //array, tests to verify by Condition
	protected $_operator = 'or'; //or|and

	//----------------------------------------------------------------------------
	/**
	 * @param String $operator
	 * @return Void
	 */
	public function __construct($operator){
		$this->_operator = $operator;
	} //end of method

	//----------------------------------------------------------------------------
	/**
	 * 
	 * @param Rb_Condition $condition
	 * @return Void
	 */
	public function setCondition(Rb_Condition &$condition){
		$this->_condition[] =& $condition;
	} //end of method

	//----------------------------------------------------------------------------
	/**
	 * 
	 * @return Void
	 */
	public function getConditions(){
		return $this->_condition;
	} //end of method

	//----------------------------------------------------------------------------
	/**
	 * 
	 * @param String $property_name
	 * @param String $operator
	 * @param String $property_value
	 * @return unknown_type
	 */
	public function setTest($property_name, $operator, $property_value){
		$this->_tests[] = array($operator,
								$property_name,
								$property_value);
	} //end of method

	//----------------------------------------------------------------------------
	/**
	 * 
	 * @return Array
	 */
	public function getTests(){
		return $this->_tests;
	} //end of method

	//----------------------------------------------------------------------------
	/**
	 * 
	 * @param unknown_type $object
	 * @return Bool
	 */
	public function isValid($object){
		foreach($this->_condition as $condition){
			$result[] = $condition->isValid($object);
		}
		foreach($this->_tests as $test){
			switch ($test[0]){
				case '==':
					$result[] = $test[1] == $test[2];
					break;
				case '>':
					$result[] = $test[1] > $test[2];
					break;
				case '<':
					$result[] = $test[1] < $test[2];
					break;
				case '>=':
					$result[] = $test[1] >= $test[2];
					break;
				case '<=':
					$result[] = $test[1] <= $test[2];
					break;
			}
		}

		if($this->_operator == 'and'){
			$prod = (int) array_product($result);
			return $prod ? true : false;
		}else{
			$sum = (int) array_sum($result);
			return $sum == 0 ? false : true;
		}

	} //end of method

} //End of class

