<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
CREATE TABLE `partners` (
  `partner_id` int(11) NOT NULL,
  `partner_number` varchar(32) NOT NULL,
  `partner_type` enum('customer','supplier','staff') default NULL,
  `first_name` varchar(64) default NULL,
  `last_name` varchar(64) default NULL,
  `adress` varchar(64) default NULL,
  `city` varchar(64) default NULL,
  `zip_code` int(11) default NULL,
  `phone` varchar(64) default NULL,
  `cell_phone` varchar(64) default NULL,
  `mail` varchar(64) default NULL,
  `web_site` varchar(64) default NULL,
  `activity` varchar(64) default NULL,
  `company` varchar(64) default NULL,
  PRIMARY KEY  (`partner_id`),
  UNIQUE KEY `UC_partner_number` (`partner_number`)
) ENGINE=InnoDB;
*/

//require_once('core/dao/abstract.php');
//require_once('core/object/permanent.php');

/*! \brief a Partner is an entry of the ranchbe adress book.
*/
class Rb_Partner extends Rb_Object_Permanent {

  protected $SPACE_NAME   = 'Partner';
  protected $OBJECT_TABLE  = 'partners';
  protected $FIELDS_MAP_ID  = 'partner_id';
  protected $FIELDS_MAP_NUM = 'partner_number';
  protected $FIELDS_MAP_NAME = 'first_name';

  protected static $_registry = array(); //(array) registry of instances

	/** id of default parent resource
	 * 
	 * @var Integer
	 */
	protected static $_defaultResource_id = 9;
  
  //-------------------------------------------------------------------------
  function __construct($id=0){
    $this->_init($id);
  }//End of method

  //-------------------------------------------------------------------------
  /*! \brief init the properties of the object
  *  Return this
  * 
  * \param $project_id(integer) Id of the project
  */
  protected function _init($id){
    $this->_dao = new Rb_Dao( Ranchbe::getDb() );
    $this->_dao->setTable($this->OBJECT_TABLE, 'object');
    $this->_dao->setKey($this->FIELDS_MAP_ID, 'primary_key');
    $this->_dao->setKey($this->FIELDS_MAP_NUM, 'number');
    $this->_dao->setKey($this->FIELDS_MAP_NAME, 'name');
    return parent::_init($id);
  }//End of method

  //-------------------------------------------------------------------------
  /*! \brief init and clean current registry
  */
  public static function initRegistry(){
    self::$_registry = array();
  }

//-------------------------------------------------------------------------
  /*! \brief return a instance of object.
  *   if none parameter return a special instance with no possibilies to change here properties
  *   and wich can not be saved. This instance is just for use in static method where access to database is require,
  *   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
  *   If object "$id" dont exist return false.
  *   If object "$id" has already been instantiated, then return this instance (singleton pattern).
  * 
  */
  static function get($id = -1){
    if($id < 0) $id = -1; //forced to value -1
  
    if($id == 0)
      return new self(0);
  
    if( !self::$_registry[$id] ){
      self::$_registry[$id] = new self($id);
    }
    return self::$_registry[$id];
  }//End of method

//----------------------------------------------------------
  /*! \brief Get the property of the container by the property name.
  * 
  * \param $property_name(string)
  */
  function getProperty($property_name){
    switch($property_name){
      case 'number':
        $property_name = 'partner_number';
        break;
      case 'name':
        $property_name = 'first_name';
        break;
      case 'partner_number':
      case 'partner_type':
      case 'first_name':
      case 'last_name':
      case 'adress':
      case 'city':
      case 'zip_code':
      case 'phone':
      case 'cell_phone':
      case 'mail':
      case 'web_site':
      case 'activity':
      case 'company':
        break;
    }
    if( array_key_exists($property_name, $this->core_props) )
      return $this->core_props[$property_name];
  }//End of method

//-------------------------------------------------------------------------
  /*! \brief set the property $property_name with value of $property_value
  * can not call this method from instance where id < 0.
  * Return $this, so you can use sythax type $object->setProperty($property_name, $property_value)->getProperty($property_name);
  *
  */
  public function setProperty($property_name, $property_value){
    if($this->getId() < 0) return false; //Can not change property on object -1
    $this->isSaved = false;
    switch($property_name){
      case 'number':
        $property_name = 'partner_number';
        break;
      case 'name':
        $property_name = 'first_name';
        break;
      case 'partner_number':
      case 'partner_type':
      case 'first_name':
      case 'last_name':
      case 'adress':
      case 'city':
      case 'zip_code':
      case 'phone':
      case 'cell_phone':
      case 'mail':
      case 'web_site':
      case 'activity':
      case 'company':
        break;
      default:
        return false;
        break;
    }
    $this->core_props[$property_name] = $property_value;
    return $this;
  }//End of method

//----------------------------------------------------------
  /*!\brief
   * Get list of Partner
   *
   \param $params(array) is use for manage the display. See parameters function of getQueryOptions()
  */
  public function getAll( $params=array() ){
    return $this->_dao->getAllBasic( $params );
  }//End of method

//----------------------------------------------------------
  protected function _create(){
    if( $this->_id !== 0 ){
      Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id), 
                        tra('can not performed this action on object id %id%'));
      trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
      return false;
    }

    if(!$this->getProperty('partner_number')){
      $this->setProperty('partner_number', self::composeNumber(
                                              $this->getProperty('first_name'),
                                              $this->getProperty('last_name')
                                              )
                        );
    }
  
    //Create a basic object
    if( $this->_id = $this->_dao->create( $this->getProperties() ) ){
      self::$_registry[$this->_id] =& $this;
      $this->isSaved = true;
    }else{
      Ranchbe::getError()->push(Rb_Error::ERROR, 
                                        array( 'element'=>$this->getNumber() ),
                                                tra('cant create %element%') );
      return $this->_id = 0;
    }
    return $this->_id;
  }//End of method

//-------------------------------------------------------------------------
  /*!\brief write modification in database.
  * Return true or false.
  *   
  */
  protected function _update(){
    if( $this->_id < 1 ){
      Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id), 
                      tra('can not performed this action on object id %id%'));
      trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
      return false;
    }

    $this->setProperty('partner_number', self::composeNumber(
                                            $this->getProperty('first_name'),
                                            $this->getProperty('last_name')
                                            )
                      );

    return $this->_dao->update( $this->getProperties() , $this->_id);
  }//End of method

//----------------------------------------------------------
  /*!\brief
   * Suppress current object
   *
  */
  function suppress(){
    if( $this->_id < 1 ){
      Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
                          tra('can not performed this action on object id %id%'));
      trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
      return false;
    }
  
    if($ret = $this->_dao->suppress($this->_id)){
      $this->_suppressPermanentObject();
      //clean registry
      unset(self::$_registry[$this->_id]);
    }
    return $ret;
  }//End of method

//-------------------------------------------------------------
  /*!\brief Get number or other single infos about a Partner.
   * return a string if success, else return false.
   *    
    \param $id(integer) id of the Partner.
  */
  function getName($id){
  	if (is_null ($id) && !is_numeric($id)){
     Ranchbe::getError()->push(Rb_Error::ERROR, array(), 'Invalid id' );
  	}
  	return $this->_dao->getBasicNumber($id);
  }//End of method

//---------------------------------------------------------------------------
  /*!\brief Import partners from a csv file.
  * Return true if no errors or false.
  * 
  * The first line of the csv file define the field of the partners table. The next lines content the values.
    \param $csvfile(string) path to csv file.
    \param  $replace(bool) if true, the existing Partner will be modified.
  */
  function ImportPartnerCsv( $csvfile , $replace = true) {
    //require_once('core/import.php');
    $records = Rb_Import::importCsv($csvfile);
  
  	foreach ($records as $p) {
      $partner = new Rb_Partner(0);
      foreach($p as $name=>$value){
        $partner->setProperty($name,$value);
      }
      if(!$partner->save()){
        $err[] = $p;
      }
    }
  }//End of method
  
//---------------------------------------------------------------------------
  /*!\brief Test if the Partner exist.
   * return partner_id, else return false.
   *    
    \param $number(string) Partner number to check.
  */  
  function ifExist($number) {
    $id=$this->_dao->getBasicId($number);
    if(!$id) return false;
    else return $id;
  }//End of method

//---------------------------------------------------------------------------
  /*!\brief Compose the Partner number from the first and last name.
   * return partner_number, else return false.
   *    
    \param $first_name(string) Partner first name.
    \param $last_name(string) Partner last name.
  */  
  static function composeNumber($first_name , $last_name){
    if(!empty($first_name) && !empty($last_name) )
       $separator = '_';
    return $partner_number = self::no_accent($first_name . $separator . $last_name);
  }//End of method
  
//---------------------------------------------------------------------------
  /*!\brief Suppress accent of the input string.
   * return the input without accents, else return false.
   *    
    \param $in(string) input string.
  */  
  static function no_accent($in) {
    //thank to 'http://www.wikistuce.info/doku.php/php/supprimer_tous_les_caracteres_speciaux_d-une_chaine'
  	$search = array ('@[����]@','@[���]@','@[��]@','@[���]@','@[��]@','@[����]@','@[���]@','@[��]@','@[��]@','@[��]@','@[�]@i','@[�]@i','@[ ]@i','@[^a-zA-Z0-9_]@');
  	$replace = array ('e','a','i','u','o','E','A','I','U','O','c','C','_','');
  	return preg_replace($search, $replace, $in);
  }//End of method


}//End of class


