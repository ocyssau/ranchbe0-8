<?php

//require_once('Zend/Acl.php');
//require_once('Zend/Acl/Role.php');
//require_once('Zend/Acl/Resource.php');
//require_once('core/group.php');
//require_once('core/user.php');

class Rb_Acl extends Zend_Acl {

	protected $dbranchbe;
	static protected $aclDao = false;
	static protected $isInit = false;

	/* default resource name to define access to basic function of ranchbe*/
	const ROOT_RESOURCE = 1;

	//const INHERIT = 0;
	//const TYPE_ALLOW = 1;
	//const TYPE_DENY = 2;

	static protected $privileges = array(
		    'get'=>1,
		    'edit'=>2,
		    'create'=>3,
		    'suppress'=>4,
		    'admin'=>5,
		    'document_get'=>21,
		    'document_edit'=>22, //checkin,checkout,assocfile,edit properties,mark to suppress
		    'document_create'=>23,
		    'document_suppress'=>24,
		    'document_admin'=>25,
		    'document_unlock'=>26,
		    'document_edit_number'=>27, //change number of document or not
		    'admin_users'=>100,
		    'admin_doctype'=>110,
		    'admin_process'=>120,
		    'admin_partner'=>130,
	);
	static protected $privileges_by_id = false;

	
	//-------------------------------------------------------------------------
	/** Constructor
	 * 
	 * @return Rb_Acl
	 */
	public function __construct(){
		$this->dbranchbe =& Ranchbe::getDb();
		return $this;
	} //End of method

	
	//-------------------------------------------------------------------------
	/** Init the groups and current user
	 * 
	 * @return Rb_Acl
	 */
	public function init(){
		if(self::$isInit) trigger_error('acls are yet init', E_USER_WARNING);
		self::$isInit = true;
		
		//Create default resources
		//require_once('Rb/Acl/Resource/Default.php');
		$this->add( new Rb_Acl_Resource_Default( self::ROOT_RESOURCE ) );

		$this->_initGroupRoles();
		//return $this;

		//Get current user and create role
		$user_groups = array();
		foreach(Rb_User::getCurrentUser()->getGroups() as $group_id){
			$user_groups[] = Rb_Group::getStaticRoleId($group_id);
		}
		
		//var_dump( Rb_User::getCurrentUser()->getRoleId() ); die;
		$this->addRole( Rb_User::getCurrentUser(), $user_groups );

		//init privileges for resources actualy defined
		$this->initPrivileges( array_keys($this->_resources) );

		//var_dump( $this->_resources );die;
		//var_dump( $this );
		//var_dump( $this->_getRules( $this->get('ranchbe'), $this->getRole('group_2') ) );
		//var_dump( $this->_getRules( $this->get('ranchbe'), $this->getRole('group_2') ) );
		//var_dump( $this->_rules );
		//die;
		return $this;
	} //End of method

	
	//-------------------------------------------------------------------------
	/** Return the root ressource object
	 * 
	 * @return Rb_Acl
	 */
	public function getRootResource (){
		return $this->get( self::ROOT_RESOURCE );
	} //End of method

	
	//-------------------------------------------------------------------------
	/** Save rules in database
	 * 
	 * @param Int $role_id
	 * @param Int $resource_id
	 * @param Array $privileges
	 * @return unknown_type
	 */
	public function save( $role_id, $resource_id, Array $privileges ){
		$role =& $this->getRole($role_id);
		$resource =& $this->get($resource_id);
		foreach ( $privileges as $privilege=>$type) {
			switch($type){
				case Zend_Acl::TYPE_ALLOW:
					$this->removeDeny($role, $resource, $privilege);
					$this->allow($role, $resource, $privilege);
					break;
				case Zend_Acl::TYPE_DENY:
					$this->deny($role, $resource, $privilege);
					break;
				default:
					$this->removeAllow($role, $resource, $privilege);
					$this->removeDeny($role, $resource, $privilege);
					break;
			}
		}
		return self::_getAclDao()->save($role_id, $resource_id, $privileges);
	} //End of method

	
	//-------------------------------------------------------------------------
	/** Get rules
	 * 
	 * @param String $resource
	 * @param String $role
	 * @return Array
	 */
	public function getRules($resource, $role){
		$rules = $this->_getRules($this->get($resource), $this->getRole($role));
		$ret = array();
		foreach($this->get($resource)->getPrivileges() as $privilege){
			$privilege = self::getPrivilegeId($privilege);
			if( !$rules['byPrivilegeId'][$privilege] ){
				$ret[$privilege]['type'] = 'INHERIT';
			}else{
				$ret[$privilege] = $rules['byPrivilegeId'][$privilege];
			}
		}
		return $ret;
	} //End of method

	
	//-------------------------------------------------------------------------
	/** Get privileges from database and create rules
	 * 
	 * @param Array|String $resource_ids ressource id to init or string of the resource id
	 * @return Bool
	 */
	public function initPrivileges( $resource_ids ){
		if(!is_array($resource_ids)) $resource_ids = array($resource_ids);

		//Privileges base query
		$query = 'SELECT privilege, resource_id, role_id, rule FROM acl_rules';
		//add sql close to query each ressource_ids by OR close
		$i=1;
		while($i <= count( $resource_ids ) ){
			if( $i == 1 ) $query .= ' WHERE ';
			$query .=  'resource_id = \''.$resource_ids[$i-1].'\'';
			if($i != count($resource_ids) ) $query .= ' OR ';
			$i++;
		}

		$rs = $this->dbranchbe->execute( $query );
		if($rs){
			while (!$rs->EOF) {
				//var_dump($rs->fields['privilege'], $rs->fields['role_id'], $rs->fields['resource_id'], $rs->fields['rule']);
				if( empty($rs->fields['role_id']) ) $rs->fields['role_id'] = null;
				if( empty($rs->fields['resource_id']) ) $rs->fields['resource_id'] = null;
				else if($rs->fields['rule'] == Zend_Acl::TYPE_ALLOW){
					$this->allow($rs->fields['role_id'], $rs->fields['resource_id'], $rs->fields['privilege'] );
				}
				else if($rs->fields['rule'] == Zend_Acl::TYPE_DENY){
					$this->deny($rs->fields['role_id'], $rs->fields['resource_id'], $rs->fields['privilege'] );
				}
				//add default rules
				//$this->allow($rs->fields['role_id'], self::ROOT_RESOURCE, 'get' );
				$rs->MoveNext();
			}
			return true;
		}else{
			Ranchbe::getErrorstack()->push(ERROR_DB, 'Fatal', array(),
			$this->dbranchbe->ErrorMsg() );
			return false;
		}
	}//End of method

	
	//-------------------------------------------------------------------------
	/** Init roles from groups record in database
	 * 
	 * @return Bool
	 */
	protected function _initGroupRoles(){
		//Get all groups from database
		$query = 'SELECT sub.group_id AS father_id, sub.subgroup_id AS son_id, grp.group_id
            FROM rb_groups AS grp
            LEFT OUTER JOIN rb_group_subgroups AS sub 
            ON grp.group_id = sub.subgroup_id';
		$rs = $this->dbranchbe->execute( $query );
		$groups = array();
		if($rs){
			while (!$rs->EOF) { //transform result to return array like group_id=>(array)fathers_ids
				if($rs->fields['son_id'] && $rs->fields['son_id'] != $rs->fields['father_id']){
					$groups[$rs->fields['son_id']][] = $rs->fields['father_id'];
				}else{
					$groups[$rs->fields['group_id']] = array();
				}
				$rs->MoveNext();
			}
		}else{
			Ranchbe::getErrorstack()->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg() );
			return false;
		}

		//Create groups rules
		foreach($groups as $group_id=>$fathers_ids){
			if(!empty($group_id)){
				$this->_recursiveAddRoles($group_id, $fathers_ids, $groups );
			}
		}
		return true;
	}//End of method

	
	//-------------------------------------------------------------------------
	/** Add recursivly groups childs to fathers groups
	 * 
	 * @param Integer $group_id
	 * @param Array $father_ids
	 * @param Array $registry registry is an associative where key = group_id and value = an array
	 * 							to define group_id and father_id of all groups to add
	 * @return unknown_type
	 */
	protected function _recursiveAddRoles($group_id, array $father_ids, array $registry){
		$fathers = array();
		if($father_ids)
		foreach( $father_ids as $father_id){
			if(!$this->hasRole( Rb_Group::getStaticRoleId($father_id) )
			&& $group_id != $father_id ){
				if( is_array($registry[$father_id]) )
				$this->_recursiveAddRoles($father_id, $registry[$father_id], $registry );
				else
				$this->_recursiveAddRoles($father_id, array(), $registry );
			}
			$fathers[] =& Rb_Group::get($father_id);
		}
		if(!$this->hasRole( Rb_Group::getStaticRoleId($group_id) ) && $group_id ){
			$this->addRole( Rb_Group::get($group_id), $fathers );
		}
	}//End of method

	
	//-------------------------------------------------------------------------
	/** return an instance of Rb_Acl_Dao used to access data in database.
	 *   this method is a singleton factory method so return always the same instance
	 *   of Rb_Acl_Dao
	 * 
	 * @return Rb_Acl_Dao
	 */
	protected static function _getAclDao(){
		if(!self::$aclDao){
			self::$aclDao = new Rb_Acl_Dao();
		}
		return self::$aclDao;
	}

	
	//-------------------------------------------------------------------------
	/** Return id from privilege name $name
	 * 
	 * @param String $name
	 * @return Integer
	 */
	public static function getPrivilegeId($name){
		/*
		 if(!self::$privileges){
		 $dao = new rb_dao(Ranchbe::getDb());
		 $dao->setTable('privileges');
		 self::$privileges = $dao->getAll();
		 }
		 */
		if(!self::$privileges[$name]) return $name;
		return self::$privileges[$name];
	}

	
	//-------------------------------------------------------------------------
	/** Return name from privilege id
	 * 
	 * @param Integer $id
	 * @return String
	 */
	public static function getPrivilegeName($id){
		if(!self::$privileges_by_id)
		self::$privileges_by_id = array_flip(self::$privileges);
		if(!self::$privileges_by_id[$id]) return $id;
		return self::$privileges_by_id[$id];
	}

	
	//-------------------------------------------------------------------------
	/** return name from resource id
	 * 
	 * @param Integer $id
	 * @return String
	 */
	public static function getResourceName($id){
		if(!self::$resources_by_id)
		self::$resources_by_id = array_flip(self::$resources);
		if(!self::$resources_by_id[$id]) return $id;
		return self::$resources_by_id[$id];
	}


} //End of class
