<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


/*
CREATE TABLE `objects` (
  `object_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `space_id` int(11) NOT NULL,
  PRIMARY KEY (`object_id`),
  KEY `INDEX_objects_1` (`space_id`)
) ENGINE=InnoDB;

CREATE TABLE `objects_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB;
INSERT INTO `objects_seq` (`id`) VALUES
(100);
*/

/** Permanent object must be stored in database, 
 *  he has a dependance to rb_dao_abstract class
 *
 *
 */
//require_once('core/observable.php');
//require_once('core/dao.php');


abstract class Rb_Object_Permanent extends Rb_Observable {

	/** Id of current object
	 * 
	 * @var integer
	 */
	protected $_id = 0;
	
	/**
	 * 
	 * @var Rb_Dao_Abstract
	 */
	protected $_dao = false;
	
	/** Properties of current object
	 * 
	 * @var array
	 */
	protected $core_props = array ();
	
	/** Extends properties of current object
	 * 
	 * @var array
	 */
	protected $extend_props = array ();
	
	/**
	 * 
	 * @var unknown_type
	 */
	protected $_father = false;
	
	/**
	 * 
	 * @var unknown_type
	 */
	protected $space = false;
	
	/**
	 * 
	 * @var Boolean
	 */
	public $stopIt = false;
	
	/**
	 * 
	 * @var Boolean
	 */
	protected $isSaved = false;
	
	protected $_preCreateEventName = 'pre_create';
	protected $_postCreateEventName = 'post_create';
	protected $_preUpdateEventName = 'pre_update';
	protected $_postUpdateEventName = 'post_update';
	protected $SEQ_NAME = 'objects_seq';
	protected $MIN_SEQ_NUMBER = 100;
	
	/** Define the id for each sub-class of Rb_Object_Permanent
	 * 
	 * @var array
	 */
	protected static $_class_registry = array (
										1 =>  'Rb_Object_Acl', 
										2 =>  'Rb_User', 
										3 =>  'Rb_Group', 
										10 => 'Rb_Project', 
										20 => 'Rb_Container_Bookshop', 
										21 => 'Rb_Container_Cadlib', 
										22 => 'Rb_Container_Mockup', 
										23 => 'Rb_Container_Workitem', 
										30 => 'Rb_Document', 
										31 => 'Rb_Document_Version', 
										32 => 'Rb_Document_Iteration', 
										40 => 'Rb_Recordfile', 
										41 => 'Rb_Recordfile_Version', 
										42 => 'Rb_Recordfile_Iteration', 
										50 => 'Rb_Docfile', 
										51 => 'Rb_Docfile_Version', 
										52 => 'Rb_Docfile_Iteration', 
										60 => 'Rb_Reposit', 
										61 => 'Rb_Reposit_Read', 
										70 => 'Rb_Partner', 
										80 => 'Rb_Workflow', 
										81 => 'Rb_Workflow_Docflow', 
										90 => 'Rb_Workflow_Process', 
										100 => 'Rb_Category', 
										110 => 'Rb_Doctype', 
										120 => 'Rb_Metadata_Dictionary', 
										130 => 'Rb_Container_Notification', 
										140 => 'Rb_Import_History' );

/*										
	protected static $_class_registry = array (
										1 => 'rb_object_acl', 
										10 => 'rb_project', 
										20 => 'rb_container_bookshop', 
										21 => 'rb_container_cadlib', 
										22 => 'rb_container_mockup', 
										23 => 'rb_container_workitem', 
										30 => 'rb_document', 
										31 => 'rb_document_version', 
										32 => 'rb_document_iteration', 
										40 => 'rb_recordfile', 
										41 => 'rb_recordfile_version', 
										42 => 'rb_recordfile_iteration', 
										50 => 'rb_docfile', 
										51 => 'rb_docfile_version', 
										52 => 'rb_docfile_iteration', 
										60 => 'rb_reposit', 
										61 => 'rb_reposit_read', 
										70 => 'rb_partner', 
										80 => 'rb_workflow', 
										81 => 'rb_workflow_docflow', 
										90 => 'rb_workflow_process', 
										100 => 'rb_category', 
										110 => 'rb_doctype', 
										120 => 'rb_metadata_dictionary', 
										130 => 'rb_container_notification', 
										140 => 'rb_import_history' );
										*/
										
	//-------------------------------------------------------------------------
	public function __set($name, $value) {
		return $this->setProperty ( $name, $value );
	}
	
	//-------------------------------------------------------------------------
	public function __get($name) {
		return $this->getProperty ( $name );
	}
	
	//-------------------------------------------------------------------------
	/**
	 */
	public function __wakeup(){
		$this->_dao->reconnect( Ranchbe::getDb() );
	}//End of method
	
	//-------------------------------------------------------------------------
	/*
	  static function get($id = -1){
	    if($id < 0) $id = -1; //forced to value -1
	
	    if($id == 0)
	      return new self(0);
	
	    if( !self::$_registry[$id] ){
	      self::$_registry[$id] = new rb_project($id);
	    }
	    return self::$_registry[$space_id][$class_id][$id];
	    //return self::$_registry[$object_id];
	  }//End of method
	  */
	
	//-------------------------------------------------------------------------
	/**! \brief init current instance, populate properties from database,
	 *       init access to database parameters.
	 * Return array.
	 * 
	 */
	protected function _init($id) {
		$this->_dao->setSequence ( $this->SEQ_NAME );
		$this->_dao->setMinSequenceNumber ( $this->MIN_SEQ_NUMBER );
		if ($id > 0) {
			$this->_id = ( int ) $id;
			$this->_initProperties ();
			if (! $this->core_props) {
				trigger_error ( 'this object dont exist' , E_USER_ERROR );
			}
		} else if ($id < 0) {
			$this->_id = - 1;
		} else {
			$this->_id = 0;
		}
		return true;
	} //End of method
	

	//-------------------------------------------------------------------------
	/**! \brief get properties from database
	 *   return true
	 */
	protected function _initProperties() {
		$this->core_props = $this->_dao->getBasicInfos ( $this->_id );
		unset ( $this->core_props [$this->FIELDS_MAP_ID] );
		return true;
	} //End of method
	
	
	//--------------------------------------------------------------------
	/**!\brief create a new database record for object.
	 *   Return the id if no errors, else return FALSE
	 *
	 */
	abstract protected function _create();
	
	//-------------------------------------------------------------------------
	/**!\brief write modification in database.
	 * Return true or false.
	 *   
	 */
	abstract protected function _update();
	
	//-------------------------------------------------------------------------
	/**! \brief Create a new Permanent object definition in objects table
	 * 
	 */
	protected function _createPermanentObject() {
		$id = $this->_dao->setSeqId (); //init the id on current dao object
		Ranchbe::getError()->info('New object id ' . $id);
		$objectdao = new Rb_Dao ( Ranchbe::getDb () );
		$objectdao->setTable ( 'objects', 'object' );
		$objectdao->setKey ( 'object_id', 'primary' );
		$objectdao->setSequence ( 'none' );
		$data = array (	'object_id' => $id, 
						'class_id' => $this->getClassId () );
		if (is_a ( $this->space, 'Rb_Space' )) {
			$data ['space_id'] = $this->space->getId ();
		} else {
			$data ['space_id'] = 0;
		}
		$ok = $objectdao->create ( $data );
		if($ok) $ok = $this->_create ();
		return $ok;
	} //End of method
	

	//-------------------------------------------------------------------------
	/**! \brief Suppress Permanent object definition in objects table
	 * 
	 */
	protected function _suppressPermanentObject() {
		$dao = new Rb_Dao ( Ranchbe::getDb () );
		$dao->setTable ( 'objects', 'object' );
		$dao->setKey ( 'object_id', 'primary' );
		$params = array ('exact_find' => array ('object_id' => $this->_id, 'class_id' => $this->getClassId () ) );
		if (is_a ( $this->space, 'Rb_Space' )) {
			$params ['exact_find'] ['space_id'] = $this->space->getId ();
		} else {
			$params ['exact_find'] ['space_id'] = 0;
		}
		if ($dao->suppressQuery ( $params )) {
			return true;
		} else
			return false;
	} //End of method

	//-------------------------------------------------------------------------
	/**! \brief return array of properties of the current object.
	 *
	 */
	public function getProperties() {
		if (! isset ( $this->core_props ))
			return false;
		else {
			$ret = $this->core_props;
			$ret [$this->FIELDS_MAP_ID] = $this->_id;
			return $ret;
		}
	} //End of method

	//-------------------------------------------------------------------------
	/**! \brief return value of property
	 * can not call this method from instance where id < 0
	 */
	/*exemple of implementation
	  public function getProperty($property_name){
	    if( array_key_exists($property_name, $this->core_props) ) //prefer array_key_exists to isset to accept values NULL
	      return $this->core_props[$property_name];
	  }
	  */
	abstract public function getProperty($property_name);
	
	//-------------------------------------------------------------------------
	/**! \brief set the property $property_name with value of $property_value
	 * can not call this method from instance where id < 0.
	 * Return $this, so you can use sythax type $object->setProperty($property_name, $property_value)->getProperty($property_name);
	 *
	 */
	/*exemple of implementation
	  public function setProperty($property_name, $property_value){
	    $this->core_props[$property_name] = $property_value;
	    return $this;
	  }
	  */
	abstract public function setProperty($property_name, $property_value);

	
	//-------------------------------------------------------------------------
	/**! \brief unset the property
	 *
	 */
	public function unsetProperty($property_name) {
		unset ( $this->core_props [$property_name] );
		return $this;
	}
	
	//-------------------------------------------------------------------------
	/**! \brief Record properties of current instance in database.
	 * If object exist ($id > 0), call update method.
	 * If object is not already recorded, call create method and return an initialized instance.
	 * 
	 */
	public function save($notify = true) {
		Ranchbe::getError()->push ( Rb_Error::INFO, array (),
			"Save $this->getName()" );
		if ($notify)
			$this->_initObservers ();
		if ($this->_id < 0) {
			Ranchbe::getLogger()->log('Id is not valid', 4);
			return false;
		} else if ($this->_id == 0) {
			if ($notify) {
				$this->notify_all ( $this->_preCreateEventName, $this );
				if ($this->stopIt){
					Ranchbe::getLogger()->log("Stop on $this->_preCreateEventName", 4);
					return false; //use by observers to control execution
				}
			}
			Ranchbe::getDb ()->StartTrans ();
			self::_createPermanentObject ();
			if ($this->_id) {
				Ranchbe::getDb ()->CompleteTrans ();
				if ($notify) {
					$this->notify_all ( $this->_postCreateEventName, $this );
					if ($this->stopIt){
						Ranchbe::getLogger()->log("Stop on $this->_postCreateEventName", 4);
						return false; //use by observers to control execution
					}
				}
				return $this;
			} else{
				Ranchbe::getDb ()->FailTrans ();
				Ranchbe::getDb ()->CompleteTrans ();
				Ranchbe::getLogger()->log("_create failed");
				return false;
			}
		} else if ($this->_id > 0) {
			if ($notify) {
				$this->notify_all ( $this->_preUpdateEventName, $this );
				if ($this->stopIt){
					Ranchbe::getLogger()->log("Stop on $this->_preUpdateEventName", 4);
					return false; //use by observers to control execution
				}
			}
			if ($this->_update ()) {
				if ($notify) {
					$this->notify_all ( $this->_postUpdateEventName, $this );
					if ($this->stopIt){
						Ranchbe::getLogger()->log("Stop on $this->_preUpdateEventName", 4);
						return false; //use by observers to control execution
					}
				}
				return $this;
			} else{
				Ranchbe::getLogger()->log("_update failed");
				return false;
			}
		}
	} //End of method
	

	//-------------------------------------------------------------------------
	/**! \brief suppress current object database records, files, and all depandancies
	 * and clean the registry of current class to prevent recall of this instance.
	 * can not call this method from instance where id <= 0.
	 * Return false or true.
	 * 
	 */
	/*example of implementation, the use of self::$_registry forbidden to implements this method here
	  public function suppress(){
	    if( $this->_id < 1 ){
	      $this->error_stack->push(Rb_Error::ERROR, array('id'=>$this->_id), tra('can not performed this action on object id %id%'));
	      trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
	      return false;
	    }
	  
	    if($ret = $this->_basicSuppress($this->_id)){
	      //Write history
	      $this->writeHistory('Suppress');
	      //clean registry
	      unset( self::$_registry[$this->_id] );
	    }
	    return $ret;
	  }//End of method
	  */
	abstract public function suppress();
	
	//-------------------------------------------------------------------------
	/**! \brief return id of object
	 * 
	 */
	public function getId() {
		return $this->_id;
	} //End of method
	

	//-------------------------------------------------------------------------
	/**! \brief set name of current object
	 * 
	 */
	public function setName($name) {
		return $this->setProperty ( 'name', $name );
	} //End of method
	

	//--------------------------------------------------------
	/**! \brief return name of current object
	 */
	public function getName() {
		return $this->getProperty ( 'name' );
	} //End of method
	

	//-------------------------------------------------------------------------
	/**! \brief set name of current object
	 * 
	 */
	public function setNumber($num) {
		return $this->setProperty ( 'number', $num );
	} //End of method
	

	//--------------------------------------------------------
	/**! \brief return name of current object
	 */
	public function getNumber() {
		return $this->getProperty ( 'number' );
	} //End of method
	

	//-------------------------------------------------------------------------
	/**!\brief
	 *   
	 */
	public function getSpaceName() {
		return $this->space->getName ();
	}
	
	//-------------------------------------------------------------------------
	/**!\brief
	 *   
	 */
	public function getSpace() {
		return $this->space;
	}
	
	//-------------------------------------------------------------------------
	/**!\brief
	 *   
	 */
	public function getDao() {
		return $this->_dao;
	}
	
	//-------------------------------------------------------------------------
	/**!\brief return a uniq identifier for the current class
	 *   
	 */
	public function getClassId() {
		$cr = array_flip ( Rb_Object_Permanent::$_class_registry );
		return $cr [  get_class ( $this )  ];
		//return $cr [strtolower ( get_class ( $this ) )];
	}
	
	//-------------------------------------------------------------------------
	/**!\brief return the class name from identifier
	 *   
	 */
	public static function getClassName($id) {
		return Rb_Object_Permanent::$_class_registry [$id];
	}

} //End of class
