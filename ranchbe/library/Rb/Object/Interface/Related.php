<?php
/** A Related object is in hierachical relation with an other object
 *  A Related object may have one father
 *
 */ 
interface Rb_Object_Interface_Related{

  //-------------------------------------------------------------------------
  /*!\brief set father of current object
  * Return $this or false.
  *   
  */
  public function setFather(&$father);

  //-------------------------------------------------------------------------
  /*!\brief get the father object of current object
  * Return an object or false.
  *   
  */
  public function getFather();
  
}
