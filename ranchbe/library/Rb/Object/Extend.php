<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/** Extends Rb_Object_Permanent properties
 *
 */
class Rb_Object_Extend{

	protected $_id = false; //(int)
	protected $core_props = array(); //(array)
	protected $extend_props = array(); //(array)
	protected $_dictionary = array(); //(array)
	protected $_dao = false; //(Rb_Dao)
	protected $_permanent = false; //(Rb_Object_Permanent)

	//-------------------------------------------------------------------------
	/*! \brief constructor
	 *
	 */
	public function __construct(Rb_Object_Permanent &$permanent){
		$this->_dao = new Rb_Dao(Ranchbe::getDb());
		$this->_dao->setTable(self::getExtendTable($permanent->getDao()));
		$this->_dao->setKey('extend_id');
		$this->_dao->setSequence('none');
		$this->_permanent =& $permanent;
		$id = $permanent->getId();
		if( $id > 0 ){
			$this->_id = (int) $id;
			$this->core_props = $this->_permanent->getProperties();
			$this->extend_props = $this->_dao->getBasicInfos($this->_id, array());
		}else if( $id < 0 ){
			$this->_id = -1;
			$this->core_props = $this->_permanent->getProperties();
		}else{
			$this->_id = 0;
			$this->core_props = $this->_permanent->getProperties();
			$this->extend_props = array();
			//var_dump($this->core_props,$this->extend_props);
		}
		return true;
	} //End of method

	//----------------------------------------------------------
	/*! \brief Magic php5 method
	 *
	 */
	public function __set($name, $value){
		return $this->setProperty($name, $value);
	}//End of method

	//----------------------------------------------------------
	/*! \brief Magic php5 method
	 *
	 */
	public function __get($name){
		return $this->getProperty($name);
	}//End of method

	//----------------------------------------------------------
	/*! \brief Magic php5 method
	 *
	 */
	public function __call($name, array $args){
		//var_dump($name,$args);
		if($args)
		$args = implode(',',$args);
		return $this->_permanent->$name($args);
	}//End of method

	//----------------------------------------------------------
	/*! \brief Magic php5.1 method
	 *
	 */
	public function __isset($name){
		if(isset($this->core_props[$name])
		|| isset($this->extend_props[$name])){
			return true;
		}else
		return false;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Magic php5.1 method
	 *
	 */
	public function __unset($name){
		if(isset($this->core_props[$name])){
			$this->core_props[$name] = null;
			$this->_permanent->setProperty($name,null);
		}
		if(isset($this->extend_props[$name])){
			$this->extend_props[$name] = null;
		}
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get the Extend table name from a dao object
	 *
	 */
	public static function getExtendTable(Rb_Dao_Abstract $dao){
		return $dao->getTableName('object').'_extends';
	}//End of method

	//----------------------------------------------------------
	/*! \brief Return the associated Rb_Object_Permanent object
	 *
	 */
	public function getPermanent(){
		return $this->_permanent;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Return the associated Rb_Dao object
	 *
	 */
	public function getDao(){
		return $this->_dao;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Return the Extend properties dictionary
	 *   Return Array
	 */
	public function getDictionary(){
		if( $this->_dictionary ) return $this->_dictionary;
		$dictionary = Rb_Metadata_Dictionary::get()
		->getMetadatas($this->_permanent->getDao(),
		array('select'=>array(
                                                      'property_fieldname'
                                                      ),
                                           'getRs'=>true,
                                                      )
                                                      );
                                                      foreach($dictionary as $property){
                                                      	$this->_dictionary[] = $property['property_fieldname'];
                                                      }
                                                      return $this->_dictionary;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get the property
	 *
	 * \param $property_name(string)
	 */
	public function getProperty($name){
		if(isset($this->extend_props[$name]))
		return $this->extend_props[$name];
		else
		return $this->_permanent->getProperty($name);
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get all properties
	 *
	 */
	public function getProperties(){
		return array_merge($this->core_props, $this->extend_props);
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get all properties
	 *
	 */
	public function setProperty($name,$value){
		if($this->_id < 0) return false; //Can not change property on object -1
		if($this->_permanent->setProperty($name,$value)){
			$this->core_props = $this->_permanent->getProperties();
		}else{
			if(in_array($name, $this->getDictionary() ) ){
				$this->extend_props[$name] = $value;
			}
		}
		return $this;
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief
	 * This method can be used to get list of all container with extends properties.
	 * return a array or recordset if success, else return false.
	 \param $params is use for manage the display. See parameters function of getQueryOptions()
	 */
	public function getAll( $params = array() ){
		$params['with'][] = array('type'=>'LEFT OUTER',
                              'table'=>$this->_dao->getTableName(),
                              'col1'=>$this->_permanent->getDao()->getKey('primary'),
                              'col2'=>$this->_dao->getKey('primary')
		);
		$params['select'][] = $this->_dao->getTable().'.property_name';
		$params['select'][] = $this->_dao->getTable().'.property_fieldname';
		$params['select'][] = $this->_dao->getTable().'.property_description';
		return $this->_permanent->getAll($params);
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief Record properties of current instance in database.
	 * If object exist ($id > 0), call update method.
	 * If object is not already recorded, call create method and return an initialized instance.
	 *
	 */
	public function save($notify=true){
		if($this->_id < 0) return false;
		if($this->_permanent->save($notify)){
			if($this->_id == 0){
				$this->_id = $this->_permanent->getId();
				$this->_id = $this->_create();
				if($this->_id){
					return $this;
				}else return false;
			}else if($this->_id > 0){
				if( $this->_update() ){
					return $this;
				}else return false;
			}
		}else
		return false;
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief Copy properties to object $to_id
	 *   Return Rb_Object_Extend if no error
	 *   Return -1 if no datas to record
	 *   Return false if error
	 *   \param Rb_Object_Permanent
	 */
	public function copy(Rb_Object_Permanent &$to_permanent){
		if($this->_id < 0) return false;
		if($this->extend_props){
			$to_extend = new self($to_permanent);
			foreach($this->extend_props as $name=>$value){
				$to_extend->setProperty($name,$value);
			}
			return $to_extend;
		}
		return -1;
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief create a new database record for object.
	 *   Return the id if no errors
	 *   Return -1 if no datas to record
	 *   Return false if error
	 *
	 */
	protected function _create(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}
		if($this->extend_props){
			$data = $this->extend_props;
			$data[$this->_dao->getKey('primary')] = $this->_id;
			return $this->_dao->create($data);
		}
		return -1;
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief create a new database record for object.
	 *   Return the id if no errors, else return FALSE
	 *
	 */
	protected function _update(){
		if( $this->_id < 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}
		if($this->extend_props){
			return $this->_dao->replace( $this->extend_props, $this->_id);
		}else return true;
	}//End of method

} //End of class
