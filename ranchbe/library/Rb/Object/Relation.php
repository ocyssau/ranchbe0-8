<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


/*
 CREATE TABLE `objects_rel` (
 `link_id` int(11) NOT NULL,
 `parent_id` int(11) NOT NULL,
 `child_id` int(11) NOT NULL,
 `ext_id` int(11) NOT NULL,
 PRIMARY KEY  (`link_id`),
 UNIQUE KEY `UNIQ_objects_rel_1` (`parent_id`,`child_id`, `ext_id`),
 KEY `INDEX_objects_rel_1` (`parent_id`),
 KEY `INDEX_objects_rel_2` (`child_id`),
 KEY `INDEX_objects_rel_3` (`ext_id`)
 ) ENGINE=InnoDB;

 ALTER TABLE `objects_rel`
 ADD CONSTRAINT `FK_objects_rel_1` FOREIGN KEY (`parent_id`)
 REFERENCES `objects` (`object_id`) ON DELETE CASCADE,
 ADD CONSTRAINT `FK_objects_rel_2` FOREIGN KEY (`child_id`)
 REFERENCES `objects` (`object_id`) ON DELETE CASCADE;
 */

/** This is a Relation between two object where one is parent of one or many childs
 *
 **/
class Rb_Object_Relation extends Rb_Object_Root {

	protected $_id = 0;
	protected $_dao = false;
	protected $core_props = array ();
	protected $OBJECT_TABLE = 'objects_rel';
	protected $FIELDS_MAP_ID = 'link_id';
	protected static $_registry = array (); //(array) registry of instances

	public function __construct($link_id = 0) {
		$this->_dao = new Rb_Dao ( Ranchbe::getDb () );
		$this->_dao->setTable ( $this->OBJECT_TABLE, 'object' );
		$this->_dao->setKey ( $this->FIELDS_MAP_ID, 'primary_key' );
		$this->_init ( $link_id );
	} //End of method


	//-------------------------------------------------------------------------
	/**
	 *
	 * @param Integer $id
	 * @return Rb_Object_Relation
	 */
	public static function get($id = -1) {
		if ($id < 0) $id = - 1; //forced to value -1
		if ($id == 0) return new self ( 0 );
		if (! self::$_registry [$id]) {
			self::$_registry [$id] = new self ( $id );
		}
		return self::$_registry [$id];
	} //End of method


	//-------------------------------------------------------------------------
	/** Init current instance, populate properties from database,
	 *       init access to database parameters.
	 *
	 * @param Integer $id
	 * @return Boolean
	 *
	 */
	protected function _init($id) {
		if ($id > 0) {
			$this->_id = ( int ) $id;
			$this->_initProperties ();
			if (! $this->core_props) {
				trigger_error ( 'this object dont exist' , E_USER_ERROR);
				die ();
			}
		} else if ($id < 0) {
			$this->_id = - 1;
		} else {
			$this->_id = 0;
		}
		return true;
	} //End of method


	//-------------------------------------------------------------------------
	/** Get properties from database
	 * 
	 * @return Boolean
	 * 
	 */
	protected function _initProperties() {
		$this->core_props = $this->_dao->getBasicInfos ( $this->_id );
		unset ( $this->core_props [$this->FIELDS_MAP_ID] );
		return true;
	} //End of method


	//----------------------------------------------------------
	/** Set the parent object id
	 * 
	 * @param Integer $id
	 */
	public function setParent($id) {
		$this->core_props ['parent_id'] = $id;
	} //End of method


	//----------------------------------------------------------
	/** Set child id
	 * 
	 * @param Integer $id
	 */
	public function setChild($id) {
		$this->core_props ['child_id'] = $id;
	} //End of method


	//----------------------------------------------------------
	/** Set extend id
	 * 
	 * @param Integer $id
	 */
	public function setExtend($id) {
		$this->core_props ['ext_id'] = $id;
	} //End of method


	//-------------------------------------------------------------------------
	/** Return id of object
	 * 
	 * @return Integer
	 * 
	 */
	public function getId() {
		return $this->_id;
	} //End of method


	//----------------------------------------------------------
	/** Save the link in db
	 * 
	 * @return Rb_Object_Relation | false
	 */
	public function save() {
		if ($this->_id < 0) {
			return false;
		} else if ($this->_id == 0) {
			$this->_create ();
			if ($this->_id) {
				return $this;
			} else
			return false;
		} else if ($this->_id > 0) {
			if ($this->_update ()) {
				return $this;
			} else
			return false;
		}
	} //End of method


	//----------------------------------------------------------
	/**
	 * @return Integer
	 */
	public function _create() {
		//Create a basic object
		$this->_id = $this->_dao->create ( $this->core_props );
		if (! $this->_id) {
			return $this->_id = 0;
		} else {
			return $this->_id;
		}
	} //End of method


	//-------------------------------------------------------------------------
	/** Write modification in database.
	 * 
	 * @return Boolean
	 *
	 */
	protected function _update() {
		return $this->_dao->update ( $this->core_props, $this->_id );
	} //End of method


	//----------------------------------------------------------
	/** Suppress the current link
	 * 
	 * @return Boolean
	 *
	 */
	public function suppress() {
		if ($this->_id < 1) {
			return false;
		}
		return $this->_dao->suppress ( $this->_id );
	} //End of method


	//----------------------------------------------------------
	/** Suppress link
	 *
	 * @param Integer
	 * @param Integer
	 * 
	 * @return Boolean
	 * 
	 */
	public function suppressLink($parent_id, $child_id) {
		$params = array ('exact_find' => array ('parent_id' => $parent_id, 'child_id' => $child_id ) );
		return $this->_dao->suppressQuery ( $params );
	} //End of method


	//----------------------------------------------------------
	/** Copy links of object to another
	 *
	 * @param Integer
	 * @param Integer
	 * 
	 * @return Boolean
	 */
	public function copyLinks($from_object_id, $to_object_id) {
		$sql = array ();
		//get links
		$params = array ('exact_find' => array ('parent_id' => $from_object_id ), 'getRs' => true );
		$links = $this->_dao->getAllBasic ( $params );
		if (! $links)
		return true;
		foreach ( $links as $link ) {
			$sql [] = 'VALUES(' . $this->_dao->setSeqId () . ',' . $link->parent_id . ',' . $link->child_id . ')';
		}
		if (! $sql)
		return true;

		$adodb = & $this->_dao->getAdo ();
		$adodb->StartTrans ();
		$query = 'INSERT INTO ' . $this->_dao->getTableName () . '(link_id, parent_id, child_id)' . implode ( ',', $sql );
		$ok = $adodb->execute ( $query );

		if (! $ok) {
			Ranchbe::getError()->errorDb( $adodb->ErrorMsg (), $query);
			Ranchbe::getError()->push ( ERROR_DB, 'Fatal', array ('query' => $query ), $adodb->ErrorMsg () );
			$adodb->FailTrans ();
		}
		return $adodb->CompleteTrans ();
	} //End of method


} //End of class
