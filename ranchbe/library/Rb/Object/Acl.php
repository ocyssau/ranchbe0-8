<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/object/permanent.php');
//require_once('core/Acl/resource/interface.php');

/**
 * Acl object is a Permanent object with access control list capabilities.
 * Rb_Object_Acl type can be under ACL control.
 * Rb_Object_Acl are resource for Zend_Acl, so it implements Zend_Acl_Resource_Interface
 * 	trough Rb_Acl_Resource_Interface
 * If you want undestand Acl, refer to Zend_Acl documentation.
 *
 *
 */
abstract class Rb_Object_Acl extends Rb_Object_Permanent implements Rb_Acl_Resource_Interface {
	protected $history = false;
	protected $_resource = false;
	protected $_observerIsInit = false;
	protected static $_defaultResource_id = 1;

	protected $_privileges = array (
								'admin', 
								'get', 
								'create', 
								'edit', 
								'suppress' ); //(array)default privileges name


	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_init($id)
	 */
	protected function _init($id) {
		parent::_init ( $id );
		$this->_initAcl ();
		return true;
	} //End of method


	//-------------------------------------------------------------------------
	/** Add this resource to Acl and create associated rules
	 *
	 * @return Rb_Object_Acl	return self instance
	 */
	protected function _initAcl() {
		$parentResource = false;
		if ($this->_id > 0){
			if ( $this->getFather () ){
				$parentResource = $this->getFather ()->getResource();
			}
		}
		if( ! $parentResource ){
			$parentResource = $this->getDefaultResource ();
		}
		$toInit = array();
		if ( $parentResource->getResourceId () > 1 ) {
			$toInit[] = $parentResource->getResourceId ();
		}
		if (! Ranchbe::getAcl ()->has ( $this->getResourceId() )) {
			Ranchbe::getAcl ()->add ( $this->getResource(), $parentResource );
			$toInit[] = $this->getResourceId ();
		}
		if($toInit){
			Ranchbe::getAcl ()->initPrivileges ( $toInit );
		}
		return $this;
	} //End of method
	
	
	//-------------------------------------------------------------------------
	/** Get the default resource for current object
	 * 
	 * @return Rb_Acl_Resource_Default
	 */
	public function getDefaultResource() {
		return Ranchbe::getAcl ()->getRootResource ();
	} //End of method
	
	
	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see external/Zend/Acl/Resource/Zend_Acl_Resource_Interface#getResourceId()
	 */
	public function getResourceId() {
		return self::getStaticResourceId ( $this->getId () );
	} //End of method
	
	
	//-------------------------------------------------------------------------
	/**
	 * Return resource object of this element
	 *
	 * @return Rb_Acl_Resource_Default
	 */
	public function getResource() {
		if(!$this->_resource){
			$this->_resource = new Rb_Acl_Resource_Default( $this->getResourceId() );
		}
		return $this->_resource;
	} //End of method
	
	
	//----------------------------------------------------------------------------
	/**
	 * Returns the string identifier from specified $id
	 *
	 * @param string $id
	 * @return string
	 */
	public static function getStaticResourceId($id) {
		if ($id < 0) return static::$_defaultResource_id;
		return $id;
	} //End of method
	
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Acl/Resource/Rb_Acl_Resource_Interface#getPrivileges()
	 */
	public function getPrivileges() {
		return $this->_privileges;
	}
	
	
	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#save($notify)
	 */
	public function save($notify = true, $history = true) {
		Ranchbe::getError()->info('Save object ' . $this->getNumber());
		
		if ($notify) $this->_initObservers ();
		if ($this->_id < 0) return false;

		//If id == 0, object is not yet created
		if ($this->_id == 0) {
			if ($notify) {
				$this->notify_all ( $this->_preCreateEventName, $this );
				if ($this->stopIt){
					Ranchbe::getLogger()->log("Stop on $this->_preCreateEventName", 3);
					return false; //use by observers to control execution
				}
			}
			Ranchbe::getDb ()->StartTrans ();
			Rb_Object_Permanent::_createPermanentObject ();
			if ($this->_id) {
				Ranchbe::getDb ()->CompleteTrans ();
				if ($notify) {
					$this->notify_all ( $this->_postCreateEventName, $this );
					if ($this->stopIt){
						Ranchbe::getLogger()->log("Stop on $this->_postCreateEventName", 3);
						return false; //use by observers to control execution
					}
				}
				if ($history) {
					$this->writeHistory ( '_create' );
				}
				return $this;
			} else{
				Ranchbe::getDb ()->FailTrans ();
				Ranchbe::getDb ()->CompleteTrans ();
				Ranchbe::getLogger()->log('_create failed', 3);
				return false;
			}
			//if id > 0, object existing in database
		} else if ($this->_id > 0) {
			if ($notify) {
				$this->notify_all ( $this->_preUpdateEventName, $this );
				if ($this->stopIt){
					Ranchbe::getLogger()->log("Stop on $this->_preUpdateEventName", 3);
					return false; //use by observers to control execution
				}
			}
			if ($this->_update ()) {
				if ($notify) {
					$this->notify_all ( $this->_postUpdateEventName, $this );
					if ($this->stopIt){
						Ranchbe::getLogger()->log("Stop on $this->_postUpdateEventName", 3);
						return false; //use by observers to control execution
					}
				}
				if ($history) {
					$this->writeHistory ( '_update' );
				}
				return $this;
			} else {
				Ranchbe::getLogger()->log('_update failed', 3);
				return false;
			}
		}
	} //End of method


	//---------------------------------------------------------------------------
	/**
	 * Save current object in history
	 *
	 * @param string $action	name of action
	 * @return boolean
	 */
	public function writeHistory($action) {
		if ($this->_id <= 0) {
			$this->error_stack->push ( Rb_Error::ERROR, array (), '$this->_id is not set' );
			return false;
		}
		if (! $this->getHistory ()) return false;
		//Write history
		$history = $this->getProperties ();
		$history [$this->getDao ()->getKey ( 'primary' )] = $this->_id;
		$history ['action_name'] = $action;
		return $this->getHistory ()->write ( $history );
	} //End of method


	//---------------------------------------------------------------------------
	/**
	 * Return instance of history
	 *
	 * @return Rb_History
	 */
	public function getHistory() {
		if ($this->history) return $this->history;
		if ($this->_id <= 0) {
			$this->error_stack->push ( Rb_Error::ERROR, array (), '$this->_id is not set' );
			return false;
		}
		return $this->history = new Rb_History ( $this );
	} //End of method


} //End of class
