<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


/*
 CREATE TABLE `reposits` (
 `reposit_id` int(11) NOT NULL,
 `reposit_number` varchar(16) NOT NULL,
 `reposit_name` varchar(16) NOT NULL,
 `reposit_description` varchar(16) NOT NULL,
 `reposit_type` int(1) default 1,
 `reposit_mode` int(1) default 1,
 `reposit_url` text,
 `open_by` int(11) default NULL,
 `open_date` int(11) default NULL,
 `is_active` int(1) default 0,
 `priority` int(1) default 0,
 `space_id` int(11) default 0,
 PRIMARY KEY  (`reposit_id`),
 UNIQUE KEY `UNIQ_reposits_1` (`reposit_url`(512)),
 KEY `INDEX_reposits_1` (`reposit_number`),
 KEY `INDEX_reposits_2` (`is_active`),
 KEY `INDEX_reposits_3` (`priority`),
 KEY `INDEX_reposits_4` (`space_id`)
 ) ENGINE=InnoDB;

 --
 -- Contraintes pour la table `reposits`
 --
 ALTER TABLE `reposits`
 ADD CONSTRAINT `FK_reposits_1` FOREIGN KEY (`open_by`)
 REFERENCES `rb_users` (`auth_user_id`) ON UPDATE CASCADE;

 /*! \brief A Reposit is a url to a directory or other emplacement.  Each Reposit
 *   is recorded in database.
 *
 */
class Rb_Reposit extends Rb_Object_Permanent {

	protected $OBJECT_TABLE = 'reposits'; //(String)

	const TYPE_REPOSIT = 1;
	const TYPE_READ = 2;

	const MODE_SYMLINK = 1;
	const MODE_COPY = 2;


	//Fields:
	protected $FIELDS_MAP_ID = 'reposit_id'; //(String)Name of the field of the primary key
	protected $FIELDS_MAP_NUM = 'reposit_number'; //(String)Name of the field to define the number
	protected $FIELDS_MAP_NAME = 'reposit_name'; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC = 'reposit_description'; //(String)Name of the field to define the description
	protected $FIELDS_MAP_STATE = 'is_active'; //(String)Name of the field where is store the state
	protected $FIELDS_MAP_FATHER; //(String)Name of the field where is store the id of the father


	protected $core_props = array ();

	protected static $_registry = array (); //(array) registry of constructed objects
	protected static $_active_reposit = array (); //(array of Rb_Reposit) instance of active Reposit for each space

	/** id of default parent resource
	 *
	 * @var Integer
	 */
	protected static $_defaultResource_id = 7;

	//-------------------------------------------------------------------------
	public function __construct($id = 0) {
		$this->setProperty ( 'state', 1 );
		$this->setProperty ( 'open_date', time () );
		$this->setProperty ( 'open_by', Rb_User::getCurrentUser ()->getId () );
		$this->setProperty ( 'url', '' );
		$this->setProperty ( 'type', 1 );
		$this->setProperty ( 'mode', 1 );
		$this->setProperty ( 'priority', 2 );
		$this->setProperty ( 'space_id', 0 );
		$this->_init ( $id );
	} //End of method


	//-------------------------------------------------------------------------
	/*! \brief init the property of the Reposit
	 *
	 * \param $reposit_id(integer) Id of the Reposit
	 */
	protected function _init($reposit_id) {
		$this->_dao = new Rb_Dao ( Ranchbe::getDb () );
		$this->_dao->setTable ( $this->OBJECT_TABLE );
		$this->_dao->setKey ( $this->FIELDS_MAP_ID, 'primary_key' );
		$this->_dao->setKey ( $this->FIELDS_MAP_NUM, 'number' );
		$this->_dao->setKey ( $this->FIELDS_MAP_NAME, 'name' );
		$this->_dao->setKey ( $this->FIELDS_MAP_DESC, 'description' );
		$this->_dao->setKey ( $this->FIELDS_MAP_STATE, 'state' );
		$this->setProperty ( $this->FIELDS_MAP_NUM, '' );
		$this->setProperty ( $this->FIELDS_MAP_NAME, '' );
		$this->setProperty ( $this->FIELDS_MAP_DESC, '' );
		$ret = parent::_init ( $reposit_id );
		if ($this->getProperty ( 'space_id' ))
		$this->space = & Rb_Space::get ( $this->getProperty ( 'space_id' ) );
		return $ret;
	} //End of method


	//-------------------------------------------------------------------------
	/*! \brief init and clean current registry
	 */
	public static function initRegistry() {
		self::$_registry = array ();
	}

	//--------------------------------------------------------------------
	/*!\brief create a new database record for object.
	 *   Return the id if no errors, else return FALSE
	 *
	 */
	protected function _create() {
		if ($this->_id !== 0) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('id' => $this->_id ), tra ( 'can not performed this action on object id %id%' ) );
			trigger_error ( 'can not performed this action on object id ' . $this->_id , E_USER_WARNING);
			return false;
		}

		if (! $this->getNumber ()) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array (), 'number is not set' );
			return false;
		}

		if (! $this->getProperty ( 'url' )) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array (), 'url is not set' );
			return false;
		}

		//Create a new Rb_Directory to store files
		if (! $this->_initDir ( $this->getProperty ( 'url' ) )) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('number' => $this->getNumber () ), tra ( 'creation of Reposit %number% failed' ) );
			return false;
		}

		//Create a basic object
		if ($this->_id = $this->_dao->create ( $this->getProperties () )) {
			$this->isSaved = true;
			self::$_registry [$this->_id] = & $this;
		} else {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('element' => $this->number () ), tra ( 'cant create the Reposit %element%' ) );
			return $this->_id = 0;
		}

		return $this->_id;
	} //End of method


	//--------------------------------------------------------
	/*!\brief
	 * init the directory where store files
	 *
	 */
	protected static function _initDir($repositDir) {
		if (empty ( $repositDir )) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array (), tra ( 'Reposit is not set' ) );
			return false;
		} else {
			Ranchbe::getError()->push ( Rb_Error::INFO, array ('Reposit' => $repositDir ), tra('Try to create Reposit directory %Reposit%') );
		}

		if (! is_dir ( $repositDir )) { //Check if Reposit dir is existing
			if (! Rb_Directory::createdir ( $repositDir, 0755 )) { //..and create it if not
				Ranchbe::getError()->push ( Rb_Error::ERROR, array ('dir' => $repositDir), tra('creation of directory %dir% failed') );
				return false;
			}
		}
		return true;
	} //End of method


	//-------------------------------------------------------------------------
	/*!\brief write modification in database.
	 * Return true or false.
	 *
	 */
	protected function _update() {
		if ($this->_id < 1) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('id' => $this->_id ), tra ( 'can not performed this action on object id %id%' ) );
			trigger_error ( 'can not performed this action on object id ' . $this->_id , E_USER_WARNING);
			return false;
		}
		return $this->_dao->update ( $this->getProperties (), $this->_id );
	} //End of method


	//-------------------------------------------------------------------------
	/*! \brief accesor to object
	 * If $id is < 0 then forced id to -1
	 * If $id = 0 to create a new object not yet recorded in database
	 *
	 * \param $reposit_id(integer) id of Reposit
	 */
	public static function get($id = -1) {
		if ($id < 0)
		$id = - 1; //forced to value -1
		if ($id == 0)
		return new Rb_Reposit ( 0 );
		if (! self::$_registry [$id]) {
			self::$_registry [$id] = new Rb_Reposit ( $id );
		}
		return self::$_registry [$id];
	} //End of method


	//--------------------------------------------------------------------
	/*!\brief
	 * This method can be used to get list of all Reposit.
	 * return a array or recordset if success, else return false.
	 \param $params is use for manage the display. See parameters function of getQueryOptions()
	 */
	public function getAll($params = array()) {
		return $this->_dao->getAllBasic ( $params );
	} //End of method


	//-------------------------------------------------------------------------
	/** Return current active Reposit
	 *
	 * @param Rb_Space
	 * @return Rb_Reposit
	 */
	public static function getActiveReposit(Rb_Space &$space) {
		if (! self::$_active_reposit [$space->getId ()]) {
			$p = array ('exact_find' => array ('is_active' => 1, 'space_id' => $space->getId () ),
						'select' => array ('reposit_id' ),
						'extra' => 'GROUP BY priority HAVING(MAX(priority))' );
			$id = Rb_Reposit::get ()->getDao ()->getOne ( $p );
			if (! $id) {
				Ranchbe::getError()->push ( Rb_Error::ERROR, array (),
				tra ('unable to get active Reposit. Create or activate a Reposit.') );
			} else
			self::$_active_reposit [$space->getId ()] = & self::get ( $id );
		}
		return self::$_active_reposit [$space->getId ()];
	} //End of method


	//--------------------------------------------------------
	/** Get part of path after the $this->url path to define subdir for each iteration
	 *  If suffix is a integer, create a iteration directory
	 *  else, if suffix is string, add path $suffix to reposit path
	 *
	 * @param unknown_type $suffix
	 */
	public static function getTrailingPath($suffix) {
		if (! $suffix){
			trigger_error('none suffix is set', E_USER_ERROR);
			die;
		}
		$suffix = str_ireplace  ( array('/', '\\')  , ''  , $suffix );
		//Test if iteration is integer
		if( ctype_digit($suffix) ){
			$path = '__iterations/' . $suffix;
		}else{
			$path = $suffix;
		}
		return $path;
	} //End of method

	//--------------------------------------------------------
	/** Get full path to reposit directory for suffix
	 *
	 * @param Rb_Reposit $reposit
	 * @param unknown_type $suffix
	 */
	public static function getBasePath(Rb_Reposit &$reposit, $suffix) {
		if (! $reposit || $reposit->getId() < 1 ) {
			trigger_error('$reposit is not set', E_USER_ERROR);
			return false;
		}
		$path = $reposit->getProperty ( 'url' ) .'/'. self::getTrailingPath($suffix);
		if (! is_dir ( $path )) { //Check if version Reposit dir is existing
			if (! self::_initDir ( $path ))
			return false;
		}
		return $path;
	} //End of method



	//----------------------------------------------------------
	/*! \brief Get the property of the Reposit by the property name. init() must be call before
	 *
	 * \param $property_name(string) = reposit_id,reposit_number,reposit_description,reposit_state,reposit_version.
	 */
	public function getProperty($property_name) {
		switch ($property_name) {
			case 'id' :
			case 'reposit_id' :
				return $this->_id;
				break;
					
			case 'number' :
				$property_name = $this->FIELDS_MAP_NUM;
				break;
					
			case 'name' :
				$property_name = $this->FIELDS_MAP_NUM;
				break;
					
			case 'description' :
				$property_name = $this->FIELDS_MAP_DESC;
				break;
					
				//1=is active, 0=unactive
			case 'state' :
				$property_name = $this->FIELDS_MAP_STATE;
				break;
					
			case 'url' :
				$property_name = 'reposit_url';
				break;
					
				//1=Reposit, 2=read
			case 'type' :
				$property_name = 'reposit_type';
				break;
					
				//1=read by symlink, 2=read by copy
			case 'mode' :
				$property_name = 'reposit_mode';
				break;
					
			case 'open_date' :
			case 'open_by' :
			case 'priority' :
			case 'space_id' :
				break;
		} //End of switch
		return $this->core_props [$property_name];
	} //End of method

	
	/**
	 * 
	 * @param boolean $bool
	 */
	public function setActive($bool) {
		if($bool) $bool = true;
		else $bool = false;
		$this->setProperty('state', $bool);
	}
	

	//-------------------------------------------------------------------------
	/*! \brief set the property $property_name with value of $property_value
	 * can not call this method from instance where id < 0.
	 * Return $this, so you can use sythax type $object->setProperty($property_name, $property_value)->getProperty($property_name);
	 *
	 */
	public function setProperty($property_name, $property_value) {
		if ($this->getId () < 0)
		return false; //Can not change property on object -1
		$this->isSaved = false;
		switch ($property_name) {
			case 'id' :
			case $this->FIELDS_MAP_ID :
				break; //this properties are not core properties
				

			case 'number' :
			case $this->FIELDS_MAP_NUM :
				$this->core_props [$this->FIELDS_MAP_NUM] = $property_value;
				break;
					
			case 'name' :
			case $this->FIELDS_MAP_NAME :
				$this->core_props [$this->FIELDS_MAP_NAME] = $property_value;
				break;
					
			case 'description' :
			case $this->FIELDS_MAP_DESC :
				$this->core_props [$this->FIELDS_MAP_DESC] = $property_value;
				break;
					
				//1=is active, 0=unactive
			case 'state' :
			case $this->FIELDS_MAP_STATE :
				$this->core_props [$this->FIELDS_MAP_STATE] = ( int ) $property_value;
				break;
					
			case 'url' :
			case 'reposit_url' :
				$this->core_props ['reposit_url'] = $property_value;
				break;
					
				//1=Reposit, 2=read
			case 'type' :
			case 'reposit_type' :
				if ($property_value == 'Reposit')
				$property_value = 1;
				if ($property_value == 'read')
				$property_value = 2;
				$this->core_props ['reposit_type'] = ( int ) $property_value;
				break;
					
				//1=read by symlink, 2=read by copy, 3=read by hardlink
			case 'mode' :
			case 'reposit_mode' :
				if ($property_value == 'symlink')
				$property_value = 1;
				if ($property_value == 'copy')
				$property_value = 2;
				$this->core_props ['reposit_mode'] = ( int ) $property_value;
				break;
					
			case 'open_date' :
			case 'open_by' :
			case 'priority' :
			case 'space_id' :
				$this->core_props [$property_name] = ( int ) $property_value;
				break;
					
			default :
				return false;
				break;
		} //End of switch
		return $this;
	} //End of method


	//-------------------------------------------------------------------------
	/*! \brief suppress current object database records, files, and all depandancies
	 * and clean the registry of current class to prevent recall of this instance.
	 * can not call this method from instance where id <= 0.
	 * Return false or true.
	 *
	 */
	public function suppress() {
		if ($this->_id < 1) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('id' => $this->_id ), tra ( 'can not performed this action on object id %id%' ) );
			trigger_error ( 'can not performed this action on object id ' . $this->_id , E_USER_WARNING);
			return false;
		}
		if ($ret = $this->_dao->suppress ( $this->_id )) {
			$this->_suppressPermanentObject ();
			//clean registry
			unset ( self::$_registry [$this->_id] );
		}
		return $ret;
	} //End of method
}//End of class
