<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

// brief! create params to search on Filesystem

class Rb_Search_Filesystem implements Rb_Search_Interface{

protected $limit       = 50; /*!< limit the number of records in the result*/
protected $offset      = 0; /*!< defined the pagination page to display*/
protected $recordCount; //(Integer)

/*
field use for sort sql option
*/
protected $sortBy; //(string)

/*
('ASC' or 'DESC') sort direction sql option
*/
protected $sortOrder='ASC'; //(string) ASC or DESC

/*
its a assoc array where key=a field and value=a word to find in the table.
This array is translate to a query option like 'where key=value'
*/
protected $exact_find; //(array)

/*
its a assoc array where key=a field and value=a string to find in the table.
This array is translate to a query option like 'WHERE key LIKE %value%'
to find multi value on the same field just separate the word by close OR or AND like this :
nom1 OR nom2 OR nom3 with space on each side of OR
*/
protected $find; //(array) 

/*
any query to add to end
*/
protected $extra;//(string)

/*
params send to basic object
*/
protected $params=array(); //(array)

public function __construct(){
}

//---------------------------------------------------------------------------
/*! \brief
\param $what (string) a word to find
\param $where (string) field where search
\param $table (string) search in table, default = current object table
\param $at (string) begin, end, anywhere, exact
*/
function setFind( $what, $where, $at='anywhere' ){
  $this->find[$where] = $this->setPassThrough($what, $at);
}//End of method

//---------------------------------------------------------------------------
/*! \brief add regexp '^' or '$' symbols
*/
private static function setPassThrough( $what, $at='anywhere' ){
  switch($at){
    case 'anywhere':
      return $what;
      break;
    case 'begin':
      return '^'.$what;
      break;
    case 'end':
      return $what.'$';
      break;
    case 'exact':
      return '^'.$what.'$';
      break;
  }
}//End of method

//---------------------------------------------------------------------------
/*! \brief
*/
function setLimit( $int ){
  $this->limit = $int;
}//End of method

//---------------------------------------------------------------------------
/*! \brief
*/
function setOffset( $int ){
  $this->offset = $int;
}//End of method

//---------------------------------------------------------------------------
/*! \brief
*/
function setOrder( $by , $dir='ASC' ){
  $this->sortBy = $by;
  $this->sortOrder = $dir;
}//End of method

//---------------------------------------------------------------------------
/*! \brief
*/
function setSelect( $mixed ){
}//End of method

//---------------------------------------------------------------------------
/*! \brief
*/
function set( $params, $value ){
  $this->params[$params] = $value;
}//End of method

//---------------------------------------------------------------------------
/*! \brief
*/
function getParams(){
  $params = $this->params;
  $params['numrows']=$this->limit;
  $params['offset']=$this->offset;
  $params['sort_field']=$this->sortBy;
  $params['sort_order']=$this->sortOrder;
  $params['find']=$this->find;
  return $params;
}//End of method

}//End of class
