<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

interface Rb_Search_Abstract{

	/**
	 *
	 * @var Rb_Space
	 */
	protected $space;

	/**
	 * limit the number of records in the result
	 *
	 * @var integer
	 */
	protected $limit       = 50;

	/**
	 * defined the pagination page to display
	 *
	 * @var integer
	 */
	protected $offset      = 0;

	/**
	 * field use for sort sql option
	 *
	 * @var string
	 */
	protected $sortBy;

	/**
	 * ('ASC' or 'DESC') sort direction sql option
	 *
	 * @var string  ASC or DESC
	 */
	protected $sortOrder='ASC';

	/**
	 *	its a assoc array where key=a field and value=a word to find in the table.
	 *	This array is translate to a query option like 'where key=value'
	 *
	 * @var array
	 */
	protected $exact_find;

	/**
	 *	its a assoc array where key=a field and value=a string to find in the table.
	 *	This array is translate to a query option like 'WHERE key LIKE %value%'
	 *	to find multi value on the same field just separate the word by close OR or AND like this :
	 *	nom1 OR nom2 OR nom3 with space on each side of OR
	 *
	 * @var array
	 */
	protected $find;

	/**
	 *	array(field1,field2,field3,...) its a nonassoc array with all fields to put in the sql select option
	 *
	 * @var array|mixed
	 */
	protected $select = '*';

	/**
	 *	Input special where close. Exemple : $params[where] = array('field1 > 1' , 'field2 = 2')
	 *
	 * @var array
	 */
	protected $where = array();

	/**
	 * any query to add to end
	 *
	 * @var string
	 */
	protected $extra;

	//---------------------------------------------------------------------------
	/**
	 *
	 *	@param string	$what 	a word to find
	 *	@param string	$where	field where search
	 *	@param string	$table	search in table, default = current object table
	 *	@param string	$at		begin, end, anywhere, exact
	 */
	abstract public function setFind( $what, $where, $at='anywhere' );

	//---------------------------------------------------------------------------
	/**
	 *
	 * @param integer $int
	 */
	abstract public function setLimit( $int );
	//---------------------------------------------------------------------------
	/**
	 *
	 * @param integer $int
	 */
	abstract public function setOffset( $int );

	//---------------------------------------------------------------------------
	/**
	 *
	 * @param string $by
	 * @param string $dir
	 */
	abstract public function setOrder( $by , $dir='ASC' );

	//---------------------------------------------------------------------------
	/**
	 *
	 * @param $mixed
	 */
	abstract public function setSelect( $mixed );

	//---------------------------------------------------------------------------
	/**
	 * @return array
	 */
	abstract public function getParams();

}//End of class
