<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/**
 * Create params to search in database
 *
 * @author Administrateur
 *
 */
class Rb_Search_Db implements Rb_Search_Interface{

	/** To define junction
	 *
	 * @var array
	 */
	protected $with = array();

	/**
	 *
	 * @var unknown_type
	 */
	protected $_limit       = 50; /*!< limit the number of records in the result*/

	/**
	 *
	 * @var unknown_type
	 */
	protected $_offset      = 0; /*!< defined the pagination page to display*/

	/**
	 *
	 * @var integer
	 */
	protected $_recordCount; //(Integer)

	/** field use for sort sql option
	 *
	 * @var string
	 */
	protected $_sortBy; //(string)

	/** ('ASC' or 'DESC') sort direction sql option
	 *
	 * @var string
	 */
	protected $_sortOrder='ASC'; //(string) ASC or DESC

	/**
	 * its a assoc array where key=a field and value=a word to find in the table.
	 * This array is translate to a query option like 'where key=value'.
	 *
	 * @var array
	 */
	protected $_exact_find; //(array)

	/**
	 * its a assoc array where key=a field and value=a string to find in the table.
	 * This array is translate to a query option like 'WHERE key LIKE %value%'
	 * to find multi value on the same field just separate the word by close OR or AND like this :
	 * nom1 OR nom2 OR nom3 with space on each side of OR
	 *
	 * @var array
	 */
	protected $_find; //(array)

	/** array(field1,field2,field3,...) its a nonassoc array with all fields to put in the sql select option
	 *
	 * @var array
	 */
	protected $_select = array(); //(array or *)

	/** Input special where close. Exemple : $params[where] = array('field1 > 1' , 'field2 = 2')
	 *
	 * @var array
	 */
	protected $_where = array(); //(array)

	/**
	 * @var Rb_Dao_Abstract
	 */
	protected $_dao = false; //(Rb_Dao_Abstract)

	/** Name of main table to search
	 *
	 * @var string
	 */
	protected $_table; //(string)

	/** define the select close for query the database.
	 * @var string
	 */
	protected $_selectClose = '';

	/** define the where close for query the database.
	 *
	 * @var string
	 */
	protected $_whereClose  = '';

	/** define the order for sort the result.
	 *
	 * @var string
	 */
	protected $_orderClose  = '';

	/** any query to add to end.
	 *
	 * @var string
	 */
	protected $_extra;//(string)


	/**
	 *
	 * @param Rb_Dao_Abstract $dao
	 */
	public function __construct(Rb_Dao_Abstract &$dao){
		$this->_dao =& $dao;
		$this->_table = $dao->getTableName();
	}

	//---------------------------------------------------------------------------
	/**
	 * @param string	$what 	a phrase to find. Eeach word may be prefixed by OR or AND to create conditions.
	 * @param string	$where	field where search, name of the column.
	 * @param string	$at		begin, end, anywhere, exact.
	 * @param string	$table 	search in table, default = current object table
	 * @return void
	 */
	public function setFind( $what, $where, $at='anywhere', $table='' ){
		if( empty($table) ) $table = $this->_table;

		if($at=='exact') $op = ' = ';
		else $op = ' LIKE ';

		$whats = array();
		$i=0;

		$whats = explode(' ', $what ); //Explode
		//for each word create setFind. If word = OR ignore it and and set a OR close for the next
		for( $i=0; $i < count($whats); $i++ ){
			$whats[$i] = trim($whats[$i]);
			//if word is not = OR
			if( $whats[$i] == 'AND' ) $i++;
			if( empty($whats[$i]) ) $i++;
			if( $whats[$i] != 'OR' ){
				$this->_find['AND'][] = $table.'.'.$where.$op.self::setPassThrough( $whats[$i], $at );
			}else if( $whats[$i] == 'OR' ){
				$i++;
				$this->_find['OR'][] = $table.'.'.$where.$op.self::setPassThrough( $whats[$i], $at );
			}
		}
	}//End of method
	
	//---------------------------------------------------------------------------
	/**
	 *
	 * @param string	$what
	 * @param string	$at
	 * @return string
	 */
	private static function setPassThrough( $what, $at='anywhere' ){
		switch($at){
			case 'anywhere':
				return '\'%'.$what.'%\'';
				break;
			case 'begin':
				return '\''.$what.'%\'';
				break;
			case 'end':
				return '\'%'.$what.'\'';
				break;
			case 'exact':
				return '\''.$what.'\'';
				break;
		}
	}//End of method

	//---------------------------------------------------------------------------
	/**
	 *
	 * @param integer $int
	 * @return void
	 */
	public function setLimit( $int ){
		$this->_limit = $int;
	}//End of method

	//---------------------------------------------------------------------------
	/**
	 *
	 * @param integer $int
	 * @return void
	 */
	public function setOffset( $int ){
		$this->_offset = $int;
	}//End of method

	//---------------------------------------------------------------------------
	/**
	 *
	 * @param string $by
	 * @param string $dir
	 * @return void
	 */
	public function setOrder( $by , $dir='ASC' ){
		$this->_sortBy = $by;
		$this->_sortOrder = $dir;
	}//End of method

	//---------------------------------------------------------------------------
	/**
	 * Add a select
	 *
	 * @param array | string 	$mixed
	 * @return void
	 */
	public function setSelect( $mixed ){
		if(is_array($mixed) && is_array($this->_select))
		$this->_select = array_merge( $this->_select , $mixed);
		else if(is_array($mixed))
		$this->_select = $mixed;
		else
		$this->_select[] = $mixed;
	}//End of method

	//---------------------------------------------------------------------------
	/**
	 * Add a where instruction to SQL query
	 *
	 * @param string $str
	 * @return void
	 */
	public function setWhere( $str ){
		$this->_where[] = $str;
	}//End of method

	//---------------------------------------------------------------------------
	/** Create a join close on query
	 * @params $table (string) table to join
	 * @params $col (variant) String field use for ON condition,
	 Array col1=>'name of field for first table',
	 col2=>'name of field for 2nd table'
	 * @params $type (string) INNER or OUTER
	 * @return void
	 */
	public function setWith( $table, $col, $type='INNER' ){
		if(is_string($col)){
			$col = array('col1'=>$col, 'col2'=>$col);
		}
		$this->with[] = array('type'=>$type,
                        'table'=>$table,
                        'col1'=>$col['col1'],
                        'col2'=>$col['col2']
		);
	}//End of method

	//-------------------------------------------------------------------------
	/** This method decompose the var $params and return the correct synthase for the "where" and "select" for SQL query.
	 *
	 *  Exemple of query :
	 *  $query = "SELECT $this->_selectClose FROM `TABLE`
	 *     $this->_whereClose
	 *     $this->_orderClose
	 *     ";
	 *  Ranchbe::getDb()->SelectLimit( $query , $this->_limit , $this->_offset);
	 *  This method is associated to script "filterManager.php" wich get values from forms
	 *
	 *  return :
	 *  $this->_selectClose
	 *  $this->_whereClose
	 *  $this->_orderClose
	 *  $this->_limit
	 *  $this->_offset
	 *  $this->_from
	 */
	/*
	 protected function _getQueryOptions(){

	 if( isset($this->with[0]) ){
	 foreach( $this->with as $with ){
	 $array_joinClose[] = $with['type'].' JOIN '. $with['table'] .' ON '. $with['table'].'.'.$with['col'] .'='.$this->_table.'.'.$with['col'];
	 }
	 }
	 if( is_array($array_joinClose) )
	 $joinClose = implode(' ', $array_joinClose );

	 if( is_array ($this->_where) ){
	 foreach($this->_where as $where)
	 $whereClose[] = "($where)";
	 }

	 if(!empty($this->_sortBy)){
	 $orderClose = " ORDER BY " . $this->_sortBy . " " . $this->_sortOrder;
	 }

	 if(!empty( $whereClose )){
	 $whereClose = 'WHERE ' . implode(' AND ', $whereClose);
	 }

	 if ( is_array($this->_select) ){
	 if(count($this->_select) > 0)
	 $selectClose = '*';
	 else
	 $selectClose = implode(' , ' , $this->_select);
	 }else $selectClose = $this->_select;

	 if (isset($selectClose))  $this->_selectClose = $selectClose;
	 else $this->_selectClose = NULL;
	 if (isset($whereClose))   $this->_whereClose  = $whereClose;
	 else $this->_whereClose = NULL;
	 if (isset($orderClose))   $this->_orderClose  = $orderClose;
	 else $this->_orderClose = NULL;
	 $this->_limit = intval($this->_limit);
	 $this->_offset = intval($this->_offset);
	 if (isset($this->_table)) $this->_from  = $this->_table;
	 if (isset($joinClose))    $this->_joinClose   = $joinClose;
	 if (isset($params['extra'])) $this->_extra    = $params['extra'];

	 }//End of method
	 */

	//---------------------------------------------------------------------------
	/** Return the options in a array
	 */
	/*
	 function getQueryOptionsArray( $params=array() ){
	 $this->_getQueryOptions($params);
	 return array(
	 'selectClose'=>$this->_selectClose,
	 'whereClose'=>$this->_whereClose,
	 'orderClose'=>$this->_orderClose,
	 'limit'=>$this->_limit,
	 'offset'=>$this->_offset,
	 'from'=>$this->_from,
	 'joinClose'=>$this->_joinClose,
	 'extra'=>$this->extra,
	 );
	 }//End of method
	 */

	//---------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Search/Rb_Search_Interface#getParams()
	 */
	public function getParams(){
		$params['numrows']		= $this->_limit;
		$params['offset']		= $this->_offset;
		$params['sort_field']	= $this->_sortBy;
		$params['sort_order']	= $this->_sortOrder;
		$params['find']			= $this->_find;
		$params['select']		= $this->_select;
		$params['where']		= $this->_where;
		$params['with']			= $this->with;
		return $params;
	}//End of method
	
	//---------------------------------------------------------------------------
	/**
	 *
	 * @return Rb_Dao_Abstract
	 */
	public function getDao(){
		return $this->_dao;
	}//End of method

}//End of class
