<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

interface Rb_Search_Interface{

//---------------------------------------------------------------------------
/*! \brief
\param $what (string) a word to find
\param $where (string) field where search
\param $table (string) search in table, default = current object table
\param $at (string) begin, end, anywhere, exact
*/
public function setFind( $what, $where, $at='anywhere' );

//---------------------------------------------------------------------------
/*! \brief
*/
public function setLimit( $int );
//---------------------------------------------------------------------------
/*! \brief
*/
public function setOffset( $int );

//---------------------------------------------------------------------------
/*! \brief
*/
public function setOrder( $by , $dir='ASC' );

//---------------------------------------------------------------------------
/*! \brief
*/
public function setSelect( $mixed );

//---------------------------------------------------------------------------
/*! \brief
*/
public function getParams();

}//End of class
