<?php

/** Extends Zend_Mail to manip mails in RanchBE
 * 
 * @author Olivier CYSSAU
 *
 */
class Rb_Mail extends Zend_Mail{
	
	protected static $_initmail = false;
	
    /**
     * Public constructor
     *
     */
    public function __construct(){
    	if(!self::$_initmail){
			$config = Ranchbe::getConfig()->mail;
			$params = array();
			if(!$config->enable) die('mails are disabled. See mail.enable in your config file');
			switch($config->transport){
				case 'smtp':
					if($config->smtp->auth){
						$params['auth'] = $config->smtp->auth;
						$params['username'] = $config->smtp->username;
						$params['password'] = $config->smtp->password;
					}
					if($config->smtp->ssl){
						$params['ssl'] = $config->smtp->ssl;
						$params['port'] = $config->smtp->port;
					}
					$tr = new Zend_Mail_Transport_Smtp($config->smtp->server, $params);
					Zend_Mail::setDefaultTransport($tr);
					break;
				case 'sendmail':
					break;
			}
			self::$_initmail = true;
		}
        $this->_charset = $config->encoding;
    }
	
} //End of class
