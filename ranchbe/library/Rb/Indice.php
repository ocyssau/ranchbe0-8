<?php

//require_once('core/dao/abstract.php');

/*
CREATE TABLE `indices` (
  `indice_id` int(11) NOT NULL default '0',
  `indice_value` varchar(8) NOT NULL default '',
  PRIMARY KEY  (`indice_id`),
  UNIQUE KEY `UC_indice_value` (`indice_value`)
) ENGINE=InnoDB;
*/

//Class to access to Indice definition database to replace version by a textual name
class Rb_Indice extends Rb_Dao_Abstract{

protected $indice_id; //(Integer)
protected $indice_value; //(String)
protected $indice; //(Array)
protected $dbranchbe; //(Object)
private static $_instance; //(Object) singleton instance

//Private for prevent direct construction
private function __construct(){
  $this->dbranchbe =& Ranchbe::getDb();
  $this->OBJECT_TABLE  = 'indices';
  $this->FIELDS_MAP_ID  = 'indice_id';
  $this->FIELDS_MAP_NUM = 'indice_value';
}//End of method

// Singleton method
public static function singleton(){
  if (!isset(self::$_instance)) {
      $c = __CLASS__;
      self::$_instance = new $c;
  }
  return self::$_instance;
}

// Pr�vient les utilisateurs sur le cl�nage de l'instance
public function __clone(){
  trigger_error('clone is forbidden', E_USER_ERROR);
}

//-------------------------------------------------------------------
  /*! \brief Get number of Indice list link to space
   *  return an array indice_id=>indice_number if success, false else. 
   *   
   *  \param  $currentIndice_id(integer) If is set return only the Indice greater than $currentIndice_id.
   */
  function getIndices($currentIndice_id = 0, $params=array() ){
    $query = 'SELECT '.$this->FIELDS_MAP_ID.' , indice_value FROM '.$this->OBJECT_TABLE
            .' WHERE '.$this->FIELDS_MAP_ID.' > '.$currentIndice_id;
    if(!$all = $this->dbranchbe->CacheGetAssoc(3600,$query)){
      Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
      return false;
    }else{
      //$all = $all->GetAssoc(); //To transform object result in array;
      return $all;
    }

  }//End of method

//-------------------------------------------------------------------
  /*! \brief
   */
  function getName($indice_id){
    if( isset($this->indices[$indice_id]) ) {
      return $this->indices[$indice_id];
    }
    if(!$this->indices[$indice_id] = $this->getBasicNumber($indice_id) ){
      return $this->indices[$indice_id] = tra('undefined');
    }else{
      return $this->indices[$indice_id];
    }
  }//End of method

} //End of class
