<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
CREATE TABLE `%container%_files` (
  `file_id` int(11) NOT NULL,
  `file_bid` CHAR(15) NOT NULL,
  `%container%_id` int(11) NOT NULL default '0',
  `import_order` int(11) default NULL,
  `file_name` varchar(128)  NOT NULL,
  `reposit_id` int(11) NOT NULL,
  `file_version` int(11) NOT NULL default 1,
  `file_iteration` int(11) NOT NULL default 1,
  `file_access_code` int(11) NOT NULL default 0,
  `file_life_stage` int(1) NOT NULL default 1,
  `file_path` text ,
  `file_root_name` varchar(128) NOT NULL,
  `file_extension` varchar(16) default NULL,
  `file_state` varchar(16) NOT NULL default 'init',
  `file_type` varchar(128) NOT NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) default NULL,
  `file_open_date` int(11) NOT NULL,
  `file_open_by` int(11) NOT NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  `from_file` int(11) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `UNIQ_%container%_files_2` (`file_bid`, `file_version`, `file_iteration`),
  KEY `INDEX_%container%_files_1` (`%container%_id`),
  KEY `INDEX_%container%_files_2` (`file_name`),
  KEY `INDEX_%container%_files_3` (`file_root_name`),
  KEY `INDEX_%container%_files_4` (`file_extension`),
  KEY `INDEX_%container%_files_5` (`file_type`),
  KEY `INDEX_%container%_files_6` (`file_md5`),
  KEY `INDEX_%container%_files_7` (`file_checkout_by`),
  KEY `INDEX_%container%_files_8` (`file_name`, `file_iteration`),
  KEY `INDEX_%container%_files_9` (`reposit_id`)
) ENGINE=InnoDB ;


--
-- Contraintes pour la table `%container%_files`
--
ALTER TABLE `%container%_files` 
  ADD CONSTRAINT `FK_%container%_files_1` FOREIGN KEY (`%container%_id`) 
  REFERENCES `%container%s` (`%container%_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%container%_files_2` FOREIGN KEY (`file_id`) 
  REFERENCES `objects` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;
  ADD CONSTRAINT `FK_%container%_files_3` FOREIGN KEY (`reposit_id`) 
  REFERENCES `reposits` (`reposit_id`) ON UPDATE CASCADE;
*/


//require_once('core/Recordfile/abstract.php');
/**! \brief It's a record in database of a file linked to a container.
 *
 */
class Rb_Recordfile extends Rb_Recordfile_Abstract {
	protected $container = false; //Object container, father container object of the Recordfile
	protected static $_registry = array (); //(array) registry of constructed objects
	protected $_saveRegistry = false;
	protected $_preCreateEventName = 'recordfile_pre_create';
	protected $_postCreateEventName = 'recordfile_post_create';
	protected $_preUpdateEventName = 'recordfile_pre_update';
	protected $_postUpdateEventName = 'recordfile_post_update';
	
	/** add this suffix to reposit path
	 * @var String
	 */
	public $reposit_suffix = '__imported';
	
	/**! \brief Constructor.
	 */
	public function __construct(Rb_Space $space, $file_id = 0, $manage_sub = false) {
		$this->space = & $space;
		$this->_init ( $file_id, $manage_sub );
		$this->reposit_suffix = Ranchbe::getConfig()->reposit->suffix;
	} //End of method
	

	//-------------------------------------------------------------------------
	/**! \brief init the property of the container
	 * 
	 * \param $container_id(integer) Id of the container
	 */
	protected function _init($file_id, $manage_sub = false) {
		
		$this->MANAGE_SUB = $manage_sub; //if you want manage subdirectories
		$this->OBJECT_TABLE = $this->space->FILE_TABLE;
		$this->FIELDS_MAP_FATHER = $this->space->CONT_FIELDS_MAP_ID;

		$this->_dao = new Rb_Dao ( Ranchbe::getDb () );
		$this->_dao->setTable ( $this->OBJECT_TABLE );
		$this->_dao->setKey ( $this->FIELDS_MAP_ID, 'primary_key' );
		$this->_dao->setKey ( $this->FIELDS_MAP_NUM, 'number' );
		$this->_dao->setKey ( $this->FIELDS_MAP_NAME, 'name' );
		$this->_dao->setKey ( $this->FIELDS_MAP_DESC, 'description' );
		$this->_dao->setKey ( $this->FIELDS_MAP_STATE, 'state' );
		$this->_dao->setKey ( $this->FIELDS_MAP_VERSION, 'version' );
		$this->_dao->setKey ( $this->FIELDS_MAP_FATHER, 'father' );
		$this->_dao->setSequence ( 'recordfile_seq' );

		$now = time ();
		$this->setProperty ( 'file_open_date', $now );
		$this->setProperty ( 'file_open_by', Rb_User::getCurrentUser ()->getId () );
		$this->setProperty ( 'file_update_date', $now );
		$this->setProperty ( 'file_update_by', Rb_User::getCurrentUser ()->getId () );
		$this->setProperty ( 'file_bid', uniqid () );
		$this->setProperty ( 'file_version', 1 );
		$this->setProperty ( 'file_iteration', 1 );
		$this->setProperty ( 'file_life_stage', 1 );
		return Rb_Object_Acl::_init ( $file_id );
	} //End of method
	
	//--------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Rb_Observable#_initObservers()
	 */
	protected function _initObservers() {
		if( $this->_observerIsInit ) return;
		$this->attach_all ( new Rb_Observer_Datatypescript ( ) );
		$this->_observerIsInit = true;
	} //End of method
	
	//-------------------------------------------------------------------------
	/**! \brief save current instance in registry
	 */
	protected function _saveRegistry() {
		if ( $this->_id > 0 && $this->_saveRegistry )
			self::$_registry [$this->_id] = & $this;
	}
	
	
	//-------------------------------------------------------------------------
	/**! \brief return a instance of object.
	 *   if none parameter return a special instance with no possibilies to change here properties
	 *   and wich can not be saved. This instance is just for use in static method where access to database is require,
	 *   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
	 *   If object "$id" dont exist return false.
	 *   If object "$id" has already been instantiated, then return this instance (singleton pattern).
	 * 
	 */
	public static function get($space_name, $id = -1) {
		if ($id < 0)
			$id = - 1; //forced to value -1
		if ($id == 0){
			return new Rb_Recordfile ( Rb_Space::get ( $space_name ), 0 );
		}
		if (! self::$_registry [$id]  ) { //always use registry for -1 object
			return self::$_registry [$id] = new Rb_Recordfile ( Rb_Space::get ( $space_name ), $id );
		}
		return self::$_registry [$id];
	} //End of method
	

	//-------------------------------------------------------------------------
	/**! \brief
	 */
	public static function getFileId($space_name, $file_name) {
		//return call_user_method('get',get_called_class(),$space_name)->_getFileId($file_name);
		return self::get ( $space_name )->_getFileId ( $file_name );
	} //End of method
	
	
	//-------------------------------------------------------------------------
	/**!\brief get the father object of current object
	 * Return Rb_Object_Acl or false.
	 *   
	 */
	function getFather() {
		if ($this->container)
			return $this->container;
		if ($this->getId () < 0)
			return false;
		if (! $this->getProperty ( 'father_id' )) {
			trigger_error ( 'can not get father_id property', E_USER_WARNING );
			return false;
		}
		return $this->container = & Rb_Container::get ( $this->space->getName (), $this->getProperty ( 'father_id' ) );
	} //End of method
	

	//-------------------------------------------------------------------------
	/**!\brief set father of current object
	 * Return $this or false.
	 *   
	 */
	function setFather(&$container) {
		return $this->setContainer ( $container );
	} //End of method
	

	//----------------------------------------------------------
	/**
	 * Set the container linked to the current Recordfile
	 */
	function setContainer(Rb_Container &$container) {
		$this->setProperty ( 'father_id', $container->getId () );
		$this->container = & $container;
	} //End of method
	

	//----------------------------------------------------------
	/**
	 * Set import order of Recordfile
	 */
	function setImportOrder($id) {
		$this->core_props ['import_order'] = $id;
	} //End of method
	

	//----------------------------------------------------------
	/**
	 */
	public function setProperty($property_name, $property_value) {
		if ($property_name == 'container_id')
			$property_name = $this->FIELDS_MAP_FATHER;
		return parent::setProperty ( $property_name, $property_value );
	} //End of method
	

	//--------------------------------------------------------
	/**
	 */
	function getProperty($property_name) {
		switch ($property_name) {
			case 'container_id' :
				$property_name = $this->FIELDS_MAP_FATHER;
				break;
			case 'file_path' :
				return Rb_Reposit::getBasePath ( $this->getReposit (), $this->reposit_suffix );
				break;
		} //End of switch
		return parent::getProperty ( $property_name );
	} //End of method
	
	
	//-------------------------------------------------------------------------
	/**! \brief suppress current object database records, files, and all depandancies
	 * and clean the registry of current class to prevent recall of this instance.
	 * can not call this method from instance where id <= 0.
	 * Return false or true.
	 * 
	 */
	public function suppress() {
		if ($this->_id < 1) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, 
					array ('id' => $this->_id ), 
					tra ( 'can not performed this action on object id %id%' ) );
			trigger_error ( 'can not performed this action on object id ' . $this->_id, E_USER_WARNING );
			return false;
		}
		
		//Notiy observers on event
		$this->notify_all('recordfile_pre_suppress', $this);
		if( $this->stopIt ) { //use by observers to control execution
			return false;
		}
		
		$ret = $this->_dao->suppress($this->_id);
		if($ret){
			$this->_suppressPermanentObject();
			//Write history
			//$this->writeHistory('Suppress');
			//clean registry
			unset( self::$_registry[$this->_id] );
		}else{
			Ranchbe::getError()->push(Rb_Error::ERROR, array('name'=>$this->getName()),
			tra('suppress failed for object %name%'));
		}
		
		//Notiy observers on event
		$this->notify_all('recordfile_post_suppress', $this);
		
		return $ret;
		
		
		/*		
		$ret = $this->_suppressPermanentObject ();
		Ranchbe::getAcl ()->remove( $object );
		if($ret){
			$this->writeHistory ( 'Suppress' );
			unset ( self::$_registry [$this->_id] );
		}
		return $ret;
		*/
	} //End of method
	
	
	/*	
	//-------------------------------------------------------------------------
	public static function flushRegistry() {
		$i=1;
		foreach(self::$_registry as $key=>&$element){
			if( $key == -1 ) continue;
			self::$_registry[$key] = null;
		}
	}
	*/

	//-------------------------------------------------------------------------
	/** For debug
	 * 
	 * @return unknown_type
	 */
	function __destruct() {
		if( Ranchbe::getAcl ()->has( $this->getResource() ) ){
			Ranchbe::getAcl ()->remove( $this->getResource() );
		}
		//print "Destruction de " . $this->getName() . "<br>";
	}
	
	
} //End of class
