<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


/** Access database classe to a single table
 *
 */
class Rb_Dao extends Rb_Dao_Abstract {
	
	//-------------------------------------------------------------------------
	/**
	 * 
	 * @param ADOConnection $db
	 * @return void
	 */
	public function __construct(ADOConnection &$db) {
		$this->dbranchbe =& $db;
	} //End of method
	
	//----------------------------------------------------------------------
	/** Implement Rb_Dao_Abstract::_basicCreate() method
	 * 
	 * @param Array $data
	 * @return Integer
	 */
	public function create($data) {
		return $this->_basicCreate ( $data );
	} //End of method
	

	//----------------------------------------------------------------------
	/** Implement Rb_Dao_Abstract::_basicUpdate() method
	 * 
	 * @param Array $data
	 * @param Integer $object_id
	 * @return Bool
	 */
	public function update($data = array(), $object_id) {
		return $this->_basicUpdate ( $data, $object_id );
	} //End of method
	

	//----------------------------------------------------------------------
	/** Implement Rb_Dao_Abstract::_basicReplace() method
	 * 
	 * @param Array $data
	 * @param Integer $object_id
	 * @return Bool
	 */
	public function replace($data = array(), $object_id) {
		return $this->_basicReplace ( $data, $object_id );
	} //End of method
	

	//----------------------------------------------------------------------
	/** Standard method to suppress a record in database 
	 *   Return true if success, false else.
	 *   This method use the transactions.
	 * 
	 * @param Integer $object_id
	 * @return Bool
	 */
	public function suppress($object_id) {
		return $this->_basicSuppress ( $object_id );
	} //End of method
	

	//----------------------------------------------------------------------
	/** Standard method to suppress a record in database limit to query
	 *   Return true if success, false else.
	 *   This method use the transactions.
	 * @param array $params
	 * @return Bool
	 */
	public function suppressQuery(array $params = array()) {
		return $this->_basicSuppressQuery ( $params );
	} //End of method
	

	//----------------------------------------------------------------------
	/** Alias of Rb_Dao_Abstract::getAllBasic() method
	 * 
	 * @param array $params
	 * @return Array
	 */
	public function getAll(array $params = array()) {
		return $this->getAllBasic ( $params );
	} //End of method
	

}// End of class
