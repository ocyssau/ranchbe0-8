<?php

//require_once('core/filesystem.php');
//require_once('core/datatype/interface.php');

/** A file on the filesystem
 * 
 * @author Administrateur
 *
 */
class Rb_File extends Rb_Filesystem implements Rb_Datatype_Interface{

	protected $file; //(string) full path of the File
	protected $file_id; //(string) path and full name of the File.
	protected $file_class;
	protected $test_mode=false; //(Bool) if true, test write operations only

	protected $file_props; //(array) content all properties of the File.
	protected $file_name; //(string) Name of the File 'File.ext'
	protected $file_path; //(string) path to the File '/dir/sub_dir'
	protected $file_extension; //(string) extension of the File '.ext'
	protected $file_root_name; //(string) root name of the File 'File'
	protected $file_size; //(integer) Size of the File in octets
	protected $file_mtime; //(integer) modification time of the File
	protected $file_type; //(string) type of File = File, adrawc5, adrawc4, camu, cadds4, cadds5, pstree...
	protected $file_md5; //(string) md5 code of the File
	protected $file_iteration; //(integer) version of the File

	public $displayMd5 = false; //true if you want return the md5 property of the File

	//----------------------------------------------------------
	function __construct($file){
		$this->file = $file;
		$this->file_props['file_type'] = 'File';
		$this->file_type =& $this->file_props['file_type'];
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Datatype/Rb_Datatype_Interface#getProperty($property_name)
	 */
	function getProperty($property_name){
		if(!is_file($this->file))
	 return false;
	 switch ($property_name){
	 	case 'natif_file_name':
	 	case 'file_name':
	 		if(isset($this->file_props['file_name'])) return $this->file_props['file_name'];
	 		$this->file_props['file_name'] = basename($this->file);
	 		return $this->file_name =& $this->file_props['file_name'];
	 		break;

	 	case 'file_size':
	 		if(isset($this->file_props['file_size'])) return $this->file_props['file_size'];
	 		$this->file_props['file_size'] = filesize($this->file);
	 		return $this->file_size =& $this->file_props['file_size'];
	 		break;

	 	case 'file_path':
	 		if(isset($this->file_props['file_path'])) return $this->file_props['file_path'];
	 		$this->file_props['file_path'] = dirname($this->file);
	 		return $this->file_path =& $this->file_props['file_path'];
	 		break;

	 	case 'file_version':
	 		if(isset($this->file_props['file_version'])) return $this->file_props['file_version'];
	 		return false;
	 		break;

	 	case 'file_iteration':
	 		if(isset($this->file_props['file_iteration'])) return $this->file_props['file_iteration'];
	 		return false;
	 		break;

	 	case 'file_mtime':
	 		if(isset($this->file_props['file_mtime'])) return $this->file_props['file_mtime'];
	 		$this->file_props['file_mtime'] = filemtime($this->file);
	 		return $this->file_mtime =& $this->file_props['file_mtime'];
	 		break;

	 	case 'file_extension':
	 		if(isset($this->file_props['file_extension'])) return $this->file_props['file_extension'];
	 		$this->file_props['file_extension'] = Rb_File::GetExtension($this->file);
	 		return $this->file_extension =& $this->file_props['file_extension'];
	 		break;
 
	 	case 'doc_name':
	 	case 'file_root_name':
	 		if(isset($this->file_props['file_root_name'])) return $this->file_props['file_root_name'];
	 		$this->file_props['file_root_name'] = $this->getRoot();
	 		return $this->file_root_name =& $this->file_props['file_root_name'];
	 		break;

	 	case 'file_type':
	 		if($this->file_props['file_type'])
	 		return $this->file_props['file_type'];
	 		else return 'File';
	 		break;

	 	case 'file':
	 		return $this->file_props['file'] =& $this->file;
	 		break;

	 	case 'file_md5':
	 		if(isset($this->file_props['file_md5'])) return $this->file_props['file_md5'];
	 		$this->file_props['file_md5'] = md5_file($this->file);
	 		return $this->file_md5 =& $this->file_props['file_md5'];
	 		break;

	 	case 'file_mimetype':
	 		if(isset($this->file_props['file_mimetype'])) return $this->file_props['file_mimetype'];
	 		$this->getMimeTypeInfos();
	 		return $this->file_mimetype;
	 		break;

	 	case 'visu2D_extension':
	 		if(isset($this->file_props['visu2D_extension'])) return $this->file_props['visu2D_extension'];
	 		$this->getMimeTypeInfos();
	 		return $this->visu2D_extension;
	 		break;

	 	case 'visu3D_extension':
	 		if(isset($this->file_props['visu3D_extension'])) return $this->file_props['visu3D_extension'];
	 		$this->getMimeTypeInfos();
	 		return $this->visu3D_extension;
	 		break;

	 	case 'no_read':
	 		if(isset($this->file_props['no_read'])) return $this->file_props['no_read'];
	 		$this->getMimeTypeInfos();
	 		return $this->no_read;
	 		break;

	 	case 'viewer_driver':
	 		if(isset($this->file_props['viewer_driver'])) return $this->file_props['viewer_driver'];
	 		$this->getMimeTypeInfos();
	 		return $this->viewer_driver;
	 		break;

	 	case 'reposit_sub_dir':
	 		if( isset( $this->reposit_sub_dir ) ) return $this->reposit_sub_dir;
	 		break;

	 } //End of switch
	}//End of method

	//-------------------------------------------------------
	/** Return the infos record in the conf/mimes.csv file about the extension of the $filename.
	 * 
	 * @return array|false
	 */
	private function getMimeTypeInfos() {
		$mimefile = Ranchbe::getConfig()->mimes_file;
		$this->getProperty('file_extension');

		$handle = fopen($mimefile, "r");
		if(!$handle) return false;

		while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
			//$mimetype = $data[0];
			//$description = $data[1];
			//$extension = $data[2];
			//$no_read = $data[3];
			//viewer_driver = $data[4];
			//$visu2D = $data[5]; experimental
			//$visu3D = $data[6]; experimental
			if (strstr($data[2] , $this->file_extension)){
				$this->file_props['file_mimetype'] = $data[0];
				$this->file_mimetype =& $this->file_props['file_mimetype'];
				$this->file_props['file_mimetype_description'] = $data[1];
				$this->file_mimetype_description =& $this->file_props['file_mimetype_description'];
				$this->file_props['no_read'] = $data[3];
				$this->no_read =& $this->file_props['no_read'];
				$this->file_props['viewer_driver'] = $data[4]; //Experimental
				$this->viewer_driver =& $this->file_props['viewer_driver'];
				//$this->file_props['visu2D_extension'] = $data[5]; //Experimental
				//$this->visu2D_extension =& $this->file_props['visu2D_extension'];
				//$this->file_props['visu3D_extension'] = $data[6]; //Experimental
				//$this->visu3D_extension =& $this->file_props['visu3D_extension'];
				return $data;
				break;
			}
		}
		fclose($handle);

		return false;
	}//End of method

	//-------------------------------------------------------
	/** Get infos about the File or the cadds part.
	 * Return a array if no errors, else return false.
	 * 
	 * (non-PHPdoc)
	 * @see library/Rb/Datatype/Rb_Datatype_Interface#getInfos($displayMd5)
	 * 
	 * @param $displayMd5(bool) true if you want return the md5 code of the File.
	 * 
	 */
	function getInfos($displayMd5=true){
		//Be careful, never change key name of this array. Else the key name must be existing in all database files table
		if(is_file($this->file)){
			$this->file_props = array('file_name'      => $this->getProperty('file_name'),
                              'natif_file_name'=> $this->getProperty('natif_file_name'),
                              'file_size'      => $this->getProperty('file_size'),
                              'file_path'      => $this->getProperty('file_path'),
                              'file_iteration' => $this->getProperty('file_iteration'),
                              'file_mtime'     => $this->getProperty('file_mtime'),
                              'file_extension' => $this->getProperty('file_extension'),
                              'file_root_name' => $this->getProperty('file_root_name'),
                              'file_type'      => $this->getProperty('file_type'),
                              'File'           => $this->file,
			);
			if($displayMd5)
			$this->file_props['file_md5'] = md5_file($this->file);
			return $this->file_props;
		}else return false;
	}//End of method

	//-------------------------------------------------------
	function getProperties($displayMd5=true){
		return $this->getInfos($displayMd5);
	}//End of method

	//-------------------------------------------------------
	/**
	 * Return File extension.(ie: /dir/File.ext, return '.ext')
	 *
	 * @return string
	 */
	function getExtension(){
		return self::sGetExtension($this->file);
	}//End of function

	//-------------------------------------------------------
	/**
	 * Static method version of getExtension()
	 *
	 * @param string $file_name
	 * @return string
	 */
	static function sGetExtension($file_name){
		return substr($file_name, strrpos($file_name, '.'));
	}//End of function

	//-------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Datatype/Rb_Datatype_Interface#getRoot()
	 * 
	 * Return root name of File.(ie: /dir/File.ext, return 'File')
	 * @return string
	 * 
	 */
	function getRoot(){
		return self::sGetRoot( $this->getProperty('file_name') );
	}//End of function

	//-------------------------------------------------------
	/**
	 * Static method version of getRoot()
	 *
	 * @param string $file_name
	 * @return string
	 */
	static function sGetRoot($file_name){
		return substr($file_name, 0, strrpos($file_name, '.'));
	}//End of function

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Datatype/Rb_Datatype_Interface#move($dst, $replace)
	 * 
	 * @param $dst(string) fullpath to new File
	 * @param $replace(bool) true for replace File

	 */
	function move($dst , $replace=false){
		if(!is_file($this->file)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$this->file, 'debug'=>array()), '%element% is not a File');
			return false;
		}

		if (!Rb_Filesystem::limitDir($dst)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$dst, 'debug'=>array()), 'you have no right access on File %element%');
			return false;
		}

		if(!is_dir(dirname($dst))){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>dirname($dst), 'debug'=>array()), 'the directory %element% dont exist');
			return false;
		}

		//Copy File , and suppress File
		if($this->copy($dst , 0755 , $replace)){
			$this->suppress();
			$this->file = $dst;
			$this->file_props['file_name'] = basename($this->file);
			$this->file_props['file_path'] = dirname($this->file);
			$this->file_props['file_extension'] = Rb_File::getExtension($this->file);
			$this->file_props['file_root_name'] = Rb_File::getRoot(basename($this->file));
			return true;
		}else return false;

	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Datatype/Rb_Datatype_Interface#copy($dst, $mode, $replace)
	 */
	function copy($dst, $mode=0755, $replace=true){
		if(!Rb_Filesystem::limitDir($this->file)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$this->file, 'debug'=>array()), 'copy error : you have no right access on File %element%');
			return false;
		}

		if(!Rb_Filesystem::limitDir($dst)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$dst, 'debug'=>array()), 'copy error : you have no right access on File %element%');
			return false;
		}

		if(is_file($dst) && !$replace){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$dst, 'debug'=>array()), 'copy error: File %element% exist');
			return false;
		}

		if( is_dir($dst) ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$dst, 'debug'=>array()), 'copy error: %element% exist and is a directory');
			trigger_error("copy error: $dst exist and is a directory", E_USER_ERROR);
			die;
			return false;
		}

		if(is_file($this->file)){
			if(copy($this->file , $dst)){
				touch($dst, filemtime($this->file));
				chmod ($dst , $mode);
				return true;
			}else{
				Ranchbe::getError()->push(Rb_Error::ERROR, array('src'=>$this->file, 'target'=>$dst ), 'copy error from File %src% to %target%');
				return false;
			}
		}else{
			Ranchbe::getError()->push(Rb_Error::ERROR, array('src'=>$this->file), '%src% is not a plain File');
			return false;
		}

	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Datatype/Rb_Datatype_Interface#suppress()
	 */
	function suppress(){
		if(!Rb_File::putInTrash($this->file)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$this->file, 'debug'=>array()), 'suppress of File %element% failed');
			return false;
		}else return true;
	}//End of method

	//-------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Datatype/Rb_Datatype_Interface#putInTrash($verbose, $trashDir)
	 */
	function putInTrash($verbose = false, $trashDir=DEFAULT_TRASH_DIR){
		//Check if trash exists and is writable
		if(!is_dir($trashDir) || !is_writable($trashDir)){
			print ' error : '. "$trashDir" . ' don\'t exist <br /> or is not writable';
			return false;
		}

		//Add original path to name of dstfile
		//$oriPath = $this->encodePath(dirname($dir));
		$oriPath = $this->encodePath(dirname($this->file));
		/*
		 $oriPath = str_replace( '/' , '%2F', "$oriPath" );
		 $oriPath = str_replace( '.' , '%2E', "$oriPath" );
		 $oriPath = str_replace( ':' , '%3A', "$oriPath" );
		 $oriPath = str_replace( '\\' , '%5C', "$oriPath" );
		 */

		//Check if there is not conflict name in trash dir else rename it
		$dstfile = $trashDir .'/'. "$oriPath" .'%&_'. basename($this->file);
		$i = 0;
		if(is_file($dstfile)){
			$Renamedstfile = $dstfile; //Rename destination dir if exist
			$path_parts = pathinfo($Renamedstfile);

			while(is_file($Renamedstfile)){
				$Renamedstfile = $path_parts['dirname'] .'/'. $path_parts['filename'] . '(' . $i . ')' . '.' . $path_parts['extension'];
				$i ++;
			}
			$dstfile = $Renamedstfile;
		}

		//Copy File to trash
		if (!$this->copy($dstfile)){ //copy File to dirtrash
			Ranchbe::getError()->push(Rb_Error::ERROR, array('File'=>$this->file, 'target'=>$trashDir ), 'error copying File %File% to Trash %target%');
			//print 'error copying File '. $this->File . ' to Trash: '. $trashDir . ' <br />';
			return false;
		}else{ //Suppress original File
			chmod($dstfile , 0755);
			if (!unlink($this->file)){
				Ranchbe::getError()->push(Rb_Error::ERROR, array('File'=>$this->file, 'target'=>$trashDir ), 'error during suppressing File %File%');
				//print 'info: error in suppressing File :'. $this->File . ' <br />';
				return false;
			}
		}

		Ranchbe::getError()->info('File %File% has been put in trash', array('File'=>$this->file));
		return true;

	}//End of function

	//-------------------------------------------------------------------
	function downloadFile(){
		$fileName = $this->getProperty('file_name');
		$mimeType = $this->getProperty('file_mimetype');
		if($mimeType == 'no_read')
		return false;
		header("Content-disposition: attachment; filename=$fileName");
		header("Content-Type: " . $mimeType);
		header("Content-Transfer-Encoding: $fileName\n"); // Surtout ne pas enlever le \n
		header("Content-Length: ".filesize($this->file));
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		readfile($this->file);
		return true; //no die, put it in rb_fsdata::downloadFile()
	}//End of method

	//-------------------------------------------------------
	/** Copy a upload File to $dstfile
	 * 
	 * @param array $file
	 *		$file[name](string) name of the uploaded File
 	 *		$file[type](string) mime type of the File
	 *		$file[tmp_name](string) temp name of the File on the server
	 *		$file[error](integer) error code if error occured during transfert(0=no error)
	 *		$file[size](integer) size of the File in octets
	 * 	
	 * @param boolean $replace 	if true and if the File exist in the wildspace, it will be replaced by the uploaded File.
	 * @param string $dstdir 	Path of the dir to put the File.
	 * @return boolean
	 */
	static function uploadFile($file , $replace, $dstdir){
		$dstfile = $dstdir.'/'.$file['name'];
		if(!$replace) //Test if File exist in target dir only if replace=false
		if(is_file($dstfile)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$dstfile), '%element% File exist');
			return false;}
			if(!is_file($file['tmp_name'])){
				Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$file['tmp_name'], 'debug'=>array($file)), '%element% is not a File');
				return false;}
				if(copy($file['tmp_name'] , $dstfile)){
					chmod ($dstfile , 0775);
					return true;
				}else{
					Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$file['tmp_name'], 'debug'=>array($file['tmp_name'] , $dstfile)), 'can\'t copy File %element%');
					return false;}
	}//End of method

	//----------------------------------------------------------
	/** Rename the current File
	 * 
	 * @param string $newName fullpath to new File
	 * @return boolean
	 */
	function rename($newName){
		$newName = trim($newName);

		if(!Rb_Filesystem::limitDir($this->file)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$this->file, 'debug'=>array()), 'rename error : you have no right access on File %element%');
			return false;
		}

		if(!Rb_Filesystem::limitDir($newName)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$newName, 'debug'=>array()), 'rename error : you have no right access on File %element%');
			return false;
		}

		if(empty($newName)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$newName, 'debug'=>array()), 'rename error: none specified new name');
			return false;
		}

		if(is_file($newName)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$newName, 'debug'=>array()), 'rename error: File %element% exist');
			return false;
		}

		if (!is_writeable($this->file)) {
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$this->file), 'rename error : File %element% is not writable');
			return false;
		}

		if (!@rename($this->file, $newName)) {
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$this->file,'newName'=>$newName), 'rename error : can not rename File %element% to %newName%');
			return false;
		}
		
		$this->file = $newName;
		
		unset($this->file_props['file_name']);
		unset($this->file_props['file_path']);
		unset($this->file_props['file_extension']);
		unset($this->file_props['file_root_name']);
		unset($this->file_props['file_mimetype']);
		unset($this->file_props['visu2D_extension']);
		unset($this->file_props['visu3D_extension']);
		unset($this->file_props['viewer_driver']);
		unset($this->file_props['no_read']);
		unset($this->file_props['reposit_sub_dir']);
		
		return true;

	}//End of method

} //End of class

