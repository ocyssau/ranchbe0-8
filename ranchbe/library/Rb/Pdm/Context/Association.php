<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
TABLE DEFINITION :
DROP TABLE `pdm_context_association_seq`;
DROP TABLE `pdm_context_association`;
CREATE TABLE `pdm_context_association` (
  `link_id` int(11) NOT NULL,
  `pd_id` int(11) NOT NULL,
  `act_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL default 0,
  PRIMARY KEY  (`link_id`),
  UNIQUE KEY `UNIQ_pdm_context_association_1` (`pd_id`,`act_id`),
  KEY `INDEX_pdm_context_association_1` (`pd_id`),
  KEY `INDEX_pdm_context_association_2` (`act_id`),
  KEY `INDEX_pdm_context_association_3` (`role_id`)
);
*/

//require_once('core/pdm/abstract.php');
//require_once('core/pdm/context/application.php');
//require_once('core/pdm/context/role.php');
//require_once('core/pdm/product/definition.php');
//-------------------------------------------------------------------------
/*! \brief This class implement the product_definition_context_association entity of STEP PDM SHEMA.

 * The product_definition_context_association entity allows for multiple additional 
 * contexts to be associated with a single product_definition.
 *
 * With 'Part as Product', there is always one required 'primary' context that identifies 
 * the defining application domain and life-cycle information from which the data 
 * is viewed. This 'primary' context is the value of the attribute 
 * product_definition.frame_of_reference.
 * Additional product_definition_context entities identify additional concurrent relevant 
 * views on the product_definition. These additional contexts are related to the product 
 * definition by the entity product_definition_context_association.
 */
class Rb_Pdm_Context_Association extends Rb_Pdm_Abstract{

  protected $OBJECT_TABLE = 'pdm_context_association'; //(String)Table where are stored items definitions

  //Fields:
  protected $FIELDS_MAP_ID = 'link_id'; //(String)Name of the field of the primary key

  protected $core_props = array(
    'pd_id'=>0, //id of Rb_Pdm_Product_Definition
    'act_id'=>0, //id of Rb_Pdm_Context_Productdefinition
    'role_id'=>0, //id of Rb_Pdm_Context_Role
  ); //(array) contains the properties of the current object

  protected static $_registry = array(); //(array) registry of instances

  /**
   * attribute provides a reference to the associated product_definition
   *
   * @var Rb_Pdm_Product_Definition
   */
  protected $_definition = false; //(Rb_Pdm_Product_Definition)

  /**
   * attribute is a pointer to the associated product_definition_context
   *
   * @var Rb_Pdm_Context_Productdefinition
   */
  protected $_context = false; //(Rb_Pdm_Context_Productdefinition)

  /**
   * attribute gives an optional role indication to the association.
   *
   * @var Rb_Pdm_Context_Role
   */
  protected $_role = false; //(Rb_Pdm_Context_Role)

//--------------------------------------------------------------------
function __construct( $id = 0 ){
  $this->dbranchbe =& Ranchbe::getDb();
  $this->error_stack  =& Ranchbe::getError();
  $this->_init($id);
}//End of method

//-------------------------------------------------------------------------
/*! \brief return a instance of object.
*   if none parameter return a special instance with no possibilies to change here properties
*   and wich can not be saved. This instance is just for use in static method where access to database is require,
*   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
*   If object "$id" dont exist return false.
*   If object "$id" has already been instantiated, then return this instance (singleton pattern).
* 
*/
public static function get( $id = -1 ){
  if($id < 0) $id = -1; //forced to value -1

  if($id == 0)
    return new self(0);

  if( !self::$_registry[$id] ){
    self::$_registry[$id] = new self($id);
  }

  return self::$_registry[$id];
}//End of method

//--------------------------------------------------------------------
/*!\brief record current instance in class registry.
*
*/
protected function _saveRegistry(){
    self::$_registry[$this->_id] =& $this;
} //End of method

//----------------------------------------------------------
/*! \brief
 */
public function setDefinition(Rb_Pdm_Product_Definition &$definition){
  if($this->getId() < 0) return false;
  if($definition->getId() < 1) return false;
  $this->setProperty( 'pd_id', $definition->getId() );
  $this->_definition =& $definition;
  return $this->_definition;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getDefinition(){
  if( $this->getId() < 0 ) return false;
  if( $this->_definition ) return $this->_definition;
  if( !$this->getProperty('pd_id') ){
    return false;
  }
  return $this->_definition =& Rb_Pdm_Product_Definition::get( $this->getProperty('pd_id') );
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function setContext(Rb_Pdm_Context_Productdefinition &$context){
  if($this->getId() < 0) return false;
  if($context->getId() < 1) return false; //the context must be recorded before
  $this->setProperty( 'act_id', $context->getId() );
  $this->_context =& $context;
  return $this->_context;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getContext(){
  if( $this->getId() < 0 ) return false;
  if( $this->_context ) return $this->_context;
  if( !$this->getProperty('act_id') ){
    return false;
  }
  return $this->_context =& Rb_Pdm_Context_Productdefinition::get( $this->getProperty('act_id') );
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function setRole(Rb_Pdm_Context_Role &$role){
  if($this->getId() < 0) return false;
  if($role->getId() < 1) return false; //the context must be recorded before
  $this->setProperty( 'role_id', $role->getId() );
  $this->_role =& $role;
  return $this->_role;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getRole(){
  if( $this->getId() < 0 ) return false;
  if( $this->_role ) return $this->_role;
  if( !$this->getProperty('role_id') ){
    return false;
  }
  return $this->_role =& Rb_Pdm_Context_Role::get( $this->getProperty('role_id') );
}//End of method

}//End of class
