<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
TABLE DEFINITION :
DROP TABLE `pdm_context_role_seq`;
DROP TABLE `pdm_context_role`;

CREATE TABLE `pdm_context_role` (
  `role_id` int(11) NOT NULL,
  `name` varchar(128) NULL,
  `description` varchar(512) NULL,
  PRIMARY KEY  (`role_id`),
  KEY `INDEX_pdm_context_role_1` (`name`),
  KEY `INDEX_pdm_context_role_2` (`description`)
);
*/
//require_once('core/pdm/abstract.php');

//-------------------------------------------------------------------------
/*! \brief This class implement the product_definition_context_role entity of STEP PDM SHEMA.

 * The product_definition_context_role entity provides a role string that is
 * to a product_definition_context_association entity.
 *
 */
class Rb_Pdm_Context_Role extends Rb_Pdm_Abstract{
  protected $OBJECT_TABLE = 'pdm_context_role'; //(String)Table where are stored items definitions

  //Fields:
  protected $FIELDS_MAP_ID = 'role_id'; //(String)Name of the field of the primary key
  protected $FIELDS_MAP_NAME = 'name'; //(String)Name of the field to define the name
  protected $FIELDS_MAP_DESC = 'description'; //(String)Name of the field to define the description

  protected $core_props = array(
    'name'=>'', //word or group of words by which the role is referred
    'description'=>'', //additional descriptive information related to the role
  ); //(array) contains the properties of the current object

  protected static $_registry = array(); //(array) registry of instances

//--------------------------------------------------------------------
function __construct( $id = 0 ){
  $this->dbranchbe =& Ranchbe::getDb();
  $this->error_stack  =& Ranchbe::getError();
  $this->_init($id);
}//End of method

//-------------------------------------------------------------------------
/*! \brief return a instance of object.
*   if none parameter return a special instance with no possibilies to change here properties
*   and wich can not be saved. This instance is just for use in static method where access to database is require,
*   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
*   If object "$id" dont exist return false.
*   If object "$id" has already been instantiated, then return this instance (singleton pattern).
* 
*/
public static function get( $id = -1 ){
  if($id < 0) $id = -1; //forced to value -1

  if($id == 0){
    return new self(0);
  }

  if( !self::$_registry[$id] ){
    self::$_registry[$id] = new self($id);
  }

  return self::$_registry[$id];
}//End of method

//--------------------------------------------------------------------
/*!\brief record current instance in class registry.
*
*/
protected function _saveRegistry(){
    self::$_registry[$this->_id] =& $this;
} //End of method

}//End of class

