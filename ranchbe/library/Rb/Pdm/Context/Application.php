<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
TABLE DEFINITION :
CREATE TABLE `pdm_context_application` (
  `cta_id` int(11) NOT NULL,
  `application` varchar(128) NULL,
  PRIMARY KEY  (`cta_id`),
  KEY `INDEX_pdm_context_productdefinition_1` (`application`)
);
*/

//-------------------------------------------------------------------------
/*! \brief This class implement the application_context entity of STEP PDM SHEMA.

 * The application_context entity identifies the application domain that defined
 * the data. The application_context entity may have an identifier associated with it
 * through the entity id_attribute and its attribute_value attribute. The application_context
 * entity may have a description associated with it through the entity description_attribute 
 * via the attribute attribute_value. It is not recommended to instantiate these optional 
 * values.
 *
 * In the case of application protocol identification the application domain is optional
 * which provides a basis for the interpretation of all information represented in 
 * the product data exchange.
 *
 * In the case of application context information, there exists a 'primary' application 
 * context for each product_definition. This is the value of the attribute product_definition
 * it is the frame_of_reference for the 'primary' product_definintion_context
 * (see product_definition_context). This 'primary' application context represents the 
 * defining application domain for each product_definition. Additional application 
 * domains may be associated with a product_definition through additional product_definition_contexts 
 * via the entity product_definition_context_association.
 *
 */
abstract class Rb_Pdm_Context_Application extends Rb_Pdm_Abstract{

  protected $core_props = array(
    'application'=>'' //name of the general application domain that defined the data.
  ); //(array) contains the properties of the current object

}//End of class

