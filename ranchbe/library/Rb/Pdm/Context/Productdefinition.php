<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
TABLE DEFINITION :
ALTER TABLE `pdm_context_application`
 ADD `name` VARCHAR( 128 ) NULL,
 ADD `life_cycle_stage` VARCHAR( 128 ) NULL,
 ADD INDEX `INDEX_pdm_context_productdefinition_2` ( `name` ),
 ADD INDEX `INDEX_pdm_context_productdefinition_3` ( `life_cycle_stage` );
*/

//require_once('core/pdm/context/application.php');
//-------------------------------------------------------------------------
/*! \brief This class implement the product_definition_context entity of STEP PDM SHEMA.

 * The product_definition_context entity is a subtype of application_context_element
 * All STEP product_definitions must be founded in a product_definition_context to
 * identify the product definition type and life-cycle stage from which the data
 * is viewed.
 *
 * There are two ways to implement the product_definition_context in STEP:
 *
 * * association of a single primary context with a product_definition,
 * * assignment of more than one (additional) context to a product_definition.
 *
 * The value of the attribute product_definition.frame_of_reference is a product_definition_context
 * that represents the 'primary' defining context for a view. In general this defining 
 * context is qualified both by type (product_definition_context.name) and by life-cycle 
 * (product_definition_context.life_cycle_stage) information. The primary context 
 * also has associated application domain information (see application_context).
 *
 * The product_definition_context_association entity allows for multiple additional 
 * contexts to be associated with a single product_definition. There is always one 
 * required 'primary' context that identifies the defining application domain and 
 * life-cycle information. Additional product_definition_context entities identify 
 * additional concurrent relevant views on the product_definition. These application_contexts
 * are related to the product definition by product_definition_context_association.
 *
 */
class Rb_Pdm_Context_Productdefinition extends Rb_Pdm_Context_Application{

  protected $OBJECT_TABLE = 'pdm_context_application'; //(String)Table where are stored items definitions

  //Fields:
  protected $FIELDS_MAP_ID = 'cta_id'; //(String)Name of the field of the primary key
  protected $FIELDS_MAP_NAME = 'name'; //(String)Name of the field to define the name

  protected $core_props = array(
    'application'=>'', //name of the general application domain that defined the data.
    'name'=>'', //attribute indicates the type of view being defined.
    'life_cycle_stage'=>'', //identifies the life-cycle view
  ); //(array) contains the properties of the current object

  protected static $_registry = array(); //(array) registry of instances

  /**
   * attribute is a pointer to the associated application_context entity.
   *
   * @var Rb_Pdm_Context_Application
   */
  protected $_frame_of_reference = false; //(Rb_Pdm_Context_Application)

//--------------------------------------------------------------------
function __construct( $id = 0 ){
  $this->dbranchbe =& Ranchbe::getDb();
  $this->error_stack  =& Ranchbe::getError();
  $this->_init($id);
}//End of method

//-------------------------------------------------------------------------
/*! \brief return a instance of object.
*   if none parameter return a special instance with no possibilies to change here properties
*   and wich can not be saved. This instance is just for use in static method where access to database is require,
*   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
*   If object "$id" dont exist return false.
*   If object "$id" has already been instantiated, then return this instance (singleton pattern).
* 
*/
public static function get( $id = -1 ){
  if($id < 0) $id = -1; //forced to value -1

  if($id == 0){
    return new self(0);
  }

  if( !self::$_registry[$id] ){
    self::$_registry[$id] = new self($id);
  }

  return self::$_registry[$id];
}//End of method

//--------------------------------------------------------------------
/*!\brief record current instance in class registry.
*
*/
protected function _saveRegistry(){
    self::$_registry[$this->_id] =& $this;
} //End of method

//--------------------------------------------------------------------
/*!\brief record current instance in class registry.
*
*/
public function suppress(){
  if($this->_id < 1) return false;
  return $this->_basicSuppress($this->_id);
} //End of method

}//End of class

