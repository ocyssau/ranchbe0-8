<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
 TABLE DEFINITION :
 DROP TABLE `pdm_effectivity`;
 DROP TABLE `pdm_effectivity_seq`;
 CREATE TABLE `pdm_effectivity` (
 `effectivity_id` int(11) NOT NULL,
 `number` varchar(128) NULL,
 `name` varchar(128) NULL,
 `description` varchar(512) NULL,
 `usage_id` int(11) NULL,
 `usage_type` int(11) NULL,
 `cd_id` int(11) NULL,
 `start_date` int(11) NULL,
 `end_date` int(11) NULL,
 `lot_id` int(11) NULL,
 `lot_size` int(11) NULL,
 `snum_start_id` int(11) NULL,
 `snum_end_id` int(11) NULL,
 PRIMARY KEY  (`effectivity_id`),
 UNIQUE KEY `UNIQ_pdm_effectivity_1` (`number`),
 KEY `INDEX_pdm_effectivity_1` (`name`),
 KEY `INDEX_pdm_effectivity_2` (`description`),
 KEY `INDEX_pdm_effectivity_3` (`usage_id`),
 KEY `INDEX_pdm_effectivity_4` (`cd_id`),
 KEY `INDEX_pdm_effectivity_5` (`start_date`),
 KEY `INDEX_pdm_effectivity_6` (`end_date`),
 KEY `INDEX_pdm_effectivity_7` (`lot_id`),
 KEY `INDEX_pdm_effectivity_8` (`snum_start_id`),
 KEY `INDEX_pdm_effectivity_9` (`snum_end_id`)
 );
 */

//require_once('core/pdm/abstract.php');

/*! \brief This class implement the effectivity entity of STEP PDM SHEMA.

The effectivity entity supports the specification of a domain of applicability
for product data. Effectivity is a generic concept defining the valid use of
the product data to which the effectivity is assigned.

NOTE - The assignment of effectivities is often done during the approval
process, i.e, when releasing product data for the next stage of the
development process, it gets effectivity information assigned to define when
and in which context these product data may be used.

Within the PDM Schema, there are two areas identified for the usage of the
effectivity concept: configuration effectivity and general validity period
effectivity. These two areas are reflected by two orthogonal subtype
structures defined for the effectivity entity.

One subtype structure contains the effectivity subtypes dated_effectivity,
lot_effectivity, serial_numbered_effectivity, and
time_interval_based_effectivity, restricting the domain of applicability of
the associated product data to a date range, a particular lot or serial number
range, or to a time interval respectively. There is a ONEOF constraint between
these subtypes, i.e., an effectivity can be, at most, one of dated_effectivity,
lot_effectivity, serial_numbered_effectivity, and
time_interval_based_effectivity.

The other subtype structure contains product_definition_effectivity as subtype
of effectivity, and configuration_effectivity as subtype of
product_definiton_effectivity. The subtypes in the second structure are
restricted to apply to a particular usage occurrence of a constituent part in
some higher-level assembly.
*/
abstract class Rb_Pdm_Effectivity extends Rb_Pdm_Abstract{
	protected $OBJECT_TABLE = 'pdm_effectivity'; //(String)Table name

	//Fields:
	protected $FIELDS_MAP_ID = 'effectivity_id'; //(String)Name of the field of the primary key
	protected $FIELDS_MAP_NUM = 'number'; //(String)Name of the field to define the number id
	protected $FIELDS_MAP_NAME = 'name'; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC = 'description'; //(String)Name of the field to define the description

	protected $core_props = array(
    'number'=>'',
    'name'=>'',
    'description'=>'',
	); //(array) contains the properties of the current object

	//--------------------------------------------------------------------
	function __construct( $id = 0 ){
		$this->dbranchbe =& Ranchbe::getDb();
		$this->error_stack  =& Ranchbe::getError();
		$this->_init($id);
	}//End of method

}//End of class

