<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
TABLE DEFINITION :
DROP TABLE `pdm_configuration_design_seq`;
CREATE TABLE `pdm_configuration_design` (
  `cd_id` int(11) NOT NULL,
  `name` varchar(128) NULL,
  `description` varchar(512) NULL,
  `ci_id` int(11) NOT NULL,
  `pv_id` int(11) NOT NULL,
  PRIMARY KEY  (`ci_id`),
  UNIQUE KEY  `UNIQ_pdm_configuration_design_1` (`ci_id`, `pv_id`),
  INDEX `INDEX_pdm_configuration_design_1` (`name`),
  INDEX `INDEX_pdm_configuration_design_2` (`ci_id`),
  INDEX `INDEX_pdm_configuration_design_3` (`pv_id`)
);
*/

//require_once('core/pdm/abstract.php');
//require_once('core/pdm/configuration/concept.php');

/*! \brief This class implement the configuration_item entity of STEP PDM SHEMA.

The configuration design entity represents an association between a 
configuration_item identifying a particular product configuration and a 
product design intended to implement that item. Thus, a configuration_design 
entity represents the association of a configuration_item with a 
product_definition or product_definition_formation to specify that the 
corresponding design is a solution for a given configuration_item. A given 
design may be the design for different configuration_items belonging to the 
same or even different product_concepts.

The configuration_design entity may have, at most, one name associated with it 
through the entity name_attribute. If a name is associated, the 
configuration_design is referenced as the named_item by a name_attribute where 
the name is stored in the attribute attribute_value. It is not recommended to 
instantiate this additional value.

The configuration_design entity may have a description associated with it 
through the entity description_attribute. If a description is associated, the 
configuration_design is referenced as the described_item by a 
description_attribute where the description is stored in the attribute 
attribute_value. It is not recommended to instantiate this additional value. 
Attributes

The configuration attribute specifies the configuration_item, i.e., the 
product configuration that the associated design implements.

The design attribute specifies the product_definition or product_definition_formation 
that is a candidate for use in manufacturing actual units of a 
configuration_item.

*/
class Rb_Pdm_Configuration_Design extends Rb_Pdm_Abstract{

  protected $OBJECT_TABLE = 'pdm_configuration_design'; //(String)Table name
  protected $FIELDS_MAP_NAME = 'name'; //(String)Name of the field to define the name
  protected $FIELDS_MAP_DESC = 'description'; //(String)Name of the field to define the description

  //Fields:
  protected $FIELDS_MAP_ID = 'cd_id'; //(String)Name of the field of the primary key

  protected $core_props = array(
    'name'=>'', /*  */
    'description'=>'', /*  */
    'ci_id'=>null, /* configuration_item id for which a design is specified */
    'pv_id'=>null, /* product_definition_formation id that is a design for the associated configuration_item */
  ); //(array) contains the properties of the current object
  
  /* specifies the product_concept of which the configuration_item is a 
    member, i.e., of which it is a configuration */
  protected $_product = false; //(Rb_Pdm_Product_Version)


  /* specifies the configuration_item, i.e., the product configuration that 
     the associated design implements
  */
  protected $_configurationItem = false; //(Rb_Pdm_Configuration_Item)

  protected static $_registry = array(); //(array) registry of instances

//--------------------------------------------------------------------
function __construct( $id = 0 ){
  $this->dbranchbe =& Ranchbe::getDb();
  $this->error_stack  =& Ranchbe::getError();
  $this->_init($id);
}//End of method

//-------------------------------------------------------------------------
/*! \brief return a instance of object.
*   if none parameter return a special instance with no possibilies to change here properties
*   and wich can not be saved. This instance is just for use in static method where access to database is require,
*   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
*   If object "$id" dont exist return false.
*   If object "$id" has already been instantiated, then return this instance (singleton pattern).
* 
*/
public static function get( $id = -1 ){
  if($id < 0) $id = -1; //forced to value -1

  if($id == 0)
    return new self(0);

  if( !self::$_registry[$id] ){
    self::$_registry[$id] = new self($id);
  }

  return self::$_registry[$id];
}//End of method

//--------------------------------------------------------------------
/*!\brief record current instance in class registry.
*
*/
protected function _saveRegistry(){
    self::$_registry[$this->_id] =& $this;
} //End of method

//----------------------------------------------------------
/*! \brief
 */
public function setProduct(Rb_Pdm_Product_Version &$product){
  if($this->getId() < 0) return false;
  if($product->getId() < 1) return false; //the product must be recorded before
  $this->setProperty( 'pv_id', $product->getId() );
  $this->_product =& $product;
  return $this->_product;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getProduct(){
  if( $this->getId() < 0 ) return false;
  if( $this->_product ) return $this->_product;
  if( !$this->getProperty('pv_id') ){
    return false;
  }
  return $this->_product =& Rb_Pdm_Product_Version::get( $this->getProperty('pv_id') );
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function setConfigurationItem(Rb_Pdm_Configuration_Item &$ci){
  if($this->getId() < 0) return false;
  if($ci->getId() < 1) return false; //the ci must be recorded before
  $this->setProperty( 'ci_id', $ci->getId() );
  $this->_configurationItem =& $ci;
  return $this->_configurationItem;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getConfigurationItem(){
  if( $this->getId() < 0 ) return false;
  if( $this->_configurationItem ) return $this->_configurationItem;
  if( !$this->getProperty('ci_id') ){
    return false;
  }
  return $this->_configurationItem =& Rb_Pdm_Configuration_Item::get( $this->getProperty('ci_id') );
}//End of method

}//End of class


