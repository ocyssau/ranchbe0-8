<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
TABLE DEFINITION :
DROP TABLE `pdm_configuration_concept_seq`;

CREATE TABLE `pdm_configuration_concept` (
  `concept_id` int(11) NOT NULL,
  `number` varchar(128) NOT NULL,
  `name` varchar(128) NULL,
  `description` varchar(512) NULL,
  PRIMARY KEY  (`concept_id`),
  UNIQUE KEY `UNIQ_pdm_configuration_concept_1` (`number`),
  KEY `INDEX_pdm_configuration_concept_1` (`name`),
  KEY `INDEX_pdm_configuration_concept_2` (`description`)
);
*/

//require_once('core/pdm/abstract.php');
//require_once('core/pdm/configuration/context.php');

/*! \brief This class implement the product_concept entity of STEP PDM SHEMA.

 * A product_concept describes a class of similar products that an organization 
 * provides to its customers. It represents the idea of a product as identified 
 * by potential or actual customer requirements. Therefore, a product_concept may 
 * exist before the parts have been defined that implement it.
 * 
 * Depending on the kind of industry and products, a product_concept might be 
 * offered to the customers in one or many different configurations. If exactly 
 * one configuration is defined, the product_concept corresponds to a particular 
 * part design. If the product concept is offered in different configurations, 
 * each of these configurations is a member of the class of products defined by 
 * this product_concept.
*/
class Rb_Pdm_Configuration_Concept extends Rb_Pdm_Abstract{

  protected $OBJECT_TABLE = 'pdm_configuration_concept'; //(String)Table name

  //Fields:
  protected $FIELDS_MAP_ID = 'concept_id'; //(String)Name of the field of the primary key
  protected $FIELDS_MAP_NUM = 'number'; //(String)Name of the field
  protected $FIELDS_MAP_NAME = 'name'; //(String)Name of the field
  protected $FIELDS_MAP_DESC = 'description'; //(String)Name of the field

  protected $core_props = array(
    'number'=>'', /* identifier of the product_concept. assigned by the organization that provides the product */
    'name'=>'', /* */
    'description'=>'', /* */
    'context_id'=>0, /* market context id*/
  ); //(array) contains the properties of the current object
  
  /**
   *  The market for the products belonging to a product_concept is further 
   *   described in the market_context attribute. All product_concepts must have a 
   *   related product_concept_context, which specifies their market context. 
  */
  protected $_context = false; //(Rb_Pdm_Configuration_Context)

  protected static $_registry = array(); //(array) registry of instances

//--------------------------------------------------------------------
function __construct( $id = 0 ){
  $this->dbranchbe =& Ranchbe::getDb();
  $this->error_stack  =& Ranchbe::getError();
  $this->_init($id);
}//End of method

//-------------------------------------------------------------------------
/*! \brief return a instance of object.
*   if none parameter return a special instance with no possibilies to change here properties
*   and wich can not be saved. This instance is just for use in static method where access to database is require,
*   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
*   If object "$id" dont exist return false.
*   If object "$id" has already been instantiated, then return this instance (singleton pattern).
* 
*/
public static function get( $id = -1 ){
  if($id < 0) $id = -1; //forced to value -1

  if($id == 0)
    return new self(0);

  if( !self::$_registry[$id] ){
    self::$_registry[$id] = new self($id);
  }

  return self::$_registry[$id];
}//End of method

//--------------------------------------------------------------------
/*!\brief record current instance in class registry.
*
*/
protected function _saveRegistry(){
    self::$_registry[$this->_id] =& $this;
} //End of method


//----------------------------------------------------------
/*! \brief
 */
public function setContext(Rb_Pdm_Configuration_Context &$context){
  if($this->getId() < 0) return false;
  if($context->getId() < 1) return false; //the context must be recorded before
  $this->setProperty( 'context_id', $context->getId() );
  $this->_context =& $context;
  return $this->_context;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getContext(){
  if( $this->getId() < 0 ) return false;
  if( $this->_context ) return $this->_context;
  if( !$this->getProperty('context_id') ){
    return false;
  }
  return $this->_context =& Rb_Pdm_Configuration_Context::get( $this->getProperty('context_id') );
}//End of method

}//End of class

