<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
TABLE DEFINITION :
ALTER TABLE `pdm_context_application`
 ADD `name` VARCHAR( 128 ) NULL,
 ADD `market_segment_type` VARCHAR( 128 ) NULL,
 ADD INDEX `INDEX_pdm_context_productdefinition_2` ( `name` );
 ADD INDEX `INDEX_pdm_context_productdefinition_4` ( `market_segment_type` );
*/

//require_once('core/pdm/context/application.php');

/*! \brief This class implement the product_concept_context entity of STEP PDM SHEMA.

 * The product_concept_context entity is a subtype of 
 * application_context_element. It defines the market context in which a 
 * product_concept is defined and may include information characterizing the 
 * potential customers of the products of the associated product_concept. The 
 * application domain is identified by the associated application_context entity.
*/
class Rb_Pdm_Configuration_Context extends Rb_Pdm_Context_Application{

  protected $OBJECT_TABLE = 'pdm_context_application'; //(String)Table name

  //Fields:
  protected $FIELDS_MAP_ID = 'context_id'; //(String)Name of the field of the primary key
  protected $FIELDS_MAP_NAME = 'name'; //(String)Name of the field

  protected $core_props = array(
    'application'=>'', //name of the general application domain that defined the data.
    'name'=>'', /* */
    'market_segment_type'=>'', /* kind of consumer preferences associated with a product_concept. */
  ); //(array) contains the properties of the current object
  
  protected static $_registry = array(); //(array) registry of instances

//--------------------------------------------------------------------
function __construct( $id = 0 ){
  $this->dbranchbe =& Ranchbe::getDb();
  $this->error_stack  =& Ranchbe::getError();
  $this->_init($id);
}//End of method

//-------------------------------------------------------------------------
/*! \brief return a instance of object.
*   if none parameter return a special instance with no possibilies to change here properties
*   and wich can not be saved. This instance is just for use in static method where access to database is require,
*   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
*   If object "$id" dont exist return false.
*   If object "$id" has already been instantiated, then return this instance (singleton pattern).
* 
*/
public static function get( $id = -1 ){
  if($id < 0) $id = -1; //forced to value -1

  if($id == 0)
    return new self(0);

  if( !self::$_registry[$id] ){
    self::$_registry[$id] = new self($id);
  }

  return self::$_registry[$id];
}//End of method

//--------------------------------------------------------------------
/*!\brief record current instance in class registry.
*
*/
protected function _saveRegistry(){
    self::$_registry[$this->_id] =& $this;
} //End of method


}//End of class

