<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
TABLE DEFINITION :
CREATE TABLE `pdm_configuration_item` (
  `ci_id` int(11) NOT NULL,
  `number` varchar(128) NULL,
  `name` varchar(128) NULL,
  `description` varchar(512) NULL,
  `concept_id` int(11) NOT NULL,
  `purpose` varchar(512) NULL,
  PRIMARY KEY  (`ci_id`),
  UNIQUE KEY  `UNIQ_pdm_configuration_item_1` (`number`),
  INDEX `INDEX_pdm_configuration_item_1` (`name`),
  INDEX `INDEX_pdm_configuration_item_2` (`concept_id`),
  INDEX `INDEX_pdm_configuration_item_3` (`purpose`)
);
*/

//require_once('core/pdm/abstract.php');
//require_once('core/pdm/configuration/concept.php');

/*! \brief This class implement the configuration_item entity of STEP PDM SHEMA.

 * The configuration_item entity is a key concept to support explicit 
 * configuration management. It represents the identification of a particular 
 * configuration, i.e., variation of a product_concept. A configuration_item is 
 * defined with respect to the product concept, i.e., the class of similar 
 * products of which it is a member.
 * 
 * The configuration_item defines a manufacturable end item, or something that is 
 * conceived and expected as such. The association between a configuration_item 
 * and a corresponding part design is established using a configuration_design. 
 * The valid use of component parts for planned units of manufacturing of a 
 * particular configuration_item may be specified using configuration effectivity 
 * 
 * NOTE - Depending on the type of products, and the organization that defines 
 * and sells the products, a product_concept may directly correspond to a 
 * particular part design, which means that only one variation, i.e., one 
 * configuration_item exists for that particular product_concept.
 * 
 * NOTE - ISO 10303-44 allows the configuration_item to be a variation of a 
 * product concept, or of any of its discrete portions that is treated as a 
 * single unit in the configuration management process. According to this 
 * definition, it is possible to associate a configuration_item describing a 
 * particular configuration of, for example, an engine to a product concept 
 * describing a class of cars. This usage is not recommended.
*/
class Rb_Pdm_Configuration_Item extends Rb_Pdm_Abstract{

  protected $OBJECT_TABLE = 'pdm_configuration_item'; //(String)Table name
  protected $FIELDS_MAP_NUM = 'number'; //(String)Name of the field to define the number id
  protected $FIELDS_MAP_NAME = 'name'; //(String)Name of the field to define the name
  protected $FIELDS_MAP_DESC = 'description'; //(String)Name of the field to define the description

  //Fields:
  protected $FIELDS_MAP_ID = 'ci_id'; //(String)Name of the field of the primary key


  protected $core_props = array(
    'number'=>'', /* identifier that distinguishes the configuration_item */
    'name'=>'', /* word or common name used to refer to the configuration_item */
    'description'=>'', /* specifies additional information about the configuration_item */
    'purpose'=>'', /* descriptive label providing a reason to create the item_concept */
    'concept_id'=>null, /* specifies the product_concept id */
  ); //(array) contains the properties of the current object
  
  /* specifies the product_concept of which the configuration_item is a 
    member, i.e., of which it is a configuration */
  protected $_concept = false; //(rb_pdm_configuration_concept)

  protected static $_registry = array(); //(array) registry of instances

//--------------------------------------------------------------------
function __construct( $id = 0 ){
  $this->dbranchbe =& Ranchbe::getDb();
  $this->error_stack  =& Ranchbe::getError();
  $this->_init($id);
}//End of method

//-------------------------------------------------------------------------
/*! \brief return a instance of object.
*   if none parameter return a special instance with no possibilies to change here properties
*   and wich can not be saved. This instance is just for use in static method where access to database is require,
*   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
*   If object "$id" dont exist return false.
*   If object "$id" has already been instantiated, then return this instance (singleton pattern).
* 
*/
public static function get( $id = -1 ){
  if($id < 0) $id = -1; //forced to value -1

  if($id == 0)
    return new self(0);

  if( !self::$_registry[$id] ){
    self::$_registry[$id] = new self($id);
  }

  return self::$_registry[$id];
}//End of method

//--------------------------------------------------------------------
/*!\brief record current instance in class registry.
*
*/
protected function _saveRegistry(){
    self::$_registry[$this->_id] =& $this;
} //End of method

//----------------------------------------------------------
/*! \brief
 */
public function setConcept(rb_pdm_configuration_concept &$concept){
  if($this->getId() < 0) return false;
  if($concept->getId() < 1) return false; //the concept must be recorded before
  $this->setProperty( 'concept_id', $concept->getId() );
  $this->_concept =& $concept;
  return $this->_concept;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getConcept(){
  if( $this->getId() < 0 ) return false;
  if( $this->_concept ) return $this->_concept;
  if( !$this->getProperty('concept_id') ){
    return false;
  }
  return $this->_concept =& rb_pdm_configuration_concept::get( $this->getProperty('concept_id') );
}//End of method

}//End of class
