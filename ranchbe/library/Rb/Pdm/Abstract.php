<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


/**
 * Provide generic methods and properties for all classes of rb_pdm
 *
 * @author Administrateur
 *
 */
abstract class Rb_Pdm_Abstract extends Rb_Dao_Abstract{

	protected $OBJECT_TABLE = ''; //(String)Table name

	//Fields:
	protected $FIELDS_MAP_ID = 'id'; //(String)Name of the field of the primary key
	protected $FIELDS_MAP_NUM = 'number'; //(String)Name of the field to define the number id
	protected $FIELDS_MAP_NAME = 'name'; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC = 'description'; //(String)Name of the field to define the description

	protected $_id = 0; //(Integer) id of the current item
	protected $error_stack; //(Object)

	protected $core_props = array(
    'number'=>'',
    'name'=>'',
    'description'=>'',
	); //(array) contains the properties of the current object

	protected $_privileges = array(
                                'admin',
                                'get',
                                'create',
                                'edit',
                                'suppress',
	); //(array)default privileges name

	//-------------------------------------------------------------------------
	/*! \brief return a instance of object.
	 *   if none parameter return a special instance with no possibilies to change here properties
	 *   and wich can not be saved. This instance is just for use in static method where access to database is require,
	 *   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
	 *   If object "$id" dont exist return false.
	 *   If object "$id" has already been instantiated, then return this instance (singleton pattern).
	 *
	 */
	abstract public static function get( $id = -1 );

	//-------------------------------------------------------------------------
	/*! \brief init current instance, populate properties from database,
	 *       init access to database parameters.
	 * Return array.
	 *
	 */
	protected function _init($id){
		if( $id > 0 ){
			$this->_id = (int) $id;
			$this->core_props = $this->getBasicInfos($this->_id);
			if( !$this->core_props ) {
				trigger_error('this object dont exist');
				die;
			}
			//$this->_initAcl();
		}else if( $id < 0 ){
			$this->_id = -1;
			//$this->_initAcl();
		}else{
			$this->_id = 0;
			//$this->_initAcl();
		}
		return true;
	} //End of method

	//----------------------------------------------------------
	/*! \brief Get all properties of the current item
	 *  Return array
	 */
	public function getProperties(){
		if(!isset($this->core_props)) return false;
		else return $this->core_props;
	}//End of method

	//----------------------------------------------------------
	public function getProperty($property_name){
		switch($property_name){
			case 'name':
				return $this->core_props[$this->FIELDS_MAP_NAME];
				break;
			case 'number':
				return $this->core_props[$this->FIELDS_MAP_NUM];
				break;
			case 'description':
				return $this->core_props[$this->FIELDS_MAP_DESC];
				break;
			default:
				if( array_key_exists($property_name, $this->core_props) )
				return $this->core_props[$property_name];
				break;
		} //End of switch
	}

	//----------------------------------------------------------
	public function setProperty($property_name, $property_value){
		if($this->getId() < 0) return false; //Can not change property on object -1
		switch($property_name){
			case 'name':
				$this->core_props[$this->FIELDS_MAP_NAME] = $property_value;
				break;
			case 'number':
				if( !empty($property_value) ){
					$this->core_props[$this->FIELDS_MAP_NUM] = $property_value;
				}
				break;
			case 'description':
				$this->core_props[$this->FIELDS_MAP_DESC] = $property_value;
				break;
			case 'id':
			case $this->FIELDS_MAP_ID: //prevent modification of primary key
				break;
			default:
				$this->core_props[$property_name] = $property_value;
				break;
		} //End of switch
		return $this;
	}

	//----------------------------------------------------------
	/*! \brief Return id of current item
	 * Return false or integer
	 */
	public function getId(){
		return $this->_id;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Return id of current item
	 * Return false or integer
	 */
	public function getName(){
		return $this->getProperty('name');
	}//End of method

	//----------------------------------------------------------
	/*! \brief Return id of current item
	 * Return false or integer
	 */
	public function getNumber(){
		return $this->getProperty('number');
	}//End of method

	//----------------------------------------------------------
	/*! \brief
	 */
	public function getAll( $params=array() ){
		return $this->getAllBasic($params);
	}//End of method

	//----------------------------------------------------------------------------
	/*! \brief Returns the string identifier from specified $id
	 * \param $id(string)
	 * \return string
	 */
	public static function getStaticResourceId($id){
		return $id;
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief Record properties of current instance in database.
	 */
	public function save($notify=false, $history=false){
		if($notify)
		$this->_initObservers();
		if($this->_id < 0){
			return false;
		}else
		if($this->_id == 0){
			if($notify){
				$this->notify_all($this->_preCreateEventName, $this);
				if( $this->stopIt ) return false;//use by observers to control execution
			}
			$this->_create();
			if($this->_id){
				if($notify){
					$this->notify_all($this->_postCreateEventName, $this);
					if( $this->stopIt ) return false;//use by observers to control execution
				}
				if($history){
					$this->writeHistory('_create');
				}
				return $this;
			}else return false;
		}else
		if($this->_id > 0){
			if($notify){
				$this->notify_all($this->_preUpdateEventName, $this);
				if( $this->stopIt ) return false;//use by observers to control execution
			}
			if( $this->_update() ){
				if($notify){
					$this->notify_all($this->_postUpdateEventName, $this);
					if( $this->stopIt ) return false;//use by observers to control execution
				}
				if($history){
					$this->writeHistory('_update');
				}
				return $this;
			}else return false;
		}
	} //End of method

	//-------------------------------------------------------------------------
	/*!\brief write modification in database.
	 * Return true or false.
	 *
	 */
	protected function _update(){
		if( $this->_id < 1 ){
			$this->error_stack->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id);
			return false;
		}
		return $this->_basicUpdate( $this->getProperties() , $this->_id);
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief create a new database record for object.
	 *   Return the id if no errors, else return FALSE
	 *
	 */
	//abstract protected function _create();

	//--------------------------------------------------------------------
	/*!\brief record current instance in class registry.
	 *
	 */
	/*Example of implementation
	 protected function _saveRegistry(){
	 self::$_registry[$this->_id] =& $this;
	 } //End of method
	 */
	abstract protected function _saveRegistry();

	//--------------------------------------------------------------------
	/*!\brief create a new database record for object.
	 *   Return the id if no errors, else return FALSE
	 *
	 */
	protected function _create(){
		if( $this->_id !== 0 ){
			$this->error_stack->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id);
			return false;
		}

		//Create a basic object
		if( $this->_id = $this->_basicCreate( $this->getProperties() ) ){
			$this->_saveRegistry();
			$this->isSaved = true;
		}else{
			$this->error_stack->push(Rb_Error::ERROR, array( 'element'=>$this->getName() ),
			tra('cant create %element%') );
			return $this->_id = 0;
		}
		return $this->_id;

	}//End of method

}//End of class
