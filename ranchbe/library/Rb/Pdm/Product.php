<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
 TABLE DEFINITION :
 DROP TABLE `pdm_product_seq`;
 DROP TABLE `pdm_product`;

 CREATE TABLE `pdm_product` (
 `p_id` int(11) NOT NULL,
 `p_number` varchar(128) NOT NULL,
 `p_name` varchar(128) NULL,
 `p_description` varchar(512) NULL,
 PRIMARY KEY  (`p_id`),
 UNIQUE KEY `pdm_product_uniq1` (`p_number`),
 KEY `INDEX_pdm_product_1` (`p_name`),
 KEY `INDEX_pdm_product_2` (`p_description`)
 );

 `instance_id` int(11) NOT NULL,
 `version_id` int(11) NOT NULL default '1',
 `version_description` varchar(512) NULL,
 `document_id` int(11) NOT NULL default '0',
 `space_name` varchar(16) NULL,
 `quantity` int(11) NOT NULL,
 `unit` enum(),
 `axis1x` decimal(11) NOT NULL default '0',
 `axis1y` decimal(11) NOT NULL default '0',
 `axis1z` decimal(11) NOT NULL default '0',
 `axis2x` decimal(11) NOT NULL default '0',
 `axis2y` decimal(11) NOT NULL default '0',
 `axis2z` decimal(11) NOT NULL default '0',
 `axis3x` decimal(11) NOT NULL default '0',
 `axis3y` decimal(11) NOT NULL default '0',
 `axis3z` decimal(11) NOT NULL default '0',
 `originx` decimal(11) NOT NULL default '0',
 `originy` decimal(11) NOT NULL default '0',
 `originz` decimal(11) NOT NULL default '0',
 `access_code` int(2) NOT NULL default '0',

 */

//require_once('core/pdm/product/abstract.php');
////require_once('core/pdm/context/product_context.php');
////require_once('core/pdm/validity.php');

//-------------------------------------------------------------------------

/*! \brief This class implement the product entity of STEP PDM SHEMA.

* The product entity represents the product master base information. This
* collects all information that is common among the different versions and
* of the product. The product number is strictly an identifier. It should not
* used as a 'smart string' with some parse able internal coding scheme, e.g.,
* identify version or classification information.
*
* The product number identifier must be unique within the scope of the business
* of the information exchange. This is typically not a problem when the product
* is only used within a single company. If the data is being assembled for an
* use, the identification must be interpreted as unique within that broader
* Processors may need to evaluate more than one string (i.e., product.id) to
* unique identification of a part; there may be a combination of parameters
* make part identification unique. The associated organization entity with the
* 'id owner' can be used to derive a uniqueness parameter if the product.id
* is not unique within the domain of the business arrangement of the exchange.
*
*/
class Rb_Pdm_Product extends Rb_Pdm_Product_Abstract{

	protected $OBJECT_TABLE = 'pdm_product'; //(String)Table where are stored items definitions

	//Fields:
	protected $FIELDS_MAP_ID = 'p_id'; //(String)Name of the field of the primary key
	protected $FIELDS_MAP_NUM = 'p_number'; //(String)Name of the field to define the number id
	protected $FIELDS_MAP_NAME = 'p_name'; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC = 'p_description'; //(String)Name of the field to define the description

	protected $core_props = array(
    'p_number'=>'',
    'p_name'=>'',
    'p_description'=>'',
	); //(array) contains the properties of the current object

	protected $_versions = array(); //(array) registry of versions

	protected static $_registry = array(); //(array) registry of instances

	//--------------------------------------------------------------------
	function __construct( $item_id = 0 ){
		$this->dbranchbe =& Ranchbe::getDb();
		$this->error_stack  =& Ranchbe::getError();
		$this->_init($item_id);
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief return a instance of object.
	 *   if none parameter return a special instance with no possibilies to change here properties
	 *   and wich can not be saved. This instance is just for use in static method where access to database is require,
	 *   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
	 *   If object "$id" dont exist return false.
	 *   If object "$id" has already been instantiated, then return this instance (singleton pattern).
	 *
	 */
	public static function get( $id = -1 ){
		if($id < 0) $id = -1; //forced to value -1

		if($id == 0){
			return new self(0);
		}

		if( !self::$_registry[$id] ){
			self::$_registry[$id] = new self($id);
		}

		return self::$_registry[$id];
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief record current instance in class registry.
	 *
	 */
	protected function _saveRegistry(){
		self::$_registry[$this->_id] =& $this;
	} //End of method

	//----------------------------------------------------------
	/*! \brief
	 */
	public function save(){
		if( !$this->getNumber() ){
			$this->setProperty('p_number', '');
		}
		return parent::save();
	}//End of method

	//----------------------------------------------------------
	/*! \brief return object Rb_Pdm_Product_Version
	 */
	public function getVersion($version_id){
		if( !$this->_versions[$version_id] ){
			$vid = Rb_Pdm_Product_Version::get(-1)->ifExist( $this->getId(), $version_id);
			$this->_versions[$version_id] = Rb_Pdm_Product_Version::get($vid);
		}
		return $this->_versions[$version_id];
	}//End of method

	//----------------------------------------------------------
	/*! \brief
	 */
	public function setVersion(Rb_Pdm_Product_Version $version){
		if($this->getId() < 0) return false;
		return $this->_versions[$version->getId()] = $version;
	}//End of method

}//End of class
