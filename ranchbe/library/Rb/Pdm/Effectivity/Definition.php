<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/pdm/usage/abstract.php');
//require_once('core/pdm/effectivity.php');

/*! \brief This class implement the product_definition_effectivity entity of STEP PDM SHEMA.

The product_definition_effectivity entity is a subtype of effectivity. It 
applies to a particular occurrence of a component or sub-assembly part as used 
within an assembly. It is the identification of a valid use of a particular 
product_definition in the context of its participation in a given 
product_definition_usage. The effectivity applies to a product_definition that 
is referenced as the related_product_definition by the 
product_definition_relationship associated to the 
product_definition_effectivity through the usage attribute.

 */
class Rb_Pdm_Effectivity_Definition extends Rb_Pdm_Effectivity{

  protected $core_props = array(
    'number'=>'',
    'name'=>'',
    'description'=>'',
    'usage_id'=>null,
    'usage_type'=>null,
  ); //(array) contains the properties of the current object

  /**
   * reference to the associated product_definition_relationship
   *
   * @var Rb_Pdm_Product_relationship
   */
  protected $_usage = false; //(Rb_Pdm_Usage_Abstract)

  protected static $_registry = array(); //(array) registry of instances

//-------------------------------------------------------------------------
/*! \brief return a instance of object.
*   if none parameter return a special instance with no possibilies to change here properties
*   and wich can not be saved. This instance is just for use in static method where access to database is require,
*   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
*   If object "$id" dont exist return false.
*   If object "$id" has already been instantiated, then return this instance (singleton pattern).
* 
*/
public static function get( $id = -1 ){
  if($id < 0) $id = -1; //forced to value -1

  if($id == 0)
    return new self(0);

  if( !self::$_registry[$id] ){
    self::$_registry[$id] = new self($id);
  }

  return self::$_registry[$id];
}//End of method

//--------------------------------------------------------------------
/*!\brief record current instance in class registry.
*
*/
protected function _saveRegistry(){
    self::$_registry[$this->_id] =& $this;
} //End of method

//----------------------------------------------------------
/*! \brief
 */
public function setUsage(Rb_Pdm_Usage_Abstract &$usage){
  if($this->getId() < 0) return false;
  if($usage->getId() < 1) return false; //the product must be recorded before
  $this->setProperty( 'usage_id', $usage->getId() );
  $this->setProperty( 'usage_type', $usage->getProperty('type_id') );
  $this->_usage =& $usage;
  return $this->_usage;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getUsage(){
  if( $this->getId() < 0 ) return false;
  if( $this->_usage ) return $this->_usage;
  if( !$this->getProperty('usage_id') ){
    return false;
  }
  return $this->_usage =& Rb_Pdm_Usage_Abstract::get( $this->getProperty('usage_id'),
                                                      $this->getProperty('usage_type') );
}//End of method

}//End of class


