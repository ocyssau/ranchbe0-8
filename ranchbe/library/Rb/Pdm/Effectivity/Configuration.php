<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/pdm/configuration/design.php');
//require_once('core/pdm/effectivity/definition.php');

/*! \brief This class implement the configuration_effectivity entity of STEP PDM SHEMA.

  The entity configuration_effectivity is a subtype of 
  product_definition_effectivity that contains information about the planned 
  usage of components in a product configuration. It defines the valid use of a 
  particular product_definition occurrence at a certain position in the assembly 
  structure for a specified product configuration.
  
  The configuration_effectivity entity allows the association of the appropriate 
  versions of constituent parts intended to be used at the defined position in 
  the assembly structure to build a configuration_item. In the context of the 
  PDM Schema, the configuration_effectivity always relates to a particular unit 
  of manufacture of the end items of the given configuration_item. The three 
  ways to instantiate configuration_effectivity are:

    * serial_numbered_effectivity, where the configuration_effectivity defines the 
        valid use of constituent part occurrences for a serial number range of 
        instances of a product configuration to be manufactured;
    * dated_effectivity, where the configuration_effectivity defines the valid use 
        of constituent part occurrences for a time range based on dates when instances 
        of the product configuration are manufactured;
    * lot_effectivity, where the configuration_effectivity defines the valid use 
        of constituent part occurrences for a given lot of instances of a product 
        configuration to be manufactured.


    The particular usage of the constituent part the effectivity applies to is 
    defined as product_definition_usage referenced by the usage attribute. The 
    product_definition_usage should be instantiated as an assembly_component_usage 
    or one of its subtypes (see Product concept). The attribute 
    related_product_definition specifies the definition of a constituent part, and 
    the attribute relating_ product_definition specifies the definition of the 
    assembly (or sub-assembly) in which the constituent may be used. This assembly 
    definition is part of the design that implements the configuration_item for 
    which the configuration_effectivity applies, i.e., it either is a part 
    definition of the product_definition_formation defining the ('complete') 
    design for the configuration_item, or it is an occurrence in the product 
    structure of that complete design, i.e., is related to that in a tree of 
    assembly_component_usage instances.

*/
class Rb_Pdm_Effectivity_Configuration extends Rb_Pdm_Effectivity_Definition{

  protected $core_props = array(
    'number'=>'',
    'name'=>'',
    'description'=>'',
    'usage_id'=>null, //usage id
    'usage_type'=>null, //usage type id : 1,2,3 for classes Rb_Pdm_Usage, Rb_Pdm_Usage_Quantified, Rb_Pdm_Usage_Promissory
    'cd_id'=>null, //configuration design id
  ); //(array) contains the properties of the current object


  /*
    * The configuration attribute identifies the configuration_design that 
        specifies the associated configuration_item for which the 
        configuration_effectivity applies, and the corresponding 
        product_definition_formation which defines the design that implements the 
        configuration_item and thus specifies the upper most node ('entry point') in 
        its product structure.
  */
  /*
   * reference to the configuration_design specifying the configuration_item
   * identifies the context in which the effectivity applies.
   */
  protected $_design = false; //(Rb_Pdm_Configuration_Design)

  /* reference to the Rb_Pdm_Effectivity subtype dated, lot or snumbered
   *
   */
  protected $_effectivity = false; //(Rb_Pdm_Effectivity)

  protected static $_registry = array(); //(array) registry of instances

//-------------------------------------------------------------------------
/*! \brief return a instance of object.
*   if none parameter return a special instance with no possibilies to change here properties
*   and wich can not be saved. This instance is just for use in static method where access to database is require,
*   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
*   If object "$id" dont exist return false.
*   If object "$id" has already been instantiated, then return this instance (singleton pattern).
* 
*/
public static function get( $id = -1 ){
  if($id < 0) $id = -1; //forced to value -1

  if($id == 0)
    return new self(0);

  if( !self::$_registry[$id] ){
    self::$_registry[$id] = new self($id);
  }

  return self::$_registry[$id];
}//End of method

//--------------------------------------------------------------------
/*!\brief record current instance in class registry.
*
*/
protected function _saveRegistry(){
    self::$_registry[$this->_id] =& $this;
} //End of method

//----------------------------------------------------------
/*! \brief
 */
public function setDesign(Rb_Pdm_Configuration_Design &$design){
  if($this->getId() < 0) return false;
  if($design->getId() < 1) return false; //must be recorded before
  $this->setProperty( 'cd_id', $design->getId() );
  $this->_design =& $design;
  return $this->_design;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getDesign(){
  if( $this->getId() < 0 ) return false;
  if( $this->_design ) return $this->_design;
  if( !$this->getProperty('cd_id') ){
    return false;
  }
  return $this->_design =& Rb_Pdm_Configuration_Design::get( $this->getProperty('cd_id') );
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getEffectivity($type = 1){
  if( $this->getId() < 0 ) return false;
  switch($type){
    case(1):
      return $this->_effectivity = new Rb_Pdm_Effectivity_Dated($this);
      break;
    case(2):
      return $this->_effectivity = Rb_Pdm_Effectivity_Lot::get( $this->getId() );
      break;
    case(3):
      return $this->_effectivity = Rb_Pdm_Effectivity_Snumbered::get( $this->getId() );
      break;
  }
  return false;
}//End of method

}//End of class
