<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

////require_once('core/pdm/effectivity.php');

/*! \brief This class implement the  entity of STEP PDM SHEMA.

 */
//class Rb_Pdm_Effectivity_Dated extends Rb_Pdm_Effectivity{
class Rb_Pdm_Effectivity_Snumbered{

  protected $core_props = array(
    'snum_start_id'=>null,
    'snum_end_id'=>null,
  ); //(array) contains the properties of the current object

  protected $_effectivity = false; //(Rb_Pdm_Effectivity)

//--------------------------------------------------------------------
function __construct( Rb_Pdm_Effectivity &$effectivity ){
  $this->_effectivity =& $effectivity;
  $this->_id = $effectivity->getId();
  $this->setProperty('snum_start_id', $effectivity->getProperty('snum_start_id') );
  $this->setProperty('snum_end_id', $effectivity->getProperty('snum_end_id') );
}//End of method

//--------------------------------------------------------------------
/*!\brief
*
*/
public function setProperty($name, $value){
  switch($name){
    case('snum_start_id'):
    case('snum_end_id'):
      $this->core_props[$name] = $value;
      $this->_effectivity->setProperty($name, $value);
      return $this;
      break;
  }
  trigger_error('you can not set this property by this way, 
                                  set it from associated Rb_Pdm_Effectivity');
  return false;
} //End of method

//--------------------------------------------------------------------
/*!\brief
*
*/
public function getProperty($name){
  return $this->core_props[$name];
} //End of method

//--------------------------------------------------------------------
/*!\brief
*
*/
public function getProperties(){
  return $this->core_props;
} //End of method

//--------------------------------------------------------------------
/*!\brief
*
*/
public function getId(){
  return $this->_id;
} //End of method

//--------------------------------------------------------------------
/*!\brief
*
*/
//public function setStartId($id){
//} //End of method

//--------------------------------------------------------------------
/*!\brief
*
*/
//public function setEndId($id){
//} //End of method

}//End of class
