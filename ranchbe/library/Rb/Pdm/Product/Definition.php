<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
TABLE DEFINITION :
DROP TABLE `pdm_product_definition_seq`;
DROP TABLE `pdm_product_definition`;

CREATE TABLE `pdm_product_definition` (
  `pd_id` int(11) NOT NULL,
  `ct_id` int(11) NOT NULL,
  `pv_id` int(11) NOT NULL,
  `pd_description` varchar(512) NULL,
  PRIMARY KEY  (`pd_id`),
  UNIQUE KEY `UNIQ_pdm_product_definition_1` (`pv_id`,`ct_id`),
  INDEX(`pd_description`)
);
*/

//require_once('core/pdm/product/abstract.php');
//require_once('core/pdm/product/version.php');

/*! \brief This class implement the product_definition entity of STEP PDM SHEMA.

 * The product_definition entity represents the identification of a particular
 * on a version of the product base identification relevant for the requirements
 * particular life-cycle stages and application domains. View may be based on
 * domain and/or life-cycle stage (e.g., design, manufacturing). A view collects
 * data for a specific task. It is possible to have many product_definition
 * for a part/version combination.
 *
 * The product_definition entity enables the establishment of many important
 * Product_definition is the central element to link product information to
 * e.g., assembly structure, properties (including shape), and external
 * of the product via documents.
 *
 * The use of product_definition entities is not strictly required by rules in
 * PDM Schema, but it is strongly recommended. All product_definition_formation
 * should always have at least one associated product_definition, except in the
 * of supplied product identification and version history information.
 *
 * @access public
 * @author olivier cyssau, <ocyssau@free.fr>
 * @package pdm/product
 */
class Rb_Pdm_Product_Definition extends Rb_Pdm_Product_Abstract{

  protected $OBJECT_TABLE = 'pdm_product_definition'; //(String)Table name

  //Fields:
  protected $FIELDS_MAP_ID = 'pd_id'; //(String)Name of the field of the primary key
  protected $FIELDS_MAP_DESC = 'pd_description'; //(String)Name of the field to define the description

  protected $core_props = array(
    'ct_id'=>0, /* which view of the product the particular represents*/
    'pv_id'=>0, /* links to the Rb_Pdm_Product_Version */
    'pd_description'=>'',
  ); //(array) contains the properties of the current object
  
  protected $_product = false; //(Rb_Pdm_Product_Version)
  protected $_context = false; //(Rb_Pdm_Context_Productdefinition)

  protected static $_registry = array(); //(array) registry of instances

//--------------------------------------------------------------------
function __construct( $id = 0 ){
  $this->dbranchbe =& Ranchbe::getDb();
  $this->error_stack  =& Ranchbe::getError();
  $this->_init($id);
}//End of method

//-------------------------------------------------------------------------
/*! \brief return a instance of object.
*   if none parameter return a special instance with no possibilies to change here properties
*   and wich can not be saved. This instance is just for use in static method where access to database is require,
*   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
*   If object "$id" dont exist return false.
*   If object "$id" has already been instantiated, then return this instance (singleton pattern).
* 
*/
public static function get( $id = -1 ){
  if($id < 0) $id = -1; //forced to value -1

  if($id == 0)
    return new self(0);

  if( !self::$_registry[$id] ){
    self::$_registry[$id] = new self($id);
  }

  return self::$_registry[$id];
}//End of method

//-------------------------------------------------------------------------
/*! \brief return a instance of object from product version id and his context id
*   if thi object dont exists, return false
* 
*/
public static function getFromVersionContext( $pv_id, $context_id ){
  $p['exact_find']['ct_id'] = $context_id;
  $p['exact_find']['pv_id'] = $pv_id;
  $p['select'] = array('pd_id');
  $pd_id = self::get()->_dbget(null , $p , $get='one');
  if($pd_id)
    return self::get($pd_id);
  else
    return false;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function setProductVersion(Rb_Pdm_Product_Version &$product){
  if($this->getId() < 0) return false;
  if($product->getId() < 1) return false; //the product must be recorded before
  $this->setProperty( 'pv_id', $product->getId() );
  $this->_product =& $product;
  return $this->_product;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getProductVersion(){
  if( $this->getId() < 0 ) return false;
  if( $this->_product ) return $this->_product;
  if( !$this->getProperty('pv_id') ){
    return false;
  }
  return $this->_product =& Rb_Pdm_Product_Version::get( $this->getProperty('pv_id') );
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function setContext(Rb_Pdm_Context_Productdefinition &$context){
  if($this->getId() < 0) return false;
  if($context->getId() < 1) return false; //the context must be recorded before
  $this->setProperty( 'ct_id', $context->getId() );
  $this->_context =& $context;
  return $this->_context;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getContext(){
  if( $this->getId() < 0 ) return false;
  if( $this->_context ) return $this->_context;
  if( !$this->getProperty('ct_id') ){
    return false;
  }
  return $this->_context =& Rb_Pdm_Context_Productdefinition::get( $this->getProperty('ct_id') );
}//End of method

//--------------------------------------------------------------------
/*!\brief record current instance in class registry.
*
*/
protected function _saveRegistry(){
    self::$_registry[$this->_id] =& $this;
} //End of method

}//End of class
