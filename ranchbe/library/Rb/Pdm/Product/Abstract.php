<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


//require_once('core/pdm/abstract.php');

/*! \brief In step, product generalize part and document concept. This class implement
the product concept of STEP PDM SHEMA. In RanchBE, the product entity is partially implemented
by rb_pdm_item and by rb_document

see detail description extract from http://www.wikistep.org/index.php/PDM-UG:_Abstract :

A product in STEP represents the concept of a general managed item within a PDM system.
In the STEP PDM Schema, the general product concept may be interpreted as either 
  a Part (see Part Identification) or a Document (see Document Identification).
  In this way, parts and documents are managed in a consistent and parallel fashion.
Alias Identification describes a mechanism to associate product data with an 
  additional identifier (alias).
Also central to the functionality of many PDM systems is identification of external 
  files (both digital and physical), their relationship to managed documents 
  (see Relationship Between Documents and Constituent Files), and how they can be 
  associated with core product identification (see Document and File Association with Product Data).
  The external file reference mechanism in the STEP PDM Schema is described in 
  External Files of this document.
Classification of products is important in a PDM system for information 
  classification and retrieval. It also supports basic type distinction between 
  products that are parts and those that are documents. In the PDM Schema, 
  product classification is used consistently for parts (see Specific Part Type Classification)
  and documents (see Specific Document Type Classification).
Product properties are integrally related to the definition of an identified product,
  and so are naturally also included in the central core PDM information. 
  Part Properties and Document and File Properties discuss properties associated 
  with an identified product, interpreted, respectively, as either a part or a document.
Various general authorization and organizational data that are related to core 
  product identification play an important role in PDM systems. Section 13 Authorization 
  of this document describes the various organizational and management constructs 
  that support product authorization in the STEP PDM Schema.
Product structures are the principle relationships that define assemblies and 
  product configurations. Part Structure and Relationships details part structures 
  in the STEP PDM Schema; Document and File Relationships describes document 
  structures. Configuration identification and effectivity information related 
  to these structures is detailed in Configuration and Effectivity Information.
Engineering Change and Work Management describes structures to manage the documentation 
  of requests and corresponding orders for engineering action in support of the 
  change management process. Also included are representations for contract and 
  project identification.
Finally, Measure and units summarizes recommendations related to measures and units.
*/
abstract class Rb_Pdm_Product_Abstract extends Rb_Pdm_Abstract{
  public function suppress(){
    if($this->_id < 1) return trigger_error('object is not initialized');
    return $this->_basicSuppress($this->_id);
  }
}//End of class
