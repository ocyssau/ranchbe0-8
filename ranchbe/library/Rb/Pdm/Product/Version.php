<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
TABLE DEFINITION :
DROP TABLE `pdm_product_version`;
DROP TABLE `pdm_product_version_seq`;

CREATE TABLE `pdm_product_version` (
  `pv_id` int(11) NOT NULL,
  `of_product_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL,
  `pv_description` varchar(512) NULL,
  `document_id` int(11) NULL,
  `document_space` varchar(16) NULL,
  PRIMARY KEY  (`pv_id`),
  UNIQUE KEY `pdm_product_version_uniq1` (`of_product_id`,`version_id`),
  KEY `INDEX_pdm_product_version_1` (`of_product_id`),
  KEY `INDEX_pdm_product_version_2` (`version_id`),
  KEY `INDEX_pdm_product_version_3` (`pv_description`)
);
*/

//require_once('core/pdm/product/abstract.php');
//require_once('core/pdm/product.php');
//require_once('core/document.php');

//-------------------------------------------------------------------------

/*! \brief This class implement the product_definition_formation entity of STEP PDM SHEMA.

 * The product_definition_formation entity represents the identification of a
 * version of the base product identification. A particular
 * is always related to exactly one product.
 *
 * Each instance of product is required to have an associated instance of
 * A single product entity may have more than one associated
 * The set of these product_definition_formation entities represents the
 * history of the product.
 *
 *
 */

class Rb_Pdm_Product_Version extends Rb_Pdm_Product_Abstract{

  protected $OBJECT_TABLE = 'pdm_product_version'; //(String)Table name

  //Fields:
  protected $FIELDS_MAP_ID = 'pv_id'; //(String)Name of the field of the primary key
  protected $FIELDS_MAP_DESC = 'pv_description'; //(String)Name of the field to define the description

  protected $_id = 0; //(Integer) id of the current item

  protected $core_props = array(
    'pv_id'=>0, //part version identification
    'document_id'=>0,
    'of_product_id'=>0,
    'pv_description'=>'',
  ); //(array) contains the properties of the current object
  
  protected $_product = false; //(Rb_Pdm_Product)
  protected $_document = false; //(Rb_Document)

  protected static $_registry = array(); //(array) registry of instances

//--------------------------------------------------------------------
function __construct( $id = 0 ){
  $this->dbranchbe =& Ranchbe::getDb();
  $this->error_stack  =& Ranchbe::getError();
  $this->_init($id);
}//End of method

//-------------------------------------------------------------------------
/*! \brief return a instance of object.
*   if none parameter return a special instance with no possibilies to change here properties
*   and wich can not be saved. This instance is just for use in static method where access to database is require,
*   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
*   If object "$id" dont exist return false.
*   If object "$id" has already been instantiated, then return this instance (singleton pattern).
* 
*/
public static function get( $id = -1 ){
  if($id < 0) $id = -1; //forced to value -1

  if($id == 0)
    return new self(0);

  if( !self::$_registry[$id] ){
    self::$_registry[$id] = new self($id);
  }

  return self::$_registry[$id];
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function setDocument(Rb_Document &$document){
  if($this->getId() < 0 ) return false;
  $this->setProperty( 'document_id', $document->getId() );
  $this->setProperty( 'document_space', $document->getSpaceName() );
  return $this->_document =& $document;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getDocument(){
  if( $this->getId() < 0 ) return false;
  if( $this->_document ) return $this->document;
  if( !$this->getProperty('document_id') ){
    return false;
  }

  return $this->_document =& Rb_Document::get( $this->getProperty('document_space'),
                                               $this->getProperty('document_id')
                                             );
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function setProduct(Rb_Pdm_Product &$product){
  if($this->getId() < 0) return false;
  if($product->getId() < 1) return false; //the product must be recorded before
  $this->setProperty( 'of_product_id', $product->getId() );
  $this->_product =& $product;
  $this->_product->setVersion($this); //bi-directionnal link
  return $this->_product;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getProduct(){
  if( $this->getId() < 0 ) return false;
  if( $this->_product ) return $this->_product;
  if( !$this->getProperty('of_product_id') ){
    return false;
  }
  return $this->_product =& Rb_Pdm_Product::get( $this->getProperty('of_product_id') );
}//End of method

//--------------------------------------------------------
/*!\brief
 * This method return the id of the product version from of_product_id and version_id.
 * 
 \param $product_id(string)
 \param $version_id(int)
*/
public function ifExist( $product_id , $version_id ){
  $query =  "SELECT $this->FIELDS_MAP_ID FROM $this->OBJECT_TABLE
            WHERE of_product_id = '$product_id'
            AND version_id = $version_id";
  $one = $this->dbranchbe->GetOne($query);
  return $one;
}//End of method

//--------------------------------------------------------------------
/*!\brief record current instance in class registry.
*
*/
protected function _saveRegistry(){
    self::$_registry[$this->_id] =& $this;
} //End of method

}//End of class
