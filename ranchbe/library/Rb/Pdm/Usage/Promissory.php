<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/pdm/usage/abstract.php');

/**
 * This page belongs to the PDM Usage Guide.
 *
 * The STEP PDM Schema supports promissory usage relationships between
 * and some higher-level assembly that is not the immediate parent in the
 * This may be important to relate an outsourced component to a top-level
 * definition related to a configuration and product_concept. A supplier may
 * require the configuration and end item identification that is associated with
 * component of interest, and may not need to know the detailed assembly
 * in between. The promissory component usage may be useful during early design
 * to create preliminary BOMs for prototyping.
 *
 *
 * This entity is a subtype of assembly_component_usage. It represents the usage
 * of a component within a higher-level assembly that is not the immediate
 * in the case where the detailed assembly structure in between the component
 * the higher-level assembly is not represented. A promissory_usage_occurrence
 * the intention to use the constituent in an assembly. It may also be used when
 * product structure is not completely defined.
 *
 * @access public
 * @author olivier cyssau, <ocyssau@free.fr>
 * @package pdm/usage
 */
abstract class Rb_Pdm_Usage_Promissory extends Rb_Pdm_Usage_Abstract
{
  protected static $_registry = array(); //(array) registry of instances


//--------------------------------------------------------------------
function __construct( $id = 0 ){
  $this->dbranchbe =& Ranchbe::getDb();
  $this->error_stack  =& Ranchbe::getError();
  $this->_init($id);
}//End of method

//-------------------------------------------------------------------------
/*! \brief return a instance of object.
*   if none parameter return a special instance with no possibilies to change here properties
*   and wich can not be saved. This instance is just for use in static method where access to database is require,
*   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
*   If object "$id" dont exist return false.
*   If object "$id" has already been instantiated, then return this instance (singleton pattern).
* 
*/
public static function get( $id = -1 ){
  if($id < 0) $id = -1; //forced to value -1

  if($id == 0)
    return new self(0);

  if( !self::$_registry[$id] ){
    self::$_registry[$id] = new self($id);
  }

  return self::$_registry[$id];
}//End of method

//--------------------------------------------------------------------
/*!\brief record current instance in class registry.
*
*/
protected function _saveRegistry(){
  self::$_registry[$this->_id] =& $this;
} //End of method

}//End of class

