<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
TABLE DEFINITION :

DROP TABLE `pdm_usage_seq`;
DROP TABLE `pdm_usage`;

CREATE TABLE `pdm_usage` (
  `usg_id` int(11) NOT NULL,
  `usg_number` varchar(128) NOT NULL,
  `usg_name` varchar(128) NULL,
  `usg_description` varchar(512) NULL,
  `usg_type_id` int(1) NOT NULL default 1,
  `parent_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  PRIMARY KEY  (`usg_id`),
  KEY(`usg_number`),
  INDEX(`usg_name`),
  INDEX(`child_id`),
  INDEX(`parent_id`)
);
*/

//require_once('core/pdm/abstract.php');

//-------------------------------------------------------------------------

/*! \brief This class implement the assembly_component_usage entity of STEP PDM SHEMA.

 * This entity represents the general relationship between two part master
 * one a definition of a component and the other a definition of the parent
 * This entity is normally not recommended to be instantiated as itself, but is
 * instantiated as the subtype next_assembly_usage_occurrence. This subtype
 * a unique individual occurrence of the component definition as used within the
 * assembly. The assembly_component_usage is only instantiated as itself if
 * is an explicit requirement to represent the general relationship between a
 * and an assembly definition.
 *
 */
abstract class Rb_Pdm_Usage_Abstract extends Rb_Pdm_Abstract
{

  protected $OBJECT_TABLE = 'pdm_usage'; //(String)Table name

  //Fields:
  protected $FIELDS_MAP_ID = 'usg_id'; //(String)Name of the field of the primary key

  protected $_id = 0; //(Integer) id of the current item

  protected $core_props = array(
    'usg_number'=>'',
    'usg_name'=>'',
    'usg_description'=>'',
    'usg_type_id'=>1, //1=basic type, 2=quantified type, 3=promissory type
    'child_id'=>0,
    'parent_id'=>0,
  ); //(array) contains the properties of the current object
  
  protected $_child = false; //(Rb_Pdm_Product_Definition)
  protected $_parent = false; //(Rb_Pdm_Product_Definition)

//-------------------------------------------------------------------------
/*! \brief return a instance of object.
* 
*/
public static function get( $id = -1, $type_id = 1 ){
  switch($type_id){
    case(1):
      return Rb_Pdm_Usage::get($id);
      break;
    case(2):
      return Rb_Pdm_Usage_Quantified::get($id);
    break;
    case(3):
      return Rb_Pdm_Usage_Promissory::get($id);
    break;
  }
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function setParent(Rb_Pdm_Product_Definition &$product){
  if($this->getId() < 0 ) return false;
  $this->setProperty( 'parent_id', $product->getId() );
  return $this->_parent =& $product;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getParent(){
  if( $this->getId() < 0 ) return false;
  if( $this->_parent ) return $this->_parent;
  if( !$this->getProperty('parent_id') ){
    return false;
  }
  return $this->_parent =& Rb_Pdm_Product_Definition::get( $this->getProperty('parent_id') );
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function setChild(Rb_Pdm_Product_Definition &$product){
  if($this->getId() < 0 ) return false;
  $this->setProperty( 'child_id', $product->getId() );
  return $this->_child =& $product;
}//End of method

//----------------------------------------------------------
/*! \brief
 */
public function getChild(){
  if( $this->getId() < 0 ) return false;
  if( $this->_child ) return $this->_child;
  if( !$this->getProperty('child_id') ){
    return false;
  }
  return $this->_child =& Rb_Pdm_Product_Definition::get( $this->getProperty('child_id') );
}//End of method

//----------------------------------------------------------
/*! \brief get the product_definition with no father defined in usage of the context
 * return the root usages
 */
public function getRoot($context_id, $rs = false){
  /*root are product definition of the context without fathers in this context.
    So the root product_definition id is not define like a child in the usages of the context
  */
  $query = "SELECT * FROM pdm_product_definition
   WHERE pdm_product_definition.ct_id = $context_id
   AND pdm_product_definition.pd_id NOT IN (
     SELECT pdm_usage.child_id
     FROM pdm_usage
     JOIN pdm_product_definition
     ON pdm_usage.parent_id = pdm_product_definition.pd_id
     WHERE pdm_product_definition.ct_id = $context_id
   )";

  if(!$rs = $this->dbranchbe->Execute( $query ) ){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query), 
                                                  $this->dbranchbe->ErrorMsg());
    return false;
  }else{
    if ( $rs === true ) return $rs;
    return $rs->GetArray(); //To transform result in array;
  }
}

}//End of class

