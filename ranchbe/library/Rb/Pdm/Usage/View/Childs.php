<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
TABLE DEFINITION :
DROP VIEW pdm_usage_view_childs;

CREATE ALGORITHM = UNDEFINED VIEW pdm_usage_view_childs AS 
SELECT 
pdm_usage.usg_id,
pdm_usage.usg_number,
pdm_usage.usg_name,
pdm_usage.usg_description,
pdm_usage.usg_type_id,
pdm_usage.parent_id,
pdm_product_definition.pd_id,
pdm_product_definition.ct_id,
pdm_product_definition.pd_description,
pdm_product_version.pv_id,
pdm_product_version.version_id,
pdm_product_version.pv_description,
pdm_product_version.document_id,
pdm_product_version.document_space,
pdm_product.p_id,
pdm_product.p_number,
pdm_product.p_name,
pdm_product.p_description
FROM pdm_usage
INNER JOIN pdm_product_definition ON 
pdm_product_definition.pd_id = pdm_usage.child_id

INNER JOIN pdm_product_version ON
pdm_product_definition.pv_id = pdm_product_version.pv_id

INNER JOIN pdm_product ON
pdm_product.p_id = pdm_product_version.of_product_id

;
*/

//require_once('core/dao/abstract.php');

/*! \brief define a view for each product with his definition, version and context
 */
class Rb_Pdm_Usage_View_Childs extends Rb_Dao_Abstract{

  protected $OBJECT_TABLE = 'pdm_usage_view_childs'; //(String)Table name

  protected $core_props = array(
    'p_id'=>0,  /* product id */
    'pv_id'=>0, /* product version id */
    'pd_id'=>0, /* product definition id */
    'ct_id'=>0, /* primary context id */
    'usg_id'=>0, /* usage id */
    'parent_id'=>0, /* parent product definition id */
    'p_number'=>'',/* product number */
    'usg_number'=>'', /* usage number */
    'p_name'=>'',/* product name */
    'usg_name'=>'', /* usage name */
    'p_description'=>'',/* product description */
    'pv_description'=>'',/* product version description */
    'pd_description'=>'',/* product definition description */
    'usg_description'=>'', /* usage description */
    'version_id'=>0,/* version */
    'document_id'=>0,/* associated document id */
    'document_space'=>0,/* associated document space */
    'usg_type_id'=>0, /* type of usage: quantified, promissory...*/
  ); //(array) contains the properties of the current object

  //protected static $_registry = array(); //(array) registry of instances
  protected static $_registry = false; //singleton registry

//--------------------------------------------------------------------
/** protected to forced use of get method
 */
protected function __construct(){
  $this->dbranchbe =& Ranchbe::getDb();
  $this->error_stack  =& Ranchbe::getError();
}//End of method

public function get(){
  if ( !self::$_registry ){
    self::$_registry = new self();
  }
  return self::$_registry;
}//End of method

//--------------------------------------------------------------------
function getAll($params = array() ){
  return $this->getAllBasic($params);
}//End of method

}//End of class
