<?php

/** Message to ranchbe users. Class generate from tikiwiki Messu class
 * 
 * @todo : rewrite on Zend_Mail model
 *
 */
class Rb_Message extends Rb_Dao_Tiki{

	//--------------------------------------------------------------------
	public function __construct(ADOConnection $db) {
		$this->db = $db;
	}

	//--------------------------------------------------------------------
	/**
	 * Check if a users exists (wrapper for userlib function)
	 */
	public function userExists($user) {
		//$id = Ranchbe::getAuthAdapter()->getUserIdFromName($user);
		$id = Rb_User::getUserIdFromName($user);
		if($id) return $id;
		else return false;
	}

	//--------------------------------------------------------------------
	/**
	 * Put sent Message to 'sent' box
	 */
	public function saveSentMessage($user, $from, $to, $cc, $subject, $body, $priority, $replyto_hash='') {

		$subject = strip_tags($subject);
		//$body = strip_tags($body, '<a><b><img><i>');
		$body = $this->cleanPutText($body);

		// Prevent duplicates
		$hash = md5($subject . $body);

		if ($this->getOne("select count(*) from `messu_sent` where `user`=? and `user_from`=? and `hash`=?",array($user,$from,$hash))) {
			return false;
		}

		$now = date('U');
		$query = "insert into `messu_sent`(`user`,`user_from`,`user_to`,`user_cc`,`subject`,`body`,`date`,`isRead`,`isReplied`,`isFlagged`,`priority`,`hash`,`replyto_hash`) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$this->db->execute($query, array($user,$from,$to,$cc,$subject,$body,(int) $now,'n','n','n',(int) $priority,$hash,$replyto_hash) );

		return true;
	}

	//--------------------------------------------------------------------
	/** Send a Message to a user
	 * 
	 * @param string | integer $user id or handle
	 * @param string | integer $from user id or handle
	 * @param string | integer $to user id or handle
	 * @param string | integer $cc copy user id or handle
	 * @param string $subject
	 * @param string $body
	 * @param integer $priority
	 * @param string $replyto_hash
	 * @return boolean
	 */
	public function postMessage($user, $from, $to, $cc, $subject, $body, $priority, $replyto_hash='') {
		$subject = strip_tags($subject);
		$body = $this->cleanUserInputText($body);
		// Prevent duplicates
		$hash = md5($subject . $body . time());
		
		if ($this->getOne("select count(*) from `messu_messages` where `user`=? and `user_from`=? and `hash`=?",array($user,$from,$hash))) {
			return false;
		}

		//convert user_name to user_id
		if( is_int($user) )
			$user = Rb_User::get($user)->getName();
		if( is_int($from) )
			$from = Rb_User::get($from)->getName();
		if( is_int($to) )
			$to = Rb_User::get($to)->getName();
		if( is_int($cc) )
			$cc = Rb_User::get($cc)->getName();

		Rb_Message_Notification::notify( Rb_User::getUserIdFromName($to) );

		$now = date('U');
		$query = "insert into `messu_messages`(`user`,`user_from`,`user_to`,`user_cc`,`subject`,`body`,`date`,`isRead`,`isReplied`,`isFlagged`,`priority`,`hash`,`replyto_hash`) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$this->query( $query, array($user,$from,$to,$cc,$subject,$body,(int) $now,'n','n','n',(int) $priority,$hash,$replyto_hash) );

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * Get a list of messages from users mailbox or users mail archive (from
	 * which depends on $dbsource)
	 */
	public function listUserMessages($user, $offset, $maxRecords, $sort_mode, $find, $flag = '', $flagval = '', $prio = '', $dbsource, $replyto_hash='', $orig_or_reply='r') {
		if ($dbsource=='') $dbsource='messages';
		$bindvars = array($user);
		$mid='';
		if ($prio) {
			$mid = ' and priority=? ';
			$bindvars[] = $prio;
		}
		if ($replyto_hash) {
			// find replies
			if ($orig_or_reply == 'r') {
				$mid .= ' and replyto_hash=? ';
				// find original for the reply
			} else {
				$mid .= ' and hash=? ';
			}
			$bindvars[] = $replyto_hash;
		}
		if ($flag) {
			// Process the flags
			$mid.= " and `$flag`=? ";
			$bindvars[] = $flagval;
		}
		if ($find) {
			$findesc = '%'.$find.'%';
			$mid.= " and (`subject` like ? or `body` like ?)";
			$bindvars[] = $findesc;
			$bindvars[] = $findesc;
		}

		$query = "select * from `messu_".$dbsource."` where `user`=? $mid order by ".$this->convert_sortmode($sort_mode).",".$this->convert_sortmode("msgId_desc");
		$query_cant = "select count(*) from `messu_".$dbsource."` where `user`=? $mid";
		$result = $this->query($query, $bindvars, $maxRecords, $offset);
		$cant = $this->getOne($query_cant, $bindvars);
		$ret = array();

		while ($res = $result->fetchRow()) {

			$res["len"] = strlen($res["body"]);

			if (empty($res['subject']))
			$res['subject'] = tra('NONE');

			$ret[] = $res;
		}

		$retval = array();
		$retval["data"] = $ret;
		$retval["cant"] = $cant;
		return $retval;
	}

	//--------------------------------------------------------------------
	/**
	 * Get the number of messages in the users mailbox or mail archive (from
	 * which depends on $dbsource)
	 */
	public function countMessages($user, $dbsource='messages') {
		if ($dbsource=='') $dbsource="messages";
		$bindvars = array($user);
		$query_cant = "select count(*) from `messu_".$dbsource."` where `user`=?";
		$cant = $this->getOne($query_cant,$bindvars);
		return $cant;
	}

	//--------------------------------------------------------------------
	/**
	 * Update Message flagging
	 */
	public function flagMessage($user, $msgId, $flag, $val, $dbsource="messages") {
		if (!$msgId)
		return false;
		if ($dbsource=='') $dbsource="messages";
		$query = "update `messu_".$dbsource."` set `$flag`=? where `user`=? and `msgId`=?";
		$this->query($query,array($val,$user,(int)$msgId));
	}

	//--------------------------------------------------------------------
	/**
	 * Mark a Message as replied
	 */
	public function markReplied($user, $replyto_hash, $dbsource="sent") {
		if ((!$replyto_hash) || ($replyto_hash==''))
		return false;
		if ($dbsource=='') $dbsource="sent";
		$query = "update `messu_".$dbsource."` set `isReplied`=? where `user`=? and `hash`=?";
		$this->query($query,array('y', $user, $replyto_hash));
	}

	//--------------------------------------------------------------------
	/**
	 * Delete Message from mailbox or users mail archive (from which depends on
	 * $dbsource)
	 */
	public function deleteMessage($user, $msgId, $dbsource="messages") {
		if (!$msgId)
		return false;
		if ($dbsource=='') $dbsource="messages";
		$query = "delete from `messu_".$dbsource."` where `user`=? and `msgId`=?";
		$this->query($query,array($user,(int)$msgId));
	}

	//--------------------------------------------------------------------
	/**
	 * Delete Message older than x days from mailbox or users mail archive (from which depends on
	 * $dbsource)
	 */
	public function deleteMessages($user, $days, $dbsource="messages") {
		if ($days<1)
		return false;
		if ($dbsource=='') $dbsource="messages";
		$age = date("U") - ($days * 3600 * 24);

		$query = "delete from `messu_".$dbsource."` where `user`=? and `isRead`=? and `date`<=?";
		$this->query($query,array($user, 'y',(int)$age));
	}

	//--------------------------------------------------------------------
	/**
	 * Move Message from mailbox to users mail archive
	 */
	public function archiveMessage($user, $msgId, $dbsource="messages") {
		if (!$msgId)
		return false;
		if ($dbsource=='') $dbsource="messages";
		$query = "insert into `messu_archive` select * from `messu_".$dbsource."` where `user`=? and `msgId`=?";
		$this->query($query,array($user,(int)$msgId));

		$query = "delete from `messu_".$dbsource."` where `user`=? and `msgId`=?";
		$this->query($query,array($user,(int)$msgId));
	}

	//--------------------------------------------------------------------
	/**
	 * Move read Message older than x days from mailbox to users mail archive
	 */
	public function archiveMessages($user, $days, $dbsource="messages") {
		if ($days<1)
		return false;
		if ($dbsource=='') $dbsource="messages";
		$age = date("U") - ($days * 3600 * 24);

		// @todo: only move as much msgs into archive as there is space left in there
		$query = "insert into `messu_archive` select * from `messu_".$dbsource."` where `user`=? and `isRead`=? and `date`<=?";
		$this->query($query,array($user, 'y',(int)$age));

		$query = "delete from `messu_".$dbsource."` where `user`=? and `isRead`=? and `date`<=?";
		$this->query($query,array($user, 'y',(int)$age));
	}

	//--------------------------------------------------------------------
	/**
	 * Move forward to the next Message and get it from the database
	 */
	public function getNextMessage($user, $msgId, $sort_mode, $find, $flag, $flagval, $prio, $dbsource="messages") {
		if (!$msgId)
		return 0;
		if ($dbsource=='') $dbsource="messages";

		$mid = "";
		$bindvars = array($user,(int)$msgId);
		if ($prio) {
			$mid.= " and priority=? ";
			$bindvars[] = $prio;
		}

		if ($flag) {
			// Process the flags
			$mid.= " and `$flag`=? ";
			$bindvars[] = $flagval;
		}
		if ($find) {
			$findesc = '%'.$find.'%';
			$mid.= " and (`subject` like ? or `body` like ?)";
			$bindvars[] = $findesc;
			$bindvars[] = $findesc;
		}

		$query = "select min(`msgId`) as `nextmsg` from `messu_".$dbsource."` where `user`=? and `msgId` > ? $mid";
		$result = $this->query($query,$bindvars,1,0);
		$res = $result->fetchRow();

		if (!$res)
		return false;

		return $res['nextmsg'];
	}

	//--------------------------------------------------------------------
	/**
	 * Move backward to the next Message and get it from the database
	 */
	public function getPrevMessage($user, $msgId, $sort_mode, $find, $flag, $flagval, $prio, $dbsource="messages") {
		if (!$msgId)
		return 0;
		if ($dbsource=='') $dbsource="messages";

		$mid = "";
		$bindvars = array($user,(int)$msgId);
		if ($prio) {
			$mid.= " and priority=? ";
			$bindvars[] = $prio;
		}

		if ($flag) {
			// Process the flags
			$mid.= " and `$flag`=? ";
			$bindvars[] = $flagval;
		}
		if ($find) {
			$findesc = '%'.$find.'%';
			$mid.= " and (`subject` like ? or `body` like ?)";
			$bindvars[] = $findesc;
			$bindvars[] = $findesc;
		}

		$query = "select max(`msgId`) as `prevmsg` from `messu_".$dbsource."` where `user`=? and `msgId` < ? $mid";
		$result = $this->query($query,$bindvars,1,0);
		$res = $result->fetchRow();

		if (!$res)
		return false;

		return $res['prevmsg'];
	}

	//--------------------------------------------------------------------
	/**
	 * Get a Message from the users mailbox or his mail archive (from which
	 * depends on $dbsource)
	 */
	public function getMessage($user, $msgId, $dbsource='messages') {
		if ($dbsource=='') $dbsource="messages";
		$bindvars = array($user,(int)$msgId);
		$query = "select * from `messu_".$dbsource."` where `user`=? and `msgId`=?";
		$result = $this->query($query,$bindvars);
		$res = $result->fetchRow();
		//$res['parsed'] = $this->parse_data($res['body']);
		//$res['parsed'] = html_entity_decode($res['body']);
		$res['parsed'] = $this->cleanPutText($res['body']);
		$res['len'] = strlen($res['parsed']);

		if (empty($res['subject']))
		$res['subject'] = tra('NONE');

		return $res;
	}

	//--------------------------------------------------------------------
	/**
	 * Get Message from the users mailbox or his mail archive (from which
	 * depends on $dbsource)
	 */
	public function getMessages($user, $dbsource='messages', $subject='', $to='', $from='') {
		if ($dbsource=='') $dbsource="messages";
		$bindvars[] = array($user);

		$mid = "";

		// find mails with a specific subject
		if ($subject<>'') {
			$findesc = '%'.$subject.'%';
			$bindvars[] = $findesc;
			$mid.= " and `subject` like ?";
		}
		// find mails to a specific user (to, cc, bcc)
		if ($to<>'') {
			$findesc = '%'.$to.'%';
			$bindvars[] = $findesc;
			$bindvars[] = $findesc;
			$bindvars[] = $findesc;
			$mid.= " and (`user_to` like ? or `user_cc` like ? or `user_bcc` like ?)";
		}
		// find mails from a specific user
		if ($from<>'') {
			$findesc = '%'.$from.'%';
			$bindvars[] = $findesc;
			$mid.= " and `user_from` like ?";
		}
		$query = "select * from `messu_".$dbsource."` where `user`=? $mid";

		$result = $this->query($query,$bindvars);
		while ($res = $result->fetchRow()) {
			//$res['parsed'] = $this->parse_data($res['body']);
			$res['parsed'] = $this->cleanPutText($res['body']);
			$res['len'] = strlen($res['parsed']);
			if (empty($res['subject']))
			$res['subject'] = tra('NONE');
			$ret[] = $res;
		}
		return $ret;
	} //End of method
	
	//--------------------------------------------------------------------
	/**
	 * Suppress quote, html tag and php tags from a string
	 */
	public function cleanUserInputText($string) {
		$string = (get_magic_quotes_gpc())?stripslashes($string) : $string; //Suppress quotes if necessary
		//$string = strip_tags($string, '<a><b><img><i>'); //Suppress html tags except ...
		//$string = nl2br($string); //Convert html entities
		//$string = htmlentities($string); //Convert html entities
		return $string;
	} //End of method

	//--------------------------------------------------------------------
	/**
	 * Suppress quote, html tag and php tags from a string
	 */
	public function cleanPutText($string) {
		$string = (get_magic_quotes_gpc())?stripslashes($string) : $string;
		$string = html_entity_decode($string);
		return $string;
	} //End of method

} //End of class
