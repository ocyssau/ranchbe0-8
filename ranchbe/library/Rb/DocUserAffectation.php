<?php

/*
CREATE TABLE `doc_user_affectation` (
  `document_id` int(11) NOT NULL,
  `document_space` char(8) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` varchar(256) default NULL,
  PRIMARY KEY  (`document_id`),
  KEY `doc_user_affectation_1` (`user_id`),
  KEY `doc_user_affectation_2` (`document_id`,`document_space`,`user_id`)
) ENGINE=InnoDB;
*/

/* THIS CLASS IS NOT USE
*/
//require_once('core/dao/abstract.php');
class DocUserAffectation extends Rb_Dao_Abstract{

  public $document_id;
  public $document_space;
  public $user_id;
  protected $message;

  function __construct(){
  } //end of method

  function init($document_id, $document_space, $user_id){
  } //end of method

  function save(){
  } //end of method

  function setMessage($message){
  } //end of method

  function getMessage(){
  } //end of method

  function isAffected(){
  } //end of method

} //End of class


