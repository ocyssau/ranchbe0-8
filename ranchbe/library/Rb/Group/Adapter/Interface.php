<?php

interface Rb_Group_Adapter_Interface{

  //----------------------------------------------------------------------------
  /* get groups of this user
  */
  public function getGroups();

  //----------------------------------------------------------------------------
  /* get groups of this group
  * return associative array where key = group_id
  */
  public function getSubGroups( $group_id );

  //----------------------------------------------------------------------------
  /* get users of this group
  * return associative array where key = user_id
  */
  public function getUsers( $group_id );

  //----------------------------------------------------------------------------
  /* Get infos about group.
  */
  public function getInfos($group_id);

  //----------------------------------------------------------------------------
  public function suppress($id);

  //----------------------------------------------------------------------------
  public function save(Rb_Group &$group);

  //----------------------------------------------------------------------------
  public function saveGroups(Rb_Group &$group);

} //End of class
