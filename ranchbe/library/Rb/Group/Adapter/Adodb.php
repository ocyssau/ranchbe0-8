<?php
//http://my.opera.com/zomg/blog/2007/04/30/using-Adodb-with-zend-auth-and-zend-auth-adapter

/*
CREATE TABLE `rb_groups` (
  `group_id` int(11) NOT NULL,
  `group_define_name` varchar(32) default NULL,
  `group_description` varchar(64) default NULL,
  `is_active` tinyint(1) default '1',
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `rb_groups_uniq1` (`group_define_name`),
  KEY `INDEX_rb_groups_1` (`group_define_name`)
) ENGINE=InnoDB;

CREATE TABLE `rb_group_subgroups` (
  `group_id` int(11) NOT NULL,
  `subgroup_id` int(11) NOT NULL,
  UNIQUE KEY `rb_group_subgroups_uniq1` (`group_id`,`subgroup_id`),
  KEY `INDEX_rb_group_subgroups_1` (`group_id`),
  KEY `INDEX_rb_group_subgroups_2` (`subgroup_id`)
) ENGINE=InnoDB ;

ALTER TABLE `rb_group_subgroups`
  ADD CONSTRAINT `FK_rb_group_subgroups_1` FOREIGN KEY (`group_id`) 
  REFERENCES `rb_groups` (`group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_rb_group_subgroups_2` FOREIGN KEY (`subgroup_id`) 
  REFERENCES `rb_groups` (`group_id`) ON DELETE CASCADE;


... See too rb_groupusers ...

*/

//require_once('core/group/adapter/interface.php');
//require_once('core/dao/abstract.php');

class Rb_Group_Adapter_Adodb extends Rb_Dao_Abstract implements Rb_Group_Adapter_Interface{
  protected $dbranchbe;

  protected $OBJECT_TABLE = 'rb_groups';
  protected $FIELDS_MAP_ID = 'group_id';
  protected $FIELDS_MAP_NUM = 'group_define_name';
  protected $FIELDS_MAP_DESC = 'group_description';
  protected $MIN_SEQ_NUMBER = 100;
  protected $SEQ_NAME       = 'roles_seq';

  //----------------------------------------------------------------------------
  public function __construct(ADOConnection &$db){
    $this->dbranchbe =& $db;
  } //End of method

  //----------------------------------------------------------------------------
  /* Get all groups definition from database
   * Return array
  */
  public function getGroups(){
    $params['getRs'] = true;
    return $this->getAllBasic($params)->getAssoc();
  }//End of method

  //----------------------------------------------------------------------------
  /* get groups of this user
  */
  public function getSubGroups( $group_id ){
    $query = 'SELECT group_id, subgroup_id FROM rb_group_subgroups
              WHERE group_id = '.$group_id;
    $rs = $this->dbranchbe->execute( $query );

    if($rs){
      $groups = array();
      while (!$rs->EOF) { //transform result to return array
        $groups[$rs->fields['subgroup_id']] = $rs->fields['subgroup_id'];
        $rs->MoveNext();
      }
      return $groups;
    }else{
      Ranchbe::getErrorstack()->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg() );
      return false;
    }
  }//End of method

  //----------------------------------------------------------------------------
  /* get users of this group
  * return a associative array where key user_id
  */
  public function getUsers( $group_id ){
    $query = "SELECT * FROM `$this->OBJECT_TABLE` JOIN `rb_groupusers`
              ON `$this->OBJECT_TABLE`.`group_id` = `rb_groupusers`.`group_id` JOIN
              `rb_users` ON `rb_users`.`auth_user_id` = `rb_groupusers`.`auth_user_id`
              WHERE `$this->OBJECT_TABLE`.`group_id` = $group_id
              ";

    $rs = $this->dbranchbe->execute( $query );

    if($rs){
      $users = array();
      while (!$rs->EOF) { //transform result to return array
        $users[$rs->fields['auth_user_id']] = $rs->fields;
        $rs->MoveNext();
      }
      return $users;
    }else{
      Ranchbe::getErrorstack()->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg() );
      return false;
    }

  }//End of method

  //----------------------------------------------------------------------------
  /* Get infos about group.
  */
  public function getInfos($group_id){
    $params['exact_find']['group_id'] = $group_id;
    $params['getRs'] = true;

    return $this->getAllBasic($params)->fetchRow(); //return just one row
  }//End of method

  //----------------------------------------------------------------------------
  /* Suppress the group
  *  Return true or false
  */
  public function suppress($id){
    return $this->_basicSuppress($id);
  }//End of method

  //----------------------------------------------------------------------------
  /* Save group $group in database
  *  If group is yet existing, update it, else create it
  *  Return new group id or false
  */
  public function save(Rb_Group &$group){
    $data[$this->FIELDS_MAP_NUM]      = $group->getGroupname();
    $data[$this->FIELDS_MAP_DESC]     = $group->getProperty('group_description');
    $data['is_active']                = $group->getProperty('is_active');

    if( !$group->getId() )
      return $this->_basicCreate($data);
    else
      return $this->_basicUpdate( $data, $group->getId() );
  }//End of method

  //----------------------------------------------------------------------------
  /* Save the current sub-group associations from group $group
  *  Return true or false
  */
  public function saveGroups(Rb_Group &$group){
    $this->dbranchbe->StartTrans();

    if( !$this->_suppressGroups( $group->getId() ) ){ //before clean all groups
      $this->dbranchbe->CompleteTrans(false);
      return false;
    }

    $this->dbranchbe->CompleteTrans();

    $query = "INSERT INTO rb_group_subgroups (
                                                    `group_id` ,
                                                    `subgroup_id`
                                                    )";
    $i=0;
    foreach( $group->getSubGroups() as $group_id=>$group_properties){ //Generate insert
      if ($i == 0) $query .= ' VALUES ';
      if ($i > 0) $query .= ',';
      $query .= "('".$group->getId()."', '$group_id')";
      $i++;
    }
    if($i == 0) return $this->dbranchbe->CompleteTrans();

    if ( !$this->dbranchbe->Execute( $query ) ){
      Ranchbe::getError()->errorDb(Ranchbe::getDb()->ErrorMsg(), $query);
      $this->dbranchbe->FailTrans();
    }

    return $this->dbranchbe->CompleteTrans();

  }//End of method

  //----------------------------------------------------------------------------
  /* suppress group id $group_id
  */
  protected function _suppressGroups($group_id){
    if( !$group_id ) return false;

    $query = 'DELETE FROM rb_group_subgroups WHERE group_id = \''.$group_id.'\'';

    //Execute the query
    if(!$this->dbranchbe->Execute($query)){
      Ranchbe::getError()->errorDb(Ranchbe::getDb()->ErrorMsg(), $query);
      $this->dbranchbe->FailTrans();
      return false;
    }

    return true;

  } //End of method

} //End of class
