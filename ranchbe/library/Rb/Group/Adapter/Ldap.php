<?php

//require_once('Zend/Ldap.php');
//require_once('core/group/adapter/interface.php');

class Rb_Group_Adapter_Ldap implements Rb_Group_Adapter_Interface{

  //----------------------------------------------------------------------------
  public function __construct(){
  } //End of method

  //----------------------------------------------------------------------------
  /* Get all groups definition from Ldap reposit
   * Return array
  */
  public function getGroups(){
    return array();
  }//End of method

  //----------------------------------------------------------------------------
  /* Get subgroups from group id = $group_id
  */
  public function getSubGroups( $group_id ){
    return array();
    return false;
  }//End of method

  //----------------------------------------------------------------------------
  /* Get users of this group
  *  Return a associative array where key = user_id
  */
  public function getUsers( $group_id ){
    return array();
    return false;
  }//End of method

  //----------------------------------------------------------------------------
  /* Get infos about group.
  */
  public function getInfos($group_id){
    return array();
  }//End of method

  //----------------------------------------------------------------------------
  /* Suppress the group id=$id
  *  Return true or false
  */
  public function suppress($id){
    return false;
    return true;
  }//End of method

  //----------------------------------------------------------------------------
  /* Save the group $group in Ldap database
  *  If group is yet existing, update it, else create it
  *  Return new group id or false
  */
  public function save(Rb_Group &$group){
    return false;
    return integer;
  }//End of method

  //----------------------------------------------------------------------------
  /* Save the current sub-group associations from group $group
  *  Return true or false
  */
  public function saveGroups(Rb_Group &$group){
    return false;
    return true;
  }//End of method

} //End of class
