<?php

/*
CREATE TABLE `messu_notification` (
  `user_id` int(11) NOT NULL,
  KEY  (`user_id`)
) ENGINE=InnoDB ;

ALTER TABLE `messu_notification` 
  ADD CONSTRAINT `FK_messu_notification_1` FOREIGN KEY (`user_id`) 
  REFERENCES `rb_users` (`auth_user_id`) 
  ON DELETE CASCADE ;
*/

/* Notification is used to create Notification system wich not depends of read/noread
* states. Create a index when a message is send and notify user when this index is not empty
*
*/
class Rb_Message_Notification{
  protected static $_instance = false;
  protected static $OBJECT_TABLE = 'messu_notification';
  protected static $FIELDS_MAP_ID = 'user_id';

  //----------------------------------------------------------------------------
  public static function notify($user_id){
    return Ranchbe::getDb()->AutoExecute( self::$OBJECT_TABLE , array('user_id'=>$user_id) , 'INSERT');
  }

  //----------------------------------------------------------------------------
  public static function hasMessage($user_id){
    $query = 'SELECT user_id FROM '.self::$OBJECT_TABLE.' WHERE user_id = '.$user_id;
    $rset = Ranchbe::getDb()->Execute($query);
    if(!$rset) return 0;
    return $rset->RecordCount();
  }

  //----------------------------------------------------------------------------
  public static function clear($user_id){
    $query = 'DELETE FROM '.self::$OBJECT_TABLE.' WHERE user_id = '.$user_id;
    return Ranchbe::getDb()->Execute($query);
  }

} //End of class
