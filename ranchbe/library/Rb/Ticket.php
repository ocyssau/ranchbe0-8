<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


/** Security ticket.
 *  Ticket use here is a md5 hash security code to compare with code record in db.
 * 	This ticket is use to give access to file to user without valid access to ranchbe.
 *  This ticket has limited time validity.
 *  
 *  ticket_id is security code (md5 hash).
 *  expiration_date is timestamp of up to date ticket validity.
 *  for_object_id is the identifier of the object to which the ticket provides access.
 *
 * @author Olivier CYSSAU
 *
 */
class Rb_Ticket{
	
	//----------------------------------------------------------
	/** Return expiration time stamp if ticket is valid or false if not.
	 * 
	 * @param string	value of ticket
	 * @param integer 	id of object to which the ticket provides access
	 * @return integer
	 */
	public static function isValid($ticket, $object_id){
		$query = "SELECT expiration_date FROM tickets WHERE ticket_id = '$ticket'"
				 .' AND expiration_date > '. time()
				 ." AND for_object_id = $object_id";
		$ret = Ranchbe::getDb()->getOne($query);
		if( $mess = Ranchbe::getDb()->ErrorMsg() ){
			Ranchbe::getError()->errorDb($mess);
			return false;
		}
		return $ret;
	}//End of method

	
	//----------------------------------------------------------
	/** Set a new ticket and return the ticket value.
	 * 
	 * @param integer 	id of object to which the ticket provides access
	 * @return string	ticket value
	 */
	public static function setTicket($object_id){
		$expiration_date = time() + Ranchbe::getConfig()->security->ticket->expiration_time;
		$ticket = md5( rand(-10000, +10000) . uniqid() . $expiration_date );
		$data = array('ticket_id'=>$ticket,
						'expiration_date' => $expiration_date,
						'for_object_id' => $object_id);
		$ok = Ranchbe::getDb()->AutoExecute ( 'tickets', $data, 'INSERT' );
		if( $mess = Ranchbe::getDb()->ErrorMsg() ){
			Ranchbe::getError()->errorDb($mess);
			return false;
		}
		return $ticket;
	}//End of method

	
	//----------------------------------------------------------
	/** Delete all expired tickets in database
	 * 
	 * @return void
	 */
	public static function cleanExpired(){
		$query = 'DELETE FROM tickets WHERE expiration_date < '. time();
		$ret = Ranchbe::getDb()->Execute($query);
		if( $mess = Ranchbe::getDb()->ErrorMsg() ){
			Ranchbe::getError()->errorDb($mess);
			return false;
		}
		return $ret;
	}//End of method
	
	
} //End of class
