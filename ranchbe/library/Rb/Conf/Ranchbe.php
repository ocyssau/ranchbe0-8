<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of Ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

final class Ranchbe {
	const RANCHBE_VER = '0.8.0.5Beta2'; /* Current version of Ranchbe */
	const RANCHBE_BUILD = '%RANCHBE_BUILD%'; /* Build number of Ranchbe*/
	const RANCHBE_COPYRIGHT = '&#169;2007-2010 by Ranchbe group'; /* Copyright */

	protected static $_instance = null;
	protected static $_dbranchbe = null;
	protected static $_rodbranchbe = null;
	protected static $_logger = null;
	protected static $_errorStack = null;
	protected static $_loggerEnable = true;
	protected static $_fileTypes = null; //
	protected static $_fileExtensions = null; //authorized extensions
	protected static $_visuFileExtensions = null; //(array) authorized file extensions for visu files
	protected static $_authAdapter = null; //(object)
	protected static $_groupAdapter = null; //(object)
	protected static $_session = null; //(Send_Session)
	
	protected static $_acl = null;
	protected static $_context = null;
	protected static $_config = false;
	
	protected static $_browser = "firefox";
	public static $_publicPath;
	/**
	 * Create Adodb object
	 *
	 * @param array $options
	 * @param boolean $db_consult_only
	 * @return ADOConnection
	 */
	protected static function _createAdodb(array $options=array(), $db_consult_only = false) {
		/*Database connexion parameters*/
		$adodb_driver   = $options['adapter'];
		$host_ranchbe   = $options['params']['host'];
		$dbs_ranchbe    = $options['params']['dbname'];
		if($db_consult_only){ //read only access user
			$user_ranchbe   = $options['params']['readonly']['username'];
			$pass_ranchbe   = $options['params']['readonly']['password'];
		}else{ //Full access user
			$user_ranchbe   = $options['params']['username'];
			$pass_ranchbe   = $options['params']['password'];
		}
		define ('ADODB_LANG' , $options['lang'] ); /*Language to use for adodb not from user prefs because else create infinit loop... */
		require_once('adodb.inc.php');
		//require_once('adodb-pear.inc.php');
		/*
		 The behaviour of GetUpdateSQL() and GetInsertSQL() when converting empty or null PHP variables to SQL is controlled by the global $ADODB_FORCE_TYPE variable. Set it to one of the values below. Default is ADODB_FORCE_VALUE (3):
		  
		 0 = ignore empty fields. All empty fields in array are ignored.
		 1 = force null. All empty, php null and string 'null' fields are changed to sql NULL values.
		 2 = force empty. All empty, php null and string 'null' fields are changed to sql empty '' or 0 values.
		 3 = force value. Value is left as it is. Php null and string 'null' are set to sql NULL values and
		 empty fields '' are set to empty '' sql values.
		  
		 define('ADODB_FORCE_IGNORE',0);
		 define('ADODB_FORCE_NULL',1);
		 define('ADODB_FORCE_EMPTY',2);
		 define('ADODB_FORCE_VALUE',3);
		 */
		$ADODB_FORCE_TYPE = ADODB_FORCE_NULL;
		$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC; //This is a global variable that determines how arrays are retrieved by recordsets.See Adodb manual.

		//$dsn = "$adodb_driver://$user_ranchbe:$pass_ranchbe@$host_ranchbe/$dbs_ranchbe";
		$dbranchbe = &ADONewConnection ( $adodb_driver ); //Create a new Adodb object

		//Connect to database
		if (! @$dbranchbe->Connect ( $host_ranchbe, $user_ranchbe, $pass_ranchbe, $dbs_ranchbe )) {
			print "
		      <html><body>
		      <h2><font color='red'>RanchBE is not properly set up:</font></h1>
		      <div>Unable to connect to the database !</div>
		      <!--<a href='Ranchbe-install.php'>Go here to begin the installation process</a>, if you haven't done so already.</p>-->
		      </body></html>
		      ";
			print $dbranchbe->ErrorMsg ();
			die ();
		}

		if ($options['profiling']['log'] || $options['profiling']['display']) {
			$dbranchbe->LogSQL (); // turn on logging
			define ( 'ADODB_PERF_NO_RUN_SQL', 1 ); //Desactive allow users to enter and run SQL interactively via the "Run SQL" link.
		}

		if ($options['debug']) $dbranchbe->debug = true;
			
		$dbranchbe->SetTransactionMode("SERIALIZABLE");

		return $dbranchbe;

	} // End of method
	
	
	/** EXPERIMENTAL, NOT USE
	 * Create Adodb object with PDO
	 *
	 * @param array $options
	 * @param boolean $db_consult_only
	 * @return ADOConnection
	 */
	protected static function _createAdodbPdo(array $options=array(), $db_consult_only = false) {
		/*Database connexion parameters*/
		$driver   	= $options['adapter'];
		$host  	 	= $options['params']['host'];
		$dbname		= $options['params']['dbname'];
		if($db_consult_only){ //read only access user
			$user   = $options['params']['readonly']['username'];
			$pass   = $options['params']['readonly']['password'];
		}else{ //Full access user
			$user   = $options['params']['username'];
			$pass   = $options['params']['password'];
		}
		define ('ADODB_LANG' , $options['lang'] ); /*Language to use for adodb not from user prefs because else create infinit loop... */
		require_once('adodb.inc.php');
		//require_once('adodb-pear.inc.php');
		/*
		 The behaviour of GetUpdateSQL() and GetInsertSQL() when converting empty or null PHP variables to SQL is controlled by the global $ADODB_FORCE_TYPE variable. Set it to one of the values below. Default is ADODB_FORCE_VALUE (3):
		  
		 0 = ignore empty fields. All empty fields in array are ignored.
		 1 = force null. All empty, php null and string 'null' fields are changed to sql NULL values.
		 2 = force empty. All empty, php null and string 'null' fields are changed to sql empty '' or 0 values.
		 3 = force value. Value is left as it is. Php null and string 'null' are set to sql NULL values and
		 empty fields '' are set to empty '' sql values.
		  
		 define('ADODB_FORCE_IGNORE',0);
		 define('ADODB_FORCE_NULL',1);
		 define('ADODB_FORCE_EMPTY',2);
		 define('ADODB_FORCE_VALUE',3);
		 */
		$ADODB_FORCE_TYPE = ADODB_FORCE_NULL;
		$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC; //This is a global variable that determines how arrays are retrieved by recordsets.See Adodb manual.

		//$dsn = "$adodb_driver://$user_ranchbe:$pass_ranchbe@$host_ranchbe/$dbs_ranchbe";
		//$dbranchbe = &ADONewConnection ( $driver ); //Create a new Adodb object
		
		//Connect to database
		$dsn = "$driver:host=$host;dbname=$dbname";
		try{
			$dbranchbe = NewADOConnection('pdo');
			$dbranchbe->PConnect ( $dsn, $user, $pass);
		}catch(PDOException $e){
			print $e->getMessage();
		}
		/*
		$dbranchbe = &ADONewConnection ( 'pdo' ); //Create a new Adodb object
		if (! @$dbranchbe->Connect ( $host, $user, $pass, $dbname )) {
			print "
		      <html><body>
		      <h2><font color='red'>RanchBE is not properly set up:</font></h1>
		      <div>Unable to connect to the database !</div>
		      <!--<a href='Ranchbe-install.php'>Go here to begin the installation process</a>, if you haven't done so already.</p>-->
		      </body></html>
		      ";
			print $dbranchbe->ErrorMsg ();
			die ();
		}
		*/
		
		/* not with pdo:
		if ($options['profiling']['log'] || $options['profiling']['display']) {
			$dbranchbe->LogSQL (); // turn on logging
			define ( 'ADODB_PERF_NO_RUN_SQL', 1 ); //Desactive allow users to enter and run SQL interactively via the "Run SQL" link.
		}
		*/

		if ($options['debug']) $dbranchbe->debug = true;
			
		$dbranchbe->SetTransactionMode("SERIALIZABLE");

		return $dbranchbe;

	} // End of method
	
	
	
	/** EXPERIMENTAL, NOT USE
	 * Create Pdo object
	 *
	 * @param array $options
	 * @param boolean $db_consult_only
	 * @return ADOConnection
	 */
	protected static function _createPdo(array $options=array(), $db_consult_only = false) {
		/*Database connexion parameters*/
		$driver   = $options['adapter'];
		$host   = $options['params']['host'];
		$dbname    = $options['params']['dbname'];
		if($db_consult_only){ //read only access user
			$user   = $options['params']['readonly']['username'];
			$pass   = $options['params']['readonly']['password'];
		}else{ //Full access user
			$user   = $options['params']['username'];
			$pass   = $options['params']['password'];
		}
		
		try{
			$dsn = "$driver:host=$host;dbname=$dbname";
			$dbh = new PDO($dsn, $user, $pass, array(PDO::ATTR_PERSISTENT => true));
		}catch(PDOException $e){
			print $e->getMessage();
		}
		
		return $dbh;
	} // End of method
	

	//------------------------------------------------------------------------
	//Authorized files types
	//------------------------------------------------------------------------
	/**
	 * Set Authorized files types.
	 * 	Extract list of type from mimes.conf file and populate
	 *  static array var self::$_fileTypes and self::$_fileExtensions
	 *
	 * @return void
	 */
	protected static function _getFilesTypes() {
		$valid_file_ext = array (); //init var
		if (! $handle = fopen ( Ranchbe::getConfig()->mimes_file, "r" )) {
			print 'cant open mime definition file :'.Ranchbe::getConfig()->mimes_file;
			die ();
		} else {
			while ( ! feof ( $handle ) ) {
				$data = fgetcsv ( $handle, 1000, ";" );
				if (@implode ( ' ', $data )) { //Test for ignore empty lines
					$extensions = explode ( ' ', $data [2] );
					$valid_file_ext = array_merge ( $valid_file_ext, $extensions );
				}
			}
			fclose ( $handle );
		}

		self::$_fileTypes ['file'] = 'file';
		self::$_fileExtensions ['file'] = array_combine ( $valid_file_ext, $valid_file_ext ); //Valid extension for type file
		sort ( self::$_fileExtensions ['file'] );
		unset ( $extensions );
		unset ( $valid_file_ext );

		//Define extension to determine no file type
		self::$_fileTypes ['nofile'] = 'nofile'; //Never change this value
		self::$_fileExtensions ['nofile'] = array ('NULL' => '' ); //Valid extension for type nofile

		self::$_visuFileExtensions = explode ( ';', VISU_FILE_EXTENSION_LIST );
		self::$_visuFileExtensions = array_combine ( self::$_visuFileExtensions, self::$_visuFileExtensions );
		sort ( self::$_visuFileExtensions );

		//Zend_Registry::set('FILE_TYPE_LIST', self::$_fileTypes);
		//Zend_Registry::set('FILE_EXTENSION_LIST', self::$_fileExtensions);
		//Zend_Registry::set('VISU_FILE_EXTENSION_LIST', self::$_visuFileExtensions);

		return $this;

	} // End of method


	/**
	 * Check that necessary base directories of ranchbe exists
	 *
	 * @return boolean
	 */
	public static function checkDirectories() {
		foreach(Ranchbe::getConfig ()->path->reposit as $reposit){
			if (! is_dir ( $reposit )) {
				print 'Error : ' . $reposit . '  do not exits';
				die ();
			}
		}
		return true;
	}

	//------------------------------------------------------------------------
	/**
	 * Create a new auth adapter if necessary and return it
	 *
	 * @return Rb_User_Adapter_Adodb
	 */
	public static function getAuthAdapter() {
		if (self::$_authAdapter === null) {
			self::$_authAdapter = new Rb_User_Adapter_Adodb ( self::getDb () );
		}
		return self::$_authAdapter;
	}

	//------------------------------------------------------------------------
	/**
	 * Create a new group adapter if necessary and return it
	 *
	 * @return Rb_Group_Adapter_Adodb
	 */
	public static function getGroupAdapter() {
		if (self::$_groupAdapter === null) {
			self::$_groupAdapter = new Rb_Group_Adapter_Adodb ( self::getDb () );
		}

		return self::$_groupAdapter;
	}

	/**
	 * Create logger object from Zend_Log
	 *
	 * @return Rb_Log
	 */
	public static function getLogger() {
		if (self::$_logger === null) {
			$logfile = Ranchbe::getConfig()->resources->ranchbe->log->file;
			self::$_logger = new Rb_Log();
			if($logfile == 'syslog'){
				$writer = new Zend_Log_Writer_Syslog();
				$writer->setApplication('RanchBE');
				//$writer->setFacility(LOG_USER);
				self::$_logger->addWriter($writer);
			}
			else if($logfile){
				$writer = new Zend_Log_Writer_Stream($logfile);
				$format = '[ranchbe]'
//				. Rb_User::getCurrentUser()->getName() //Cancel, because logger is set before definition of user
				. '%timestamp% %priorityName% (%priority%): %message%'
				. PHP_EOL;
				$writer->setFormatter(new Zend_Log_Formatter_Simple($format));
				self::$_logger->addWriter($writer);
			}
		}
		return self::$_logger;

		/*
		 //$popup = Ranchbe::getConfig()->resources->ranchbe->debug->popup;
		 if (self::$_logger === null) {
			$logfile = Ranchbe::getConfig()->resources->ranchbe->logfile;
			$popup = Ranchbe::getConfig()->resources->ranchbe->debug->popup;
			require_once 'Log.php';
			self::$_logger = & Log::singleton ( 'composite' );
			if ($logfile !== 0) {
			$filelogger = & Log::singleton ( 'file',
			$logfile,
			'ranchbe [' . self::getAuth ()->getIdentity () . '] [' . time () . ']' );
			self::$_logger->addChild ( $filelogger );
			}
			if ($popup) {
			$winlogger = & Log::singleton ( 'win', 'LogWindow', 'ranchbe', array ( 'title' => 'RanchBE Log Output' ) ); //$handler, $name, $ident, $conf, $maxLevel
			self::$_logger->addChild ( $winlogger );
			}
			}
			return self::$_logger;
			*/
	}

	/**
	 * Activate/desactivate logger
	 *
	 * @param boolean $bool
	 * @return void
	 */
	public static function activateLogger($bool) {
		self::$_loggerEnable = (bool) $bool;
	}

	/**
	 * Create if necessary a error manager $error_stack and return it
	 *
	 * @return Rb_ErrorManager
	 */
	public static function getError() {
		if (self::$_errorStack === null) {
			self::$_errorStack = new Rb_Error ( 'Ranchbe' );
			if (is_object ( self::getLogger () ) ){
				self::$_errorStack->setLogger ( self::getLogger () );
				self::$_errorStack->setLoglevel( self::getConfig()->resources->ranchbe->log->level );
			}
			//self::$_errorStack->pushCallback ( 'Ranchbe::userPushCallback' );
		}
		return self::$_errorStack;
	}
	
	/**
	 * Alias for getError
	 *
	 * @return Rb_ErrorManager
	 */
	public static function getErrorStack() {
		return self::getError();
	}
	

	//------------------------------------------------------------------------
	//
	/**
	* Callback function to chose type of return log or push or push and log
	*
	* @param array $err
	* @return integer
	*/
	public static function userPushCallback($err) {
		switch ($err ['code']) {
			/**
			 * If this is returned, then the error will only be pushed onto the stack,
			 * and not logged.
			 */
			case 1 :
				return PEAR_ERRORSTACK_PUSHANDLOG;
				break;
				/**
				 * If this is returned, then the error will only be pushed onto the stack,
				 * and not logged.
				 */
			case 'PUSH' :
			case 2 :
				return PEAR_ERRORSTACK_PUSH;
				break;
				/**
				 * If this is returned, then the error will only be logged, but not pushed
				 * onto the error stack.
				 */
			case 'LOG' :
			case 3 :
				return PEAR_ERRORSTACK_LOG;
				break;
				/**
				 * If this is returned, then the error is completely ignored.
				 */
			case 'IGNORE' :
			case 4 :
				return PEAR_ERRORSTACK_IGNORE;
				break;
				/**
				 * If this is returned, then the error is logged and die() is called.
				 */
			case 'DIE' :
			case 5 :
				return PEAR_ERRORSTACK_DIE;
				break;
			default :
				return PEAR_ERRORSTACK_PUSHANDLOG; //push and log
				break;
		}
	}

	/**
	 * Get adodb instance with full access
	 *
	 * @param array $option
	 * @return ADOConnection
	 */
	public static function getDb(array $option=array()) {
		if (self::$_dbranchbe === null) {
			if(!$option) return false;
			self::$_dbranchbe =& self::_createAdodb($option);
		}
		
		return self::$_dbranchbe;
	}

	/**
	 * Get adodb instance with read only access
	 *
	 * @param array $option
	 * @return ADOConnection
	 */
	public static function getRoDb(array $option=array()) {
		if (self::$_rodbranchbe === null) {
			if(!$option) return false;
			self::$_rodbranchbe =& self::_createAdodb($option, true);
		}
		
		return self::$_dbranchbe;
	}
	
	
	
	/**
	 * Get file type from _getFilesTypes() method
	 *
	 * @return array
	 */
	public static function getFileTypes() {
		if (self::$_fileTypes === null) {
			self::_getFilesTypes ();
		}
		return self::$_fileTypes;
	}

	/**
	 * Get file extensions from _getFilesTypes() method
	 *
	 * @return array
	 */
	public static function getFileExtensions() {
		if (self::$_fileExtensions === null) {
			self::_getFilesTypes ();
		}
		return self::$_fileExtensions;
	}

	/**
	 * Get visu file extensions list from _getFilesTypes() method
	 *
	 * @return array
	 */
	public static function getVisuFileExtensions() {
		if (self::$_visuFileExtensions === null) {
			self::_getFilesTypes ();
		}
		return self::$_visuFileExtensions;
	}

	/**
	 * Get the version, build and copyright in array
	 *
	 * @return array
	 */
	public static function getVersion() {
		return array ('ranchbe_ver' => self::RANCHBE_VER,
						'ranchbe_build' => self::RANCHBE_BUILD, 
						'ranchbe_copyright' => self::RANCHBE_COPYRIGHT );
	}


	/**
	 * Get the auth instance. It a shortcut for Zend_Auth::getInstance () method
	 *
	 * @return Zend_Auth
	 */
	public static function getAuth() {
		return Zend_Auth::getInstance ();
	}

	/**
	 * Get and init the acls instance
	 *
	 * @return Rb_Acl
	 */
	public static function getAcl() {
		if (self::$_acl === null) {
			//require_once('Rb/Acl.php');
			self::$_acl = new Rb_Acl ( );
			self::$_acl->init ();
		}
		return self::$_acl;
	} // End of method


	//------------------------------------------------------------------------
	/**
	 * Get context instance
	 *
	 * @return Rb_Context
	 */
	public static function getContext() {
		if (self::$_context === null) {
			self::$_context = new Rb_Context ( );
		}
		return self::$_context;
	} // End of method

	//------------------------------------------------------------------------
	/**
	 * Get the smarty instance
	 *
	 * @return Smarty
	 */
	public static function getSmarty() {
		return Zend_Controller_Front::getInstance()
		->getParam('bootstrap')->getResource('Smarty')->getEngine();
	} // End of method

	//------------------------------------------------------------------------
	/**
	 * Get the layout instance
	 *
	 * @return Zend_Layout
	 */
	public static function getLayout() {
		return Zend_Controller_Front::getInstance()
		->getParam('bootstrap')->getResource('Layout');
	} // End of method

	//------------------------------------------------------------------------
	/**
	 * Get the application instance
	 *
	 * @return Zend_Application
	 */
	public static function getApplication() {
		return Zend_Controller_Front::getInstance()
		->getParam('bootstrap')->getApplication();
	} // End of method

	//------------------------------------------------------------------------
	/**
	 * Get the config instance
	 *
	 * @return Zend_Config
	 */
	public static function getConfig() {
		return self::$_config;
	} // End of method
	
	//------------------------------------------------------------------------
	/** Get the session instance object
	 *
	 * @return Zend_Session_Namespace
	 */
	public static function getSession() {
		if( !self::$_session ){
			$namespace = self::getConfig()->session->namespace;
			self::$_session = new Zend_Session_Namespace($namespace);
		}
		return self::$_session;
	} // End of method
	
	//------------------------------------------------------------------------
	/**
	 * Set the config
	 *
	 * @param Zend_Config $config
	 * @return void
	 */
	public static function setConfig(Zend_Config $config) {
		self::$_config = $config;
	} // End of method
	
	
	
	
	public static function getBrowser() {
		return self::$_browser;
	}
	public static function setBrowser($css) {
		self::$_browser = $css;
	}
	
	
	
	public static function setPublicPath($Path){
		self::$_publicPath = $Path;
	}
	
	public static function getPublicPath(){
		return self::$_publicPath;
	}
	
	//------------------------------------------------------------------------
	/**
	 * Ckeck permissions
	 * 
	 * @param string $privilege name of privilege to test
	 * @param mixed $resource 	Rb_Object or integer or instance of Zend_Acl_Resource or id of the resource
	 * @param boolean $secure	if true, die the script if permission is not granted else just return false
	 * @return boolean
	 */
	static function checkPerm($privilege, &$resource, $secure = true) {
		if (self::getAcl ()->inheritsRole ( Rb_User::getCurrentUser (), 21 )) { //members of admins have all rights
			return true;
		}
		
		if (Rb_User::getCurrentUser ()->getRoleId () == 22) { //user is admin and get all rights
			return true;
		}
		
		$privilege_id = Rb_Acl::getPrivilegeId ( $privilege );
		if (! self::getAcl ()->isAllowed ( Rb_User::getCurrentUser (), $resource, $privilege_id )) {
			if (is_object ( $resource ))
			$resourceId = $resource->getResourceId ();
			else
			$resourceId = $resource;
			self::getError()->push ( Rb_Error::ERROR, array (), tra ( 'You are not authorized to process this request.' ) . ' ' . sprintf ( tra ( 'You must have the privilege %s for ressource %s' ), $privilege, $resourceId ) );
			if ($secure) {
				self::getError()->checkErrors ();
				die ();
			} else {
				return false;
			}
		} else {
			return true;
		}
	} //End of method


	//------------------------------------------------------------------------
	/**
	 * Serialize the $_request and create hidden form
	 * $select(array) permet de selectionner les cle � serialiser
	 * si $form est fournis, genere des elements hidden quick_form
	 * 
	 * @param unknown_type $form
	 * @param unknown_type $select
	 * @return unknown_type
	 */
	public static function serialize_request_post(&$form = '', $select = '') {
		//@todo: revise cette function pour les tableau > 2 dimension
		if (is_array ( $select )) {
			$select = array_flip ( $select );
			$request = array_intersect_key ( $_REQUEST, $select );
		} else {
			$request = $_REQUEST;
		}
		$html = '';
		foreach ( $request as $key => $val ) {
			if ($key == 'logout' || $key == 'handle' || $key == 'passwd' || $key == 'handle' || $key == 'tab' || $key == 'kt_language' || $key == 'tz_offset' || $key == 'username' || $key == 'ml_id' || $key == 'PHPSESSID' || $key == 'LuSession') {
				continue;
			}
			if (is_array ( $val )) {
				foreach ( $val as $sub_key => $sub_sval ) {
					if (is_object ( $form ))
					$form->addElement ( 'hidden', $key . '[' . $sub_key . ']', $sub_sval );
					else
					$html .= '<input type="hidden" name="' . $key . '[' . $sub_key . ']' . '" value="' . $sub_sval . '" />' . "\n";
				}
			} else {
				if (is_object ( $form ))
				$form->addElement ( 'hidden', $key, $val );
				else
				$html .= '<input type="hidden" name="' . $key . '" value="' . $val . '" />' . "\n";
			}
		}
		return $html;
	} //End of function

} //End of class
