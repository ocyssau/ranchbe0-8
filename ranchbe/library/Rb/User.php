<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/acl/role/interface.php');

/** Access to User properties.
 * this class call the object defined by Ranchbe::getAuthAdapter() to access to data
 * so Ranchbe::getAuthAdapter() must return a object wich implements the rb_user_interface_adapter
 *
 * @see Rb_Wildspace
 * @see Rb_User_Preferences
 * @see Rb_Group
 *
 * @author Olivier CYSSAU
 *
 */
class Rb_User implements Rb_Acl_Role_Interface{

	/**
	 *
	 * @var integer
	 */
	protected $user_id = 0; //(integer)

	/**
	 *
	 * @var string
	 */
	protected $user_name = 0; //(string)

	/**
	 *
	 * @var array
	 */
	protected $core_props = array(); //(array)

	/**
	 *
	 * @var boolean
	 */
	protected $isActive = false; //(bool)

	/**
	 *
	 * @var integer	timestamp
	 */
	protected $lastLogin = false; //(integer timestamp)
	//protected $email = false; //(string)

	/**
	 * @var Rb_Wildspace
	 */
	protected $_wildspace = false; //(object)

	/**
	 *
	 * @var Rb_User_Preference
	 */
	protected $_preference = false; //(object)

	/**
	 *
	 * @var Rb_User
	 */
	protected static $_currentuser = false; //(object)

	protected static $_users = false; //(false or array) associatvie array of users of ranchbe where key = user_id
	protected static $_ousers = false; //(false or array) associatvie array User object
	protected static $_groups = false; //(false or array) associatvie array of groups of the current User where key = user_id

	//----------------------------------------------------------------------------
	public function __construct($id=0){
		if($id){
			$this->user_id = (int) $id;
			$this->core_props = Rb_User::getInfos($id);
			Ranchbe::getAcl();
		}
	}//End of method

	//----------------------------------------------------------------------------
	/** Factory method
	 * If object $id is yet existing, so this static method return it, else construct
	 * and return a new object
	 *
	 * @param integer
	 * @return this
	 *
	 */
	public static function get($id){
		if(!self::$_ousers[$id])
		self::$_ousers[$id] = new Rb_User($id);
		return self::$_ousers[$id];
	}//End of method

	//----------------------------------------------------------------------------
	/** defintion of method from Zend_Acl_Role_Interface. See Zend documentation.
	 * this class is a role
	 *
	 * return roleId(string). roleId is used by Zend_Acl to identify the roles. In fact
	 * this method add a prefix to the current id because id of a group can be = to id of a User
	 *
	 * @return string
	 *
	 */
	public function getRoleId(){
		return self::getStaticRoleId( $this->getId() );
	}//End of method

	//----------------------------------------------------------
	/** Returns the string identifier from specified $id
	 *
	 * @param string
	 * @return string
	 */
	public static function getStaticRoleId($id){
		return $id;
	}//End of method

	//----------------------------------------------------------------------------
	/** Return the id of current object
	 * 
	 * @return integer
	 */
	public function getId(){
		return $this->user_id;
	}//End of method

	//----------------------------------------------------------------------------
	/** Set the User name
	 * 
	 * @param string
	 */
	public function setUsername($username){
		return $this->setProperty('handle', $username);
	}//End of method

	//----------------------------------------------------------------------------
	/** Get the User name
	 * 
	 * @return string
	 */
	public function getUsername(){
		return $this->getProperty('handle');
	}//End of method

	//----------------------------------------------------------------------------
	/** Alias of getUsername
	 */
	public function getName(){
		return $this->getUsername();
	}//End of method

	//----------------------------------------------------------------------------
	/** Set the property
	 *
	 * the first parameter defined the name of property, the second his value.
	 * You can input any strings as input or value but this property must be created
	 * in the current object table.
	 * @param string
	 * @param string
	 * @return this
	 */
	public function setProperty($property, $value){
		switch($property){
			case 'passwd':
				$this->core_props[$property] = md5($value);
				break;
			default:
				$this->core_props[$property] = $value;
				break;
		}
		return $this;
	}//End of method

	//----------------------------------------------------------------------------
	/** Get the value of the property
	 *
	 * @param string
	 * @return variant
	 */
	public function getProperty($property){
		return $this->core_props[$property];
	}//End of method

	//----------------------------------------------------------------------------
	/** Return an array of all properties for current object
	 * 
	 * @return array
	 *
	 */
	public function getProperties(){
		if(!isset($this->core_props)) return false;
		else return $this->core_props;
	}//End of method

	//----------------------------------------------------------------------------
	/** Get and set User object with current User datas
	 *
	 * to get current User id :
	 * Rb_User::getCurrentUser()->getId();
	 * this method return always the same instance of class Rb_User. Its a singlton method
	 * so you can call it many time in your code without overload server.
	 * 
	 * @return Rb_User
	 *
	 */
	public static function getCurrentUser(){
		if( self::$_currentuser ) return self::$_currentuser;
		$storage = Zend_Auth::getInstance()->getStorage()->read(); //Get info from session
		//@todo: revoir ce qui suit , creer un identifiant ineltarable pour anonymous
		if(!$storage) {
			$storage['user_id']  = 2147483647; //(int 11) voir les entier php et les limitations de taille en fonction de l'OS
			$storage['user_name']= 'anonymous';
		}
		self::$_currentuser = new Rb_User( $storage['user_id'] );
		self::$_currentuser->setUsername( $storage['user_name'] );
		self::$_currentuser->setProperty( 'email', $storage['email'] );
		return self::$_currentuser;
	}//End of method
	
	/** Set the current user. You can too directly call getCurrentUser if session are operate
	 * 
	 * @param Rb_User $user
	 * @return Rb_User
	 */
	public static function setCurrentUser(Rb_User $user){
		return self::$_currentuser = $user;
	}//End of method
	

	//----------------------------------------------------------------------------
	/** Get all users recorded
	 *
	 * return an array of id of all users of ranchbe
	 * implements a simili singleton pattern, so the db is query only at first call,
	 * after return the content of the static var $_users. So becarefull if you create a new
	 * User and use this method after for second time.
	 * 
	 * @return array
	 * 
	 */
	public static function getUsers(){
		if( !self::$_users )
		self::$_users = Ranchbe::getAuthAdapter()->getUsers();
		return self::$_users;
	}//End of method

	//----------------------------------------------------------------------------
	/** Get infos about a User
	 *
	 * return an array with all properties of object id.
	 * implements a simili singleton pattern, so the db is query only at first call,
	 * after return the content of a static var.
	 * 
	 * @param integer
	 * @return array
	 */
	public static function getInfos($user_id){
		if( self::$_users[$user_id] )
		return self::$_users[$user_id];
		return Ranchbe::getAuthAdapter()->getInfos($user_id);
	}//End of method

	//----------------------------------------------------------------------------
	/** Get all groups of the current User
	 *
	 * @return array : an associative array where key = group_id and value = group_id too!
	 */
	public function getGroups(){
		if( self::$_groups[$this->user_id] ) return self::$_groups[$this->user_id];
		return self::$_groups[$this->user_id] = Ranchbe::getAuthAdapter()->getGroups($this->user_id);
	}//End of method

	//----------------------------------------------------------------------------
	/** Assign a group to the current User. the method saveGroup must be call after
	 *
	 * Return the group_id
	 * @param Rb_Group
	 * @return integer
	 */
	public function setGroup(Rb_Group &$group){
		return self::$_groups[$this->user_id][$group->getId()] = $group->getId();
	}//End of method

	//----------------------------------------------------------------------------
	/** Unassign a group to the current User. the method saveGroup must be call after
	 *
	 * @param Rb_Group
	 * @return void
	 */
	public function unsetGroup(Rb_Group &$group){
		unset( self::$_groups[$this->user_id][$group->getId()] );
	}//End of method

	//----------------------------------------------------------------------------
	/** Get the wildspace object of the current User
	 *
	 * @return Rb_Wildspace
	 */
	public function getWildspace(){
		if($this->_wildspace) return $this->_wildspace;
		return $this->_wildspace = new Rb_Wildspace($this);
	}//End of method

	//----------------------------------------------------------------------------
	/** Suppress the current User
	 * 
	 * @return boolean
	 *
	 */
	public function suppress(){
		if($this->user_id = 22) return false; //can not suppress admin User
		if( Ranchbe::getAuthAdapter()->suppress($this->user_id) ){
			unset( self::$_ousers[$this->user_id] );
			unset( self::$_users[$this->user_id] );
			unset( self::$_groups[$this->user_id] );
			return true;
		}
		return false;
	}//End of method

	//----------------------------------------------------------------------------
	/** Save the current User in database
	 * 
	 * @return integer
	 */
	public function save(){
		return $this->user_id = Ranchbe::getAuthAdapter()->save($this);
	}//End of method

	//----------------------------------------------------------------------------
	/** Save the current User groups in database
	 *
	 * @return void
	 */
	public function saveGroups(){
		Ranchbe::getAuthAdapter()->saveGroups($this);
	}//End of method

	//------------------------------------------------------------------------
	/** Get the userPrefernce object of the current User
	 *
	 * @return Rb_User_Preferences
	 */
	public function getPreference(){
		if ( $this->_preference === false ) {
			//require_once('core/User/preferences.php'); //Class to manage User preferences
			$this->_preference = new Rb_User_Preferences($this->user_id);
			//$this->_preference->setAllowUserPrefs(ALLOW_USER_PREFS);
		}
		return $this->_preference;
	} // End of method

	//----------------------------------------------------------------
	/* To get user_id from a User name
	 * 
	 *  @return integer
	 */
	public static function getUserIdFromName($handle){
		return Ranchbe::getAuthAdapter()->getUserIdFromName($handle);
	} // End of method

} //End of class
