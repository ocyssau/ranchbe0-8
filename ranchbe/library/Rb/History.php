<?php
//------------------------------------------------------------------------------
/*! \brief Class for manage History to follow the life of all ranchBE components.
 */
class Rb_History extends Rb_Object_Permanent{

	/**
	 * 
	 * @var array
	 */
	protected $_comments = array(); //comments of History action
	
	/**
	 * Instance of object to log
	 * 
	 * @var Rb_Object_Permanent
	 */
	protected $_recorded = false;
	
	/**
	 * 
	 * @param Rb_Object_Permanent $recorded
	 * @return void
	 */
	public function __construct(Rb_Object_Permanent &$recorded){
		$this->_dao = new Rb_Dao( Ranchbe::getDb() );
		if(!$this->_dao->setTable($recorded->getDao()->getTableName('history'))){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(),
			tra('History table is not set'));
		}
		$this->_dao->setKey('histo_order', 'primary_key');
		$this->_dao->setSequence('history_seq');
		$this->_recorded =& $recorded;
	}//End of method

	//-------------------------------------------------------------------------
	/**
	 * init and clean current registry
	 * 
	 * @return void
	 */
	public static function initRegistry(){
		self::$_registry = array();
	}

	//------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_create()
	 */
	protected function _create(){
		return $this->_dao->create($this->getProperties());
	}//End of method

	//------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_update()
	 */
	protected function _update(){
		return $this->_dao->update($this->getProperties(), $this->getId());
	}//End of method

	//------------------------------------------------------------------------
	/**
	 * 
	 * @return string
	 */
	protected function _getComment(){
		if( is_array( $this->_comments ) ){
			return implode( ';', $this->_comments );
		}
		else return '';
	}//End of method

	//------------------------------------------------------------------------
	/**
	 * Remove history records
	 * $params is use for create a query for select all records to suppress
	 * 
	 * @param array $params (exact_find=>"word to find in field", find_field=>"field where to search word")
	 * @return boolean
	 */
	protected function _removeHistory( array $params ){
		return $this->_dao->suppressQuery($params);
	}//End of method

	//------------------------------------------------------------------------------
	/**
	 * Write History data in History table.
	 * Return Integer histo_order if success, else return false
	 * 
	 * @param	array	$data 	keys/values are fields/values of the History record to write.
	 * @return	integer			id of new histo_order
	 */
	public function write($data){
		$this->setProperty('action_date', time());
		$this->setProperty('action_by', Rb_User::getCurrentUser()->getUsername());
		$this->setProperty('comment', $this->_getComment());
		foreach($data as $name=>$value){
			$this->setProperty($name, $value);
		}
		$histo_order = $this->_create();
		$this->_comments = array(); //clean comment
		$this->core_props = array(); //clean properties
		return $histo_order;
	}//End of method

	//------------------------------------------------------------------------------
	/**
	 * Update record history
	 * 
	 * @param array 	$data
	 * @param integer 	$histo_order
	 * @return integer	id of histo_order
	 */
	public function update($data = array(), $histo_order){
		$histo_order = $this->_dao->update($data, $histo_order);
		$this->_comments = array(); //clean comment
		$this->core_props = array(); //clean properties
		return $histo_order;
	}//End of method

	//------------------------------------------------------------------------
	/**
	 * Method for get all History record filter by the option set in $params
	 * 
	 * @param array $params		See parameters function of getQueryOptions()
	 * @return array
	 */
	public function getAllHistory ($params){
		return $this->_dao->getAllBasic($params);
	}//End of method

	//------------------------------------------------------------------------
	/**
	 * Get history of a particular object record filter by the option set in $params
	 * 
	 * @param integer $id		id of the object to list history
	 * @param array $params		See parameters function of getQueryOptions()
	 * @return array
	 */
	public function getHistory($id , array $params = array() ){
		$params['exact_find'][$this->_recorded->getDao()->getFieldName('id')] = $id;
		return $this->_dao->getAllBasic($params);
	}//End of method

	//------------------------------------------------------------------------
	/**
	 * Remove History records from the histo_order id
	 * 
	 * @param integer $histo_order
	 * @return boolean
	 */
	public function suppressHistory( $histo_order ){
		$params['exact_find']  = array($this->_dao->getPrimayKey()=>$histo_order);
		return $this->_removeHistory($params);
	}//End of method

	//------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#suppress()
	 */
	public function suppress(){
		return $this->suppressHistory($this->getId());
	}//End of method

	//------------------------------------------------------------------------
	/**
	 * Add a comment to history
	 * 
	 * @param string $comment
	 * @return void
	 */
	public function setComment( $comment ){
		$this->_comments[] = $comment;
	}//End of method

	//------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#setProperty($property_name, $property_value)
	 */
	public function setProperty($property_name, $property_value){
		$this->core_props[$property_name] = $property_value;
		return $this;
	}//End of method

	//------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#getProperty($property_name)
	 */
	public function getProperty($property_name){
		return $this->core_props[$property_name];
	}//End of method

} //End of class
