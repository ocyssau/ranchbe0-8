<?php

if(!function_exists ('symlink')){
	/**
	 *
	 * @param string $from The link target, it is the original file or source file
	 * @param string $to The link name, it is the new name for file $target
	 * @return boolean
	 */
	function symlink($from, $to){
		$from = str_replace('\\', '/', realpath($from));
		$to = str_replace('\\', '/', $to);
		$cmd =  Ranchbe::getConfig()->cmd->symlink;
		if(!$cmd) return false;
		$cmd = str_replace('%from%', $from, $cmd);
		$cmd = str_replace('%to%', $to, $cmd);
		system($cmd, $retval);
		//var_dump(!($retval), $cmd, $target, $path);die;
		return !($retval);
	}
}

/** Symbolic link on filesystem
 * 
 * @author Administrateur
 *
 */
class Rb_Symlink extends Rb_Fslink_Abstract{

	//----------------------------------------------------------
	/** Create a new link
	 *
	 * @param string $from The link target, it is the original file or source file
	 * @param string $to The link name, it is the new name for file $target
	 * @return boolean
	 */
	public static function create($from, $to){
		//if(!Rb_Filesystem::limitDir($to)) return false;
		//if(!Rb_Filesystem::limitDir($from)) return false;
		if(symlink($from, $to)) return new self($to);
		else return false;
	}//End of method
	
	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Fslink/Rb_Fslink_Abstract#suppress()
	 */
	public function suppress(){
		Ranchbe::getError()->push(Rb_Error::INFO, array('file'=>$this->_path),
                                              'try to suppress symlink %file%');
		if(!$this->test()) return false;
		if(!Rb_Filesystem::limitDir($this->_path)) return false;
		if(!unlink($this->_path)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('file'=>$this->_path),
                                        'error during suppressing symlink %file%');
			return false;
		}
		Ranchbe::getError()->push(Rb_Error::INFO, array('file'=>$this->_path),
                                             'symlink %file% has been suppressed');
		return true;
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Fslink/Rb_Fslink_Abstract#test()
	 */
	public function test(){
		if(!is_link($this->_path)){
			Ranchbe::getError()->push(Rb_Error::INFO, array('file'=>$this->_path),
                                               '%file% is not a symbolic link');
			return false;
		}else return true;
	}//End of method

} //End of class
