<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*! \brief Iteration is created on each reposit update
 *
 */
class Rb_Docfile_Iteration{

	//-------------------------------------------------------
	/** Get iterations of docfile
	 * Returned array is sort by Iteration. older Iteration first to last created
	 *
	 * @param Rb_Docfile $docfile
	 * @param Bool $object
	 *				if true return array where values are Rb_Docfile,
	 *     				else values are array of docfiles properties
	 * @return Array
	 */
	public static function getIterations(Rb_Docfile &$docfile,
	$object = false){
		$query = 'SELECT * FROM '.$docfile->getDao()->getTableName('object')
		.' WHERE file_bid = \''.$docfile->getProperty('file_bid').'\''
		.' AND file_version = '.$docfile->getProperty('file_version')
		.' AND file_life_stage = 3'
		.' ORDER BY file_version ASC';
		$res = Ranchbe::getDb()->execute($query);
		if($res === false ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), Ranchbe::getDb()->ErrorMsg());
			return false;
		}
		if($object){
			$i=0;
			while($row = $res->FetchRow() ){
				$result[$i] = Rb_Docfile::get($docfile->getSpaceName(), $row['file_id']);
				$i++;
			}
			return $result;
		}else{
			return $res->GetArray();
		}
	}//End of method

	//-------------------------------------------------------
	/** Get one Iteration of docfile
	 *
	 * @param Rb_Docfile $docfile
	 * @param Integer $iteration_id
	 * @param Bool $object
	 * 		if true return array where values are Rb_Docfile,
	 *     		else values are array of docfiles properties
	 * @return Rb_Docfile
	 */
	public static function getIteration(Rb_Docfile &$docfile, $iteration_id, $object = false){
		$query = 'SELECT file_id FROM '.$docfile->getDao()->getTableName('object')
		.' WHERE file_bid = \''.$docfile->getProperty('file_bid').'\''
		.' AND file_version = '.$docfile->getProperty('file_version')
		.' AND file_iteration = '.$iteration_id
		.' AND file_life_stage = 3';
		$res = Ranchbe::getDb()->GetOne($query);
		if($res === false ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), Ranchbe::getDb()->ErrorMsg());
			return false;
		}
		if($object){
			return Rb_Docfile::get($docfile->getSpaceName(), $res);
		}else{
			return $res;
		}
	}//End of method

	//-------------------------------------------------------
	/** Create a Iteration of the $docfile
	 *
	 * @param Rb_Docfile $from_docfile original docfile from create Iteration
	 * @param Rb_Fsdata $from_fsdata data to use to create new docfile
	 * @param integer $version_id
	 * @return Rb_Docfile
	 */
	public static function create(Rb_Docfile &$from_docfile,
									Rb_Fsdata $from_fsdata,
									$version_id=0 ){

		$back_iteration_number = Ranchbe::getConfig()->iteration->max_back_number;
		//Dont keep Iteration if 0
		if($back_iteration_number === 0) return $from_docfile;
		$iterationId = Rb_Docfile_Iteration::getLastIid($from_docfile) + 1;
		if(!$version_id) $version_id = $from_docfile->getProperty('file_version');

		if(!$from_fsdata->isExisting()){
			Ranchbe::getError()->push(Rb_Error::ERROR,
			array('element'=>$from_docfile->getNumber()),
                                  'none valid fsdata for %element%');
			return false;
		}

		$to_reposit =& Rb_Reposit::getActiveReposit($from_docfile->getSpace());
		$dstfile = Rb_Reposit::getBasePath($to_reposit,$iterationId)
					.'/'.$from_docfile->getProperty('file_name');
		$srcfile = $from_fsdata->getProperty('file');

		if( $dstfile == $srcfile ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('name'=>$dstfile ),
                      'you try to create a copy of docfile %name% on himself' );
			return false;
		}

		$backupfsdata = $from_docfile->getFsdata();
		$from_docfile->setFsdata($from_fsdata); //to create data from specifed data and not from data of docfile
		$to_docfile = $from_docfile->copy($dstfile, $from_docfile->getFather()->getId(), false);

		if(!$to_docfile) return false;
		$to_docfile->setIterationId( $iterationId );
		$to_docfile->setVersionId( $version_id );
		$to_docfile->setReposit( $to_reposit );
		$to_docfile->setProperty( 'file_bid', $from_docfile->getProperty('file_bid'));
		$to_docfile->setProperty( 'file_checkout_by', $from_docfile->getProperty('file_checkout_by'));
		$to_docfile->setProperty( 'file_checkout_date', $from_docfile->getProperty('file_checkout_date'));
		$to_docfile->setProperty( 'file_access_code', 1);
		//$from_docfile->unsetFsdata(); //cancel specified fsdata
		$from_docfile->setFsdata($backupfsdata);

		return $to_docfile;
	}//End of method

	//-------------------------------------------------------
	/** Return the last Iteration identifier
	 *
	 * @param Rb_Docfile $docfile
	 * @return Integer
	 */
	public static function getLastIid(Rb_Docfile &$docfile){
		$query = 'SELECT MAX(file_iteration)'
		.' FROM '.$docfile->getDao()->getTableName('object')
		.' WHERE file_name = \''.$docfile->getNumber().'\''
		.' GROUP BY file_name';
		$one = Ranchbe::getDb()->GetOne($query);
		return $one;
	}//End of method

	//-------------------------------------------------------
	/** Lock the docfile and mark it as Iteration state
	 *
	 * @param Rb_Docfile $docfile
	 * @return Rb_Docfile
	 */
	public static function setAsIteration(Rb_Docfile &$docfile){
		$docfile->setProperty( 'file_access_code', 170 );
		$docfile->setProperty( 'file_life_stage', 3 );
		$docfile->setProperty( 'file_checkout_by', NULL );
		$docfile->setProperty( 'file_checkout_date', NULL );
		//$docfile->save();
		return $docfile;
	}//End of method

	//-------------------------------------------------------
	/** Remove obsolete Iteration
	 *
	 * @param Rb_Docfile $docfile
	 * @return Rb_Docfile
	 */
	public static function deleteObsolete(Rb_Docfile &$docfile){
		$back_iteration_number = Ranchbe::getConfig()->iteration->max_back_number;
		if($back_iteration_number  < 1) return $docfile;
		$iteration_id = $docfile->getProperty('iteration_id');
		if($iteration_id - $back_iteration_number > 0){
			$iterations = self::getIterations($docfile, true); //Get iterations
			if($iterations && (count($iterations) >= $back_iteration_number)){
				for($i=0; $i <= (count($iterations) - $back_iteration_number); $i++){ //<= because current Iteration is not yet create, so real Iteration number is current + 1
					Ranchbe::getError()->notice('Suppress Iteration %iteration% of file %name%', array(
								'name'=>$iterations[$i]->getName(),
                    			'iteration'=>$iterations[$i]->getProperty('iteration_id')) );
					$iterations[$i]->removeFile(true, false);
				}
			}
		}
		return $docfile;
	}//End of method

} //End of class
