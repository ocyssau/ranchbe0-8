<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
DROP TABLE `workitem_doc_file_role`;
CREATE TABLE `workitem_doc_file_role` (
  `link_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `role_id` int(1) NOT NULL,
  PRIMARY KEY  (`link_id`),
  UNIQUE KEY `workitem_doc_file_role_uniq1` (`file_id`,`role_id`)
) ENGINE=InnoDB;

ALTER TABLE `workitem_doc_file_role`
  ADD CONSTRAINT `FK_workitem_doc_file_role_1` FOREIGN KEY (`file_id`)
  REFERENCES `workitem_doc_files` (`file_id`) ON DELETE CASCADE;
*/

//require_once('core/dao/abstract.php');

//==============================================================================
/*! \brief define Role of a docfile for the document. Role can be MAIN, VISU, PICTURE, 
    or ANNEX
*  A MAIN docfile is minimum document requierement.
*  A MAIN docfile may be use to visualisation or a other docfile may be defined to the Role
*  A PICTURE may be used for VISU
*  All type may be ANNEX
*  
*/
class Rb_Docfile_Role extends Rb_Dao_Abstract {

  protected $_id = 0;
  protected $error_stack=false;

  protected $core_props=array();

  protected $OBJECT_TABLE='';
  protected $FIELDS_MAP_ID=-1;
  protected $FIELDS_MAP_NUM='';

  protected static $_registry = array(); //(array) registry of constructed objects

  const MAIN = 0;
  const VISU = 1;
  const PICTURE = 2;
  const ANNEX = 3;

  function __construct(Rb_Space &$space, $link_id = 0){
    $this->dbranchbe =& Ranchbe::getDb();
    $this->error_stack =& Ranchbe::getError();
    $this->space =& $space;
    
    $this->OBJECT_TABLE = $this->space->getName().'_doc_file_role';
    $this->FIELDS_MAP_ID = 'link_id';
    
    $this->LEFT_OBJECT_TABLE = $this->space->getName().'_doc_files';
    $this->LEFT_FIELDS_MAP_ID = 'file_id';
    $this->LEFT_FIELDS_MAP_NUM = 'file_name';
    
    $this->_init($link_id);
  }//End of method

  //-------------------------------------------------------------------------
  /*! \brief factory method with singleton pattern
  */
  public static function get( $space_name, $id = -1){
    if($id < 0) $id = -1; //forced to value -1

    if($id == 0)
      return new Rb_Docfile_Role(Rb_Space::get($space_name), 0);

    if( !self::$_registry[$space_name][$id] ){
      self::$_registry[$space_name][$id] = new Rb_Docfile_Role(Rb_Space::get($space_name), $id);
    }

    return self::$_registry[$space_name][$id];
  }//End of method

  //-------------------------------------------------------------------------
  /*! \brief init current instance, populate properties from database,
  *       init access to database parameters.
  * Return array.
  * 
  */
  protected function _init($id){
    if( $id > 0 ){
      $this->_id = (int) $id;
      $this->core_props = $this->getBasicInfos($this->_id);
    }else if( $id < 0 ){
      $this->_id = -1;
    }else{
      $this->_id = 0;
    }
    return true;
  } //End of method

  //-------------------------------------------------------------------------
  public function getDocfileId(){
    return $this->core_props['file_id'];
  }//End of method

  //-------------------------------------------------------------------------
  public function getRoleId(){
    return $this->core_props['role_id'];
  }//End of method

  //-------------------------------------------------------------------------
  public function setRole($id){
    $this->core_props['role_id'] = $id;
    return $this;
  }//End of method

  //-------------------------------------------------------------------------
  public function setFile($id){
    $this->core_props['file_id'] = $id;
    return $this;
  }//End of method

  //-------------------------------------------------------------------------
  public function getId(){
    return $this->_id;
  }//End of method

  //-------------------------------------------------------------------------
  public function save($notify=true, $history=true){
    if($this->_id < 0){
      return false;
    }else
    if($this->_id == 0){
      $this->_basicCreate($this->core_props);
      if($this->_id){
        return $this;
      }else return false;
    }else
    if($this->_id > 0){
      if( _basicUpdate($this->core_props, $this->_id) ){
        return $this;
      }else return false;
    }
  }//End of method

  //-------------------------------------------------------------------------
  public function suppress($file_id,$role_id){
    $query = 'DELETE FROM '.$this->OBJECT_TABLE
             .' WHERE '.$this->OBJECT_TABLE.'.file_id = \''.$file_id.'\''
             .' AND '.$this->OBJECT_TABLE.'.role_id = \''.$role_id.'\'';
    //Execute the query
    if(!$this->dbranchbe->Execute($query)){
      $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query),
                               $this->dbranchbe->ErrorMsg());
      return false;
    }
    return true;
  }//End of method

  //-------------------------------------------------------------------------
  public static function copy(Rb_Docfile &$from_docfile, Rb_Docfile &$to_docfile){
    $roles = Rb_Docfile_Role::getRoles($from_docfile);
    foreach($roles as $role_id){
      $role = new Rb_Docfile_Role($to_docfile->getSpace(), 0);
      $role->setFile($to_docfile->getId());
      $role->setRole($role_id);
      $role->save();
    }
  }//End of method

  //-------------------------------------------------------------------------
  /** Get roles for docfile
  *   Return array where key = link_id and value = role_id
  */
  public static function getRoles(Rb_Docfile &$docfile){
    $role =& self::get( $docfile->getSpaceName() );
    $rs = $role->getAllBasic(array('exact_find'=>array(
                                              $role->getTableName().'.file_id'=>
                                                               $docfile->getId()
                                             ),
                                   'getRs'=>true,
                              ));
    $ret = array();
    if ($rs){
      while (!$rs->EOF) {
        $ret[(int)$rs->fields['link_id']] = (int) $rs->fields['role_id'];
        $rs->MoveNext();
      }
    }
    return $ret;
  }//End of method

  //-------------------------------------------------------------------------
  /** Get all docfile and his Role
  *   Return array where key = link_id and value = role_id
  */  
  public function getAll($params){
    $params['with'][] = array('type'=>'RIGHT OUTER',
                              'table'=>$this->LEFT_OBJECT_TABLE,
                              'col'=>$this->LEFT_FIELDS_MAP_ID);
    if(isset($params['select'])){
      $t = array();
      foreach($params['select'] as $select){
        if($select == 'role_id' || $select == 'link_id')
          $t[] = $this->OBJECT_TABLE.'.'.$select;
        else
          $t[] = $this->LEFT_OBJECT_TABLE.'.'.$select;
      }
      $params['select'] = $t;
    }
    $params['where'][] = 'file_life_stage < 3';
    return $this->getAllBasic($params);
  }//End of method

} //End of class


