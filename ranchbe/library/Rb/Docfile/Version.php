<?php
// +----------------------------------------------------------------------+
// | This source file is subject to Version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*! \brief
 *
 */
class Rb_Docfile_Version{

	//-------------------------------------------------------
	/*!\brief
	 * Get versions of docfile
	 * return array of objects or false
	 * $object (bool) if true return an array of Rb_Docfile objects, else return array
	 */
	public function getVersions(Rb_Docfile $docfile, $object = false){
		$query = 'SELECT * FROM '.$docfile->getDao()->getTableName('object')
		.' WHERE file_bid = '.$docfile->getProperty('file_bid')
		.' AND file_life_stage < 3';

		$res = Ranchbe::getDb()->execute($query);
		if($res === false ){
			Ranchbe::getError()->errorDb(Ranchbe::getDb()->ErrorMsg(), $query);
			return false;
		}
		if($object){
			$i=0;
			while($row = $res->FetchRow() ){
				$result[$i] = Rb_Docfile::get($docfile->getSpaceName(), $row['file_id']);
				$i++;
			}
			return $result;
		}else{
			return $res->GetArray();
		}

	}//End of method

	//-------------------------------------------------------
	/*!\brief create a Version of the $docfile
	 *  return Rb_Docfile
	 *  \param Rb_Docfile original docfile from create Version
	 *  \param Rb_Fsdata data to use to create new docfile
	 */
	public static function create(Rb_Docfile &$from_docfile,
	Rb_Fsdata &$from_fsdata,
	Rb_Container &$container = null
	){

		$iterationId = Rb_Docfile_Iteration::getLastIid($from_docfile) + 1;
		$versionId = Rb_Docfile_Version::getLastVid($from_docfile) + 1;

		if(!$from_fsdata->isExisting()){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$from_docfile->getNumber()),
                                  'none valid fsdata for %element%');
			return false;
		}

		$to_reposit =& Rb_Reposit::getActiveReposit($from_docfile->getSpace());
		$dstfile = Rb_Reposit::getBasePath($to_reposit,$iterationId).
    			'/'.$from_docfile->getProperty('file_name');
		$srcfile = $from_fsdata->getProperty('file');

		if( $dstfile == $srcfile ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('name'=>$dstfile ),
                      'you try to create a copy of docfile %name% on himself' );
			return false;
		}

		$backupfsdata = $from_docfile->getFsdata();
		$from_docfile->setFsdata($from_fsdata); //to create data from specifed data and not from data of docfile
		$to_docfile = $from_docfile->copy($dstfile, $from_docfile->getFather()->getId(), false);
		
		if(!$to_docfile) return false;
		$to_docfile->setIterationId($iterationId);
		$to_docfile->setVersionId($versionId);
		$to_docfile->setReposit( $to_reposit );
		$to_docfile->setProperty('file_bid', $from_docfile->getProperty('file_bid'));
		$to_docfile->setProperty('file_checkout_by', null);
		$to_docfile->setProperty('file_checkout_date', null);
		$to_docfile->setProperty( 'file_access_code', 0 );
		$to_docfile->setProperty( 'file_life_stage', 1 );
		//$to_docfile->unsetFsdata(); //cancel specified fsdata
		$from_docfile->setFsdata($backupfsdata);
		return $to_docfile;
	}//End of method

	//-------------------------------------------------------
	/*! \brief return the last Version identifier
	 */
	public static function getLastVid(Rb_Docfile &$docfile){
		$query = 'SELECT MAX(file_version)'
		.' FROM '.$docfile->getDao()->getTableName('object')
		.' WHERE file_bid = \''.$docfile->getProperty('file_bid').'\''
		.' GROUP BY file_bid';
		$one = Ranchbe::getDb()->GetOne($query);
		return $one;
	}//End of method

}//End of class
