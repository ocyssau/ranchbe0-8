<?php

/*
CREATE TABLE `import_history` (
  `import_order` int(11) NOT NULL default '0',
  `container_id` int(11) NOT NULL default '0',
  `description` varchar(64) default NULL,
  `state` varchar(32) default NULL,
  `import_date` int(11) default NULL,
  `import_by` int(11) default NULL,
  `errors_report` text,
  `logfile` text,
  `listfile` text,
  `file_name` varchar(128) NOT NULL default '',
  `file_extension` varchar(32) default NULL,
  `file_mtime` int(11) default NULL,
  `file_size` int(11) default NULL,
  `file_md5` varchar(128) NOT NULL default '',
  PRIMARY KEY  (`import_order`)
) ENGINE=InnoDB;
*/

//------------------------------------------------------------------------------
/**! \brief History of imported packages. file_* var refere to package file properties
 */
class Rb_Import_History extends Rb_Object_Permanent {
	protected $OBJECT_TABLE = 'import_history';
	protected $FIELDS_MAP_ID = 'import_order';
	
	//Fields:
	protected $FIELDS_MAP_NUM = 'file_name'; //(String)Name of the field to define the number
	protected $FIELDS_MAP_NAME = 'file_name'; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC = 'description'; //(String)Name of the field where is store the description
	protected $FIELDS_MAP_STATE = 'state'; //(String)Name of the field where is store the state
	protected $FIELDS_MAP_FATHER; //(String)Name of the field where is store the id of the father
	
	protected $_container = false;
	protected $_reposit = false;
	protected $_preCreateEventName = 'import_history_pre_create';
	protected $_postCreateEventName = 'import_history_post_create';
	protected $_preUpdateEventName = 'import_history_pre_update';
	protected $_postUpdateEventName = 'import_history_post_update';
	protected static $_registry = array ();
	protected static $_useRegistry = true;
	
	/**
	 * 
	 * @var Rb_Import_Log
	 */
	protected $_log = false;
	
	/**
	 * 
	 * @var Rb_Import_Log
	 */
	protected $_contentlog = false;
	
	/**
	 * 
	 * @var Rb_Import_Log
	 */
	protected $_errorlog = false;
	
	function __construct(Rb_Space &$space, $id = 0) {
		$this->space = & $space;
		$this->_init ( $id );
	} //End of method
	

	//-------------------------------------------------------------------------
	/**! \brief init the properties
	 * 
	 */
	protected function _init($id) {
		//$this->OBJECT_TABLE  = $this->space->getName().'_import_history';
		$this->FIELDS_MAP_FATHER = $this->space->CONT_FIELDS_MAP_ID;
		$this->_dao = new Rb_Dao ( Ranchbe::getDb () );
		$this->_dao->setTable ( $this->OBJECT_TABLE );
		$this->_dao->setKey ( $this->FIELDS_MAP_ID, 'primary_key' );
		$this->_dao->setKey ( $this->FIELDS_MAP_NUM, 'number' );
		$this->_dao->setKey ( $this->FIELDS_MAP_NAME, 'name' );
		$this->_dao->setKey ( $this->FIELDS_MAP_DESC, 'description' );
		$this->_dao->setKey ( $this->FIELDS_MAP_STATE, 'state' );
		$this->_dao->setKey ( $this->FIELDS_MAP_FATHER, 'father' );
		$this->_dao->setSequence ( $this->SEQ_NAME );
		$now = time ();
		$this->setProperty ( 'import_date', $now );
		$this->setProperty ( 'import_by', Rb_User::getCurrentUser ()->getId () );
		$this->setProperty ( 'state', 'init' );
		return parent::_init ( $id );
	} //End of method
	

	//--------------------------------------------------------------------
	/**!\brief create a new database record for object.
	 *   Return the id if no errors, else return FALSE
	 *
	 */
	protected function _create() {
		if ($this->_id !== 0) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('id' => $this->_id ), tra ( 'can not performed this action on object id %id%' ) );
			trigger_error ( 'can not performed this action on object id ' . $this->_id, E_USER_ERROR );
			return false;
		}
		//Create a basic object
		$this->_id = $this->_dao->create ( $this->getProperties () );
		if ($this->_id) {
			$this->isSaved = true;
			$this->_saveRegistry ();
			return $this->_id;
		} else {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('element' => $this->getNumber () ), tra ( 'cant create the recordfile %element%' ) );
			return $this->_id = 0;
		}
	} //End of method
	

	//-------------------------------------------------------
	/**! \brief Update the record
	 *
	 */
	protected function _update() {
		if ($this->_id < 1) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('id' => $this->_id ), tra ( 'can not performed this action on object id %id%' ) );
			trigger_error ( 'can not performed this action on object id ' . $this->_id , E_USER_WARNING);
			return false;
		}
		if (! $this->_dao->update ( $this->getProperties (), $this->_id ))
			return false;
		$this->isSaved = true;
		return true;
	} //End of method
	

	//-------------------------------------------------------------------------
	/**! \brief save current instance in registry
	 */
	protected function _saveRegistry() {
		if (self::$_useRegistry === true) {
			if ($this->_id > 0)
				self::$_registry [$this->space->getName ()] [$this->_id] = & $this;
		}
	}
	
	//-------------------------------------------------------------------------
	/**! \brief init and clean current registry
	 */
	public static function initRegistry() {
		self::$_registry = array ();
	}
	
	//-------------------------------------------------------------------------
	/**! \brief return a instance of object.
	 */
	public static function get($space_name, $id = -1) {
		if ($id < 0)
			$id = - 1; //forced to value -1
		if ($id == 0)
			return new self ( Rb_Space::get ( $space_name ), 0 );
		if (! self::$_registry [$space_name] [$id] && (self::$_useRegistry || $id == - 1)) { //always use registry for -1 object
			return self::$_registry [$space_name] [$id] = new self ( Rb_Space::get ( $space_name ), $id );
		} else if (self::$_useRegistry === false) {
			return new self ( Rb_Space::get ( $space_name ), $id );
		}
		return self::$_registry [$space_name] [$id];
	} //End of method
	

	//----------------------------------------------------------
	/**! \brief Get the property of the recordfile by the property name. init() must be call before.
	 * 
	 * \param Variant
	 */
	public function getProperty($property_name) {
		switch ($property_name) {
			case 'id' :
			case 'file_id' :
				return $this->_id;
				break;
			
			case 'number' :
				$property_name = $this->FIELDS_MAP_NUM;
				break;
			
			case 'name' :
				$property_name = $this->FIELDS_MAP_NAME;
				break;
			
			case 'description' :
				$property_name = $this->FIELDS_MAP_DESC;
				break;
			
			case 'state' :
				$property_name = $this->FIELDS_MAP_STATE;
				break;
			
			case 'father_id' :
				$property_name = $this->FIELDS_MAP_FATHER;
				break;
		} //End of switch
		if (array_key_exists ( $property_name, $this->core_props )) {
			return $this->core_props [$property_name];
		} else {
			return false;
		}
	} //End of method
	

	//----------------------------------------------------------
	/**! \brief Set property of the recordfile
	 * Return true or false 
	 * 
	 * \param String property name
	 * \param Variant property value
	 */
	public function setProperty($property_name, $property_value) {
		switch ($property_name) {
			case 'container_id' :
				$this->core_props ['container_id'] = $property_value;
				break;
			
			case 'number' :
			case 'file_number' :
			case $this->FIELDS_MAP_NUM :
				$this->core_props [$this->FIELDS_MAP_NUM] = $property_value;
				break;
			
			case 'name' :
			case 'file_name' :
			case $this->FIELDS_MAP_NAME :
				$this->core_props [$this->FIELDS_MAP_NAME] = $property_value;
				break;
			
			case 'description' :
			case $this->FIELDS_MAP_DESC :
				$this->core_props [$this->FIELDS_MAP_DESC] = $property_value;
				break;
			
			case 'state' :
			case $this->FIELDS_MAP_STATE :
				$this->core_props [$this->FIELDS_MAP_STATE] = $property_value;
				break;
			
			case 'father_id' :
			case $this->FIELDS_MAP_FATHER :
				$this->core_props [$this->FIELDS_MAP_FATHER] = ( int ) $property_value;
				break;
			
			case 'import_date' :
			case 'import_by' :
			case 'file_mtime' :
			case 'file_size' :
				$this->core_props [$property_name] = ( int ) $property_value;
				break;
			
			case 'errors_report' :
			case 'logfile' :
			case 'listfile' :
			case 'file_type' :
			case 'file_md5' :
			case 'file_extension' :
			case 'target_dir' :
				$this->core_props [$property_name] = $property_value;
				break;
			
			default :
				return false;
				break;
		} //End of switch
		return $this;
	} //End of method
	

	//-------------------------------------------------------------------------
	/**!\brief get the father object of current object
	 *   
	 */
	//  function getFather(){
	//    return $this->space;
	//  } //End of method
	
	//-------------------------------------------------------------------------
	/** Get the father object of current object
	 * 
	 * @param Rb_Container $container
	 */
	public function setContainer(Rb_Container &$container) {
		$this->setProperty ( 'container_id', $container->getId () );
		$this->_container = & $container;
		return $this;
	} //End of method
	

	/**
	 * 
	 * @param Rb_Reposit $reposit
	 */	
	public function setReposit(Rb_Reposit &$reposit) {
		$this->setProperty ( 'reposit_id', $reposit->getId () );
		$this->_reposit = & $reposit;
		return $this;
	} //End of method
	
	//-------------------------------------------------------------------------
	/** Attach a log
	 * 
	 * @param Rb_Import_Log $log
	 */
	public function setLog(Rb_Import_Log $log) {
		$this->_log = $log;
		return $this;
	} //End of method
	
	//-------------------------------------------------------------------------
	/** Attach a log
	 * 
	 * @param Rb_Import_Log $log
	 */
	public function setContentLog(Rb_Import_Log $log) {
		$this->_contentlog = $log;
		return $this;
	} //End of method
	
	//-------------------------------------------------------------------------
	/** Attach a log
	 * 
	 * @param Rb_Import_Log $log
	 */
	public function setErrorLog(Rb_Import_Log $log) {
		$this->_errorlog = $log;
		return $this;
	} //End of method
	
	/**
	 * @return Rb_Import_Log
	 */
	public function getLog(){
		return $this->_log;
	}
	
	
	/**
	 * @return Rb_Import_Log
	 */
	public function getContentLog(){
		return $this->_contentlog;
	}
	

	/**
	 * @return Rb_Import_Log
	 */
	public function getErrorLog(){
		return $this->_errorlog;
	}
	
	//-------------------------------------------------------------------------
	/** Get history
	 *
	 *    @param Array 
	 *    @return Array | Bool
	 */
	public function getAll($params = array()) {
		return $this->_dao->getAllBasic ( $params );
	}//End of method
	
	//-------------------------------------------------------------------------
	/**
	 * @return Rb_Container
	 */
	function getContainer() {
		if ($this->getId () < 0)
			return false;
		if (! $this->_container && $this->getProperty ( 'container_id' ) ) {
			$this->_container = & Rb_Container::get ( $this->space->getName (), $this->getProperty ( 'container_id' ) );
		}
		return $this->_container;
	} //End of method
	

	//-------------------------------------------------------------------------
	function getReposit() {
		if ($this->getId () < 0)
			return false;
		if (! $this->_reposit && $this->getProperty ( 'reposit_id' ) ) {
			$this->_reposit = & Rb_Reposit::get ( $this->getProperty ( 'reposit_id' ) );
		}
		return $this->_reposit;
	} //End of method
	

	//-------------------------------------------------------------------------
	/**! \brief get id from the number
	 */
	public static function getIdFromNumber($space_name, $number) {
		return self::get ( $space_name )->getDao ()->getBasicId ( $number );
	} //End of method
	

	//-------------------------------------------------------------------------
	/**! \brief get id from the number
	 */
	public static function getIdFromMd5($space_name, $md5) {
		$query = 'SELECT MAX(import_order)' . ' FROM ' . self::get ( $space_name )->getDao ()->getTableName ( 'object' ) . ' WHERE' . ' file_md5=\'' . $md5 . '\'' . ' GROUP BY file_md5';
		$one = Ranchbe::getDb ()->GetOne ( $query );
		return $one;
	} //End of method
	

	//-------------------------------------------------------------------------
	/**! \brief suppress current object database records, files, and all depandancies
	 * and clean the registry of current class to prevent recall of this instance.
	 * can not call this method from instance where id <= 0.
	 * Return false or true.
	 * 
	 */
	public function suppress() {
		if ($this->_id < 1) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('id' => $this->_id ), tra ( 'can not performed this action on object id %id%' ) );
			trigger_error ( 'can not performed this action on object id ' . $this->_id , E_USER_WARNING);
			return false;
		}
		$ret = $this->_suppressPermanentObject ();
		if($ret) unset ( self::$_registry [$this->space->getName ()] [$this->_id] );
		return $ret;
	} //End of method


} //End of class
