<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


class Rb_Import_Log extends Rb_File {
	protected $_content = array ();
	protected $_handle = false;
	protected $_mode = 'a'; //mode to open file: a, w
	

	/**
	 * 
	 * @param String $content
	 * @return Rb_Import_Log
	 */
	public function setContent(array $content) {
		$this->_content = $content;
	}
	
	/**
	 * 
	 * @param String $content
	 * @return Rb_Import_Log
	 */
	public function addContent($content) {
		$this->_content [] = $content;
		return $this;
	}
	
	/**
	 * 
	 * @return Array
	 */
	public function getContent() {
		return $this->_content;
	}
	
	/**
	 * 
	 * @return Boolean || Handle
	 */
	public function getHandle() {
		if (! $this->_handle) {
			$this->_handle = fopen ( $this->file, $this->_mode );
		}
		if (! $this->_handle) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('file' => $this->file ), 'unable to write Log file %file%. Check permissions.' );
			return false;
		}
		return $this->_handle;
	}
	
	/** Save Log in file
	 * 
	 * @return Rb_Import_Log || Boolean
	 */
	public function save() {
		if($this->_content) {
			$handle = & $this->getHandle ();
			if (! $handle)
				return false;
			foreach ( $this->_content as $line ) {
				fwrite ( $handle, $line . PHP_EOL );
			}
		}
		$this->_content = array();
		return $this;
	} //End of method


}//End of class
