<?php
//require_once('core/acl/resource/interface.php');

class Rb_Acl_Resource_Default implements Rb_Acl_Resource_Interface
{
    protected $_resourceId;
    
    protected $_privileges = array(
                                'get',
                                'create',
                                'edit',
                                'suppress',
                                'admin_users',
                                'admin_doctype',
                                'admin_process',
                                'admin_partner',
                                ); //(array)Default privileges name

    public function __construct($resourceId){
        $this->_resourceId = (string) $resourceId;
    } //End of method

    public function getResourceId(){
        return $this->_resourceId;
    } //End of method

    public static function getStaticResourceId($id){
      return $id;
    } //End of method
    
	public function getDefaultResource() {
		return false;
	} //End of method

    public function getPrivileges(){
      return $this->_privileges;
    } //End of method
    
    public function setPrivileges(array $privileges){
      $this->_privileges = $privileges;
      return $this;
    } //End of method
    
}
