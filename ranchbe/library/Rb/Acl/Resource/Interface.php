<?php

//require_once('Zend/Acl/Resource/Interface.php');

interface Rb_Acl_Resource_Interface extends Zend_Acl_Resource_Interface
{
	
	//----------------------------------------------------------
	/*! \brief Define in Zend_Acl_Resource_Interface
	public function getResourceId();
	*/
	
	//----------------------------------------------------------
	/*! \brief Returns the string identifier from specified $id
	* \param $id(string)
	* \return string
	*/
	public static function getStaticResourceId($id);
	
	//-------------------------------------------------------------------------
	/** Get the default resource for current object
	*
	* @return Rb_Acl_Resource_Default
	*/
	public function getDefaultResource();
		
	//----------------------------------------------------------
	/*! \brief init acl and permissions
	* \return $this
	*/
	//protected function _initAcl();
	
	//----------------------------------------------------------
	/*! \brief return privileges for this resource
	* \return array
	*/
	public function getPrivileges();
	
} //End of method
