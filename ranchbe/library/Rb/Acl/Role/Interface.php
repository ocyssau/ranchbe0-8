<?php

//require_once('libext/Zend/Acl/Role/Interface.php');

interface Rb_Acl_Role_Interface extends Zend_Acl_Role_Interface
{

//----------------------------------------------------------
/*! \brief Returns the string identifier of the Role
* \param 
* \return string
*/
//public function getRoleId();

//----------------------------------------------------------
/*! \brief Returns the string identifier from specified $id
* \param $id(string)
* \return string
*/
public static function getStaticRoleId($id);

} //End of class
