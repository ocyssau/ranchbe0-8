<?php

/*
 CREATE TABLE `acl_rules` (
 `privilege` int(11)  NOT NULL,
 `resource_id` int(11) NOT NULL,
 `role_id` int(11)  NOT NULL,
 `rule` varchar(32)  NOT NULL,
 UNIQUE KEY `UC_resources_acl` (`resource_id`,`role_id`,`privilege`),
 KEY `INDEX_acl_privileges_1` (`resource_id`),
 KEY `INDEX_acl_privileges_2` (`role_id`),
 KEY `INDEX_acl_privileges_3` (`privilege`),
 KEY `INDEX_acl_privileges_4` (`rule`)
 ) ENGINE=InnoDB ;
 */

class Rb_Acl_Dao {
	protected $dbranchbe;
	protected $OBJECT_TABLE = 'acl_rules';
	protected $FIELDS_MAP_ID = 'privilege_id';
	protected $FIELDS_MAP_NUM = 'privilege';

	//----------------------------------------------------------------------------
	function __construct(){
		$this->dbranchbe =& Ranchbe::getDb();
		return $this;
	} //End of method

	//----------------------------------------------------------------------------
	/* save rules in database
	 */
	public function save($role_id, $resource_id, $privileges){
		$this->dbranchbe->StartTrans();

		if(!$this->_suppressRules($role_id, $resource_id)){ //before clean all rules
			$this->dbranchbe->CompleteTrans(false);
			return false;
		}
		$query = "INSERT INTO $this->OBJECT_TABLE (
                                      `privilege` ,
                                      `resource_id` ,
                                      `role_id` ,
                                      `rule`
                                      )";
		$i=0;
		foreach($privileges as $privilege=>$rule){ //Generate insert
			if($rule == 'INHERIT') continue;
			$privilege = Rb_Acl::getPrivilegeId($privilege);
			if ($i == 0) $query .= ' VALUES ';
			if ($i > 0) $query .= ',';
			$query .= "('$privilege', '$resource_id', '$role_id', '$rule')";
			$i++;
		}

		//if $i == 0 then all rule are inherit and sql query is faulty because values is not set
		if($i == 0) return $this->dbranchbe->CompleteTrans();

		if ( !$this->dbranchbe->Execute( $query ) ){
			Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
			$this->dbranchbe->FailTrans();
		}
		return $this->dbranchbe->CompleteTrans();
	} //End of method

	//----------------------------------------------------------------------------
	/* suppress all rules of ressource_id for role_id
	 */
	protected function _suppressRules($role_id, $resource_id){
		if(empty($role_id)) return false;
		if(empty($resource_id)) return false;

		$query = 'DELETE FROM '.$this->OBJECT_TABLE.' WHERE role_id = \''.$role_id.'\'
                AND resource_id = \''.$resource_id.'\'';

		//Execute the query
		if(!$this->dbranchbe->Execute($query)){
			Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
			$this->dbranchbe->FailTrans();
			return false;
		}
		return true;
	} //End of method

} //End of class
