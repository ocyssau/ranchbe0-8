<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/** An abstract class implementing Observer objects
 * 
 * @author Olivier CYSSAU
 * 
 * Methods to override: notify($event, $msg)
 * This implements the Observer design pattern defining the Observer class.
 * Observer objects can be "attached" to Observable objects to listen for
 * a specific event.
 * Example:
 * $log = new Logger($logfile); //Logger extends Observer
 * $foo = new Foo(); //Foo extends Observable
 * $foo->attach('moo',$log); //Now $log observers 'moo' events in $foo class
 * $foo->attach_all($log); // Same but all events are listened
 * 
 *
 */
abstract class Rb_Observer implements Rb_Observer_Interface{
	///This will be assigned by an observable object when attaching.
	public $_observerId = '';

	function setObserverId($id){
		$this->_observerId=$id;
	}

	function getObserverId(){
		return $this->_observerId;
	}
}

