<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//THIS CLASS IS NOT IN USE!!!


//----------------------------------------------------------------------
/*! \brief Basic class for manage database access.
 *
 *  This class manage access to database. It define standard method to create, modify or suppress a single
 *  or a group of records in database. 
 *  She define too standards methods to get records. 
 */
abstract class Rb_Storage{

/*! \brief Standard method to create a record in database
*
*   Return the object id if success, false else.
*   This method use the transactions.
*   \param $data(array), its an array where keys/values are fields/values of the record to create.
*/
static function create(ADOConnection &$db, $data){
  $db->StartTrans();
  $id = self::$db->GenID(self::$OBJECT_TABLE.'_seq', 1); //Increment the sequence number
  $data[self::$FIELDS_MAP_ID] = $id;
  
  if (!$db->AutoExecute( self::$OBJECT_TABLE , $data , 'INSERT')){
    self::$error_stack->push(ERROR_DB, 'Fatal', array('query'=>'INSERT'.implode(';',$data) ), $db->ErrorMsg());
    $db->FailTrans();
  }
  
  if(!$db->CompleteTrans()){
    return false;
  }
  
  return $id;
}//End of method

//----------------------------------------------------------------------
/*! \brief Standard method to update a record in database.
*   Return true if success, false else.
*      
*   This method dont use the transactions.
*   \param $data(array), its an array where keys/values are fields/values of the record to update.
*   \param $object_id, is the id of the record to update.
*/
static function update(ADOConnection &$db, $data , $object_id){
  //Update the object data
  if (!$db->AutoExecute( "self::$OBJECT_TABLE" , $data , 'UPDATE' , "self::$FIELDS_MAP_ID = '$object_id'")){
    self::$error_stack->push(ERROR_DB, 'Fatal', array('query'=>'UPDATE' . implode(';',$data) , 'debug'=>array($data, "self::$FIELDS_MAP_ID=`$object_id`", "self::$OBJECT_TABLE")), $db->ErrorMsg());
    return false;
  }
  return true;
}//End of method

//----------------------------------------------------------------------
/*! \brief Standard method to suppress a record in database 
 *
 *   Return true if success, false else.
 *   This method use the transactions.
 *   \param $object_id, is the id of the record to suppress.
 */
function suppress(ADOConnection &$db, $object_id) {
  $query = 'DELETE FROM '.self::$OBJECT_TABLE.' WHERE '.self::$OBJECT_TABLE.'.'.self::$FIELDS_MAP_ID.' = \''.$object_id.'\'';

  //Execute the query
  if(!$db->Execute($query)){
    self::$error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), self::$dbranchbe->ErrorMsg());
    return false;
  }
  return true;
}//End of method

//----------------------------------------------------------------------
/*! \brief Standard method to get all or a filter list of records of a table.
*
*   Return a array if success, false else.
*   This method dont use the transactions.
   \param $params[offset](integer) offset sql option
   \param $params[maxRecords](integer) limit sql option
   \param $params[sort_field](string) field use for sort sql option
   \param $params[sort_order]('ASC' or 'DESC') sort direction sql option
   \param $params[exact_find](array) its a assoc array where key=a field and value=a word to find in the table.
                                      This array is translate to a query option like 'where key=value'
   \param $params[find](array) its a assoc array where key=a field and value=a string to find in the table.
                                      This array is translate to a query option like 'WHERE key LIKE %value%'
   \param $params[select](array) its a nonassoc array with all fields to put in the sql select option
*/

static function getAll(ADOConnection &$db, $params, $return_rs=true){
 $queryOption = self::_getQueryOptions($params);

 $query = 'SELECT '.$queryOption['selectClose'].' FROM '.$table
                .' '.$queryOption['joinClose']
                .' '.$queryOption['whereClose']
                .' '.$queryOption['orderClose']
                .' '.$queryOption['extra']
                ;

  $ADODB_COUNTRECS = false;
  if(!$rs = $db->SelectLimit( $query , $queryOption['limit'] , $queryOption['offset'] ) ){
    self::$error_stack->push(ERROR_DB, 'Fatal', array(), $db->ErrorMsg());
    return false;
  }else{
    if($return_rs)
      return $rs;
    else
      return $rs->GetArray();
  }
}//End of method


//----------------------------------------------------------------------
/*! \brief Get the id from the number of an object. return id if success, false else.
 *
 *  object is somthing like : project, workitem, bookshop, cadlib, mockup, product, partner...etc.
 *  \param  $number(string) is the number to translate in id. Number is the content of the field [object]_number
 */
static function getId(ADOConnection &$db, $number){
  $query = 'SELECT '.self::$FIELDS_MAP_ID
           .' FROM '.self::$OBJECT_TABLE
           .' WHERE '.self::$FIELDS_MAP_NUM.' = \''.$number.'\'';
  return $db->GetOne($query);
}//End of method

//-------------------------------------------------------------
/*! \brief Get the number from the id of an object
 *   return id if success, false else.
 *   
 *  object is somthing like : project, workitem, bookshop, cadlib, mockup, product, partner...etc.
 *  \param  $object_id(integer) is the id of the object to translate
 */
static function getNumber(ADOConnection &$db, $object_id){

	if (is_null($object_id) && !is_numeric($object_id)){
    $this->error_stack->push(Rb_Error::ERROR, array('element'=>$object_id, 'debug'=>array()), 'Invalid object_id : %element%' );
	}

  $query = 'SELECT '.self::$FIELDS_MAP_NUM
           .' FROM '.self::$OBJECT_TABLE
           .' WHERE ('.self::$FIELDS_MAP_ID.' = \''.$object_id.'\')';

  return $db->GetOne($query);

}//End of method

//-------------------------------------------------------------------
/*! \brief Get records from database
 *   return a array if success, false else.
 *   
 *  object is somthing like : project, workitem, bookshop, cadlib, mockup, product, partner...etc.
 *  \param $get(string) = 'all': return all records
 *  \param $get(string) = 'row': return just one row of the recordset
 *  \param $get(string) = 'one': return just the first field of the first row of the recordset
 *  \param $rset_return(bool) = 'true': return the recordset(object) in place array
       \param $params[offset](integer) offset sql option
       \param $params[maxRecords](integer) limit sql option
       \param $params[sort_field](string) field use for sort sql option
       \param $params[sort_order]('ASC' or 'DESC') sort direction sql option
       \param $params[exact_find](array) its a assoc array where key=a field and value=a word to find in the table.
                                          This array is translate to a query option like 'where key=value'
       \param $params[find](array) its a assoc array where key=a field and value=a string to find in the table.
                                          This array is translate to a query option like 'WHERE key LIKE %value%'
       \param $params[select](array(field1,field2,field3,...)) to define the sql select option 
       \$params[where](array field => value) Input special where close
       \$params['with'](array table=>"table to join" , col=>"field use for ON condition") Use this param for create a join close on query
 */
static function get(ADOConnection &$db, $table , $params , $get='all' , $rset_return=false){

 $queryOption = self::_getQueryOptions($params);

 $query = 'SELECT '.$queryOption['selectClose'].' FROM '.$table
                .' '.$queryOption['joinClose']
                .' '.$queryOption['whereClose']
                .' '.$queryOption['orderClose']
                .' '.$queryOption['extra']
                ;

  if($get ==='one')$rset = $db->GetOne($query);
  if($get ==='row')$rset = $db->GetRow($query);
  if($get ==='all')$rset = $db->SelectLimit( $query , $queryOption['limit'] , $queryOption['offset']);
  if($rset === false){
    $this->error_stack->push(ERROR_DB, 'Error', array('query'=>$query), $db->ErrorMsg());
    return false;
  }
  if($get === 'all' && $rset_return === false) $rset = $rset->GetArray(); //To transform object result in array;
  return $rset;
}//End of method

//-------------------------------------------------------------------------
/*! \brief This method decompose the var $params and return the correct synthase for the "where" and "select" for SQL query.
 *   
 *  Exemple of query :
 *  $query = "SELECT $this->selectClose FROM `TABLE`
 *     $this->whereClose
 *     $this->orderClose
 *     ";
 *  Ranchbe::getDb()->SelectLimit( $query , $this->limit , $this->offset);
 *  This method is associated to script "filterManager.php" wich get values from forms    
 *     
 *  return :
 *  $this->selectClose
 *  $this->whereClose
 *  $this->orderClose
 *  $this->limit
 *  $this->offset
 *  $this->from
 
     \param $params[offset](integer) offset sql option
     \param $params[maxRecords](integer) limit sql option
     \param $params[sort_field](string) field use for sort sql option
     \param $params[sort_order]('ASC' or 'DESC') sort direction sql option
     \param $params[exact_find](array) its a assoc array where key=a field and value=a word to find in the table.
                                        This array is translate to a query option like 'where key=value'
     \param $params[find](array) its a assoc array where key=a field and value=a string to find in the table.
                                        This array is translate to a query option like 'WHERE key LIKE %value%'
      to find multi value on the same field just separate the word by close OR or AND like this :
      nom1 OR nom2 OR nom3 with space on each side of OR
     \param $params[select]=array(field1,field2,field3,...) its a nonassoc array with all fields to put in the sql select option 
     \$params[where](array) Input special where close. Exemple : $params[where] = array('field1 > 1' , 'field2 = 2')
     \$params['with'][] = array(type=>"INNER or OUTER" , table=>"table to join" , col=>"field use for ON condition") Use this param for create a join close on query
     \$params['extra'] = any query to add to end
 */
static protected function getQueryOptions($params){
  
  $fromClose  = self::$OBJECT_TABLE;
  
  if( count($params['with']) > 0 ){
    foreach( $params['with'] as $with ){
      $array_joinClose[] = $with['type'].' JOIN '. $with['table'] .' ON '. $with['table'].'.'.$with['col'] .'='.self::$OBJECT_TABLE.'.'.$with['col'];
    }
  }
  if( is_array($array_joinClose) )
    $joinClose = implode(' ', $array_joinClose );
  
  if(!empty($params['offset']) && is_numeric($params['offset'])){
    $offset = $params['offset'];
  }else{
    $offset = 0;
  }
  
  if(!empty($params['numrows']) && is_numeric($params['numrows'])){
    $limit = $params['numrows'];
  }else $limit = '9999';
  
  if(!empty($params['sort_field'])){
    if (!isset($params['sort_order'])) $params['sort_order']='ASC';
    $orderClose = " ORDER BY " . $params['sort_field'] . " " . $params['sort_order'];
  }
  
  $find = $params['find'];
  if( count($params['exact_find']) > 0 ){
    foreach ( $params['exact_find'] as $field => $term){
      $find['AND'][] = "($field = '$term')";
    }
  }
  
  $whereClose = NULL;
  if(is_array ($params['where'])){
    foreach($params['where'] as $wc)
    $whereClose[] = "($wc)";
  }
  //...
  
  if(!empty( $find['AND'] ))
    $whereClose[] = implode(' AND ', $find['AND']);
  if(!empty( $find['OR'] ))
    $whereClose[] = ' OR '.implode(' OR ', $find['OR']);
  
  if(!empty( $whereClose ))
    $whereClose = ' WHERE '.implode(' AND ', $whereClose);
  
  if (is_array($params['select'])){
    $selectClose = implode(' , ' , $params['select']);
  } else { $selectClose = '*'; }
  
  return array(
    'selectClose' => $selectClose,
    'whereClose'  => $whereClose,
    'orderClose'  => $orderClose,
    'limit'       => $limit,
    'offset'      => $offset,
    'from'        => $fromClose,
    'joinClose'   => $joinClose,
    'extra'       => $params['extra'],
  );
  
}//End of method

//---------------------------------------------------------------------------
static function getTableName($table='object'){
  switch($table){
    case 'history':
      if(isset(self::$HISTORY_TABLE))
        return self::$HISTORY_TABLE;
      return false;
      break;
    case 'object':
      if(isset(self::$OBJECT_TABLE))
        return self::$OBJECT_TABLE;
      return false;
      break;
    case 'indice':
      if(isset(self::$INDICE_TABLE))
        return self::$INDICE_TABLE;
      return false;
      break;
  } //End of switch
  return false;
}//End of method

//---------------------------------------------------------------------------
static function getFieldName($field){
  switch($field){
    case 'id':
      if(isset(self::$FIELDS_MAP_ID))
        return self::$FIELDS_MAP_ID;
      return false;
      break;
    case 'number':
      if(isset(self::$FIELDS_MAP_NUM))
        return self::$FIELDS_MAP_NUM;
      return false;
      break;
    case 'description':
      if(isset(self::$FIELDS_MAP_DESC))
        return self::$FIELDS_MAP_DESC;
      return false;
      break;
    case 'state':
      if(isset(self::$FIELDS_MAP_STATE))
        return self::$FIELDS_MAP_STATE;
      return false;
      break;
    case 'indice':
      if(isset(self::$FIELDS_MAP_VERSION))
        return self::$FIELDS_MAP_VERSION;
      return false;
      break;
    case 'father':
      if(isset(self::$FIELDS_MAP_FATHER))
        return self::$FIELDS_MAP_FATHER;
      return false;
      break;
  } //End of switch
  return false;
}//End of method


//-------------------------------------------------------------------
/*! \brief Get number of indice list link to space
 *  return an array indice_id=>indice_number if success, false else. 
 *   
 *  object is somthing like : project, workitem, bookshop, cadlib, mockup...etc.
 *  Each space has his own indice list definition.
 *  \param  $object_id(integer) is the id of the object
 *  \param  $currentIndice_id(integer) If is set return only the indice greater than $currentIndice_id.
 */
//@todo: Cette methode n'a pas de raison d etre herite par les objets partner, doctypes, metadata....
static function getIndiceList($currentIndice_id = 0){

  $query = 'SELECT '.self::$FIELDS_MAP_VERSION.' , indice_value FROM '.self::$INDICE_TABLE
          .' WHERE '.self::$FIELDS_MAP_VERSION.' > '.$currentIndice_id;

  if(!$all = Ranchbe::getDb()->Execute($query)){
    self::$error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), Ranchbe::getDb()->ErrorMsg());
    return false;
  }else{
    $all = $all->GetAssoc(); //To transform object result in array;
    return $all;
  }

}//End of method

//-------------------------------------------------------------------
/*! \brief Translate indice id in usual name
 */
static function getIndiceName($indice_id){
  if( isset(self::$cache_usualName['indices'][$indice_id]) ) {
    return self::$cache_usualName['indices'][$indice_id];
  }
  $query = "SELECT indice_value FROM indices WHERE document_version = '$indice_id'
            ";
  if(!self::$cache_usualName['indices'][$indice_id] = Ranchbe::getDb()->GetOne($query)){
    return self::$cache_usualName['indices'][$indice_id] = tra('undefined');
  }else{
    return self::$cache_usualName['indices'][$indice_id];
  }
}//End of method

}// End of class

