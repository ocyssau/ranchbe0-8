<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
DROP TABLE `workitem_docnotifications`;
CREATE TABLE `workitem_docnotifications` (
  `notification_id` INT NOT NULL ,
  `user_id` INT NOT NULL ,
  `document_id` INT NOT NULL ,
  `script` BLOB,
  PRIMARY KEY  (`notification_id`),
  KEY `INDEX_workitem_docnotifications_1` (`user_id`),
  KEY `INDEX_workitem_docnotifications_2` (`document_id`)
) ENGINE=InnoDB;

ALTER TABLE `workitem_doccomments`
  ADD CONSTRAINT `FK_workitem_docnotifications_1` FOREIGN KEY (`document_id`)
  REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE;

*/

//require_once('core/dao/abstract.php');

class Rb_Document_Notification extends Rb_Dao_Abstract{

function __construct(Rb_Space &$space){
  
  $this->dbranchbe =& Ranchbe::getDb();
  $this->space =& $space;
  
  $this->OBJECT_TABLE = $this->space->getName().'_doccomments';
  $this->FIELDS_MAP_ID = 'comment_id';
  
  $this->RIGHT_OBJECT_TABLE = $this->space->getName().'_documents';
  $this->RIGHT_FIELDS_MAP_ID = 'document_id';
  
}//End of method

//--------------------------------------------------------------------
/*! \brief Add a comment
* 
* \param $input(array)
*/
function add( $document_id, $comment ){
  //Filter input
  $data['document_id'] = $document_id;
  $data['comment'] = $comment;
  $data['open_by'] = Rb_User::getCurrentUser()->getId();
  return $this->_basicCreate($data);
}//End of method

//----------------------------------------------------------
/*! \brief Update a comment
* 
  \param $propset_id(integer)
  \param $input(array)
*/
function modify( $comment_id , $input ){
  unset($input['comment_id']); //to prevent the change of id
  return $this->_basicUpdate($input , $comment_id);
}//End of method

//----------------------------------------------------------
/*! \brief Suppress a comment
* Return bool
  \param $comment_id(integer)
*/
function suppress( $comment_id ){
  return $this->_basicSuppress( $comment_id );
}//End of method

//----------------------------------------------------------
/*!\brief Get the comments
* Return array
*/
function get( $document_id , $params=array() ){
  $params['exact_find']['document_id'] = $document_id;
  return $this->getAllBasic($params);
}//End of method

} //End of class

