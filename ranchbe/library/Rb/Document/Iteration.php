<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*! \brief This class is helper to create and get iterations of a document
* Iteration is a Rb_Document object equal to basic Rb_Document but without relations
* definition
*
*/
class Rb_Document_Iteration{
  
  //-------------------------------------------------------
  /*!\brief Get iterations of document
  * return array of objects or false
  * @param Rb_Document
  * @param bool if true return an array of Rb_Document objects, else return array
  */
  public static function getIterations(Rb_Document &$document, $object = false){
    $query = 'SELECT * FROM '.$document->getDao()->getTableName('object')
             .' WHERE document_bid = \''.$document->getProperty('document_bid').'\''
             .' AND document_version = '.$document->getProperty('document_version')
             .' AND document_life_stage = 3';

    $res = Ranchbe::getDb()->execute($query);
    if($res === false ){
      Ranchbe::getError()->push(Rb_Error::ERROR, array(), Ranchbe::getDb()->ErrorMsg());
      return false;
    }
    if($object){
      $i=0;
      while($row = $res->FetchRow() ){
        $result[$i] = Rb_Document::get($document->getSpaceName(), $row['file_id']);
        $i++;
      }
      return $result;
    }else{
      return $res->GetArray();
    }
  }//End of method

  //-------------------------------------------------------
  /*!\brief Get one Iteration of document
  * return array of objects or false
  * @param Rb_Document
  * @param integer
  * @param bool if true return an array of Rb_Document objects, else return array
  */
  public static function getIteration(Rb_Document &$document, $iteration_id, $object = false){
    $query = 'SELECT file_id FROM '.$document->getDao()->getTableName('object')
             .' WHERE document_bid = \''.$document->getProperty('document_bid').'\''
             .' AND document_version = '.$document->getProperty('document_version')
             .' AND document_iteration = '.$iteration_id
             .' AND document_life_stage = 3';

    $res = Ranchbe::getDb()->GetOne($query);
    if($res === false ){
      Ranchbe::getError()->push(Rb_Error::ERROR, array(), Ranchbe::getDb()->ErrorMsg());
      return false;
    }
    if($object){
      return Rb_Document::get($document->getSpaceName(), $res);
    }else{
      return $res;
    }
  }//End of method
  
  //--------------------------------------------------------
  /*!\brief create a Iteration of document
  *  return Rb_Document
  * @param Rb_Document
  */
  public static function keepIteration(Rb_Document &$document){
    if(DEFAULT_BACK_ITERATIONS_NUM === 0) //Dont keep Iteration if 0
      return true;

    Ranchbe::getError()->notice( 'create Iteration from document: '.$document->getName() );

    $currentIteration = $document->getProperty('file_iteration');
    if( !$currentIteration ){
      Ranchbe::getError()->push(Rb_Error::ERROR, 
                            array('element'=>$document->getName()), 
                              'none Iteration for document %element%');
      return false;
    }

    $iteration = new Rb_Document($document->getSpace(), 0);
    //create a new record of Iteration from recordfile datas and update father and path
    foreach($document->getProperties() as $property=>$value){
      $iteration->setProperty( $property, $value );
    }
    $iteration->setProperty( 'document_access_code', 170 );
    $iteration->setProperty( 'document_life_stage', 3 );
    $iteration->setProperty( 'from_document', $document->getId() );
    $iteration->recordfile();

    return $iteration;

  }//End of method
  
} //End of class
