<?php
// +----------------------------------------------------------------------+
// | This source file is subject to Version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*! \brief This class is helper to create and get versions of a document
* Document versions are same objects that basic document. Version are created
* on user request or by workflow rules and are used to create various possibilities
* of the same document or keep historical copy of the life of a document.
*
*/
class Rb_Document_Version{

  //-------------------------------------------------------
  /*!\brief Get versions of document
  * return array or false
  * @param Rb_Document
  * @param bool if true return an array of Rb_Document objects, else return array with properties only
  */
  public static function getVersions(Rb_Document &$document, $object = false){
    $query = 'SELECT * FROM '.$document->getDao()->getTableName('object')
             .' WHERE document_bid = \''.$document->getProperty('document_bid').'\''
             .' AND document_life_stage < 3';
    $res = Ranchbe::getDb()->execute($query);
    if($res === false ){
      Ranchbe::getError()->errorDb(Ranchbe::getDb()->ErrorMsg(), $query);
      return false;
    }
    if($object){
      $i=0;
      while($row = $res->FetchRow() ){
        $result[$i] = Rb_Document::get($document->getSpaceName(), $row['file_id']);
        $i++;
      }
      return $result;
    }else{
      return $res->GetArray();
    }
  }//End of method

  //-------------------------------------------------------
  /*!\brief Get one Version of document
    * return integer or Rb_Document
    * @param Rb_Document
    * @param integer Version id to get
    * @param bool if true return an array of Rb_Document objects, else return Integer
  */
  public static function getVersion(Rb_Document &$document, $version, $object = false){
    $query = 'SELECT document_id FROM '.$document->getDao()->getTableName('object')
             .' WHERE document_bid = \''.$document->getProperty('document_bid').'\''
             .' AND document_life_stage < 3'
             .' AND document_version = '.$version;

    $one = Ranchbe::getDb()->GetOne($query);
    if(!$one){
      return false;
    }
    if($object){
      return Rb_Document::get($document->getSpaceName(), $one);
    }else{
      return $one;
    }
  }//End of method

  //-------------------------------------------------------
  /*! \brief return the last Version identifier
    * return integer
    * @param Rb_Document
  */
  public static function getLastVid(Rb_Document &$document){
    $query = 'SELECT MAX(document_version)'
             .' FROM '.$document->getDao()->getTableName('object')
             .' WHERE `document_bid` = \''.$document->getProperty('document_bid').'\''
             .' AND document_life_stage < 3'
             .' GROUP BY document_bid';
    $one = Ranchbe::getDb()->GetOne($query);
    return $one;
  }//End of method


  //-------------------------------------------------------
  /*! \brief test if Version $version_id for document $document is valid
    * return bool
    * @param Rb_Document document to test
    * @param integer Version id to test
  */
  public static function ifValid(Rb_Document &$document, $version_id){
    if ( $version_id <= $document->getProperty('document_version') ||
         self::getVersion($document, $version_id, false) ){
      return false;
    }else return true;
  }//End of method

}//End of class
