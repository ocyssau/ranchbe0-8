<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
 CREATE TABLE `document_properties` (
 `link_id` int(11) NOT NULL,
 `document_id` int(11) NOT NULL,
 `property_id` int(11) NOT NULL,
 `property_value` varchar(255) NOT NULL default '',
 `unit` varchar(128) NOT NULL default '',
 PRIMARY KEY  (`link_id`),
 UNIQUE KEY `UNIQ_document_properties_1` (`document_id`,`property_id`)
 ) ENGINE=InnoDB;
 */

/*! \brief This class manage Properties definition.
 *
 */
class Rb_Document_Properties extends Rb_Dao_Join{

	function __construct(Rb_Space &$space , $link_id=NULL){
		$this->dbranchbe =& Ranchbe::getDb();
		$this->space =& $space;

		$this->OBJECT_TABLE = $this->space->getName().'_document_properties';
		$this->FIELDS_MAP_ID = 'link_id';

		$this->LEFT_OBJECT_TABLE = $this->space->getName().'_documents';
		$this->LEFT_FIELDS_MAP_ID = 'document_id';
		$this->RIGHT_FIELDS_MAP_NUM = 'document_number';

		$this->RIGHT_OBJECT_TABLE = 'properties';
		$this->RIGHT_FIELDS_MAP_ID = 'property_id';
		$this->LEFT_FIELDS_MAP_NUM = 'property_name';

		if(!empty($link_id)){
			$this->init($link_id);
		}
	}//End of method

} //End of class

