<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
CREATE TABLE `%container%_document_representation_digital` (
  `link_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `docfile_id` int(11) NOT NULL,
  PRIMARY KEY  (`link_id`),
  UNIQUE KEY `UNIQ_%container%_document_representation_1` (`document_id`,`docfile_id`),
  KEY `INDEX_%container%_document_representation_1` (`document_id`),
  KEY `INDEX_%container%_document_representation_2` (`docfile_id`)
) ENGINE=InnoDB;

ALTER TABLE `%container%_document_representation`
  ADD CONSTRAINT `FK_%container%_document_representation_1` FOREIGN KEY (`document_id`) 
  REFERENCES `%container%_documents` (`document_id`) ON DELETE CASCADE ON UPDATE CASCADE;
  ADD CONSTRAINT `FK_%container%_document_representation_2` FOREIGN KEY (`docfile_id`) 
  REFERENCES `%container%_doc_file` (`docfile_id`) ON DELETE CASCADE ON UPDATE CASCADE;
*/

//require_once('core/document/representation.php');

/*! \brief Digital document is a piece of product data that is archived in a Digital format
*   Rb_Document_Representation_Digital is implementation of 
*   Digital_file class from OMG:Product life cycle management service V2.0
*   
*/
class Rb_Document_Representation_Digital extends Rb_Document_Representation{

  //-------------------------------------------------------
  /**
   *  
  */
  function __construct(Rb_Container &$container , $link_id=NULL){
    $this->dbranchbe =& Ranchbe::getDb();
    $this->error_stack =& Ranchbe::getError();
  
    $this->container =& $container;
    $this->space =& $container->space;
  
    $this->OBJECT_TABLE = $this->space->getName().'_category_rel';
    $this->FIELDS_MAP_ID = 'link_id';
    
    $this->LEFT_OBJECT_TABLE = $this->container->GetTableName('object');
    $this->LEFT_FIELDS_MAP_ID = $this->container->GetFieldName('id');
    $this->LEFT_FIELDS_MAP_NUM = $this->container->GetFieldName('number');
  
    $this->RIGHT_OBJECT_TABLE = $this->space->getName().'_categories';
    $this->RIGHT_FIELDS_MAP_ID = 'category_id';
    $this->RIGHT_FIELDS_MAP_NUM = 'category_number';
  
    $this->FIELDS = array();
    
    if(!empty($link_id)){
      $this->init($link_id);
    }
  }//End of method

}//End of class
