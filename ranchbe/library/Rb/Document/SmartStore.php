<?php

/** NOT USED
 * 
 * 
 * SmartStore is a complex object to help to create/update/checkin document and files
 * This object may choose alone the right action to apply
 * 
 * 
 * 
 * 
 *
 */
class Rb_Document_SmartStore{
	
	/**
	 * 
	 * @var Rb_Document
	 */
	protected $_document; //object document
	
	/**
	 * 
	 * @var Rb_Container
	 */
	protected $_container; //object container
	
	/**
	 * 
	 * @var Rb_Docfile
	 */
	protected $_docfile;
	
	
	/**
	 * 
	 * @var Rb_Space
	 */
	protected $_space; //object space
	
	
	const CREATE_DOC = 1;
	const CREATE_DOC_VERSION = 2;
	const UPDATE_DOC_FILE = 3;
	const UPDATE_DOC = 4;
	const UPDATE_FILE = 5;
	const ADD_FILE = 6;
	const IGNORE = 7;
	
	/**
	 * 
	 * @var array
	 */
	protected $_action_definition = array(
              'create_doc' => 'Create a new document',
              'update_doc_file' => 'Update the document and files',
              'update_doc' => 'Update the document',
              'update_file' => 'Update the file only',
              'add_file' => 'Add the file to current document',
              'ignore' => 'Ignore',
              'upgrade_doc' => 'Create a new indice',
	);
	
	/**
	 * action apply to this document
	 * 
	 * @var array
	 */
	protected $_actions = array('ignore'=>'Ignore');
	
	/**
	 * 
	 * @var string
	 */
	protected $_default_action = 'ignore';
	
	/**
	 * full path to file for update
	 * 
	 * @var string
	 */
	protected $_file;
	
	/**
	 * name of file
	 * 
	 * @var string
	 */
	protected $_file_name;
	
	/**
	 * id of docfile to update
	 * 
	 * @var integer
	 */
	protected $_file_id;
	
	/**
	 * id of indice
	 * 
	 * @var integer
	 */
	protected $_indice_id;

	/** List of default document properties to set from form input
	 * @var array
	 */
	protected $_fields  = array('document_number', 'description',
	                              'category_id', 'document_version');

	/**
	 * 
	 * @var array
	 */	
	protected $_errors;
	
	/**
	 * 
	 * @var array
	 */	
	protected $_feedbacks;
	
	
	//----------------------------------------------------------
	/*
	*
	*/
	function __construct($space_name){
		$this->_space_name = $space_name;
		$this->_space =& Rb_Space::get($this->_space_name);
	}//End of method

	//----------------------------------------------------------
	protected function _update_doc(){
		if(!$this->_document->save()){
			$this->_errors[] = 'Can not update the document = '.$this->_document->getNumber();
			return false;
		}else{
			$this->_feedbacks[] = 'the document ='.$this->_document->getNumber().' has been updated.';
			return true;
		}
	} //End of function

	//----------------------------------------------------------
	protected function _update_doc_file(){
		if( $this->_update_doc() ){
			if( $this->_update_file() ){
				return true;
			}
		}
		return false;
	} //End of function

	//----------------------------------------------------------
	protected function _update_file(){
		if( $this->_docfile ){
			$this->_feedbacks[] = 'update_file : '. $this->_docfile->getNumber();
			//---------- Check if file exist in wildspace
			if($this->data->isExisting() ){ //-----update the file
				$this->_document->checkOut(false); //Checkout without files
				if($this->_docfile->checkOutFile(false)){
					if( $this->_document->checkInAndRelease() ){ //replace document and files
						$this->_feedbacks[] = $this->_file_name.' has been updated.';
					}else{
						$this->_errors[] = 'Can not checkin the file ='. $this->_file_name;
						return false;
					}
				}else{
					$this->_errors[] = 'Can not checkout the file ='. $this->_file_name;
					return false;
				}
			}else{
				$this->_errors[] = 'The file '.$this->_file_name.' is not in your Wildspace ';
				return false;
			}
		}
		return true;
	} //End of function

	//----------------------------------------------------------
	protected function _add_file(){
		$this->_create_doc();
	} //End of function

	//----------------------------------------------------------
	/** Create a new document
	*/
	protected function _create_doc(){
		$fsdata = new Rb_Fsdata($this->_file);
		if($fsdata){ //Add file to doc
			if( $this->_document->ifExist($this->_document->getNumber()) ){
				$this->_feedbacks[] = 'add_file : '. $this->_file_name .' to '. $this->_document->getNumber();
				if( $this->_document->associateDocfile($this->_docfile , true) ){
					$this->_feedbacks[] = 'the file '.$this->_file_name.' has been associated to document '.$this->_document->getNumber();
				}else{
					$this->_errors[] = 'The file '.$this->_file_name.' can not be associated to the document';
					return false;
				}
			}else{ //Create the doc
				$this->_feedbacks[] = 'create_doc : '.$this->_document->getNumber();
				if( $this->_document->store(false) ){
					$this->_feedbacks[] = 'The doc '.$this->_document->getNumber().' has been created';
				}else{
					$this->_errors[] = 'Can not create the doc '.$this->_document->getNumber();
					return false;
				}
			} //End of create doc
		}else{
			$this->_errors[] = 'The file '.$this->_file_name.' is not in your Wildspace';
			return false;
		}

		return true;

	} //End of function

	//----------------------------------------------------------
	/* Create a new indice and update the files if specifed
	*/
	protected function _upgrade_doc(){
		if(Ranchbe::checkPerm( 'document_create' , $this->_document , false) === false){
			$this->_feedbacks[] = 'You have not permission to upgrade the document '.$this->_document->getNumber();
			break;
		}
		$this->_feedbacks[] = 'Try to upgrade document '.$this->_document->getNumber();
		//Check access
		$accessCode = $this->_document->checkAccess();
		if($accessCode == 0){
			//Lock the current indice
			if( !$this->_document->lockDocument(11, true) ){
				$this->_errors[] = 'Can not lock the document '.$this->_document->getNumber();
				break;
			}
			//Lock access to files too
			$docfiles =& $this->_document->getDocfiles();
			if(is_array($docfiles))
			foreach($docfiles as $docfile){
				$docfile->lockFile(11, true);
			}
			$this->_feedbacks[] = 'the document '.$this->_document->getNumber().' has been locked.';
		}else{
			if($accessCode == 1){
				$this->_errors[] = 'This document is checkout';
				break;
			}
		}

		//Create the new indice
		if( $this->_document_version > $this->_document->getProperty('document_version') )
		$new_indice_id = $this->_document_version;
		else
		$new_indice_id = NULL; // Let createVersion method choose new indice
		if( $new_doc_id = $this->_document->createVersion($new_indice_id, $this->_container->getId()) ){
			$this->_feedbacks[] = 'the document ID='.$new_doc_id.' has been created.';
			$onewDocument =& Rb_Document::get($this->_space->getNumber(), $new_doc_id);
			//----------------- Update the files
			//Get docfiles
			if($file_id = Rb_Docfile::get($this->_space_name)->getFileId( $this->_file_name , $new_doc_id ) ){ //- Get the id of file from new document
				$odocfile =& Rb_Docfile::get($this->_space_name, $file_id);
				$odata = Rb_Fsdata::_dataFactory($this->_file);
				if($odata){ //-----update the file
					$onewDocument->checkOut(false); //Checkout document without files
					if($odocfile->checkOutFile(false)){ //Checkout file but without copy of file in wildspace
						if( $onewDocument->checkInAndRelease() ){ //checkin files
							$this->_feedbacks[] = $this->_file_name.' has been updated.';
						}else{
							$this->_errors[] = 'Can not checkin the file ='. $this->_file_name;
						}
					}else{
						$this->_errors[] = 'Can not checkout the file ='. $this->_file_name;
					}
				}else{
					$this->_errors[] = 'The file '.$this->_file_name.' is not in your Wildspace ';
				}
			}else $this->_errors[] = 'Can not update the new indice';
		}else $this->_errors[] = 'Can not create the new indice';
		return true;
	} //End of function

	//-----------------------------------------------------------------
	public function setIndice($id){
		$this->_indice_id = $id;
	}

	//---------------------------------------------------------------------
	/** Check if file exist in path
	 * 
	 * @param unknown_type $file_name
	 * @param unknown_type $path
	 * @return Rb_Fsdata|false
	 */
	public function testFile($file_name, $path){
		$data = new Rb_Fsdata($path.'/'.$file_name);
		if( !$data->isExisting() ){ //file dont exist so return error message to user
			$this->_errors[] = $file_name.' is not in your Wildspace.';
			$this->_actions['ignore'] = 'Ignore';
			return false;
		}else{
			$this->_file_name = $data->getProperty('file_name');
			$this->_file = $data->getProperty('file');
			return $this->data =& $data;
		}
	} //End of method
	
	//---------------------------------------------------------------------
	/** Test is file exist
	 * 
	 * @param Rb_Fsdata	$data
	 */
	public function isDocfile(Rb_Fsdata $data){
		$file_name = $data->getProperty('file_name');
		
		if($file_id = Rb_Docfile::getFileId($this->_space_name, $file_name) ){ //- The file is recorded in database
			$this->_docfile =& Rb_Docfile::get($this->_space_name, $file_id);
			$this->_document =& $this->_docfile->getFather();
			
			$this->_errors[] = 'file '.$file_name . ' exist in this database. ID=' . $file_id;
			$this->_errors[] = 'file '.$file_name . ' is linked to document ' .
							$this->_document->getName().' ID='.$this->_document->getId().
                              ' in container '.$this->_document->getFather()->getNumber();
							
			//Compare md5
			if( $this->_docfile->getProperty('file_md5') == $data->getProperty('file_md5') ){
				$equal_md5 = true;
			}
			
			if($this->_document->checkAccess() != 0){
				$this->_errors[] = 'The document '.$this->_document->getNumber().' is not free ';
			}else if( $this->_docfile->checkAccess() != 0 ){
				$this->_errors[] = 'file '.$file_name . ' is not free ';
			}else{
				if( $equal_md5 ){
					$this->_errors[] = 'files '.$file_name.' from wildspace and from vault are strictly identicals';
					$this->_actions['update_doc'] = 'Update the document '.$this->_document->getNumber();
					//$this->_default_action = 'update_doc';
				}else{
					$this->_actions['update_doc_file'] = 'Update the document '.$this->_document->getNumber().' and files';
					$this->_actions['update_file'] = 'Update the file '.$file_name.' only';
					$this->_actions['upgrade_doc'] = 'Create a new indice of '.$this->_document->getNumber();
					//$this->_default_action = 'update_doc_file';
				}
			}
			return true;
		}else{
			return false;
		}
	} //End of method
	
	//---------------------------------------------------------------------
	/** Test if $doc_name is a existing document. If is, init document
	 * 
	 * @param string $doc_name
	 */
	public function isDocument($doc_name){
		if( $document_id = Rb_Document::get($this->_space_name)->ifExist( $doc_name ) ){
			$this->_document =& Rb_Document::get($this->_space_name, $document_id );
			$this->_errors[] = 'The document '.$this->_document->getNumber() .
                                  ' exist in this database. ID=' . $document_id;
			if($this->_document->checkAccess() !== 0){
				$this->_errors[] = 'The document '.$this->_document->getNumber() . ' is not free ';
			}else{
				$this->_actions['add_file'] = 'add_file';
				$this->_actions['upgrade_doc'] = 'Create a new indice of '.$this->_document->getNumber();
			}
			return true;
		}else{
			return false;
		}
	} //End of method
	
	//---------------------------------------------------------------------
	/** Init a new document and a new docfile
	 * 
	 * @param Rb_Container $container
	 * @param Rb_Fsdata $fsdata
	 * @param string $document_number
	 */
	public function initNewDocument(Rb_Container &$container, $fsdata = null, $document_number = null){
		$this->_document = new Rb_Document( $this->_space , 0);
		$this->_document->setContainer( $container );
		if($fsdata){
			$this->_docfile = new Rb_Docfile( $this->_space , 0);
			$this->_docfile->setFsdata( $fsdata );
			$this->_document->setDocfile( $this->_docfile, 'main' );
		}
		if( $document_number ){
			$this->_document->setProperty( 'document_number', $document_number );
		}else if($fsdata){
			$this->_document->setProperty( 'document_number', $fsdata->getProperty('doc_name') );
		}
		$this->_actions['create_doc'] = 'Create a new document';
		$this->_default_action = 'create_doc';
	} //End of method
	
	//---------------------------------------------------------------------
	/** Check the doctype for the new document
	 * 
	 */
	public function setDoctype(){
		if($this->data){
			$doctype =& $this->_document->setDocType($this->data->getProperty('file_extension'),
			$this->data->getProperty('file_type'));
		}else{
			$doctype =& $this->_document->setDocType('','nofile');
		}

		if(!$doctype){
			$this->_errors[] = 'This document has a bad doctype';
			$this->_actions = array('ignore'=>'Ignore'); //Reinit all actions
			return false;
		}else{
			$this->_feedbacks[] = 'Reconize doctype : '.$doctype->getProperty('doctype_number');
			return $doctype;
		}
	} //End of method
	
	//---------------------------------------------------------------------
	/** Set category for document with doctype
	 * 
	 * @param Rb_Doctype $doctype
	 * @param integer $category_id
	 * @return void
	 */
	public function setCategory(&$doctype, $category_id = false){
		//Get the category
		if($category_id){
			$this->_document->setProperty( 'category_id', $category_id );
		}else if(is_a($doctype, 'Rb_Doctype')){
			$categories = Rb_Container_Relation_Doctype_Category::get()
						->getCategories($this->_document->getProperty('container_id'),$doctype->getId());
			$this->_document->setProperty('category_id', $categories[0]['category_id']);
		}
	} //End of method
	
	//---------------------------------------------------------------------
	/**
	 * 
	 */
	public function getDocument(){
		return $this->_document;
	} //End of method
	
	//---------------------------------------------------------------------
	/**
	 * 
	 */
	public function getDocfile(){
		return $this->_docfile;
	} //End of method
	
	//---------------------------------------------------------------------
	/**
	 * @return array
	 */
	public function getActions(){
		return $this->_actions;
	} //End of method
	
	//---------------------------------------------------------------------
	/**
	 * @return array
	 */
	public function getErrors(){
		return $this->_errors;
	} //End of method

	
	//---------------------------------------------------------------------
	/**
	 * 
	 * @param string $message
	 */
	public function setError($message){
		$this->_errors[] = $message;
	} //End of method

	//---------------------------------------------------------------------
	/**
	 * 
	 * @param string $message
	 */
	public function setFeedback($message){
		$this->_feedbacks[] = $message;
	} //End of method

	//---------------------------------------------------------------------
	/**
	 * 
	 * @param string $action
	 */
	public function setAction($action){
		$this->_actions[] = $action;
	} //End of method

	//---------------------------------------------------------------------
	/**
	 * 
	 * @param string $property_name
	 */
	public function setPropertyToUpdate($property_name){
		$this->_fields[] = $property_name;
	} //End of method


	//-----------------------------------------------------------------
	/**
	 * 
	 * @param integer $docaction
	 */
	public function apply($docaction){
		switch($docaction){
			case "ignore": //Ignore
			case Rb_Document_SmartStore::IGNORE: //Ignore
				return true;
				break;
			case "update_doc":
			case Rb_Document_SmartStore::UPDATE_DOC: //Update the document only
				if ( $this->_update_doc() ){  //call function
					return true;
				}
				break;
			case "update_doc_file":
			case Rb_Document_SmartStore::UPDATE_DOC_FILE: //Update the document and the file if specified
				if ( $this->_update_doc_file() ){  //call function
					return true;
				}
				break;
			case "update_file":
			case Rb_Document_SmartStore::UPDATE_FILE: //Update the associated file only
				if ( $this->_update_file() ){  //call function
					return true;
				}
				break;
			case "add_file":
			case Rb_Document_SmartStore::ADD_FILE: //Add the file to the document
				if ( $this->_add_file() ){  //call function
					return true;
				}
				break;
			case "create_doc":
			case Rb_Document_SmartStore::CREATE_DOC: //Create a new document
				if ( $this->_create_doc() ){  //call function
					return true;
				}
				break;
			case "upgrade_doc":
			case Rb_Document_SmartStore::CREATE_DOC_VERSION: //Create a new indice and update the files if specifed
				if ( $this->_upgrade_doc() ){  //call function
					return true;
				}
				break;
				} //End of switch
			return false;
		} //End of function
	
} //End of class

