<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

class Rb_Document_Serialize{

	//----------------------------------------------------------
	/*!\brief
	 *
	 */
	public static function toXml(Rb_Document &$document, XMLWriter &$xml){
		$xml->startElement ('document');
		
		//export document properties
		$xml->startElement('properties');
		foreach($document->getProperties() as $name=>$value){
			$xml->writeElement($name, $value);
		}
		$xml->endElement();

		//export document extends properties
		$xml->startElement('extends_properties');
		$xml->endElement();

		//export associated files
		$xml->startElement('docfiles');
		$params=array('exact_find'=>array('document_id' => $document->getId()),
                'select'=>array('file_id',
                                'role_id',
                                'file_name',
                                'file_version',
                                'file_iteration'),
                'getRs'=>true);
		$rs = Rb_Docfile_Role::get($document->getSpaceName())->getAll($params);
		if($rs){
			while (!$rs->EOF){
				$docfile = Rb_Docfile::get($document->getSpaceName(), $rs->fields['file_id']);
				$xml->writeElement($docfile->getNumber(), $docfile->getId());
				$xml->writeElement('role_id', $docfile->getId());
				$rs->MoveNext();
			}
		}
		$xml->endElement();

		//export container
		$xml->startElement('container');
		Rb_Object_Serialize::toXml($document->getContainer(), $xml);
		$xml->endElement();

		//export category
		$xml->startElement('category');
		$category =& Rb_Category::get($document->getSpace()->getName(),
		$document->getProperty('category_id'));
		if($category->getId() > 0)
		Rb_Object_Serialize::toXml($category, $xml);
		$xml->endElement();

		//export doctype
		$xml->startElement('doctype');
		$doctype =& $document->getDoctype();
		if($doctype)
		Rb_Object_Serialize::toXml($doctype, $xml);
		$xml->endElement();

		//export process instance
		$xml->startElement('process_instance');
		$xml->endElement();

		//export create user
		$xml->startElement('create_user');
		$user =& Rb_User::get($document->getProperty('open_by'));
		if($user)
		Rb_User_Serialize::toXml($user, $xml);
		$xml->endElement();

		//export update user
		$xml->startElement('update_user');
		$user =& Rb_User::get($document->getProperty('update_by'));
		if($user)
		Rb_User_Serialize::toXml($user, $xml);
		$xml->endElement();

		//export owner
		$xml->startElement('owner');
		$user =& Rb_User::get($document->getProperty('owned_by'));
		if($user)
		Rb_User_Serialize::toXml($user, $xml);
		$xml->endElement();

		//export from document
		$xml->startElement('from_document');
		$from = Rb_Document::get($document->getSpaceName(),
		$document->getProperty('from_document'));
		if($from->getId() > 0)
		foreach($from->getProperties() as $name=>$value){
			$xml->writeElement($name, $value);
		}
		$xml->endElement();

		return $xml;
	}//End of method

}//End of class
