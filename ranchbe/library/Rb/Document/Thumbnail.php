<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//========================================================================================
/*! \brief Thumbnail of the document to rapid pre-visualisation of the document.
*
*/
class Rb_Document_Thumbnail{

protected $_document = false;
protected $_file_extension = false; //output file extension .jpg|.gif|.png
protected $_error_stack = false;
protected $_thumbnail_dir = false; //path to thumbnails directory
protected $_outputFile = false; //output file path

public $width; //width of the Thumbnail in pixel
public $height; //height of the Thumbnail in pixel

function __construct(Rb_Document &$document){
  if($document->getId() < 1){
    trigger_error('document_id is not set', E_USER_WARNING);
    return false;
  }
  $this->_document =& $document;
  $this->_file_extension = '.' . trim(Ranchbe::getConfig()->thumbnails->extension, '.');
  $this->width = Ranchbe::getConfig()->thumbnails->xsize;
  $this->height = Ranchbe::getConfig()->thumbnails->ysize;
  $this->_thumbnail_dir = Ranchbe::getConfig()->thumbnails->path;
}//End of method

//-------------------------------------------------------------------------
/*! \brief Get the file name and path of the attachment file
* 
*/
protected function _getOutputfile(){
  if(!is_dir($this->_thumbnail_dir.'/'.$this->_document->getSpaceName())){
    trigger_error($this->_thumbnail_dir.' is not reachable', E_USER_WARNING);
    return false;
  }
  return $this->_outputFile = $this->_thumbnail_dir.'/'
                              .$this->_document->getSpaceName().'/'
                              .$this->_document->getId()
                              .$this->_file_extension;
}//End of method

//-------------------------------------------------------------------------
/*! \brief generate a Thumbnail of the document from the main picture docfile
* 
*/
function generate(){
  $docfile =& $this->_document->getDocfile('mainpicture');
  if($docfile){
    $file = $docfile->getProperty('file');
  }else{
    return false;
  }

  if( !$outputFile=$this->_getOutputfile() ) return false;

  // Def of width and height
  $width =& $this->width;
  $height =& $this->height;

  $file_extension = substr($file, strrpos($file, '.'));
  switch($file_extension){
    case('.jpg'):
    case('.gif'):
    case('.png'):
    case('.bmp'):
      break;
    default:
      return false;
      break;
  }

  // Cacul des nouvelles dimensions
  list($width_orig, $height_orig) = getimagesize($file);

  $ratio_orig = $width_orig/$height_orig;
  if ($width/$height > $ratio_orig) {
    $width = $height*$ratio_orig;
  } else {
    $height = $width/$ratio_orig;
  }
  
  // Redimensionnement
  switch($file_extension){
    case('.jpg'):
      $image = imagecreatefromjpeg($file);
      break;
    case('.gif'):
      $image = imagecreatefromgif($file);
      break;
    case('.png'):
      $image = imagecreatefrompng($file);
      break;
    case('.bmp'):
      $image = imagecreatefromwbmp($file);
      break;
    default:
      return false;
      break;
  }
  $image_p = imagecreatetruecolor($width, $height);
  imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
  imagetruecolortopalette($image_p , true , 16); //convert in 16 colors
  // Create file
  switch($this->_file_extension){
    case('.png'):
      imagepng($image_p, $outputFile);
      break;
    case('.gif'):
      imagegif($image_p, $outputFile);
      break;
    case('.jpg'):
      imagejpeg($image_p, $outputFile);
      break;
    default:
      return false;
      break;
  }

  return $outputFile;

}//End of method

} //End of class

