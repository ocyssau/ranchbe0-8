<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/Viewer.php');

//===========================================================
/*! \brief Class to view file or attachment file
*/
class Rb_Document_Viewer extends Rb_Viewer{
  private $_document;
  private $_visuFile; //Filename and path to the visu file
  private $_thumbFile; //Filename and path to the thumbnail file
  private $_pictureFile; //Filename and path to the picture file

//----------------------------------------------------------
  function __construct(Rb_Document &$document){
    $this->error_stack =& Ranchbe::getError();
    return $this->_initDocument($document);
  }//End of method

//----------------------------------------------------------
  function init(&$object){
    if(is_a($object,Rb_Document)){
      return $this->_initDocument($object);
    }else{
      trigger_error('this method accept only Rb_Document', E_USER_WARNING);
      return false;
    }
  }//End of method

//----------------------------------------------------------
  public function __get($property_name){
    switch($property_name){
      case 'document':
        return $this->_document;
      case 'visuFile':
        return $this->_getVisuFile;
      case 'thumbFile':
        return $this->_thumbFile;
      case 'pictureFile':
        return $this->_getPictureFile;
      default:
        return parent::__get($property_name);
    }
  }//End of method

//----------------------------------------------------------
/**
* Get the properties from fsdata object
*/
private function _initDocument(Rb_Document $document){
  if($document->getId() < 1) trigger_error('document is not set', E_USER_WARNING);
  $this->_document =& $document;
  return true;
} //End of method

//-------------------------------------------------------------------------
/*! \brief Show the picture associate to current document in a html page (embeded)
* 
* \param 
*/
function displayPicture(){
  if(!$this->_getPictureFile()) return false;
  $encHeader = 'data:'.$this->_mimetype.';base64,';
  $imgdata = $encHeader.base64_encode(file_get_contents($this->_pictureFile));
  return '<img border=0 align="center" src="'.$imgdata.'" height="300" />';
}//End of method

//-------------------------------------------------------------------------
/*! \brief Show the visualisation file of the current document in a html page (embeded)
*   
* \param 
*/
public function displayVisu(){
  if(!$this->_getVisuFile()) return false;
  $this->_initDriver(); //init the driver for the file
  return $this->_embeded(); //Return the embeded Viewer
}//End of method

//-------------------------------------------------------------------------
/*! \brief Show the visualisation file of the current document in a html page (embeded)
*   
* \param 
*/
public function display(){
  if(!$this->_getMainFile()) return false;
  $this->_initDriver(); //init the driver for the file
  return $this->_embeded(); //Return the embeded Viewer
}//End of method

//-------------------------------------------------------------------------
/*! \brief Push picture file to download it
* 
*/
public function pushPicture(){
  if(!$this->_getPictureFile()) return false;
  return $this->_pushfile();
}//End of method

//-------------------------------------------------------------------------
/*! \brief Push visu file to download it
*   
*/
public function pushVisu(){
  if(!$this->_getVisuFile()) return false;
  return $this->_pushfile();
}//End of method

//-------------------------------------------------------------------------
/*! \brief Get the visualisation file name and path
* 
* \param 
*/
public function push(){
  if(!$this->_getMainFile()) return false;
  return $this->_pushfile();
}//End of method

//-------------------------------------------------------------------------
/*! \brief Get the picture file name and path
* Return true or false
* \param 
*/
private function _getPictureFile(){
  if(!$this->_pictureFile){
    if(!isset($this->_document)) return false;
    $docfile =& $this->_document->getDocfile('mainpicture');
    if($docfile){
      $this->_initFsdata($docfile->getFsdata());
      $this->_pictureFile = $this->_file_path;
    }else return false;
  }
  return $this->_pictureFile;
}//End of method

//-------------------------------------------------------------------------
/*! \brief Get the visualisation file name and path
* 
* \param 
*/
private function _getVisuFile(){
  if(!$this->_visuFile){
    if(!isset($this->_document)) return false;
    $docfile = false;
    $search_order = Ranchbe::getConfig()->visu->roles->search_order;
    if(!$search_order) $search_order = array('main');
    foreach($search_order as $role){
    	$docfile =& $this->_document->getDocfile($role);
        if($docfile) break;
    }
    if($docfile){
    	$this->_initFsdata($docfile->getFsdata());
      	$this->_visuFile = $this->_file_path;
    }else return false;
  }
  return $this->_visuFile;
}//End of method

//-------------------------------------------------------------------------
/*! \brief Get the visualisation file name and path
* 
* \param 
*/
private function _getMainFile(){
  if(!$this->_mainFile){
    if(!isset($this->_document)) return false;
    $docfile =& $this->_document->getDocfile('main');
    if($docfile){
      $this->_initFsdata($docfile->getFsdata());
      $this->_mainFile = $this->_file_path;
    }else return false;
  }
  return $this->_mainFile;
}//End of method

}//End of class

