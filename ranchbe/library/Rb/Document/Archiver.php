<?php

class Document_Archiver{
	
	/**
	 * 
	 * @var Rb_Document
	 */
	protected $_document;
	
	
	/**
	 * 
	 * @var string
	 */
	protected $_repositFolder;
	
	
	/**
	 * 
	 * @var string
	 */
	public static $_state = 'archived';
	
	
	/**
	 * Value of the offset to apply to access_code
	 * 
	 * @var integer
	 */
	public static $_accessCode = 100;
	
	/**
	 * 
	 * Constructor
	 * @param document $document
	 */
	public function __construct( Rb_Document $document ){
		$this->_document =& $document;
		$this->_repositFolder =& self::getFolder( $this->_document->getContainer() );
	}
	
	/**
	 * 
	 * Run archiving on document.
	 * All Docfiles of document are move in archiving reposit.*
	 * Document access_code is update to self::$_accessCode value.
	 * 
	 */
	public function archive(){
		if( $this->_document->GetDocProperty('document_access_code') > 100){
			return true;
		}
		
		$docfiles = $this->_document->GetDocfiles();
		
		foreach($docfiles as $docfile){
			$ok = self::cleanVersions($docfile);
			
			if( $docfile->GetProperty('file_access_code') > 100 ){
				continue;
			}
			
			$archivedFileName = $docfile->GetProperty('file_root_name') . '.' 
								. $docfile->GetProperty('file_md5')
								. $docfile->GetProperty('file_extension');
								
			$ok = $docfile->Rename( $this->_repositFolder . '/' . $archivedFileName );
			if($ok){
				$docfile->SetProperty('file_access_code', $docfile->getProperty('file_access_code') + self::$_accessCode );
				$docfile->save();
			}
		}
		
		$myAccessCode = $this->_document->GetDocProperty('document_access_code') + self::$_accessCode;
		$this->_document->SetDocProperty( 'document_access_code', $myAccessCode );
		$number = $this->_document->getDocProperty('document_number');
		$this->_document->SetDocProperty( 'document_number', $number . uniqid('.A.') );
		
		$ok = $this->_document->UpdateDocument( $this->_document->GetProperties() );
		return $ok;
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @param recordfile $docfile
	 * @return boolean
	 */
	public static function cleanVersions(Rb_Recordfile_Abstract $docfile){
		$currentVerId = $docfile->GetProperty('file_version');
		$obsVerId = $currentVerId - 1;
		
		$check = true;
		while($obsVerId > 0){
			$obsoleteVersion = new recordfileVersion($docfile->space); //Create the recordfile version object
			if( $obsoleteVersion->initVersion($docfile->GetId(), $obsVerId) ){ //false if not on filesystem
				if( !$obsoleteVersion->RemoveFile(true, false) ){
					$check = false;
				}
			}
			$obsVerId = $obsVerId - 1;
		}
		
		return $check;
	}
	
	
	/**
	 * 
	 * @param container $container
	 * @return string
	 * 
	 */
	public static function getFolder(container $container){
		$type = $container->SPACE_NAME;
		$name = $container->GetProperty($type . '_number');
		
		switch($type){
			case 'bookshop':
				$path = DEFAULT_ARCHIVED_BOOKSHOP_DIR;
				break;
			case 'cadlib':
				$path = DEFAULT_ARCHIVED_CADLIB_DIR;
				break;
			case 'mockup':
				$path = DEFAULT_ARCHIVED_MOCKUP_DIR;
				break;
			case 'workitem':
				$path = DEFAULT_ARCHIVED_WORKITEM_DIR;
				break;
		}
		
		require_once('class/Sys/Folder.php');
		$path = $path . '/' . $name;
		//$Folder = new Sys_Folder( $path . '/' . $name );
		
		if( !is_dir($path) ){
			mkdir( $path, 0755, true );
		}
		
		return $path;
	}
}
