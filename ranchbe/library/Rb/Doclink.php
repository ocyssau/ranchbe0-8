<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
 CREATE TABLE `workitem_doc_rel` (
 `dr_link_id` int(11) NOT NULL,
 `dr_document_id` int(11) default NULL,
 `dr_l_document_id` int(11) default NULL,
 `dr_access_code` int(11) NOT NULL default '15',
 PRIMARY KEY  (`dr_link_id`),
 UNIQUE KEY `workitem_doc_rel_uniq1` (`dr_document_id`,`dr_l_document_id`),
 KEY `INDEX_workitem_doc_rel_1` (`dr_document_id`),
 KEY `INDEX_workitem_doc_rel_2` (`dr_l_document_id`)
 ) ENGINE=InnoDB;


 --
 -- Contraintes pour la table `bookshop_doc_rel`
 --
 ALTER TABLE `workitem_doc_rel`
 ADD CONSTRAINT `FK_workitem_doc_rel_1` FOREIGN KEY (`dr_document_id`)
 REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE,
 ADD CONSTRAINT `FK_workitem_doc_rel_2` FOREIGN KEY (`dr_l_document_id`)
 REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE;
 */

//require_once('core/dao/abstract.php');

/** This class manage Doclink.
 * A Doclink define links betweens documents.
 * 
 *
 */
class Rb_Doclink extends Rb_Dao_Abstract{

	protected $OBJECT_TABLE; //(String)Table where are stored the containers definitions

	/** 
	 * Name of the field of the primary key
	 * @var String
	 */
	protected $FIELDS_MAP_ID;

	/**
	 * Id of the current document
	 * @var Integer
	 */
	protected $doclink_id;

	/**
	 * Main document
	 * @var Rb_Document
	 */
	protected $document;

	//--------------------------------------------------------------------
	/**
	 * 
	 * @param Rb_Document $document
	 * @return Void
	 */
	function __construct(Rb_Document &$document){
		$this->document =& $document;
		$this->space =& $document->getSpace();
		$this->dbranchbe =& Ranchbe::getDb();
		$this->OBJECT_TABLE  = $this->space->getName() . '_doc_rel';
		$this->FIELDS_MAP_ID = 'dr_link_id';
	}//End of method

	//----------------------------------------------------------
	/**
	 * Link current document to one other. The link is oriented : father document to child document
	 * Return the link_id of the new link if success, else false.
	 * 
	 * @param Integer $son_document_id
	 * @return Integer
	 */
	function addSon($son_document_id){
		$data['dr_document_id'] = $this->document->getId();
		$data['dr_l_document_id'] = $son_document_id;
		return $this->_basicCreate($data);
	}//End of method

	//----------------------------------------------------------
	/**
	 * Suppress a link between document
	 * Return true or false
	 * 
	 * @param Integer $link_id
	 * @return Bool
	 */
	function suppressSon($link_id){
		if (empty($link_id)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), 'none selected link' );
			return false;
		}
			return $this->_basicSuppress($link_id);
	}//End of method

	//----------------------------------------------------------
	/**
	 * Update a link between document
	 * Return true or false
	 *
	 * This method search in the _doc_rel table the document linked with id $l_document_id and replace it
	 * by the the $_new_l_document_id
	 * This function is used when the document is indice upgraded for always keeps the links.
	 * The link is replace only if is not explicitly locked. A link is locked if the value of dr_access_code is > to 14.
	 * 
	 * @param Integer $son_id
	 * @param Integer $new_son_id
	 * @return Bool
	 */
	function replaceDocLink($son_id , $new_son_id){

		$query = "UPDATE $this->OBJECT_TABLE
            SET `dr_l_document_id` = REPLACE (`dr_l_document_id`, $son_id, $new_son_id)
            WHERE `dr_l_document_id` = '$son_id'
            AND (`dr_access_code` < '15' OR `dr_access_code` IS NULL)
          ";

		//Update the object data
		if ($this->dbranchbe->Execute("$query") === false){
			Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
			return false;
		}

		return true;

	}//End of method

	//----------------------------------------------------------
	/**
	 * Get documents linked to one document
	 * Return a array with properties of the linked documents
	 * 
	 * @param Array $params ,See parameters function of getQueryOptions()
	 * @return Bool
	 */
	function getSons($params=array()){
		$params['exact_find']['dr.dr_document_id'] = $this->document->getId();

		$this->_getQueryOptions($params);

		//Junction of tables doctypes and link_table
		$query = 'SELECT '.$this->selectClose.' FROM '.$this->OBJECT_TABLE.' as dr JOIN '.$this->space->DOC_TABLE.' as dt
		            ON dr.dr_l_document_id = dt.document_id '.
					$this->whereClose.' '.
					$this->orderClose;

		if(! $links = $this->dbranchbe->Execute($query) ){
			Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
			return false;
		}else{
			$links = $links->GetArray(); //To transform object result in array;
			foreach ($links as $key => $value){
				$links[$key]['object_class'] = 'Doclink'; //Add the type of object to each key
			}
			return $links;
		}
		return false;
	}//End of method

	//----------------------------------------------------------
	/**
	 * Get links of the document.
	 * Return a array with links properties of the links.
	 * 
	 * @param Array $params , See parameters function of getQueryOptions()
	 * @return Bool
	 */
	function getDocumentLinks($params=array()){
		$params['exact_find']['dr_l_document_id'] = $this->document->getId();
		$this->_getQueryOptions($params);
		$query = "SELECT $this->selectClose FROM $this->OBJECT_TABLE
		$this->whereClose
		$this->orderClose
            ";
		if(!$links = $this->dbranchbe->Execute($query)){
			Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
			return false;
		}else{
			$links = $links->GetArray(); //To transform object result in array;
			foreach ($links as $key => $value){
				$links[$key]['object_class'] = 'Doclink'; //Add the type of object to each key
			}
			return $links;
		}
		return false;
	}//End of method

	//----------------------------------------------------------
	/**
	 * Get documents father links of one document
	 * Return a array with properties of the linked documents
	 * This method return a array with properties of documents calling by link the document with id document_id.
	 * 
	 * @param Array $params, See parameters function of getQueryOptions()
	 * @return Array|Bool
	 */
	function getFathers($params=array()){
		$this->_getQueryOptions($params);

		//Junction of tables doctypes and link_table
		$query = 'SELECT '.$this->selectClose.' FROM '.$this->space->DOC_TABLE.' as dt JOIN '.$this->OBJECT_TABLE.' as dr
	            ON dr.dr_document_id = dt.document_id 
	            WHERE dr.dr_l_document_id = '.$this->document->getId().' '.
				$this->whereClose.' '.
				$this->orderClose;

		if(! $links = $this->dbranchbe->Execute($query) ){
			Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
			return false;
		}else{
			$links = $links->GetArray(); //To transform object result in array;
			foreach ($links as $key => $value){
				$links[$key]['object_class'] = 'Doclink'; //Add the type of object to each key
			}
			return $links;
		}
		return false;
	}//End of method

	//----------------------------------------------------------
	/**
	 * Copy the links of the document to the document t_document_id
	 * Return false or true if success.
	 * 
	 * @param Integer $t_document_id
	 * @return Bool
	 */
	function copySons($t_document_id){
		$links = $this->getSons();//Get links of the document
		foreach($links as $link){
			$data['dr_document_id'] = $t_document_id;
			$data['dr_l_document_id'] = $link['dr_l_document_id'];
			$this->_basicCreate($data);
		}
		return true;
	}//End of method

	//----------------------------------------------------------
	/**
	 * Lock all links of the document document_id with code access_code
	 * When Doclink is locked, he is no more following indice upgrade.
	 * Return false or true if success.
	 * 
	 * @param Integer $code
	 * @return Bool
	 */
	function lockAll($code = 15){
		$data['dr_access_code'] = $code;
		if(!$this->dbranchbe->AutoExecute($this->OBJECT_TABLE , $data , 'UPDATE' , "dr_document_id = ".$this->document->getId())) { //Update table
			Ranchbe::getError()->push(Rb_Error::ERROR, array('query'=>'UPDATE' . implode(';',$data) , 'debug'=>array($data, $this->OBJECT_TABLE, $this->document->getId())), $this->dbranchbe->ErrorMsg());
			return false;
		}
		return true;
	}//End of method

	//----------------------------------------------------------
	/**
	 * Lock one links with code access_code
	 * Return false or true if success.
	 * 
	 * @param Integer $link_id
	 * @param Integer $code
	 * @return Bool
	 */
	function lockDocLink($link_id,$code = 15){
		$data['dr_access_code'] = $code;
		if(!$this->dbranchbe->AutoExecute($this->OBJECT_TABLE , $data , 'UPDATE' , "dr_link_id = ".$link_id)) { //Update table
			Ranchbe::getError()->push(Rb_Error::ERROR,
			array('query'=>'UPDATE' . implode(';',$data) ,
                                        'debug'=>array($data, 
			$this->OBJECT_TABLE,
			$this->document->getId()
			)
			),
			$this->dbranchbe->ErrorMsg()
			);
			return false;
		}
		return true;
	}//End of method

}//End of class

