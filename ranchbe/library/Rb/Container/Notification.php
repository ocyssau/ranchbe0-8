<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

class Rb_Container_Notification extends Rb_Object_Permanent{

	/*
	 DROP TABLE `workitem_cont_notifications`;
	 CREATE TABLE `workitem_cont_notifications` (
	 `notification_id` INT NOT NULL ,
	 `user_id` INT NOT NULL ,
	 `workitem_id` INT NOT NULL ,
	 `event` VARCHAR (64) NOT NULL ,
	 `Rb_Condition` BLOB,
	 PRIMARY KEY  (`notification_id`),
	 KEY `INDEX_workitem_cont_notifications_1` (`user_id`),
	 KEY `INDEX_workitem_cont_notifications_2` (`workitem_id`),
	 KEY `INDEX_workitem_cont_notifications_3` (`event`),
	 UNIQUE KEY `UNIQ_workitem_cont_notifications_1` (`user_id`,`workitem_id`,`event`)
	 ) ENGINE=InnoDB;

	 ALTER TABLE `workitem_cont_notifications`
	 ADD CONSTRAINT `FK_workitem_cont_notifications_1` FOREIGN KEY (`workitem_id`)
	 REFERENCES `workitems` (`workitem_id`) ON DELETE CASCADE;
	 */

	protected $_condition = false; //(Rb_Condition)
	protected static $_registry = array(); //(array) registry of instances

	function __construct(Rb_Space &$space, $notification_id = 0){
		$this->space =& $space;
		$this->OBJECT_TABLE = $this->space->getName().'_cont_notifications';
		$this->FIELDS_MAP_ID = 'notification_id';
		$this->FIELDS_MAP_FATHER = $this->space->getName().'_id';
		$this->_dao = new Rb_Dao( Ranchbe::getDb() );
		$this->_dao->setTable($this->OBJECT_TABLE, 'object');
		$this->_dao->setKey($this->FIELDS_MAP_ID, 'primary_key');
		$this->_dao->setKey($this->FIELDS_MAP_FATHER, 'father');
		$this->_dao->setSequence('notification_seq');
		return $this->_init($notification_id);
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief init and clean current registry
	 */
	public static function initRegistry(){
		self::$_registry = array();
	}

	//-------------------------------------------------------------------------
	/*! \brief return a instance of object.
	 *   if none parameter return a special instance with no possibilies to change here properties
	 *   and wich can not be saved. This instance is just for use in static method where access to database is require,
	 *   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
	 *   If object "$id" dont exist return false.
	 *   If object "$id" has already been instantiated, then return this instance (singleton pattern).
	 *
	 */
	public static function get( $space_name , $id = -1){
		if($id < 0) $id = -1; //forced to value -1

		if($id == 0)
		return new Rb_Container_Notification(Rb_Space::get($space_name), 0);

		if( !self::$_registry[$space_name][$id] ){
			self::$_registry[$space_name][$id] = new Rb_Container_Notification(Rb_Space::get($space_name), $id);
		}
		return self::$_registry[$space_name][$id];
	}//End of method

	//--------------------------------------------------------------------
	public function getProperty($property_name){
		switch($property_name){
			case 'father_id':
			case 'container_id':
				$property_name = $this->FIELDS_MAP_FATHER;
				break;
		} //End of switch

		if( array_key_exists($property_name, $this->core_props) )
		return $this->core_props[$property_name];
	}

	//--------------------------------------------------------------------
	public function setProperty($property_name, $property_value){
		switch($property_name){
			case 'father_id':
			case 'container_id':
				$this->core_props[$this->FIELDS_MAP_FATHER] = $property_value;
				break;
			case '_id':
				break;
			default:
				$this->core_props[$property_name] = $property_value;
				break;
		} //End of switch
		return $this;
	}

	//--------------------------------------------------------------------
	/*! \brief Add a Notification
	 * Return false or integer
	 *
	 */
	protected function _create(){
		if($this->_id < 0)
		return trigger_error('can not performed this action on object id '.$this->_id, 	E_USER_WARNING);


		//Create a basic object
		if( $this->_id = $this->_dao->create( $this->getProperties() ) ){
			self::$_registry[$this->space->getName()][$this->_id] =& $this;
			$this->isSaved = true;
		}else{
			Ranchbe::getError()->error(tra('cant create %element%'), array( 'element'=>$this->getName() ) );
			return $this->_id = 0;
		}

		return $this->_id;

	}//End of method

	//----------------------------------------------------------
	/*! \brief Update a Notification
	 * Return bool
	 *
	 */
	protected function _update(){
		if($this->_id < 1)
		return trigger_error('can not performed this action on object id '.$this->_id, 	E_USER_WARNING);
		return $this->_dao->update($this->getProperties() , $this->_id);
	}//End of method

	//----------------------------------------------------------
	/*! \brief Suppress a Notification
	 * Return bool
	 */
	public function suppress(){
		if($this->_id < 1)
		return trigger_error('can not performed this action on object id '.$this->_id, 	E_USER_WARNING);

		if($ret = $this->_dao->suppress($this->_id)){
			//clean registry
			unset(self::$_registry[$this->space->getName()][$this->_id]);
		}
		return $ret;
	}//End of method

	//----------------------------------------------------------
	public function save(){
		if( $this->_condition ){
			$this->setProperty('Rb_Condition' , serialize($this->_condition));
		}else{
			$this->setProperty('Rb_Condition' , null);
		}
		return parent::save(false);
	}//End of method

	//----------------------------------------------------------
	public function getAll( $container_id=0 , $user_id=0 , $event=0){
		if($container_id)
		$params['exact_find'][$this->space->getName().'_id'] = $container_id;
		if($user_id)
		$params['exact_find']['user_id'] = $user_id;
		if($event)
		$params['exact_find']['event'] = $event;
		return $this->_dao->getAllBasic($params);
	}//End of method

	//----------------------------------------------------------
	public function getCondition(){
		if(!$this->_condition){
			//require_once('core/condition.php');
			$this->_condition = unserialize( $this->getProperty('Rb_Condition') );
		}
		return $this->_condition;
	}//End of method

	//----------------------------------------------------------
	public function setCondition(Rb_Condition &$condition){
		$this->_condition =& $condition;
		return $this;
	}//End of method

	//----------------------------------------------------------
	public function unsetCondition(){
		$this->_condition = false;
		return $this;
	}//End of method

} //End of class

