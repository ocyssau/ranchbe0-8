<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/object/relation.php');

/** Relation between container and categories. This is useful to define a set of
 *  Category in use for a container.
 *
 **/  
class Rb_Container_Relation_Category extends Rb_Object_Relation{

  //-------------------------------------------------------------------------
  public static function get($id = -1){
    if($id < 0) $id = -1; //forced to value -1
    if($id == 0)
      return new self(0);
    if( !Rb_Object_Relation::$_registry[__CLASS__][$id] ){
      Rb_Object_Relation::$_registry[__CLASS__][$id] = new self($id);
    }
    return Rb_Object_Relation::$_registry[__CLASS__][$id];
  }//End of method

  //----------------------------------------------------------
  /*!\brief Get documents metadatas of containers id in $of_parents
  *  @param Mixte, if integer, return childs of the object id,
  *                if array, return childs of each object id in array 
  */
  public function getCategories( $of_parents , array $params=array() ){
    $params['with'][] = array(
                          'table'=> 'categories',
                          'col1'=> 'child_id',
                          'col2'=> 'category_id',
                        );

    $params['with'][] = array(
                          'table'=> 'objects',
                          'col1'=> 'child_id',
                          'col2'=> 'object_id',
                        );

    $params['exact_find']['objects.class_id'] = 100;

    if( is_array($of_parents) ){
      foreach($of_parents as $parent_id){
        if($parent_id > 0)
          $params['where'][] = 'objects_rel.parent_id = '.$parent_id;
      }
    }else{
      if( $of_parents > 0 )
        $params['exact_find']['objects_rel.parent_id'] = $of_parents;
    }

    return $this->_dao->getAllBasic($params);
  }//End of method

} //End of class
