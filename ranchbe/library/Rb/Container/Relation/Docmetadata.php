<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/object/relation.php');

/** This is a link between talbes metadata_dictionary, objects and container to
 *  define wich extend properties must be set for document of the container
 *
 **/  
class Rb_Container_Relation_Docmetadata extends Rb_Object_Relation{

  //-------------------------------------------------------------------------
  public static function get($id = -1){
    if($id < 0) $id = -1; //forced to value -1
    if($id == 0)
      return new self(0);
    if( !Rb_Object_Relation::$_registry[__CLASS__][$id] ){
      Rb_Object_Relation::$_registry[__CLASS__][$id] = new self($id);
    }
    return Rb_Object_Relation::$_registry[__CLASS__][$id];
  }//End of method

  //----------------------------------------------------------
  /**\brief Get metadatas of document for container $id
  *  \return Array
  *  \param Integer
  *  \param Array
  */
  public function getMetadatas( $id , array $params=array() ){
    $params['with'][] = array(
                          'table'=> 'metadata_dictionary',
                          'col1'=> 'child_id',
                          'col2'=> 'property_id',
                        );

    $params['with'][] = array(
                          'table'=> 'objects',
                          'col1'=> 'child_id',
                          'col2'=> 'object_id',
                        );

    $params['exact_find']['objects.class_id'] = 120;

    if( $id > 0 )
      $params['exact_find']['objects_rel.parent_id'] = $id;

    return $this->_dao->getAllBasic($params);
  }//End of method

} //End of class
