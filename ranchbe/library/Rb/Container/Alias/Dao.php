<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/Dao/abstract.php');

/*
 CREATE TABLE `workitem_alias` (
 `alias_id` int(11) NOT NULL ,
 `workitem_id` int(11) NOT NULL,
 `workitem_number` varchar(16)  NOT NULL,
 `workitem_description` varchar(128)  default NULL,
 PRIMARY KEY  (`alias_id`),
 UNIQUE KEY `UC_workitem_alias` (`alias_id`,`workitem_id`),
 KEY `K_workitem_alias_1` (`workitem_id`),
 KEY `K_workitem_alias_2` (`workitem_number`)
 ) ENGINE=InnoDB ;

 --
 -- Contraintes pour la table `workitem_alias`
 --
 ALTER TABLE `workitem_alias`
 ADD CONSTRAINT `FK_workitem_alias_1` FOREIGN KEY (`workitem_id`)
 REFERENCES `workitems` (`workitem_id`) ON DELETE CASCADE;
 */

/*! \brief
 */
class Rb_Container_Alias_Dao extends Rb_Dao_Abstract{

	protected $core_props = array();
	protected $_id = 0;

	//function __construct(rb_container &$container , $alias_id = 0){
	function __construct(&$space , $alias_id = 0){
		$this->dbranchbe =& Ranchbe::getDb();
		//$this->container =& $container;
		$this->space =& $space;
		$this->space_name = $this->space->getName();
		$this->_init($alias_id);
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief init the properties
	 *
	 * \param $alias_id(integer)
	 */
	protected function _init($alias_id){

		$this->OBJECT_TABLE  = $this->space_name.'_alias';
		$this->FIELDS_MAP_ID = 'alias_id';
		$this->FIELDS_MAP_NUM = $this->space->CONT_FIELDS_MAP_NUM;
		$this->FIELDS_MAP_DESC = $this->space->CONT_FIELDS_MAP_DESC;

		$this->FIELDS = array(
		$this->space_name.'_id'       =>$this->OBJECT_TABLE.'.'.$this->space_name.'_id',
		$this->FIELDS_MAP_NUM         =>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_NUM,
		$this->FIELDS_MAP_DESC        =>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_DESC,
                  'object_class'                =>$this->OBJECT_TABLE.'.object_class',
                  'alias_id'                    =>$this->OBJECT_TABLE.'.alias_id',
		);

		if( $alias_id > 0 ){
			$this->_id = (int) $alias_id;
			$this->core_props = $this->getBasicInfos($this->_id);
		}else if( $alias_id < 0 ){
			$this->_id = -1;
		}else{
			$this->_id = 0;
		}

		return true;

	}//End of method

	//--------------------------------------------------------------------
	/*! \brief Add an alias
	 *
	 * \param $params(array)
	 */
	protected function _create( array $data ){
		return $this->_basicCreate( $data );
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief Create a new record in database from properties in array $data,
	 * return the new id or false. Do not init the current instance with properties of $data.
	 *
	 */
	public static function create( $space_name, array $data ){
		return self::get($space_name)->_create($data);
	}//End of method

	//--------------------------------------------------------------------
	/*! \brief Add an alias
	 *
	 * \param $params(array)
	 */
	function save(){
		if($this->_id < 0){
			return false;
		}else
		if($this->_id == 0){
			return $this->_basicCreate( $this->getProperties() );
		}else
		if($this->_id > 0){
			return $this->update( $this->getProperties() );
		}
	}//End of method

	//----------------------------------------------------------
	/*! \brief Update a alias
	 *
	 \param $data(array)
	 */
	function update( array $data ){
		if( $this->_id <= 0){
			Ranchbe::getError()->notice('$this->_id is not set');
			return false;
		}
		unset($data['alias_id']); //to prevent the change of id
		return $this->_basicUpdate($data , $this->_id);
	}//End of method

	//----------------------------------------------------------
	/*! \brief Suppress an alias
	 *
	 \param $alias_id(integer)
	 */
	function suppress( $alias_id ){
		return $this->_basicSuppress($alias_id);
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief
	 * This method can be used to get list of all container.
	 * return a array if success, else return false.
	 \param $params is use for manage the display. See parameters function of _getQueryOptions()
	 */
	function getAll($params = array() ){
		$params['with'][0]['table'] = $this->space->getName().'_alias';
		$params['with'][0]['col'] = $this->FIELDS_MAP_ID;
		return $this->getAllBasic($params);
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get the property of the container by the property name. init() must be call before
	 *
	 * \param $property_name(string) = container_id,container_number,container_description,container_state,container_version.
	 */
	function getProperty($property_name){
		switch($property_name){
			case 'container_id':
				if(isset($this->core_props[ $this->space->CONT_FIELDS_MAP_ID ]))
				return $this->core_props[ $this->space->CONT_FIELDS_MAP_ID ];
				else $realName = $this->space->CONT_FIELDS_MAP_ID;
				break;

			case 'container_number':
				if(isset($this->core_props[$this->FIELDS_MAP_NUM])){
					return $this->core_props[$this->FIELDS_MAP_NUM];
				}else $realName = $this->FIELDS_MAP_NUM;
				break;

			case 'container_description':
				if(isset($this->core_props[$this->FIELDS_MAP_DESC]))
				return $this->core_props[$this->FIELDS_MAP_DESC];
				else $realName = $this->FIELDS_MAP_DESC;
				break;

			default:
				if(isset($this->core_props[$property_name]))
				return $this->core_props[$property_name];
				else $realName = $property_name;
				break;

		} //End of switch

		if( $this->_id <= 0 ){
			Ranchbe::getError()->notice('$this->_id is not set');
			return false;
		}

		$query = 'SELECT '.$realName.' FROM '.$this->OBJECT_TABLE.' WHERE '.$this->FIELDS_MAP_ID.' = \''.$this->_id.'\'';
		$res = $this->dbranchbe->GetOne($query);
		if($res === false){
			Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
			return false;
		}
		else
		return $this->core_props[$realName] = $res;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get the property of the container by the property name. init() must be call before
	 *
	 * \param $property_name(string) = container_id,container_number,container_description,container_state,container_version.
	 */
	function setProperty($property_name, $property_value){
		switch($property_name){
			case 'container_id':
				return $this->core_props[$this->space->CONT_FIELDS_MAP_ID] = $property_value;
				break;

			case 'container_number':
				return $this->core_props[$this->FIELDS_MAP_NUM] = $property_value;
				break;

			case 'container_description':
				return $this->core_props[$this->FIELDS_MAP_DESC] = $property_value;
				break;

			default:
				return $this->core_props[$property_name] = $property_value;
				break;
		} //End of switch
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief return array of properties of the current object.
	 *
	 */
	public function getProperties(){
		if(!isset($this->core_props)) return false;
		else return $this->core_props;
	}//End of method

} //End of class
