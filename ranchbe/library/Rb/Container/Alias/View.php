<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
CREATE ALGORITHM = TEMPTABLE VIEW workitem_alias_view AS 
SELECT 
  workitems.workitem_id,
  workitem_alias.alias_id as alias_id,
  workitem_alias.workitem_number as workitem_number,
  workitem_alias.workitem_description as workitem_description,
  workitems.workitem_state as workitem_state,
  workitems.file_only as file_only,
  workitems.workitem_version as workitem_version,
  workitems.project_id as project_id,
  workitems.default_process_id as default_process_id,
  workitems.default_file_path as default_file_path,
  workitems.open_date as open_date,
  workitems.open_by as open_by,
  workitems.forseen_close_date as forseen_close_date,
  workitems.close_date as close_date,
  workitems.close_by as close_by,
  workitems_extends.*
FROM workitems 
RIGHT OUTER JOIN workitem_alias ON workitem_alias.workitem_id = workitems.workitem_id
LEFT OUTER JOIN workitems_extends ON workitems_extends.extend_id = workitems.workitem_id
UNION
SELECT
  workitems.workitem_id,
  "" as alias_id,
  workitems.workitem_number as workitem_number,
  workitems.workitem_description as workitem_description,
  workitems.workitem_state as workitem_state,
  workitems.file_only as file_only,
  workitems.workitem_version as workitem_version,
  workitems.project_id as project_id,
  workitems.default_process_id as default_process_id,
  workitems.default_file_path as default_file_path,
  workitems.open_date as open_date,
  workitems.open_by as open_by,
  workitems.forseen_close_date as forseen_close_date,
  workitems.close_date as close_date,
  workitems.close_by as close_by,
  workitems_extends.*
FROM workitems 
LEFT OUTER JOIN workitem_alias ON workitem_alias.workitem_id = workitems.workitem_id
LEFT OUTER JOIN workitems_extends ON workitems_extends.extend_id = workitems.workitem_id
*/

/*
CREATE ALGORITHM = TEMPTABLE VIEW workitem_alias_view AS 
SELECT 
  workitems.workitem_id,
  workitem_alias.alias_id as alias_id,
  workitem_alias.workitem_number as workitem_number,
  workitem_alias.workitem_description as workitem_description,
  workitems.workitem_state as workitem_state,
  workitems.file_only as file_only,
  workitems.workitem_version as workitem_version,
  workitems.project_id as project_id,
  workitems.default_process_id as default_process_id,
  workitems.default_file_path as default_file_path,
  workitems.open_date as open_date,
  workitems.open_by as open_by,
  workitems.forseen_close_date as forseen_close_date,
  workitems.close_date as close_date,
  workitems.close_by as close_by
FROM workitems 
RIGHT OUTER JOIN workitem_alias ON workitem_alias.workitem_id = workitems.workitem_id
UNION
SELECT
  workitems.workitem_id,
  "" as alias_id,
  workitems.workitem_number as workitem_number,
  workitems.workitem_description as workitem_description,
  workitems.workitem_state as workitem_state,
  workitems.file_only as file_only,
  workitems.workitem_version as workitem_version,
  workitems.project_id as project_id,
  workitems.default_process_id as default_process_id,
  workitems.default_file_path as default_file_path,
  workitems.open_date as open_date,
  workitems.open_by as open_by,
  workitems.forseen_close_date as forseen_close_date,
  workitems.close_date as close_date,
  workitems.close_by as close_by
FROM workitems 
LEFT OUTER JOIN workitem_alias ON workitem_alias.workitem_id = workitems.workitem_id

*/


//require_once('core/dao/View.php');

/*! \brief
*/
class Rb_Container_Alias_View extends Rb_Dao_View{

  public function __construct($space){
    $this->dbranchbe =& Ranchbe::getDb();
    $this->OBJECT_TABLE = $space->getName().'_alias_view';
    $this->FIELDS_MAP_ID = $space->getName().'_id';
    $this->FIELDS_MAP_NUM = $space->getName().'_number';
    $this->FIELDS_MAP_DESC = $space->getName().'_description';
    $this->FIELDS_MAP_STATE = $space->getName().'_state';
    $this->FIELDS_MAP_VERSION = $space->getName().'_version';
    $this->FIELDS_MAP_ITERATION = $space->getName().'_iteration';
    $this->FIELDS_MAP_FATHER = $space->getName().'project_id';
  }//End of method

  public function getAll(array $params = array() ){
    return $this->getAllBasic($params);
  }//End of method

} //End of class
