<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/space.php');
//require_once('core/container.php');

/** container of Workitem space. Workitem is linked to only and at least one project. The other containers type can be linked to 0 or n projects
 */
class Rb_Container_Workitem extends Rb_Container{

	protected $father = false; //Object of the father project

	//Tables
	protected $OBJECT_TABLE = 'workitems'; //(String)

	//Fields:
	protected $FIELDS_MAP_ID         = 'workitem_id'; //(String)primary key
	protected $FIELDS_MAP_NUM        = 'workitem_number'; //(String)
	protected $FIELDS_MAP_NAME       = 'workitem_name'; //(String)
	protected $FIELDS_MAP_DESC       = 'workitem_description'; //(String)
	protected $FIELDS_MAP_STATE      = 'workitem_state'; //(String)
	protected $FIELDS_MAP_VERSION    = 'workitem_version'; //(String)
	protected $FIELDS_MAP_ITERATION  = 'workitem_iteration'; //(String)
	protected $FIELDS_MAP_FATHER     = 'project_id'; //(String)
	
	/** id of default parent resource
	 *
	 * @var Integer
	 */
	protected static $_defaultResource_id = 6;
	

	//-------------------------------------------------------------------------
	protected function __construct($container_id = 0){
		$this->space =& Rb_Space::get('workitem');
		parent::_init($container_id);
	}//End of method

	//----------------------------------------------------------
	/*! \brief Define the father project of the Workitem.
	 *
	 */
	public function setProject($project){
		if( $this->_id < 0 ){
			trigger_error( '$this->_id is not set' );
			return false;
		}
		$this->setProperty('project_id', $project->getId() );
		return $this->father =& $project;
	}//End of method

	//-------------------------------------------------------------------------
	public function getProject(){
		if( $this->father ) return $this->father;

		if( $this->_id < 0 ){
			trigger_error( '$this->_id is not set', E_USER_WARNING );
			return false;
		}

		//require_once('core/project.php');
		return $this->father =& Rb_Project::get( $this->getProperty('project_id') );
	}//End of method

	//-------------------------------------------------------------------------
	/*!\brief get the father object of current object
	 * Return Rb_Object_Permanent or false.
	 *
	 */
	public function getFather(){
		return $this->getProject();
	}//End of method

	//-------------------------------------------------------------------------
	/*!\brief set father of current object
	 * Return $this or false.
	 *
	 */
	public function setFather( &$project ){
		return $this->setProject($project);
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get all doctypes linked to this container.
	 * Return an array with the doctypes of the container and of the father project
	 *
	 \param $extension(string) limit result to doctype wich accept extension.
	 \param $type(string) limit result to doctype wich accept the type.
	 \param $params(array) is use for manage the display. See parameters function of getQueryOptions().
	 \param $recursive(bool) true to get the doctype defined on the father project.
	 */
	function getDoctypes($extension=0 , $type=0, $recursive=false){
		if( !$this->_doctypes[$extension][$type] ){
			$this->_doctypes[$extension][$type] =
			Rb_Container_Relation_Doctype::get()
			->getDoctypes($extension,
			$type,
			array($this->_id,
			$this->getProperty('project_id')
			)
			);
		}
		return $this->_doctypes[$extension][$type];
	}//End of method

	//----------------------------------------------------------
	/*!\brief
	 * Get the process linked to the doctype for the container
	 *
	 \param $doctype_id(integer) Primary key of doctype.
	 */
	/*
	 function getDoctypesProcess($doctype_id, $recursive=false){
	 if( $this->_id < 1 ){
	 Ranchbe::getError()->error('$this->_id is not set');
	 return false;
	 }
	 //Get the process linked to the doctype/container
	 $p1['exact_find']['workitem_doctype_process.doctype_id'] = $doctype_id;
	 $p1['select'] = array('process_id', 'link_id');
	 $res = $this->getDoctypes(NULL, NULL, $p1, false);
	 if(!empty($res[0]['process_id'])) return $res[0]['process_id'];

	 if($recursive){ //Get the process linked to the doctype/project
	 $p2['exact_find']['project_doctype_process.doctype_id'] = $doctype_id;
	 $p2['select'] = array('process_id', 'link_id');
	 $res = $this->getFather()->getDoctypes(NULL, NULL, $p2); //-- Get doctype linked to father project
	 if(!empty($res[0]['process_id'])) return $res[0]['process_id'];
	 }

	 return false;
	 }//End of method
	 */

	//----------------------------------------------------------
	/*!\brief
	 * Check if there is at least one doctype linked to container
	 * Return false or true
	 */
	function hasDoctype(){
		if( $this->_id < 1 ){
			trigger_error('$this->_id is not set', E_USER_WARNING);
			Ranchbe::getError()->notice('$this->_id is not set');
			return false;
		}
		return Rb_Container_Relation_Doctype::get()->hasDoctypes(
		array($this->_id,
		$this->getProperty('project_id')
		)
		);
	}//End of method

}//End of class
