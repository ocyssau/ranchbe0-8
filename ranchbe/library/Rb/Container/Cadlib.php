<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/space.php'); //Class to manage the bookshop
//require_once('core/container.php'); //Class to manage the bookshop

/** container of Cadlib space.
 */
class Rb_Container_Cadlib extends Rb_Container{
	public $SPACE_NAME = 'Cadlib'; //(String)

	//Tables
	protected $OBJECT_TABLE = 'cadlibs'; //(String)
	protected $INDICE_TABLE = 'indices'; //(String)

	//Fields:
	protected $FIELDS_MAP_ID         = 'cadlib_id'; //(String)primary key
	protected $FIELDS_MAP_NUM        = 'cadlib_number'; //(String)
	protected $FIELDS_MAP_NAME       = 'cadlib_name'; //(String)
	protected $FIELDS_MAP_DESC       = 'cadlib_description'; //(String)
	protected $FIELDS_MAP_STATE      = 'cadlib_state'; //(String)
	protected $FIELDS_MAP_VERSION    = 'cadlib_version'; //(String)
	protected $FIELDS_MAP_ITERATION  = 'cadlib_iteration'; //(String)
	protected $FIELDS_MAP_FATHER     = 'project_id'; //(String)

	/** id of default parent resource
	 *
	 * @var Integer
	 */
	protected static $_defaultResource_id = 4;
	
	function __construct($container_id=NULL){
		$this->space        =& Rb_Space::get('cadlib');
		parent::_init($container_id);
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 */
	protected function _initFather(){
		return;
	}//End of method

}//End of class

