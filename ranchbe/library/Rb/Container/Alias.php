<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/container.php');

/*! \brief
*/
class Rb_Container_Alias extends Rb_Container{

function __construct(Rb_Space &$space , $alias_id=NULL){
  $this->space        =& $space;

  parent::_init($alias_id);

  //Tables
  $this->OBJECT_TABLE = $this->space->getName().'s'; //(String)
  
  //Fields:
  $this->FIELDS_MAP_ID         = $this->space->getName().'_id'; //(String)primary key
  $this->FIELDS_MAP_NUM        = $this->space->getName().'_number'; //(String)
  $this->FIELDS_MAP_DESC       = $this->space->getName().'_description'; //(String)
  $this->FIELDS_MAP_STATE      = $this->space->getName().'_state'; //(String)
  $this->FIELDS_MAP_VERSION    = $this->space->getName().'_indice_id'; //(String)

  $this->FIELDS = array(
                        'container_id'=>$this->space->getName().'_alias.workitem_id',
                        'container_number'=>$this->space->getName().'_alias.workitem_number',
                        'container_description'=>$this->space->getName().'_alias.workitem_description',
                        'object_class'=>$this->space->getName().'_alias.object_class',
                        'container_state'=>$this->OBJECT_TABLE.'.workitem_state',
                        'file_only'=>$this->OBJECT_TABLE.'.file_only',
                        'container_version'=>$this->OBJECT_TABLE.'.workitem_version',
                        'project_id'=>$this->OBJECT_TABLE.'.project_id',
                        'default_process_id'=>$this->OBJECT_TABLE.'.default_process_id',
                        'default_file_path'=>$this->OBJECT_TABLE.'.default_file_path',
                        'open_date'=>$this->OBJECT_TABLE.'.open_date',
                        'open_by'=>$this->OBJECT_TABLE.'.open_by',
                        'forseen_close_date'=>$this->OBJECT_TABLE.'.forseen_close_date',
                        'close_date'=>$this->OBJECT_TABLE.'.close_date',
                        'close_by'=>$this->OBJECT_TABLE.'.close_by',
                        'alias_id'=>$this->space->getName().'_alias.alias_id',
                        );

  $this->FIELDS[$this->FIELDS_MAP_ID]     = $this->space->getName().'_alias.'.$this->FIELDS_MAP_ID;
  $this->FIELDS[$this->FIELDS_MAP_NUM]    = $this->space->getName().'_alias.'.$this->FIELDS_MAP_NUM;
  $this->FIELDS[$this->FIELDS_MAP_DESC]   = $this->space->getName().'_alias.'.$this->FIELDS_MAP_DESC;
  $this->FIELDS['object_class']           = $this->space->getName().'_alias.object_class';
  $this->FIELDS['alias_id']           = $this->space->getName().'_alias.alias_id';

}//End of method

//--------------------------------------------------------------------
/*!\brief
* This method can be used to get list of all container.
* return an array if success, else return false.
\param $params is use for manage the display. See parameters function of _getQueryOptions()
*/
function getAll( $params = array() ){
  $params['with'][0]['table'] = $this->space->getName().'_alias';
  $params['with'][0]['col'] = $this->FIELDS_MAP_ID;
  return $this->getAllBasic($params);
}//End of method

//-------------------------------------------------------------------------
/*! \brief this object is a ressource, so this method must be defined.
* See the zend_acl documentation
* 
*/
function getResourceId(){
  return self::getStaticResourceId($this->getId());
}//End of method

//----------------------------------------------------------------------------
/*! \brief defintion of method from Zend_Acl_Role_Interface. See Zend documentation
* so this class is a role
*/
public static function getStaticResourceId($id){
  return 'container_alias_'.$id;
}//End of method

} //End of class
