<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
 CREATE TABLE `doctypes` (
 `doctype_id` int(11) NOT NULL default '0',
 `doctype_number` varchar(64) NOT NULL default '',
 `doctype_description` varchar(128) default NULL,
 `can_be_composite` tinyint(4) NOT NULL default '1',
 `file_extension` varchar(32) default NULL,
 `file_type` varchar(128) default NULL,
 `icon` varchar(32) default NULL,
 `script_post_store` varchar(64) default NULL,
 `script_pre_store` varchar(64) default NULL,
 `script_post_update` varchar(64) default NULL,
 `script_pre_update` varchar(64) default NULL,
 `recognition_regexp` text,
 `visu_file_extension` varchar(32) default NULL,
 PRIMARY KEY  (`doctype_id`),
 UNIQUE KEY `UC_doctype_number` (`doctype_number`)
 ) ENGINE=InnoDB;
 */

//require_once('core/object/permanent.php');

/*! \brief This class manage the doctypes.
 * A Doctype type the document and can be use to restrict the file name, file extension, and file type
 * and defined particular action to apply on document respecting a name convention
 */
class Rb_Doctype extends Rb_Object_Permanent{
	
	/**
	 * 
	 * @var Integer
	 */
	protected $_id = 0; //(Integer) id of the current Doctype

	/**
	 * 
	 * @var Array
	 */
	protected $core_props = array(
                                'doctype_number'=>'',
                                'doctype_description'=>'',
                                'can_be_composite'=>1,
                                'file_extension'=>'',
                                'file_type'=>'',
                                'icon'=>'',
                                'script_post_store'=>'',
                                'script_pre_store'=>'',
                                'script_post_update'=>'',
                                'script_pre_update'=>'',
                                'recognition_regexp'=>'',
                                'visu_file_extension'=>'',
	); //(Array)

	protected $OBJECT_TABLE = 'doctypes';
	protected $FIELDS_MAP_ID = 'doctype_id';
	protected $FIELDS_MAP_NUM = 'doctype_number';
	protected $FIELDS_MAP_NAME = 'doctype_number';
	protected $FIELDS_MAP_DESC = 'doctype_description';

	protected static $_registry = array(); //(array) registry of constructed objects

	//-------------------------------------------------------------------------
	/**
	 * @param Integer$doctype_id
	 * @return Void
	 */
	function __construct($doctype_id=NULL){
		$this->dbranchbe =& Ranchbe::getDb();
		$this->_init($doctype_id);
	}//End of method

	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_init($id)
	 */
	protected function _init($id){
		$this->_dao = new Rb_Dao( Ranchbe::getDb() );
		$this->_dao->setTable($this->OBJECT_TABLE, 'object');
		$this->_dao->setKey($this->FIELDS_MAP_ID, 'primary_key');
		$this->_dao->setKey($this->FIELDS_MAP_NUM, 'number');
		$this->_dao->setKey($this->FIELDS_MAP_NAME, 'name');
		$this->_dao->setKey($this->FIELDS_MAP_DESC, 'description');
		return parent::_init($id);
	} //End of method


	//-------------------------------------------------------------------------
	/** Factory method with singleton pattern
	 * 
	 * @param Integer $id
	 * @return Rb_Doctype
	 */
	public static function get( $id = 0){
		if($id < 0) $id = -1; //forced to value -1
		if($id == 0)
		return new Rb_Doctype(0);
		if( !self::$_registry[$id] ){
			self::$_registry[$id] = new Rb_Doctype($id);
		}
		return self::$_registry[$id];
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#getProperty($property_name)
	 */
	function getProperty($property_name){
		switch($property_name){
			case 'number':
				$property_name = 'doctype_number';
				break;
			case 'name':
				$property_name = 'doctype_number';
				break;
			case 'description':
				$property_name = 'doctype_description';
				break;
			case 'can_be_composite':
			case 'file_extension':
			case 'file_type':
			case 'icon':
			case 'script_post_store':
			case 'script_pre_store':
			case 'script_post_update':
			case 'script_pre_update':
			case 'recognition_regexp':
			case 'visu_file_extension':
				break;
		}
		if( array_key_exists($property_name, $this->core_props) )
		return $this->core_props[$property_name];
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#setProperty($property_name, $property_value)
	 */
	function setProperty($property_name, $property_value){
		if($this->getId() < 0) return false; //Can not change property on object -1
		$this->isSaved = false;
		switch($property_name){
			case 'number':
				$property_name = 'doctype_number';
				break;
			case 'name':
			case 'doctype_name':
				$property_name = 'doctype_number';
				break;
			case 'description':
				$property_name = 'doctype_description';
				break;
			case 'doctype_number':
			case 'doctype_description':
				break;
			case 'file_extension':
			case 'file_type':
			case 'visu_file_extension':
				if(is_array($property_value)){
					$property_value = implode(' ' , $property_value);
				}
				break;
			case 'can_be_composite':
				if($property_value) $property_value = 1;
				else $property_value = 0;
				break;
			case 'icon':
			case 'script_post_store':
			case 'script_pre_store':
			case 'script_post_update':
			case 'script_pre_update':
			case 'recognition_regexp':
				break;
			default:
				return false;
				break;
		}
		$this->core_props[$property_name] = $property_value;
		return $this;
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_create()
	 */
	public function _create(){
		if( $this->_id !== 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}

		//Create a basic object
		$this->_id = $this->_dao->create( $this->getProperties() );
		if( $this->_id ){
			self::$_registry[$this->_id] =& $this;
			$this->isSaved = true;
			self::compileIcon($this->getProperty('icon'), $this->_id);
		}else{
			Ranchbe::getError()->push(Rb_Error::ERROR,
			array( 'element'=>$this->getNumber() ),
			tra('cant create %element%') );
			return $this->_id = 0;
		}
		return $this->_id;
	}//End of method

	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_update()
	 */
	protected function _update(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}
		self::compileIcon($this->getProperty('icon'), $this->_id);
		return $this->_dao->update( $this->getProperties() , $this->_id);
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief Create a new doctypes from the properties of the param $data.
	 * return the id if success, else return false.
	 *
	 \param $data(array) field=>value of the properties to records.
	 \param  $replace(array) if true, update the existing Doctype.
	 */
	/*
	 function create($data , $replace=false) {

	 if(is_array($data['file_type'])){
	 $data['file_type'] = implode(' ' , $data['file_type']);
	 }

	 if(is_array($data['file_extension'])){
	 $data['file_extension'] = implode(' ' , $data['file_extension']);
	 }

	 if ($replace){
	 if ($id = $this->ifExist($data['doctype_number'])){
	 self::get($id)->_dao()->update($data);
	 $this->compileIcon( $data['icon'] , $id);
	 return $id;
	 }else $replace = false;
	 }

	 if (!$replace){
	 if (!$id = $this->_dao->_basicCreate($data) ) return false;
	 else $this->compileIcon( $data['icon'] , $id);
	 }

	 return $id;

	 }//End of method
	 */
	//---------------------------------------------------------------------------
	/** Test if the Doctype exist.
	 * return doctype_id, else return false.
	 * 
	 * @param String $doctype_number
	 * @return Integer
	 */
	function ifExist($doctype_number){
		$params = array(
                'select'=>array('doctype_id'),
                'exact_find'=>array('doctype_number'=>$doctype_number),
		);
		if(!$ret = $this->_dao->getOne($params)) return false;
		else return $ret;

	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief Modify a Doctype.
	 * return true or false.
	 *
	 \param $data(array) field=>value of the properties to records.
	 */
	/*
	 function update($data) {
	 if( !$this->_id ){
	 trigger_error('$this->_id is not set', E_USER_WARNING);
	 return false;
	 }

	 if(is_array($data['file_type'])){
	 $data['file_type'] = implode(' ' , $data['file_type']);
	 }

	 if(is_array($data['file_extension'])){
	 $data['file_extension'] = implode(' ' , $data['file_extension']);
	 }

	 if ( !$this->_basicUpdate($data, $this->_id) ) return false;

	 foreach($data as $property_name=>$property_value){
	 $this->setProperty( $property_name , $property_value );
	 }

	 $this->compileIcon( $data['icon'] , $this->_id);

	 return $this;

	 }//End of method
	 */

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#suppress()
	 */
	function suppress(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}

		if($ret = $this->_dao->suppress($this->_id)){
			$this->_suppressPermanentObject();
			//clean registry
			unset( self::$_registry[$this->_id] );
		}
		return $ret;
	}//End of method

	//---------------------------------------------------------------------------
	/** Get list of all doctypes.
	 * return a array if success, else return false.
	 * 
	 * @param Array $params, See parameters function of getQueryOptions()
	 * @return Array|Bool
	 */
	function getAll(array $params = array()){
		return $this->_dao->getAllBasic($params);
	}//End of method

	//---------------------------------------------------------------------------
	/** Get info about a single Doctype by his doctype_id.
	 * return a array if success, else return false.
	 * 
	 * @param Integer $doctype_id
	 * @param array $selectClose
	 * @return Array|Bool
	 */
	public static function getInfos( $doctype_id , array $selectClose = array()){
		return self::get()->getDao()->getBasicInfos($doctype_id, array('select'=>$selectClose));
	}//End of method

	//---------------------------------------------------------------------------
	/**
	 * Create a icon file in "C" dir for icon to associate to Doctype.
	 *   Return true if no errors or false
	 * 
	 * @param String $icon
	 * @param Integer $doctype_id
	 * @return Bool
	 */
	public static function compileIcon( $icon , $doctype_id){
		$source_icon_dir = Ranchbe::getConfig()->icons->doctype->source->path;
		$compiled_icon_dir = Ranchbe::getConfig()->icons->doctype->compiled->path;
		if(is_file ( $source_icon_dir .'/'. $icon )){
			if(filesize($source_icon_dir .'/'. $icon) > 10000){
				Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$icon),
                                     'Icon file must be less than 10Ko <br />');
				return false;}
				$path_parts = pathinfo($icon);
				if( !is_dir($compiled_icon_dir)  ){
					if(!mkdir($compiled_icon_dir, 0755)) return false;
				}
				copy($source_icon_dir.'/'.$icon ,
				$compiled_icon_dir.'/'.$doctype_id.'.'.$path_parts['extension']);
		}
		return true;
	}//End of method

}//End of class
