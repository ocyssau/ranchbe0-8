<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*! \brief 
*
*/
class Rb_Viewer_Default {

  protected $_viewer; //(Rb_Viewer)

  public function __construct($viewer){
    $this->_viewer = $viewer;
  }//End of method
       
  public static function embededViewer($file){
  }//End of method

  public function viewFile($file){
    return $this->viewRawFile($file);
  }//End of method

//-------------------------------------------------------
  /*! \brief View the document file.
  *
  * This method send the content of the $file to the client browser. It send too the mimeType to help the browser to choose the right application to use for open the file.
  * If the mimetype = no_read, display a error.
  * Return true or false.
    \param $file(string) path of the file to display on the client computer
  */
  public function viewRawFile($file) {
    $fileName = basename($file);
    if ($this->_viewer->noread == 'no_read') {
      Ranchbe::getError()->error('you cant view this file %element% by this way', array('element'=>$file));
      return false;
    }

    header("Content-disposition: attachment; filename=$fileName");
    header("Content-Type: " . $this->_viewer->mimetype);
    header("Content-Transfer-Encoding: $fileName\n"); // Surtout ne pas enlever le \n
    header("Content-Length: ".filesize($file));
    header("Pragma: no-cache");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
    header("Expires: 0");
    readfile($file);
    die;
  }//End of method

} //End of class

