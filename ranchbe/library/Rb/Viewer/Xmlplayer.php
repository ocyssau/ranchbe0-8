<?php

// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*! \brief 
*
*/

class Rb_Viewer_Xmlplayer extends Rb_Viewer_Default {

  function __construct($viewer){
    $this->_viewer = $viewer;
  }//End of method

  public static function embededViewer($file){
    return ('
        <object type="application/x-3dxmlplugin" id="Xmlplayer" width="500" height="300" style="MARGIN: 2px" border="0">
        <param name="DocumentFile" value="'.$file.'"">
        </object>
    ');
  }//End of method
} //End of class


