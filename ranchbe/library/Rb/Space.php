<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//@todo : Revoir utilit� des variables SPEC_FIELD, FIELDS...

/*! \brief This class manage the Space: Set database parameters.
 * Space is use to define database fields name for each family of datas.
 * Space bookshop, cadlib, mockup, workitem have specific table and field name.
 * Exemple : bookshops -> bookshop_id ; workitems -> workitem_id ; workitem_document ; bookshop_document...
 * Properties record in Space permit to define common methods to access to data in database.
 */
class Rb_Space{

	protected $SPACE_NAME; //(String)Name of the Space : workitem, project, bookshop...etc.
	protected $_id=0; //(Integer) identifier for Space
	protected static $_registry = array(); //(array) registry of constructed objects

	//For the files
	//Tables
	public $DOC_FILE_TABLE; //(String)Name of the table where are stored the document files definition
	public $FILE_TABLE; //(String)Name of the table where are stored the files definition

	//Others
	public $DEFAULT_REPOSIT_DIR; //(String)Path to the default reposit dir

	public $LINK_FILE_TABLE; //(String)Name of table where are stored the definition of links between files
	public $LINK_DOC_TABLE; //(String) Name of table where are stored the definition of links between documents

	public $SPEC_FIELDS; //(Array) ????
	public $FIELDS; //(Array) ????

	public $FIELDS_MAPPING; //(Array) To associate a description to the fields name

	//-----------------------------
	//For the containers:
	//Tables:
	public $CONT_TABLE; //(String)Table where are stored the containers definitions
	public $CONT_INDICE_TABLE; //(String)Table where are stored the container indices
	//public $CONT_METADATA_TABLE; //(String)Table where are stored the metadatas
	//Fields:
	public $CONT_FIELDS_MAP_ID; //(String)Name of the field of the primary key of the container table
	public $CONT_FIELDS_MAP_NUM; //(String)Name of the field to define the name of the container
	public $CONT_FIELDS_MAP_DESC; //(String)Name of the field where is store the description of the container
	public $CONT_FIELDS_MAP_STATE; //(String)Name of the field where is store the state of the container
	public $CONT_FIELDS_MAP_VERSION; //(String)Name of the field where is store the indice of the container
	public $CONT_FIELDS_MAP_REPOSITDIR; //(String)Name of the field where is store the default reposit directory of the container
	public $CONT_FIELDS_MAP_FATHER; //(String)Name of the field where is store the id of the father

	//For the documents
	//Tables
	public $DOC_TABLE; //(String)Name of the table where are stored the documents
	public $DOC_INDICE_TABLE; //(String)Table where are stored the container indices
	//public $DOC_METADATA_TABLE; //(String)Table where are stored the metadatas
	//Fields
	public $DOC_FIELDS_MAP_ID; //(String)Name of the field of the primary key of the container table
	public $DOC_FIELDS_MAP_NUM; //(String)Name of the field to define the name of the container
	public $DOC_FIELDS_MAP_DESC; //(String)Name of the field where is store the description of the container
	public $DOC_FIELDS_MAP_STATE; //(String)Name of the field where is store the state of the container
	public $DOC_FIELDS_MAP_VERSION; //(String)Name of the field where is store the indice of the container
	public $DOC_FIELDS_MAP_REPOSITDIR; //(String)Name of the field where is store the default reposit directory of the container
	public $DOC_FIELDS_MAP_FATHER; //(String)Name of the field where is store the id of the father
	public $DOC_TABLE_ID; //(String)Name of the field of the primary key of the document table

	//----------------------------------------------------------
	/**
	 * @param integer | string		Space identifiant or name
	 */
	function __construct($space_id){
		//var_dump($space_id);die;
		switch($space_id){
			case(10):
			case('bookshop'):
				$this->SPACE_NAME = 'bookshop';
				$this->_id = 10;
				$this->init();
				$this->initBookshop();
				break;
			case(15):
			case('cadlib'):
				$this->SPACE_NAME = 'cadlib';
				$this->_id = 15;
				$this->init();
				$this->initCadlib();
				break;
			case(20):
			case('mockup'):
				$this->SPACE_NAME = 'mockup';
				$this->_id = 20;
				$this->init();
				$this->initMockup();
				break;
			case(25):
			case('workitem'):
				$this->SPACE_NAME = 'workitem';
				$this->_id = 25;
				$this->init();
				$this->initWorkitem();
				break;
			case(30):
			case('project'):
				$this->SPACE_NAME = 'project';
				$this->_id = 30;
				$this->_initProject();
				break;
			default:
				trigger_error('Space object : Unknow object type', E_USER_ERROR);
				die;
				break;
		}
	} //End of method

	//----------------------------------------------------------
	/** Accesor to objects
	 *
	 * @param string | integer $space_id
	 * @return Rb_Space
	 */
	public static function get($space_id){
		switch($space_id){
			case(10):
				$space_id = 'bookshop';
				break;
			case(15):
				$space_id = 'cadlib';
				break;
			case(20):
				$space_id = 'mockup';
				break;
			case(25):
				$space_id = 'workitem';
				break;
		}
		if( !$_registry[$space_id] ){
			self::$_registry[$space_id] = new Rb_Space($space_id);
			//var_dump(self::$_registry[$space_id]);
			//die;
		}
		return self::$_registry[$space_id];
	} //End of method

	//----------------------------------------------------------
	/** Return name of space
	 *
	 * @return string
	 */
	public function getName(){
		return $this->SPACE_NAME;
	} //End of method

	//----------------------------------------------------------
	/** Id of space
	 * 
	 * @return integer
	 */
	public function getId(){
		return $this->_id;
	} //End of method

	//----------------------------------------------------------
	/**
	 * @return void
	 */
	private function init(){
		//Container
		//Tables
		$this->CONT_TABLE   = $this->SPACE_NAME.'s';
		$this->CONT_INDICE_TABLE    =  'indices';
		//$this->CONT_METADATA_TABLE  = $this->SPACE_NAME.'_cont_metadata';
		//Fields
		$this->CONT_FIELDS_MAP_ID = $this->SPACE_NAME.'_id';
		$this->CONT_FIELDS_MAP_NUM = $this->SPACE_NAME.'_number';
		$this->CONT_FIELDS_MAP_DESC = $this->SPACE_NAME.'_description';
		$this->CONT_FIELDS_MAP_STATE = $this->SPACE_NAME.'_state';
		$this->CONT_FIELDS_MAP_VERSION = $this->SPACE_NAME.'_indice_id';
		$this->CONT_FIELDS_MAP_FATHER = 'project_id';

		//Document
		//Tables
		$this->DOC_TABLE            = $this->SPACE_NAME.'_documents';
		$this->DOC_INDICE_TABLE     =  'indices';
		//$this->DOC_METADATA_TABLE = $this->SPACE_NAME.'_metadata';
		//Fields
		$this->DOC_FIELDS_MAP_ID = 'document_id';
		$this->DOC_FIELDS_MAP_NUM = 'document_number';
		$this->DOC_FIELDS_MAP_DESC = 'description';
		$this->DOC_FIELDS_MAP_STATE = 'document_state';
		$this->DOC_FIELDS_MAP_VERSION = 'document_version';
		$this->DOC_FIELDS_MAP_FATHER = $this->CONT_FIELDS_MAP_ID;

		//Files
		$this->DOC_FILE_TABLE     = $this->SPACE_NAME.'_doc_files';
		$this->FILE_TABLE         = $this->SPACE_NAME.'_files';

		$this->LINK_FILE_TABLE = 'documents_files_links';
		$this->LINK_DOC_TABLE = 'documents_container_links';

		$this->READ_DIR = DEFAULT_READ_DIR;
	}

	//----------------------------------------------------------
	/**
	 * @return void
	 */
	private function initWorkitem(){
		$this->FIELDS_MAP_REPOSITDIR = 'default_file_path';
		$this->DEFAULT_REPOSIT_DIR = Ranchbe::getConfig()->path->reposit->workitem;
		$this->FIELDS_MAPPING['project_number'] = 'Project';
		$this->FIELDS_MAPPING['default_file_path'] = 'Reposit dir';
	}
	//----------------------------------------------------------
	/**
	 * @return void
	 */
	private function initBookshop(){
		$this->FIELDS_MAP_REPOSITDIR = 'default_file_path';
		$this->DEFAULT_REPOSIT_DIR = Ranchbe::getConfig()->path->reposit->bookshop;
		$this->FIELDS_MAPPING['default_file_path'] = 'Reposit dir';
	}
	//----------------------------------------------------------
	/**
	 * @return void
	 */
	private function initCadlib(){
		$this->FIELDS_MAP_REPOSITDIR = 'default_file_path';
		$this->DEFAULT_REPOSIT_DIR = Ranchbe::getConfig()->path->reposit->cadlib;
		$this->FIELDS_MAPPING['default_file_path'] = 'Reposit dir';
	}
	//----------------------------------------------------------
	/**
	 * @return void
	 */
	private function initMockup(){
		$this->FIELDS_MAP_REPOSITDIR = 'default_file_path';
		$this->DEFAULT_REPOSIT_DIR = Ranchbe::getConfig()->path->reposit->mockup;
		$this->FIELDS_MAPPING['default_file_path'] = 'Reposit dir';
	}

	//----------------------------------------------------------
	/**
	 * @return void
	 */
	private function _initProject(){
		//Tables
		$this->CONT_TABLE   = 'projects';
		$this->CONT_INDICE_TABLE    =  'indices';
		//Fields
		$this->CONT_FIELDS_MAP_ID = $this->SPACE_NAME.'_id';
		$this->CONT_FIELDS_MAP_NUM = $this->SPACE_NAME.'_number';
		$this->CONT_FIELDS_MAP_NAME = $this->SPACE_NAME.'_name';
		$this->CONT_FIELDS_MAP_DESC = $this->SPACE_NAME.'_description';
		$this->CONT_FIELDS_MAP_STATE = $this->SPACE_NAME.'_state';
		$this->CONT_FIELDS_MAP_VERSION = $this->SPACE_NAME.'_indice_id';
		$this->CONT_FIELDS_MAP_FATHER = 'project_id';
	}

}//End of class
