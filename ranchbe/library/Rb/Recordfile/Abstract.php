<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+



//require_once('core/object/acl.php');
//require_once('core/space.php');

/** Its a record in database of a file linked to a container.
 * 
 * @author Olivier CYSSAU
 * 
 * This class define the followings events
 * recordfile_pre_move
 * recordfile_post_move
 * recordfile_pre_copy
 * recordfile_post_copy
 * recordfile_pre_copyfile
 * recordfile_post_copyfile
 * recordfile_post_putInWildspace
 *
 */
abstract class Rb_Recordfile_Abstract extends Rb_Object_Acl implements Rb_Object_Interface_Related {
	protected $OBJECT_TABLE; //(String)Table
	protected $INDICE_TABLE; //(String)Table

	//Fields:
	protected $FIELDS_MAP_ID = 'file_id'; //(String)Name of the field of the primary key
	protected $FIELDS_MAP_NUM = 'file_name'; //(String)Name of the field to define the number
	protected $FIELDS_MAP_NAME = 'file_name'; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC = 'file_name'; //(String)Name of the field where is store the description
	protected $FIELDS_MAP_STATE = 'file_state'; //(String)Name of the field where is store the state
	protected $FIELDS_MAP_VERSION = 'file_version'; //(String)Name of the field where is store the indices
	protected $FIELDS_MAP_ITERATION = 'file_iteration'; //(String)Name of the field where is store the indices
	protected $FIELDS_MAP_FATHER; //(String)Name of the field where is store the id of the father

	protected $_fsdata = false; //Object rb_datatype_interface, data file object of this recordfile
	protected $_reposit = false; //Object Rb_Reposit, father object of the recordfile

	protected $core_prop = array ('file_iteration' => 1, 'file_state' => 'init' ); //Object, father object

	protected $_preCreateEventName = 'recordfile_pre_create';
	protected $_postCreateEventName = 'recordfile_post_create';
	protected $_preUpdateEventName = 'recordfile_pre_update';
	protected $_postUpdateEventName = 'recordfile_post_update';

	protected $displayMd5 = false; //true if you want return the md5 property of the file


	//--------------------------------------------------------
	/** Return all single files (not linked to document) linked to a container.
	 *  return true or false.
	 *  
	 * @param array 	$params 	See parameters function of Rb_Basic::getQueryOptions().
	 * @param integer 	$father_id 	id of the docfiles father
	 * @return boolean
	 */
	public function getAll(array $params, $father_id = 0) {
		if ($father_id){
			$params ['exact_find'] [$this->FIELDS_MAP_FATHER] = $father_id;
		}
		return $this->_dao->getAllBasic ( $params );
	} //End of method


	//-------------------------------------------------------
	/** Move a file from a container to another.
	 *  return true or false.
	 *  This method update record in the table []_doc_file with value of the $data param.
	 * 
	 * @param Rb_Reposit 	$to_reposit
	 * @param boolean 		$moveIterations		if true move also iterations files of current recordfile.
	 * @return boolean
	 */
	public function moveFile(Rb_Reposit &$to_reposit, $moveIterations = true) {
		$this->_dao->getAdo ()->StartTrans (); //Start a transaction
		if (! $this->_fsdata)
		$this->getFsdata ();
		$to_dir = Rb_Reposit::getBasePath ( $to_reposit, $this->getProperty ( 'file_iteration' ) );
		$to_file = $to_dir . '/' . $this->getProperty ( 'file_name' );

		//Notiy observers on event
		$this->notify_all ( 'recordfile_pre_move', $this );

		//$this->setProperty('file_path', $to_dir);
		$this->setReposit ( $to_reposit );
		$ok = $this->_update ();
		if ($ok) $ok = $this->_fsdata->move ( $to_file, true );
		if(!$ok){
			$this->_dao->getAdo ()->FailTrans ();
			Ranchbe::getError()->push ( Rb_Error::ERROR,
			array ('file' => $this->file_name, 'dstfile' => $to_file ),
					'can\'t move %file% to %dstfile%' );
			return $this->_dao->getAdo ()->CompleteTrans ();
		}
		$this->_dao->getAdo ()->CompleteTrans ();

		Ranchbe::getError()->push ( Rb_Error::INFO,
				array ('file' => $this->file_name, 'dir'=> $to_dir),
				'data %file% is moved to %dir%' );

		if ($moveIterations) {
			$iterations = Rb_Recordfile_Iteration::get ( $this->space->getName () )->getIterations ( $this, true ); //Get iteration objects
			if (is_array ( $iterations ))
			foreach ( $iterations as $iteration ) {
				$iteration->moveFile ( $to_reposit, false );
			}
		}

		//Notiy observers on event
		$this->notify_all ( 'recordfile_post_move', $this );

		return true;
	} //End of method


	//---------------------------------------------------------------------------
	/** Lock a file.
	 *  return true or false.
	 * 	This method is call by the checkoutFile method. When a file is checkOut he is locked to prevent a second checkout.
	 * 
	 * @param integer 	$code 	code to set for access_code.
	 * @return boolean
	 */
	public function lockFile($code = 1) {
		$this->setProperty ( 'file_access_code', $code );
		if ($code == 1) {
			$this->setProperty ( 'file_checkout_by', Rb_User::getCurrentUser ()->getId () );
			$this->setProperty ( 'file_checkout_date', time () );
		}
		if ($code == 0) {
			$this->setProperty ( 'file_checkout_by', NULL );
			$this->setProperty ( 'file_checkout_date', NULL );
		}
		//update database to lock document
		$data ['file_access_code'] = $this->getProperty ( 'file_access_code' );
		$data ['file_checkout_by'] = $this->getProperty ( 'file_checkout_by' );
		$data ['file_checkout_date'] = $this->getProperty ( 'file_checkout_date' );
		return $this->_dao->update ( $data, $this->_id );
	} //End of method


	//-------------------------------------------------------
	/** Check if a file is access free.
	 *  Return the access code (0 for free without restriction , 100 if error).
	 * 
	 * @return integer
	 */
	public function checkAccess() {
		return ( int ) $this->getProperty ( 'file_access_code' );
	} //End of method


	//-------------------------------------------------------
	/** Copy current recordfile in new recordfile in path $to_file.
	 * 	Note that the recordfile is not saved at method completion.
	 *
	 * @param String $to_file Path to new file
	 * @param Int $father_id Id of the docfile father (document_id or container_id)
	 * @param Bool $replace If you want replace the file in the target directory
	 * @return Rb_Recordfile_Abstract
	 */
	public function copy( $to_file, $father_id, $replace = false) {
		Ranchbe::getError()->push ( Rb_Error::DEBUG, array (),
			'try to copy file : ' . $this->getProperty ( 'file' ) . ' to : ' . $to_file );

		//Notiy observers on event
		$this->notify_all ( 'recordfile_pre_copy', $this );

		if (! $this->getFsdata ()->copy ( $to_file, 0755, $replace )) {
			Ranchbe::getError()->push ( Rb_Error::ERROR,
			array ('from' => $this->_fsdata->getProperty ( 'file' ),
						'to' => $to_file ), 'cant copy file %from% to %to%' );
			return false;
		}
		$class = get_class ( $this );
		$to_recordfile = new $class ( $this->space, 0 ); //Create a new recordfile
		$to_recordfile->setProperty ( 'father_id', $father_id );
		$to_recordfile->setFsdata ( new Rb_Fsdata ( $to_file ) );
		//$to_recordfile->save (); //cant save because reposit_id is not set here

		return $to_recordfile;
	} //End of method
	
	//-------------------------------------------------------
	/** Copy current recordfile in new reposit. Note that the recordfile is save at method completion
	 *
	 * @param Rb_Reposit $to_reposit Reposit for copy
	 * @param String $to_file Part of path after the reposit path where copy the file
	 * @param Integer $father_id Id of the docfile father (document_id or container_id)
	 * @param String $suffix iteration number or part of path after the reposit base path
	 * @param Bool $replace If you want replace the file in the target directory
	 * @return Rb_Recordfile_Abstract
	 */
	public function copyToReposit( Rb_Reposit $to_reposit, $to_file, $father_id, $suffix, $replace = false) {
		
		$to_file = Rb_Reposit::getBasePath($to_reposit, $suffix) . '/' . basename($to_file);
		
		$to_recordfile = $this->copy( $to_file, $father_id, $replace);
		if(!$to_recordfile) return false;
		$to_recordfile->setProperty ( 'reposit_id', $to_reposit->getId() );
		$to_recordfile->save ();
		
		//Notiy observers on event
		$this->notify_all ( 'recordfile_post_copy', $this );
		
		return $to_recordfile;
		
	} //End of method
	


	//-------------------------------------------------------
	/** Copy file to directory $target_dir.
	 * 
	 * @param string $target_dir	directory where copy file.
	 * @param boolean $replace		true if you want replace the file in the target directory
	 * @param integer $mode			chmod mode for copy
	 * @return boolean
	 */
	public function copyFile($target_dir, $replace = false, $mode = 0755) {
		$this->getFsdata ();
		$dstfile = $target_dir . '/' . $this->getProperty ( 'file_name' );

		//Notiy observers on event
		$this->notify_all ( 'recordfile_pre_copyfile', $this );

		if (! $this->_fsdata->copy ( $dstfile, $mode, $replace )) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('element' => $dstfile ), 'cant copy file %element%' );
			return false;
		}

		//Notiy observers on event
		$this->notify_all ( 'recordfile_post_copyfile', $this );

		return true;
	} //End of method


	//--------------------------------------------------------
	/** Return the file id for greater iteration of the file number if exist.
	 * 
	 * @param string $file_name	number of the file
	 * @return integer
	 */
	protected function _getFileId($file_name) {
		//@todo : check if close AND file_life_stage = 1 is correct
		$query = "SELECT file_id FROM $this->OBJECT_TABLE
              WHERE file_name = '$file_name'
			  AND file_life_stage = 1
              AND file_iteration = 
              (SELECT MAX(file_iteration)
              FROM $this->OBJECT_TABLE
              WHERE file_name = '$file_name'
              GROUP  BY file_name)";
		$one = $this->_dao->getAdo ()->GetOne ( $query );
		return $one;
	} //End of method


	//--------------------------------------------------------
	/** Query database and get all properties from id
	 * 
	 * @param integer	$file_id		Id of file
	 * @param string	$space_name		name of space
	 * @param array		$selectClose	array of fields name to return
	 * @return array | false
	 */
	public static function getInfos($file_id = 0, $space_name, $selectClose = array()) {
		return self::get ( $space_name )->getDao ()->getBasicInfos ( $file_id, array ('select' => $selectClose ) );
	} //End of method


	//-------------------------------------------------------
	/** Put the file in the wildspace.
	 * The file is put with a default prefix "consult__"  to prevent lost of data.
	 * Return true or false.
	 * 
	 * @param String string to add before filename.
	 * @return boolean
	 */
	public function putFileInWildspace($addPrefix = NULL) {
		if (is_null ( $addPrefix ))
		$addPrefix = 'consult__';
		$addPrefix = $addPrefix . $this->getProperty ( 'file_iteration' ) . '__';
		if ($this->getFsdata ()) {
			return $this->_fsdata->putInWildspace ( $addPrefix, true );
		}
		return false;

		//Notiy observers on event
		$this->notify_all ( 'recordfile_post_putInWildspace', $this );

		//@todo : Add a confirmation for replace it
	} //End of method


	//---------------------------------------------------------------------------
	/**	Remove a file from a container. Suppress recorded infos in database
	 *  and suppress the file from the reposit directory of the container.
	 *  return true or false.
	 *
	 * @param Boolean if true suppress the file from the reposit directory of the container.
	 * @param Boolean if true suppress iterations of the file
	 * @return boolean
	 * 
	 */
	public function removeFile($delfile = false, $deliteration = false) {
		if ($delfile === true) { //Delete the file if $delfile = true
			if ($this->getFsdata ()) {
				if (! $this->getFsdata ()->putInTrash ()) {
					Ranchbe::getError()->push ( Rb_Error::ERROR, array ('element' => $this->getProperty ( 'file' ) ), 'can\'t suppress file %element%' );
					return false;
				}
			} else {
				Ranchbe::getError()->push ( Rb_Error::INFO, array ('file' => $this->getNumber () ), 'A suppress request occuring on a inexistant file: %file%' );
			}
		}

		$r = $this->suppress (); //delete record in database

		if ($deliteration === true && $r) { //Delete the iterations files
			Ranchbe::getError()->push ( Rb_Error::INFO, array ('element' => $this->getProperty ( 'file' ) ), 'try to suppress iterations files of %element%' );
			if (is_a ( $this, 'Rb_Docfile' )) {
				$iterations = Rb_Docfile_Iteration::getIterations ( $this, true ); //Get iteration objects
			} else if (is_a ( $this, 'Rb_Recordfile' )) {
				$iterations = Rb_Recordfile_Iteration::getIterations ( $this, true ); //Get iteration objects
			}
			if (is_array ( $iterations ))
			foreach ( $iterations as $iteration ) {
				$iteration->removeFile ( true, false );
			}
		}

		Ranchbe::getError()->push ( Rb_Error::INFO, array ('file' => $this->getProperty ( 'file_name' ) ), 'recordfile %file% has been removed' );
		return true;

	} //End of method


	//---------------------------------------------------------------------------
	/** Unlock a file.
	 *  return true or false.
	 *
	 * This method is call by the checkinFile method. When a file is checkOut is locked to prevent a second checkout.
	 * When is checkin, is unlock to permit checkout.
	 * 
	 * @return boolean
	 */
	public function unlockFile() {
		return $this->lockFile ( 0 );
	} //End of method


	//-------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_update()
	 */
	protected function _update() {
		if ($this->_id < 1) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('id' => $this->_id ), tra ( 'can not performed this action on object id %id%' ) );
			trigger_error ( 'can not performed this action on object id ' . $this->_id , E_USER_WARNING);
			return false;
		}
		if (! $this->_dao->update ( $this->getProperties (), $this->_id ))
			return false;
		$this->isSaved = true;
		return true;
	} //End of method


	//-------------------------------------------------------
	/** Check if the uers can update a document.
	 *  Return true if access is free, else return false.
	 *  
	 *  @return boolean
	 *
	 */
	protected function _checkFileUpdateRight() {
		//@todo: call current properties, is maybe better to create a new call to database to be sure of state of current file
		if ($this->getProperty ( 'file_access_code' ) == 1) {
			if ($this->getProperty ( 'file_checkout_by' ) == Rb_User::getCurrentUser ()->getId ()) {
				return true; //Update is permit for current user
			}
		}
		return false;

	} //End of method


	//--------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_create()
	 */
	protected function _create() {
		if ($this->_id !== 0) {
			Ranchbe::getError()->push ( Rb_Error::ERROR,
			array ('id' => $this->_id ),
			tra ( 'can not performed this action on object id %id%' ) );
			trigger_error ( 'can not performed this action on object id ' . $this->_id , E_USER_WARNING );
			return false;
		}
		//Create a basic object
		$this->_id = $this->_dao->create ( $this->getProperties () );
		if ($this->_id) {
			$this->isSaved = true;
			$this->_saveRegistry ();
			return $this->_id;
		} else {
			Ranchbe::getError()->push ( Rb_Error::ERROR,
			array ('element' => $this->getNumber () ),
			tra ( 'cant create the recordfile %element%' ) );
			return $this->_id = 0;
		}
	} //End of method


	//-------------------------------------------------------------------------
	/** Save current instance in registry
	 * 
	 * @return boolean
	 */
	abstract protected function _saveRegistry();

	//--------------------------------------------------------
	/** Get the current fsdata object linked to the current object 
	 * 	and check if the data path equal the parameter
	 *  if not, init the fsdata
	 *  
	 *  @return Rb_Fsdata
	 */
	public function getFsdata() {
		if (! $this->_fsdata && $this->_id > - 1) {
			$this->_fsdata = new Rb_Fsdata ( $this->getProperty ( 'file' ) );
			if (! $this->_fsdata->isExisting ()) {
				return $this->_fsdata = false;
			}
		}
		return $this->_fsdata;
	} //End of method


	//----------------------------------------------------------
	/** Set the reposit
	 * 
	 * @param Rb_Reposit $reposit
	 * @return @this
	 */
	public function setReposit(Rb_Reposit &$reposit) {
		if ($this->_id < 0) {
			trigger_error (tra ( 'can not performed this action on object id %id%' ), E_USER_WARNING );
			return false;
		}
		$this->setProperty ( 'reposit_id', $reposit->getId () );
		$this->_reposit = & $reposit;
		return $this;
	} //End of method


	//----------------------------------------------------------
	/** Get reposit
	 * 
	 * @return Rb_Reposit
	 *
	 */
	public function getReposit() {
		if (! $this->_reposit && $this->_id > - 1) {
			$this->_reposit = new Rb_Reposit ( $this->getProperty ( 'reposit_id' ) );
		}
		return $this->_reposit;
	} //End of method


	//----------------------------------------------------------
	/** Set the fsdata linked to the current recordfile
	 * 
	 * @param Rb_Fsdata
	 * @return this
	 */
	public function setFsdata(Rb_Fsdata &$fsdata) {
		if ($this->_id < 0) {
			trigger_error ( '$this->_id is not set' , E_USER_WARNING);
			return false;
		}
		if ($fsdata->getProperty ( 'dataType' ) === false) {
			trigger_error ( 'fsdata of ' . $fsdata->getProperty ( 'file' ) . ' is unreachable' , E_USER_WARNING);
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('file' => $fsdata->getProperty ( 'file' ) ), tra ( 'fsdata of %file% is unreachable' ) );
			return false;
		}
		$this->_fsdata = & $fsdata;
		$this->setProperty ( 'file_name', $fsdata->getProperty ( 'file_name' ) );
		$this->setProperty ( 'file_size', $fsdata->getProperty ( 'file_size' ) );
		$this->setProperty ( 'file_mtime', $fsdata->getProperty ( 'file_mtime' ) );
		$this->setProperty ( 'file_md5', $fsdata->getProperty ( 'file_md5' ) );
		$this->setProperty ( 'file_extension', $fsdata->getProperty ( 'file_extension' ) );
		$this->setProperty ( 'file_root_name', $fsdata->getProperty ( 'file_root_name' ) );
		$this->setProperty ( 'file_type', $fsdata->getProperty ( 'file_type' ) );
		$this->setProperty ( 'file_path', $fsdata->getProperty ( 'file_path' ) );
		return $this;
	} //End of method


	//----------------------------------------------------------
	/**	Unset the fsdata linked to the current recordfile
	 * 
	 * @return this
	 */
	public function unsetFsdata() {
		$this->_fsdata = false;
		$this->unsetProperty ( 'file_name' )
			 ->unsetProperty ( 'file_size' )
			 ->unsetProperty ( 'file_path' )
			 ->unsetProperty ( 'file_mtime' )
			 ->unsetProperty ( 'file_md5' )
			 ->unsetProperty ( 'file_extension' )
			 ->unsetProperty ( 'file_root_name' )
			 ->unsetProperty ( 'file_type' )
			 ->unsetProperty ( 'file_path' );
		return $this;
	} //End of method


	//----------------------------------------------------------
	/** Get the property of the recordfile by the property name.
	 * 	init() must be call before.
	 *
	 * @param String
	 */
	function getProperty($name) {
		switch ($name) {
			case 'id' :
			case 'file_id' :
				return $this->_id;
				break;
					
				//basic id
			case 'base_id' :
			case 'bid' :
				$name = 'file_bid';
				break;
					
			case 'file' :
				return $this->getProperty ( 'file_path' ) . '/' . $this->getProperty ( 'file_name' );
				break;
					
			case 'file_path' :
				if( $this->getId() > 0 )
				return Rb_Reposit::getBasePath ( $this->getReposit (), $this->getProperty ( 'file_iteration' ) );
				break;
					
			case 'number' :
				$name = $this->FIELDS_MAP_NUM;
				break;
					
			case 'name' :
				$name = $this->FIELDS_MAP_NAME;
				break;
					
			case 'doc_name' :
				$name = 'file_root_name';
				break;
					
			case 'description' :
				$name = $this->FIELDS_MAP_DESC;
				break;
					
			case 'file_state' :
			case 'state' :
				$name = $this->FIELDS_MAP_STATE;
				break;
					
			case 'version' :
			case 'file_version' :
			case 'version_id' :
			case 'version' :
			case 'vid' :
				$name = $this->FIELDS_MAP_VERSION;
				break;
					
			case 'iteration' :
			case 'file_iteration' :
			case 'iteration_id' :
				$name = $this->FIELDS_MAP_ITERATION;
				break;
					
			case 'father_id' :
			case $this->FIELDS_MAP_FATHER:
				$name = $this->FIELDS_MAP_FATHER;
				break;

			case 'md5' :
				$name = 'file_md5';
				break;

		} //End of switch

		if (array_key_exists ( $name, $this->core_props )) {
			return $this->core_props [$name];
		} else if ($this->_fsdata) { // try to get Property from fsdata
			return $this->_fsdata->getProperty ( $name );
		} else {
			trigger_error ( '$this->_fsdata is not set or property is not set : '.$name , E_USER_NOTICE );
			return false;
		}
	} //End of method


	//----------------------------------------------------------
	/** Set property of the recordfile
	 * 	Return true or false
	 *
	 * @param String 	property name
	 * @param Variant 	property value
	 * @return this
	 */
	public function setProperty($name, $value) {
		switch ($name) {
			case 'file_bid' :
			case 'base_id' :
			case 'bid' :
				$this->core_props ['file_bid'] = $value;
				break;
					
			case 'number' :
			case 'file_number' :
			case $this->FIELDS_MAP_NUM :
				$this->core_props [$this->FIELDS_MAP_NUM] = $value;
				break;
					
			case 'name' :
			case 'file_name' :
			case $this->FIELDS_MAP_NAME :
				$this->core_props [$this->FIELDS_MAP_NAME] = $value;
				break;
					
			case 'doc_name' :
			case 'file_root_name' :
				$this->core_props ['file_root_name'] = $value;
				break;
					
			case 'description' :
			case $this->FIELDS_MAP_DESC :
				$this->core_props [$this->FIELDS_MAP_DESC] = $value;
				break;
					
			case 'file_state' :
			case 'state' :
			case $this->FIELDS_MAP_STATE :
				$this->core_props [$this->FIELDS_MAP_STATE] = $value;
				break;
					
			case 'version' :
			case 'file_version' :
			case 'version_id' :
			case 'version' :
			case 'vid' :
			case $this->FIELDS_MAP_VERSION :
				$this->core_props [$this->FIELDS_MAP_VERSION] = ( int ) $value;
				break;
					
			case 'iteration' :
			case 'file_iteration' :
			case 'iteration_id' :
			case 'itid' :
			case $this->FIELDS_MAP_ITERATION :
				$this->core_props [$this->FIELDS_MAP_ITERATION] = ( int ) $value;
				break;
					
			case 'father_id' :
			case $this->FIELDS_MAP_FATHER :
				$this->core_props [$this->FIELDS_MAP_FATHER] = ( int ) $value;
				break;
					
			case 'file_checkout_date' :
			case 'file_checkout_by' :
			case 'from_file' :
				if(!$value) $value=null; else $value=(int) $value;
				$this->core_props[$name] = $value;
				break;

			case 'file_open_date' :
			case 'file_open_by' :
			case 'file_update_date' :
			case 'file_update_by' :
			case 'file_mtime' :
			case 'file_access_code' :
			case 'file_size' :
			case 'document_id' :
			case 'file_mode' :
			case 'file_life_stage' :
			case 'owned_by' :
			case 'reposit_id' :
				$this->core_props [$name] = ( int ) $value;
				break;
					
			case 'file_type' :
			case 'file_md5' :
			case 'file_extension' :
				$this->core_props [$name] = $value;
				break;
					
			default :
				return false;
				break;
		} //End of switch
		return $this;
	} //End of method


	//----------------------------------------------------------
	/** Set iteration id
	 *
	 * @param Integer
	 * @return void
	 */
	public function setIterationId($id) {
		$this->core_props ['file_iteration'] = $id;
	} //End of method


	//----------------------------------------------------------
	/** Set version id
	 *
	 * @param Integer
	 * @return void
	 */
	public function setVersionId($id) {
		$this->core_props ['file_version'] = $id;
	} //End of method


} //End of class

