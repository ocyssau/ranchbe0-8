<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*! \brief Iteration is created on each reposit update
*
*/
class Rb_Recordfile_Iteration{
  
  //-------------------------------------------------------
  /*!\brief
  * Get iterations of recordfile
  * return array of objects or false
  * \param $recordfile (rb_recordfile_abstract)
  * \param $object (bool) if true return an array of Rb_Recordfile objects, else return array
  */
  public static function getIterations(Rb_Recordfile &$recordfile, $object = false){
    $query = 'SELECT * FROM '.$recordfile->getDao()->getTableName('object')
             .' WHERE file_bid = \''.$recordfile->getProperty('file_bid').'\''
             .' AND file_version = '.$recordfile->getProperty('file_version')
             .' AND file_life_stage = 3';

    $res = Ranchbe::getDb()->execute($query);
    if($res === false ){
      Ranchbe::getError()->push(Rb_Error::ERROR, array(), Ranchbe::getDb()->ErrorMsg());
      return false;
    }
    if($object){
      $i=0;
      while($row = $res->FetchRow() ){
        $result[$i] = Rb_Recordfile::get($recordfile->getSpaceName(), $row['file_id']);
        $i++;
      }
      return $result;
    }else{
      return $res->GetArray();
    }
  }//End of method

  //-------------------------------------------------------
  /*!\brief
  * Get one Iteration of recordfile
  * return array of objects or false
  * $object (bool) if true return an array of Rb_Recordfile objects, else return array
  */
  public static function getIteration(Rb_Recordfile &$recordfile, $iteration_id, $object = false){
    $query = 'SELECT file_id FROM '.$recordfile->getDao()->getTableName('object')
             .' WHERE file_bid = \''.$recordfile->getProperty('file_bid').'\''
             .' AND file_version = '.$recordfile->getProperty('file_version')
             .' AND file_iteration = '.$iteration_id
             .' AND file_life_stage = 3';

    $res = Ranchbe::getDb()->GetOne($query);
    if($res === false ){
      Ranchbe::getError()->push(Rb_Error::ERROR, array(), Ranchbe::getDb()->ErrorMsg());
      return false;
    }
    if($object){
      return Rb_Recordfile::get($recordfile->getSpaceName(), $res);
    }else{
      return $res;
    }
  }//End of method
  
  //-------------------------------------------------------
  /*!\brief create a Iteration of the $recordfile
  *  return Rb_Recordfile
  *  \param Rb_Recordfile original recordfile from create Iteration
  *  \param Rb_Fsdata data to use to create new recordfile
  */
  public static function create(Rb_Recordfile &$recordfile,
                                Rb_Fsdata $fromFsdata){
	$back_iteration_number = Ranchbe::getConfig()->iteration->max_back_number;
    if($back_iteration_number === 0) //Dont keep Iteration if 0
      return $recordfile;

    $iterationId = $recordfile->getProperty('file_iteration')+1;

    //$fsdata =& $recordfile->getFsdata();
    if(!$fromFsdata->isExisting()){
      Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$recordfile->getNumber()),
                                             'none valid fsdata for %element%');
      return false;
    }
    
    $dstfile = Rb_Reposit::getBasePath(
                        Rb_Reposit::getActiveReposit($recordfile->getSpace()),
                        $iterationId)
                        .'/'.$recordfile->getProperty('file_name');

    $srcfile = $fromFsdata->getProperty('file');
    
    if( $dstfile == $srcfile ){
      Ranchbe::getError()->push(Rb_Error::ERROR, array('name'=>$dstfile ), 
                   'you try to create a copy of recordfile %name% on himself' );
      return false;
    }

    $backupfsdata = $recordfile->getFsdata();
    $recordfile->setFsdata($fromFsdata); //to create data from specifed data and not from data of recordfile
    $new_recordfile = $recordfile->copy($dstfile, $recordfile->getFather()->getId(), false);
    if(!$new_recordfile) return false;
    $new_recordfile->setIterationId( $iterationId );
    $new_recordfile->setVersionId( $recordfile->getProperty('file_version') );
    $new_recordfile->setProperty( 'file_bid', $recordfile->getProperty('file_bid'));
    $new_recordfile->setProperty( 'file_checkout_by', $recordfile->getProperty('file_checkout_by'));
    $new_recordfile->setProperty( 'file_checkout_date', $recordfile->getProperty('file_checkout_date'));
    //$recordfile->unsetFsdata(); //cancel specified fsdata
    $recordfile->setFsdata($backupfsdata);

    //Remove obsolete Iteration
    $obsIteId  = ($iterationId - $back_iteration_number);
    if($obsIteId > 0){
      $obsIte = self::getIteration($recordfile, $obsIteId, true); //Get Iteration
      if($obsIte)
        $obsIte->removeFile(true, false);
    }

    return $new_recordfile;
    
  }//End of method

  //-------------------------------------------------------
  /*!\brief lock the recordfile and mark it as Iteration state
  *  return Rb_Recordfile
  */
  public static function setAsIteration(Rb_Recordfile &$recordfile){
    $recordfile->setProperty( 'file_access_code', 170 );
    $recordfile->setProperty( 'file_life_stage', 3 );
    $recordfile->setProperty( 'file_checkout_by', NULL );
    $recordfile->setProperty( 'file_checkout_date', NULL );
    //$recordfile->save();
    return $recordfile;
  }//End of method

} //End of class
