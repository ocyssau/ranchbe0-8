<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/filesystem.php');

/** Manipulation of the directories on the server
 * The Directory class existing yet in PHP, so is why this class is named rdirectory(ranchbe direcory)
 *
 */
class Rb_Directory extends Rb_Filesystem{
	//-----------------------------------------------------------------------------
	/** A function to copy files from one Directory to another one, 
	 * including subdirectories and nonexisting or newer files. 
	 * 
	 * @param String $srcdir
	 * @param String $dstdir
	 * @param Bool $verbose
	 * @param Bool $initial
	 * @param Bool $recursive
	 * @param Integer $mode
	 * @return Bool
	 */
	static function dircopy($srcdir, $dstdir, $verbose = false, $initial=true, $recursive=true, $mode=0755){
		if (!Rb_Filesystem::LimitDir($srcdir)){
			print 'dircopy error : you have no right access on file '. $srcdir . ' <br />';
			return false;
		}

		if (!Rb_Filesystem::LimitDir($dstdir)){
			print 'dircopy error : you have no right access on file '. $dstdir . ' <br />';
			return false;
		}

		if(!is_dir($dstdir)) mkdir($dstdir, $mode, true);
		if($curdir = opendir($srcdir)) {
			while($file = readdir($curdir)) {
				if($file != '.' && $file != '..') {
					$srcfile = "$srcdir" . '/' . "$file";
					$dstfile = "$dstdir" . '/' . "$file";
					if(is_file("$srcfile")) {
						if($verbose) echo "Copying '$srcfile' to '$dstfile'...";
						if(copy("$srcfile", "$dstfile")) {
							touch("$dstfile", filemtime("$srcfile"));
							if($verbose) echo "OK\n";
						}
						else {print "Error: File '$srcfile' could not be copied!\n"; return false;}
					}
					else{
						if(is_dir("$srcfile") && $recursive){
							self::dircopy("$srcfile", "$dstfile", $verbose, false);
						}
					}
				}
			}
			closedir($curdir);
		}
		return true;
	}//End of function

	//-------------------------------------------------------------------------------
	/** create a new Rb_Directory recursivly
	 * 
	 * @param String $path
	 * @param Integer $mode
	 * @param Bool $secure, if true, check that Directory to create is in a authorized Directory
	 * @param Bool $test_mode
	 * @return Bool
	 */
	static function createdir($path, $mode=0755, $secure=true, $test_mode=false){

		if($secure){
			if (!Rb_Filesystem::LimitDir($path)){
				Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$path), 'can create Directory : %element%. This path is not authorized by the ranchbe configuration' );
				return false;
			}
		}

		if( file_exists($dir) ){
			$msg = 'a file with same name that Directory to create is yet existing. Can not create Directory %element%';
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$path), $msg );
			return false;
		}

		if( $test_mode) return true;

		//Le "mode" ne fonctionne pas sous windows, ce qui oblige a faire un
		//chmod par la suite.
		//bool mkdir ( string pathname [, int mode [, bool recursive [, resource context]]] )
		if(!is_dir($path)){
			if(mkdir($path, $mode, true)){
				if (! chmod ($path, $mode) ) { //!!chmod ne fonctionne pas avec les fichiers distants!!
					Ranchbe::getError()->warning('Chmod impossible on %file%', array('file'=>$path));
					return true;
				}
				Ranchbe::getError()->notice('Create directory %file%', array('file'=>$path));
				return true;
			}else{
				Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$path), 'An unknow error is occuring. Directory %element% is not created');
				return false;
			}
		}else{
			Ranchbe::getError()->warning('Directory %file% is yet existing', array('file'=>$path));
			return true;
		}

	}//End of function

	//-------------------------------------------------------------------------------
	/** Remove files from one Directory to another one, including subdirectories and nonexisting or newer files.
	 *   This function is a adaptation from the script of "....." visible in PHP help site.
	 * 
	 * @param String $path
	 * @param Bool $recursive
	 * @param Bool $verbose
	 * @param Bool $test_mode
	 * @return Bool
	 * 
	 */
	static function removeDir($path, $recursive=false, $verbose = false , $test_mode=false ){
		if (!Rb_Filesystem::LimitDir($path)) {
			print 'removeDir error : you have no right access on file '. $path;
			return false;
		}

		if( $test_mode) return true;

		// Add trailing slash to $path if one is not there
		if (substr($path, -1, 1) != "/"){
			$path .= "/";
		}
		foreach (glob($path . "*") as $file){
			if (is_file($file) === TRUE){
				// Remove each file in this Directory
				if (!unlink($file)){echo "failed to removed File: " . $file . "<br>"; return false;
				}else {if($verbose) echo "Removed File: " . $file . "<br>";}
			}
			else if (is_dir($file) === true && $recursive === true){
				// If this Directory contains a Subdirectory and if recursivity is requiered, run this Function on it
				if (!self::removeDir($file, $recursive, $verbose)){
					if($verbose) echo "failed to removed File: " . $file . "<br>";
					return false;
				}
			}
		}

		// Remove Directory once Files have been removed (If Exists)
		if(is_dir($path)){
			if(@rmdir($path)){
				if($verbose) echo "<br>Removed Directory: " . $path . "<br><br>";
				return true;
			}
		}
		return false;
	}//End of function

	//-------------------------------------------------------------------------------
	/** remove files from one Directory to another one, including subdirectories and put in a trash.
	 * 
	 * @param String $dir
	 * @param Bool $verbose
	 * @param Bool $recursive
	 * @param String $trashDir
	 * @param Bool $test_mode
	 * @return Bool
	 */
	static function putDirInTrash($dir, $verbose = false, $recursive=false, 
									$trashDir=DEFAULT_TRASH_DIR, $test_mode=false){

		//Check if directories exists and are writable
		if (!is_dir($dir) || !is_dir($trashDir) || !is_writable($trashDir)) {
			print ' error : '. "$dir" . ' don\'t exist or is not writable <br />';
			print ' or    : '. "$trashDir" . ' don\'t exist or is not writable <br />';
			return false;
		}

		if( $test_mode) return true;

		//Add original path to name of dstfile
		$oriPath = self::encodePath(dirname($dir));
		/*
		 $oriPath = str_replace( '/' , '%2F', "$oriPath" );
		 $oriPath = str_replace( '.' , '%2E', "$oriPath" );
		 $oriPath = str_replace( ':' , '%3A', "$oriPath" );
		 $oriPath = str_replace( '\\' , '%5C', "$oriPath" );
		 */
		//Check if there is not conflict name in trash dir else rename it
		//$dstdir = $trashDir .'/'. "$camuDir"  .'_'. basename($dir);
		$dstdir = $trashDir .'/'. "$oriPath" .'%&_'. basename($dir);
		$i = 0;
		if(is_dir($dstdir)){
			//Rename destination dir if exist
			$Renamedstdir = $dstdir;
			while(is_dir($Renamedstdir)){
				$Renamedstdir = $dstdir . '(' . "$i" . ')';
				$i ++;
			}
			$dstdir = $Renamedstdir;
		}

		//Copy dir to trash
		if (!self::dircopy($dir , $dstdir, false, false, true, 0755 )){ //copy dir to dirtrash
			print 'error copying dir '. $dir . ' to Trash: '. $dstdir . ' <br />';
			return false;
		}else{ //Suppress original dir
			if (!self::removeDir($dir, $recursive, $verbose)){
				//print 'error suppressing dir :'. $dir . ' <br />';
				return false;
			}
		}
		return true;

	}//End of function

	//-------------------------------------------------------
	/** Get files in a Directory and return infos about
	 *   Return a array of file properties if no errors, else return FALSE
	 * 
	 * @param string $path
	 * @param bool $getMd5
	 * @param string $regex regular expression to verified by file name
	 * @param bool $return_collection if true, return array of Rb_Fsdata object, else return array of properties
	 * @return unknown_type
	 */
	static function listDir($path , $getMd5=true, $regex=false, $return_collection = false){
		if (!$handle  = opendir($path)){
			print 'failed to open ' . $path . '<br>';
			return false ;
		}
		$return=array();
		while ($file = @readdir($handle)){
			if($file == '.' || $file == '..' ) continue;
			if($regex){
				if( !preg_match('/'.$regex.'/i', $file) ) continue;
			}
			if($fsdata = new Rb_Fsdata($path.'/'.$file))
			if($return_collection) $return[] = $fsdata;
			else $return[] = $fsdata->getProperties($getMd5);
		}
		@closedir($handle);
		return $return;
	}//End of method

	//----------------------------------------------------------
	/** This method can be used to get files, cadds parts, camu, adraw in a Directory.
	 *  Return a array if no errors, else return false.
	 *  
	 *  \param $params[maxRecords](integer) max number of records to display
	 *	\param $params[sort_field](string)
	 *	\param $params[sort_order](string = ASC or DESC)
	 *	\param $params[find](string)
	 *	\param $params[displayMd5] (bool)
	 *	\param $regex (string) regular expression to verified by file name
	 * 
	 * @param String $path
	 * @param Array $params
	 * @param String $regex
	 * @return Array|Bool
	 */
	public static function getDatas($path = '', Array $params, $regex=false){
		if(!empty($params['find'])){
			foreach($params['find'] as $key=>$val){
				$find_field=$key;
				$search_word = $val;
			}
		}

		if(empty($params['sort_field']))
		$params['sort_field'] = 'file_name';

		if($params['sort_order'] == 'DESC'){
			$params['sort_order'] = SORT_DESC;
		}else $params['sort_order'] = SORT_ASC;

		if(empty($path)){
			$this->error_stack->push(Rb_Error::ERROR, array(), 'None path is specified');
			return false;
		}

		if (!$handle = opendir($path)){
			$this->error_stack->push(Rb_Error::ERROR, array('element'=>$path),
                                                    'failed to open %element%');
			return false;
		}

		$count = 0;
		while($file = @readdir($handle)){ //list all files of the Directory
			if($file == '.' || $file == '..' ) continue;
			if($regex) if( !preg_match('/'.$regex.'/i', $file) ) continue;
			$file = $path .'/'. $file;
			if($odata =& Rb_Fsdata::_dataFactory($file)){
				//If file is a camu, get adraws
				if($odata->getProperty('file_type') == 'camu'){
					if($adraws = $odata->getAdraws()){
						foreach($adraws as $oadraw){
							$return[] = $oadraw->getInfos($params['displayMd5']);
						}
					}
				}
				$return[] = $odata->getInfos($params['displayMd5']);
				$count ++;
			}
		} //End of while
		@closedir($handle);

		//Sort files by field
		if($return){
			foreach ($return as $key => $row) { //product a array from field to sort (for use array_multisort function)
				$field[$key]  = $row[$params['sort_field']];
			}
			array_multisort( $field, $params['sort_order'], $return); //multidimensionnal sort
			//var_dump($field);
			//var_dump($return);
			//@todo : revoir ce mecanisme afin de filtrer les donnees plus en amont pour gagner en performances
			//Filter data from $term
			if(!empty($search_word) && !empty($find_field)){
				foreach($return as $file){
					if(preg_match ( '/'.$search_word.'/i' , $file[$find_field] ))
					$finfos[] = $file;
				}
				return $finfos;
			}
			return $return;
		}
		return false;
	}//End of method

	//----------------------------------------------------------
	/** This method can be used to get files, cadds parts, camu, adraw in a Directory.
	 *  Return a array if no errors, else return false.
	 * 
	 * @param String $path
	 * @param Array $params
	 * @param String $regex
	 * @return Array|Bool
	 */
	function getCollectionDatas($path = '', array $params, $regex=false){
		if(!empty($params['find'])){
			foreach($params['find'] as $key=>$val){
				$find_field=$key;
				$search_word = $val;
			}
		}

		if(empty($params['sort_field']))
		$params['sort_field'] = 'file_name';

		if(empty($path)){
			$this->error_stack->push(Rb_Error::ERROR, array(), 'None path is specified');
			return false;
		}

		if (!$handle = opendir($path)){
			$this->error_stack->push(Rb_Error::ERROR, array('element'=>$path),
                                                    'failed to open %element%');
			return false;
		}

		$count = 0;
		while($file = @readdir($handle)){ //list all files of the Directory
			if($file == '.' || $file == '..' ) continue;
			if($regex) if( !preg_match('/'.$regex.'/i', $file) ) continue;
			$file = $path .'/'. $file;
			if($odata = new Rb_Fsdata($file)){
				if(!$odata->isExisting()) continue;
				//If file is a camu, get adraws
				if($odata->getProperty('file_type') == 'camu'){
					if($adraws = $odata->getAdraws()){
						foreach($adraws as $oadraw){
							$return[] = $oadraw;
						}
					}
				}
				$return[] = $odata;
				$count ++;
			}
		} //End of while
		@closedir($handle);

		//Sort files by field
		if(!$return) return $return;
		$returnb = array();
		if($params['sort_field']){
			foreach ($return as $fsdata) { //product a array from field to sort (for use array_multisort function)
				$key = $fsdata->getProperty($params['sort_field']);
				$returnb[$key] = $fsdata;
			}
			if($params['sort_order'] == 'DESC') krsort($returnb);
			else ksort($returnb);
			$return = array_values($returnb);
		}
		//@todo : revoir ce mecanisme afin de filtrer les donnees plus en amont pour gagner en performances
		//Filter data from $term
		$returnb = array();
		if(!empty($search_word) && !empty($find_field)){
			foreach($return as $fsdata){
				if(preg_match ( '/'.$search_word.'/i' , $fsdata->getProperty($find_field)))
				$returnb[] = $fsdata;
			}
			return $returnb;
		}
		return $return;
	}//End of method

	//-------------------------------------------------------
	/** Alternative (peu performante) a disk_total_space qui ne comprend pas les chemins
	 * relatifs (sur windows)
	 * source du scripts : http://fr3.php.net/manual/fr/function.disk-total-space.php
	 * 
	 * @param String $path
	 * @return Integer
	 */
	static function dskspace($path){
		$s = filesize($path);
		$size = $s;
		if (is_dir($path)){
			$dh = opendir($path);
			while (($file = readdir($dh)) !== false)
			if ($file != "." and $file != "..")
			$size += self::dskspace($path."/".$file);
			closedir($dh);
		}
		return $size;
	}//End of method

} //End of class
