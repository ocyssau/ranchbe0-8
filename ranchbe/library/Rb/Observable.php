<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/** An abstract class implementing Observable objects
 * 
 * @author Olivier CYSSAU
 * 
 * Methods to override: NONE
 * This class implements the Observer design pattern defining Observable
 * objects, when a class extends Observable Observers can be attached to
 * the class listening for some event. When an event is detected in any
 * method of the derived class the method can call notifyAll($event,$msg)
 * to notify all the observers listening for event $event.
 * The Observer objects must extend the Observer class and define the
 * notify($event,$msg) method.
 *
 */
abstract class Rb_Observable extends Rb_Object_Root implements Rb_Observable_Interface{
	private $_observers = Array();
	protected $_observerIsInit = false; //(Bool) true if observers are inits

	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Observable/Rb_Observable_Interface#attach($event, $obj)
	 * 
	 * This method can be used to attach an object to the class listening for
	 * some specific event. The object will be notified when the specified
	 * event is triggered by the derived class.
	 * 
	 */
	function attach($event, Rb_Observer_Interface &$obj){
		if (!is_object($obj)){
			return false;
		}
		$obj->_observerId = uniqid(rand());
		$this->_observers[$event][$obj->_observerId] = &$obj;
	} //End of method

	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Observable/Rb_Observable_Interface#attach_all($obj)
	 * 
	 * Attaches an object to the class listening for any event.
	 * The object will be notified when any event occurs in the derived class.
	 * 
	 */
	function attach_all(Rb_Observer_Interface &$obj){
		if (!is_object($obj)) {
			return false;
		}
		$obj->_observerId = uniqid(rand());
		$this->_observers['all'][$obj->_observerId] = &$obj;
	} //End of method

	/** Detaches an observer from the class.
	 * 
	 * (non-PHPdoc)
	 * @see library/Rb/Observable/Rb_Observable_Interface#dettach($obj)
	 */
	function dettach(Rb_Observer_Interface &$obj){
		if(isset($this->_observers[$obj->_observerId])){
			unset($this->_observers[$obj->_observerId]);
		}
	} //End of method

	/**
	 * Method used to notify objects of an event. This is called in the
	 * methods of the derived class that want to notify some event.
	 * 
	 * @param string $event
	 * @param variant $msg
	 * @return void
	 */
	protected function notify_all($event, $msg){
		if(isset($this->_observers[$event])){
			foreach ($this->_observers[$event] as $observer){
				$observer->notify($event,$msg);
			}
		}
		if(isset($this->_observers['all'])){
			foreach ($this->_observers['all'] as $observer){
				$observer->notify($event,$msg);
			}
		}
	} //End of method

	/** Method to override to set the observers
	 * 
	 * @return void
	 */
	protected function _initObservers(){
		$this->_observerIsInit = true;
	}//End of method

} //End of class

