<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
 CREATE TABLE `categories` (
 `category_id` int(11) NOT NULL,
 `category_number` varchar(32) NOT NULL,
 `category_description` varchar(512) default NULL,
 `category_icon` varchar(32) default NULL,
 PRIMARY KEY  (`category_id`),
 UNIQUE KEY `UC_category_number` (`category_number`)
 ) ENGINE=InnoDB;
*/

//require_once('core/object/permanent.php');

/** A Category define sub unit of organisation in the container.
 *
 */
class Rb_Category extends Rb_Object_Permanent{

	protected $OBJECT_TABLE = 'categories'; //(String)Table where are stored the containers definitions
	protected $FIELDS_MAP_ID = 'category_id'; //(String)Name of the field of the primary key
	protected $FIELDS_MAP_NUM = 'category_number'; //(String)Name of the field to define the number
	protected $FIELDS_MAP_NAME = 'category_number'; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC = 'category_description'; //(String)Name of the field where is store the description
	protected $FIELDS_MAP_FATHER = 'father_id'; //(String)Name of the field where is store the id of the father

	protected static $_registry = array(); //(array) registry of instances

	/**
	 * 
	 * @param Integer $category_id
	 * @return void
	 */
	public function __construct($category_id=0){
		$this->_init($category_id);
	}//End of method

	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_init($id)
	 */
	protected function _init($id){
		$this->_dao = new Rb_Dao( Ranchbe::getDb() );
		$this->_dao->setTable($this->OBJECT_TABLE, 'object');
		$this->_dao->setKey($this->FIELDS_MAP_ID, 'primary_key');
		$this->_dao->setKey($this->FIELDS_MAP_NUM, 'number');
		$this->_dao->setKey($this->FIELDS_MAP_NAME, 'name');
		$this->_dao->setKey($this->FIELDS_MAP_DESC, 'description');
		$this->_dao->setKey($this->FIELDS_MAP_FATHER, 'father');
		return parent::_init($id);
	}//End of method

	//-------------------------------------------------------------------------
	/** Return a instance of object.
	 *   if none parameter return a special instance with no possibilies to change here properties
	 *   and wich can not be saved. This instance is just for use in static method where access to database is require,
	 *   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
	 *   If object "$id" dont exist return false.
	 *   If object "$id" has already been instantiated, then return this instance (singleton pattern).
	 * 
	 * @param Integer $id
	 * @return Rb_Category
	 */
	static function get($id = -1){
		if($id < 0) $id = -1; //forced to value -1

		if($id == 0)
		return new Rb_Category(0);

		if( !self::$_registry[$id] ){
			self::$_registry[$id] = new Rb_Category($id);
		}
		return self::$_registry[$id];
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#getProperty($property_name)
	 */
	function getProperty($property_name){
		switch($property_name){
			case 'number':
				$property_name = 'category_number';
				break;
			case 'name':
			case 'doctype_name':
				$property_name = 'category_number';
				break;
			case 'description':
				$property_name = 'category_description';
				break;
			case 'icon':
				$property_name = 'category_icon';
				break;
		}
		if( array_key_exists($property_name, $this->core_props) )
		return $this->core_props[$property_name];
	}//End of method

	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#setProperty($property_name, $property_value)
	 */
	public function setProperty($property_name, $property_value){
		if($this->getId() < 0) return false; //Can not change property on object -1
		$this->isSaved = false;
		switch($property_name){
			case 'number':
				$property_name = 'category_number';
				break;
			case 'name':
			case 'category_name':
				$property_name = 'category_number';
				break;
			case 'description':
				$property_name = 'category_description';
				break;
			case 'icon':
				$property_name = 'category_icon';
				break;

			case 'category_number':
			case 'category_description':
			case 'category_icon':
				break;

			default:
				return false;
				break;
		}
		$this->core_props[$property_name] = $property_value;
		return $this;
	}//End of method

	//----------------------------------------------------------
	/** Get list of categories
	 * 
	 * @param Array $params See parameters function of getQueryOptions()
	 * @return Array|RecordSet
	 */
	public function getAll( array $params=array() ){
		return $this->_dao->getAllBasic( $params );
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_create()
	 */
	protected function _create(){
		if( $this->_id !== 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}

		//Create a basic object
		if( $this->_id = $this->_dao->create( $this->getProperties() ) ){
			self::$_registry[$this->_id] =& $this;
			$this->isSaved = true;
		}else{
			Ranchbe::getError()->push(Rb_Error::ERROR,
			array( 'element'=>$this->getNumber() ),
			tra('cant create %element%') );
			return $this->_id = 0;
		}
		return $this->_id;
	}//End of method

	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_update()
	 */
	protected function _update(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}
		return $this->_dao->update( $this->getProperties() , $this->_id);
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#suppress()
	 */
	function suppress(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}

		if($ret = $this->_dao->suppress($this->_id)){
			$this->_suppressPermanentObject();
			//clean registry
			unset(self::$_registry[$this->_id]);
		}
		return $ret;
	}//End of method

	//----------------------------------------------------------
	/** Return the id of the categorie $category_number if exist. Else return false.
	 * 
	 * @param String $category_number
	 * @return Integer
	 */
	function getCategoryId($category_number){
		return $this->_dao->getBasicId($category_number);
	}//End of method

} //End of class

