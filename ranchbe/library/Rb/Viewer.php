<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//===========================================================
/*! \brief Class to view file or attachment file
*/
class Rb_Viewer{
  protected $_fsdata; //fsdata Object
  protected $_file_path; //Path to file
  protected $_file_extension;
  protected $_mimetype;
  protected $_driverName; //The name of the driver to use
  protected $_driver; //The object driver to use
  protected $_noread; //The object driver to use

//----------------------------------------------------------
  function __construct(){
  $this->error_stack =& Ranchbe::getError();
  }//End of method

//----------------------------------------------------------
  function init($object){
    if(is_a($object,Rb_Docfile)){
      return $this->_initDocfile($object);
    }else if(is_a($object,Rb_Fsdata)){
      return $this->_initFsdata($object);
    }else if(is_string($object)){
      return $this->_initFile($object);
    }else{
      trigger_error('this method accept only Rb_Docfile, Rb_Fsdata or string', E_USER_WARNING);
      return false;
    }
  }//End of method

//----------------------------------------------------------
  public function __get($property_name){
    switch($property_name){
      case 'mimetype':
        return $this->_mimetype;
      case 'fsdata':
        return $this->_fsdata;
      case 'file':
      case 'file_path':
        return $this->_file_path;
      case 'file_extension':
        return $this->_file_extension;
      case 'driverName':
        return $this->_driverName;
      case 'driver':
        return $this->_driver;
      default:
        return;
    }
  }//End of method

//----------------------------------------------------------
/**
 * Load the code for a specific Viewer driver. protected function.
 */
protected function _initDriver(){
  if(empty($this->_driverName))
  	$this->_driverName = 'default';
	$driverClass = 'Rb_Viewer_'.$this->_driverName;
	$this->_driver = new $driverClass($this);
	return true;
} //End of method

//----------------------------------------------------------
/**
* Get the properties from fsdata object
*/
protected function _initFsdata(Rb_Fsdata $fsdata){
  $this->_fsdata =& $fsdata;
  $this->_file_path       = $this->_fsdata->getProperty('file');
  $this->_file_extension  = $this->_fsdata->getProperty('file_extension');
  $this->_mimetype        = $this->_fsdata->getProperty('file_mimetype');
  $this->_noread          = $this->_fsdata->getProperty('no_read');
  $this->_driverName      = $this->_fsdata->getProperty('viewer_driver');
  return true;
} //End of method

//----------------------------------------------------------
/**
* Get the properties from docfile object
*/
protected function _initDocfile(Rb_Docfile $docfile){
  $this->_fsdata =& $docfile->getFsdata();
  $this->_file_path       = $docfile->getProperty('file');
  $this->_file_extension  = $docfile->getProperty('file_extension');
  $this->_mimetype        = $docfile->getProperty('file_mimetype');
  $this->_noread          = $this->_fsdata->getProperty('no_read');
  $this->_driverName      = $this->_fsdata->getProperty('viewer_driver');
  return true;
} //End of method

//----------------------------------------------------------
/**
 * Init the properties of the current object from the object ofile
 */
protected function _initFile($file){
  if(is_file($file)){
    //require_once('core/file.php');
    $this->_fsdata = new Rb_File($file);
  }else if(is_dir($file)){
    //require_once('core/fsdata.php');
    $this->_fsdata = new Rb_Fsdata($file);
  }else return false;
  $this->_file_path       = $this->_fsdata->getProperty('file');
  $this->_file_extension  = $this->_fsdata->getProperty('file_extension');
  $this->_mimetype        = $this->_fsdata->getProperty('file_mimetype');
  $this->_noread          = $this->_fsdata->getProperty('no_read');
  $this->_driverName      = $this->_fsdata->getProperty('viewer_driver');
  return true;
} //End of method

//-------------------------------------------------------------------------
/*! \brief Generate a embeded Viewer for the file
*  Return html code of the embeded Viewer
* 
* \param 
*/
protected function _embeded(){
  if(isset($this->_driver) && isset($this->_file_path))
    return $this->_driver->embededViewer($this->_file_path);
  else
    return false;
}//End of method

//-------------------------------------------------------------------------
/*! \brief Push the file to the client
* 
* This method send the content of the $file to the client browser. It send too the mimeType to help the browser to choose the right application to use for open the file.
* If the mimetype = no_read, display a error.
* Return true or false.
  \param $file(string) path of the file to display on the client computer
*/
protected function _pushfile(){
  if($this->_noread == 'no_read'){
    $this->error_stack->push(Rb_Error::ERROR, array('element'=>$file),
                                'you cant view this file %element% by this way');
    return false;
  }
  $this->_fsdata->downloadFile(); die;
}//End of method

//-------------------------------------------------------------------------
/*! \brief Push main file to download it
*/
public function push(){
  return $this->_pushfile();
}//End of method

//-------------------------------------------------------------------------
/*! \brief Show the visualisation file of the current document in a html page (embeded)
*   
* \param 
*/
public function display(){
  $this->_initDriver(); //init the driver for the file
  return $this->_embeded(); //Return the embeded Viewer
}//End of method

}//End of class

