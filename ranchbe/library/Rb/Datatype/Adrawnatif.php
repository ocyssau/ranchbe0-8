<?php
//require_once('core/datatype/cadds.php');

/*! \brief a cadds data directory
*
*/
class Rb_Datatype_Adrawnatif extends Rb_Datatype_Cadds{

  protected $path; //(string) full path to cadds data directory
  protected $mainfile; //(string) full path to cadds main file

  protected $test_mode=false; //(Bool) if true, test write operations only

  public $displayMd5 = false; //true if you want return the md5 property of the file
  public $file_props; //(array) content all properties of the file.

//----------------------------------------------------------
  function __construct($path){
    $this->path = $path;
    $this->mainfile = $this->path.'/_pd';
    $this->file_props = array();
    $this->file_props['file_type'] = 'adrawc4';
    $this->_checkPath(); //check path to prevent lost of data
  }//End of method

//----------------------------------------------------------
/*! \brief
* 
*/
function getProperty($property_name){
	if(!is_dir($this->path))
	 return false;
  switch ($property_name){

    case 'file_name':
      if(isset($this->file_props['file_name'])) return $this->file_props['file_name'];
      return $this->file_props['file_name'] = basename(dirname($this->path)) .'/'. basename($this->path);
    break;
    
    case 'file_path':
      if(isset($this->file_props['file_path'])) return $this->file_props['file_path'];
      return $this->file_props['file_path'] = dirname(dirname($this->path));
    break;
  
    case 'file_root_name':
      if(isset($this->file_props['file_root_name'])) return $this->file_props['file_root_name'];
      return $this->file_props['file_root_name'] = basename(dirname($this->path)) .'/'. basename($this->path);
    break;

    case 'doc_name':
      if(isset($this->file_props['doc_name'])) return $this->file_props['doc_name'];
      return $this->file_props['doc_name'] = basename(dirname($this->path)).'.'.basename($this->path);
    break;

    case 'camu_path':
      if(isset($this->file_props['camu_path'])) return $this->file_props['camu_path'];
      return $this->file_props['camu_path'] = dirname($this->path);
    break;

    case 'camu_name':
      if(isset($this->file_props['camu_name'])) return $this->file_props['camu_name'];
      return $this->file_props['camu_name'] = dirname( $this->getProperty('file_name') );
    break;

    case 'adraw_name':
      if(isset($this->file_props['adraw_name'])) return $this->file_props['adraw_name'];
      return $this->file_props['adraw_name'] = basename( $this->getProperty('file_name') );
    break;

    default:
      return parent::getProperty($property_name);
    break;

  } //End of switch
}//End of method

//----------------------------------------------------------
/*! \brief move the current cadds part
* 
* \param $dst(string) fullpath to new file
* \param $replace(bool) true for replace file
*/
function move($dst , $replace=false){
  return false;
}//End of method

//----------------------------------------------------------
/*! \brief copy the current file
* 
* \param $dst(string) fullpath to new file
* \param $mode(integer) mode of the new file
* \param $replace(bool) true for replace existing file
*/
function copy($dst,$mode=0755,$replace=true){
  return false;
}//End of method

} //End of class

