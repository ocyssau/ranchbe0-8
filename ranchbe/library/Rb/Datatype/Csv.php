<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

class Rb_Datatype_Csv extends Rb_File{

	//---------------------------------------------------------------------------
	/**
	 * Open and parse the csv file
	 * 
	 * @param string 	$csvfile	Path to csvfile to parse
	 * @param integer 	$maxrow 	Max number of row to parse default 10000
	 * @return array | false
	 */
	static function parse( $csvfile , $maxrow=10000){
		if(!$handle = fopen($csvfile, "r")){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$csvfile), 'cant open %element%' );
			return false;
		}

		//Get the field name from the first line of csv file
		$fields = fgetcsv($handle, 1000, ";");
		if(!$fields[0]){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$csvfile),
				 'The file %element% is not a CSV file or has not a correct syntax' );
			return false;
		}

		while (!feof($handle) && $m != $maxrow) {
			$data = fgetcsv($handle, 1000, ";");
			if(@implode(' ',$data)){ //Test for ignore empty lines
				$temp_max = count($fields);
				for ($i = 0; $i < $temp_max; $i++){
					@$ar[$fields[$i]] = $data[$i];
				}
				$records[] = $ar;
			}
			$m++;
		}
		fclose($handle);

		if ($m == $maxrow){
			Ranchbe::getError()->info('The number of record is greater than %maxrow%. The %maxrow% first line only are display here', array('maxrow'=>$maxrow) );
		}

		if (!is_array($records)) {
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$records),
				'No records were found. Check the file please!' );
			return false;
		}

		return $records;
	}//End of method

}//End of class
