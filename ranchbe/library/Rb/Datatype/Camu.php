<?php
//require_once('core/datatype/cadds.php');

/*! \brief a cadds data directory
*
*/
class Rb_Datatype_Camu extends Rb_Datatype_Cadds{

  protected $path; //(string) full path to cadds data directory
  protected $mainfile; //(string) full path to cadds main file
  protected $adraws; //(array) fsdata objects of adraws
  protected $test_mode=false; //(Bool) if true, test write operations only

  public $displayMd5 = false; //true if you want return the md5 property of the file
  public $file_props; //(array) content all properties of the file.

//----------------------------------------------------------
function __construct($path){
    $this->path = $path;
    $this->mainfile = $this->path.'/_db';
    $this->file_props = array();
    $this->file_props['file_type'] = 'Camu';
    $this->_checkPath(); //check path to prevent lost of data
}//End of method

//----------------------------------------------------------
/*! \brief suppress the current cadds directory
*   Returns TRUE or FALSE
*/
function suppress(){
  //Test if they are adraw in this directory...if true, dont suppress it
  if($this->getAdraws()){
    Ranchbe::getError()->notice('Camu directory %file% contains adraws. Camu directory will be not suppressed.', array('file'=>$this->path));
  }
  if(!$this->putInTrash()){
    Ranchbe::getError()->error('suppress of Camu directory %element% failed', array('file'=>$this->path) );
    return false;
  }else return true;
}//End of method

//-------------------------------------------------------------------------------
/*! \brief put cadds directory in the trash dir
*   Returns TRUE or FALSE
*/
function putInTrash($verbose = false, $trashDir=DEFAULT_TRASH_DIR){
  //Test if they are adraw in this directory...if true, dont suppress it
  if($this->getAdraws()){
    Ranchbe::getError()->notice('Camu directory %file% contains adraws. Camu directory will be not suppressed.', array('file'=>$this->path));
  }
  if( $this->test_mode) return true;
  return Rb_Directory::putDirInTrash($this->path, $verbose, false, $trashDir);
}//End of function

//----------------------------------------------------------
/*! \brief copy the current file
* 
* \param $dst(string) fullpath to new path
* \param $mode(integer) mode of the new file
* \param $replace(bool) true for replace existing file
*/
function copy($dst,$mode=0755,$replace=true){
  if(!Rb_Filesystem::limitDir($this->path)){
    Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$this->file), 'copy error : you have no right access on file %element%');
    return false;
  }

  if(!Rb_Filesystem::limitDir($dst)){
    Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$dst), 'copy error : you have no right access on file %element%');
    return false;
  }

	if(is_file($dst.'/'.$this->getProperty('file_extension')) && !$replace){
    Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$dst.'/'.$this->getProperty('file_extension')), 'copy error: file %element% exist');
    return false;
	}

	if(is_file($this->mainfile)){
    if( $this->test_mode) return true;
    if(Rb_Directory::dircopy($this->path, $dst ,false, true, false, 0755)){
      touch($dst.'/'.$this->getProperty('file_extension'), filemtime($this->mainfile));
      chmod ($dst.'/'.$this->getProperty('file_extension') , $mode);
      return true;
    }
	}

  Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$this->file), 'copy error on cadds part %element%');
  return false;

}//End of method

//----------------------------------------------------------
/*! \brief move the current cadds part
* 
* \param $dst(string) fullpath to new file
* \param $replace(bool) true for replace file
*/
function move($dst , $replace=false){
  if(!is_dir($this->path)){
    Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$this->file), '%element% is not a cadds directory');
    return false;
  }

  if (!Rb_Filesystem::limitDir($dst)){
    Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$dst), 'you have no right access on file %element%');
    return false;
  }

  if(!is_dir(dirname($dst))){
    Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>dirname($dst)), 'the directory %element% dont exist');
    return false;
  }

  if( $this->test_mode) return true;

  //Copy file , and suppress file
  if(Rb_Directory::dircopy($this->path, $dst ,false, true, false, 0755)){
    $this->suppress();
    $this->path = $dst;
    unset($this->file_props['file_name']);
    unset($this->file_props['file_path']);
    unset($this->file_props['file_root_name']);
    return true;
  }else return false;

}//End of method

//--------------------------------------------------------
/*! \brief get adraw from a Camu directory
*   Return an array of fsdata object if no errors else return false
*   
* \param $init(bool) if true, re-scan Camu directory, else return existing $adraws array
*/
function getAdraws($init = false){
  if(isset($this->adraws) && !$init) return $this->adraws;
  if(!$handle  = opendir($this->path)){
    print 'failed to open ' . $this->path . '<br>';
    return false ;
  }
  //require_once('core/fsdata.php');
  while ($item = @readdir($handle)){ //list adraw of the directory "Camu"
    if( $item == '.' || $item == '..' ) continue;
    $path = $this->path.'/'.$item;
    if(is_dir($path)){
      $datatype=Rb_Fsdata::setDataType($path);
      //Check if subdir is a caddspart
      if ($datatype['type'] == 'adrawc4' || $datatype['type'] == 'adrawc5' ){
        $this->adraws[] =& Rb_Fsdata::_dataFactory($path);
      }else{print 'error: presence of a unknow subdir in Camu directory <br />';}
    } //End of is_dir
 	} //End of while

  if(is_array($this->adraws)) return $this->adraws;
  return false;
}//End of method

} //End of class

