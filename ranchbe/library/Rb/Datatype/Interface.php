<?php

//==================================================================

/*! \brief represent a file on the filesystem
*
*/

interface Rb_Datatype_Interface{

//----------------------------------------------------------
/*! \brief Get the property of the document by the property name. init() must be call before
* 
*/
function getProperty($property_name);

//-------------------------------------------------------
/*! \brief Get infos about the file or the cadds part.
* Return a array if no errors, else return FALSE.
*
* \param $file(string) Full path to file.
* \param $displayMd5(bool) true if you want return the md5 code of the file.
*/
function getInfos($displayMd5=true);

//-------------------------------------------------------
/*! \brief Return file extension.(ie: /dir/file.ext, return '.ext')
*
* \param $file(string) File name. 
*/
function getExtension();

//-------------------------------------------------------
/*! \brief Return root name of file.(ie: /dir/file.ext, return 'file')
*
* \param $file(string) File name. 
*/
function getRoot();

//----------------------------------------------------------
/*! \brief move the current file
* 
* \param $dst(string) fullpath to new file
* \param $replace(bool) true for replace file
*/
function move($dst , $replace=false);

//----------------------------------------------------------
/*! \brief copy the current file
* 
* \param $dst(string) fullpath to new file
* \param $mode(integer) mode of the new file
* \param $replace(bool) true for replace existing file
*/
function copy($dst,$mode=0755,$replace=true);

//----------------------------------------------------------
/*! \brief suppress the current attachment file
* 
* \param $dst(string) fullpath to new file
*/
function suppress();

//-------------------------------------------------------------------------------
/*! \brief put file in the trash dir
*   Returns TRUE or FALSE
*/
function putInTrash($verbose = false, $trashDir=DEFAULT_TRASH_DIR);

//-------------------------------------------------------------------------------
function downloadFile();

} //End of class


