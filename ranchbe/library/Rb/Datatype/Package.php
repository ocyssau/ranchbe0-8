<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this Package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


/*Events :
onExtractBegin, //Begin the extraction of archive file
onExtractEnd, //End of extraction
*/

/*! \brief This class manage the import of files or documents in the containers.
 */
class Rb_Datatype_Package extends Rb_File implements Rb_Observable_Interface {
	protected $_unpack; //List of unpack files
	protected $_unpack_Root; //List of unpack files/directory but only from root of archive. Ignore contents of subdirectories.
	protected $_imported_iterator = 0; //Count the number of files imported in database
	protected $_observers = array ();
	//Properties used by progressObserver
	public $pb_current_name = '';
	public $pb_max = 0;

	//---------------------------------------------------------------------------
	/** \brief Uncompress Package
	 *  Return true or false.
	 *
	 \param $targetDir(string) Path of the dir where put files of the Package file.
	 */
	public function unpack($targetDir) {
		if (! is_dir ( $targetDir )) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('element' => $targetDir ), '%element% is not directory' );
			return false;
		}
		if (! is_file ( $this->file )) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('element' => $this->file ), '%element% is not a file' );
			return false;
		}

		if ($this->getExtension () == '.Z') {
			if (exec ( UNZIPCMD . " $this->file" )) {
				//@todo: suppress $file from history table
				$this->getImportHistory ()->updateImportPackage ( array ('package_file_name' => $this->file ), NULL, $this->file );
				$this->file = rtrim ( $this->file, '.Z' );
				$this->file_props = array ();
			} else {
				Ranchbe::getError()->push ( Rb_Error::ERROR, array ('element' => $this->file ), 'cant uncompress this file %element%' );
				return false;
			}
		}
		$this->_notify_all ( 'onExtractBegin', $this );

		if (substr ( $this->file, strrpos ( $this->file, '.' ) ) == '.tar') {
			/*
			 define('TARCMD','C:/Program Files/GnuWin32/bin/tar');
			 $cd = getcwd(); //Remember the current directory
			 if( chdir($targetDir) ){
			 system(TARCMD.' -xvf '.$file, $retval);
			 }
			 var_dump(TARCMD.' -xvf '.$file);
			 var_dump($retval);
			 chdir($cd);
			 */
				
			/*
			 require_once('Archive/Tar.php');
			 $tar = new Archive_Tar($this->file);
			 $res_tar = $tar->extract($targetDir);
			 if( $res_tar === false ){
			 print 'extraction has failed for unknow error<br />';
			 var_dump($res_tar);die;
			 }
			 unset($tar);
			 */
				
			$cmd = 'tar -xvf ' . $this->file . ' -C ' . $targetDir;
			exec ( $cmd, $output, $return_var );
				
			if (! $output) {
				var_dump ( $output );
				print 'extraction has failed for unknow error<br />';
				print 'or archive is maybe empty<br />';
				die ();
			} else {
				print '<b>Last tar message: ';
				print end ( $output ) . '</b><br />';
			}
			require_once 'File/Archive.php'; //File_Archive PEAR Package extension
			$result = File_Archive::read ( $this->file . '/', $targetDir . '/' );
		} else {
			require_once 'File/Archive.php'; //File_Archive PEAR Package extension
			//See File_Archive PEAR Package:
			File_Archive::extract ( $result = File_Archive::read ( $this->file . '/', $targetDir . '/' ), File_Archive::toFiles () );
			$result->close (); //Move back to the begining of the source
			/*while($result->next()){ //for each unpack file
			 $this->_unpack[] = $result->getFilename(); //Get the name of the unpack file
			 }*/
			//suppress since Ranchbe0.6 replace by next code :
		}

		$this->_notify_all ( 'onExtractEnd', $this );

		if (PEAR::isError ( $result )) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array (), $result->getMessage () . '<br />' . $result->getUserInfo () );
			return false;
		}
		return $result;
	} //End of method


	//---------------------------------------------------------------------------
	/** \brief Display the content of the Package file.
	 *   Return true or false.
	 *
	 *   \param Bool, if true return array, else return File_Archive_Result
	 */
	public function getContent($getArray = true) {
		if (! is_file ( $this->file ))
		return false;
		require_once 'File/Archive.php'; //File_Archive PEAR Package extension
		//See File_Archive PEAR Package :
		$result = File_Archive::read ( "$this->file/" );
		$result->close (); //Move back to the begining of the source
		if ($getArray) {
			$i = 0;
			while ( $result->next () ) { //for each unpack file
				$res [$i] = $result->getFilename (); //Get the name of the unpack file
				$res [$i] ['stat'] = $result->getStat ();
				$i ++;
			}
			return $res;
		} else return $result;
	} //End of method


	//---------------------------------------------------------------------------
	/**\brief Uncompress a archive file with the utility defined by UNZIPCMD.
	 *   Return the new file name or false.
	 *
	 */
	public function uncompress() {
		system ( UNZIPCMD . ' "' . $this->file . '"', $exec_return );
		if ($exec_return == 0) {
			$this->file = rtrim ( $this->file, '.Z' );
			$this->file_props = array ();
			return $this->file;
		} else {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('name' => $this->file ), 'cant uncompress this file %name%' );
			return false;
		}
	} //End of method


	//---------------------------------------------------------------------------
	/** Implement Rb_observable_Interface
	 */
	function attach($event, Rb_Observer_Interface &$obj) {
		if (! is_object ( $obj )) {
			return false;
		}
		$obj->setObserverId(uniqid ( rand () ));
		$this->_observers [$event] [$obj->_observerId] = &$obj;
	} //End of method


	/** Implement Rb_observable_Interface
	 */
	function attach_all(Rb_Observer_Interface &$obj) {
		if (! is_object ( $obj )) {
			return false;
		}
		$obj->setObserverId(uniqid ( rand () ));
		$this->_observers ['all'] [$obj->_observerId] = &$obj;
	} //End of method


	//---------------------------------------------------------------------------
	/** Implement Rb_observable_Interface
	 */
	function dettach(Rb_Observer_Interface &$obj) {
		if (isset ( $this->_observers [$obj->_observerId] )) {
			unset ( $this->_observers [$obj->_observerId] );
		}
	} //End of method


	//---------------------------------------------------------------------------
	/** Implement Rb_observable_Interface
	 */
	protected function _notify_all($event, Rb_Datatype_Package $msg) {
		if (isset ( $this->_observers [$event] )) {
			foreach ( $this->_observers [$event] as $observer ) {
				$observer->notify ( $event, $msg );
			}
		}
		if (isset ( $this->_observers ['all'] )) {
			foreach ( $this->_observers ['all'] as $observer ) {
				$observer->notify ( $event, $msg );
			}
		}
	} //End of method

}//End of class
