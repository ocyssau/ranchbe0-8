<?php
//require_once('core/datatype/adraw.php');

/*! \brief a cadds data directory
*
*/
class Rb_Datatype_Adrawc4 extends Rb_Datatype_Adraw{

//----------------------------------------------------------
  function __construct($path){
    $this->zipAdrawExt = '.Adrawc4';
    $this->zipAdrawDatatype = 'zipadrawc4';
    $this->reposit_sub_dir = '__caddsDatas';
    return parent::__construct($path);
  }//End of method

} //End of class

