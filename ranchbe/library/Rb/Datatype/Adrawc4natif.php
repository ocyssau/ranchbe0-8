<?php
//require_once('core/datatype/adraw.php');

/*! \brief a cadds data directory
*
*/
class Rb_Datatype_Adrawc4 extends Rb_Datatype_Adraw{

  protected $path; //(string) full path to cadds data directory
  protected $mainfile; //(string) full path to cadds main file

  public $displayMd5 = false; //true if you want return the md5 property of the file
  public $file_props; //(array) content all properties of the file.

//----------------------------------------------------------
  function __construct($path){
    $this->path = $path;
    $this->mainfile = $this->path.'/_pd';
    $this->file_props = array();
    $this->file_props['file_type'] = 'adrawc4';
    $this->_checkPath(); //check path to prevent lost of data
  }//End of method

} //End of class

