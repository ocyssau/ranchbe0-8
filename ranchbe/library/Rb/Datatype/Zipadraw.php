<?php
//require_once('core/file.php');

/*! \brief an zipped adraw cadds data file
*
*/
class Rb_Datatype_Zipadraw extends Rb_File{

  //protected $path; //(string) full path to cadds data directory
  protected $mainfile; //(string) full path to cadds main file
  protected $reposit_sub_dir='__caddsDatas'; //string sub directory of reposit dir where are stored the cadds datas

//----------------------------------------------------------
function __construct($file){
  $this->file = $file;
  $this->file_props = array();
  $this->file_props['file_type'] = 'Zipadraw';
  $this->file_type =& $this->file_props['file_type'];
  return true;
}//End of method

} //End of class

