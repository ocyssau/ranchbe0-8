<?php
//require_once('core/datatype/adraw.php');

/*! \brief a cadds data directory
*
*/
class Rb_Datatype_Adrawc5 extends Rb_Datatype_Adraw{

//----------------------------------------------------------
  function __construct($path){
    $this->path = $path;
    $this->mainfile = $this->path.'/_fd';
    $this->file_props = array();
    $this->file_props['file_type'] = 'Adrawc5';
    $this->_checkPath(); //check path to prevent lost of data
    $this->zipAdrawExt = '.Adrawc5';
    $this->zipAdrawDatatype = 'zipadrawc5';
    return true;
  }//End of method
  
} //End of class

