<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/acl/role/interface.php');
class Rb_Group implements Rb_Acl_Role_Interface{
  protected $error_stack;
  protected $group_id = 0;
  protected $core_props = array();
  protected $isActive = false;
  protected $users = 0; //(0, array) users of Group. 0 if not init, else array

  protected static $_groups = false; //(false or array) list of groups of ranchbe
  protected static $_ogroups = false; //(false or array) list of Group object
  protected static $_subgroups = false; //(false or array) list of Group object

  //----------------------------------------------------------------------------
  public function __construct($id=0){
    $this->error_stack =& Ranchbe::getError();
    if($id){
      $this->group_id = (int) $id;
      $this->core_props = Rb_Group::getInfos($id);
    }
  }//End of method
  
  //----------------------------------------------------------------------------
  /*! \brief factory method
  *
  */
  public static function get($id){
    if(!self::$_ogroups[$id])
      self::$_ogroups[$id] = new Rb_Group($id);
    return self::$_ogroups[$id];
   }//End of method

  //----------------------------------------------------------------------------
  /*! \brief defintion of method from Zend_Acl_Role_Interface. See Zend documentation
  * so this class is a role
  */
  public function getRoleId(){
    return self::getStaticRoleId( $this->getId() );
  }//End of method

  //----------------------------------------------------------------------------
  /*! \brief defintion of method from Zend_Acl_Role_Interface. See Zend documentation
  * so this class is a role
  */
  public static function getStaticRoleId($id){
    return $id;
  }//End of method
  
  //----------------------------------------------------------------------------
  public function getId(){
    return $this->group_id;
  }//End of method

  //----------------------------------------------------------------------------
  public function setGroupname($groupname){
    return $this->setProperty('group_define_name',$groupname);
  }//End of method

  //----------------------------------------------------------------------------
  public function getGroupname(){
    return $this->getProperty('group_define_name');
  }//End of method

  //----------------------------------------------------------------------------
  public function setProperty($property, $value){
    $this->core_props[$property] = $value;
    return $this;
  }//End of method

  //----------------------------------------------------------------------------
  public function getProperty($property){
    return $this->core_props[$property];
  }//End of method

  //----------------------------------------------------------------------------
  public function getProperties(){
    if(!isset($this->core_props)) return false;
    else return $this->core_props;
  }//End of method

  //----------------------------------------------------------------------------
  /*! \brief get infos about a Group
  *
  */
  public static function getInfos($group_id){
    if( self::$_groups[$group_id] )
      return self::$_groups[$group_id];
    return Ranchbe::getGroupAdapter()->getInfos($group_id);
  }//End of method

  //----------------------------------------------------------------------------
  /*! \brief get all groups recorded
  *
  */
  public static function getGroups(){
    if( !self::$_groups )
      self::$_groups = Ranchbe::getGroupAdapter()->getGroups();
    
    return self::$_groups;

  }//End of method

  //----------------------------------------------------------------------------
  /* get users of this Group
  * return a associative array where key user_id
  */
  public function getUsers(){
    if( !$this->users ){
      $this->users = Ranchbe::getGroupAdapter()->getUsers($this->group_id);
    }
    return $this->users;
  }//End of method

  //----------------------------------------------------------------------------
  /*! \brief get all users recorded
  *
  */
  public function getSubGroups(){
    if( !self::$_subgroups[$this->group_id] )
      self::$_subgroups[$this->group_id] = Ranchbe::getGroupAdapter()->getSubGroups($this->group_id);

    return self::$_subgroups[$this->group_id];

  }//End of method

  //----------------------------------------------------------------------------
  public function setSubGroup(Rb_Group &$subgroup){
    if($subgroup->getId() == $this->group_id) return false;

    //if try to add a child  group_id to a father fahter_id where father_id is yet child of group_id...
    //...prevent infinite loop
    $acl =& Ranchbe::getAcl();
    if( $acl->inheritsRole( $this, $subgroup) ){
      Ranchbe::getErrorstack()->push(Rb_Error::WARNING, array(),
        'Group '.$this->getGroupname().' is yet members of Group '.$subgroup->getGroupname() );
      return false;
    }

    return self::$_subgroups[$this->group_id][$subgroup->getId()] = $subgroup->getId();

  }//End of method

  //----------------------------------------------------------------------------
  public function unsetSubGroup(Rb_Group &$group){
    unset( self::$_subgroups[$this->group_id][$group->getId()] );
  }//End of method

  //----------------------------------------------------------------------------
  public function save(){
    $this->user_id = Ranchbe::getGroupAdapter()->save($this);
  }//End of method

  //----------------------------------------------------------------------------
  public function saveGroups(){
    Ranchbe::getGroupAdapter()->saveGroups($this);
  }//End of method

  //----------------------------------------------------------------------------
  public function suppress(){
    if($this->group_id == 21) return false; //can not suppress admin Group
    if( Ranchbe::getGroupAdapter()->suppress($this->group_id) ){
      unset( self::$_groups[$this->group_id] );
      unset( self::$_ogroups[$this->group_id] );
      unset( self::$_subgroups[$this->group_id] );
      return true;
    }
    return false;
  }//End of method

} //End of class
