<?php

class Rb_Converter_Transport_Soap implements Rb_Converter_Transport_Interface{
 
  private $_request;

  public function __construct(Rb_Converter_Request $request){
    $this->_request =& $request;
  }

  //------------------------------------------------------------------------------ 
  public function getOptions() {

    $options = array();

    $options['transport']  = 'SOAP';
    $options['to_file']    = $this->_request->file_name.'.'.$this->_request->toType; //result file name
    $options['from_mtype'] = $this->_request->file_extension;
    $options['to_mtype']   = $this->_request->toType;

    //put content in option array
    $handle = fopen($this->_request->file, "rb");
    $options['data'] = base64_encode( fread($handle, filesize($this->_request->file)) );
    fclose($handle);

    return $options;

  } //End of method

  //------------------------------------------------------------------------------ 
  public function getResult() {
    return array();
  } //End of method
 
} //End of class
