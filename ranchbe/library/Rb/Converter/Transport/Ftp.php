<?php

class Rb_Converter_Transport_Ftp implements Rb_Converter_Transport_Interface{
  
  private $_request;
  private $_ftpconnexion; //connexion to converter Ftp server
  private $_conf;

  public function __construct(Rb_Converter_Request $request){
    $this->_request =& $request;
    $this->_conf =& $this->_request->conf;
  }

  //------------------------------------------------------------------------------ 
  public function getOptions() {

    $options = array();

    //Send file to conversion server
    if(!$this->_ftp_put($this->_request->file, $this->_request->toFile)) return array();

    $options['transport']  = 'FTP';
    $options['from_file']  = $this->_request->toFile; //name of file to convert on server
    $options['to_file']    = $this->_request->file_name.'.'.$this->_request->toType; //result file name
    $options['from_mtype'] = $this->_request->file_extension;
    $options['to_mtype']   = $this->_request->toType;

    return $options;

  } //End of method

  //------------------------------------------------------------------------------ 
  public function getResult() {

    //Get converted file from Ftp server and put in temporary directory
    $temp_file = tempnam(sys_get_temp_dir(), 'rbc');
    $this->_ftp_get($this->_request->file_name.'.'.$this->_request->toType, $temp_file);
    $res['file'] = $temp_file;
    
    //Close Ftp connexion
    ftp_close($this->_ftpconnexion);

    return $res;

  } //End of method

  //------------------------------------------------------------------------------ 
  protected function _getFtpConnexion() {
    if(!$this->_ftpconnexion){
      //Connect to Ftp server
      if(!$this->_ftpconnexion = ftp_connect($this->_conf->ftp->host, $this->_conf->ftp->port)){
       echo "Connexion to Ftp server failed \n";
       return false;
      }
      //Authenticate on Ftp server
      ftp_login($this->_ftpconnexion, $this->_conf->ftp->user, $this->_conf->ftp->passwd);
    }

    return $this->_ftpconnexion;
  } //End of method

  //------------------------------------------------------------------------------ 
  protected function _ftp_put($localfile, $remotefile) {
    //Connect to Ftp server
    $ftp =& $this->_getFtpConnexion();

    //Put file on Ftp server
    if (!ftp_put( $ftp, $remotefile, $localfile, FTP_BINARY)) {
     echo "Il y a eu un probl�me lors du chargement du fichier $localfile\n";
     return false;
    }

    return true;
  } //End of method

  //------------------------------------------------------------------------------ 
  protected function _ftp_get($remotefile, $localfile) {

    //Connect to Ftp server
    $ftp =& $this->_getFtpConnexion();

    //Put file on Ftp server
    if (!ftp_get( $ftp, $localfile, $remotefile, FTP_BINARY)) {
     echo "Il y a eu un probl�me lors du chargement du fichier $remotefile\n";
     return false;
    }
    
    return true;

  } //End of method


} //End of class
