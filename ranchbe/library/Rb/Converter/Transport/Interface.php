<?php

interface Rb_Converter_Transport_Interface{
  
  //------------------------------------------------------------------------------ 
  /* return array of options to send to rbConveter server
  */
  public function getOptions();
  
  //------------------------------------------------------------------------------ 
  /* get result from rbConverter server result data
  *  return array
  */
  public function getResult();
  
} //End of class
