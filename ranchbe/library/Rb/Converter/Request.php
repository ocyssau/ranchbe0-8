<?php

class Rb_Converter_Request{
  
  private $_file;//full path to file to convert
  private $_file_extension;//original file extension
  private $_file_path;//path of original file to convert
  private $_file_basename; //basename of original file (without path)
  private $_file_name; //name of original file (without extension)
  private $_toType; //type of result file
  private $_toFile; //name of file to transfert to converter server
  private $_conf;

  public function __construct($file, $toType){
    //file infos
    $path_infos = pathinfo($file);
    $this->_file = $file;
    $this->_file_extension = strtolower($path_infos['extension']);
    $this->_file_path = $path_infos['dirname'];
    $this->_file_basename = $path_infos['basename'];
    $this->_file_name = $path_infos['filename'];
    $this->_toType = $toType;
    $this->_toFile = uniqId().'.'.$this->_file_extension;
    
    $baseConf = new Zend_Config_Ini(
                                  APPLICATION_PATH.'/conf/converter.ini',
                                  'base'
                                  );
    
    //Select the application
    $extension = $this->_file_extension;
    $application = $baseConf->application->$extension->$toType;
    
    //Get the configuration for application server
    $this->_conf = new Zend_Config_Ini(
                                    APPLICATION_PATH.'/conf/converter.ini',
                                    $application
                                    );
    
    switch($this->_conf->transport){
      case('soap'):
        $this->setTransport( new Rb_Converter_Transport_Soap($this) );
        break;
      case('ftp'):
        $this->setTransport( new Rb_Converter_Transport_Ftp($this) );
        break;
    }
  }

  //------------------------------------------------------------------------------ 
  public function convert() {
    
    $options = array();
    
    //Send soap Request to server
    $service = new SoapClient($this->_conf->soap->wsdluri, 
                              array('soap_version'=>$this->_conf->soap->version,
                                    'cache_wsdl'=>$this->_conf->soap->wsdlCache)
                             );

    $res = $service->convert( $this->_transport->getOptions() );

    if($res['error']){
      return $res;
    }

    $res['attachment'] = $this->_transport->getResult();

    return $res;

  } //End of method

  //------------------------------------------------------------------------------ 
  public function setTransport( Rb_Converter_Transport_Interface $transport) {
    $this->_transport = $transport;
  } //End of method

  //------------------------------------------------------------------------------ 
  public function __get($name) {
    switch($name){
      case 'file':
        return $this->_file;
        break;
      case 'toFile':
        return $this->_toFile;
        break;
      case 'file_name':
        return $this->_file_name;
        break;
      case 'toType':
        return $this->_toType;
        break;
      case 'file_extension':
        return $this->_file_extension;
        break;
      case 'conf':
        return $this->_conf;
        break;
    }
  } //End of method
  
} //End of class
