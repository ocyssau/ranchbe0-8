<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
 CREATE TABLE `projects` (
 `project_id` int(11) NOT NULL,
 `project_number` varchar(16) NOT NULL,
 `project_name` varchar(64) NOT NULL,
 `project_description` varchar(128) default NULL,
 `project_state` varchar(16) NOT NULL default 'init',
 `project_indice_id` int(11) default NULL,
 `default_process_id` int(11) default NULL,
 `open_by` int(11) default NULL,
 `open_date` int(11) default NULL,
 `forseen_close_date` int(11) default NULL,
 `close_by` int(11) default NULL,
 `close_date` int(11) default NULL,
 PRIMARY KEY  (`project_id`),
 UNIQUE KEY `UC_project_number` (`project_number`),
 KEY `INDEX_projects_1` (`project_indice_id`),
 KEY `INDEX_projects_2` (`project_description`)
 ) ENGINE=InnoDB;

 --
 -- Contraintes pour la table `projects`
 --
 ALTER TABLE `projects`
 ADD CONSTRAINT `FK_projects_1` FOREIGN KEY (`project_indice_id`)
 REFERENCES `indices` (`indice_id`),
 ADD CONSTRAINT `FK_projects_2` FOREIGN KEY (`default_process_id`)
 REFERENCES `galaxia_processes` (`pId`);

 CREATE TABLE `project_history` (
 `histo_order` int(11) NOT NULL,
 `action_name` varchar(32) NOT NULL,
 `action_by` varchar(32) NOT NULL,
 `action_date` int(11) NOT NULL,
 `project_id` int(11) NOT NULL,
 `project_number` varchar(16)  default NULL,
 `project_description` varchar(128) default NULL,
 `project_state` varchar(16)  default NULL,
 `project_indice_id` int(11) default NULL,
 `default_process_id` int(11) default NULL,
 `open_by` int(11) default NULL,
 `open_date` int(11) default NULL,
 `forseen_close_date` int(11) default NULL,
 `close_by` int(11) default NULL,
 `close_date` int(11) default NULL,
 PRIMARY KEY  (`histo_order`)
 ) ENGINE=InnoDB;

 --
 -- Contraintes pour la table `project_history`
 --
 ALTER TABLE `project_history`
 ADD CONSTRAINT `FK_project_history_1` FOREIGN KEY (`project_id`)
 REFERENCES `projects` (`project_id`) ON DELETE CASCADE;
 */

//require_once('core/object/acl.php');
//require_once('core/object/interface/related.php');

/*! \brief This class manage the projects.
 * A Project group containers and define property and permission common to workitem containers linked to the Project.
 * This class is finale because use of static var $_registry. Static var call by self::$_registry
 */
final class Rb_Project extends Rb_Object_Acl implements Rb_Object_Interface_Related{

	protected $OBJECT_TABLE = 'projects'; //(String)Table where are stored the containers definitions
	//Fields:
	protected $FIELDS_MAP_ID = 'project_id'; //(String)Name of the field of the primary key
	protected $FIELDS_MAP_NUM = 'project_number'; //(String)Name of the field to define the number
	protected $FIELDS_MAP_NAME = 'project_name'; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC = 'project_description'; //(String)Name of the field where is store the description
	protected $FIELDS_MAP_STATE = 'project_state'; //(String)Name of the field where is store the state
	protected $FIELDS_MAP_VERSION = 'project_indice_id'; //(String)Name of the field where is store the indices
	protected $FIELDS_MAP_FATHER; //(String)Name of the field where is store the id of the father

	//protected $_doctype    = false; //(object rb_project_doctypeProcessLink)
	protected $_doctypes   = array(); //(Array) definition of doctypes authorized for this container
	protected $_bookshop  = false; //(object rb_project_bookshopLink)
	protected $_cadlib    = false; //(object rb_project_cadlibLink)
	protected $_mockup    = false; //(object rb_project_mockupLink)
	
	/** Registry of constructed objects
	 * 
	 * @var Array
	 */
	protected static $_registry = array();

	/** id of default parent resource
	 * 
	 * @var Integer
	 */
	protected static $_defaultResource_id = 2;

	/** Default privileges name
	 * 
	 * @var Array
	 */
	protected $_privileges = array(
                              'get',
                              'create',
							  'edit',
                              'suppress',
                              'admin', //container link, doctypes, history
                              'admin_users', //permissions
                              'admin_container_alias', //create,suppress,edit alias
                              'document_create',
                              'document_edit',
                              'document_suppress',
                              'document_get');

	//-------------------------------------------------------------------------
	public function __construct($project_id = 0){
		$this->space =& Rb_Space::get('project'); //utile pour les historiques seulement?
		$this->_init($project_id);
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief init the properties of the Project
	 *  Return this
	 *
	 * \param $project_id(integer) Id of the Project
	 */
	protected function _init($project_id){
		$this->_dao = new Rb_Dao( Ranchbe::getDb() );
		$this->_dao->setTable($this->OBJECT_TABLE, 'object');
		$this->_dao->setTable('project_history', 'history');
		$this->_dao->setKey($this->FIELDS_MAP_ID, 'primary_key');
		$this->_dao->setKey($this->FIELDS_MAP_NUM, 'number');
		$this->_dao->setKey($this->FIELDS_MAP_NAME, 'name');
		$this->_dao->setKey($this->FIELDS_MAP_DESC, 'description');
		$this->_dao->setKey($this->FIELDS_MAP_STATE, 'state');
		$this->_dao->setKey($this->FIELDS_MAP_VERSION, 'version');
		return Rb_Object_Acl::_init($project_id);
	}//End of method

	//-------------------------------------------------------------------------
	/** Get the default resource for current object
	 *
	 * @return Rb_Acl_Resource_Default
	 */
	public function getDefaultResource() {
		if (! Ranchbe::getAcl ()->has ( static::$_defaultResource_id ) ) {
			$defaultResource = new Rb_Acl_Resource_Default( static::$_defaultResource_id );
			$defaultResource->setPrivileges( $this->_privileges );
			Ranchbe::getAcl ()->add ( $defaultResource, Ranchbe::getAcl ()->getRootResource () );
		}else{
			$defaultResource = Ranchbe::getAcl ()->get( static::$_defaultResource_id );
		}
		return $defaultResource;
	} //End of method


	//-------------------------------------------------------------------------
	/*! \brief init and clean current registry
	 */
	public static function initRegistry(){
		self::$_registry = array();
	}

	//-------------------------------------------------------------------------
	/*! \brief return a instance of object.
	 *   if none parameter return a special instance with no possibilies to change here properties
	 *   and wich can not be saved. This instance is just for use in static method where access to database is require,
	 *   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
	 *   If object "$id" dont exist return false.
	 *   If object "$id" has already been instantiated, then return this instance (singleton pattern).
	 *
	 */
	static function get($id = -1){
		if($id < 0) $id = -1; //forced to value -1

		if($id == 0)
		return new Rb_Project(0);

		if( !self::$_registry[$id] ){
			self::$_registry[$id] = new Rb_Project($id);
		}
		return self::$_registry[$id];
	}//End of method

	//-------------------------------------------------------------------------
	/*!\brief Get all projects.
	 * Return array or false.
	 *
	 \param $params(array) is use for manage the display. See parameters function of getQueryOptions().
	 */
	public function getAll( $params = array() ){
		return $this->_dao->getAllBasic($params);
	}//End of method

	//-------------------------------------------------------------------------
	/*!\brief get the father object of current object
	 * Return Rb_Object_Acl or false.
	 *
	 */
	function getFather(){
		if( $this->father ) return $this->father;
		return false;
	}//End of method

	//-------------------------------------------------------------------------
	/*!\brief set father of current object
	 * Return $this or false.
	 *
	 */
	function setFather(&$ranchbe){
		$this->father =& $ranchbe;
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief return value of property
	 */
	public function getProperty($property_name){
		switch($property_name){
			case 'project_name':
			case 'name':
				$property_name = 'project_name';
				break;
			case 'project_number':
			case 'number':
				$property_name = 'project_number';
				break;
			case 'project_description':
			case 'description':
				$property_name = 'project_description';
				break;
			case 'project_state':
			case 'state':
				$property_name = 'project_state';
				break;
			case 'project_indice_id':
			case 'indice':
				$property_name = 'project_indice_id';
				break;
			case 'default_process_id':
			case 'process_id':
			case 'process':
				$property_name = 'default_process_id';
				break;
			case 'open_by':
				$property_name = 'open_by';
				break;
			case 'open_date':
				$property_name = 'open_date';
				break;
			case 'forseen_close_date':
				$property_name = 'forseen_close_date';
				break;
			case 'close_by':
				$property_name = 'close_by';
				break;
			case 'close_date':
				$property_name = 'close_date';
				break;
		}
		if( array_key_exists($property_name, $this->core_props) )
		return $this->core_props[$property_name];
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief set the property $property_name with value of $property_value
	 * can not call this method from instance where id < 0.
	 * Return $this, so you can use sythax type $object->setProperty($property_name, $property_value)->getProperty($property_name);
	 *
	 */
	public function setProperty($property_name, $property_value){
		if($this->getId() < 0) return false; //Can not change property on object -1
		$this->isSaved = false;
		switch($property_name){
			case 'project_name':
			case 'name':
				$property_name = 'project_name';
				break;
			case 'project_number':
			case 'number':
				$property_name = 'project_number';
				break;
			case 'project_description':
			case 'description':
				$property_name = 'project_description';
				break;
			case 'project_state':
			case 'state':
				$property_name = 'project_state';
				break;
			case 'project_indice_id':
			case 'indice':
				$property_name = 'project_indice_id';
				break;
			case 'default_process_id':
			case 'process':
				$property_name = 'default_process_id';
				break;
			case 'open_by':
			case 'open_date':
			case 'forseen_close_date':
			case 'close_by':
			case 'close_date':
				break;
			default:
				return false;
				break;
		}
		$this->core_props[$property_name] = $property_value;
		return $this;
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief suppress current object database records, files, and all depandancies
	 * and clean the registry of current class to prevent recall of this instance.
	 * can not call this method from instance where id <= 0.
	 * Return false or true.
	 *
	 */
	public function suppress(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id), tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}

		if($ret = $this->_dao->suppress($this->_id)){
			$this->_suppressPermanentObject();
			//Write history
			$this->writeHistory('Suppress');
			//clean registry
			unset( self::$_registry[$this->_id] );
		}
		return $ret;
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief create a new database record for object.
	 *   Return the id if no errors, else return FALSE
	 *
	 */
	protected function _create(){
		if( $this->_id !== 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id), tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}

		if( !$this->getNumber() ){
			trigger_error('number is not set', E_USER_NOTICE);
			return false;
		}
		
		//Set current time
		$now = time();
		$this->setProperty('open_date', $now);
		$this->setProperty('update_date', $now);

		//Set current users
		$this->setProperty('open_by', Rb_User::getCurrentUser()->getId() );
		$this->setProperty('update_by', Rb_User::getCurrentUser()->getId() );
		$this->setProperty('project_state', 'init' );

		if ( !$this->getProperty('forseen_close_date') ){
			$this->setProperty('forseen_close_date',
			time() + Ranchbe::getConfig()->date->object->lifetime );
		}
		
		//Create a basic object
		$this->_id = $this->_dao->create( $this->getProperties() );
		if( $this->_id ){
			self::$_registry[$this->_id] =& $this;
			$this->isSaved = true;
		}else{
			Ranchbe::getError()->push(Rb_Error::ERROR,
						array( 'name'=>$this->getNumber() ),
						tra('cant create the Project %name%'));
			return $this->_id = 0;
		}
		return $this->_id;
	}//End of method

	//-------------------------------------------------------------------------
	/*!\brief write modification in database.
	 * Return true or false.
	 *
	 */
	protected function _update(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id), tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_ERROR);
			return false;
		}
		/*
		 $allow_input = array( 'project_number',
		 'project_description',
		 'project_indice',
		 'project_state',
		 'forseen_close_date',
		 'close_date',
		 'close_by',
		 'default_process_id',
		 );
		 $props = $this->getProperties();
		 //Filter input data for prevent maliscious informations
		 foreach ( $allow_input as $input){
		 if (isset($props[$input]))
		 $data[$input] = $props[$input];
		 }
		 */
		return $this->_dao->update( $this->getProperties() , $this->_id);
		var_dump($this);
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief return a list of all object recorded in database.
	 * return an array where key = id of object, and value = array of properties.
	 * $select is an no associative array to defined the sql select. Values defined name of fields to select
	 */
	public static function getInfos($project_id , array $selectClose=array()){
		return Rb_Project::get()->getDao()
		->getBasicInfos($project_id, array('select'=>$selectClose));
	}//End of method

	//----------------------------------------------------------
	/*!\brief Return the default process id of the Project.
	 * Return integer or false.
	 *
	 \param $project_id(integer) id of the Project.
	 */
	/*
	 public function getProcessId( $project_id ){
	 $select[] = 'default_process_id';
	 $result = $this->_dao->getBasicInfos($project_id , $select);
	 if (!$result) return false;
	 else return $result['default_process_id'];
	 }//End of method
	 */
	//-------------------------------------------------------------------------
	/*!\brief Get all ranchbe components linked to this Project.
	 * Return array or false.
	 *
	 */
	public function getChilds(){
		if( $this->_id < 1 ) trigger_error( '$this->_id is not set', E_USER_NOTICE);

		//require_once('core/container.php');
		$pw['with'][0] = array(
                          'type'=>'RIGHT OUTER',
                          'table'=> 'objects',
                          'col1'=> 'workitem_id',
                          'col2'=> 'object_id',
		);
		$pw['exact_find']['project_id'] = $this->_id;
		$pw['select'][] = 'workitem_id';
		$pw['select'][] = 'workitem_number';
		$pw['select'][] = 'workitem_name';
		$pw['select'][] = 'workitem_description';
		$pw['select'][] = 'workitem_version';
		$pw['select'][] = 'class_id';
		$pw['select'][] = 'space_id';
		$workitems   = Rb_Container::get('workitem')->getAll($pw);

		$containerRelation = new Rb_Project_Relation_Container();
		$mockups = $containerRelation->getMockups($this->_id);
		$cadlibs = $containerRelation->getCadlibs($this->_id);
		$bookshops = $containerRelation->getBookshops($this->_id);
		$doctypes = $containerRelation->getDoctypes(null, null, $this->_id);

		//$childs = $containerRelation->getChilds($this->_id);
		//var_dump($workitems);

		//$mockups     = $this->_getMockup()->getContainers();
		//$cadlibs     = $this->_getCadlib()->getContainers();
		//$bookshops   = $this->_getBookshop()->getContainers();
		//$process     = $processManager->getInfos($this->getProcessId($project_id));
		//$doctypes    = $this->getDoctype()->getDoctypes(NULL , NULL, $params);

		$objectsList=array();
		if ($workitems) $objectsList = $workitems;
		if ($mockups)   $objectsList = array_merge($mockups , $objectsList );
		if ($cadlibs)   $objectsList = array_merge($cadlibs , $objectsList );
		if ($bookshops) $objectsList = array_merge($bookshops , $objectsList );
		if ($doctypes)  $objectsList = array_merge($doctypes , $objectsList );
		//if ($partners)  $objectsList = array_merge($partners , $objectsList);
		//if ($process)   $objectsList = array_merge($process , $objectsList );

		if(is_null($objectsList))
		return false;

		//var_dump($objectsList);
		return $objectsList;
	}//End of method

	//----------------------------------------------------------
	/*!\brief Defined the default process of the Project.
	 * Return true or false.
	 *
	 \param $process_id(integer) id of process to link.
	 */
	/*
	 public function setDefaultProcess($process_id){
	 if( $this->_id < 1 ) trigger_error( '$this->_id is not set', , E_USER_WARNING);
	 $data['default_process_id'] = $process_id;
	 return $this->_basicUpdate( $data , $this->_id );
	 }//End of method
	 */
	//----------------------------------------------------------
	/*!\brief
	 * Get all doctypes linked to container
	 * Return false if error or an array with the doctype properties
	 *
	 \param $extension(string) limit the result to doctype with extension $extension.
	 \param $type(string) limit the result to doctype with type $type.
	 */
	public function getDoctypes($extension=0 , $type=0){
		if( !$this->_doctypes[$extension][$type] ){
			$this->_doctypes[$extension][$type] =
			Rb_Project_Relation_Doctype::get()
			->getDoctypes($extension,$type,$this->_id);
		}
		return $this->_doctypes[$extension][$type];
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 * get the doctype link object
	 * Return false if error or object
	 *
	 */
	/*
	 public function getDoctype(){
	 if( !$this->_doctype ){
	 if( $this->_id < 1 ) trigger_error( '$this->_id is not set', E_USER_WARNING);
	 //require_once('core/Project/relation/doctype.php');
	 $this->_doctype = new Rb_Project_Relation_Doctype();
	 }
	 return $this->_doctype;
	 }//End of method
	 */

	//----------------------------------------------------------------------------
	/*
	 */
	/*
	 public function getContainerLink($space_name){
	 switch($space_name){
	 case 'bookshop':
	 return $this->_getBookshop();
	 case 'cadlib':
	 return $this->_getCadlib();
	 case 'mockup':
	 return $this->_getMockup();
	 }
	 trigger_error( '$space_name is not set' );
	 return false;
	 }
	 */

	//----------------------------------------------------------------------------
	/* Accesor to bookshopLink object
	 */
	/*
	 protected function _getBookshop(){
	 if( !$this->_id ) trigger_error( '$this->_id is not set' );
	 if( !$this->_bookshop ){
	 //require_once('core/Project/bookshopLink.php');
	 $this->_bookshop = new rb_project_bookshopLink($this->_id);
	 }
	 return $this->_bookshop;
	 }
	 */
	//----------------------------------------------------------------------------
	/* Accesor to cadlibLink object
	 */
	/*
	 protected function _getCadlib(){
	 if( !$this->_id ) trigger_error( '$this->_id is not set' );
	 if( !$this->_cadlib ){
	 //require_once('core/Project/cadlibLink.php');
	 $this->_cadlib = new rb_project_cadlibLink($this->_id);
	 }
	 return $this->_cadlib;
	 }
	 */
	//----------------------------------------------------------------------------
	/* Accesor to mockupLink object
	 */
	/*
	 protected function _getMockup(){
	 if( !$this->_id ) trigger_error( '$this->_id is not set' );
	 if( !$this->_mockup ){
	 //require_once('core/Project/mockupLink.php');
	 $this->_mockup = new rb_project_mockupLink($this->_id);
	 }
	 return $this->_mockup;
	 }
	 */
}//End of class

