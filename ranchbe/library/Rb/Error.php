<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('PEAR/ErrorStack.php');

/*! \brief Class for manage errors occuring in ranchbe objects.
 *
 * This class extends the PEAR_ErrorStack class. PEAR_ErrorStack is the next generation
 * of error manager for PHP and replace the PEAR_error package.
 * See http://www.go-pear.org/manual/fr/core.pear.pear-errorstack.intro.php.<br>
 *
 * Errors are push in the stack by method error_stack->push(CODE(string), LEVEL(string), PARAMS(array), MSG(string) );<br>
 * The CODE = ERROR for an common error, ERROR_DB for an database error.<br>
 * LEVEL = Fatal, Warning, Info, Debug, Log<br>
 * PARAM = is a array with optionnals keys<ul>
 *    <li>element : for use in MSG</li>
 *    <li>query : is the content of the query. To use for debug</li>
 *    <li>debug : to records var content for debug</li></ul>
 * MSG is the message to display to user. If a key element is record in PARAM, the text %element% will be replaced by his value record in param array.<br>
 *
 * For control display to user the constant DEBUG of the ranchbe_setup.php file is set to true for
 * display all content of the error.
 * If set to false, display only the MSG.
 */
//class Rb_ErrorManager extends PEAR_ErrorStack {
class Rb_Error {
	
    const EMERG      = 0;  // Emergency: system is unusable
    const ALERT      = 1;  // Alert: action must be taken immediately
    const CRITICAL   = 2;  // Critical: critical conditions
    const ERROR      = 3;  // Error: error conditions
    const WARNING    = 4;  // Warning: warning conditions
    const NOTICE     = 5;  // Notice: normal but significant condition
    const INFO       = 6;  // Informational: informational messages
    const DEBUG      = 7;  // Debug: debug messages
    const DEPRECATED = 8;  // Deprecated: deprecated function or method
    const DISPLAY  	 = 256; //message to display
    
    /** 
     * Errors levels label for each error code
     * 
	 * @var array
	 * @access private
     */
    protected $_levels = array();
    
	/**
	 * Errors are stored in the order that they are pushed on the stack.
	 * @var array
	 * @access private
	 */
	protected $_errors = array();
	
	/**
	 * @var false|Zend_Log
	 * @access protected
	 */
	protected $_logger = false;
	
	/**
	 * Minimal code of error to log.
	 * -1: Log none
	 * 0: log EMERGENCY
	 * 1: log ALERT and EMERGENCY 
	 * 2: log ERROR and ALERT and EMERGENCY
	 * ...
	 * 8 or sup: Log all
	 * 
	 * @var integer
	 * @access protected
	 */
	protected $_logLevel = 0;
	
	
	/**
	 * Set up a new error stack
	 *
	 * @param string   $package name of the package this error stack represents
	 * @param callback $msgCallback callback used for error message generation
	 */
	function __construct($package)
	{
		$this->_package = $package;
		$this->_levels = array(
          self::EMERG =>'emergency',
          self::ALERT => 'alert',
          self::CRITICAL => 'critical',
          self::ERROR => 'error',
          self::WARNING => 'warning',
          self::NOTICE => 'notice',
          self::INFO => 'info',
          self::DEBUG => 'debug',
          self::DEPRECATED => 'deprecated',
          self::DISPLAY => 'display',
          );
	}
	
	/** 
	 * @param array
	 * @return void
	 *
	 */
	function _log($err)
	{
		if ( !$this->_logger ){
			return;
		}
		
		$level = $err['code'];
		
		if( $level <= $this->_logLevel ){
			$message = '[line: '.$err['context']['line']
			.'] [function: '.$err['context']['function']
			.'] ';
			if(isset($err['context']['class'])){
				$message .=  '[class: '. $err['context']['class'] . '] ';
			}
			$message .=  $err['message'];
			$this->_logger->log($message, $level);
		}
	}//End of method
	
	
	/** Check if errors occured and display user message.
	 *
	 * @param $params('close_button','back_button','home_button')
	 */
	public function checkErrors( $params=array() ){
		if ($this->hasErrors()){
			$errors = array();
			foreach($this->getErrors() as $error){
				if( $error['code'] <= Ranchbe::getConfig ()->error->display->level || $error['code'] >= Rb_Error::DISPLAY ){
					$errors[] = $error;
				}
			}
			if(count($errors) == 0) return;
			Ranchbe::getSmarty()->assign('errors', $errors);
			if( isset($params['close_button']) )
				Ranchbe::getSmarty()->assign('close_button', $params['close_button']);
			if( isset($params['back_button']) )
				Ranchbe::getSmarty()->assign('back_button', $params['back_button']);
			if( isset($params['home_button']) )
				Ranchbe::getSmarty()->assign('home_button', $params['home_button']);
			$path = & Ranchbe::getConfig ()->resources->smarty->scriptPath;
			$debug = Ranchbe::getConfig ()->debug->display;
			Ranchbe::getSmarty()->assign('debug', $debug);
			Ranchbe::getSmarty()->display($path.'error_stack.tpl');
		}
	}//End of method
	
	
	/** Flush the current stack in log file and clean up the message stack
	 *
	 */
	public function flush()
	{
		$errors = $this->getErrors(true);
		if (is_a($this->_logger, 'Rb_Log')) {
			foreach($errors as $error){
				$this->_log($error);
			}
		}
	}

	/**
	 * Retrieve all errors since last purge
	 *
	 * @param boolean set in order to empty the error stack
	 * @return array
	 */
	function getErrors($purge = false)
	{
		$ret = $this->_errors;
		if ($purge) {
			$this->_errors = array();
		}
		return $ret;
	}


	/**
	 *
	 * @return string
	 */
	function getLastMessage()
	{
		return $this->_errors[0]['message'];
	}
	
	/**
	 *
	 * @return string
	 */
	function getMessages()
	{
		$ret = array();
		foreach($this->_errors as $error){
			$ret[] = $error['message'];
		}
		return $ret;
	}
	

	/**
	 * Determine whether there are any errors on the stack
	 * @return boolean
	 */
	function hasErrors()
	{
		return count($this->_errors);
	}

	/**
	 * @param string $code
	 * @param array $params
	 * @param string $msg
	 */
	function push($code, $params = array(), $msg = false, &$backtrace = null)
	{
		// grab error context
		if(!$backtrace){
			$backtrace = debug_backtrace();
		}
		
		$context = array(
			'file' => $backtrace[1]['file'],
			'line' => $backtrace[1]['line'],
			'function' => $backtrace[1]['function'],
		);
		if(isset($backtrace[1]['class'])){
			$context['class'] = $backtrace[1]['class'];
		}
		
		foreach($params as $search=>$replace){
			/*
			if (is_object($replace)) {
				if (method_exists($replace, '__toString')) {
					$replace = $replace->__toString();
				}
			}
			*/
			$msg = str_replace('%' . $search . '%', $replace, $msg);
		}

		// save error
		$time = explode(' ', microtime());
		$time = $time[1] + $time[0];
		$err = array(
                'code' => $code,
                'package' => $this->_package,
                'level' => $this->_levels[$code],
                'time' => $time,
                'context' => $context,
                'message' => $msg,
		);
		array_unshift($this->_errors, $err);
		
		if ($this->_logger) {
			$this->_log($err);
		}
		
		return $err;
	}
	
	/**
	 * Record a error
	 * 
	 * @param string $msg
	 * @param array  array to set replacement in msg string
	 * @param array  debugging
	 * @return void
	 */
	function error($msg = false, array $replace=array(), array $debug = array() )
	{
		return $this->push(self::ERROR, $replace, $msg, debug_backtrace());
	}
	
	/**
	 * Record a error from adodb
	 * 
	 * @param string $msg
	 * @param array  array to set replacement in msg string
	 * @param string  sql request wich cause the error
	 * @return void
	 */
	function errorDb($msg = false, $sql = '' )
	{
		return $this->push(self::ERROR, $replace, $msg, debug_backtrace());
	}
	
	
	/**
	 * Record a warning
	 * 
	 * @param string $msg
	 * @param array  array to set replacement in msg string
	 * @param array  debugging
	 * @return void
	 */
	function warning($msg = false, array $replace=array(), array $debug = array() )
	{
		return $this->push(self::WARNING, $replace, $msg, debug_backtrace());
	}
	
	/**
	 * Record a notice
	 * 
	 * @param string $msg
	 * @param array  array to set replacement in msg string
	 * @param array  debugging
	 * @return void
	 */
	function notice($msg = false, array $replace=array(), array $debug = array() )
	{
		return $this->push(self::NOTICE, $replace, $msg, debug_backtrace());
	}
	
	/**
	 * Record a informational message
	 * 
	 * @param string $msg
	 * @param array  array to set replacement in msg string
	 * @return void
	 */
	function info($msg = false, array $replace=array() )
	{
		return $this->push(self::INFO, $replace, $msg, debug_backtrace());
	}
	
	
	/**
	 * Set up a Zend_Log object for this error stack
	 * @param Zend_Log $log
	 */
	function setLogger($log)
	{
		if (is_object($log) && method_exists($log, 'log') ) {
			$this->_logger = $log;
		}
	}
	
	
	/**
	 * Set up the level of error to log
	 * Minimal code of error to log.
	 * -1: Log none
	 * 0: log EMERGENCY
	 * 1: log ALERT and EMERGENCY 
	 * 2: log ERROR and ALERT and EMERGENCY
	 * ...
	 * 8 or sup: Log all
	 * 
	 * @param integer
	 */
	function setLoglevel($level = -1)
	{
		$this->_logLevel = (int) $level;
	}
	

}//End of class




