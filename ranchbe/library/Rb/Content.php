<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies Content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
 DROP VIEW contents;
 CREATE ALGORITHM = UNDEFINED VIEW contents AS

 SELECT document_id as id, document_number as name, description ,
 document_iteration as version , document_version as indice,
 "bookshop" as space_name, "document" as class_name
 FROM bookshop_documents

 UNION ALL
 SELECT document_id as id, document_number as name, description ,
 document_iteration as version , document_version as indice,
 "cadlib" as space_name, "document" as class_name
 FROM cadlib_documents

 UNION ALL
 SELECT document_id as id, document_number as name, description ,
 document_iteration as version , document_version as indice,
 "mockup" as space_name, "document" as class_name
 FROM mockup_documents

 UNION ALL
 SELECT document_id as id, document_number as name, description ,
 document_iteration as version , document_version as indice,
 "workitem" as space_name, "document" as class_name
 FROM workitem_documents


 UNION ALL
 SELECT file_id as id, file_name as name, file_root_name as description,
 file_iteration as version , file_iteration as indice,
 "bookshop" as space_name, "docfile" as class_name
 FROM bookshop_doc_files

 UNION ALL
 SELECT file_id as id, file_name as name, file_root_name as description,
 file_iteration as version , file_iteration as indice,
 "cadlib" as space_name, "docfile" as class_name
 FROM cadlib_doc_files

 UNION ALL
 SELECT file_id as id, file_name as name, file_root_name as description,
 file_iteration as version , file_iteration as indice,
 "mockup" as space_name, "docfile" as class_name
 FROM mockup_doc_files

 UNION ALL
 SELECT file_id as id, file_name as name, file_root_name as description,
 file_iteration as version , file_iteration as indice,
 "workitem" as space_name, "docfile" as class_name
 FROM workitem_doc_files


 UNION ALL
 SELECT file_id as id, file_name as name, file_root_name as description,
 file_iteration as version , file_iteration as indice,
 "bookshop" as space_name, "recordfile" as class_name
 FROM bookshop_files

 UNION ALL
 SELECT file_id as id, file_name as name, file_root_name as description,
 file_iteration as version , file_iteration as indice,
 "cadlib" as space_name, "recordfile" as class_name
 FROM cadlib_files

 UNION ALL
 SELECT file_id as id, file_name as name, file_root_name as description,
 file_iteration as version , file_iteration as indice,
 "mockup" as space_name, "recordfile" as class_name
 FROM mockup_files

 UNION ALL
 SELECT file_id as id, file_name as name, file_root_name as description,
 file_iteration as version , file_iteration as indice,
 "workitem" as space_name, "recordfile" as class_name
 FROM workitem_files;

 */

//require_once('core/dao/view.php');

//-------------------------------------------------------------------------
/** Content is a docfile, a recordfile or a document
 *
 * this class is used to access to this element (docfile, a recordfile or a document)
 * and specialy for search
 * the OBJECT_TABLE is a view of union of tables _documents, _docfiles, _recordfiles of all spaces
 */
class Rb_Content extends Rb_Dao_View{
	//DATABASE SETTING FOR USE BY CLASS basic
	protected $OBJECT_TABLE = 'contents'; //(String)Table where are stored the containers definitions

	//Fields:
	protected $FIELDS_MAP_ID = 'id'; //(String)Name of the field of the primary key
	protected $FIELDS_MAP_NUM = 'name'; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC = 'description'; //(String)Name of the field where is store the description
	protected $FIELDS_MAP_STATE = 'state'; //(String)Name of the field where is store the state
	protected $FIELDS_MAP_VERSION = 'version'; //(String)Name of the field where is store the indices
	protected $FIELDS_MAP_ITERATION = 'iteration'; //(String)Name of the field where is store the iteration
	protected $FIELDS_MAP_FATHER = 'father'; //(String)Name of the field where is store the id of the father

	/**
	 * 
	 * @return void
	 */
	public function __construct(){
		$this->dbranchbe =& Ranchbe::getDb();
	}//End of method

	/**
	 * 
	 * @param array $params
	 * @return array
	 */
	public function getAll(array $params = array() ){
		return $this->getAllBasic($params);
	}//End of method

} //end of class

