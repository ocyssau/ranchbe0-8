<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/object/relation.php');

/** Relation between project and doctypes. This is useful to define a set of
 *  doctypes for a project and limit creation of document with a Doctype compatible
 *  with this set.
 *
 **/  
class Rb_Project_Relation_Doctype extends Rb_Object_Relation{

  //-------------------------------------------------------------------------
  public static function get($id = -1){
    if($id < 0) $id = -1; //forced to value -1
    if($id == 0)
      return new self(0);
    if( !Rb_Object_Relation::$_registry[__CLASS__][$id] ){
      Rb_Object_Relation::$_registry[__CLASS__][$id] = new self($id);
    }
    return Rb_Object_Relation::$_registry[__CLASS__][$id];
  }//End of method

  //----------------------------------------------------------
  /*!\brief Get doctypes associated to project
  */
  public function getDoctypes($extension='' , $type = '' ,$of_project=0, $params=array()){
    if(!empty($extension)) //Get only the Doctype with the correct file extension
      $params['exact_find']['file_extension'] = $extension;
    if(!empty($type))
      $params['exact_find']['file_type'] = $type;

    $params['with'][] = array(
                          'table'=> 'doctypes',
                          'col1'=> 'child_id',
                          'col2'=> 'doctype_id',
                        );

    $params['with'][] = array(
                          'table'=> 'objects',
                          'col1'=> 'child_id',
                          'col2'=> 'object_id',
                        );

    $params['with'][] = array(
                          'type'=>'LEFT OUTER',
                          'table'=> GALAXIA_TABLE_PREFIX.'processes',
                          'col1'=> 'ext_id',
                          'col2'=> 'pId',
                        );
    $params['exact_find']['objects.class_id'] = 110;
    $params['select'][] = 'link_id';
    $params['select'][] = 'pId AS process_id';
    $params['select'][] = 'normalized_name AS process_nname';
    $params['select'][] = 'name AS process_name';
    $params['select'][] = 'description AS process_description';
    $params['select'][] = 'version AS process_version';
    $params['select'][] = 'doctype_id';
    $params['select'][] = 'doctype_number';
    $params['select'][] = 'file_extension';
    $params['select'][] = 'file_type';
    if( $of_project > 0 )
      $params['exact_find']['objects_rel.parent_id'] = $of_project;
    return $this->_dao->getAllBasic($params);
  }//End of method

} //End of class
