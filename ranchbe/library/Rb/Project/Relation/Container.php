<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/object/relation.php');

/** relations between project and bookshops. To affect bookshop to project
 *
 **/  
class Rb_Project_Relation_Container extends Rb_Object_Relation{

  //-------------------------------------------------------------------------
  public static function get($id = -1){
    if($id < 0) $id = -1; //forced to value -1
    if($id == 0)
      return new self(0);
    if( !Rb_Object_Relation::$_registry[__CLASS__][$id] ){
      Rb_Object_Relation::$_registry[__CLASS__][$id] = new self($id);
    }
    return Rb_Object_Relation::$_registry[__CLASS__][$id];
  }//End of method

  //----------------------------------------------------------
  /*!\brief get all containers of project
  */
  public function getChilds($of_project, array $params=array()){
    $params['with'][] = array(
                          'table'=> 'objects',
                          'col1'=> 'child_id',
                          'col2'=> 'object_id',
                        );

    $params['select'][] = 'link_id';
    $params['select'][] = 'object_id';
    $params['select'][] = 'class_id';
    $params['select'][] = 'space_id';

    if( $of_project > 0 )
      $params['exact_find']['objects_rel.parent_id'] = $of_project;

    /*
    20=>'Rb_Container_Bookshop',
    21=>'Rb_Container_Cadlib',
    22=>'Rb_Container_Mockup',
    23=>'Rb_Container_Workitem',
    */
    //$params['where'][]= 'objects.class_id > 19 AND objects.class_id < 24';

    return $this->_dao->getAllBasic($params);
  }//End of method

  //----------------------------------------------------------
  /*!\brief get bookshops of project
  */
  public function getBookshops($of_project, array $params=array()){
    $params['with'][] = array(
                          'table'=> 'objects',
                          'col1'=> 'child_id',
                          'col2'=> 'object_id',
                        );

    $params['with'][] = array(
                          'table'=> 'bookshops',
                          'col1'=> 'child_id',
                          'col2'=> 'bookshop_id',
                        );

    if( $of_project > 0 )
      $params['exact_find']['objects_rel.parent_id'] = $of_project;

    $params['select'][] = 'link_id';
    $params['select'][] = 'bookshop_id';
    $params['select'][] = 'bookshop_number';
    $params['select'][] = 'bookshop_name';
    $params['select'][] = 'bookshop_description';
    $params['select'][] = 'bookshop_version';
    $params['select'][] = 'class_id';
    $params['select'][] = 'space_id';

    /*
    20=>'rb_container_bookshop',
    21=>'rb_container_cadlib',
    22=>'rb_container_mockup',
    23=>'rb_container_workitem',
    */
    $params['exact_find']['objects.class_id'] = 20;

    return $this->_dao->getAllBasic($params);
  }//End of method

  //----------------------------------------------------------
  /*!\brief get cadlibs of project
  */
  public function getCadlibs($of_project, array $params=array()){
    $params['with'][] = array(
                          'table'=> 'objects',
                          'col1'=> 'child_id',
                          'col2'=> 'object_id',
                        );

    $params['with'][] = array(
                          'table'=> 'cadlibs',
                          'col1'=> 'child_id',
                          'col2'=> 'cadlib_id',
                        );

    if( $of_project > 0 )
      $params['exact_find']['objects_rel.parent_id'] = $of_project;

    $params['select'][] = 'link_id';
    $params['select'][] = 'cadlib_id';
    $params['select'][] = 'cadlib_number';
    $params['select'][] = 'cadlib_name';
    $params['select'][] = 'cadlib_description';
    $params['select'][] = 'cadlib_version';
    $params['select'][] = 'class_id';
    $params['select'][] = 'space_id';

    /*
    20=>'rb_container_bookshop',
    21=>'rb_container_cadlib',
    22=>'rb_container_mockup',
    23=>'rb_container_workitem',
    */
    $params['exact_find']['objects.class_id'] = 21;

    return $this->_dao->getAllBasic($params);
  }//End of method

  //----------------------------------------------------------
  /*!\brief get mockups of project
  */
  public function getMockups($of_project, array $params=array()){
    $params['with'][] = array(
                          'table'=> 'objects',
                          'col1'=> 'child_id',
                          'col2'=> 'object_id',
                        );

    $params['with'][] = array(
                          'table'=> 'mockups',
                          'col1'=> 'child_id',
                          'col2'=> 'mockup_id',
                        );

    if( $of_project > 0 )
      $params['exact_find']['objects_rel.parent_id'] = $of_project;

    $params['select'][] = 'link_id';
    $params['select'][] = 'mockup_id';
    $params['select'][] = 'mockup_number';
    $params['select'][] = 'mockup_name';
    $params['select'][] = 'mockup_description';
    $params['select'][] = 'mockup_version';
    $params['select'][] = 'class_id';
    $params['select'][] = 'space_id';

    /*
    20=>'rb_container_bookshop',
    21=>'rb_container_cadlib',
    22=>'rb_container_mockup',
    23=>'rb_container_workitem',
    */
    $params['exact_find']['objects.class_id'] = 22;

    return $this->_dao->getAllBasic($params);
  }//End of method

  //----------------------------------------------------------
  /*!\brief get workitems of project
  */
  /*
  public function getWorkitems($of_project, array $params=array()){
    $params['with'][] = array(
                          'type'=>'RIGHT OUTER',
                          'table'=> 'objects',
                          'col1'=> 'child_id',
                          'col2'=> 'object_id',
                        );

    $params['with'][] = array(
                          'type'=>'RIGHT OUTER',
                          'table'=> 'workitems',
                          'col1'=> 'child_id',
                          'col2'=> 'workitem_id',
                        );

    if( $of_project > 0 )
      $params['exact_find']['workitems.project_id'] = $of_project;

    $params['select'][] = 'link_id';
    $params['select'][] = 'workitem_id';
    $params['select'][] = 'workitem_number';
    $params['select'][] = 'workitem_name';
    $params['select'][] = 'workitem_description';
    $params['select'][] = 'workitem_version';
    $params['select'][] = 'class_id';
    $params['select'][] = 'space_id';

    $params['exact_find']['objects.class_id'] = 23;

    return $this->_dao->getAllBasic($params);
  }//End of method
  */

  //----------------------------------------------------------
  /*!\brief Get doctypes associated to project
  */
  public function getDoctypes($extension='' , $type = '' ,$of_project=0, $params=array()){
    if(!empty($extension)) //Get only the doctype with the correct file extension
      $params['exact_find']['file_extension'] = $extension;
    if(!empty($type))
      $params['exact_find']['file_type'] = $type;

    $params['with'][] = array(
                          'table'=> 'doctypes',
                          'col1'=> 'child_id',
                          'col2'=> 'doctype_id',
                        );

    $params['with'][] = array(
                          'table'=> 'objects',
                          'col1'=> 'child_id',
                          'col2'=> 'object_id',
                        );

    $params['exact_find']['objects.class_id'] = 110;

    $params['select'][] = 'link_id';
    $params['select'][] = 'doctype_id';
    $params['select'][] = 'doctype_number';
    $params['select'][] = 'file_extension';
    $params['select'][] = 'file_type';
    $params['select'][] = 'class_id';
    $params['select'][] = 'space_id';

    if( $of_project > 0 )
      $params['exact_find']['objects_rel.parent_id'] = $of_project;

    return $this->_dao->getAllBasic($params);
  }//End of method

} //End of class
