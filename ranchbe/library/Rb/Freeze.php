<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/directory.php');

class Rb_Freeze{

protected $container;
protected $freezeReposit;
protected $serializedReposit;

function __construct(){
  $this->error_stack =& Ranchbe::getError();
  //require_once('core/serialize.php');
  $this->serialize = new Rb_Serialize();
}//End of method

//------------------------------------------------------------------------------
protected function _initDirectory(){
  if(!is_dir($this->freezeReposit)){ //Check if reposit dir is existing
    if(!Rb_Directory::createdir($this->freezeReposit, 0755)){ //..and create it if not
      $this->error_stack->push(Rb_Error::ERROR, array( 'element'=>$this->freezeReposit ), 
                                          'cant create directory : %element%.');
      return false;
    }
  }
  if(!is_dir($this->serializedReposit)){ //Check if reposit dir is existing
    if(!Rb_Directory::createdir($this->serializedReposit, 0755)){ //..and create it if not
      $this->error_stack->push(Rb_Error::ERROR, array( 'element'=>$this->serializedReposit ), 
                                          'cant create directory : %element%.');
      return false;
    }
  }
  return true;
}//End of method

//------------------------------------------------------------------------------
public function freeze(&$object){
  if( is_a($object, docfile) ){
    $this->container =& $object->getFather()->getContainer();
  }else{
    $this->container =& $object->getFather();
  }

  $this->freezeReposit = $this->container->getProperty('default_file_path').'/__freeze';
  $this->serializedReposit = $this->freezeReposit.'/__serialized_datas';

  if( is_a($object, document) ){
    return $this->_freezeDocument($object);
  }else
  if( is_a($object, Rb_Recordfile_abstract) ){
    return $this->_freezeRecordfile($object);
  }
} //end of method

//------------------------------------------------------------------------------
protected function _freezeDocument(Rb_Document &$document){
  if( !$this->_initDirectory() ) return false;

  //Create serialized document file
  $serialized_data_file = $document->getProperty('document_number').'__'.
                          $document->getProperty('document_version').'__'.
                          $document->getProperty('document_iteration').
                          '.document_serialized.xml';
  $serialized_data_file = $this->serializedReposit.'/'.$serialized_data_file;
  if( !$serialized_data_handle = fopen( $serialized_data_file, 'w' ) ) {
    $this->error_stack->push(Rb_Error::ERROR, array( 'file'=>$serialized_data_file ),
                                                     tra('cant open file %file%') );
    return false;
  }
  fwrite($serialized_data_handle, $this->serialize->serializeDocument($document) );
  fclose($serialized_data_handle);

  //Create serialized docfiles file
  $docfiles =& $document->getDocfiles();
  if( is_array( $docfiles ) )
  foreach( $docfiles as $docfile ){
    if( !$this->_freezeRecordfile($docfile) ){
      $this->error_stack->push(Rb_Error::ERROR, 
            array( 'document'=>$document->getProperty('normalized_name') ),
            'an error is occured during Freeze of document %document%');
      return false;
    }
  }

  return true;

}//End of method

//------------------------------------------------------------------------------
protected function _freezeRecordfile( Rb_Recordfile_Abstract &$recordfile ){
  if( !$this->_initDirectory() ) return false;

  $serialized_data_file = $recordfile->getProperty('file_name').'__'.
                          $recordfile->getProperty('file_iteration').'.docfile_serialized.xml';
  $serialized_data_file = $this->serializedReposit.'/'.$serialized_data_file;
  if( !$serialized_data_handle = fopen( $serialized_data_file, 'w' ) ) {
    $this->error_stack->push(Rb_Error::ERROR, array( 'file'=>$serialized_data_file ), 'cant open file : %file%.');
    return false;  
  }
  fwrite( $serialized_data_handle, $this->serialize->serializeObject($recordfile) );
  fclose( $serialized_data_handle );

  return $recordfile->copyFile( $this->freezeReposit, true ); //replace files if exists

}//End of method


} //End of class

