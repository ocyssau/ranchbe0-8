<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
 CREATE TABLE `checkout_index` (
 `file_name` varchar(128)  NOT NULL default '',
 `description` varchar(128)  default NULL,
 `container_type` varchar(16)  NOT NULL default '',
 `container_id` int(11) NOT NULL default '0',
 `container_number` varchar(128)  NOT NULL default '',
 `check_out_by` int(11) NOT NULL default '0',
 `document_id` int(11) NOT NULL default '0'
 ) ENGINE=InnoDB ;
 */


/** This class is used to retrieve informations about checkout document
 *   from the file in the wildspace.
 *   The table checkout_index keep container name, space name, document name and
 *   user information about file to display to the final user.
 *
 */
class Rb_CheckoutIndex{
	protected $docfile;
	protected $space;
	protected $error_stack;
	protected $dbranchbe;
	protected $usr;
	protected $OBJECT_TABLE;
	protected $SPACE_NAME;

	//-------------------------------------------------------
	/**
	 * 
	 * @param Rb_Docfile $docfile
	 * @return Void
	 */
	function __construct(Rb_Docfile $docfile){
		$this->docfile =& $docfile;
		$this->space =& $docfile->getSpace();
		$this->error_stack =& $this->space->error_stack;
		$this->dbranchbe =& Ranchbe::getDb();
		$this->CurrentUserId = Rb_User::getCurrentUser()->getId();
		$this->OBJECT_TABLE  = 'checkout_index';
	}//End of method

	//-------------------------------------------------------
	/** Create entry in index table for retrieve container of a checkout file.
	 * When a file is checkOut, a record is create in the "checkout_index" table
	 * for record the file name attach to document that are checkOut.
	 * This infos are used for retrieve the file that are checkout from the list of files in the Wildspace.
	 * 
	 * @return Bool
	 */
	function indexCheckOutAdd(){
		$data['file_name']        = $this->docfile->getProperty('file_name');
		$data['container_type']   = $this->space->getName();
		$odocument                =& $this->docfile->getFather();
		$data['container_id']     = $odocument->getProperty('container_id');
		$data['description']      = $odocument->getProperty('description');
		$ocontainer               =& $odocument->getContainer();
		$data['container_number'] = $ocontainer->getNumber();
		$data['document_id']      = $this->docfile->getProperty('document_id');
		$data['check_out_by']     = $this->CurrentUserId;

		if (!$this->dbranchbe->AutoExecute( "checkout_index" , $data , 'INSERT')){
			$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>'INSERT' . implode(';',$data) , 'debug'=>array($data, "checkout_index")), $this->dbranchbe->ErrorMsg());
			return false;
		}
		return true;
	}//End of method

	//-------------------------------------------------------
	/** Suppress entry in checkout index table.
	 * 
	 * @return Bool
	 */
	function indexCheckOutDel(){
		$file_name = $this->docfile->getProperty('file_name');
		$query = 'DELETE FROM checkout_index WHERE file_name = \''.$file_name.'\'
                          AND container_type = \''.$this->space->getName().'\'';
		//Execute the query
		if (!$this->dbranchbe->Execute($query)){
			$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
		}
		return true;
	}//End of method

	//-------------------------------------------------------
	/** Get infos on stored document linked to "$file".
	 * 
	 * @param String $file
	 * @return Array
	 */
	static function getCheckoutIndex($file){
		$query = 'SELECT * FROM checkout_index WHERE file_name = \''.$file.'\'';
		if(!$infos = Ranchbe::getDb()->GetRow($query)){
			return false;
		}else return $infos;
	}//End of method

}//End of class


