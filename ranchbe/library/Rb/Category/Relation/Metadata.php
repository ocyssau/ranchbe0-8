<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/object/relation.php');

/** This is a link between metadata_dictionary, objects and a category.
 *  When a document is in category, a set of properties is automaticly
 *  affected to this document.
 *
 **/
class Rb_Category_Relation_Metadata extends Rb_Object_Relation{

  //-------------------------------------------------------------------------
  public static function get($id = -1){
    if($id < 0) $id = -1; //forced to value -1
    if($id == 0)
      return new self(0);
    if( !Rb_Object_Relation::$_registry[__CLASS__][$id] ){
      Rb_Object_Relation::$_registry[__CLASS__][$id] = new self($id);
    }
    return Rb_Object_Relation::$_registry[__CLASS__][$id];
  }//End of method

  //----------------------------------------------------------
  /*!\brief Get documents metadatas of container $id
  *  @param Mixte, if integer, return childs of the object id,
  *                if array, return childs of each object id in array 
  */
  public function getMetadatas( $of_parents, array $params=array() ){
    //Join objects dictionary table as child
    $params['with'][] = array(
                          'type'=>'RIGHT',
                          'table'=> 'metadata_dictionary',
                          'col1'=> 'child_id',
                          'col2'=> 'property_id',
                        );

    //Join objects table as child
    $params['with'][] = array(
                          'type'=>'RIGHT',
                          'table'=> 'objects',
                          'col1'=> 'child_id',
                          'col2'=> 'object_id',
                        );

    //Select objects of class 'rb_metadata_dictionary'
    $params['exact_find']['objects.class_id'] = 120;

    //Select only childs of objects class 'rb_category'
    //$params['where'][] = 'parent_id IN (SELECT object_id FROM objects WHERE class_id = 100)';

    if( is_array($of_parents) && $of_parents ){
      $params['where'][] = 'parent_id IN ('.implode(',', $of_parents).')';
      $params['extra'] = 'GROUP BY property_id';
    }else{
      if( $of_parents > 0 )
        $params['exact_find']['objects_rel.parent_id'] = $of_parents;
    }

    return $this->_dao->getAllBasic($params);
    
  }//End of method

} //End of class
