<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/object/relation.php');

/** To get relation between a category and some Doctype.
 *  A category define a set of doctypes. This relations may be used, to
 *  affect automaticly a category of a document with a Doctype of the list. 
 *
 **/
class Rb_Category_Relation_Doctype extends Rb_Object_Relation{

  //-------------------------------------------------------------------------
  public static function get($id = -1){
    if($id < 0) $id = -1; //forced to value -1
    if($id == 0)
      return new self(0);
    if( !Rb_Object_Relation::$_registry[__CLASS__][$id] ){
      Rb_Object_Relation::$_registry[__CLASS__][$id] = new self($id);
    }
    return Rb_Object_Relation::$_registry[__CLASS__][$id];
  }//End of method

  //----------------------------------------------------------
  /*!\brief Get doctypes permit for category $id
  *  @param Integer category id
  *  @param Array
  */
  public function getDoctypes( $id , $params=array() ){
    //Join doctypes table
    $params['with'][] = array(
                          'type'=>'RIGHT',
                          'table'=> 'doctypes',
                          'col1'=> 'child_id',
                          'col2'=> 'doctype_id',
                        );

    //Join objects table
    $params['with'][] = array(
                          'type'=>'RIGHT',
                          'table'=> 'objects',
                          'col1'=> 'child_id',
                          'col2'=> 'object_id',
                        );

    //Select only objects of class rb_doctype
    $params['exact_find']['objects.class_id'] = 110;

    //Select only childs of objects class 'rb_category'
    //$params['where'][] = 'parent_id IN (SELECT object_id FROM objects WHERE class_id = 100)';

    if($id > 0)
      $params['exact_find']['objects_rel.parent_id'] = $id;

    return $this->_dao->getAllBasic($params);
  }//End of method

} //End of class
