<?php
//require_once('core/observer/scriptcaller.php');


//Execute evenement scripts
//To associate a scripts on evenement just create a scripts in the datatype_scripts dir
//with name datatypeScript_[event name].php and adjust access right to this file : chown [ranchbe_user] [script file]; chmod 500 [script file];
//In this file, create function datatypeScript_[name of datatype]_[event](recordfile &$recordfile).
//This function must return a bool.
class Rb_Observer_Datatypescript extends Rb_Observer_Scriptcaller{
	protected $scriptsDir;

	function notify($event, &$recordfile) {
		if (! is_a ( $recordfile, 'Rb_Recordfile_Abstract' )) return false;
		$this->eventName = $event;
		$this->args = array (&$recordfile ); //pass by reference
		if ( !$recordfile->getFsdata() ) return false;
		$type = $recordfile->getProperty ( 'file_type' );
		$this->scriptsDir = Ranchbe::getConfig ()->path->scripts->datatype;
		$this->scriptFile = $this->scriptsDir . '/' . $type . '.php';
		$this->functionName = $this->eventName;
		$this->className = 'datatypeScript_' . $type;
		if (! $this->execute ()) {
			Ranchbe::getError()->push ( Rb_Error::WARNING,
			array('element1'=>$recordfile->getProperty('file_name'),
                        'element2'=>$this->scriptFile, 
                        'function'=>$this->functionName),
                'File %element1% : Script %element2% failed or have none return.'
                .' Add "return true" at function %function% end');
                return false;
		}
		return true;
	} //End of method


} //End of class
