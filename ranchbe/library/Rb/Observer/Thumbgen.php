<?php
//Generate thumbnails if necessary
class Rb_Observer_Thumbgen extends Rb_Observer{

	public function notify($event, &$document){
		if(!is_a($document, 'Rb_Document')) return false;
		if(	$event == 'doc_post_update' || $event == 'doc_post_store')
		{
			$thumbnail = new Rb_Document_Thumbnail($document);
			$thumbnail->generate();
		}
	} //End of method

} //End of class
