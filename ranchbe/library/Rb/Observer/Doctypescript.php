<?php
//require_once('core/observer/scriptcaller.php');


//Execute doctype scripts
//To associate a scripts on doctype save script in directory DEFAULT_DOCTYPE_SCRIPTS_DIR (see conf) and edit doctype to associate it this script.
//Adjust access right to scripts file : chown [ranchbe_user] [script file]; chmod 500 [script file];
//The script must defined function doctypeScript_[envent name]. This function must return a value TRUE or FALSE.
class Rb_Observer_Doctypescript extends Rb_Observer_Scriptcaller {
	
	protected $scriptsDir;
	
	function notify($event, &$document) {
		if (! is_a ( $document, 'Rb_Document' ))
			return false;
		$this->args = array (&$document ); //pass by reference
		$this->eventName = $event;
		$this->scriptsDir = Ranchbe::getConfig ()->path->scripts->doctype;
		
		$doctype = & $document->getDoctype ();
		if (! is_a ( $doctype, 'Rb_Doctype' )) {
			Ranchbe::getError()->push ( Rb_Error::WARNING, array (), 'doctype is not set' );
			return false;
		}
		switch ($event) {
			case ('doc_pre_update') :
				$script = $doctype->getProperty ( 'script_pre_update' );
				break;
			
			case ('doc_post_update') :
				$script = $doctype->getProperty ( 'script_post_update' );
				break;
			
			case ('doc_pre_store') :
				$script = $doctype->getProperty ( 'script_pre_store' );
				break;
			
			case ('doc_post_store') :
				$script = $doctype->getProperty ( 'script_post_store' );
				break;
				
			case ('doc_pre_create_solution') :
				break;
				
			case ('doc_post_create_solution') :
				break;
			
			default :
				$script = '';
				return true;
				break;
		}
		$this->scriptFile = $this->scriptsDir . '/' . $script;
		$this->functionName = $this->eventName;
		$this->className = 'doctypeScript_' . substr ( $script, 0, strrpos ( $script, '.' ) );
		if (! empty ( $script )) {
			if (! $this->execute ()) {
				Ranchbe::getError()->push ( Rb_Error::WARNING, 
					array (
						'element1' => $document->getProperty ( 'document_number' ), 
						'element2' => $this->scriptFile, 
						'element3' => $this->functionName ), 
						'Document %element1% : Script %element2% failed or have none return.' . ' Add "return true" at function end. Check that function %element3%' . ' is correctly defined in script file %element2%.' );
				return false;
			}
		}
		return true;
	} //End of method
} //End of class
