<?php

//Execute evenement scripts
//To associate a scripts on evenement just create a scripts in the events_scripts dir of the reposit_dir of container
//with name containerScript_[event name].php and adjust access right to this file : chown [ranchbe_user] [script file]; chmod 500 [script file];
class Rb_Observer_Containerscript extends Rb_Observer_Scriptcaller {
	protected $scriptsDir;
	
	function notify($event, &$document) {
		if (! is_a ( $document, 'Rb_Document' ))
			return false;
		$this->args = array (&$document ); //pass by reference
		$this->eventName = $event;
		$container = & $document->getContainer ();
		$this->className = 'containerScript_' . $container->getProperty ( 'container_number' );
		$this->scriptsDir = Ranchbe::getConfig ()->path->scripts->container;
		$this->scriptFile = $this->scriptsDir . '/' . $this->className . '.php';
		$this->functionName = $this->eventName;
		
		if (! $this->execute ()) {
			Ranchbe::getError()->push ( Rb_Error::WARNING, array (
				'docnumber' => $document->getProperty ( 'document_number' ), 
				'scriptfile' => $this->scriptFile ), 
				'Document %docnumber% : Script %scriptfile% failed or have none return. Add "return true" at function end' );
			return false;
		}
		return true;
	} //End of method

} //End of class

