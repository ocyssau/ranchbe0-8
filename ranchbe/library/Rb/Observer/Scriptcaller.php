<?php

/*
Execute scripts associated to events
Possible events: 
case('doc_pre_update'):
case('doc_post_update'):
case('doc_pre_reset'):
case('doc_post_reset'):
case('doc_pre_store'):
case('doc_post_store'):
case('doc_pre_checkout'):
case('doc_post_checkout'):
case('doc_pre_copy'):
case('doc_post_copy'):
case('doc_pre_move'):
case('doc_post_move'):
case('doc_pre_createVersion'):
case('doc_post_createVersion'):
case('doc_pre_view'):
case('doc_pre_associateFile'):
case('doc_post_associateFile'):
case('doc_pre_updateProperties'):
case('doc_post_updateProperties'):
case('doc_pre_suppress'):
case('doc_post_suppress'):

case('docfile_pre_update'):
case('docfile_post_update'):
case('docfile_pre_reset'):
case('docfile_post_reset'):
case('docfile_pre_store'):
case('docfile_post_store'):
case('docfile_pre_checkout'):
case('docfile_post_checkout'):

case('recordfile_pre_copy'):
case('recordfile_post_copy'):
case('recordfile_pre_move'):
case('recordfile_post_move'):
case('recordfile_post_putInWildspace'):
*/
abstract class Rb_Observer_Scriptcaller extends Rb_Observer{

  protected $args; //(array) array of arguments of function
  protected $scriptsDir; //(string) Directory where is store the script
  protected $eventName; //(string) Name of the event
  protected $scriptFile; //(string) fullpath to script file
  protected $functionName; //(string) Name of function to call defined in script file
  protected $className; //(string) Name of class to call defined in script file

  /*! \brief execute the user function
  *  Return result of user function or false if attempt function is not defined or true if function file is not find.
  * 
  */
  protected function execute(){
    if(is_file($this->scriptFile)){
    	include_once($this->scriptFile);
    	if( class_exists($this->className) ){
        $obj = new $this->className();
        if( method_exists($obj, $this->functionName) ){
          //echo 'execute function : '.$this->functionName.'<br>';
          return call_user_func_array(array($obj,$this->functionName), $this->args); //use call_user_func_array for use by ref args. args of call_user_func are not by reference.
        }else{
          Ranchbe::getError()->warning('the function %function% dont exist in class file %class%', array(
          										'class'=>$this->className, 
                                              	'file'=>$this->scriptFile, 
                                              	'function'=>$this->functionName));
        }
      }else{
        Ranchbe::getError()->warning('the class %class% dont exist in script file %file%', array(
        									'class'=>$this->className,
        									'file'=>$this->scriptFile));
      }
    }
    return true;
  } //End of method

} //End of class

