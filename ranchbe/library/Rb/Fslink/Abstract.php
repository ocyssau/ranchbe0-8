<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+



	
if(!function_exists ('dellink')){
	/**
	 *
	 * @param string $link
	 * @return boolean
	 */
	function dellink($link){
		$cmd =  Ranchbe::getConfig()->cmd->delln;
		if(!$cmd) return false;
		$cmd = str_replace('%path%', $link, $cmd);
		system($cmd, $retval);
		return !($retval);
	}
}

abstract class Rb_Fslink_Abstract{

	protected $_path;
	
	//----------------------------------------------------------
	/**
	 *
	 * @param string $path Path to link element
	 * @return void
	 */
	function __construct($path){
		$this->_path = $path;
	}//End of method
	
	//----------------------------------------------------------
	/** Get path to current link element
	 *
	 * @return string
	 */
	public function getPath(){
		return $this->_path;
	}//End of method
	
	//----------------------------------------------------------
	/** Create a new link
	 *
	 * @param string $from The link target, it is the original file or source file
	 * @param string $to The link name, it is the new name for file $target
	 * @return boolean
	 */
	abstract public static function create($from, $to);
	
	//----------------------------------------------------------
	/** Suppress current link only if it is a link
	 *
	 * @return boolean
	 */
	abstract public function suppress();

	//----------------------------------------------------------
	/** Test if the current path is a link
	 *
	 * @return boolean
	 */
	abstract public function test();

} //End of class
