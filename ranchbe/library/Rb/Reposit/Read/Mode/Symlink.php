<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/** Link between recordfile fsdata in reposit and symbolic link in read reposit
 *
 */
class Rb_Reposit_Read_Mode_Symlink implements Rb_Reposit_Read_Mode_Bylink{

	/**
	 * 
	 * @var Rb_Recordfile_Abstract
	 */
	protected $_recordfile = false;
	
	/**
	 * 
	 * @var Rb_Reposit_Read
	 */
	protected $_reposit = false;
	
	/**
	 * 
	 * @var Rb_Symlink
	 */
	protected $_link = false;

	//-------------------------------------------------------------------------
	/** 
	 * 
	 * @param Rb_Reposit_Read $reposit
	 * @param Rb_Recordfile_Abstract $recordfile
	 * @return void
	 */
	public function __construct(Rb_Reposit_Read &$reposit,
								Rb_Recordfile_Abstract &$recordfile){
		$this->_reposit =& $reposit;
		$this->_recordfile =& $recordfile;
		$symlink_path = $this->_reposit->getProperty('url')
							.'/'.$this->_recordfile->getProperty('file_name');
		$this->_link = new Rb_Symlink($symlink_path);
	}//End of method

	//----------------------------------------------------------
	/** Get the reposit
	 * 
	 * @return Rb_Reposit_Read
	 */
	public function getReposit(){
		return $this->_reposit;
	}//End of method

	//----------------------------------------------------------
	/** Get the recordfile
	 * 
	 * @return Rb_Recordfile_Abstract
	 */
	public function getRecordfile(){
		return $this->_recordfile;
	}//End of method

	//----------------------------------------------------------
	/** Get the symlink
	 * 
	 * @return Rb_Symlink
	 */
	public function getLink(){
		return $this->_link;
	}//End of method

	//----------------------------------------------------------
	/** Create Symlink in reposit directory
	 * 
	 * @return Rb_Symlink | false
	 */
	public function save(){
		if(!$this->_reposit || !$this->_recordfile) return false;
		if(!$this->_link) return false;
		/*
		 $symlink_path = $this->_reposit->getProperty('url')
						 .'/'.$this->_recordfile->getProperty('file_name');
		 */
		$target = $this->_recordfile->getProperty('file');
		$this->_link = Rb_Symlink::create( $target, $this->_link->getPath() );
		return $this->_link;
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Reposit/Read/Mode/Rb_Reposit_Read_Mode_Interface#suppress()
	 */
	public function suppress(){
		if(!$this->_link) return false;
		return $this->_link->suppress();
	}//End of method
	
}//End of class
