<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a Copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*! \brief Link between recordfile fsdata in reposit and symbolic link in read reposit
 *
 */
class Rb_Reposit_Read_Mode_Copy implements Rb_Reposit_Read_Mode_Interface{
	
	/**
	 * 
	 * @var Rb_Recordfile_Abstract
	 */
	protected $_recordfile = false;
	
	/**
	 * 
	 * @var Rb_Reposit_Read
	 */
	protected $_reposit = false;

	//-------------------------------------------------------------------------
	public function __construct(Rb_Reposit &$reposit,
								Rb_Recordfile_Abstract &$recordfile){
		$this->_reposit =& $reposit;
		$this->_recordfile =& $recordfile;
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Reposit/Read/Rb_Reposit_Read_Mode#getReposit()
	 */
	public function getReposit(){
		return $this->_reposit;
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Reposit/Read/Rb_Reposit_Read_Mode#getRecordfile()
	 */
	public function getRecordfile(){
		return $this->_recordfile;
	}//End of method

	//----------------------------------------------------------
	/** Create Copy in reposit directory
	 * 
	 * @see library/Rb/Reposit/Read/Rb_Reposit_Read_Mode#save()
	 */
	public function save(){
		if(!$this->_reposit || !$this->_recordfile) return false;
		$to = $this->_reposit->getProperty('url');
		return $this->_recordfile->copyFile($to, true, 0755);
	}//End of method
	
	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Reposit/Read/Mode/Rb_Reposit_Read_Mode_Interface#suppress()
	 */
	public function suppress(){
		//@todo : implement this method
		return;
	}//End of method
	
}//End of class
