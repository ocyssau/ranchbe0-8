<?php
class Rb_Reposit_Read_Dispatcher extends Rb_Observer{
	public function notify($event, &$message){
		switch($event){
			case('recordfile_post_move'):
			case('docfile_post_create'):
				if(is_a($message,'Rb_Docfile')){
					$docfile =& $message;
					$container = $docfile->getFather();
					while(!is_a($container,'Rb_Container')){
						$container = $container->getFather();
					}
					$reads = Rb_Reposit_Read::getReadReposit($container);
					foreach($reads as $read){
						$readlink = Rb_Reposit_Read::getReadLink($read, $docfile);
						$readlink->save();
					}
				}
				break;
			
			//When a docfile is move to another reposit, update links
			case('docfile_post_suppress'):
				if(is_a($message,'Rb_Docfile')){
					$docfile =& $message;
					if($docfile->getProperty('file_life_stage') > 1) return false;
					$container = $docfile->getFather();
					while(!is_a($container,'Rb_Container')){
						$container = $container->getFather();
					}
					$reads = Rb_Reposit_Read::getReadReposit($container);
					foreach($reads as $read){
						$readlink = Rb_Reposit_Read::getReadLink($read, $docfile);
						$readlink->suppress();
					}
				}
				break;
			
			//Update reads for docfile of document
			case('doc_pre_move'):
				if(is_a($message,'Rb_Document')){
					$document =& $message;
					$container = $document->getFather();
					while(!is_a($container,'Rb_Container')){
						$container = $container->getFather();
					}
					$reads = Rb_Reposit_Read::getReadReposit($container);
					$docfiles = $document->getDocfiles();
					foreach( $docfiles as $docfile ){
						foreach($reads as $read){
							$readlink = Rb_Reposit_Read::getReadLink($read, $docfile);
							$readlink->suppress();
						}
					}
				}
				break;

			//When a doc is associate to another container, update read links
			case('doc_post_move'):
				if(is_a($message,'Rb_Document')){
					$document =& $message;
					$container = $document->getFather();
					while(!is_a($container,'Rb_Container')){
						$container = $container->getFather();
					}
					$reads = Rb_Reposit_Read::getReadReposit($container);
					$docfiles = $document->getDocfiles();
					foreach( $docfiles as $docfile ){
						foreach($reads as $read){
							$readlink = Rb_Reposit_Read::getReadLink($read, $docfile);
							$readlink->save();
						}
					}
				}
				break;
		}
	} //End of method
} //End of class
