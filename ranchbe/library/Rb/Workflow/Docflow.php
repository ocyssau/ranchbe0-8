<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/Rb_Workflow_Workflow/workflow.php');

//-------------------------------------------------------------------------
/*! \brief This class manage Rb_Workflow_Workflow associate to document.
*
* Rb_Workflow_Workflow is an agrega of basics objects of the Rb_Workflow_Workflow : process, activity, instance
* 
* How to use :
* $Rb_Workflow_Docflow = new Rb_Workflow_Docflow($document); //First construct object from a document object
* $Rb_Workflow_Docflow->initProcess(); //init the process from the document or from the instance if init before
* 
* $Rb_Workflow_Docflow->initInstance(); //init the instance linked to document or return an not init instance object
* or
* $Rb_Workflow_Docflow->initInstance($instanceId);
* 
* $Rb_Workflow_Docflow->execute($activityId); //execute the activity
* 
* 
*/
class Rb_Workflow_Docflow extends Rb_Workflow_Workflow{

  protected $document; //(document object) father document

  //--------------------------------------------------------------------
  function __construct(Rb_Document &$document){
    $this->dbGalaxia =& Ranchbe::getDb();
    $this->document =& $document;
  }//End of method
  
  //-------------------------------------------------------------------------
  /*! \brief attach process
  *  Return object
  * 
  */
  function initProcess($process_id = 0){
    if( $process_id == 0 ){
      if( isset($this->instance) )
        $process_id = $this->instance->pId; //try to get process_id from instance
      if( empty($process_id) )
        $process_id = $this->document->getProcessId(); //try to get process id from document
    }
    return Rb_Workflow_Workflow::initProcess($process_id);
  }//End of method
  
  //-------------------------------------------------------------------------
  /*! \brief attach instance
  *  Return object
  * 
  */
  function initInstance($instance_id=0){
    if( !isset($this->instance) ){
      $this->instance = new Rb_Workflow_InstanceDocflow($this->dbGalaxia);
      $this->instance->setDocflow($this); //bi-Directionnal link : instance is link to Rb_Workflow_Docflow, Rb_Workflow_Docflow is link to instance
    }
    if( $instance_id == 0 ){ //try to get the instance_id from the document
      $instance_id = $this->document->getProperty('instance_id');
    }
    if( $instance_id != 0 ){ //try to get the instance_id from the document
      $this->instance->getInstance($instance_id);
    }
    return $this->instance;
  }//End of method
  
  //-------------------------------------------------------------------------
  /*! \brief get
  *  Return object
  * 
  */
  function getDocument(){
    return $this->document;
  }//End of method
  
  //-------------------------------------------------------------------------
  /*! \brief execute the activity
  * $iid and $name parameters are just to keep compatibility with Rb_Workflow_Workflow::execute method
  * but it never be to set.
  * 
  */
  function execute($activityId, $iid=NULL, $name=NULL){
  
    $iid = $this->document->getProperty('instance_id');
    $name = $this->document->getProperty('normalized_name');
    $accessCode = $this->document->getProperty('document_access_code');
    if($accessCode > 9) return false; //can not execute activity on locked document
  
    Ranchbe::getError()->push(Rb_Error::NOTICE, array(),
                 'execute activity id: '.$activityId );

    if( !Rb_Workflow_Workflow::execute($activityId, $iid, $name) ) return false;
  
    if(empty($iid)) { // If its empty, then link the current instance to document
      $iid = $this->instance->instanceId;
      $this->document->setInstance($iid, false); //...link the instance
      Ranchbe::getError()->push(Rb_Error::NOTICE, array(),
        'link instance : '.$iid.' to document : '.$this->document->getNumber() );
    }

    Ranchbe::getError()->push(Rb_Error::NOTICE, array(),
      'current linked instance id : '.$iid.' to document : '.$this->document->getNumber() );

    //Save document properties, instance_id and set in activities scripts
    $this->document->save(false, false);

    //Write history
    $history =& $this->document->getHistory();
    if ( $history ){
      $history->setProperty('instance_id', $iid);
      $history->setProperty('activity_id', $activityId);
      $this->document->writeHistory('changeState');
    }
  
    return $this->instance;
  
  }//End of method
  
  //---------------------------
  /* reset the process, unlink instance of document
  * 
  * \param $instanceId (int) optionnel. if omit get instance_id from the document
  */
  function resetProcess( $instanceId = 0){
    if( isset($this->document) ){
      Ranchbe::getError()->push(Rb_Error::NOTICE, array(),
        'unlink process from document :'.$this->document->getNumber() );
      if( $instanceId == 0 )
        $instanceId = $this->document->getProperty('instance_id');
    	$this->document->unsetInstance(false);
      $this->document->unLockDocument(false);
      $this->document->setProperty('document_state', 'aborted'); //Change state of document
      if($this->document->save()){
        //Write history
        if (isset($this->document->history)){
          $this->document->history['instance_id'] = $instanceId;
          $this->document->writeHistory('ChangeState');
        }
      }else return false;
    }
    return Rb_Workflow_Workflow::resetProcess($instanceId);
  } //End of function

  //---------------------------
  /* add a comment for this activity and create a comment in document history
  * 
  */
  public function setComment($title, $comment){
    if( !$activityId = $this->getActivity()->getActivityId() ) return false;
    parent::setComment($title, $comment);
    
    $history =& $this->document->getHistory();
    $p = array(
    	'select'=>array('histo_order'),
    	'exact_find'=>array('instance_id'=>$this->getInstance()->getInstanceId(),
    						'activity_id'=>$activityId,
    						'action_name'=>'changeState',
    						'action_by'=>Rb_User::getCurrentUser()->getName(),
    						),
    	'sort_field'=>'histo_order',
    	'sort_order'=>'DESC',
    );
    $histo_order = $history->getDao()->getOne ($p);
    if($histo_order)
      $this->document->getHistory()->update(array('comment'=>$title.' '.$comment), $histo_order);
    return true;
  }//End of method

}//End of class

