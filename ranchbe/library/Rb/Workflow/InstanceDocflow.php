<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


//-------------------------------------------------------------------------

//include_once (GALAXIA_LIBRARY . 'Galaxia/Instance.php');

/*! \brief This class manage instancce of a document workflow.
*
*/
class Rb_Workflow_InstanceDocflow extends Galaxia_Instance {
	
	protected $docflow; //(object)
	protected $usr; //(object)
	protected $user; //(string)
	
	public $error_stack; //(Object)
	public $logger; //(Object)
	

	//--------------------------------------------------------------------
	function __construct($db) {
		parent::__construct ( $db );
		$this->error_stack = & Ranchbe::getError();
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief Prepare and execute code of activity
*  Return boolean
* 
*/
	function setDocflow(Rb_Workflow_Docflow &$docflow) {
		$this->docflow = & $docflow;
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief Prepare and execute code of activity
*  Return boolean
* 
* this method is call by instance::sendTo methods to execute automatic activity
* she dont must be directly call to create a new instance.
* This method is used to set environnement of the activities scripts. Vars set here are accessible in activities scripts.
* You can use objects $activity, $document, $instance for example in activities scripts.
* the objects $this->docflow, $this->docflow->process, $this->docflow->document must be set before call
*/
	function executeActivity($activityId, $auto) {
		if (! isset ( $this->docflow )) {
			$this->error_stack->push ( Rb_Error::WARNING, array (), '$this->docflow is not set, execution failed' );
			return false;
		}
		
		//set references to object to use in activities scripts
		$baseActivity = new Galaxia_Activity ( $this->db );
		$activity = $baseActivity->getActivity ( $activityId );
		$process = & $this->docflow->getProcess ();
		$document = & $this->docflow->getDocument ();
		$container = & $document->getContainer (); //compatibility with ranchbe < 0.5.3
		$Manager = & $container; //compatibility with ranchbe < 0.5.3
		$docflow = & $this->docflow;
		$instance = & $this;
		$error_stack =& Ranchbe::getError();
		
		//set var to use in activities scripts
		$document_id = $document->getId ();
		$iid = $this->getInstanceId ();
		$process_id = $activity->getProcessId ();
		$user = $this->user;
		$_REQUEST ['iid'] = $iid; //this is necessary for galaxia pre_activity script to set instance object
		$_REQUEST ['activityId'] = $activityId; //$_REQUEST['activityId'] is use by instance::complete method to defined the activity to run.
		
		//test if a process is set
		if (is_null ( $process_id )) {
			$this->error_stack->push ( Rb_Error::WARNING, array (), 'process is not initialized' );
			return false;
		}
		
		$this->error_stack->push ( Rb_Error::INFO, array (), 'executeActivity' . $activity->getName () );
		
		if (empty ( $iid ) && $activity->getType () != 'start') {
			$this->error_stack->push ( Rb_Error::WARNING, array (), 'instanceId is empty' );
			return false;
		}
		
		//create and set instance property document_id to keep link between instance and document
		if ($activity->getType () == 'start') {
			$this->set ( 'document_id', $document->getId () );
		}
		
		$__activity_completed = false; //global var use by ranchbe but why???
		
		// Determine the activity using the activityId request parameter and get the activity information
		// load then the compiled version of the activity
		
		// load then the compiled version of the activity
		//compileActivity is defined in galaxia_setup.php
		$codes = self::compileActivity ( $process, $activity ); //recompile activity if necessary and return path to scripts
		
		$this->error_stack->push ( LOG, 'Debug', array (), 'include activity script :' . $codes ['shared'] );
		if (! include ($codes ['shared']))
			return false; // Include the shared code
		

		$this->error_stack->push ( LOG, 'Debug', array (), 'include activity script :' . $codes ['source'] );
		if (! include ($codes ['source']))
			return false; // Now do whatever you have to do in the activity
		

		// If its a end activity, detach instance from document
		if ($activity->getType () == 'end') {
			$this->docflow->getDocument ()->setProperty ( 'instance_id', NULL ); //...unlink the instance
			$this->error_stack->push ( Rb_Error::INFO, array (), 'unlink instance id: ' . $iid . ' from document: ' . $this->docflow->getDocument ()->getNumber () );
		}
		
		return $this;
	
	} //End of method
	

	//---------------------------
	/* Send a message to next users of next activities
*/
	function sendMessageNextUsers($to = '') {
		
		if (! isset ( $this->message ))
			return false;
		if (! isset ( $this->CnextUsers ))
			return false;
		
		if (is_array ( $to ) && is_array ( $this->CnextUsers )) {
			$toUsers = array_merge ( $to, $this->CnextUsers );
		} else if (! empty ( $to ) && is_array ( $this->CnextUsers )) {
			$toUsers = $this->CnextUsers;
			$toUsers [] = $to;
		} else if (! empty ( $to )) {
			$toUsers = $to;
		} else if (is_array ( $this->CnextUsers )) {
			$toUsers = $this->CnextUsers;
		}
		
		//Send message to next users
		//require_once('core/message.php');
		$message = new Rb_Message ( $this->db );
		$from = $this->user;
		$cc = '';
		$priority = 3;
		
		if (is_array ( $toUsers ))
			foreach ( $toUsers as $to ) {
				$message->postMessage ( $to, $from, $to, $cc, $this->message ['subject'], $this->message ['body'], $priority );
			}
	} //End of method
	

	//---------------------------
	/* Send a message to next users of next activities
*/
	function sendMessage($to, $subject, $body) {
		//if( !isset($this->message) ) return false;
		

		if (is_array ( $to )) {
			$toUsers = $to;
		} else if (! empty ( $to )) {
			$toUsers [] = $to;
		} else {
			return false;
		}
		
		//Send message to next users
		//require_once('core/message.php');
		$message = new Rb_Message ( $this->db );
		$from = $this->user;
		$cc = '';
		$priority = 3;
		
		if (is_array ( $toUsers ))
			foreach ( $toUsers as $to ) {
				$message->postMessage ( $to, $from, $to, $cc, $subject, $body, $priority );
			}
	} //End of method
	

	//---------------------------
	/* Update instance comment after validation of post operations
*/
	function setMessageToNextUsers($subject, $body) {
		if (! empty ( $subject ))
			$this->message ['subject'] = $subject;
		if (! empty ( $body ))
			$this->message ['body'] = $body;
	} //End of method
	

	//---------------------------
	/* Compile activity if necessary
*/
	static public function compileActivity(&$process, &$activity) {
		
		// Get paths for original and compiled activity code
		$origcode = GALAXIA_PROCESSES . '/' . $process->getNormalizedName () . '/code/activities/' . $activity->getNormalizedName () . '.php';
		$compcode = GALAXIA_PROCESSES . '/' . $process->getNormalizedName () . '/compiled/' . $activity->getNormalizedName () . '.php';
		
		// Now get paths for original and compiled template
		$origtmpl = GALAXIA_PROCESSES . '/' . $process->getNormalizedName () . '/code/templates/' . $activity->getNormalizedName () . '.tpl';
		$comptmpl = GALAXIA_TEMPLATES . '/' . $process->getNormalizedName () . '/' . $activity->getNormalizedName () . '.tpl';
		
		$recomplile = false; //var init to choose recompile or not
		//If activity is not interactive, they are not template file
		if ($activity->isInteractive ()) {
			if (filemtime ( $origtmpl ) > filemtime ( $comptmpl ))
				$recomplile = true;
		}
		if ((filemtime ( $origcode ) > filemtime ( $compcode ))) {
			$recomplile = true;
		}
		
		// Check whether the activity code or template are newer than their compiled counterparts,
		// i.e. check if the source code or template were changed; if so, we need to recompile
		if ($recomplile = true) {
			// Recompile the activity
			//include_once (GALAXIA_LIBRARY . '/src/ProcessManager/Galaxia_Manager_Activity.php');
			$activityManager = new Galaxia_Manager_Activity ( Ranchbe::getDb () );
			$activityManager->compile_activity ( $activity->getProcessId (), $activity->getActivityId () );
		}
		
		// Get paths for shared code and activity
		$shared = GALAXIA_PROCESSES . '/' . $process->getNormalizedName () . '/code/shared.php';
		$source = $compcode;
		return array ('shared' => $shared, 'source' => $source );
	
	} //End of function
	

	public function showError($message) {
		$this->error_stack->push ( Rb_Error::ERROR, array (), $message );
		$this->error_stack->checkErrors ();
		return true;
	} //End of method


}//End of class

