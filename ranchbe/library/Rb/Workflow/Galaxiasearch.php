<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require('rb/search/interface.php');

// brief! create params to search in database
class Rb_Workflow_Galaxiasearch implements Rb_Search_Interface{

public $selectClose = ''; /*!< define the select close for query the database*/
public $whereClose  = ''; /*!< define the where close for query the database*/
public $orderClose  = ''; /*!< define the order for sort the result*/
public $limit       = 50; /*!< limit the number of records in the result*/
public $offset      = 0; /*!< defined the pagination page to display*/
public $recordCount; //(Integer)

/*
field use for sort sql option
*/
public $sortBy; //(string)

/*
('ASC' or 'DESC') sort direction sql option
*/
public $sortOrder='ASC'; //(string) ASC or DESC

/*
its a assoc array where key=a field and value=a word to find in the table.
This array is translate to a query option like 'where key=value'
*/
public $exact_find; //(array)

/*
its a assoc array where key=a field and value=a string to find in the table.
This array is translate to a query option like 'WHERE key LIKE %value%'
to find multi value on the same field just separate the word by close OR or AND like this :
nom1 OR nom2 OR nom3 with space on each side of OR
*/
public $find; //(array) 

/*
array(field1,field2,field3,...) its a nonassoc array with all fields to put in the sql select option 
*/
public $select = '*'; //(array or *)

/*
Input special where close. Exemple : $params[where] = array('field1 > 1' , 'field2 = 2')
*/
public $where = array(); //(array) 

/*
any query to add to end
*/
public $extra;//(string)

/*
To define junction
*/
public $with = array();
protected $with_iterator = 0;

public function __construct(){
}

//---------------------------------------------------------------------------
/*! \brief
\param $what (string) a word to find
\param $where (string) field where search
\param $table (string) search in table, default = current object table
\param $at (string) begin, end, anywhere, exact
*/
function setFind( $what, $where, $at='anywhere', $table='' ){
  if( empty($table) ) $table = $this->OBJECT_TABLE;
  
  if($at=='exact') $op = ' = ';
  else $op = ' LIKE ';

  $whats = array();
  $i=0;

  $whats = explode(' ', $what ); //Explode
  //for each word create setFind. If word = OR ignore it and and set a OR close for the next
  for( $i=0; $i < count($whats); $i++ ){
    $whats[$i] = trim($whats[$i]);
    //if word is not = OR
    if( $whats[$i] == 'AND' ) $i++;
    if( empty($whats[$i]) ) $i++;
    if( $whats[$i] != 'OR' ){
      $this->find['AND'][] = $table.'.'.$where.$op.self::setPassThrough( $whats[$i], $at );
    }else if( $whats[$i] == 'OR' ){
      $i++;
      $this->find['OR'][] = $table.'.'.$where.$op.self::setPassThrough( $whats[$i], $at );
    }
  }
}//End of method

//---------------------------------------------------------------------------
/*! \brief
*/
private static function setPassThrough( $what, $at='anywhere' ){
  switch($at){
    case 'anywhere':
      return '\'%'.$what.'%\'';
      break;
    case 'begin':
      return '\''.$what.'%\'';
      break;
    case 'end':
      return '\'%'.$what.'\'';
      break;
    case 'exact':
      return '\''.$what.'\'';
      break;
  }
}//End of method

//---------------------------------------------------------------------------
/*! \brief
*/
function setLimit( $int ){
  $this->limit = $int;
}//End of method

//---------------------------------------------------------------------------
/*! \brief
*/
function setOffset( $int ){
  $this->offset = $int;
}//End of method

//---------------------------------------------------------------------------
/*! \brief
*/
function setOrder( $by , $dir='ASC' ){
  $this->sortBy = $by;
  $this->sortOrder = $dir;
}//End of method

//---------------------------------------------------------------------------
/*! \brief
*/
function setSelect( $mixed ){
  if(is_array($mixed) && is_array($this->select))
    $this->select[] = array_merge( $this->select , $mixed);
  else if(is_array($mixed))
    $this->select = $mixed;
  else
    $this->select[] = $mixed;
}//End of method

//---------------------------------------------------------------------------
/*! \brief
*/
function setWhere( $str ){
  $this->where[] = $str;
}//End of method

//---------------------------------------------------------------------------
/*! \brief Create a join close on query
  \params $type (string) INNER or OUTER
  \params $table (string) table to join
  \params $col (string) field use for ON condition
*/
function setWith( $table, $col, $type='INNER' ){
  $i = count($this->with);
  $i++;
  $this->with[$i]['type'] = $type;
  $this->with[$i]['table'] = $table; //Container
  $this->with[$i]['col'] = $col;
}//End of method

//-------------------------------------------------------------------------
/*! \brief This method decompose the var $params and return the correct synthase for the "where" and "select" for SQL query.
 *   
 *  Exemple of query :
 *  $query = "SELECT $this->selectClose FROM `TABLE`
 *     $this->whereClose
 *     $this->orderClose
 *     ";
 *  Ranchbe::getDb()->SelectLimit( $query , $this->limit , $this->offset);
 *  This method is associated to script "filterManager.php" wich get values from forms    
 *     
 *  return :
 *  $this->selectClose
 *  $this->whereClose
 *  $this->orderClose
 *  $this->limit
 *  $this->offset
 *  $this->from
 */
/*
protected function _getQueryOptions(){

if( isset($this->with[0]) ){
  foreach( $this->with as $with ){
    $array_joinClose[] = $with['type'].' JOIN '. $with['table'] .' ON '. $with['table'].'.'.$with['col'] .'='.$this->OBJECT_TABLE.'.'.$with['col'];
  }
}
if( is_array($array_joinClose) )
  $joinClose = implode(' ', $array_joinClose );

if( is_array ($this->where) ){
  foreach($this->where as $where)
  $whereClose[] = "($where)";
}

if(!empty($this->sortBy)){
  $orderClose = " ORDER BY " . $this->sortBy . " " . $this->sortOrder;
}

if(!empty( $whereClose )){
    $whereClose = 'WHERE ' . implode(' AND ', $whereClose);
}

if ( is_array($this->select) ){
  $selectClose = implode(' , ' , $this->select);
}else $selectClose = $this->select;

if (isset($selectClose))  $this->selectClose = $selectClose;
  else $this->selectClose = NULL;
if (isset($whereClose))   $this->whereClose  = $whereClose;
  else $this->whereClose = NULL;
if (isset($orderClose))   $this->orderClose  = $orderClose;
  else $this->orderClose = NULL;
$this->limit = intval($this->limit);
$this->offset = intval($this->offset);
if (isset($this->OBJECT_TABLE)) $this->from  = $this->OBJECT_TABLE;
if (isset($joinClose))    $this->joinClose   = $joinClose;
if (isset($params['extra'])) $this->extra    = $params['extra'];

}//End of method

//---------------------------------------------------------------------------
/*! \brief Return the options in a array
*/
/*
function getQueryOptionsArray( $params=array() ){
  $this->_getQueryOptions($params);
  return array(
    'selectClose'=>$this->selectClose,
    'whereClose'=>$this->whereClose,
    'orderClose'=>$this->orderClose,
    'limit'=>$this->limit,
    'offset'=>$this->offset,
    'from'=>$this->from,
    'joinClose'=>$this->joinClose,
    'extra'=>$this->extra,
  );
}//End of method
*/
//---------------------------------------------------------------------------
/*! \brief
*/
function getParams(){
  $params['numrows']=$this->limit;
  $params['offset']=$this->offset;
  $params['sort_field']=$this->sortBy;
  $params['sort_order']=$this->sortOrder;
  $params['find']=$this->find;
  $params['select']=$this->select;
  $params['where']=$this->where;
  $params['with']=$this->with;
  return $params;
}//End of method

//---------------------------------------------------------------------------
/*! \brief
*/
function getDao(){
  return $this->_dao;
}//End of method

}//End of class
