<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


/*! \brief This class manage Rb_Workflow_Workflow associate to objects.
*
* Rb_Workflow_Workflow is an agrega of basics objects of the Rb_Workflow_Workflow : process, activity, instance
* is create a coherente system and defined many factory methods to use easly the galaxia engine
* How to use :
* $Rb_Workflow_Workflow = new Rb_Workflow_Workflow(); //First construct object
* now you can init a process or an instance. example :
* $docflow->initInstance($iid); //init an instance
* $docflow->initProcess(); //init the process. the arg is not require, the process id is get from the instance
* 
* if you prefere init an process before :
* $docflow->initProcess();
* $docflow->initInstance($iid);
* $docflow->initProcess(); //recall initProcess to get $pid from instance
* 
*/
class Rb_Workflow_Workflow {
	
	protected $process; //(Galaxia/process Object) default process or current process of document
	protected $instance; //(Galaxia/instance Object) current instance running on document
	protected $dbGalaxia; //(Object) adodb to access galaxia database
	protected $dbranchbe; //(Object) adodb to access ranchbe database
	protected $activities; //(array)

	public $instanceManager; //(Object)
	public $activityManager; //(Object)
	public $roleManager; //(Object)
	public $processManager; //(Object)

	public $error_stack; //(Object)
	public $logger; //(Object)
	public $template; //(Object)
	

	//--------------------------------------------------------------------
	function __construct() {
		$this->dbGalaxia = & Ranchbe::getDb ();
		$this->dbranchbe = & Ranchbe::getDb ();
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief attach process
  *  Return boolean
  * 
  */
	function setProcess(Galaxia_Process &$process) {
		$this->process = & $process;
		return true;
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief attach instance
  *  Return boolean
  * 
  */
	function setInstance(Galaxia_Instance &$instance) {
		$this->instance = & $instance;
		return true;
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief attach process
  *  Return object
  * 
  */
	function initProcess($process_id = 0) {
		if ($process_id == 0) {
			if (is_object ( $this->instance )) //try to get the pid from instance
				$process_id = $this->instance->getProcessId (); //if a process is linked to instance
			else if (is_object ( $this->activity )) //try to get the pid from activity
				$process_id = $this->activity->getProcessId (); //if a process is linked to instance
		}
		if (! isset ( $this->process )) {
			//include_once (GALAXIA_LIBRARY . '/src/API/Galaxia_Process.php');
			$this->process = new Galaxia_Process ( $this->dbGalaxia );
		}
		if ($process_id != 0)
			$this->process->getProcess ( $process_id ); //init instance object
		return $this->process;
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief attach instance
  *  Return object
  * 
  */
	function initInstance($instance_id = 0) {
		if (! isset ( $this->instance )) {
			$this->instance = new Galaxia_Instance ( $this->dbGalaxia );
		}
		if ($instance_id != 0)
			$this->instance->getInstance ( $instance_id );
		return $this->instance;
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief attach activity
  *  Return object Activity or Galaxia_Activity
  * 
  */
	function initActivity($activityId = 0) {
		if ($this->activity) {
			return $this->activity;
		}
		//include_once (GALAXIA_LIBRARY . '/src/API/Galaxia_Activity.php');
		//include_once (GALAXIA_LIBRARY . '/src/API/activities/Activity.php');
		//include_once (GALAXIA_LIBRARY . '/src/API/activities/End.php');
		//include_once (GALAXIA_LIBRARY . '/src/API/activities/Join.php');
		//include_once (GALAXIA_LIBRARY . '/src/API/activities/Split.php');
		//include_once (GALAXIA_LIBRARY . '/src/API/activities/Standalone.php');
		//include_once (GALAXIA_LIBRARY . '/src/API/activities/Start.php');
		//include_once (GALAXIA_LIBRARY . '/src/API/activities/SwitchActivity.php');
		$this->activity = new Galaxia_Activity ( $this->dbGalaxia );
		if ($activityId != 0)
			$this->activity = $this->activity->getActivity ( $activityId );
		return $this->activity;
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief get
  *  Return object
  * 
  */
	function getProcess() {
		return $this->process;
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief get
  *  Return object
  * 
  */
	function getInstance() {
		return $this->instance;
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief get
  *  Return object
  * 
  */
	function getActivity($activityId = 0) {
		return $this->initActivity ( $activityId );
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief get
  *  Return array
  * 
  */
	function getRunningActivity() {
		return $this->instance->RunningActivities;
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief get
  *  Return array
  * 
  */
	function getInstanceInfos() {
		if (! isset ( $this->instance ) )
			return false;
		return array (
				'properties' => &$this->instance->properties,
				'status' => &$this->instance->status,
				'pId' => &$this->instance->pId,
				'instanceId' => &$this->instance->instanceId,
				'owner' => &$this->instance->owner,
				'started' => &$this->instance->started,
				'name' => &$this->instance->name,
				'ended' => &$this->instance->ended,
				'nextActivity' => &$this->instance->nextActivity,
				'nextUser' => &$this->instance->nextUser );
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief get
  *  Return array
  * 
  */
	function getProcessInfos() {
		if (! isset ( $this->process ))
			return false;
		return array (
				'name' => &$this->process->name, 
				'description' => &$this->process->description, 
				'normalized_name' => &$this->process->normalizedName, 
				'version' => &$this->process->version, 
				'pId' => &$this->process->pId );
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief get
  *  Return array with keys : standalone, activities, running
  *  
  * bool @$force , true for reget activities from database
  * 
  */
	function getInstanceActivityInfo($force = false) {
		if ($force == false)
			if (isset ( $this->activities ))
				return $this->activities;
		if (! isset ( $this->instance ))
			return false;
		$this->_initManager ( 'instanceManager' );
		$this->_initManager ( 'activityManager' );
		$standalone = $this->activityManager->list_activities ( $this->process->pId, 0, - 1, 'flowNum_asc', '', "type = 'standalone'" );
		$this->activities ['standalone'] = $standalone ['data'];
		$instance_id = $this->instance->getInstanceId ();
		$process_id = $this->process->pId;
		if ($instance_id == 0 && $process_id != 0) {
			$this->activities ['activities'] = $this->activityManager->get_process_activities ( $process_id );
		} else {
			$this->activities ['activities'] = $this->instanceManager->get_instance_activities ( $instance_id );
			$this->activities ['running'] = $this->instanceManager->get_running_activity ( $this->instance->getInstanceId () );
		}
		return $this->activities;
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief display the activity template
  * 
  */
	function getActivityTemplate($activityId) {
		if ($activityId == 0) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array (), 'activity_id is NULL' );
			return false;
		}
		
		$this->initActivity ( $activityId );
		$this->initProcess ();
		if ($this->activity->isInteractive ()) {
			$this->template = GALAXIA_TEMPLATES . '/' . $this->process->getNormalizedName () . '/' . $this->activity->getNormalizedName () . '.tpl';
			return $this->template;
		} else {
			Ranchbe::getError()->push ( Rb_Error::WARNING, array (), 'activity is not interactive or auto is set to true' );
			return $this->template = false;
		}
	} //End of function
	

	//-------------------------------------------------------------------------
	/*! \brief execute the activity
  * 
  */
	function execute($activityId, $iid, $name) {
		if ($activityId == 0) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array (), 'activity_id is NULL' );
			return false;
		}
		
		Ranchbe::getError()->push ( Rb_Error::INFO, array (), 'Rb_Workflow_Workflow::execute activityId: ' . $activityId . ' instanceId: ' . $iid );
		
		$activity = & $this->initActivity ( $activityId );
		
		// Only run a start activity if none instance is linked to document
		// and if access is = 0 to prevent reassign process on document wich is terminate
		if ($iid && $activity->getType () == 'start') {
			$msg = 'There is already an activity in progress on this item or is terminated';
			Ranchbe::getError()->push ( Rb_Error::INFO, array (), $msg );
			return false;
		}
		
		// A start activity requires a name for the instance
		/*
		if ($activity->getType () == 'start') {
			$_REQUEST ['name'] = $name; //$_REQUEST['name'] is necessary for galaxia scripts
		}
		*/
		
		// Get activity roles and check it
		$act_roles = $activity->getRoles ();
		$user_roles = $activity->getUserRoles ( Rb_User::getCurrentUser ()->getUsername () );
		// Only check roles if this is an interactive activity
		if ($activity->isInteractive ()) {
			if (! count ( array_intersect ( $act_roles, $user_roles ) )) {
				$msg = 'You cant execute this activity';
				Ranchbe::getError()->push ( Rb_Error::ERROR, array (), $msg );
				return false;
			}
		}
		
		if (! $iid) { //if none instance in process, test if process is valid
			if (! $this->getProcess ())
				$this->initProcess ();
			if (! $this->getProcess ()->isValid ()) {
				$msg = 'This process is not valid, you can not execute this activity';
				Ranchbe::getError()->push ( Rb_Error::ERROR, array (), $msg );
				return false;
			}
		}
		$this->instance->getInstance ( $iid );
		// A start activity requires a name for the instance
		if ($activity->getType () == 'start') {
			$this->instance->setName($name);
		}
		
		if (! $this->instance->executeActivity ( $activityId, $auto ))
			return false;
		//if is not a interactive activity complete it
		if (! $activity->isInteractive ()) {
			$this->instance->complete ( $activityId, false, true );
		}
		$this->instance->sendMessageNextUsers ();
		return $this->instance;
	
	} //End of method
	

	//---------------------------
	/* Update instance comment after validation of post operations
  */
	function validateActivityComment($activity_id, $title, $comment) {
		if (! isset ( $this->instance )) { //need instance object...
			Ranchbe::getError()->push ( Rb_Error::ERROR, array (), 'instance is not init' );
			return false;
		}
		Ranchbe::getError()->push ( Rb_Error::INFO, array (), 'comment instance: ' . $this->instance->getInstanceId () . ' activity id: ' . $activity_id );
		
		//$__comments = $instance->get_instance_comments($activity->getActivityId()); //actual comment
		

		$activity = & $this->initActivity ( $activityId );
		
		$instance->replace_instance_comment ( $cid, $activity->getActivityId (), $activity->getName (), Rb_User::getCurrentUser ()->getUsername (), $title, $comment );
		
		//Send comments to next user
		$CnextUsers = explode ( '%3B', $_REQUEST ['CnextUsers'] );
		$from = $user;
		$cc = '';
		$subject = $title;
		$body = 'Galaxia_Process :' . $process->getVersion () . '<br>' . 'Activity :' . $activity->getName () . '<br>' . 'Subject :' . $title . '<br>' . 'Comment :' . $comment . '<br>';
		$priority = 3;
		
		return true;
	
	} //End of function
	

	//---------------------------
	/* reset the process
  * 
  * \param $instanceId (int) optionnel. if omit try to get instance_id from current instance
  */
	public function resetProcess($instanceId = 0) {
		if (isset ( $this->instance ) && $instanceId == 0) { //need instance object...
			$instanceId = $this->instance->getInstanceId ();
		}
		if ($instanceId == 0) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array (), 'instance is not init' );
			return false;
		}
		
		Ranchbe::getError()->push ( Rb_Error::DEBUG, array (), 'resetProces : instance id: ' . $this->instance->getInstanceId () );
		
		$this->_initManager ( 'instanceManager' );
		$this->instanceManager->set_instance_status ( $instanceId, 'aborted' );
		return true;
	} //End of function
	

	//-------------------------------------------------------------------------
	/*! \brief factory method to get manager objects : 
  *  
  * bool @$force , true for reget activities from database
  * 
  */
	protected function _initManager($manager) {
		switch ($manager) {
			case 'instanceManager' :
				if (! isset ( $this->instanceManager )) {
					//include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Instance.php');
					$this->instanceManager = new Galaxia_Manager_Instance ( $this->dbGalaxia );
				}
				break;
			
			case 'activityManager' :
				if (! isset ( $this->activityManager )) {
					//include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Activity.php');
					$this->activityManager = new Galaxia_Manager_Activity ( $this->dbGalaxia );
				}
				break;
			
			case 'roleManager' :
				if (! isset ( $this->roleManager )) {
					//include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Role.php');
					$this->roleManager = new Galaxia_Manager_Role ( $this->dbGalaxia );
				}
				break;
			
			case 'processManager' :
				if (! isset ( $this->processManager )) {
					//include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/ProcessManager.php');
					$this->processManager = new Galaxia_Manager_Process ( $this->dbGalaxia );
				}
				break;
			
			default :
				return false;
				break;
		
		}
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief notify user about error
  *  
  */
	public function errorNotify($message) {
		Ranchbe::getError()->push ( Rb_Error::ERROR, array (), $message );
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief add an activity comment
  */
	public function setComment($title, $comment) {
		if (! $this->getActivity ()->getActivityId ())
			return false;
		return $this->getInstance ()->replace_instance_comment ( null, $this->getActivity ()->getActivityId (), $this->getActivity ()->getName (), Rb_User::getCurrentUser ()->getName (), $title, $comment );
	} //End of method


}//End of class
