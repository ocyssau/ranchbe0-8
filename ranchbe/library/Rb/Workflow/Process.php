<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


/*! \brief This class manage the Process.
This object complement the galaxia class.
Process is a set of activities and transitions from the workflow.
*/
class Rb_Workflow_Process extends Rb_Object_Permanent {
	
	protected $OBJECT_TABLE = 'galaxia_processes';
	protected $FIELDS_MAP_ID = 'pId';
	protected $FIELDS_MAP_NUM = 'name';
	protected $FIELDS_MAP_NAME = 'name';
	protected $core_props = array (); //(Array) properties of current object
	
	protected static $_registry = array (); //(array) registry of instances

	public function __construct($process_id = NULL) {
		$this->_init ( $process_id );
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief init the properties of the project
  *  Return this
  * 
  * \param $project_id(integer) Id of the project
  */
	protected function _init($id) {
		$this->_dao = new Rb_Dao ( Ranchbe::getDb () );
		$this->_dao->setTable ( $this->OBJECT_TABLE, 'object' );
		$this->_dao->setKey ( $this->FIELDS_MAP_ID, 'primary_key' );
		$this->_dao->setKey ( $this->FIELDS_MAP_NUM, 'number' );
		$this->_dao->setKey ( $this->FIELDS_MAP_NAME, 'name' );
		return parent::_init ( $id );
	} //End of method
	

	//-------------------------------------------------------------------------
	/*! \brief init and clean current registry
  */
	public static function initRegistry() {
		self::$_registry = array ();
	}
	
	//-------------------------------------------------------------------------
	/*! \brief return a instance of object.
  *   if none parameter return a special instance with no possibilies to change here properties
  *   and wich can not be saved. This instance is just for use in static method where access to database is require,
  *   because in this case the access to table parameter like no static property $this->OBJECT_TABLE must be set.
  *   If object "$id" dont exist return false.
  *   If object "$id" has already been instantiated, then return this instance (singleton pattern).
  * 
  */
	static function get($id = -1) {
		if ($id < 0)
			$id = - 1; //forced to value -1
		if ($id == 0)
			return new self ( 0 );
		if (! self::$_registry [$id]) {
			self::$_registry [$id] = new self ( $id );
		}
		return self::$_registry [$id];
	} //End of method
	

	//----------------------------------------------------------
	/*!\brief Get list of all Process.
   * return a array if success, else return false.
   * 
    \param $params(array) is use for manage the display. See parameters function of getQueryOptions()
  */
	public function getAll($params) {
		return $this->getAllBasic ( $params );
	} //End of method
	

	//----------------------------------------------------------
	/*!\brief Get info about a single Process.
   * return a array if success, else return false.
   * 
    \param $process_id(integer) id of the Process.
  */
	public function getInfos($process_id) {
		$params = array ('exact_find' => array ($this->FIELDS_MAP_ID => $process_id ) );
		return $this->getAllBasic ( $params );
	} //End of method
	

	//----------------------------------------------------------
	/*!\brief
   * 
  */
	public function getProperty($name, $value) {
		$this->core_props [$name] = $value;
	} //End of method


}//End of class
