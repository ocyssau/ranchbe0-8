<?php

class Rb_Test implements Rb_Datatype_Interface{
protected $_data; //(object) son object file, cadds, camu... linked
protected $_dataType; //(string) type of the Fsdata: file, cadds, camu...
protected $_dataPath; //(string) Full path to data
protected $_versionId; //(integer) version of current Fsdata

//----------------------------------------------------------
function __construct($dataPath){
  $this->init($dataPath);
}//End of method

//-------------------------------------------------------------------------
/*! \brief init the Fsdata.
*  Try to init the data.
*  If no data on filesystem, set $this->_dataType to false.
*  Return bool.
* 
* \param $dataPath(string) full datapath to data
*/
function init($dataPath){
  $this->_dataPath = $dataPath;
  $this->_data = self::_dataFactory($dataPath);
  if(!$this->_data){
    Ranchbe::getError()->push(Rb_Error::WARNING, array( 'file'=>$dataPath ),'Fsdata %file% is not on filesystem');
    return $this->_dataType = false;
  }else{
    $this->_dataType =& $this->_data->getProperty('file_type');
    return true;
  }
}//End of method

//--------------------------------------------------------
  /*!\brief return true if data is realy on file system
  */
function isExisting(){
  if ( !empty($this->_dataPath) ){
    if ( $this->_dataType !== false )
      return true;
  }
  return false;
}//End of method

//----------------------------------------------------------
/* Set type of a data. Return false or a array with "type" and "extension"*/
static function setDataType($dataPath){
  if(is_dir($dataPath)){
    //Test for adraw from datatype file
    if(is_file($dataPath.'/ranchbe.adraw')){
      if(is_file($dataPath.'/_fd')){
        return array('type'=>'adrawc5' , 'extension'=>'_fd');
      }
      if(is_file($dataPath.'/_pd')){
        return array('type'=>'adrawc4' , 'extension'=>'_pd');
      }
    }
    //Tests for cadds5 parts and adraw
    if(is_file($dataPath.'/_fd')){
      if(is_file($dataPath.'/../_db' )){//If parent dir is a camu, this part is a adraw
        touch ($dataPath.'/ranchbe.adraw');
        return array('type'=>'adrawc5' , 'extension'=>'_fd');
      }else{
        return array('type'=>'cadds5' , 'extension'=>'_fd');
      }
    }
    else if(is_file($dataPath.'/_pd')){ //Tests for cadds4 parts and adraw
      if(is_file($dataPath.'/../_db')){ //If parent dir is a camu, this part is a adraw
        touch ($dataPath.'/ranchbe.adraw');
        return array('type'=>'adrawc4' , 'extension'=>'_pd');
      }else{
        return array('type'=>'cadds4' , 'extension'=>'_pd');
      }
    }
    else if (is_file("$dataPath/_db"))//Tests for camu
      return array('type'=>'camu' , 'extension'=>'_db');
    else if (is_file("$dataPath/_ps"))//Tests for ps tree
      return array('type'=>'pstree' , 'extension'=>'_ps');
    else return false;

  }else if(is_file($dataPath)){
    $extension = substr($dataPath, strrpos($dataPath, '.'));
    switch($extension){
      case '.adrawc5':
        return array('type'=>'zipadrawc5' , 'extension'=>'.adrawc5');
        break;
      case '.adrawc4':
        return array('type'=>'zipadrawc4' , 'extension'=>'.adrawc4');
        break;
      case '.adraw':
        return array('type'=>'zipadraw' , 'extension'=>'.adraw');
        break;
      case '.z':
        return array('type'=>'archive' , 'extension'=>'.z');
      case '.zip':
        return array('type'=>'archive' , 'extension'=>'.zip');
      case '.Z':
        return array('type'=>'archive' , 'extension'=>'.Z');
        break;
      default:
        return array('type'=>'file' , 'extension'=>$extension);
        break;
    }
  }else{ //if data is not on filesystem or is other thing that file or dir
    $extension = substr($dataPath, strrpos($dataPath, '.'));
    if ($extension == '.adraw')
      return array('type'=>'adrawc4' , 'extension'=>'.adraw');
  }
  Ranchbe::getError()->push(Rb_Error::WARNING, array( 'path'=>$dataPath ), 
                        'unknow dataType or unreachable data for path : %path%');
  return false;
}//End of function

//------------------------------------------------------------------------------
static function _dataFactory($dataPath, $test_mode=false){
  $dataType = self::setDataType($dataPath);
  Ranchbe::getError()->push(Rb_Error::INFO, array('dataPath'=>$dataPath,
                                                    'type'=>$dataType['type']),
                            'create Fsdata type %type% from path : %dataPath%');
  switch($dataType['type']){
    case 'cadds5':
      return new Rb_Datatype_Cadds5($dataPath);
      break;
    case 'cadds4':
      return new Rb_Datatype_Cadds4($dataPath);
      break;
    /*
    case 'adrawc4':
      //require_once('core/datatype/adrawc4.php');
      return new Rb_Datatype_Adrawc4($dataPath);
      break;
    case 'adrawc5':
      //require_once('core/datatype/adrawc5.php');
      return new Rb_Datatype_Adrawc5($dataPath);
      break;
    case 'zipadrawc4':
      //require_once('core/datatype/zipadraw.php');
      return new Rb_Datatype_Zipadraw($dataPath);
      break;
    case 'zipadrawc5':
      //require_once('core/datatype/zipadraw.php');
      return new Rb_Datatype_Zipadraw($dataPath);
      break;
    */
    case 'adraw':
    case 'adrawc4':
    case 'adrawc5':
      return new Rb_Datatype_Adraw($dataPath);
      break;
    case 'zipadraw':
    case 'zipadrawc4':
    case 'zipadrawc5':
      return new Rb_Datatype_Zipadraw($dataPath);
      break;
    case 'camu':
      return new Rb_Datatype_Camu($dataPath);
      break;
    case 'pstree':
      return new Rb_Datatype_Pstree($dataPath);
      break;
    case 'package':
    case 'archive':
      return new Rb_Datatype_Package($dataPath);
      break;
    case 'file':
      return new Rb_File($dataPath);
      break;
    default:
      return false;
    break;
  }
}//End of function

//--------------------------------------------------------
/*!\brief
*   
*/
function getData(){
  if( isset($this->_data) ) return $this->_data;
  else return false;
}//End of method

//-------------------------------------------------------
function getProperty($property_name){
  if($property_name === 'dataType'){
    return $this->_dataType;
  }
  if($property_name === 'file_iteration'){
    return $this->_versionId;
  }
  if(is_object($this->_data)){
    return $this->_data->getProperty($property_name);
  }
  if($property_name === 'file'){
    return $this->_dataPath;
  }
  if($property_name === 'file_name'){
    return basename($this->_dataPath);
  }
  if($property_name === 'file_path'){
    return dirname($this->_dataPath);
  }
  Ranchbe::getError()->push(Rb_Error::WARNING, array( 'propname'=>$property_name ),
                                   'call of undefined property %propname%');
  return 'undefined';
}//End of method

//-------------------------------------------------------
/*! \brief Get infos about data
* Return a array if no errors, else return FALSE.
*
* \param $displayMd5(bool) true if you want return the md5 code of the file.
*/
function getProperties($displayMd5=true){
  if(is_object($this->_data)){
    $infos = $this->_data->getInfos($displayMd5);
  }else{
    $infos = $this->fsdata_props;
  }
  $infos['dataType'] = $this->_dataType;
  $infos['file_iteration'] = $this->_versionId;
  return $infos;
}//End of method

//-------------------------------------------------------
/*! \brief Alias for getProperties
*/
function getInfos($displayMd5=true){
  return $this->getProperties($displayMd5);
}//End of method

//-------------------------------------------------------
/*! \brief Return file extension.(ie: /dir/file.ext, return '.ext')
*
* \param $file(string) File name. 
*/
function getExtension(){
  if( !$this->_data ) return false;
  return $this->_data->getExtension();
}//End of method

//-------------------------------------------------------
/*! \brief Return root name of file.(ie: /dir/file.ext, return 'file')
*
* \param $file(string) File name. 
*/
function getRoot(){
  if( !$this->_data ) return false;
  return $this->_data->getRoot();
}//End of method

//----------------------------------------------------------
/*! \brief move the current file
* 
* \param $dst(string) fullpath to new file
* \param $replace(bool) true for replace file
*/
function move($dst , $replace=false){
  if( !is_object($this->_data) ) return false;
  return $this->_data->move($dst , $replace);
}//End of method

//----------------------------------------------------------
/*! \brief copy the current file
* 
* \param $dst(string) fullpath to new file
* \param $mode(integer) mode of the new file
* \param $replace(bool) true for replace existing file
*/
function copy($dst,$mode=0755,$replace=true){
  if( !$this->_data ) return false;
  return $this->_data->copy($dst,$mode,$replace);
}//End of method

//----------------------------------------------------------
/*! \brief suppress the current attachment file
* 
* \param $dst(string) fullpath to new file
*/
function suppress(){
  if( !$this->_data ) return false;
  return $this->_data->suppress();
}//End of method

//-------------------------------------------------------------------------------
/*! \brief put file in the trash dir
*   Returns TRUE or FALSE
*/
function putInTrash($verbose = false, $trashDir=DEFAULT_TRASH_DIR){
  if( !$this->_data ) return false;
  return $this->_data->putInTrash($verbose, $trashDir);
}//End of method

//-------------------------------------------------------
/*! \brief Put the file in the wildspace.
    Return true or false.
*
  \param $addPrefix(String) string to add before filename.
* The file is put with a default prefix "consult__"  to prevent lost of data.
*/
function putInWildspace($addPrefix=NULL, $replace=false){
   $dstfile = Rb_User::getCurrentUser()->getWildspace()->getPath().'/'
                                    .$addPrefix.$this->getProperty('file_name');
   if(!$this->copy($dstfile , 0775, $replace)){
    Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$this->file),
                                                  'can\'t copy file %element%');
    return false;
   }
}//End of method

//-------------------------------------------------------------------------------
/*! \brief Send the file to navigator
*/
function downloadFile(){
  if( !$this->_data ) return false;
  if( $this->_data->downloadFile() ) die;
}//End of method

}//End of class

