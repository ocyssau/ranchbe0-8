<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//require_once('core/object/relation.php');

/** This is a link between doctype, objects categories.
 *  A doctype can be automaticly affected to a Category if a relation is set
 *
 **/
class Rb_Doctype_Relation_Category extends Rb_Object_Relation{

  //-------------------------------------------------------------------------
  public static function get($id = -1){
    if($id < 0) $id = -1; //forced to value -1
    if($id == 0)
      return new self(0);
    if( !Rb_Object_Relation::$_registry[__CLASS__][$id] ){
      Rb_Object_Relation::$_registry[__CLASS__][$id] = new self($id);
    }
    return Rb_Object_Relation::$_registry[__CLASS__][$id];
  }//End of method

  //----------------------------------------------------------
  /*!\brief Get categories linked to doctype $id
  */
  public function getCategories( $id, $params=array() ){
    //Join objects dictionary table as child
    $params['with'][] = array(
                          'type'=>'RIGHT',
                          'table'=> 'categories',
                          'col1'=> 'child_id',
                          'col2'=> 'category_id',
                        );

    //Join objects table as child
    $params['with'][] = array(
                          'type'=>'RIGHT',
                          'table'=> 'objects',
                          'col1'=> 'child_id',
                          'col2'=> 'object_id',
                        );

    //Select objects of class 'rb_category'
    $params['exact_find']['objects.class_id'] = 100;
    $params['exact_find']['objects_rel.parent_id'] = $id;

    //Select only childs of objects class 'rb_dcotype'
    //$params['where'][] = 'parent_id IN (SELECT object_id FROM objects WHERE class_id = 110)';

    return $this->_dao->getAllBasic($params);
  }//End of method

} //End of class
