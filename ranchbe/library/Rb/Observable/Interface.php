<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/**
 * Observable interface
 *
 * @author Olivier CYSSAU
 *
 */
interface Rb_Observable_Interface{

	/**
	 * This method can be used to attach an object to the class listening for
	 * some specific event. The object will be notified when the specified
	 * event is triggered by the derived class.
	 */
	function attach($event, Rb_Observer_Interface &$obj);

	/**
	 * Attaches an object to the class listening for any event.
	 * The object will be notified when any event occurs in the derived class.
	 */
	function attach_all(Rb_Observer_Interface &$obj);

	/**
	 * Detaches an observer from the class.
	 */
	function dettach(Rb_Observer_Interface &$obj);

} //End of class

