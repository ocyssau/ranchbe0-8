<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('PEAR/ErrorStack.php');

/*! \brief Class for manage errors occuring in ranchbe objects.
 *
 * This class extends the PEAR_ErrorStack class. PEAR_ErrorStack is the next generation
 * of error manager for PHP and replace the PEAR_error package.
 * See http://www.go-pear.org/manual/fr/core.pear.pear-errorstack.intro.php.<br>
 *
 * Errors are push in the stack by method error_stack->push(CODE(string), LEVEL(string), PARAMS(array), MSG(string) );<br>
 * The CODE = ERROR for an common error, ERROR_DB for an database error.<br>
 * LEVEL = Fatal, Warning, Info, Debug, Log<br>
 * PARAM = is a array with optionnals keys<ul>
 *    <li>element : for use in MSG</li>
 *    <li>query : is the content of the query. To use for debug</li>
 *    <li>debug : to records var content for debug</li></ul>
 * MSG is the message to display to user. If a key element is record in PARAM, the text %element% will be replaced by his value record in param array.<br>
 *
 * For control display to user the constant DEBUG of the ranchbe_setup.php file is set to true for
 * display all content of the error.
 * If set to false, display only the MSG.
 */
class Rb_ErrorManager extends PEAR_ErrorStack {

	/** Check if errors occured and display user message.
	 *
	 * @param $params('close_button','back_button','home_button')
	 */
	public function checkErrors( $params=array() ){
		if ($this->hasErrors()){
			Ranchbe::getSmarty()->assign('debug', DEBUG);
			Ranchbe::getSmarty()->assign('errors', $this->getErrors());
			if( isset($params['close_button']) )
			Ranchbe::getSmarty()->assign('close_button', $params['close_button']);
			if( isset($params['back_button']) )
			Ranchbe::getSmarty()->assign('back_button', $params['back_button']);
			if( isset($params['home_button']) )
			Ranchbe::getSmarty()->assign('home_button', $params['home_button']);
			$path = & Ranchbe::getConfig ()->resources->smarty->scriptPath;
			Ranchbe::getSmarty()->display($path.'error_stack.tpl');
		}
	}//End of method

	/** Overload of ErrorStack::_log method
	 *
	 */
	function _log($err)
	{
		if (!is_a($this->_logger, 'Zend_Log')){
			return;
		}
		/*
		 EMERG   = 0;  // Urgence : le syst�me est inutilisable
		 ALERT   = 1;  // Alerte: une mesure corrective doit �tre prise imm�diatement
		 CRIT    = 2;  // Critique : �tats critiques
		 ERR     = 3;  // Erreur: �tats d'erreur
		 WARN    = 4;  // Avertissement: �tats d'avertissement
		 NOTICE  = 5;  // Notice: normal mais �tat significatif
		 INFO    = 6;  // Information: messages d'informations
		 DEBUG   = 7;  // Debug: messages de d�boguages
		 */

		$levels = array(
          'alert' => Zend_Log::ALERT, //1
          'exception' => Zend_Log::CRIT, //2
          'critical' => Zend_Log::CRIT, //2
          'error' => Zend_Log::ERR, //3
          'fatal' => Zend_Log::ERR, //3
          'warning' => Zend_Log::WARN, //4
          'notice' => Zend_Log::NOTICE, //5
          'info' => Zend_Log::INFO, //6
          'debug' => Zend_Log::DEBUG //7
		);
		$err['level'] = strtolower($err['level']);
		if ( !empty($err['level']) ) {
			$level = $levels[$err['level']];
		} else {
			$level = Zend_Log::INFO;
		}
		if( $level > Ranchbe::getConfig()->resources->ranchbe->log->level){
			return;
		}else{
			$message = '[line: '.$err['context']['line']
						.'] [function: '.$err['context']['function']
						.'] [class: '.$err['context']['class'].'] '
						.$err['message'];
			$this->_logger->log($message, $level);
		}
	}//End of method

	/** Overload of ErrorStack::push
	 * This function can not be overload, else contexte report failed
	 */
	/*
	public function push($code, $level = 'error', $params = array(), $msg = false,
		$repackage = false, $backtrace = false){
		return parent::push($code, $level, $params, $msg, $repackage, $backtrace);
	}
	*/
	

	/** Flush the current stack in log file and clean up the message stack
	 *
	 */
	public function flush()
	{
		$errors = $this->getErrors(true);
		if (is_a($this->_logger, 'Rb_Log')) {
			foreach($errors as $error){
				$this->_log($error);
			}
		}
	}

}//End of class
