<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
 CREATE TABLE `%container%_documents` (
 `document_id` int(11) NOT NULL,
 `document_bid` CHAR(15) NOT NULL,
 `document_number` varchar(128)  NOT NULL,
 `document_state` varchar(32)  NOT NULL default 'init',
 `document_access_code` int(11) NOT NULL default 0,
 `document_iteration` int(11) NOT NULL default 1,
 `document_version` int(11) NOT NULL default 1,
 `document_life_stage` int(1) NOT NULL default 1,
 `from_document` int(11) default NULL,
 `%container%_id` int(11) NOT NULL,
 `default_process_id` int(11) default NULL,
 `instance_id` int(11) default NULL,
 `doctype_id` int(11) NOT NULL,
 `category_id` int(11) default NULL,
 `check_out_by` int(11) default NULL,
 `check_out_date` int(11) default NULL,
 `description` varchar(128)  default NULL,
 `update_date` int(11) default NULL,
 `update_by` decimal(10,0) default NULL,
 `open_date` int(11) default NULL,
 `open_by` int(11) default NULL,
 `owned_by` int(11) default NULL,
 PRIMARY KEY  (`document_id`),
 UNIQUE KEY `UNIQ_%container%_documents_1` (`document_number`,`document_version`, `document_iteration`),
 UNIQUE KEY `UNIQ_%container%_documents_2` (`document_bid`,`document_version`, `document_iteration`),
 KEY `INDEX_%container%_documents_1` (`document_number`),
 KEY `INDEX_%container%_documents_2` (`document_version`),
 KEY `INDEX_%container%_documents_3` (`document_iteration`),
 KEY `INDEX_%container%_documents_4` (`doctype_id`),
 KEY `INDEX_%container%_documents_5` (`%container%_id`),
 KEY `INDEX_%container%_documents_6` (`category_id`),
 KEY `INDEX_%container%_documents_7` (`document_state`),
 KEY `INDEX_%container%_documents_8` (`description`),
 KEY `INDEX_%container%_documents_9` (`from_document`),
 KEY `INDEX_%container%_documents_10` (`check_out_by`),
 KEY `INDEX_%container%_documents_11` (`update_by`),
 KEY `INDEX_%container%_documents_12` (`open_by`),
 KEY `INDEX_%container%_documents_13` (`document_bid`)
 ) ENGINE=InnoDB ;

 ALTER TABLE `%container%_documents`
 ADD CONSTRAINT `FK_%container%_documents_2` FOREIGN KEY (`doctype_id`)
 REFERENCES `doctypes` (`doctype_id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_%container%_documents_3` FOREIGN KEY (`document_version`)
 REFERENCES `indices` (`indice_id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_%container%_documents_4` FOREIGN KEY (`%container%_id`)
 REFERENCES `%container%s` (`%container%_id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_%container%_documents_5` FOREIGN KEY (`category_id`)
 REFERENCES `%container%_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_%container%_documents_6` FOREIGN KEY (`instance_id`)
 REFERENCES `galaxia_instances` (`instanceId`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_%container%_documents_7` FOREIGN KEY (`default_process_id`)
 REFERENCES `galaxia_processes` (`pId`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_%container%_documents_8` FOREIGN KEY (`update_by`)
 REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_%container%_documents_9` FOREIGN KEY (`update_by`)
 REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_%container%_documents_10` FOREIGN KEY (`owned_by`)
 REFERENCES `rb_users` (`auth_user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

 CREATE TABLE `%container%_documents_history` (
 `histo_order` int(11) NOT NULL default '0',
 `action_name` varchar(32) NOT NULL default '',
 `action_by` varchar(64) NOT NULL default '',
 `action_date` int(11) NOT NULL default '0',
 `comment` varchar(255) default NULL,
 `document_id` int(11) NOT NULL,
 `document_bid` CHAR(15) NOT NULL,
 `document_number` varchar(128)  NOT NULL,
 PRIMARY KEY  (`histo_order`),
 KEY `INDEX_%container%_documents_history_1` (`action_name`)
 ) ENGINE=InnoDB;

 --
 -- Contraintes pour la table `%container%_documents_history`
 --
 ALTER TABLE `%container%_documents_history`
 ADD CONSTRAINT `FK_%container%_documents_history_1` FOREIGN KEY (`document_id`)
 REFERENCES `%container%_documents` (`document_id`) ON DELETE CASCADE;
 */

//-------------------------------------------------------------------------
/** This class manage documents.
 *
 * A Document is component of ranchbe wich is manage
 *   by a container type "document_manager". It just a database record with
 *   relations to
 *       - 0,n Rb_Docfile,
 *       - 1,1 Rb_Container,
 *   and other objects.
 *   A Document may have many docfiles and have basic properties and extends properties.
 *   Basic properties are defined in table _documents and are directly accessible
 *   by this class between Rb_Dao object ($this->_dao). Extends properties are
 *   stored in _document_extends table and are accessible by the class Rb_Object_Extend
 *   which implement decorator pattern. Basic properties are defined by ranchbe,
 *   and extends properties are defined by users.
 *     I.E set a basic property:
 *       $this->setProperty('name', 'value');
 *     I.E set a extend property:
 *       $extend = new Rb_Object_Extend($this);
 *       $extend->setProperty('extend_property', 'value');
 *       or you can directly call property
 *       $extend->extend_property = 'value';
 *   A container is a "document_manager" if his property "file_only"=false.
 *   The Document has 0 to n docfiles.
 *   The parent of a Document is a Rb_Container
 *   
 * This class define the followings events
 * doc_pre_create
 * doc_post_create
 * doc_pre_update
 * doc_post_update
 * doc_pre_store
 * doc_post_store
 * doc_pre_associateFile
 * doc_post_associateFile
 * doc_pre_checkout
 * doc_post_checkout
 * doc_pre_reset
 * doc_post_reset
 * doc_pre_checkinAndRelease
 * doc_post_checkinAndRelease
 * doc_pre_checkinAndKeep
 * doc_post_checkinAndKeep
 * doc_pre_move
 * doc_post_move
 * doc_pre_copy
 * doc_post_copy
 * doc_pre_createVersion
 * doc_post_createVersion
 * doc_pre_suppress
 * doc_post_suppress
 * doc_pre_view
 *   
 *   
 */
class Rb_Document extends Rb_Object_Acl implements Rb_Object_Interface_Related{

	//DATABASE SETTING FOR USE BY CLASS basic
	protected $OBJECT_TABLE; //(String)Table where are stored the containers definitions

	//Fields:
	protected $FIELDS_MAP_ID = 'document_id'; //(String)Name of the field of the primary key
	protected $FIELDS_MAP_NUM = 'document_number'; //(String)Name of the field to define the number
	protected $FIELDS_MAP_NAME = 'document_name'; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC = 'description'; //(String)Name of the field where is store the description
	protected $FIELDS_MAP_STATE = 'document_state'; //(String)Name of the field where is store the state
	protected $FIELDS_MAP_VERSION = 'document_version'; //(String)Name of the field where is store the indices
	protected $FIELDS_MAP_ITERATION = 'document_iteration'; //(String)Name of the field where is store the indices
	protected $FIELDS_MAP_FATHER; //(String)Name of the field where is store the id of the father

	//CURRENT INSTANCE PROPERTIES
	protected $docfiles = array('noAssoc'=>array(), //not associative list of docfiles objects
                                'byRole'=>array(), //list of docfiles object where key = role of docfile
	); //(Array) array of objects of the docfiles of the current Document
	protected $container  = false; //(Object) object of the container of the current Document
	protected $metadata   = false; //(Object) Object of the metadata linked to the current Document
	protected $metadatas  = array(); //(Array) definition of extends metadatas of the Document
	protected $doctype    = false; //(Object) Object of the doctype of current Document
	protected $category   = false; //(Object) Object of the category of current Document
	protected $indice     = false; //(Object) Object

	protected $test_mode = false; //(Bool) if true, test write operations only

	protected $_preCreateEventName  = 'doc_pre_create';
	protected $_postCreateEventName = 'doc_post_create';
	protected $_preUpdateEventName  = 'doc_pre_update';
	protected $_postUpdateEventName = 'doc_post_update';

	protected $_privileges = array(
                                'admin', //links, history, reset process
                                'admin_users', //permissions
                                'document_get',
                                'document_create',
                                'document_edit', //checkin,checkout,assocfile, esit properties, mark to suppress
                                'document_suppress',
                                'document_unlock',
                                'document_edit_number', //change number of Document or not
	); //(array)default privileges name

	protected $_lockCode = array(
					0=>'Unlocked',
					1=>'Checkout',
					5=>'InWorkflow',
					11=>'Locked',
					12=>'MarkedToSuppress',
					15=>'MarkedForHistory'
					);

	protected static $_registry = array(); //(array) registry of instances

	//-------------------------------------------------------------------------
	/**
	 * @param Rb_Space $space
	 * @param Integer $document_id
	 * @return unknown_type
	 */
	public function __construct(Rb_Space &$space, $document_id = 0){
		$this->space =& $space;
		$this->_space_id = $space->getId();
		$this->FIELDS_MAP_FATHER = $space->getName().'_id';
		return $this->_init($document_id);
	}//End of method
	
	
	//-------------------------------------------------------------------------
	/**
	 */
	public function __wakeup(){
		$this->_dao->reconnect( Ranchbe::getDb() );
	}//End of method
	
	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Acl#_init($id)
	 */
	protected function _init($document_id){
		$this->_dao = new Rb_Dao( Ranchbe::getDb() );
		$this->_dao->setTable($this->space->DOC_TABLE, 'object');
		$this->_dao->setKey($this->FIELDS_MAP_ID, 'primary_key');
		$this->_dao->setKey($this->FIELDS_MAP_NUM, 'number');
		$this->_dao->setKey($this->FIELDS_MAP_NAME, 'name');
		$this->_dao->setKey($this->FIELDS_MAP_DESC, 'description');
		$this->_dao->setKey($this->FIELDS_MAP_STATE, 'state');
		$this->_dao->setKey($this->FIELDS_MAP_VERSION, 'version');
		$this->_dao->setKey($this->FIELDS_MAP_ITERATION, 'iteration');
		$this->_dao->setKey($this->space->DOC_FIELDS_MAP_FATHER, 'father');
		return Rb_Object_Acl::_init($document_id);
	}//End of method
	
	//--------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Rb_Observable#_initObservers()
	 */
	protected function _initObservers(){
		if( $this->_observerIsInit ) return;
		//Attach doctype scripts pipe
		$this->attach_all(new Rb_Observer_Doctypescript());
		//Attach container scripts pipe
		$this->attach_all(new Rb_Observer_Containerscript());
		//Attach notification scripts pipe
		$this->attach_all(new Rb_Observer_Notification());
		//Attach thumbnail generator
		$this->attach_all(new Rb_Observer_Thumbgen());
		$this->_observerIsInit = true;
	}//End of method
	
	//--------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_create()
	 */
	protected function _create(){
		if( $this->_id !== 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
						tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}

		if( !$this->getProperty('document_iteration') )
		$this->setProperty('document_iteration', 1);
		if( !$this->getProperty('document_access_code') )
		$this->setProperty('document_access_code', 0);
		if( !is_numeric( $this->getProperty('document_version') ) )
		$this->setProperty('document_version', 1);

		//Set current time
		$now = time();
		$this->setProperty('open_date', $now);
		$this->setProperty('update_date', $now);
		//Set current users
		$this->setProperty('open_by', Rb_User::getCurrentUser()->getId() );
		$this->setProperty('update_by', Rb_User::getCurrentUser()->getId() );
		$this->setProperty('owned_by', Rb_User::getCurrentUser()->getId() );

		if(!$this->getProperty('document_bid'))
		$this->setProperty('document_bid', uniqId() );
		if(!$this->getProperty('document_life_stage'))
		$this->setProperty('document_life_stage', 1 );

		//Check that Document_number is set
		if( !$this->getNumber() ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(),
			tra('Document number is not set') );
			return false;
		}

		//Check that Document_number is not existing
		if( $id = $this->ifExist( $this->getNumber(), $this->getProperty('document_version') ) ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(),
			tra('Document number existing yet (id=') . $id .')');
			return false;
		}

		Ranchbe::getError()->info('create Document from Document number : '.$this->getNumber() );

		//Set the doctype of the current Document
		//if( !$this->doctype ){
		if( $this->getDoctype()->getId() < 1 ){
			if( $docfile =& $this->getDocfile('main') ){ //Get main docfile of the Document
				$file_extension = $docfile->getProperty('file_extension'); //Get the property of the main docfile
				$file_type = $docfile->getProperty('file_type'); //Get the property of the main docfile
			}else{
				$file_extension = NULL;
				$file_type = 'nofile';
			}
			$this->setDocType($file_extension, $file_type);
		}

		if( $this->getDoctype()->getId() < 1 ) return false;
		else $this->setProperty( 'doctype_id', $this->doctype->getId() );

		//Create a basic object
		if( $this->_id = $this->_dao->create( $this->getProperties() ) ){
			self::$_registry[$this->space->getName()][$this->_id] =& $this;
			$this->isSaved = true;
		}else{
			Ranchbe::getError()->push(Rb_Error::ERROR, array( 'element'=>$this->getNumber() ),
			tra('cant create the Document %element%') );
			return $this->_id = 0;
		}

		return $this->_id;

	}//End of method

	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#_update()
	 */
	protected function _update(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id), tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}
		if(!$this->_dao->update( $this->getProperties() , $this->_id)) return false;
		$this->isSaved = true;
		return true;
	}//End of method

	//---------------------------------------------------------------------------
	/** 
	 * Copy Document record and his relations to another object.
	 * Return Rb_Document or false.
	 * Note that Rb_Document is not save in database, you must call save() method after this method
	 * Note that extends properties are not copied here and can not be set by $properties array
	 * 
	 * @param Rb_Document $from_document
	 * @param array $properties
	 * @return Rb_Document|Bool
	 */
	protected static function _copy(Rb_Document &$from_document, array $properties=array()){
		if( !$from_document->getId() ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}

		$to_document = new Rb_Document($from_document->getSpace(), 0);
		foreach( $from_document->getProperties() as $key=>$val ){ //init properties of new doc from current doc
			$to_document->setProperty($key, $val);
		}
		$to_document->setProperty('from_document', $from_document->getId());
		$to_document->setProperty('document_access_code', 0);
		$to_document->setProperty('check_out_by', null);
		$to_document->setProperty('check_out_date', null);
		$to_document->setProperty('instance_id', null);
		$to_document->setProperty('open_by', Rb_User::getCurrentUser()->getId() );
		$to_document->setProperty('open_date', time());
		$to_document->setProperty('update_by', Rb_User::getCurrentUser()->getId() );
		$to_document->setProperty('update_date', time());
		$to_document->setProperty('owned_by', Rb_User::getCurrentUser()->getId() );
		//$to_document->setProperty('document_state', 'init');
		//$to_document->setProperty('document_life_stage', 1);
		//$to_document->setProperty('document_bid', uniqId() );
		//$to_document->setProperty('version_id', 1);
		//$to_document->setProperty('iteration_id', 1);

		if($properties)
		foreach( $properties as $name=>$value ){
			$to_document->setProperty($name, $value);
		}

		//Copy the doclinks
		$doclink = new Rb_Doclink($from_document);
		if( !$doclink->copySons($to_document->getId()) ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(),
                                                  'cant copy DocLinks');
		}

		//copy objects relation
		Rb_Object_Relation::get()->copyLinks($from_document->getId(), $to_document->getId());

		//Copy the relationship
		/*
		 //require_once('class/productStructure/relationship.php');
		 $docrel = new relationship($this);
		 if(!$docrel->copySons($t_document_id)){
		 Ranchbe::getError()->push(Rb_Error::ERROR, array('debug'=>array($this->_id)), 'cant copy Document sons');
		 }
		 */

		return $to_document;
	}//End of method

	//-------------------------------------------------------
	/** 
	 * Replace files from the wildspace to vault reposit directory
	 * and unlock the Document.
	 * return Rb_Document or false.
	 * 
	 * CheckIn is use after modify a Document. The files are copy from the user directory (the wildspace)
	 * to the protected and share reposit directory (the vault).
	 * 
	 * @param boolean $update, if true, copy files from wildspace to vault
	 * @param boolean $releasing, if true, unlock Document
	 * @param boolean | array 
	 * 						$withFiles, if true, checkin associates docfiles
	 * 						$withFiles, if false, do not checkin associates docfiles
	 * 						$withFiles, array of docfiles id, checkin only this docfile
	 * @return boolean
	 */
	protected function _checkIn($update = false , $releasing = false, $withFiles = true){
		//get list of associated files.
		if( !$this->getDocfiles() ){
			Ranchbe::getError()->push(Rb_Error::INFO, array('element'=>$this->getNumber() ),
                      'None associated file to checkin for Document %element%');
		}

		//checkIn associated files
		$hasChanged = false;
		$checkinFileError = false;
		if($withFiles){
			foreach($this->getDocfiles() as $docfile){
				if( $docfile->checkAccess() !== 1 ) continue;
				if(is_array($withFiles)){
					if(!in_array( $docfile->getId(), $withFiles ) ){
						continue;
					}
				}
				if( $update ) $iterationDocfile = $docfile->checkInFile( $releasing );
				else if( $releasing ) $iterationDocfile = $docfile->resetFile();
				if( !$iterationDocfile ){
					Ranchbe::getError()->push(Rb_Error::WARNING, array('element'=>$docfile->getNumber() ),
	                                                       'checkIn of file %element% failed');
					$checkinFileError = true;
					continue;
				}
				//Test if Document file has been changed
				if($docfile != $iterationDocfile){
					$hasChanged = true;
				}
			} //End of foreach
		}else{
			Ranchbe::getLogger()->log('checkin without files');
		}
		
		//if at least one file has change, increment iteration
		if($hasChanged){
			$this->setProperty('iteration_id', $this->getProperty('iteration_id')+1);
			if($update){ //update
				$this->setProperty('update_by', Rb_User::getCurrentUser()->getId() );
				$this->setProperty('update_date', time() );
			}
		}

		/*If a error is occured during chechin of files, get out function
		 File checkin with success are let in checkin state.
		 File failed are in checkout state. Document is let in checkout state.
		 */
		if($checkinFileError){
			Ranchbe::getError()->push(Rb_Error::ERROR, array( 'element'=>$this->getNumber() ),
					tra('checkIn of Document %element% failed or is not completed.
                  		<br />Correct the errors and retry.') );
			$this->save();
			return false;
		}
		
		$this->_dao->getAdo()->StartTrans(); //Restart a smart transaction
		
		$docfilesAreReleased = true;
		foreach($this->getDocfiles() as $docfile){
			if( $docfile->checkAccess() === 1 ) {
				$docfilesAreReleased = false;
			}
		}

		if($releasing && $docfilesAreReleased){
			//Unlock the Document
			if(!$this->unLockDocument(false)){
				Ranchbe::getError()->push(Rb_Error::ERROR,
								array('element'=>$this->getNumber() ),
								tra('cant unlock doc %element%') );
				$this->_dao->getAdo()->CompleteTrans(false);
				return false;
			}
		}

		$this->save(false);

		if($this->_dao->getAdo()->CompleteTrans() === false){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$this->getNumber() ),
				tra('checkin of doc %element% is not possible') );
			return false;
		}

		return true;
	}//End of method

	//-------------------------------------------------------------------------
	/** Return a instance of object.
	 *   if none parameter return a special instance with no possibilies to change here properties
	 *   and wich can not be saved. If object "$id" dont exist return false.
	 *   If object "$id" has already been instantiated, then return this instance (singleton pattern).
	 * 
	 * @param String $space_name
	 * @param Integer $id
	 * @return Rb_Document
	 */
	public static function get($space_name, $id = -1){
		if($id < 0) $id = -1; //forced to value -1
		if($id == 0)
		return new Rb_Document(Rb_Space::get($space_name), 0);
		$space_name = Rb_Space::get($space_name)->getName();
		if( !self::$_registry[$space_name][$id] ){
			self::$_registry[$space_name][$id] = new Rb_Document(Rb_Space::get($space_name), $id);
		}
		return self::$_registry[$space_name][$id];
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#getProperty($property_name)
	 */
	public function getProperty($name){
		switch($name){
			case 'id':
			case '_id':
			case 'file_id':
			case 'file_name':
			case 'extend_id':
			case $this->FIELDS_MAP_ID:
				return $this->_id;
				break; //this properties are not core properties

			case 'bid':
			case 'document_bid':
				return $this->core_props['document_bid'];
				break;

			case 'number':
			case $this->FIELDS_MAP_NUM:
				return $this->core_props[$this->FIELDS_MAP_NUM];
				break;

			case 'name':
			case $this->FIELDS_MAP_NAME:
				return $this->core_props[$this->FIELDS_MAP_NAME];
				//return $this->getProperty('normalized_name');
				break;

			case 'normalized_name':
				return $this->getProperty('number').'-'.
						$this->getProperty('version').'.'.
						$this->getProperty('iteration');
				break;

			case 'description':
			case $this->FIELDS_MAP_DESC:
				return $this->core_props[$this->FIELDS_MAP_DESC];
				break;

			case 'state':
			case $this->FIELDS_MAP_STATE:
				return $this->core_props[$this->FIELDS_MAP_STATE];
				break;

			case 'version':
			case 'version_id':
			case $this->FIELDS_MAP_VERSION:
				return 1;
				return $this->core_props[$this->FIELDS_MAP_VERSION];
				break;

			case 'iteration':
			case 'iteration_id':
			case 'document_iteration':
			case $this->FIELDS_MAP_ITERATION:
				return $this->core_props[$this->FIELDS_MAP_ITERATION];
				break;

			case 'indice':
			case 'document_indice':
				if(isset($this->core_props['document_indice'])) //if null retry to set too, so call to isset is correct
					return $this->core_props['document_indice'];
				else{
					return $this->core_props['document_indice'] =
						Rb_Indice::singleton()->getName($this->getProperty('version'));
				}
				break;

			case 'access_code':
			case 'document_access_code':
				return $this->core_props['document_access_code'];
				break;

			case 'life_stage':
			case 'document_life_stage':
				return $this->core_props['document_life_stage'];
				break;

			case 'process':
			case 'process_id':
			case 'default_process_id':
				return $this->core_props['default_process_id'];
				break;

			case 'instance':
			case 'instance_id':
				return $this->core_props['instance_id'];
				break;

			case 'doctype':
			case 'doctype_id':
				return $this->core_props['doctype_id'];
				break;

			case 'category':
			case 'category_id':
				return $this->core_props['category_id'];
				break;

			case 'father_id':
			case 'container_id':
			case $this->FIELDS_MAP_FATHER:
				return $this->core_props[$this->FIELDS_MAP_FATHER];
				break;

			case 'from_document':
			case 'open_date':
			case 'close_date':
			case 'update_date':
			case 'check_out_date':
			case 'close_by':
			case 'open_by':
			case 'update_by':
			case 'check_out_by':
			case 'owned_by':
			case 'file_only':
				return $this->core_props[$name];
				break;

			default:
				return false;
				break;
		} //End of switch
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#setProperty($property_name, $property_value)
	 */
	public function setProperty($name, $value){
		if($this->getId() < 0) return false; //Can not change property on object -1
		$this->isSaved = false;
		switch($name){
			case 'id':
			case '_id':
			case 'file_id':
			case 'file_name':
			case 'extend_id':
			case $this->FIELDS_MAP_ID:
				break; //this properties are not core properties

			case 'bid':
			case 'document_bid':
				$this->core_props['document_bid'] = $value;
				break;

			case 'number':
			case $this->FIELDS_MAP_NUM:
				$this->core_props[$this->FIELDS_MAP_NUM] = $value;
				break;

			case 'name':
			case $this->FIELDS_MAP_NAME:
				$this->core_props[$this->FIELDS_MAP_NAME] = $value;
				break;

			case 'description':
			case $this->FIELDS_MAP_DESC:
				$this->core_props[$this->FIELDS_MAP_DESC] = $value;
				break;

			case 'state':
			case $this->FIELDS_MAP_STATE:
				$this->core_props[$this->FIELDS_MAP_STATE] = $value;
				break;

			case 'version':
			case 'version_id':
			case $this->FIELDS_MAP_VERSION:
				$this->core_props[$this->FIELDS_MAP_VERSION] = (int) $value;
				break;

			case 'iteration':
			case 'iteration_id':
			case $this->FIELDS_MAP_ITERATION:
				$this->core_props[$this->FIELDS_MAP_ITERATION] = (int) $value;
				break;

			case 'access_code':
			case 'document_access_code':
				$this->core_props['document_access_code'] = (int) $value;
				break;

			case 'life_stage':
			case 'document_life_stage':
				$this->core_props['document_life_stage'] = (int) $value;
				break;

			case 'process':
			case 'process_id':
			case 'default_process_id':
				if(!$value) $value=null; else $value=(int) $value;
				$this->core_props['default_process_id'] = $value;
				break;

			case 'instance':
			case 'instance_id':
				if(!$value) $value=null; else $value=(int) $value;
				$this->core_props['instance_id'] = $value;
				break;

			case 'doctype':
			case 'doctype_id':
				if(!$value) $value=null; else $value=(int) $value;
				$this->core_props['doctype_id'] = $value;
				break;

			case 'category':
			case 'category_id':
				if(!$value) $value=null; else $value=(int) $value;
				$this->core_props['category_id'] = $value;
				break;

			case 'father_id':
			case 'container_id':
			case $this->FIELDS_MAP_FATHER:
				if(!$value) $value=null; else $value=(int) $value;
				$this->core_props[$this->FIELDS_MAP_FATHER] = $value;
				break;

			case 'check_out_by':
			case 'check_out_date':
			case 'from_document':
				if(!$value) $value=null; else $value=(int) $value;
				$this->core_props[$name] = $value;
				break;

			case 'open_date':
			case 'close_date':
			case 'update_date':
			case 'close_by':
			case 'open_by':
			case 'update_by':
			case 'owned_by':
			case 'file_only':
				$this->core_props[$name] = (int) $value;
				break;

			default:
				return false;
				break;
		} //End of switch
		return $this;
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#unsetProperty($property_name)
	 */
	function unsetProperty($property_name){
		switch($property_name){
			case 'father_id':
			case 'container_id':
				unset($this->core_props[$this->FIELDS_MAP_FATHER]);
				break;
			default:
				unset($this->core_props[$property_name]);
				break;
		} //End of switch
		return true;
	}//End of method

	//--------------------------------------------------------
	/** 
	 * This method can be used to get list of documents in the current space
	 * Return a array if success, else return false.
	 * 
	 * @param Array $params
	 * @param Bool $displayHistory
	 * @param Integer $container_id
	 * @return unknown_type
	 */
	public function getDocuments($params, $displayHistory=false, $container_id=0){
		if(!$displayHistory){
			$params['where'][] = $this->_dao->getTableName().'.document_access_code < \'15\'';
		}
		if($container_id){
			$params['exact_find'][$this->_dao->getTableName().'.'.$this->FIELDS_MAP_FATHER] = $container_id;
		}
		return $this->_dao->getAllBasic($params);
	}//End of method

	//--------------------------------------------------------
	/** 
	 * This method can be used to get datas recorded in database about Document.
	 * return an array if success, else return false.
	 * 
	 * @param integer $document_id
	 * @param string $space_name
	 * @param array $selectClose
	 * @return array
	 */
	public static function getInfos($document_id, $space_name, array $selectClose=array()){
		return Rb_Document::get($space_name)->getDao()
					->getBasicInfos($document_id,array('select'=>$selectClose));
	}//End of method

	//--------------------------------------------------------
	/** 
	 * Set the container of the Document by reference
	 * Example :
	 * $container = new Rb_Container($space, 1);
	 * $doc = new Rb_Document($space, 1);
	 * $doc->setContainer($container); //The object $container is now link to object $doc
	 *
	 * prefere the setFather($id) method
	 * 
	 * @param Rb_Container $container
	 * @return Rb_Document
	 */
	function setContainer(Rb_Container &$container){
		if($this->getId() < 0) return false;
		$this->setProperty('container_id', $container->getId());
		$this->container =& $container;
		return $this;
	}//End of method

	//--------------------------------------------------------
	/** 
	 * Return Rb_Container of current object
	 * 
	 * @return Rb_Container
	 */
	function getContainer(){
		if( $this->getId() < 0 ) return false; //None container on object -1
		if( $this->container ) return $this->container;
		if( !$this->getProperty('container_id') ){
			trigger_error( 'can not get container_id property', E_USER_WARNING );
			return false;
		}
		return $this->container =& Rb_Container::get($this->space->getName(), $this->getProperty('container_id'));
	}//End of method

	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Interface/Rb_Object_Interface_Related#getFather()
	 */
	function getFather(){
		return $this->getContainer();
	}//End of method
	
	//-------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Interface/Rb_Object_Interface_Related#setFather($father)
	 */
	function setFather( &$container ){
		return $this->setContainer($container);
	}//End of method
	
	//--------------------------------------------------------
	/** 
	 * Return doctype of current Document.
	 * 
	 * @return Rb_Doctype
	 */
	function getDoctype(){
		if( !$this->doctype ){
			$this->doctype =& Rb_Doctype::get( $this->getProperty('doctype_id') );
		}
		return $this->doctype;
	}//End of method
	
	//--------------------------------------------------------
	/** 
	 * Get a docfile object from $this->docfiles property
	 * return a docfile object for class main, other type return array of docfile object
	 *
	 * if $role = main|mainvisu|mainpicture|mainannex return a docfile object
	 * if $role =  visu|picture|annex return a array of docfile object
	 *
	 * there is only one main docfile for a Document. May be have many picture, visu or annex docfile.
	 * Main visu, picture or annex is the first docfile of the role
	 * 
	 * @param String $role
	 * @return Rb_Docfile
	 */
	function getDocfile($role = 'main'){
		$this->getDocfiles(); //init docfile if necessary
		switch($role){
			case 'main':
				return $this->docfiles['main'];
				break;
			case 'mainvisu':
				return $this->docfiles['byRole'][1][0];
				break;
			case 'visu':
				return $this->docfiles['byRole'][1];
				break;
			case 'mainpicture':
				return $this->docfiles['byRole'][2][0];
				break;
			case 'picture':
				return $this->docfiles['byRole'][2];
				break;
			case 'mainannex':
				return $this->docfiles['byRole'][3][0];
				break;
			case 'annex':
				return $this->docfiles['byRole'][3];
				break;
		}
		return false;
	}//End of method
	
	//--------------------------------------------------------
	/** 
	 * Get objects collection from $this->docfiles property
	 * return array or false.
	 * \param $return (string) type of request
	 *     'noAssoc' return array where key is docfile id, default value
	 *     'byRole' return a array where key is role id
	 *
	 * See too getAssociatedFile()
	 * 
	 * @param string $return	'noAssoc' | 'byRole'
	 * @return array|false
	 * 
	 */
	function getDocfiles( $return = 'noAssoc' ){
		//init docfiles
		if( !$this->docfiles['noAssoc'] ){
			$this->docfiles = array();
			$this->docfiles['noAssoc'] = array();
			$this->docfiles['byRole'] = array();
			$params['exact_find'][$this->FIELDS_MAP_ID] = $this->_id;
			$params['select'] = array('file_id','role_id');
			$params['getRs'] = true;
			$rs = Rb_Docfile_Role::get($this->space->getName())->getAll($params);
			if($rs){
				while (!$rs->EOF){
					$this->docfiles['noAssoc'][$rs->fields['file_id']] =&
						Rb_Docfile::get($this->space->getName(), $rs->fields['file_id']);
					if( !$rs->fields['role_id'] ) $rs->fields['role_id'] = 0;
					$this->docfiles['byRole'][$rs->fields['role_id']][] =&
						Rb_Docfile::get($this->space->getName(), $rs->fields['file_id']);
					$rs->MoveNext();
				}
			}else{
				return false;
			}
			$this->docfiles['main'] =& $this->docfiles['byRole'][0][0];
		}
		if( $return == 'byRole' ){
			return $this->docfiles['byRole'];
		}else{
			return $this->docfiles['noAssoc'];
		}
	}//End of method
	
	//--------------------------------------------------------
	/** 
	 * Set the docfiles of Document
	 * Return Rb_Document
	 * 
	 * @param Rb_Docfile $docfile
	 * @param integer $role
	 * @return Rb_Document
	 */
	function setDocfile(Rb_Docfile &$docfile, $role){
		$docfile->setFather($this);
		$this->docfiles['noAssoc'][$docfile->getId()] =& $docfile;
		switch($role){
			case 0:
			case 'main':
				if(!$this->docfiles['main']){
					$this->docfiles['byRole'][0][0] =& $docfile;
					$this->docfiles['main'] =& $docfile;
				}else{
					$this->docfiles['byRole'][3][] =& $docfile;
				}
				break;
			case 1:
			case 'visu':
				$this->docfiles['byRole'][1][] =& $docfile;
				break;
			case 2:
			case 'picture':
				$this->docfiles['byRole'][2][] =& $docfile;
				break;
				//case 3:
				//case 'annex':
			default:
				$this->docfiles['byRole'][3][] =& $docfile;
				break;
		}
		return $this;
	}//End of method
	
	//--------------------------------------------------------
	/** 
	 * This method return the Document id
	 * of the Document number in the max indice if exist.
	 * If indice_id is specified return id of Document number for this indice.
	 * else return false.
	 * this method is a static like and may be call from object -1
	 * 
	 * @param string $document_number
	 * @param integer $version_id
	 * @return Integer
	 */
	public function ifExist( $document_number , $version_id = 0 ){
		if( $version_id > 0 ){
			$query =  "SELECT document_id FROM ".$this->_dao->getTableName().
              " WHERE document_number = '$document_number'
              AND document_version = $version_id";
		}else{
			$query =  "SELECT document_id FROM ".$this->_dao->getTableName().
              " WHERE document_number = '$document_number'
              AND document_version = 
              (SELECT MAX(document_version)
              FROM "
			. $this->_dao->getTableName() .
              " WHERE document_number = '$document_number'
              GROUP  BY document_number)";
		}

		$one = $this->_dao->getAdo()->GetOne($query);
		return $one;
	}//End of method

	//--------------------------------------------------------------------
	/** 
	 * To recognize the type of Document
	 *  Return Rb_Doctype or false.
	 * 
	 * @param String $file_extension
	 * @param String $file_type
	 * @return Rb_Doctype | false
	 */
	function setDocType($file_extension, $file_type){
		//If there is at least one doctype link to container,
		//then doctype choice is restricted to this list.
		//First, test if there is at least one doctype link to container
		if( $this->getContainer()->hasDoctype() ){
			//Get array of all doctype linked to container
			$doctypeList = $this->getContainer()->getDoctypes($file_extension ,
			$file_type, true);
		}else{ //If no doctypes links to container, get all doctypes
			$dtparams = array();
			if(!is_null($file_extension)) $dtparams['exact_find']['file_extension'] = $file_extension;
			if(!is_null($file_type)) $dtparams['exact_find']['file_type'] = $file_type;
			$doctypeList = Rb_Doctype::get()->getAll($dtparams);
		}
		//If no valid doctypes, then this Document is not authorized
		if(!$doctypeList){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$doctypeList),
     			'This doctype %element% is not permit');
			return false;
		}

		//For each doctype test if current Document corresponding to regex
		foreach($doctypeList as $type){
			if( !empty($type['recognition_regexp']) ){
				if(preg_match('/'.$type['recognition_regexp']
				.'/', $this->getProperty('document_number') ) ){
					$doctype = $type; //assign doctype and exit loop
					break;
				}
			}else
			$genericDoctype = $type; //if recognition_regexp field is empty, its a generic doctype
		}

		//If no match doctypes, then assign the generic doctype, else return false
		if(!$doctype && $genericDoctype){
			//But the number must be respect the defined generic regex
			$genericRegEx = Ranchbe::getConfig()->document->mask;
			if(!empty($genericRegEx)){
				if(preg_match('/'.$genericRegEx['recognition_regexp']
				.'/', $this->getProperty('document_number') ) )
				$doctype = $genericDoctype;
			}else $doctype = $genericDoctype;
		}

		if(!$doctype){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(),
                                                  'This doctype is not permit');
			return false;
		}
		$this->setProperty('doctype_id', $doctype['doctype_id']);
		return $this->doctype =& Rb_Doctype::get($doctype['doctype_id']);
	}//End of method

	//---------------------------------------------------------------------------
	/**
	 * This method create a Document in a container.
	 * Note that $suppress and $keepCo can not be use together
	 * Return true or false
	 * 
	 * @param 	Boolean	$lock		, if true lock the Document and docfiles after store.
	 * @param 	Boolean	$suppress 	, if true suppress file from.
	 * @param 	Boolean	$keepCo 	, if true keep document as checkouted
	 * @return 	Boolean
	 */
	public function store($lock = false, $suppress = true, $keepCo = false){
		$this->_dao->getAdo()->StartTrans(); //Start a transaction
		$this->_initObservers();
		$this->isSaved = false;
		//Set name from docfile property
		if( $docfile =& $this->getDocfile('main') ){ //If a file is require get the main file
			Ranchbe::getError()->info( 'create Document from file : '.$docfile->getNumber() );
			if(!$this->getNumber()){ //Set name from docfile property only if a name is not yet set
				$this->setNumber( $docfile->getProperty('doc_name') );
			}
		}else{ // if no file
			Ranchbe::getError()->info( 'create Document from Document number : '.$this->getNumber() );
			//$file_extension = NULL;
			//$file_type = 'nofile';
		}

		if(!$this->getNumber()){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), tra('Document number is not set') );
			return false;
		}
		//Create Document, after setName because _create call setDoctype wich need Document name
		//$r = $this->_create();
		$r = self::_createPermanentObject();
		if( !$r || $this->_dao->getAdo()->HasFailedTrans() ){
			$this->_dao->getAdo()->CompleteTrans(false);
			return false;
		}

		//Execute evenement scripts // after setDoctype : if not can not call doctype script
		$this->notify_all('doc_pre_store', $this); //Notify observers on event
		if( $this->stopIt ) { //use by observers to control execution
			return false;
		}

		//Store the main docfiles
		if( $docfile ){
			if(!$docfile->storeFile($suppress, $keepCo)){
				$this->_dao->getAdo()->CompleteTrans(false);
				return false;
			}
		}

		//if main docfile is stored, validate transaction
		if($this->_dao->getAdo()->CompleteTrans() === false){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), $this->_dao->getAdo()->ErrorMsg());
			return false;
		}

		//Store other files
		for( $i=1 ; $i < count($this->docfiles['noAssoc']) - 1 ; $i++ ){
			$this->docfiles['noAssoc'][$i]->storeFile();
		}
		
		if($keepCo){
			$this->lockDocument(1, false);
		}else{
			if($lock){ //If $lock = true, lock the Document
				$this->setProperty('document_access_code', 11 );
			}
		}

		//Execute evenement scripts
		$this->notify_all('doc_post_store', $this); //Notify observers on event

		//Save properties in database
		if(!$this->isSaved){
			$this->_update();
		}

		//Write history
		$this->writeHistory('storeNewDocument');

		return true;

	}//End of method

	//---------------------------------------------------------------------------
	/** 
	 * This method record the file $file from the wildspace in database
	 * and copy it in vault and associate it to Document $document_id
	 * if suppress = true, the file will be suppress of the wildspace
	 *
	 * Return Rb_Docfile or false
	 *
	 * See too associateDocfile
	 *
	 * @param string $file , path and file to associate
	 * @param bool $suppress , true for suppress file of the wildspace after associate it
	 * @param integer $role, id of role for this docfile : see Rb_Dofile_Role
	 * @param integer $mode, mode of generation of this docfile: 0=generated by user, 1=generated by Ranchbe
			 when mode=1, the docfile will be never modified by user
	 * @return Rb_Docfile | false
	 */
	function associateFile($file , $suppress = false, $role=0, $mode=0){
		if( !$this->_id ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}
		//Create a new docfile
		$docfile = new Rb_Docfile($this->space); //Get the new docfile
		$docfile->setFsdata( new Rb_Fsdata($file) ); //Get the new Rb_Fsdata
		$docfile->setProperty('file_mode', $mode);
		return $this->associateDocfile($docfile, $suppress, $role);
	}//End of method

	//---------------------------------------------------------------------------
	/** 
	 * Associate the docfile to current Document
	 *  Return Rb_Docfile or false
	 *
	 *  If a docfile with same number is existing in database, create a new version
	 *   of to associate it to Document.
	 *
	 * See too associatefile
	 *
	 * @param Rb_Docfile Docfile to associate
	 * @param bool If true, suppress data after store docfile in vault
	 * @param integer Role id for new docfile
	 * 
	 * @return Rb_Docfile | false
	 * 
	 *
	 */
	function associateDocfile(Rb_Docfile $docfile , $suppress = false, $role=0){
		if( $this->_id < 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}
		//check if access is free
		$access = $this->checkAccess();
		if($access > 0){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$access),
			tra('This Document is locked with code %element%, associate is not permit'));
			return false;
		}

		$docfile->setFather($this);
		$this->docfiles['noAssoc'][0] =& $docfile;

		//Notify event to observers
		$this->_initObservers();
		$this->notify_all('doc_pre_associateFile', $this);

		//Get the iteration of file if exists
		if( $current_iteration = Rb_Docfile_Iteration::getLastIid($docfile) ){
			$docfile->setIterationId($current_iteration + 1);
		}

		//Create a new file record in database
		$file_id = $docfile->storeFile($suppress);
		if(!$file_id){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$file),
                                              'cant record the file %element%');
			return false;
		}
		unset($this->docfiles['noAssoc'][0]);
		$this->docfiles['noAssoc'][$file_id] =& $docfile;

		//Set the role
		if($role){
			$roles = new Rb_Docfile_Role($this->space, 0);
			$roles->setFile($file_id)->setRole($role);
			$roles->save();
			$this->docfiles['byRole'][$role][] =& $docfile;
		}

		//Notify event to observers
		$this->notify_all('doc_post_associateFile', $this);

		//Write history
		$this->writeHistory('associateFile');

		return $docfile;

	}//End of method

	//---------------------------------------------------------------------------
	/** 
	 * Change the state of a Document
	 *
	 * @param string 	New value of the state.
	 * @param boolean	If true, save in database.
	 * 
	 * @return boolean
	 * 
	 */
	function changeState($state='init', $save=true){
		if( $this->_id < 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}
		$this->setProperty('document_state', $state);
		if($save){
			if( $this->_dao->update(array('document_state'=>$state), $this->_id) ){
				return true;
			}else return false;
		}
		return true;
	}//End of method

	//---------------------------------------------------------------------------
	/** 
	 * Lock a Document with code.
	 *
	 * The value of the access code permit or deny action:<ul>
	 *  <li>0: Modification of Document is permit.</li>
	 *  <li>1: Modification of Document is in progress. The checkOut is deny. You can only do checkIn.</li>
	 *  <li>5: A workflow process is in progress. Modification is deny. You can only activate activities.</li>
	 *  <li>10: The Document is lock.You can only upgrade his indice.</li>
	 *  <li>11: Document is lock. You can only unlock it or upgrade his indice.</li>
	 *  <li>15: Indice is upgraded. You can only archive it.</li>
	 *  <li>20: The Document is archived.</li>
	 *</ul>
	 * @param integer		new access code.
	 * @param boolean		if true save lock in database, else not
	 * 
	 * @return boolean
	 *  
	 */
	function lockDocument($code = 1, $save = true){
		if( !$this->_id ){
			trigger_error('$this->_id is not set', E_USER_WARNING);
			return false;
		}
		$this->setProperty('document_access_code', $code );
		if($code == 1){
			$this->setProperty('check_out_by', Rb_User::getCurrentUser()->getId() );
			$this->setProperty('check_out_date', time() );
		}
		if($code == 0){
			$this->setProperty('check_out_by', NULL );
			$this->setProperty('check_out_date', NULL );
		}
		//update database to lock Document
		$data['document_access_code'] = $this->getProperty('document_access_code');
		$data['check_out_by'] = $this->getProperty('check_out_by');
		$data['check_out_date'] = $this->getProperty('check_out_date');
		if($save){
			return $this->_dao->update($data, $this->_id);
		}else{
			return true;
		}
	}//End of method
	
	//---------------------------------------------------------------------------
	/** 
	 * This method can be used to unlock a Document.
	 * 
	 * @param bool, if true save lock in database, else not
	 * @return boolean
	 *
	 */
	public function unLockDocument($save=true){
		return $this->lockDocument(0, $save);
	}//End of method
	
	//---------------------------------------------------------------------------
	/** 
	 * Return comprehensive name from lock code
	 *
	 * @param integer
	 * @return String
	 *
	 */
	public function getLockName($code){
		return $this->_lockCode[$code];
	}//End of method
	
	//-------------------------------------------------------
	/** Put files of a Document from the vault reposit directory to user wildspace and lock the Document.
	 * return true or false.
	 *
	 * CheckOut is use for modify a Document. The files are put in a directory where user has write access
	 * and the Document storing on the protected directory (the vault) is lock. Thus it is impossible that a other user modify it in the same time.
	 * 
	 * If option $checkoutFile is false, options $ifExistMode, $getFiles and $withFiles are ignored
	 * If option $getFiles is false, options $ifExistMode is ignored
	 * 
	 * 
	 * @param boolean	$checkoutFile 	if true checkout files of document
	 * @param enum		$ifExistMode 	cancel|replace|keep
	 * @param boolean	$getFiles 		if true copy file in wildspace
	 * @param boolean | array
	 * 						$withFiles, if true, checkin all associated docfiles
	 * 						$withFiles, if array of docfiles id, checkin only this docfiles
	 * @return boolean
	 * 
	 */
	public function checkOut($checkoutFile = true, $ifExistMode='cancel', $getFiles = true, $withFiles = true ){
		if( $this->_id <= 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}

		//check if access is free, valide acodes : 0-1
		$access_code = $this->checkAccess();
		if( ($access_code > 1) || ($access_code < 0) ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$access_code), tra('this Document is locked with code %element%, checkOut is not permit') );
			return false;
		}
		if($access_code === 1){
			if( !$this->checkUpdateRight() ){
				Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$access_code), tra('this Document is locked with code %element%, checkOut is not permit') );
				return false;
			}
		}else{
			$this->_initObservers();
		}


		/*About transaction and exception :
		 if an error occurs, rollback operations in database. doc and file are not checkout
		 but file(s) could remain in wildspace
		 */
		$this->_dao->getAdo()->StartTrans(); //Start a smart transaction

		//Notiy observers on event
		$this->notify_all('doc_pre_checkout', $this);
		if( $this->stopIt ) { //use by observers to control execution
			return false;
		}

		//lock the Document
		if($access_code !== 1){
			if ( !$this->lockDocument(1, true) ){
				Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$this->_id),
	                                                      'unable to lock Document');
				$this->_dao->getAdo()->CompleteTrans(false);
				return false;
			}
		}

		//get list of associated files and check access
		if($checkoutFile){
			$docfiles = $this->getDocfiles('noAssoc');
			if($docfiles === false){
				Ranchbe::getError()->push(Rb_Error::WARNING, array('element'=>$this->_id),
                                              'unable to list associated files');
			}

			//checkOut associated files
			if( is_array($docfiles) ){
				foreach( $docfiles as $docfile ){
					if(is_array($withFiles)){
						if(!in_array( $docfile->getId(), $withFiles ) ){
							continue;
						}
					}
					if( $docfile->checkAccess() !== 0 ) continue;
					if ( !$docfile->checkOutFile($getFiles, $ifExistMode) ){
						Ranchbe::getError()->push(Rb_Error::WARNING,
							array('element'=>$docfile->getProperty('file_name')),
							tra('unable to checkOut file %element%') );
						$this->_dao->getAdo()->CompleteTrans(false); //Rollback. If one error on one file all transaction is rollback, so all Document and his file are not checkout but files may be stay in wildspace.
						return false;
					}
				}
			}
		}

		$this->_dao->getAdo()->CompleteTrans();

		//Notiy observers on event
		$this->notify_all('doc_post_checkout', $this);

		//Write history
		$this->writeHistory('checkOut');

		return true;

	}//End of method

	//-------------------------------------------------------
	/**
	 * Unlock the Document after a checkOut without copy files from wildspace to vault
	 * 
	 * @return boolean
	 *
	 */
	public function cancelCheckOut(){
		if( $this->_id <= 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}
		//Check if the Document is checkOut by me
		$access = $this->checkUpdateRight();
		if(!$access){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('access'=>$access ,
    		'element'=> $this->getNumber() ), 'checkIn of doc %element% is not permit');
			return false;
		}
		$this->_initObservers();
		$this->notify_all('doc_pre_reset', $this);
		if( $this->stopIt ) { //use by observers to control execution
			return false;
		}
		if ($this->_checkIn(false, true)){
			//Write history
			$this->notify_all('doc_post_reset', $this);
			$this->writeHistory('cancelCheckout');
			return true ;
		}
		else return false;
	}//End of method

	//-------------------------------------------------------
	/** ckeckin Document and releasing reservation
	 *
	 * @param boolean | array 
	 * 						$withFiles, if true, checkin associates docfiles
	 * 						$withFiles, if false, do not checkin associates docfiles
	 * 						$withFiles, array of docfiles id, checkin only this docfile
	 * @return boolean
	 */
	public function checkInAndRelease($withFiles = true){
		if( $this->_id <= 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}
		//Check if the Document is checkOut by me
		$access = $this->checkUpdateRight();
		if(!$access){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('access'=>$access ,
    			'element'=> $this->getNumber() ), 'checkIn of doc %element% is not permit');
			return false;
		}
		$this->_initObservers();
		$this->notify_all('doc_pre_checkinAndRelease', $this); //Notify observers on event
		if( $this->stopIt ) { //use by observers to control execution
			return false;
		}
		if($this->_checkIn(true,true,$withFiles)){ //update and releasing
			$this->notify_all('doc_post_checkinAndRelease', $this);
			$this->writeHistory('checkInAndRelease');
			return true;
		}
		else return false;
	}//End of method

	//-------------------------------------------------------
	/** ckeckin Document and keep reservation
	 * 
	 * @param boolean | array 
	 * 						$withFiles, if true, checkin associates docfiles
	 * 						$withFiles, if false, do not checkin associates docfiles
	 * 						$withFiles, array of docfiles id, checkin only this docfile
	 * @return boolean
	 *
	 */
	public function checkInAndKeep($withFiles = true){
		if( $this->_id <= 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}
		//Check if the Document is checkOut by me
		$access = $this->checkUpdateRight();
		if(!$access){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('access'=>$access ,
    			'element'=> $this->getNumber() ), 'checkIn of doc %element% is not permit');
			return false;
		}
		$this->_initObservers();
		$this->notify_all('doc_pre_checkinAndKeep', $this); //Notify observers on event
		if( $this->stopIt ) { //use by observers to control execution
			return false;
		}
		if($this->_checkIn(true,false,$withFiles)){ //update and not releasing
			$this->notify_all('doc_post_checkinAndKeep', $this);
			$this->writeHistory('checkInAndKeep');
			return true ;
		}
		else return false;
	}//End of method

	//-------------------------------------------------------
	/** This method can be used to check if a Document is free.
	 * Return integer, access code (0 for free without restriction , 100 if error).
	 *
	 */
	public function checkAccess(){
		if( !$this->_id ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}
		/* request database or get from instance properties??
		 $query = 'SELECT document_access_code FROM '.$this->_dao->getTableName()
		 .' WHERE document_id = '.$this->_id;
		 $One = $this->_dao->getAdo()->GetOne($query);
		 if($One === false){
		 Ranchbe::getError()->push(Rb_Error::ERROR, array('query'=>$query), $this->_dao->getAdo()->ErrorMsg() );
		 return 100;
		 }else
		 return intval($One);
		 */
		$ret = $this->getProperty('document_access_code');
		if($ret === false) return 100;
		else return (int) $ret;
	}//End of method

	//-------------------------------------------------------
	/** This method can be used to check if the uers can update a Document
	 * Return true if document is currently checkout by current user
	 *
	 * @return boolean
	 */
	public function checkUpdateRight(){
		if( !$this->_id ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}
		if( $this->getProperty('document_access_code') == 1) { //The Document is checkOut...
			if( $this->getProperty('check_out_by') == Rb_User::getCurrentUser()->getId() ) { //... by current user
				return true; //Update is permit for current user
			}
		}
		return false;
	}//End of method
	
	//-------------------------------------------------------
	/** This method can be used to get all the files associated to a Document
	 *  You can use too getDocfiles().
	 *  This method create a query on database on
	 *  each call and not set the variable $this->docfiles.
	 *  You can create special query with parameter in $params.
	 * 
	 * @param array $params parameters of the query. See parameters function of getQueryOptions()
	 * @return array | false
	 */
	public function getAssociatedFiles($params=array()){
		if( !$this->_id ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}
		$params['exact_find'][$this->FIELDS_MAP_ID] = $this->_id;
		return Rb_Docfile::get($this->space->getName())->getAll($params);
	}//End of method

	//---------------------------------------------------------------------------
	/** Move a Document from container to another.
	 *
	 * A Document can be move only between container of the same space.
	 * You can not move a Document of a bookshop container to a cadlib container.
	 * This method modify the Document record and move the associated files from the
	 * directory of the original container to the directory of the tager container.
	 * Please, consider that this method dont move iteration files and the previous indice Document.
	 *
	 * @param Rb_Container		target container.
	 * @return boolean
	 */
	public function moveDocument(Rb_Container $to_container){
		if( !$this->_id ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}

		//Check if the target container is a Document manager. If not, exit.
		if( $to_container->isFileOnly() ){
			Ranchbe::getError()->push(Rb_Error::ERROR,
			array('name'=>$to_container->getNumber()),
                                'the target directory is a file manager type');
			return false;
		}

		//check if access is free
		if($this->checkAccess()){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('code'=>$access),
            'this Document is locked with code %code%, move is not permit');
			return false;
		}

		//Notify event to observers
		$this->_initObservers();
		$this->attach_all(new Rb_Reposit_Read_Dispatcher());
		$this->notify_all('doc_pre_move', $this);

		//link Document to a container
		$this->setContainer($to_container);
		$this->_update();

		//Notify event to observers
		$this->notify_all('doc_post_move', $this);

		//Write history
		$this->writeHistory('moveDocument');

		return true;
	}//End of method

	//---------------------------------------------------------------------------
	/** Copy Document to another Document with new name, new bid
	 *  Return Rb_Document or false.
	 * 
	 * @param Rb_Container 	$to_container, 	target container.
	 * @param string  		$to_number, 	number of the new Document to create.
	 * @param integer  		$to_version, 	number id of version for new Document.
	 * @param boolean  		$copyFiles, 	if true, copy files too and associate to new Document.
	 * @return Rb_Document | false
	 *
	 */
	public function copyDocumentToNumber(Rb_Container $to_container,
											$to_number,
											$to_version,
											$copyFiles = true){
		if( !$this->_id ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}

		//Check if the target container is a Document manager. If not, exit.
		if($to_container->isFileOnly() ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(),
                                  'the target directory is a file manager type');
			return false;
		}

		//Notify event to observers
		$this->_initObservers();
		$this->notify_all('doc_pre_copy', $this);

		if(!(is_integer($to_version) && $to_version > 0) ) $to_version = 1;

		$to_document = self::_copy($this, array('version_id'=>$to_version,
                                         'container_id'=>$to_container->getId(),
                                         'document_number'=>$to_number));
		$to_document->setProperty('document_bid', uniqId() );
		$to_document->setProperty('version_id', 1);
		$to_document->setProperty('iteration_id', 1);

		if($to_document->getNumber() != $this->getNumber()){
			$to_document->setDoctype( $this->getDocfile('main')->getProperty('file_extension'),
			$this->getDocfile('main')->getProperty('file_type') );
		}
		if( $to_document->getDoctype()->getId() > 0 ){
			//Copy extends properties
			$from_extend = new Rb_Object_Extend($this);
			$to_extend = $from_extend->copy($to_document);
			if(is_object($to_extend)) $to_extend->save(false);
			else $to_document->save(false);
		}
		if( !$to_document->getId() ){
			Ranchbe::getError()->push(Rb_Error::ERROR,
						array('element'=>$to_document->getNumber() ),
						tra('cant create the Document %element%') );
						return false;
		}

		//Copy files from ori reposit_dir to target reposit_dir and create records in database
		//$target_reposit_dir = $target_container->getProperty('default_file_path').'/__versions/1/1';
		if($copyFiles){
			foreach( $this->getDocfiles() as $docfile ){
				$t_file_root_name = str_replace($this->getProperty('document_number'),
						$to_document->getNumber(),
						$docfile->getProperty('file_root_name'),
						$replace_count
						);

				if($replace_count == 0){
					$t_file_root_name = uniqId();
				}

				$reposit =& Rb_Reposit::getActiveReposit($to_document->getSpace());
				$to_file_name = $t_file_root_name . $docfile->getProperty('file_extension');
				$iteration = 1;
				$docfile = $docfile->copyToReposit($reposit, $to_file_name, $to_document->getId(), $iteration, false);
				if(!$docfile){
					Ranchbe::getError()->push(Rb_Error::ERROR, 
													array('element'=>$srcfile),
                                                    'cant copy file %element%');
					continue;
				}
			}
		}

		//Notify event to observers
		$this->notify_all('doc_post_copy', $this);

		//Write history
		$this->getHistory()->setComment( 'copy to Document id '.$to_document->getId() );
		$this->writeHistory('copyDocument');

		return $to_document;

	}//End of method

	//-------------------------------------------------------
	/** Create a new Document version from Document
	 * 
	 * @param Rb_Container		attach version to this container
	 * @param integer	 		version id to create
	 * @param boolean			if true, copy files too and associate to new Document
	 * @return Rb_Document
	 */
	public function copyDocumentToVersion(Rb_Container &$to_container,
											$version_id,
											$copyFiles = true){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}
		$this->_dao->getAdo()->StartTrans(); //Start a transaction
		$this->_initObservers(); //init observers
		$this->notify_all('doc_pre_createVersion', $this); //Notify event to observers

		//If none version in parameter upgrade current indice of 1.
		if( !$version_id ){
			$version_id = $this->getProperty('document_version') + 1;
		}

		if(!Rb_Document_Version::ifValid($this, $version_id)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(
											'element'=>$version_id),
											tra('this indice is invalid'));
			return false;
		}

		$to_document = self::_copy($this, array(
                                'document_state'=>'init',
                                'document_life_stage'=>1,
                                'version_id'=>$version_id,
                                'iteration_id'=>1,
		));
		//set new container if a container is set in parameter
		if( $to_container->getId() &&
			( $to_container->getId() != $to_document->getProperty('container_id') ) ){
			$to_document->setContainer($to_container);
		}
		//Copy extends properties
		$from_extend = new Rb_Object_Extend($this);
		$to_extend = $from_extend->copy($to_document);
		if(is_object($to_extend)) $r=$to_extend->save(false);
		else $r = $to_document->save(false);
		if( !$r ){
			Ranchbe::getError()->push(Rb_Error::ERROR,
			array('name'=>$to_document->getName()),
                                      'unable to create Document %name%');
			return false;
		}

		if($copyFiles){
			//Create version of docfiles
			foreach($this->getDocfiles('noAssoc') as $from_docfile){
				if($from_docfile->getProperty('file_life_stage') > 1) continue;
				$to_docfile = Rb_Docfile_Version::create($from_docfile,
															$from_docfile->getFsdata(),
															$to_container);
				if(!$to_docfile) continue;
				$to_docfile->setFather($to_document);
				$to_docfile->save();
			} //End of foreach
		}

		if($this->_dao->getAdo()->CompleteTrans() === false){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('name'=>$this->getNumber()),
                                  'can not create Document version from %name%');
			return false;
		}

		//Notify event to observers
		$this->notify_all('doc_post_createVersion', $this);

		//Write history
		$this->writeHistory('createVersion');
		$to_document->writeHistory('createVersion');

		return $to_document;
	}//End of method

	//-------------------------------------------------------
	/** Copy all associated files of the Document to directory $target_dir
	 * Return true or false
	 *
	 * @param string	$target_dir, path to directory where to copy files.
	 * @param boolean	$replace true if you want replace the file in the target directory.
	 * @param integer	$mode mode for files copies
	 * @return boolean
	 */
	public function copyAssociatedFiles($target_dir , $replace=false, $mode=0755){
		if( $this->_id <= 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}
		//get the linked docfiles
		if( !$this->getDocfiles() ){
			Ranchbe::getError()->push(Rb_Error::NOTICE, array('element'=>$document_id),
                                              'unable to list associated files');
			return false;
		}
		foreach( $this->getDocfiles() as $docfile ){
			$docfile->copyFile($target_dir, $replace, $mode);
		}
	}//End of method

	//---------------------------------------------------------------------------
	/** Remove a Document
	 * This method suppress the record of the Document
	 * and suppress files and iterations files.
	 * If versions exists, suppress the current version and restore the previous version.
	 * 
	 * (non-PHPdoc)
	 * @see library/Rb/Object/Rb_Object_Permanent#suppress()
	 */
	public function suppress(){
		if( !$this->_id ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}

		//check if access is free
		$access = $this->checkAccess();
		if(!($access==0 || $access==12)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(
						'element'=>$this->getNumber(), 
						'element1'=>$access), 
		      'Document %element% is locked with code %element1%, suppress is not permit'
		      );
	      	return false;
		}

		//Notiy observers on event
		$this->notify_all('doc_pre_suppress', $this);
		if( $this->stopIt ) { //use by observers to control execution
			return false;
		}

		//@todo: Delete record of all process instance
		//@todo: Delete or archive history

		//Delete files of current iteration and old iterations
		foreach($this->getDocfiles('noassoc') as $docfile){
			if( !$docfile->removeFile(true, true) )
			Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$docfile->getNumber() ),
                                              'failed to remove file %element%');
		}

		//Start transaction
		$this->_dao->getAdo()->StartTrans(); //Start a transaction

		//Delete object_permanent
		$this->_suppressPermanentObject();

		//Delete record of Document
		if(!$this->_dao->suppress($this->_id)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('query'=>$query,
                                                            'debug'=>array()),
                              'Suppress of the Document record data is failed');
			$this->_dao->getAdo()->CompleteTrans(false);
			return false;
		}else{
			$this->_dao->getAdo()->CompleteTrans(true);
			Ranchbe::getError()->info('Document %Document% has been removed', array('Document'=>$this->getProperty('normalized_name') ));
		}

		if($this->_dao->getAdo()->CompleteTrans() === false){
			Ranchbe::getError()->errorDb($this->_dao->getAdo()->ErrorMsg());
			return false;
		}

		//Write history
		$this->writeHistory('removeDocument');

		//Notiy observers on event
		$this->notify_all('doc_post_suppress', $this);

		unset(self::$_registry[$this->space->getName()][$this->_id]); //destroy current object in registry

		return true;
	}//End of method

	//---------------------------------------------------------------------------
	/** Link a instance of a process to the Document
	 * Return true or false.
	 *
	 * @param integer $instance_id, id of the workflow instance
	 * @param boolean $save, If true, save in database.
	 * @return boolean
	 */
	public function setInstance($instance_id, $save=true){
		if( $this->_id < 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}
		$this->setProperty('instance_id', $instance_id);
		if($save){
			if( $this->_dao->update(array('instance_id'=>$instance_id), $this->_id) ){
				return true;
			}else return false;
		}
		return true;
	}//End of method

	//---------------------------------------------------------------------------
	/** Unlink a instance of a process from the Document
	 * Return true or false.
	 *
	 * Use when instance is terminate. If a terminate instance is linked to a Document
	 * is not possible to create and link a new instance to this Document
	 * 
	 * @param boolean $save, If true, save in database.
	 * @return boolean
	 */
	public function unsetInstance($save=true){
		return $this->setInstance(null, $save);
	}//End of method

	//-------------------------------------------------------
	/** View the Document file.
	 *
	 * This method send the content of the $file to the client browser.
	 * It send too the mimeType to help the browser
	 *   to choose the right application to use for open the file.
	 * If the mimetype = no_read, display a error.
	 * 
	 * @return boolean
	 * 
	 */
	public function viewDocument(){
		if( !$this->_id ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}
		//Notify event to observers
		$this->_initObservers();
		$this->notify_all('doc_pre_view', $this);

		$viewer = new Rb_Document_Viewer($this);
		if(!$viewer->pushVisu())
		if(!$viewer->pushPicture())
		if(!$viewer->push()){
			Ranchbe::getError()->push(Rb_Error::WARNING, array(),
			tra('There is no associated file to display'));
			return false;
		}

	}//End of method

	//-------------------------------------------------------
	/** Copy associated files to the wildspace to consult it.
	 * 
	 * @return boolean
	 *
	 */
	public function putInWildspace(){
		foreach($this->getDocfiles() as $docfile){
			$docfile->putFileInWildspace();
		}
	}//End of method

	//-------------------------------------------------------------------------
	/** Try to get the default process linked to Document else process linked
	 *         to container and else the process of the context
	 * Return the process_id else return false
	 * 
	 * @return integer
	 *
	 */
	public function getProcessId(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(),
                                                '$this->_id is not set');
			return false;
		}
		if( !$this->getFather() ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(),
                                                '$this->container is not set');
			return false;
		}

		//Check if there are a default process linked to Document
		$pid = $this->getProperty('default_process_id');
		if(!empty($pid)) return $pid;

		//Check if there are a default process linked to container/doctype
		//$pid = $this->container->getDoctypesProcess($this->getProperty('doctype_id'), true);
		$dao = new Rb_Dao( Ranchbe::getDb() );
		$dao->setTable('objects_rel');
		$params = array();
		$params['select'][] = 'ext_id';
		$params['exact_find']['objects_rel.parent_id'] = $this->getProperty('container_id');
		$params['exact_find']['objects_rel.child_id'] = $this->getProperty('doctype_id');
		$pid = $dao->getOne($params);
		if(!empty($pid)) return $pid;

		//Check if there are a default process linked to container
		$pid = $this->container->getProperty('default_process_id');
		if(!empty($pid)) return $pid;

		//Check if there are a default process linked to project
		$pp = Ranchbe::getContext()->getProperty('project_properties');
		$pid = $pp['default_process_id'];
		if(!empty($pid)) return $pid;

		return false;

	}//End of method

	//----------------------------------------------------------
	/** Get all metadatas of the Document from Document, container,
	 *         doctype, category
	 * Return a array with properties of the metadatas
	 * 
	 * @param array 	,see Rb_Basic::getQueryOptions()
	 * @return array
	 */
	public function getMetadatas($params=array()){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), '$this->_id is not set');
			return false;
		}

		return Rb_Metadata_Dictionary::get()->getMetadatas($this->getDao(), $params);
		/*
		 if( $this->metadatas ) return $this->metadatas;
		 $dao = new Rb_Dao( Ranchbe::getDb() );
		 $dao->setTable('objects_rel', 'object');
		 $dao->setKey('link_id', 'primary_key');
		 $params['with'][] = array(
		 'table'=> 'objects',
		 'col1'=> 'child_id',
		 'col2'=> 'object_id',
		 );

		 $params['with'][] = array(
		 'table'=> 'metadata_dictionary',
		 'col1'=> 'child_id',
		 'col2'=> 'property_id',
		 );

		 $params['where'][] = 'objects_rel.parent_id = '.$this->_id;
		 if($this->getProperty('category_id'))
		 $params['where'][] = 'objects_rel.parent_id = '.$this->getProperty('category_id');
		 if($this->getProperty('container_id'))
		 $params['where'][] = 'objects_rel.parent_id = '.$this->getProperty('container_id');

		 $this->metadatas = $dao->getAll($params);

		 if(is_array($this->metadatas)) return $this->metadatas;
		 else return $this->metadatas = array();
		 */
	}//End of method

}//End of class
