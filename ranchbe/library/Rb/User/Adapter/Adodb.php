<?php
//http://my.opera.com/zomg/blog/2007/04/30/using-Adodb-with-zend-auth-and-zend-auth-adapter


/*
 CREATE TABLE `rb_users` (
 `auth_user_id` varchar(32) NOT NULL,
 `handle` varchar(128) NOT NULL,
 `passwd` varchar(32) default NULL,
 `email` varchar(128) default NULL,
 `lastlogin` datetime default '1970-01-01 00:00:00',
 `is_active` tinyint(1) default '1',
 PRIMARY KEY  (`auth_user_id`),
 UNIQUE KEY `handle` (`handle`),
 UNIQUE KEY `auth_user_id_idx` (`auth_user_id`)
 ) ENGINE=InnoDB;

 CREATE TABLE `rb_groupusers` (
 `auth_user_id` int(11) NOT NULL,
 `group_id` int(11) NOT NULL,
 UNIQUE KEY `rb_groupusers_uniq1` (`auth_user_id`,`group_id`),
 KEY `INDEX_rb_groupusers_1` (`auth_user_id`),
 KEY `INDEX_rb_groupusers_2` (`group_id`)
 ) ENGINE=InnoDB ;

 ALTER TABLE `rb_groupusers`
 ADD CONSTRAINT `FK_rb_groupusers_1` FOREIGN KEY (`auth_user_id`)
 REFERENCES `rb_users` (`auth_user_id`) ON DELETE CASCADE,
 ADD CONSTRAINT `FK_rb_groupusers_2` FOREIGN KEY (`group_id`)
 REFERENCES `rb_groups` (`group_id`) ON DELETE CASCADE;

 */

//require_once('Zend/Auth/Adapter/Interface.php');
//require_once('core/user/adapter/interface.php');
//require_once('core/dao/abstract.php');

class Rb_User_Adapter_Adodb extends Rb_Dao_Abstract implements Zend_Auth_Adapter_Interface , Rb_User_Adapter_Interface{
	protected $dbranchbe;
	protected $OBJECT_TABLE      = 'rb_users';
	protected $FIELDS_MAP_ID     = 'auth_user_id';
	protected $FIELDS_MAP_NUM    = 'handle';
	protected $FIELDS_MAP_PASSWD = 'passwd';
	protected $SEQ_NAME          = 'roles_seq';
	protected $MIN_SEQ_NUMBER = 100;
	protected $_name='';
	protected $_password='';
	protected $_user_id=0;
	protected $_email='';
	//----------------------------------------------------------------------------
	public function __construct(ADOConnection &$db){
		$this->dbranchbe =& $db;
	} //End of method

	//----------------------------------------------------------------------------
	/* Set user identification
	 */
	public function setIdentity($name){
		$this->_name = $name;
		return $this;
	} //End of method

	//----------------------------------------------------------------------------
	/* Set user password
	 */
	public function setPassword($pass){
		$this->_password = $pass;
		return $this;
	} //End of method

	//----------------------------------------------------------------------------
	/* Check if current user is authorize to connect to this application
	 * Return object Zend_Auth_Result
	 */
	public function authenticate(){
		$query = 'SELECT '.$this->FIELDS_MAP_NUM.', '.$this->FIELDS_MAP_ID.', is_active, email '.'
              FROM '.$this->OBJECT_TABLE.'
              WHERE '.$this->FIELDS_MAP_NUM.' = ? AND '.$this->FIELDS_MAP_PASSWD.' = ?';

		$result = $this->dbranchbe->getAll($query, array($this->_name, md5($this->_password) ) );
		$authResult = array(
                   'code' => Zend_Auth_Result::FAILURE_UNCATEGORIZED,
                   'identity' => $this->_name,
                   'messages' => array()
		);
		$rows = count($result);
		if($rows == 0){
			$authResult['code'] = Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;
			$authResult['messages'][] = tra('Check your password and your username');
		}
		else if($rows > 1){
			$authResult['code'] = Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS;
			$authResult['messages'][] = tra('Login failed');
		}
		else if($result[0]['is_active'] < 1 ){
			$authResult['code'] = Zend_Auth_Result::FAILURE_UNCATEGORIZED;
			$authResult['messages'][] = tra('User is disabled');
		}
		else{
			$authResult['code'] = Zend_Auth_Result::SUCCESS;
			$authResult['messages'][] = tra('Login succesful');
			$this->_saveLastLogin( $result[0]['auth_user_id'], time() );
		}

		$this->_user_id = $result[0][$this->FIELDS_MAP_ID];
		$this->_email = $result[0]['email'];

		return new Zend_Auth_Result($authResult['code'],$authResult['identity'],
		$authResult['messages']);

	} //End of method

	//----------------------------------------------------------------------------
	/* Get user id.
	 */
	public function getId()
	{
		return $this->_user_id;
	}

	//----------------------------------------------------------------------------
	/* Get user mail.
	 */
	public function getEmail()
	{
		return $this->_email;
	}

	//----------------------------------------------------------------------------
	/* Get all users.
	 * Return associative array where key = user_id
	 */
	public function getUsers(){
		$params['select'] = array($this->FIELDS_MAP_ID, $this->FIELDS_MAP_NUM,
                        'is_active', 'email');
		$params['getRs'] = true;
		return $this->getAllBasic($params)->getAssoc();
	}//End of method

	//----------------------------------------------------------------------------
	/* Get infos about user.
	 * Return array
	 */
	public function getInfos($user_id){
		$params['select'] = array($this->FIELDS_MAP_ID, $this->FIELDS_MAP_NUM, $this->FIELDS_MAP_PASSWD,
                        'is_active', 'email');
		$params['exact_find'][$this->FIELDS_MAP_ID] = $user_id;
		$params['getRs'] = true;
		return $this->getAllBasic($params)->fetchRow(); //return just one row
	}//End of method

	//----------------------------------------------------------------------------
	/* Get groups of this user
	 * Return associative array where key = group_id
	 */
	public function getGroups( $user_id ){
		$query = 'SELECT auth_user_id AS user_id, group_id FROM rb_groupusers
              WHERE auth_user_id = '.$user_id;
		$rs = $this->dbranchbe->execute( $query );

		if($rs){
			$groups = array();
			while (!$rs->EOF) { //transform result to return array
				$groups[$rs->fields['group_id']] = $rs->fields['group_id'];
				$rs->MoveNext();
			}
			return $groups;
		}else{
			Ranchbe::getErrorstack()->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg() );
			return false;
		}
	}//End of method

	//----------------------------------------------------------------------------
	/* Save $user in database
	 *  If user is yet existing, update it, else create it
	 * Return false or id of new user
	 */
	public function save(Rb_User &$user){
		$data[$this->FIELDS_MAP_NUM]      = $user->getUsername();
		$data['email']                    = $user->getProperty('email');
		$data['is_active']                = $user->getProperty('is_active');
		$data[$this->FIELDS_MAP_PASSWD]   = $user->getProperty('passwd');

		if( !$user->getId() ){
			return $this->_basicCreate($data);
		}
		else
		return $this->_basicUpdate( $data, $user->getId() );
	}//End of method

	//----------------------------------------------------------------------------
	protected function _saveLastLogin( $user_id, $time ){
		$data['lastlogin']                = $time;
		return $this->_basicUpdate( $data, $user_id );
	}//End of method

	//----------------------------------------------------------------------------
	/* Save groups association from user $user
	 * Return false or true
	 */
	public function saveGroups(Rb_User &$user){
		$this->dbranchbe->StartTrans();

		if(!$this->_suppressGroups($user->getId()) ){ //before clean all groups
			$this->dbranchbe->CompleteTrans(false);
			return false;
		}

		$query = "INSERT INTO rb_groupusers (
                                      `auth_user_id` ,
                                      `group_id`
                                      )";
		$i=0;
		foreach( $user->getGroups() as $group_id=>$group_properties){ //Generate insert
			if ($i == 0) $query .= ' VALUES ';
			if ($i > 0) $query .= ',';
			$query .= "('".$user->getId()."', '$group_id')";
			$i++;
		}
		if($i == 0) return $this->dbranchbe->CompleteTrans();

		if ( !$this->dbranchbe->Execute( $query ) ){
			Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
			$this->dbranchbe->FailTrans();
		}

		if(!$this->dbranchbe->CompleteTrans()){
			return false;
		}

		return true;

	}//End of method

	//----------------------------------------------------------------------------
	/* Suppress all rules of ressource_id for role_id
	 */
	protected function _suppressGroups($user_id){
		if( !$user_id ) return false;
		$query = 'DELETE FROM rb_groupusers WHERE auth_user_id = \''.$user_id.'\'';
		//Execute the query
		if(!$this->dbranchbe->Execute($query)){
			Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
			$this->dbranchbe->FailTrans();
			return false;
		}
		return true;
	} //End of method

	//----------------------------------------------------------------------------
	/* Suppress user
	 */
	public function suppress($id){
		return $this->_basicSuppress($id);
	}//End of method

	//----------------------------------------------------------------
	/* To get user_id from a user name
	 */
	public function getUserIdFromName($handle) {

		$query = "SELECT auth_user_id FROM $this->OBJECT_TABLE WHERE
		$this->OBJECT_TABLE.handle = '$handle'
              ";

		if(!$id = $this->dbranchbe->GetOne($query)){
			Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
			return false;
		}

		return $id;

	} //End of method

} //End of class
