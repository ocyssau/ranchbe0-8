<?php

//require_once('Zend/Auth/Adapter/Ldap.php');

class Rb_User_Adapter_Ldap extends Zend_Auth_Adapter_Ldap implements Rb_User_Adapter_Interface{
	/**
	 * 
	 * @var integer
	 */
	protected $_user_id = 0;
	
	/**
	 * 
	 * @var string
	 */
	protected $_email='';

	//----------------------------------------------------------------------------
	/* Get the user identification
	 */
	public function getId(){
		return $this->_user_id;
	}

	//----------------------------------------------------------------------------
	/* Get the user mail
	 */
	public function getEmail(){
		return $this->_email;
	}

	//----------------------------------------------------------------------------
	/* Get all users.
	 * Return associative array where key = user_id
	 */
	public function getUsers(){
		return array();
	}//End of method

	//----------------------------------------------------------------------------
	/* Get infos about user.
	 * Return array
	 */
	public function getInfos($user_id){
		return array();
	}//End of method

	//----------------------------------------------------------------------------
	/* get groups of this user
	 * Return associative array where key = group_id
	 */
	public function getGroups( $user_id ){
		return array();
	}//End of method

	//----------------------------------------------------------------------------
	/* Save $user in Ldap reposit
	 *  If user is yet existing, update it, else create it
	 * Return false or id of new user
	 */
	public function save(Rb_User &$user){
		return $this->_user_id;
	}//End of method

	//----------------------------------------------------------------------------
	/* Save groups association from user $user
	 * Return false or true
	 */
	public function saveGroups(Rb_User &$user){
	}//End of method

	//----------------------------------------------------------------------------
	/* Suppress user id = $id
	 * Return false or true
	 */
	public function suppress($id){
	}//End of method

	//----------------------------------------------------------------
	// To get user_id from a user name
	public function getUserIdFromName($handle) {
	} //End of method
	
} //End of class
