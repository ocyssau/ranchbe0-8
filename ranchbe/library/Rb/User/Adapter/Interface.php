<?php

interface Rb_User_Adapter_Interface{

  //----------------------------------------------------------------------------
  /* set the user name
  */
  public function setIdentity($name);

  //----------------------------------------------------------------------------
  /* set the password
  */
  public function setPassword($pass);

  //----------------------------------------------------------------------------
  /* get the user id
  */
  public function getId();

  //----------------------------------------------------------------------------
  /* get the email
  */
  public function getEmail();

  //----------------------------------------------------------------------------
  /* get all users.
  */
  public function getUsers();

  //----------------------------------------------------------------------------
  /* get infos all properties of the user
  */
  public function getInfos($user_id);

  //----------------------------------------------------------------------------
  /* get groups of this user
  */
  public function getGroups( $user_id );

  //----------------------------------------------------------------------------
  /* save user
  */
  public function save(Rb_User &$user);

  //----------------------------------------------------------------------------
  /* save user groups
  */
  public function saveGroups(Rb_User &$user);

  //----------------------------------------------------------------------------
  /* suppress the user
  */
  public function suppress($id);

  //----------------------------------------------------------------
  /* To get user_id from a user name
  *  Return integer
  */
  public function getUserIdFromName($handle);


} //End of class
