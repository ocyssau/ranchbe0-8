<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


/*
 CREATE TABLE `user_prefs` (
 `user_id` int(11) NOT NULL,
 `css_sheet` varchar(32)  NOT NULL default 'default',
 `lang` varchar(32)  NOT NULL default 'default',
 `long_date_format` varchar(32)  NOT NULL default 'default',
 `short_date_format` varchar(32)  NOT NULL default 'default',
 `date_input_method` varchar(16)  NOT NULL default 'default',
 `hour_format` varchar(32)  NOT NULL default 'default',
 `time_zone` varchar(32)  NOT NULL default 'default',
 `wildspace_path` varchar(128)  NOT NULL default 'default',
 `max_record` varchar(128)  NOT NULL default 'default',
 PRIMARY KEY  (`user_id`)
 ) ENGINE=InnoDB;


 ALTER TABLE `user_prefs`
 ADD CONSTRAINT `FK_user_prefs_1` FOREIGN KEY (`user_id`)
 REFERENCES `rb_users` (`auth_user_id`) ON DELETE CASCADE;

 */

//require_once('core/dao/abstract.php');


/** Manage user Preferences
 *
 */
class Rb_User_Preferences extends Rb_Dao_Abstract{

	protected $SPACE_NAME   = 'user_prefs';
	protected $OBJECT_TABLE  = 'user_prefs';
	protected $FIELDS_MAP_ID  = 'user_id';
	protected $TABLE_SEQ      = 'user_prefs_seq';

	/**
	 *
	 * @var unknown_type
	 */
	protected $dbranchbe; //object

	/**
	 *
	 * @var integer
	 */
	protected $usr_id = 0;

	/** Array Preferences of the current user
	 * 
	 * @var array
	 */
	protected $user_prefs   = array();
	
	/** Array of default Preferences
	 * 
	 * @var array
	 */
	protected $default_prefs = array();
	
	/** Array result Preferences to apply
	 * 
	 * @var array
	 */
	protected $preferences  = array(); 

	/** true to get prefs from user profile, else set always to default values
	 * 
	 * @var boolean
	 */
	protected $allowUserPrefs = true;

	/**
	 * @var array
	 */
	protected $parameters=array();
	

	/**
	 * 
	 * @param integer $user_id
	 */
	public function __construct($user_id = 0){
		$this->dbranchbe =& Ranchbe::getDb();
		$this->user_id = $user_id;
		$this->error_stack =& Ranchbe::getError();
		$this->default_prefs = array(
                          'css_sheet' => '',
                          'lang' => '',
                          'long_date_format' => '',
                          'short_date_format' => '',
                          'date_input_method' => '',
                          'time_zone' => '',
                          'max_record' => 50,
		);
	}//End of method

	//----------------------------------------------------------
	/**
	*
	*
	* @param boolean $bool
	* @return void
	*/
	public function setAllowUserPrefs($bool){
		$this->allowUserPrefs = $bool;
	}//End of method

	//----------------------------------------------------------
	/** Set the preference value from his name
	*
	* @param string
	* @param mixed
	* @return string
	*/
	public function setPreference($preference_name, $preference_value){
		return $this->user_prefs[$preference_name] = $preference_value;
	}//End of method

	//----------------------------------------------------------
	/** Get all Preferences of current user
	*
	* @return array
	*/
	public function getUserPreferences(){
		if( $this->user_id == 0 ) return $this->default_prefs;
		if( !empty($this->user_prefs) )
		return $this->user_prefs;

		$params = array(); //init var
		$params['select'] = array_keys($this->default_prefs);
		$params['exact_find'] = array($this->FIELDS_MAP_ID => $this->user_id);

		$res = $this->_dbget($this->OBJECT_TABLE , $params , 'row' , false);

		if($res === false){
			return false;
		}else if(!$res){ //none records, return array fill with 'default' for each parameters
			return $this->user_prefs = array_combine(array_keys($this->default_prefs),
			array_fill(0, count($this->default_prefs), 'default' ) );
		}else{
			return $this->user_prefs = $res;
		}
	}//End of method

	//----------------------------------------------------------
	/** init or re-init object
	*
	* @param integer
	* @return void
	*/
	public function init($user_id){
		if(isset($this->default_prefs)) unset($this->default_prefs);
		if(isset($this->user_prefs)) unset($this->user_prefs);
		$this->usr_id =& $user_id;
		$this->getUserPreferences();
	}//End of method

	//----------------------------------------------------------
	/** Save object in database
	*  Create object if do not exist
	*  
	* @return boolean | integer
	*  
	*/
	public function save(){
		if( $this->user_id == 0 ) return false;
		//Test if object exist
		$query = 'SELECT '.$this->FIELDS_MAP_ID.' FROM '.$this->OBJECT_TABLE.' WHERE '.$this->FIELDS_MAP_ID.' = \''.$this->user_id.'\'';
		$one = $this->dbranchbe->GetOne($query);

		if( empty($one) ){ //its a new pref definition
			$this->user_prefs[$this->FIELDS_MAP_ID] = $this->user_id;
			if (!$this->dbranchbe->AutoExecute( $this->OBJECT_TABLE , $this->user_prefs , 'INSERT')){
				$this->error_stack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
				return false;
			}
		}else{ //just update pref
			$data = $this->user_prefs;
			unset($data[$this->FIELDS_MAP_ID]); //to prevent malicious effects
			$this->_basicUpdate( $this->user_prefs , $this->user_id );
		}
	}//End of method

} //End of class
