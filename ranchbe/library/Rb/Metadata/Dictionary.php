<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
 CREATE TABLE `metadata_dictionary` (
 `property_id` int(11) NOT NULL,
 `property_fieldname` varchar(32) NOT NULL default '',
 `property_name` varchar(32) NOT NULL default '',
 `property_description` varchar(128) NOT NULL default '',
 `property_type` varchar(32) NOT NULL default '',
 `property_length` int(11) default NULL,
 `extend_table` varchar(64) NOT NULL,
 `regex` varchar(255) default NULL,
 `return_name` int(1) NOT NULL default '0',
 `is_required` int(1) NOT NULL default '0',
 `is_multiple` int(1) NOT NULL default '0',
 `is_hide` int(1) NOT NULL default '0',
 `select_list` varchar(255) default NULL,
 `selectdb_where` varchar(255) default NULL,
 `selectdb_table` varchar(32) default NULL,
 `selectdb_field_for_value` varchar(32) default NULL,
 `selectdb_field_for_display` varchar(32) default NULL,
 `date_format` varchar(32) default NULL,
 `display_both` tinyint(1) NOT NULL default 0,
 PRIMARY KEY (`property_id`),
 UNIQUE KEY `UNIQ_metadata_dictionary_1` (`property_name`,`extend_table`),
 KEY `INDEX_metadata_dictionary_1` (`extend_table`),
 KEY `INDEX_metadata_dictionary_2` (`property_description`)
 ) ENGINE=InnoDB;
 */

//require_once('core/object/permanent.php');

/*! \brief This class manage metadatas definition in extend tables.
 */
class Rb_Metadata_Dictionary extends Rb_Object_Permanent{

	protected $core_props = array(
                                'property_name'=>'',
                                'property_fieldname'=>'',
                                'property_description'=>'',
                                'property_type'=>'',
                                'property_length'=>0,
                                'extend_table'=>'',
                                'regex'=>'',
                                'return_name'=>0,
                                'is_required'=>0,
                                'is_multiple'=>0,
                                'is_hide'=>0,
                                'select_list'=>'',
                                'selectdb_where'=>'',
                                'selectdb_table'=>'',
                                'selectdb_field_for_value'=>'',
                                'selectdb_field_for_display'=>'',
                                'date_format'=>'',
                                'display_both'=>0,
	);

	protected $valid_infos = array();

	//List of type of fields that can be set in metadatas
	public $accept_property_type = array('text',
                                    'long_text',
                                    'html_area',
                                    'partner',
                                    'doctype',
                                    'user',
                                    'indice',
                                    'process',
                                    'date',
                                    'integer',
                                    'decimal',
                                    'select',
                                    'selectFromDB',
                                    'adv_select',
                                    'liveSearch',
	);

	protected static $_registry = array(); //(array) registry of constructed objects

	//--------------------------------------------------------------------
	/*! \brief init the property for the metadataManager object.
	 *
	 */
	function __construct($property_id = 0){
		$this->_dao = new Rb_Dao( Ranchbe::getDb() );
		$this->_dao->setTable('metadata_dictionary', 'object');
		$this->_dao->setKey('property_id', 'primary_key');
		$this->_dao->setKey('property_fieldname', 'number');
		$this->_dao->setKey('property_name', 'name');
		$this->_dao->setKey('property_description', 'description');
		return $this->_init($property_id);
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief init and clean current registry
	 */
	public static function initRegistry(){
		self::$_registry = array();
	}

	//-------------------------------------------------------------------------
	/*! \brief return a instance of object.
	 *   if none parameter return a special instance with no possibilies to change here properties
	 *   and wich can not be saved. If object "$id" dont exist return false.
	 *   If object "$id" has already been instantiated, then return this instance (singleton pattern).
	 *
	 */
	public static function get($id = -1){
		if($id < 0) $id = -1; //forced to value -1
		if($id == 0)
		return new self(0);

		if( !self::$_registry[$id] ){
			self::$_registry[$id] = new self($id);
		}
		return self::$_registry[$id];
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief set extend table to use
	 * can not call this method from instance where id > 0
	 */
	public function setExtendTable($table_name){
		return $this->setProperty('extend_table',$table_name);
	}

	//-------------------------------------------------------------------------
	/*! \brief return value of property
	 * can not call this method from instance where id < 0
	 */
	public function getProperty($property_name){
		switch($property_name){
			case 'name':
				$property_name = 'property_name';
				break;
			case 'number':
			case 'fieldname':
				$property_name = 'property_fieldname';
				break;
			case 'description':
				$property_name = 'property_description';
				break;
			case 'type':
				$property_name = 'property_type';
				break;
			case 'length':
				$property_name = 'property_length';
				break;
		}
		if( array_key_exists($property_name, $this->core_props) )
			return $this->core_props[$property_name];
	}

	//-------------------------------------------------------------------------
	/*! \brief set the property $property_name with value of $property_value
	 * can not call this method from instance where id < 0.
	 * Return $this, so you can use sythax type $object->setProperty($property_name, $property_value)->getProperty($property_name);
	 *
	 */
	public function setProperty($property_name, $property_value){
		if($this->getId() < 0) return false; //Can not change property on object -1
		$this->isSaved = false;
		switch($property_name){
			case 'property_id':
				return false;
				break;
			case 'name':
				$property_name = 'property_name';
			case 'property_name':
				break;
			case 'number':
			case 'fieldname':
			case 'property_fieldname':
				if($this->_id > 0) return false; //can not change the name
				$property_name = 'property_fieldname';
				break;
			case 'description':
				$property_name = 'property_description';
			case 'property_description':
				break;
			case 'property_type':
			case 'type':
				$this->setType($property_value);
				break;
			case 'db_type':
				$property_value = (string) $property_value;
				break;
			case 'length':
				$property_name = 'property_length';
			case 'property_length':
				$property_value = (int) $property_value;
				break;
			case 'extend_table':
				if($this->_id > 0) return false; //can not change the table
				break;
			case 'regex':
				break;
			case 'return_name':
				$property_value = (int) $property_value;
				break;
			case 'is_required':
				$property_value = (int) $property_value;
				break;
			case 'is_multiple':
				$property_value = (int) $property_value;
				break;
			case 'is_hide':
				$property_value = (int) $property_value;
				break;
			case 'select_list':
				break;
			case 'selectdb_where':
				break;
			case 'selectdb_table':
				break;
			case 'selectdb_field_for_value':
				break;
			case 'selectdb_field_for_display':
				break;
			case 'date_format':
				break;
			case 'display_both':
				$property_value = (int) $property_value;
				break;
			default:
				return false;
				break;
		}
		$this->core_props[$property_name] = $property_value;
		return $this;
	}

	//--------------------------------------------------------------------
	/*! \brief Set the type of field.
	 *
	 * This method return the type of field to create in the table of the component
	 <br />return :<br />
	 $this->property_type(string)   For set the type of field in database. See type list in the comments.
	 <br />
	 List of valid type :<br />
	 C:  Varchar, capped to 255 characters.<br />
	 X:  Larger varchar, capped to 4000 characters (to be compatible with Oracle). <br />
	 XL: For Oracle, returns CLOB, otherwise the largest varchar size.<br />
	 <br />
	 C2: Multibyte varchar<br />
	 X2: Multibyte varchar (largest size)<br />
	 <br />
	 B:  BLOB (binary large object)<br />
	 <br />
	 D:  Date (some databases do not support this, and we return a datetime type)<br />
	 T:  Datetime or Timestamp<br />
	 L:  Integer field suitable for storing booleans (0 or 1)<br />
	 I:  Integer (mapped to I4)<br />
	 I1: 1-byte integer<br />
	 I2: 2-byte integer<br />
	 I4: 4-byte integer<br />
	 I8: 8-byte integer<br />
	 F:  Floating point number<br />
	 N:  Numeric or decimal number<br />
	 <br />
	 $this->property_length(integer)   Size of the field varchar or string.<br />
	 $this->valid_infos(array)<br /> list of infos to set when you create the metadata.

	 *
	 * @param string $type
	 * @return void
	 */
	public function setType($type){
		switch ( $type ) {
			case 'text':
			case 'long_text':
				$this->setProperty('db_type','C');
				$this->setProperty('property_length',255);
				$this->valid_infos = array('property_length','regex','is_required');
				break;
			case 'html_area':
				$this->setProperty('db_type','C');
				$this->setProperty('property_length',255);
				$this->valid_infos = array('property_length','is_required');
				break;
			case 'partner':
				$this->setProperty('db_type','C');
				$this->setProperty('property_length',32);
				$this->valid_infos = array('property_length','is_required',
                                    'is_multiple','return_name','selectdb_where');
				break;
			case 'doctype':
				$this->setProperty('db_type','C');
				$this->setProperty('property_length',32);
				$this->valid_infos = array('property_length','is_required','is_multiple',
                                   'return_name','selectdb_where');
				break;
			case 'indice':
				$this->setProperty('db_type','C');
				$this->setProperty('property_length',16);
				$this->valid_infos = array('property_length','is_required','is_multiple',
                                   'return_name','selectdb_where');
				break;
			case 'user':
				$this->setProperty('db_type','C');
				$this->setProperty('property_length',32);
				$this->valid_infos = array('property_length','is_required','is_multiple',
                                   'return_name','selectdb_where');
				break;
			case 'process':
				$this->setProperty('db_type','C');
				$this->setProperty('property_length',80);
				$this->valid_infos = array('property_length','is_required','is_multiple',
                                   'return_name','selectdb_where');
				break;
			case 'category':
				$this->setProperty('db_type','C');
				$this->setProperty('property_length',32);
				$this->valid_infos = array('property_length','is_required','is_multiple',
                                   'return_name','selectdb_where');
				break;
			case 'date':
				$this->setProperty('db_type','I');
				$this->valid_infos = array('is_required');
				break;
			case 'integer':
				$this->setProperty('db_type','I');
				$this->valid_infos = array('property_length','is_required');
				break;
			case 'decimal':
				$this->setProperty('db_type','F');
				$this->valid_infos = array('property_length','is_required');
				break;
			case 'select':
				$this->setProperty('db_type','C');
				$this->setProperty('property_length',255);
				$this->valid_infos = array('property_length','is_required','is_multiple',
                                   'return_name','select_list');
				break;
			case 'selectFromDB':
				$this->setProperty('db_type','C');
				$this->setProperty('property_length',255);
				$this->valid_infos = array('property_length','is_required','is_multiple',
                                   'return_name','selectdb_table',
                                   'selectdb_field_for_display',
                                   'property_for_display');
				break;
			case 'liveSearch':
				$this->setProperty('db_type','C');
				$this->setProperty('property_length',255);
				$this->valid_infos = array('property_length','is_required','is_multiple',
                                   'return_name','selectdb_table',
                                   'selectdb_field_for_display',
                                   'property_for_display');
				break;
			default:
				$this->setProperty('db_type','C');
				$this->setProperty('property_length',255);
				$this->valid_infos = array('property_length','regex','is_required');
				break;
		} //End of switch
	}//End of method

	//--------------------------------------------------------------------
	/*! \brief Create a metadata.
	 *
	 */
	protected function _create(){
		if( $this->_id !== 0 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}
		if( !$this->getProperty('db_type') ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), 'type is not set');
			return false;
		}
		if( !$this->getProperty('extend_table') ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), 'extend table is not set');
			return false;
		}
		if( !$this->getNumber() ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array(), 'field name is not set');
			return false;
		}
		if( !$this->getName() ){
			$this->setName($this->getNumber());
		}
		//Create a basic object
		if( $this->_id = $this->_dao->create( $this->getProperties() ) ){
			self::$_registry[$this->_id] =& $this;
			$this->isSaved = true;
		}else{
			Ranchbe::getError()->push(Rb_Error::ERROR,
			array('element'=>$this->getName()),
			tra('can not create %element%'));
			$this->_id = 0;
		}

		// -- Create a adodb datadictionnay object
		$dbdict = NewDataDictionary($this->_dao->getAdo());

		// -- Check if the field is not yet existing
		$property_in_table = $this->_dao->getAdo()
		->MetaColumnNames($this->getProperty('extend_table'));
		if(is_array($property_in_table))
		if(in_array($this->getNumber(), $property_in_table)){
			Ranchbe::getError()->push(Rb_Error::ERROR,
			array('number'=>$this->getNumber()),
                                                        'Field %number% exist');
			return false;
		}

		//Create the field in the document table
		$flds = array(
		array($this->getNumber(),
		$this->getProperty('db_type'),
		$this->getProperty('length')),
		);
		$sqlarray = $dbdict->ChangeTableSQL($this->getProperty('extend_table'), $flds);
		if(!$dbdict->ExecuteSQLArray($sqlarray) ) {
			Ranchbe::getError()->errorDb($this->_dao->getAdo()->ErrorMsg());
			return false;
		}
		return true;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Update a metadata
	 *
	 */
	protected function _update(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}
		return $this->_dao->update( $this->getProperties() , $this->_id);
	}//End of method

	//----------------------------------------------------------
	/*! \brief Suppress a metadata.
	 * This method suppress the infos record in []_metadata table and suppress the
	 *   col in the container or document table.
	 * All data records in this column will be lost.
	 */
	public function suppress(){
		if( $this->_id < 1 ){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('id'=>$this->_id),
			tra('can not performed this action on object id %id%'));
			trigger_error('can not performed this action on object id '.$this->_id, E_USER_WARNING);
			return false;
		}

		$this->_dao->getAdo()->StartTrans();
		$ret = $this->_dao->suppress($this->_id);
		if($ret){
			$dbdict = NewDataDictionary($this->_dao->getAdo()); //Create a adodb datadictionnay object
			$sqlarray = $dbdict->DropColumnSQL($this->getProperty('extend_table'),
			$this->getNumber()
			);
			if (!$dbdict->ExecuteSQLArray($sqlarray)){
				Ranchbe::getError()->errorDb($this->_dao->getAdo()->ErrorMsg());
				return $this->_dao->getAdo()->CompleteTrans(false);
			}

			//clean registry
			unset( self::$_registry[$this->_id] );
		}

		if (!$this->_dao->getAdo()->CompleteTrans()){
			return false;
		}else{
			$this->_suppressPermanentObject();
			return true;
		}
		 
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get list of metadatas.
	 *
	 \param $dao(Rb_Dao_Abstract)
	 \param $params(array) is use for manage the display.
	 See parameters function of getQueryOptions()
	 */
	public function getMetadatas(Rb_Dao_Abstract $dao, $params=array()){
		$params['exact_find']['extend_table'] = Rb_Object_Extend::getExtendTable($dao);
		return $this->_dao->getAllBasic($params);
	}//End of method

} //End of class
