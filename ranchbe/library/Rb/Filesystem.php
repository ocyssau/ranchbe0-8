<?php

/*! \brief manipulation of the Filesystem on the server
*
*/
class Rb_Filesystem {
	static $authorized_dir = array ();
	
	static public function setAuthorized($path) {
		$path = (string) $path;
		self::$authorized_dir[] = $path;
	} //End of function

	//----------------------------------------------------------
	/*! \brief This function check if the request file is in a authorized directory
*   Return true if it is authorized else return false
* 
* \param $filepath(string) fullpath to test
*/
	static function limitDir($filepath) {
		//".." , "//" "/./" forbidden in the path
		if (strpos ( "$filepath", '..' )) {
			echo 'no ..<br/>';
			return false;
		}
		if (strpos ( "$filepath", '//' )) {
			echo 'no //<br/>';
			return false;
		}
		if (strpos ( "$filepath", '/./' )) {
			echo 'no /./<br/>';
			return false;
		}
		$filepath = str_replace('\\', '/', $filepath);
		//Check that the path is in a authorized directory
		foreach ( self::$authorized_dir as $motif ) {
			if (empty ( $motif ))
				continue;
			$motif = str_replace('\\', '/', $motif);
			$motif = ltrim ( $motif, './' );
			$filepath = ltrim ( $filepath, './' );
			if (rtrim ( $filepath, '/' ) == rtrim ( $motif, '/' )) {
				echo 'its a sys dir<br/>';
				return false;
			} // Check that the system directories are not directly manipulated
			if (strpos ( $filepath, $motif ) === 0)
				return true; // Check that the file is in a directory of the system
		} //End of foreach
		return false;
	} //End of function
	

	//----------------------------------------------------------
	/*! \brief Generation of uniq identifiant
*/
	static function uniq_id() {
		return md5 ( uniqid ( rand () ) );
	}
	
	//----------------------------------------------------------
	/*! \brief Encode path
*/
	static function encodePath($path) {
		$path = str_replace ( '/', '%2F', $path );
		$path = str_replace ( '.', '%2E', $path );
		$path = str_replace ( ':', '%3A', $path );
		$path = str_replace ( '\\', '%2F', $path );
		return $path;
	}
	
	//----------------------------------------------------------
	/*! \brief Decode path
*/
	static function decodePath($path) {
		$path = str_replace ( '%2F', '/', $path );
		$path = str_replace ( '%2E', '.', $path );
		$path = str_replace ( '%3A', ':', $path );
		return $path;
	}

} //End of class


