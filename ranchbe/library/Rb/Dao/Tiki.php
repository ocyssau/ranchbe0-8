<?php
//
// $Header: /cvsroot/ranchbe/ranchbe0.7/lib/tikidblib.php,v 1.2 2009/06/05 17:06:37 ranchbe Exp $
//

/** This class is only for compatibility with tikiwiki components.
 * It is a rewrite of class TikiDB from tikiwiki application
 *  NOT USE THIS CLASS, ONLY FOR COMPATIBILITY WITH TIKIWIKI COMPONENTS
 *
 * @author See Tikiwiki
 *
 */
abstract class Rb_Dao_Tiki {
	// Database access functions

	/**
	 * 
	 * @var ADOConnection
	 */
	protected $db; // The ADODB db object used to access the database

	//--------------------------------------------------------------------
	/** Use ADOdb->qstr() for 1.8
	 * 
	 * @param string 	$str
	 * @return string
	 */
	public function qstr($str) {
		if (function_exists('mysql_real_escape_string')) {
			return "'" . mysql_real_escape_string($str). "'";
		} else {
			return "'" . mysql_escape_string($str). "'";
		}
	} //End of method


	//--------------------------------------------------------------------
	// Queries the database, *returning* an error if one occurs, rather
	// than exiting while printing the error.
	// -rlpowell
	public function queryError( $query, &$error, $values = null, $numrows = -1,
							$offset = -1 ){
		$numrows = intval($numrows);
		$offset = intval($offset);
		$this->convert_query($query);

		if ($numrows == -1 && $offset == -1)
			$result = $this->db->Execute($query, $values);
		else
			$result = $this->db->SelectLimit($query, $numrows, $offset, $values);

		if (!$result )
		{
			$error = $this->db->ErrorMsg();
			$result=false;
		}

		//count the number of queries made
		global $num_queries;
		$num_queries++;
		//$this->debugger_log($query, $values);
		return $result;
	} //End of method

	//--------------------------------------------------------------------
	/** Queries the database reporting an error if detected
	 * 
	 * @param string $query
	 * @param $values
	 * @param $numrows
	 * @param $offset
	 * @param $reporterrors
	 * @return unknown_type
	 */
	public function query($query, $values = null, $numrows = -1, $offset = -1, $reporterrors = true ){
		$numrows = intval($numrows);
		$offset = intval($offset);
		$this->convert_query($query);

		/*
		 echo "query: $query <br />";
		 echo "<pre>";
		 print_r($values);
		 echo "\n";
		 */

		if ($numrows == -1 && $offset == -1)
			$result = $this->db->Execute($query, $values);
		else
			$result = $this->db->SelectLimit($query, $numrows, $offset, $values);

		//print_r($result);
		//echo "\n</pre>\n";
		if (!$result)
		{
			if ($reporterrors)
			{
				$this->sql_error($query, $values, $result);
			}
		}

		return $result;
	} //End of method

	//--------------------------------------------------------------------
	// Gets one column for the database.
	public function getOne($query, $values = null, $reporterrors = true, $offset = 0) {
		$this->convert_query($query);

		//echo "<pre>";
		//echo "query: $query \n";
		//print_r($values);
		//echo "\n";
		$result = $this->db->SelectLimit($query, 1, $offset, $values);

		//echo "\n</pre>\n";
		if (!$result) {
			if ($reporterrors) {
				$this->sql_error($query, $values, $result);
			} else {
				return $result;
			}
		}

		$res = $result->fetchRow();

		//count the number of queries made
		global $num_queries;
		$num_queries++;
		//$this->debugger_log($query, $values);

		if ($res === false)
		return (NULL); //simulate pears behaviour

		list($key, $value) = each($res);
		return $value;
	} //End of method


	//--------------------------------------------------------------------
	// Reports SQL error from PEAR::db object.
	/*
	function sql_error($query, $values, $result) {
		global $ADODB_LASTDB, $smarty;

		trigger_error($ADODB_LASTDB . " error:  " . $this->db->ErrorMsg(). " in query:<br /><pre>\n" . $query . "\n</pre><br />", E_USER_WARNING);
		// only for debugging.
		//trigger_error($ADODB_LASTDB . " error:  " . $this->db->ErrorMsg(). " in query:<br />" . $query . "<br />", E_USER_WARNING);
		$outp = "<div class='simplebox'><b>".tra("An error occured in a database query!")."</b></div>";
		$outp.= "<br /><table class='form'>";
		$outp.= "<tr class='heading'><td colspan='2'>Context:</td></tr>";
		$outp.= "<tr class='formcolor'><td>File</td><td>".basename($_SERVER['SCRIPT_NAME'])."</td></tr>";
		$outp.= "<tr class='formcolor'><td>Url</td><td>".basename($_SERVER['REQUEST_URI'])."</td></tr>";
		$outp.= "<tr class='heading'><td colspan='2'>Query:</td></tr>";
		$outp.= "<tr class='formcolor'><td colspan='2'><tt>$query</tt></td></tr>";
		$outp.= "<tr class='heading'><td colspan='2'>Values:</td></tr>";
		foreach ($values as $k=>$v) {
			$outp.= "<tr class='formcolor'><td>$k</td><td>$v</td></tr>";
		}
		$outp.= "<tr class='heading'><td colspan='2'>Message:</td></tr><tr class='formcolor'><td>Error Message</td><td>".$this->db->ErrorMsg()."</td></tr>\n";
		$outp.= "</table>";
		//if($result===false) echo "<br>\$result is false";
		//if($result===null) echo "<br>\$result is null";
		//if(empty($result)) echo "<br>\$result is empty";
		if ($smarty) {
			$smarty->assign('msg',$outp);
			$smarty->display("error.tpl");
		} else {
			echo $outp;
		}
		// -- debugging stuff: after php 5.1.1 will disclose db user and password
		// echo "<pre>";
		// var_dump(debug_backtrace());
		// echo "</pre>";
		die;
	} //End of method
	*/

	//--------------------------------------------------------------------
	 /** Reports SQL error
	  *  Rewrite to use error stack of ranchbe
	  * 
	  * @param $query
	  * @param $values not use
	  * @param $result not use
	  * @return void
	  */	
	public function sql_error($query, $values=null, $result=null) {
		Ranchbe::getError()->errorDb( $this->dbranchbe->ErrorMsg (), $query);
	} //End of method
	
	//--------------------------------------------------------------------

	public function ifNull($narg1,$narg2) {
		return $this->db->ifNull($narg1,$narg2);
	} //End of method


	//--------------------------------------------------------------------
	/** Functions to support DB abstraction
	 * 
	 * @param string	$query
	 * @return void
	 */
	public function convert_query(&$query) {
		global $ADODB_LASTDB;
		switch ($ADODB_LASTDB) {
			case "oci8":
				$query = preg_replace("/`/", "\"", $query);

				// convert bind variables - adodb does not do that
				$qe = explode("?", $query);
				$query = '';

				$temp_max = sizeof($qe) - 1;
				for ($i = 0; $i < $temp_max; $i++) {
					$query .= $qe[$i] . ":" . $i;
				}

				$query .= $qe[$i];
				break;

			case "postgres7":
			case "sybase":
				$query = preg_replace("/`/", "\"", $query);

				break;

			case "mssql":
				$query = preg_replace("/`/" , "" , $query);
				$query = preg_replace("/\?/" , "'?'" , $query);
				break;

			case "sqlite":
				$query = preg_replace("/`/", "", $query);
				break;
		}

	} //End of method

	//--------------------------------------------------------------------
	public function blob_encode(&$blob) {
		switch($this->db->blobEncodeType) {
			case 'I':
				$blob=$this->db->BlobEncode($blob);
				break;
			case 'C':
				$blob=$this->db->qstr($this->db->BlobEncode($blob));
				break;
			case 'false':
			default:
		}
	} //End of method

	//--------------------------------------------------------------------
	/** 
	 * 
	 * @param string	$sort_mode
	 * @return string
	 */
	public function convert_sortmode($sort_mode) {
		global $ADODB_LASTDB;

		if ( !$sort_mode ) {
			return '';
		}
		// parse $sort_mode for evil stuff
		$sort_mode = preg_replace('/[^A-Za-z_,]/', '', $sort_mode);
		$sep = strrpos($sort_mode, '_');
		// force ending to either _asc or _desc
		if ( substr($sort_mode, $sep)!=='_asc' ) {
			$sort_mode = substr($sort_mode, 0, $sep) . '_desc';
		}

		switch ($ADODB_LASTDB) {
			case "postgres7":
			case "oci8":
			case "sybase":
			case "mssql":
				// Postgres needs " " around column names
				//preg_replace("#([A-Za-z]+)#","\"\$1\"",$sort_mode);
				$sort_mode = preg_replace("/_asc$/", "\" asc", $sort_mode);
				$sort_mode = preg_replace("/_desc$/", "\" desc", $sort_mode);
				$sort_mode = str_replace(",", "\",\"",$sort_mode);
				$sort_mode = "\"" . $sort_mode;
				break;

			case "sqlite":
				$sort_mode = preg_replace("/_asc$/", " asc", $sort_mode);
				$sort_mode = preg_replace("/_desc$/", " desc", $sort_mode);
				break;

			case "mysql3":
			case "mysql":
			default:
				$sort_mode = preg_replace("/_asc$/", "` asc", $sort_mode);
				$sort_mode = preg_replace("/_desc$/", "` desc", $sort_mode);
				$sort_mode = str_replace(",", "`,`",$sort_mode);
				$sort_mode = "`" . $sort_mode;
				break;
		}

		return $sort_mode;
	} //End of method

	//--------------------------------------------------------------------
	public function convert_binary() {
		global $ADODB_LASTDB;

		switch ($ADODB_LASTDB) {
			case "oci8":
			case "postgres7":
			case "sqlite":
				return;

				break;

			case "mysql3":
			case "mysql":
				return "binary";
				break;
		}
	} //End of method

	//--------------------------------------------------------------------
	public function sql_cast($var,$type) {
		global $ADODB_LASTDB;
		switch ($ADODB_LASTDB) {
			case "sybase":
				switch ($type) {
					case "int":
						return " CONVERT(numeric(14,0),$var) ";
						break;
					case "string":
						return " CONVERT(varchar(255),$var) ";
						break;
					case "float":
						return " CONVERT(numeric(10,5),$var) ";
						break;
				}
				break;
			default:
				return($var);
				break;
		}

	} //End of method

	//--------------------------------------------------------------------
	public function debugger_log($query, $values){
	} //End of method

} //End of class
