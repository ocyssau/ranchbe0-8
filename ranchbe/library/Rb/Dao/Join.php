<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

////require_once('core/dao/abstract.php');

/* Abstract class to defined relation object betwens object
* define 3 groups : LEFT, OBJECT, RIGHT
* The left table is the entry
* Object table is the relation table
* Right table is the linked table
*
*/
abstract class Rb_Dao_Join extends Rb_Dao_Abstract{

protected $link_id = 0;
protected $core_props = array();
protected $OBJECT_TABLE; //relation table name
protected $FIELDS_MAP_ID; //primary key of the relation table
protected $LEFT_OBJECT_TABLE; //left table in the Join
protected $LEFT_FIELDS_MAP_ID; //primary key of left table
protected $RIGHT_OBJECT_TABLE; //right table in the Join
protected $RIGHT_FIELDS_MAP_ID; //primary key of right table
protected $RIGHT_TYPE = 'INNER';
protected $LEFT_TYPE = 'INNER';

//-------------------------------------------------------------------------
public function __construct(ADOConnection &$db){
  $this->dbranchbe =& $db;
}//End of method

//--------------------------------------------------------------------
public function init($link_id){
  $this->link_id = $link_id;
  if(isset($this->core_props)) unset($this->core_props);
  return true;
}//End of method

public function setObjectTable( $table, $primary_key ){
  $this->OBJECT_TABLE=$table;
  $this->FIELDS_MAP_ID=$primary_key;
}

public function setLeftTable( $table, $primary_key ){
  $this->LEFT_OBJECT_TABLE=$table;
  $this->LEFT_FIELDS_MAP_ID=$primary_key;
}

public function setRightTable( $table, $primary_key ){
  $this->RIGHT_OBJECT_TABLE=$table;
  $this->RIGHT_FIELDS_MAP_ID=$primary_key;
}

public function setRightJoinOn( $key ){
  $this->RIGHT_FIELDS_MAP_ID=$key;
}

public function setLeftJoinOn( $key ){
  $this->LEFT_FIELDS_MAP_ID=$key;
}

public function getObjectTable(){
  return $this->OBJECT_TABLE;
}

public function getLeftTable(){
  return $this->LEFT_OBJECT_TABLE;
}

public function getRightTable(){
  return $this->RIGHT_OBJECT_TABLE;
}

//--------------------------------------------------------------------
/*! \brief Add a link
* 
* \param $data(array)
*/
public function addLink( $data = array() ){
  foreach($data as $key=>$val){
    //var_dump($key, $val);
    $this->setProperty($key, $val);
  }
  //var_dump($this->core_props);
  return $this->_basicCreate( $this->core_props );
}//End of method

//----------------------------------------------------------
/*! \brief Update a link
* 
  \param $link_id(integer)
  \param $data(array)
*/
public function modifyLink( $link_id , $data=array() ){
  unset($data[$this->FIELDS_MAP_ID]); //to prevent the change of id
  return $this->_basicUpdate($data , $link_id);
}//End of method

//----------------------------------------------------------
/*! \brief Suppress a link
* 
  \param $link_id(integer)
*/
public function suppressLink( $link_id ){
  return $this->_basicSuppress($link_id);
}//End of method

//----------------------------------------------------------
/*!\brief Get the right and left links from left id
  \param $id(integer)
*/
public function getLinks( $id=0 , $params=array() ){
  $params['with'][0]['type'] = $this->RIGHT_TYPE;
  $params['with'][0]['table'] = $this->RIGHT_OBJECT_TABLE; //Container
  $params['with'][0]['col'] = $this->RIGHT_FIELDS_MAP_ID;

  $params['with'][1]['type'] = $this->LEFT_TYPE;
  $params['with'][1]['table'] = $this->LEFT_OBJECT_TABLE; //Category
  $params['with'][1]['col'] = $this->LEFT_FIELDS_MAP_ID;

  if( !isset($params['select']) )
    $params['select'] = array($this->RIGHT_OBJECT_TABLE.'.'.$this->RIGHT_FIELDS_MAP_ID, 
                              $this->RIGHT_OBJECT_TABLE.'.'.$this->RIGHT_FIELDS_MAP_NUM, 
                              $this->LEFT_OBJECT_TABLE.'.'.$this->LEFT_FIELDS_MAP_ID, 
                              $this->LEFT_OBJECT_TABLE.'.'.$this->LEFT_FIELDS_MAP_NUM);

  if( $id != 0 )
    $params['exact_find'][$this->LEFT_OBJECT_TABLE.'.'.$this->LEFT_FIELDS_MAP_ID] = $id;

  return $this->getAllBasic($params);
}//End of method

//----------------------------------------------------------
/*!\brief Get the left objects properties from the right id
  \param $id(integer)
*/
public function getLeftLinks( $id=0 , $params=array() ){
  $params['with'][1]['type'] = $this->LEFT_TYPE;
  $params['with'][1]['table'] = $this->LEFT_OBJECT_TABLE; //Category
  $params['with'][1]['col'] = $this->LEFT_FIELDS_MAP_ID;

  if( !isset($params['select']) )
    $params['select'] = array($this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_ID, $this->LEFT_OBJECT_TABLE.'.*');

  if( $id != 0 )
    $params['exact_find'][$this->OBJECT_TABLE.'.'.$this->RIGHT_FIELDS_MAP_ID] = $id;

  return $this->getAllBasic($params);
}//End of method

//----------------------------------------------------------
/*!\brief Get the right objects properties from the left id
  \param $id(integer)
*/
public function getRightLinks( $id=0 , $params=array() ){
  $params['with'][0]['type'] = $this->RIGHT_TYPE;
  $params['with'][0]['table'] = $this->RIGHT_OBJECT_TABLE; //Container
  $params['with'][0]['col'] = $this->RIGHT_FIELDS_MAP_ID;

  if( !isset($params['select']) )
    $params['select'] = array($this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_ID, $this->RIGHT_OBJECT_TABLE.'.*');

  if( $id != 0 )
    $params['exact_find'][$this->OBJECT_TABLE.'.'.$this->LEFT_FIELDS_MAP_ID] = $id;

  return $this->getAllBasic($params);
}//End of method

//----------------------------------------------------------
/*! \brief getLinkId from the both elements
* 
  \param $left_id(integer)
  \param $right_id(integer)
*/
public function getLinkId( $left_id, $right_id ){
  $query =
  "SELECT $this->OBJECT_TABLE.$this->FIELDS_MAP_ID
  FROM $this->OBJECT_TABLE
  WHERE 
  $this->LEFT_FIELDS_MAP_ID = '$left_id'
  AND
  $this->RIGHT_FIELDS_MAP_ID = '$right_id'
  ";

  if(!$one = $this->dbranchbe->GetOne( $query )){
    Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
    trigger_error( $this->dbranchbe->ErrorMsg() , E_USER_WARNING);
    return false;
  }

  return $one;

}//End of method

//----------------------------------------------------------
/*! \brief Get left ids from the right id
* 
  \param $right_id(integer)
*/
public function getLeftIds( $right_id ){
  $query =
  "SELECT $this->OBJECT_TABLE.$this->FIELDS_MAP_ID
  FROM $this->OBJECT_TABLE
  WHERE 
  $this->RIGHT_FIELDS_MAP_ID = '$right_id'
  ";

  if(!$rs = $this->dbranchbe->execute( $query )){
    Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
    return false;
  }

  return $rs->GetArray();

}//End of method

//----------------------------------------------------------
/*! \brief Get right ids from the left id
* 
  \param $left_id(integer)
*/
public function getRightIds( $left_id ){
  $query =
  "SELECT $this->OBJECT_TABLE.$this->FIELDS_MAP_ID
  FROM $this->OBJECT_TABLE
  WHERE 
  $this->LEFT_FIELDS_MAP_ID = '$left_id'
  ";

  if(!$rs = $this->dbranchbe->execute( $query )){
    Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
    return false;
  }

  return $rs->GetArray();

}//End of method

//----------------------------------------------------------
/*! \brief Get the property of the link
* 
* \param $property_name(string)
*/
public function getProperty($property_name){
  if(!isset($this->link_id)){
    Ranchbe::getError()->notice('$this->link_id is not set');
    return false;
  }
  if(isset($this->core_props[$property_name]))
    return $this->core_props[$property_name];

  $query = 'SELECT '.$property_name.' FROM '.$this->OBJECT_TABLE.' WHERE '.$this->FIELDS_MAP_ID.' = \''.$this->link_id.'\'';
  $res = $this->dbranchbe->GetOne($query);
  if($res === false){
    Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
    return false;
  }
  return $this->core_props[$property_name] = $res;
}//End of method

//----------------------------------------------------------
/*! \brief Set the property. init() must be call before
* 
* \param $property_name(string)
* \param $property_value(string)
*/
public function setProperty($property_name, $property_value){
  return $this->core_props[$property_name] = $property_value;
}//End of method

} //End of class

