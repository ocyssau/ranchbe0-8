<?php
//-------------------------------------------------------------------------
  /*! \brief This method decompose the var $params and return the correct synthase for the "where" and "select" for SQL query.
   *  
   *  Exemple of query :
   *  $query = "SELECT $this->selectClose FROM `TABLE`
   *     $this->whereClose
   *     $this->orderClose
   *     ";
   *  Ranchbe::getDb()->SelectLimit( $query , $this->limit , $this->offset);
   *  This method is associated to script "filterManager.php" wich Get values from forms    
   *  
   *  return :
   *  $this->selectClose
   *  $this->whereClose
   *  $this->orderClose
   *  $this->limit
   *  $this->offset
   *  $this->from
   
       \param $params[offset](integer) offset sql option
       \param $params[maxRecords](integer) limit sql option
       \param $params[sort_field](string) field use for sort sql option
       \param $params[sort_order]('ASC' or 'DESC') sort direction sql option
       \param $params[exact_find](array) its a assoc array where key=a field and value=a word to find in the table.
                                          This array is translate to a query option like 'where key=value'
       \param $params[find](array) its a assoc array where key=a field and value=a string to find in the table.
                                          This array is translate to a query option like 'WHERE key LIKE %value%'
        to find multi value on the same field just separate the word by close OR or AND like this :
        nom1 OR nom2 OR nom3 with space on each side of OR
       \param $params[select]=array(field1,field2,field3,...) its a nonassoc array with all fields to put in the sql select option 
       \$params[where](array) Input special where close. Exemple : $params[where] = array('field1 > 1' , 'field2 = 2')
       \$params['with'][] = array(type=>"INNER or OUTER" , table=>"table to join" , col=>"field use for ON condition") Use this param for create a join close on query
       \$params['extra'] = any query to add to end
   */
  function getQueryOptions($params){

    $fromClose  = $this->OBJECT_TABLE;
    
  
    if( count($params['with']) > 0 ){
      foreach( $params['with'] as $with ){
		$with_table = explode('as', strtolower('rb_users AS uuser'));
		$with_table = trim($with_table[0]);
        $array_joinClose[] = $with['type'].' JOIN '. $with['table'] .' ON '. $with_table.'.'.$with['col'] .'='.$this->OBJECT_TABLE.'.'.$with['col'];
      }
    }
    if( is_array($array_joinClose) )
      $joinClose = implode(' ', $array_joinClose );
  
    if(!empty($params['offset']) && is_numeric($params['offset'])){
      $offset = $params['offset'];
    }else{
      $offset = 0;
    }
    
    if(!empty($params['numrows']) && is_numeric($params['numrows'])){
      $limit = $params['numrows'];
    }else $limit = '9999';
    
    if(!empty($params['sort_field'])){
      if (!isset($params['sort_order'])) $params['sort_order']='ASC';
      $orderClose = " ORDER BY " . $params['sort_field'] . " " . $params['sort_order'];
    }
    
    $this->find=$params['find'];
  
    //This section must be suppressed when all code will implemte the search object...
    //...
    if( count($params['exact_find']) > 0 ){
      foreach ( $params['exact_find'] as $field => $term){
        if( !empty( $this->FIELDS[$field] ) ) $field = $this->FIELDS[$field]; //pour faire un mapping entre le nom donn� et le nom reelle
        $this->find['AND'][] = "($field = '$term')";
      }
    }
  
    $this->whereClose = NULL;
  
    if(is_array ($params['where'])){
      foreach($params['where'] as $wc)
      $this->whereClose[] = "($wc)";
    }
    //...
  
    if(!empty( $this->find['AND'] ))
      $this->whereClose[] = implode(' AND ', $this->find['AND']);
    if(!empty( $this->find['OR'] ))
      $this->whereClose[] = ' OR '.implode(' OR ', $this->find['OR']);
  
    if(!empty( $this->whereClose ))
      $this->whereClose = ' WHERE '.implode(' AND ', $this->whereClose);
  
    if (is_array($params['select'])){
      $selectClose = implode(' , ' , $params['select']);
    } else { $selectClose = '*'; }
  
  
    if (isset($selectClose))  $this->selectClose = $selectClose;
      else $this->selectClose = NULL;
  
    if (isset($orderClose))   $this->orderClose  = $orderClose;
      else $this->orderClose = NULL;
    if (isset($limit))        $this->limit       = $limit;
    if (isset($offset))       $this->offset      = $offset;
    if (isset($fromClose))    $this->from        = $fromClose;
    if (isset($joinClose))    $this->joinClose   = $joinClose;
    if (isset($params['extra'])) $this->extra    = $params['extra'];
  
  }//End of method

//---------------------------------------------------------------------------
  /*! \brief Return the options in a array
  */
  function getQueryOptionsArray( $params=array() ){
    $this->getQueryOptions($params);
    return array(
      'selectClose'=>$this->selectClose,
      'whereClose'=>$this->whereClose,
      'orderClose'=>$this->orderClose,
      'limit'=>$this->limit,
      'offset'=>$this->offset,
      'from'=>$this->from,
      'joinClose'=>$this->joinClose,
      'extra'=>$this->extra,
    );
  }//End of method
