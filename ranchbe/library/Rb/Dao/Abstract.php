<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


/** Basic class for manage database access.
 *
 *  This class manage access to database. It define standard method to create, modify or suppress a single
 *  or a group of records in database.
 *  She define too standards methods to get records.
 */
abstract class Rb_Dao_Abstract {
	protected $selectClose = '';
	/**!< define the select close for query the database*/
	protected $whereClose = '';
	/**!< define the where close for query the database*/
	protected $orderClose = '';
	/**!< define the order for sort the result*/
	protected $limit = '';
	/**!< limit the number of records in the result*/
	protected $offset = '';
	/**!< defined the pagination page to display*/
	protected $recordCount; //(Integer)
	protected $select = array (); //(array or *)
	protected $with = array ();

	protected $space; //(Object)
	protected $error_stack; //PEAR_ErrorStack error manager
	protected $dbranchbe; //ADODB connection

	protected $OBJECT_TABLE = ''; //(String)Table name
	protected $MIN_SEQ_NUMBER = 1;
	protected $SEQ_NAME; //(String)Sequence name to use
	protected $seq_id = 0; //(Integer)

	//Fields:
	protected $FIELDS_MAP_ID = ''; //(String)Name of the field of the primary key
	protected $FIELDS_MAP_NUM = ''; //(String)Name of the field to define the number
	protected $FIELDS_MAP_NAME = ''; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC = ''; //(String)Name of the field where is store the description
	protected $FIELDS_MAP_STATE = ''; //(String)Name of the field where is store the state
	protected $FIELDS_MAP_VERSION = ''; //(String)Name of the field where is store the version
	protected $FIELDS_MAP_ITERATION = ''; //(String)Name of the field where is store the iteration
	protected $FIELDS_MAP_FATHER = ''; //(String)Name of the field where is store the id of the father

	public static $_lastQuery = '';

	/** Standard method to create a record in database
	 *   Return the object id if success, false else.
	 *   This method use the transactions.
 	 *
	 * @param Array $data , its an array where keys/values are fields/values of the record to create.
	 * @param Bool $transaction
	 * @return Integer
	 */
	protected function _basicCreate($data, $transaction = false) {

		if($transaction) $this->dbranchbe->StartTrans ();

		if ($this->SEQ_NAME != 'none') {
			if (! $this->seq_id)
			$data [$this->FIELDS_MAP_ID] = $this->setSeqId ();
			else
			$data [$this->FIELDS_MAP_ID] = $this->seq_id;
		}
		$this->seq_id = 0; //reinit sequence to ensure that it will be increment to next time

		$ok = $this->dbranchbe->AutoExecute ( $this->OBJECT_TABLE, $data, 'INSERT' );
		if (!$ok ) {
			$query = 'INSERT into ' . $this->OBJECT_TABLE . ' : ' . implode ( ';', $data );
			Ranchbe::getError()->errorDb( $this->dbranchbe->ErrorMsg (), $query);
			if($transaction) $this->dbranchbe->FailTrans ();
		}

		if($transaction) $ok = $this->dbranchbe->CompleteTrans ();
		//Zend_Debug::dump($ok);
		//Zend_Debug::dump($this->dbranchbe->HasFailedTrans() );
		if (! $ok ) {
			return false;
		}else{
			Ranchbe::getError()->push ( Rb_Error::INFO,
			array ('id' => $data [$this->FIELDS_MAP_ID], 'table' => $this->OBJECT_TABLE ),
					'new inserted entry id : %id% in table : %table%' );
			return $data [$this->FIELDS_MAP_ID];
		}
	} //End of method


	//----------------------------------------------------------------------
	/** Standard method to update a record in database.
	 *   Return true if success, false else.
	 *   This method dont use the transactions.
	 * 
	 * @param array $data , its an array where keys/values are fields/values of the record to update.
	 * @param integer $object_id
	 * @return Bool
	 */
	protected function _basicUpdate($data = array(), $object_id) {
		//Set empty values to NULL
		/*
		 foreach($data as $key=>$val){
		 if(empty($val)) $data[$key] = NULL;
		 else $data[$key] = $val;
		 }
		 */
		//Update the object data
		if (! $this->dbranchbe->AutoExecute ( $this->OBJECT_TABLE, $data, 'UPDATE', "$this->FIELDS_MAP_ID = $object_id" )) {
			$query = 'UPDATE ' . $this->OBJECT_TABLE . ' : ' . implode ( ';', $data );
			Ranchbe::getError()->errorDb( $this->dbranchbe->ErrorMsg (), $query);
			return false;
		}
		Ranchbe::getError()->push ( Rb_Error::INFO, array (
														'id' => $object_id, 
														'table' => $this->OBJECT_TABLE ),
		'updated record id : %id% in table : %table% ' . 'data=>' . var_export ( $data, true ) );
		return true;
	} //End of method


	//----------------------------------------------------------------------
	/** Standard method to update a record in database.
	 * Update record if exist, else create a new record.
	 * Return false if failed
	 * Return 1 if update
	 * Return 2 if create
	 *
	 * This method dont use the transactions.
	 * @param Array $data , its an array where keys/values are fields/values of the record to update.
	 * @param Integer $object_id, is the id of the record to update.
	 * @return Integer
	 */
	protected function _basicReplace($data = array(), $object_id) {
		// Replace: update field or create it if not existing
		$data [$this->FIELDS_MAP_ID] = $object_id;
		$ret = $this->dbranchbe->Replace ( $this->OBJECT_TABLE, $data, $this->FIELDS_MAP_ID, $autoquote = true );
		if ($ret === 0) {
			Ranchbe::getError()->push ( ERROR_DB, 'Fatal', array ('query' => 'UPDATE ' . implode ( ';', $data ) ), $this->dbranchbe->ErrorMsg () );
			return false;
		} else if ($ret === 1) {
			Ranchbe::getError()->push ( Rb_Error::INFO, array ('id' => $object_id, 'table' => $this->OBJECT_TABLE ), 'updated record id : %id% in table : %table%' );
		} else if ($ret === 2) {
			Ranchbe::getError()->push ( Rb_Error::INFO, array ('id' => $object_id, 'table' => $this->OBJECT_TABLE ), 'create record id : %id% in table : %table%' );
		}
		return $ret;
	} //End of method


	//----------------------------------------------------------------------
	/** Standard method to suppress a record in database
	 *
	 *   Return true if success, false else.
	 *   This method use the transactions.
	 *   
	 * @param Integer $object_id
	 * @return Bool
	 */
	protected function _basicSuppress($object_id) {
		$query = 'DELETE FROM ' . $this->OBJECT_TABLE . ' WHERE ' . $this->OBJECT_TABLE . '.' . $this->FIELDS_MAP_ID . ' = \'' . $object_id . '\'';
		//Execute the query
		if (! $this->dbranchbe->Execute ( $query )) {
			Ranchbe::getError()->errorDb( $this->dbranchbe->ErrorMsg (), $query);
			return false;
		}
		Ranchbe::getError()->push ( Rb_Error::INFO,
					array ('id' => $object_id, 'table' => $this->OBJECT_TABLE ),
					'suppressed record id : %id% in table : %table%' );
		return true;
	} //End of method


	//------------------------------------------------------------------------
	/**
	 * Method for remove from a query
	 *   The $params is use for create a query for select all records to suppress
	 *  $params(array) (exact_find=>"word to find in field", find_field=>"field where to search word")
	 * 
	 * @param Array $params
	 * @return Bool
	 */
	protected function _basicSuppressQuery($params) {
		$this->_getQueryOptions ( $params );
		$query = 'DELETE FROM ' . $this->OBJECT_TABLE . ' ' . $this->whereClose . ' ' . $this->extra;
		//Execute the query
		if (! $this->dbranchbe->Execute ( $query )) {
			Ranchbe::getError()->push ( ERROR_DB, 'Fatal', array ('query' => $query ), $this->dbranchbe->ErrorMsg () );
			return false;
		}
		return true;
	} //End of method


	//----------------------------------------------------------------------
	/** Standard method to get all or a filter list of records of a table.
	 *
	 * Return a array if success, false else.
	 * This method dont use the transactions.
	 * 
	 * $params[offset](integer) offset sql option
	 * $params[maxRecords](integer) limit sql option
	 * $params[sort_field](string) field use for sort sql option
	 * $params[sort_order]('ASC' or 'DESC') sort direction sql option
	 * $params[exact_find](array) its a assoc array where key=a field and value=a word to find in the table.
	 * 		This array is translate to a query option like 'where key=value'
	 * $params[find](array) its a assoc array where key=a field and value=a string to find in the table.
	 * 		This array is translate to a query option like 'WHERE key LIKE %value%'
	 * $params[select](array) its a nonassoc array with all fields to put in the sql select option
	 * $params[getRs](bool) if true, return the recordset, else return array
	 * $params[getQuery](bool) if true, return the query options only
	 * 
	 * @param Array $params
	 * @return Array
	 */
	public function getAllBasic($params) {
		if ($params ['getQuery'] == true) return $this->getQueryOptionsArray ( $params );
		//if (is_array ( $this->unionObjects )) //Be carefull to loops!
		//	return $this->getAllUnion ( $params );

		$this->_getQueryOptions ( $params );

		//$query = 'SELECT SQL_CALC_FOUND_ROWS'.$this->selectClose.' FROM '.$this->OBJECT_TABLE
		$query = 'SELECT ' . $this->selectClose . ' FROM ' . $this->OBJECT_TABLE . ' ' . $this->joinClose . ' ' . $this->whereClose . ' ' . $this->extra . ' ' . $this->orderClose;
		//echo $query.'<br>';

		$ADODB_COUNTRECS = false;
		if (! $rs = $this->dbranchbe->SelectLimit ( $query, $this->limit, $this->offset )) {
			Ranchbe::getError()->errorDb( $this->dbranchbe->ErrorMsg (), $query);
			trigger_error ( $this->dbranchbe->ErrorMsg () , E_USER_WARNING);
			return false;
		} else {
			//$rs3 = $this->dbranchbe->PageExecute($query, $this->limit, 2);
			//$rs3 = $this->dbranchbe->CachePageExecute(3600, $query, 1000, 1);
			//var_dump($rs3);
			//$All = $rs3->GetArray(); //To transform result in array;
			//return $All;
			//$query = 'SELECT FOUND_ROWS()';
			//$rs2 = $this->dbranchbe->getone($query);
			//var_dump($rs2);
			//var_dump($rs->fields);
			//$this->recordCount = $rs->RecordCount();
			//var_dump($this->recordCount);
			//var_dump($All);
			if ($params ['getRs'] === true)
			return $rs;
			return $rs->GetArray (); //To transform result in array;
		}
	} //End of method


	//-------------------------------------------------------------------
	/** Get one record
	 * 
	 * @param Array $params
	 * @return Array
	 */
	public function getOne(array $params = array()) {
		return $this->_dbget ( false, $params, 'one', $params ['getRs'] );
	} //End of method


	//-------------------------------------------------------------------
	/** Get one line
	 *
	 * @param Array $params
	 * @return Array
	 */
	public function getRow(array $params = array()) {
		return $this->_dbget ( false, $params, 'row', $params ['getRs'] );
	} //End of method


	//----------------------------------------------------------------------
	/** Get the id from the number of an object. return id if success, false else.
	 *  object is somthing like : project, workitem, bookshop, cadlib, mockup, product, partner...etc.
	 * @param String $number
	 * @return Integer
	 */
	function getBasicId($number) {
		$query = 'SELECT ' . $this->FIELDS_MAP_ID . ' FROM ' . $this->OBJECT_TABLE . ' WHERE ' . $this->FIELDS_MAP_NUM . ' = \'' . $number . '\'';
		$one = $this->dbranchbe->GetOne ( $query );
		return $one;
	} //End of method


	//-------------------------------------------------------------
	/** Get the number from the id of an object
	 *   return id if success, false else.
	 *
	 *  object is somthing like : project, workitem, bookshop, cadlib, mockup, product, partner...etc.
	 *  
	 * @param Integer $object_id
	 * @return String
	 */
	public function getBasicNumber($object_id) {
		if (is_null ( $object_id ) && ! is_numeric ( $object_id )) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('id' => $object_id ), 'Invalid object_id : %id%' );
		}
		$query = 'SELECT ' . $this->FIELDS_MAP_NUM . ' FROM ' . $this->OBJECT_TABLE . ' WHERE (' . $this->FIELDS_MAP_ID . ' = \'' . $object_id . '\')';
		$one = $this->dbranchbe->GetOne ( $query );
		return $one;
	} //End of method


	//-------------------------------------------------------------------
	/** Get informations about one object
	 *   return a array if success, false else.
	 *  object is somthing like : project, workitem, bookshop, cadlib, mockup, product, partner...etc.
	 *   
	 * @param Integer 	$object_id	is the id of the object
	 * @param Array 	$params 	See getQueryOptions()
	 * @param String 	$get		'row': return just one row of the recordset, 'one': return just the first field of the first row of the recordset
	 * @return Array
	 */
	public function getBasicInfos($object_id, $params = array(), $get = 'row') {
		if (is_null ( $object_id = intval ( $object_id ) )) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('id' => $object_id ), 'Invalid object_id : %id%' );
		}
		$params ['exact_find'] [$this->FIELDS_MAP_ID] = $object_id;
		return $this->_dbget ( $this->OBJECT_TABLE, $params, $get, $params ['getRs'] );
	} //End of method


	//-------------------------------------------------------------------
	/** Get records from database
	 *   return a array if success, false else.
	 *
	 *  @param $table(String), table name to query or false to use the default table
	 *  @param $params(Array), See parameters of _getQueryOptions()
	 *  @param $get(String) = 'all': return all records
		 'row': return just one row of the recordset
		 'one': return just the first field of the first row of the recordset
	 *  @param $getRs(bool) = 'true': return the recordset(object) in place array
	 */
	protected function _dbget($table = false, $params, $get = 'all', $getRs = false) {
		if (! $table)
		$table = $this->OBJECT_TABLE;
		$this->_getQueryOptions ( $params );
		
		/*
		$stmt = $this->dbranchbe->PrepareStmt("select * from table where name=?");
		$rs = $stmt->Execute(array('Jill'));
		foreach ($rs as $row) {
			var_dump($row);
		}
		*/

		$query = "SELECT $this->selectClose FROM $table
					$this->joinClose
					$this->whereClose
					$this->orderClose
					$this->extra";
		
		self::$_lastQuery = $query;
		if ($get === 'one')
			$rset = $this->dbranchbe->GetOne ( $query );
		if ($get === 'row')
			$rset = $this->dbranchbe->GetRow ( $query );
		if ($get === 'all')
			$rset = $this->dbranchbe->SelectLimit ( $query, $this->limit, $this->offset );
		$mess = $this->dbranchbe->ErrorMsg ();
		if ($mess) {
			Ranchbe::getError()->errorDb( $mess, $query);
			return false;
		}
		if ($get === 'all' && $getRs === false) {
			$rset = $rset->GetArray (); //To transform object result in array;
		}
		return $rset;
	} //End of method


	//-------------------------------------------------------------------------
	/** This method decompose the var $params and return the correct synthase for the "where" and "select" for SQL query.
	 *
	 *  Exemple of query :
	 *  $query = "SELECT $this->selectClose FROM `TABLE`
	 *     $this->whereClose
	 *     $this->orderClose
	 *     ";
	 *  Ranchbe::getDb()->SelectLimit( $query , $this->limit , $this->offset);
	 *  This method is associated to script "filterManager.php" wich get values from forms
	 *
	 *  return :
	 *  $this->selectClose
	 *  $this->whereClose
	 *  $this->orderClose
	 *  $this->limit
	 *  $this->offset
	 *  $this->from
	  
	 * @param $params[offset](integer) offset sql option
	 * @param $params[maxRecords](integer) limit sql option
	 * @param $params[sort_field](string) field use for sort sql option
	 * @param $params[sort_order]('ASC' or 'DESC') sort direction sql option
	 * @param $params[exact_find](array) its a assoc array where key=a field and value=a word to find in the table.
	 This array is translate to a query option like 'where key=value'
	 * @param $params[find](array) its a assoc array where key=a field and value=a string to find in the table.
	 This array is translate to a query option like 'WHERE key LIKE %value%'
	 to find multi value on the same field just separate the word by close OR or AND like this :
	 nom1 OR nom2 OR nom3 with space on each side of OR
	 * @param $params[select]=array(field1,field2,field3,...) its a nonassoc array with all fields to put in the sql select option
	 * @$params[where](array) Input special where close. Exemple : $params[where] = array('field1 > 1' , 'field2 = 2')
	 * @$params['with'][] = array(type=>"INNER or OUTER" ,
	 table=>"table to join" ,
	 col=>"field use for ON condition") Use this param for create a join close on query
	 * @$params['extra'] = any query to add to end
	 */
	protected function _getQueryOptions($params) {

		$fromClose = $this->OBJECT_TABLE;
		$array_joinClose = false;

		//initialize parameters
		$this->orderClose = null;
		$this->limit = null;
		$this->offset = null;
		$this->from = null;
		$this->joinClose = null;
		$this->extra = null;
		$this->orderClose = null;

		//JOIN
		if (count ( $params ['with'] ) > 0) {
			foreach ( $params ['with'] as $with ) {
				if (! $with ['col1']){
					$with ['col1'] = $with ['col'];
				}
				if (! $with ['col2']){
					$with ['col2'] = $with ['col'];
				}
				//extract alias table name
				$wt = explode('as', strtolower($with ['table']));
				if($wt[1]){
					$with_table = trim($wt[0]);
					$with_table_alias = trim($wt[1]);
				}else{
					$with_table = trim($with ['table']);
					$with_table_alias = $with_table;
				}
				//var_dump($with_table, $with_table_alias, $with ['table']);
				$array_joinClose [] = $with ['type'] . ' JOIN ' . $with ['table'] . ' ON ' . $this->OBJECT_TABLE . '.' . $with ['col1'] . '=' . $with_table_alias . '.' . $with ['col2'];
			}
		}
		if (is_array ( $array_joinClose ))
		$joinClose = implode ( ' ', $array_joinClose );
		
		

		/*
		 if(!empty ($params['find'])){
		 foreach ( $params['find'] as $field => $term ){
		 if( !empty( $this->FIELDS[$field] ) ) $field = $this->FIELDS[$field];
		 $orClose = array();
		 $termOrExplode = explode(' OR ', $term ); //Explode
		 if( count($termOrExplode) > 1 ){ //A OR is require
		 foreach($termOrExplode as $term){
		 $orClose[] = "$field LIKE ('%$term%')";
		 }
		 $whereClose[] = '('.implode(' OR ', $orClose).')';
		 }else{ //just a simple string
		 //$whereClose[] = $this->FIELDS[$field]." LIKE ('%$term%')";
		 $whereClose[] = $field." LIKE ('%$term%')";
		 }
		 }
		 }
	  */

		/*
		 if( count($params['exact_find']) > 0 ){
		 foreach ( $params['exact_find'] as $field => $term){
		 if( !empty( $this->FIELDS[$field] ) ) $field = $this->FIELDS[$field]; //pour faire un mapping entre le nom donn� et le nom reelle
		 $whereClose[] = "($field = '$term')";
		 }
		 }
	  */

		if (! empty ( $params ['offset'] ) && is_numeric ( $params ['offset'] )) {
			$offset = $params ['offset'];
		} else {
			$offset = 0;
		}

		if (! empty ( $params ['numrows'] ) && is_numeric ( $params ['numrows'] )) {
			$limit = $params ['numrows'];
		} else
		$limit = 999999999;

		if (! empty ( $params ['sort_field'] )) {
			if (! isset ( $params ['sort_order'] ))
			$params ['sort_order'] = 'ASC';
			$orderClose = " ORDER BY " . $params ['sort_field'] . " " . $params ['sort_order'];
		}

		/*
		 if(!empty( $whereClose )){
		 $whereClose = 'WHERE ' . implode(' AND ', $whereClose);
		 }
		 */

		$this->find = $params ['find'];

		//This section must be suppressed when all code will implemte the search object...
		//...
		if (count ( $params ['exact_find'] ) > 0) {
			foreach ( $params ['exact_find'] as $field => $term ) {
				if (! empty ( $this->FIELDS [$field] ))
				$field = $this->FIELDS [$field]; //pour faire un mapping entre le nom donn� et le nom reelle
				$this->find ['AND'] [] = "($field = '$term')";
			}
		}

		$this->whereClose = NULL;

		if (is_array ( $params ['where'] )) {
			foreach ( $params ['where'] as $wc )
			$this->whereClose [] = "($wc)";
		}
		//...


		if (! empty ( $this->find ['AND'] ))
		$this->whereClose [] = implode ( ' AND ', $this->find ['AND'] );
		if (! empty ( $this->find ['OR'] ))
		$this->whereClose [] = ' OR ' . implode ( ' OR ', $this->find ['OR'] );

		if (! empty ( $this->whereClose ))
		$this->whereClose = ' WHERE ' . implode ( ' AND ', $this->whereClose );

		if (is_array ( $params ['select'] )) {
			if (count ( $params ['select'] ) == 0)
			$selectClose = '*';
			else
			$selectClose = implode ( ' , ', $params ['select'] );
		} else {
			$selectClose = '*';
		}

		if (isset ( $selectClose ))
		$this->selectClose = $selectClose;
		else
		$this->selectClose = NULL;

		if (isset ( $orderClose ))
		$this->orderClose = $orderClose;
		else
		$this->orderClose = NULL;
		if (isset ( $limit ))
		$this->limit = $limit;
		if (isset ( $offset ))
		$this->offset = $offset;
		if (isset ( $fromClose ))
		$this->from = $fromClose;
		if (isset ( $joinClose ))
		$this->joinClose = $joinClose;
		if (isset ( $params ['extra'] ))
		$this->extra = $params ['extra'];
			
	} //End of method


	//---------------------------------------------------------------------------
	/** Return the options in a array
	 */
	public function getQueryOptionsArray($params = array()) {
		$this->_getQueryOptions ( $params );
		return array (	'selectClose' => $this->selectClose,
						'whereClose' => $this->whereClose, 
						'orderClose' => $this->orderClose, 
						'limit' => $this->limit, 
						'offset' => $this->offset, 
						'from' => $this->from, 
						'joinClose' => $this->joinClose, 
						'extra' => $this->extra 
		);
	} //End of method


	//---------------------------------------------------------------------------
	public function getTableName($table = 'object') {
		switch ($table) {
			case 'history' :
				if ($this->HISTORY_TABLE)
				return $this->HISTORY_TABLE;
				else if ($this->OBJECT_TABLE)
				return $this->OBJECT_TABLE . '_history';
				return false;
				break;
			case 'object' :
				if (isset ( $this->OBJECT_TABLE ))
				return $this->OBJECT_TABLE;
				return false;
				break;
		} //End of switch
		return false;
	} //End of method


	//---------------------------------------------------------------------------
	public function getKey($field) {
		return $this->getFieldName ( $field );
	} //End of method


	//---------------------------------------------------------------------------
	public function getPrimayKey() {
		return $this->getFieldName ( 'primary_key' );
	} //End of method


	//---------------------------------------------------------------------------
	public function getFieldName($field = 'primary_key') {
		switch ($field) {
			case 'id' :
			case 'pkey' :
			case 'primary' :
			case 'primary_key' :
				if (isset ( $this->FIELDS_MAP_ID ))
				return $this->FIELDS_MAP_ID;
				return false;
				break;
			case 'number' :
				if (isset ( $this->FIELDS_MAP_NUM ))
				return $this->FIELDS_MAP_NUM;
				return false;
				break;
			case 'name' :
				if (isset ( $this->FIELDS_MAP_NAME ))
				return $this->FIELDS_MAP_NAME;
				return false;
				break;
			case 'designation' :
			case 'description' :
				if (isset ( $this->FIELDS_MAP_DESC ))
				return $this->FIELDS_MAP_DESC;
				return false;
				break;
			case 'state' :
				if (isset ( $this->FIELDS_MAP_STATE ))
				return $this->FIELDS_MAP_STATE;
				return false;
				break;
			case 'indice' :
				if (isset ( $this->FIELDS_MAP_VERSION ))
				return $this->FIELDS_MAP_VERSION;
				return false;
				break;
			case 'version' :
				if (isset ( $this->FIELDS_MAP_VERSION ))
				return $this->FIELDS_MAP_VERSION;
				return false;
				break;
			case 'iteration' :
				if (isset ( $this->FIELDS_MAP_ITERATION ))
				return $this->FIELDS_MAP_ITERATION;
				return false;
				break;
			case 'father' :
				if (isset ( $this->FIELDS_MAP_FATHER ))
				return $this->FIELDS_MAP_FATHER;
				return false;
				break;
		} //End of switch
		return false;
	} //End of method


	//---------------------------------------------------------------------------
	public function setTable($name, $role = 'object') {
		if (! $name)
		trigger_error ( 'name is not set', E_USER_ERROR );
		switch ($role) {
			case 'history' :
				return $this->HISTORY_TABLE = $name;
				break;
			case 'object' :
				return $this->OBJECT_TABLE = $name;
				break;
		} //End of switch
		return false;
	} //End of method


	//---------------------------------------------------------------------------
	public function setKey($name, $role = 'primary') {
		if (! $name)
		trigger_error ( 'name is not set', E_USER_ERROR );
		switch ($role) {
			case 'pkey' :
			case 'primary' :
			case 'primary_key' :
			case 'id' :
				return $this->FIELDS_MAP_ID = $name;
				break;
			case 'number' :
				return $this->FIELDS_MAP_NUM = $name;
				break;
			case 'name' :
				return $this->FIELDS_MAP_NAME = $name;
				break;
			case 'designation' :
			case 'description' :
				return $this->FIELDS_MAP_DESC = $name;
				break;
			case 'state' :
				return $this->FIELDS_MAP_STATE = $name;
				break;
			case 'indice' :
				return $this->FIELDS_MAP_VERSION = $name;
				break;
			case 'iteration' :
				return $this->FIELDS_MAP_ITERATION = $name;
				break;
			case 'version' :
				return $this->FIELDS_MAP_VERSION = $name;
				break;
			case 'father' :
				return $this->FIELDS_MAP_FATHER = $name;
				break;
		} //End of switch
		return false;
	} //End of method


	//---------------------------------------------------------------------------
	/** Set name of sequence to use.
	 *  If none sequence must be used, set to 'none'.
	 *  
	 * 	@param String
	 */
	public function setSequence($name) {
		$this->SEQ_NAME = $name;
	} //End of method


	//---------------------------------------------------------------------------
	/**
	 * 
	 * @param integer $num
	 */
	public function setMinSequenceNumber($num) {
		$this->MIN_SEQ_NUMBER = $num;
	} //End of method


	//---------------------------------------------------------------------------
	/**
	 * 
	 */
	public function getAdo() {
		return $this->dbranchbe;
	} //End of method

	//-------------------------------------------------------------------------
	/**
	 * 
	 * @param ADOConnection $db
	 * @return void
	 */
	public function reconnect(ADOConnection &$db) {
		$this->dbranchbe =& $db;
	} //End of method
	
	//---------------------------------------------------------------------------
	/** Generate the sequence id for this object
	 *
	 */
	public function setSeqId() {
		if (! $this->SEQ_NAME) {
			$seq_name = $this->OBJECT_TABLE . '_seq';
		} else {
			$seq_name = $this->SEQ_NAME;
		}
		if ($this->SEQ_NAME != 'none') {
			$this->seq_id = $this->dbranchbe->GenID ( $seq_name, $this->MIN_SEQ_NUMBER ); //Increment the sequence number
		}
		Ranchbe::getError()->info('New id :' . $this->seq_id . ' from sequence :' . $seq_name);
		
		return $this->seq_id;
	} //End of method
	
}// End of class

