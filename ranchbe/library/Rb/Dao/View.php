<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

////require_once('core/dao/abstract.php');

//-------------------------------------------------------------------------
/*! \brief This class is use to access to datas from views.
*
*/
class Rb_Dao_View extends Rb_Dao_Abstract{

  public function __construct(){
    $this->dbranchbe =& Ranchbe::getDb();
  }//End of method

  /* surcharge des methodes de la classe basic
  */
  function _basicCreate($data){
    trigger_error('create is not valid on this class', E_USER_WARNING);
  }//End of method

  /* surcharge des methodes de la classe basic
  */
  function _basicUpdate($data = array(), $object_id){
    trigger_error('update is not valid on this class', E_USER_WARNING);
  }//End of method

  /* surcharge des methodes de la classe basic
  */
  function _basicSuppress($object_id) {
    trigger_error('suppress is not valid on this class', E_USER_WARNING);
  }//End of method

  function _basicSuppressQuery( $params ){
    trigger_error('suppress is not valid on this class', E_USER_WARNING);
  }//End of method

} //end of class
