<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

////require_once('core/dao/abstract.php');

//----------------------------------------------------------------------

/*! \brief Basic class for manage database access.
 *
 *  This class manage access to database. It define standard method to create, modify or suppress a single
 *  or a group of records in database. 
 *  She define too standards methods to get records. 
 */
abstract class Rb_Dao_Union extends Rb_Dao_Abstract{

  protected $unionObjects=array();

//--------------------------------------------------------------------
  /*!\brief
  */
  protected function _getAllUnion( $params = array() ){
    if( !is_array( $this->unionObjects ) ) return $this->getAllBasic( $params );
    //array_unshift( $this->unionObjects  , $this ); //add current object to Union list
  
    foreach( $this->unionObjects as $unionObject ){
      $P = $params;
      $P['getQuery'] = true; //set only the components of query without execute it
      $P['select'] = array();
      foreach( $params['select'] as $selectElement ){
        $P['select'][] = $unionObject->FIELDS[$selectElement];
      }
      $query_element = $unionObject->getAll($P);
      $queries[] = 'SELECT DISTINCT '.$query_element['selectClose']
                    .' FROM '.$query_element['from']
                    .' '.$query_element['joinClose']
                    .' '.$query_element['whereClose'];
    }
    
    if(!empty( $queries )){
      $query = implode(' UNION ', $queries);
    }
    
    $query = $query.' '.$this->orderClose.' '.$this->extra;
    
    if(!$rs = $this->dbranchbe->SelectLimit( $query , $this->limit , $this->offset )){
    	Ranchbe::getError()->errorDb($this->dbranchbe->ErrorMsg(), $query);
      return false;
    }else{
      $All = $rs->GetArray(); //To transform result in array;
      return $All;
    }
  }//End of method

//--------------------------------------------------------------------
  /*!\brief
  */
  public function setUnion( Rb_Dao_Abstract &$object , $select ){
    if( !is_object($object) ) return false;
    if( !method_exists($object, 'GetAll' ) ) return false;
  
    //construct a FIELD list array used for create same select for both sql request
    //select define the fields in both Union table
    foreach($select as $field){
      if( empty($object->FIELDS[$field] ) )
        $object->FIELDS[$field] = $this->OBJECT_TABLE.'.'.$field;
    }
    return $this->unionObjects[] =& $object;
  }//End of method

//-------------------------------------------------------------------------
  /*! \brief This method decompose the var $params and return the correct synthase for the "where" and "select" for SQL query.
   *   
   *  Exemple of query :
   *  $query = "SELECT $this->selectClose FROM `TABLE`
   *     $this->whereClose
   *     $this->orderClose
   *     ";
   *  Ranchbe::getDb()->SelectLimit( $query , $this->limit , $this->offset);
   *  This method is associated to script "filterManager.php" wich get values from forms    
   *     
   *  return :
   *  $this->selectClose
   *  $this->whereClose
   *  $this->orderClose
   *  $this->limit
   *  $this->offset
   *  $this->from
   
       \param $params[offset](integer) offset sql option
       \param $params[maxRecords](integer) limit sql option
       \param $params[sort_field](string) field use for sort sql option
       \param $params[sort_order]('ASC' or 'DESC') sort direction sql option
       \param $params[exact_find](array) its a assoc array where key=a field and value=a word to find in the table.
                                          This array is translate to a query option like 'where key=value'
       \param $params[find](array) its a assoc array where key=a field and value=a string to find in the table.
                                          This array is translate to a query option like 'WHERE key LIKE %value%'
        to find multi value on the same field just separate the word by close OR or AND like this :
        nom1 OR nom2 OR nom3 with space on each side of OR
       \param $params[select]=array(field1,field2,field3,...) its a nonassoc array with all fields to put in the sql select option 
       \$params[where](array) Input special where close. Exemple : $params[where] = array('field1 > 1' , 'field2 = 2')
       \$params['with'][] = array(type=>"INNER or OUTER" , table=>"table to join" , col=>"field use for ON condition") Use this param for create a join close on query
       \$params['extra'] = any query to add to end
   */
  protected function _getQueryOptions($params){

    $fromClose  = $this->OBJECT_TABLE;
    
    /*
    if( is_a($params, 'Rb_Search_Interface') ) {
      $params->_getQueryOptions();
      $this->selectClose = $params->selectClose;
      $this->whereClose = $params->whereClose;
      $this->orderClose = $params->orderClose;
      $this->limit = $params->limit;
      $this->offset = $params->offset;
      $this->joinClose = $params->joinClose;
      $this->extra = $params->extra;
      return true;
    }
    */
  
    /*
    if ( is_array($params['with']) ) {
      if( empty($params['with']['type']) ) $params['with']['type'] = 'INNER';
      $joinClose = $params['with']['type'].' JOIN '. $params['with']['table'] .' ON '. $params['with']['table'].'.'.$params['with']['col'] .'='.$this->OBJECT_TABLE.'.'.$params['with']['col'];
    }
    */
  /*
    if ( is_array($params['with']) ) {
      if( isset($params['with'][0]) ){
        foreach( $params['with'] as $with ){
          if( empty($with['type']) ) $with['type'] = 'INNER';
          $array_joinClose[] = $with['type'].' JOIN '. $with['table'] .' ON '. $with['table'].'.'.$with['col'] .'='.$this->OBJECT_TABLE.'.'.$with['col'];
        }
      }else{
        if( empty($params['with']['type']) ) $params['with']['type'] = 'INNER';
        $joinClose = $params['with']['type'].' JOIN '. $params['with']['table'] .' ON '. $params['with']['table'].'.'.$params['with']['col'] .'='.$this->OBJECT_TABLE.'.'.$params['with']['col'];
      }
    }
    if( is_array($array_joinClose) )
      $joinClose = implode(' ', $array_joinClose );
  */
  
    if( count($params['with']) > 0 ){
      foreach( $params['with'] as $with ){
        $array_joinClose[] = $with['type'].' JOIN '. $with['table'] .' ON '. 
                              $with['table'].'.'.$with['col'] .'='.
                              $this->OBJECT_TABLE.'.'.$with['col'];
      }
    }
    if( is_array($array_joinClose) )
      $joinClose = implode(' ', $array_joinClose );
  
  /*
    if (!empty ($params['with']) ) {
        foreach ( $params['with'] as $table => $col )
        $joinClose[] = ' JOIN '. $table .' ON '. $table.'.'.$col .'='.$params['table'].'.'.$params['with']['col'];
    }
  */
  
  /*
    if(!empty ($params['find'])){
        foreach ( $params['find'] as $field => $term ){
          if( !empty( $this->FIELDS[$field] ) ) $field = $this->FIELDS[$field];
          $orClose = array();
          $termOrExplode = explode(' OR ', $term ); //Explode
          if( count($termOrExplode) > 1 ){ //A OR is require
            foreach($termOrExplode as $term){
              $orClose[] = "$field LIKE ('%$term%')";
            }
            $whereClose[] = '('.implode(' OR ', $orClose).')';
          }else{ //just a simple string
            //$whereClose[] = $this->FIELDS[$field]." LIKE ('%$term%')";
            $whereClose[] = $field." LIKE ('%$term%')";
          }
        }
    }
  */
  
  /*  
    if( count($params['exact_find']) > 0 ){
        foreach ( $params['exact_find'] as $field => $term){
          if( !empty( $this->FIELDS[$field] ) ) $field = $this->FIELDS[$field]; //pour faire un mapping entre le nom donn� et le nom reelle
          $whereClose[] = "($field = '$term')";
        }
    }
  */
  
    if(!empty($params['offset']) && is_numeric($params['offset'])){
      $offset = $params['offset'];
    }else{
      $offset = 0;
    }
    
    if(!empty($params['numrows']) && is_numeric($params['numrows'])){
      $limit = $params['numrows'];
    }else $limit = 999999999;
    
    if(!empty($params['sort_field'])){
      if (!isset($params['sort_order'])) $params['sort_order']='ASC';
      $orderClose = " ORDER BY " . $params['sort_field'] . " " . $params['sort_order'];
    }
  
  /*
    if(!empty( $whereClose )){
        $whereClose = 'WHERE ' . implode(' AND ', $whereClose);
    }
  */
  
    $this->find=$params['find'];
  
    //This section must be suppressed when all code will implemte the search object...
    //...
  
    if( count($params['exact_find']) > 0 ){
      foreach ( $params['exact_find'] as $field => $term){
        if( !empty( $this->FIELDS[$field] ) ) $field = $this->FIELDS[$field]; //pour faire un mapping entre le nom donn� et le nom reelle
        $this->find['AND'][] = "($field = '$term')";
      }
    }
  
    $this->whereClose = NULL;
  
    if(is_array ($params['where'])){
      foreach($params['where'] as $wc)
      $this->whereClose[] = "($wc)";
    }
    //...
  
    if(!empty( $this->find['AND'] ))
      $this->whereClose[] = implode(' AND ', $this->find['AND']);
    if(!empty( $this->find['OR'] ))
      $this->whereClose[] = ' OR '.implode(' OR ', $this->find['OR']);
  
    if(!empty( $this->whereClose ))
      $this->whereClose = ' WHERE '.implode(' AND ', $this->whereClose);
  
    if (is_array($params['select'])){
      $selectClose = implode(' , ' , $params['select']);
    } else { $selectClose = '*'; }
  
  
    if (isset($selectClose))  $this->selectClose = $selectClose;
      else $this->selectClose = NULL;
  
    if (isset($orderClose))   $this->orderClose  = $orderClose;
      else $this->orderClose = NULL;
    if (isset($limit))        $this->limit       = $limit;
    if (isset($offset))       $this->offset      = $offset;
    if (isset($fromClose))    $this->from        = $fromClose;
    if (isset($joinClose))    $this->joinClose   = $joinClose;
    if (isset($params['extra'])) $this->extra    = $params['extra'];
  
  }//End of method

}// End of class
