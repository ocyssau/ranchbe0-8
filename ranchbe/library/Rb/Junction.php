<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//THIS CLASS IS NOT USED

class Rb_Junction extends Rb_Dao_Abstract{

protected $OBJECT_TABLE;
protected $FIELDS_MAP_ID;
protected $LEFT_OBJECT_TABLE;
protected $LEFT_FIELDS_MAP_ID;
protected $RIGHT_OBJECT_TABLE;
protected $RIGHT_FIELDS_MAP_ID;
protected $FIELDS = array();

function __construct(){
  global $dbranchbe;
  $this->dbranchbe =& $dbranchbe;
  global $error_stack;
  $this->error_stack =& $error_stack;
  global $usr;
  $this->usr =& $usr;
}//End of method

function setObjectTable( $table, $primary_key ){
  $this->OBJECT_TABLE=$table;
  $this->FIELDS_MAP_ID=$primary_key;
}

function setLeftTable( $table, $primary_key ){
  $this->LEFT_OBJECT_TABLE=$table;
  $this->LEFT_FIELDS_MAP_ID=$primary_key;
}

function setRightTable( $table, $primary_key ){
  $this->RIGHT_OBJECT_TABLE=$table;
  $this->RIGHT_FIELDS_MAP_ID=$primary_key;
}

function setRightJoinOn( $key ){
  $this->RIGHT_FIELDS_MAP_ID=$key;
}

function setLeftJoinOn( $key ){
  $this->LEFT_FIELDS_MAP_ID=$key;
}

function getObjectTable(){
  return $this->OBJECT_TABLE;
}

function getLeftTable(){
  return $this->LEFT_OBJECT_TABLE;
}

function getRightTable(){
  return $this->RIGHT_OBJECT_TABLE;
}

//----------------------------------------------------------
/*!\brief Get links
*/
function Get( $params ){
  $params['with'][0]['type'] = 'INNER';
  $params['with'][0]['table'] = $this->RIGHT_OBJECT_TABLE; //Container
  $params['with'][0]['col'] = $this->RIGHT_FIELDS_MAP_ID;

  $params['with'][1]['type'] = 'INNER';
  $params['with'][1]['table'] = $this->LEFT_OBJECT_TABLE; //Category
  $params['with'][1]['col'] = $this->LEFT_FIELDS_MAP_ID;

  if( !$params['select'] )
  $params['select'] = array(
                            $this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_ID,
                            $this->RIGHT_OBJECT_TABLE.'.'.$this->RIGHT_FIELDS_MAP_ID,
                            $this->LEFT_OBJECT_TABLE.'.'.$this->LEFT_FIELDS_MAP_ID,
                            );

  return $this->GetAllBasic($params);
}//End of method

}//End of class
