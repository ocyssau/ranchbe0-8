<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

if(!function_exists ('link')){
	/**
	 *
	 * @param string $from The link target, it is the original file or source file
	 * @param string $to The link name, it is the new name for file $target
	 * @return boolean
	 */
	function link($from, $to){
		$from = str_replace('\\', '/', realpath($from));
		$to = str_replace('\\', '/', $to);
		$cmd =  Ranchbe::getConfig()->cmd->link;
		if(!$cmd) return false;
		$cmd = str_replace('%from%', $from, $cmd);
		$cmd = str_replace('%to%', $to, $cmd);
		system($cmd, $retval);
		//var_dump(!($retval), $cmd, $target, $path);die;
		return !($retval);
	}
}

if(!function_exists ('readlink')){
	/**
	 * 
	 * @param string $path
	 * @return string
	 */
	function readlink($path){
		//@todo: implement this function
		if( linkinfo($path) ) return true;
	}
}

/** Hardlink on filesystem
 * 
 * @author Administrateur
 *
 */
class Rb_Hardlink extends Rb_Fslink_Abstract{

	//----------------------------------------------------------
	/** Create a new link
	 *
	 * @param string $from The link target, it is the original file or source file
	 * @param string $to The link name, it is the new name for file $target
	 * @return boolean
	 */
	public static function create($from, $to){
		if(!Rb_Filesystem::limitDir($to)) return false;
		//if(!Rb_Filesystem::limitDir($from)) return false;
		if(is_file($to)) unlink($to);
		if(link($from, $to)) {
			return new self($to);
		}else{
			return false;
		}
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Fslink/Rb_Fslink_Abstract#suppress()
	 */
	public function suppress(){
		Ranchbe::getError()->push(Rb_Error::INFO, array('file'=>$this->_path),
                                              'try to suppress hardlink %file%');
		if(!$this->test()) return false;
		if(!Rb_Filesystem::limitDir($this->_path)) return false;
		if(!unlink($this->_path)){
			Ranchbe::getError()->push(Rb_Error::ERROR, array('file'=>$this->_path),
                                        'error during suppressing hardlink %file%');
			return false;
		}
		Ranchbe::getError()->push(Rb_Error::INFO, array('file'=>$this->_path),
                                             'hardlink %file% has been suppressed');
		return true;
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Fslink/Rb_Fslink_Abstract#test()
	 */
	public function test(){
		if(!readlink($this->_path)){
			Ranchbe::getError()->push(Rb_Error::INFO, array('file'=>$this->_path),
                                                '%file% is not a hard link');
			return false;
		}else return true;
	}//End of method

} //End of class
