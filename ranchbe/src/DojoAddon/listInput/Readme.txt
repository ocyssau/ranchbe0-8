Exemple et téléchargement

Vous pouvez consulter l'exemple et récupérer la dernière version du source ici :
http://ben.dojotoolkit-fr.org/test_ListInput.html
Pour que cela fonctionne

N'oubliez pas le require :
dojo.require("dojox.form.SimpleManageableList")
De plus vous devez placer les fichiers :

    * ListInput.js dans dojox/form/
    * ListInput.css dans dojox/form/resources/
    * _ListInputItem_x.gif dans dojox/form/resources/images/

Track

Ce widget est disponible sur le track de dojo : http://bugs.dojotoolkit.org/ticket/8622