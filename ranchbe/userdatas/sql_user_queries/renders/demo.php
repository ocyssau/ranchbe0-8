<?php
function excel_render(RbView_Adodb_ExcelRender &$excelRender){

  //Write a big title on line 0
  $format_big =& $excelRender->workbook->addFormat();
  $format_big->setBold();
  $format_big->setSize(20);
  $format_big->setAlign('merge');
  $format_big->setUnderline(1);
  //$format_big->setHAlign('center'); //left, center, right, fill, justify, merge, equal_space
  $format_big->setVAlign('vcenter'); //top, vcenter, bottom, vjustify, vequal_space
  $excelRender->worksheet->write(0, 2, $excelRender->title, $format_big);
  $excelRender->worksheet->write(0, 3, '', $format_big);
  $excelRender->worksheet->write(0, 4, '', $format_big);

  /* This freezes the first six rows of the worksheet: */
  $excelRender->worksheet->freezePanes(array(2, 0));

  //Add logo
  //insertBitmap (integer $row, integer $col, string $bitmap [, integer $x=0 [, integer $y=0 [, integer $scale_x=1 [, integer $scale_y=1]]]])
  $excelRender->worksheet->setRow(0,150);
  $excelRender->worksheet->setColumn(0,0,30); // row 0 col 0
  //$excelRender->worksheet->insertBitmap(0, 0, 'img/ranchBe_logo_190.bmp', 0, 0, 1, 1);

  //An header and footer
  //Worksheet::setHeader (string $string [, float $margin=0.5])
  $excelRender->worksheet->setHeader( $excelRender->title );
  $excelRender->worksheet->setFooter ( 'From RanchBE' );

  //Set to landscape
  $excelRender->worksheet->setLandscape();

  //Worksheet::centerHorizontally -- Center the page horizontally.
  $excelRender->worksheet->centerHorizontally();
  //Worksheet::centerVertically -- Center the page vertically.
  $excelRender->worksheet->centerVertically();
  //Worksheet::setMargins_LR -- Set the left and right margins to the same value in inches.  
  $excelRender->worksheet->setMargins_LR(0.5);
  //Worksheet::setMargins_TB -- Set the top and bottom margins to the same value in inches.
  $excelRender->worksheet->setMargins_TB(0.5);

  //Fir to page 1 in horizontal 1 in vartical
  $excelRender->worksheet->fitToPages(1,1);

  $excelRender->begin_line = 1;
  $excelRender->begin_col = 0;
  
  //The body lines format
  $format['body'] =& $excelRender->workbook->addFormat(array('Size' => 10,
                                        'bold'    => 0,
                                        'Align' => 'left'));

  //The title lines format
  $format['title'] =& $excelRender->workbook->addFormat(array('Size' => 12,
                                                  'bold'    => 1,
                                                  'Align' => 'center'));

  $format['title']->setColor('yellow');
  $format['title']->setPattern(1);
  $format['title']->setFgColor('blue');
  $format['title']->setTextRotation(270); //0,90,270,-1

  return $format;
}

