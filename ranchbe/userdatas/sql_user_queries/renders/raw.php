<?php
function excel_render(RbView_Adodb_ExcelRender &$excelRender){

  //The body lines format
  $format['body'] =& $excelRender->workbook->addFormat(array('Size' => 10,
                                        'bold'    => 0,
                                        'Align' => 'left'));

  //The title lines format
  $format['title'] =& $excelRender->workbook->addFormat(array('Size' => 12,
                                                  'bold'    => 1,
                                                  'Align' => 'center'));

  $excelRender->begin_line = 0;
  $excelRender->begin_col = 0;

  return $format;
}


