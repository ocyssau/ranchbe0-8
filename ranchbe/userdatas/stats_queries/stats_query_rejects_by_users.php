<?php

function stats_query_rejects_by_users(Rb_Object_Permanent $ofObject) {
	
	//-------------- Query users which have submit a request ----------------------
	$document_table = Rb_Document::get($ofObject->getSpaceName())->getDao()->getTableName();
	$document_history_table = Rb_Document::get($ofObject->getSpaceName())->getDao()->getTableName('history');
	
	//Count les requete initiales
	$query = "SELECT COUNT(instance_id) AS count_submit , owner FROM " 
			. $document_history_table . " as histo
            JOIN galaxia_instances as instance
            ON histo.instance_id = instance.instanceId
            WHERE action_name = 'ChangeState'
            AND document_state = 'a_verifier'
            AND status = 'completed'
            GROUP BY owner
            ";
	
	if ( $ofObject->getId() ) {
		$query = $query . ' AND ' . $ofObject->getDao()->getFieldName() . ' = ' . $ofObject->getId();
		$Rset = Ranchbe::getDb ()->Execute ( $query );
		if( !$Rset ) return false;
		$Rsubmit = $Rset->GetArray ();
	}
	
	//Count les rejets
	$query = "SELECT COUNT(instance_id) AS count_reject , owner FROM " . $document_history_table . " as histo
            JOIN galaxia_instances as instance
            ON histo.instance_id = instance.instanceId
            WHERE action_name = 'ChangeState'
            AND document_state = 'rejete'
            AND status = 'completed'
            GROUP BY owner
            ";
	if ( $ofObject->getId() ) {
		$query = $query . ' AND ' . $ofObject->getDao()->getFieldName() . ' = ' . $ofObject->getId();
		$Rset = Ranchbe::getDb ()->Execute ( $query );
		$Rreject = $Rset->GetArray ();
	}
	
	//Count les approuve
	$query = "SELECT COUNT(instance_id) AS count_approve , owner FROM " . $document_history_table . " as histo
            JOIN galaxia_instances as instance
            ON histo.instance_id = instance.instanceId
            WHERE action_name = 'ChangeState'
            AND document_state = 'approuve'
            AND status = 'completed'
            GROUP BY owner
            ";
	
	if ( $ofObject->getId() ) {
		$query = $query . ' AND ' . $ofObject->getDao()->getFieldName() . ' = ' . $ofObject->getId();
	}
	
	$Rset = Ranchbe::getDb ()->Execute ( $query );
	if (! $Rset){
		print 'error on query<br />' . $query;
		return false;
	}
	$Rapprove = $Rset->GetArray ();
	
	//echo '<pre>';
	//var_dump($Rsubmit);
	//var_dump($Rreject);
	//var_dump($Rapprove);
	

	//Extract list of users
	if (! $Rsubmit)
		$Rsubmit = array ();
	foreach ( $Rsubmit as $t ) {
		if ($t ['owner'] != 'Not Interactive')
			$users [] = $t ['owner'];
	}
	if (is_array ( $users ))
		$users = array_unique ( $users );
	else
		return false;
		//var_dump($users);
	

	//Invert array : owner becomes key
	foreach ( $Rsubmit as $t ) {
		$Isubmit [$t ['owner']] = $t ['count_submit'];
	}
	foreach ( $Rapprove as $t ) {
		$Iapprove [$t ['owner']] = $t ['count_approve'];
	}
	foreach ( $Rreject as $t ) {
		$Ireject [$t ['owner']] = $t ['count_reject'];
	}
	
	$graphs [] ['text'] = "<h1>Taux d'erreurs par utilisateur</h2><br />";
	
	//-------------- Get statistics on each user ----------------------
	foreach ( $users as $user ) {
		$submit = $Isubmit [$user];
		$reject = $Ireject [$user];
		$approve = $Iapprove [$user];
		
		$text = "Number of submit for $user : " . $submit . '<br />';
		$text .= "Number of reject for $user : " . $reject . '<br />';
		$text .= "Number of accept for $user : " . $approve . '<br />';
		if ($submit != 0) {
			$quality_ratio [$user] = round ( ($approve / $submit) * 100, 1 );
			$text .= "Quality ratio : " . $quality_ratio [$user] . '%' . '<br />';
		}
		$graphs [] = array ('text' => $text );
		
		//Display graph
		$legend = array ('rejete', 'approuve' );
		$values = array ($reject, $approve );
		$title = urlencode ( 'Quality ratio for ' . $user );
		$values = urlencode ( serialize ( $values ) );
		$legend = urlencode ( serialize ( $legend ) );
		$graphs [] = array ('image' => "<img src='graphs/graphPie.php?title=$title&values=$values&legend=$legend' alt='no graph'/>", 'text' => '' );
	} //End of foreach $users
	return $graphs;
	
/*
  if(!empty($controller->container_id))
    $query = $query.' AND '.$controller->space->CONT_FIELDS_MAP_ID.' = '.$controller->container_id;

  if($Rset = Ranchbe::getDb()->Execute($query)){
    if($Rset->RecordCount() === 0) return false;
    while ($row = $Rset->FetchRow()){
      $users[] = $row['owner'];
      if($row['status'] == 'completed')
        $state[$row['owner']][] = $row['document_state'];
      if($row['status'] == 'active')
        $active[$row['owner']][] = $row['status'];
    }
  }else{print 'error in query'; return false;}

  $users = array_unique($users);
*/

//-------------- Get statistics on each user ----------------------
/*
  foreach($users as $user){
    $Count_state = array_count_values($state[$user]);

    //Set the text to display
    $rejete   = $Count_state['rejete'];
    $approuve = $Count_state['approuve'];
    $a_verifier = $Count_state['a_verifier'];

    $text =  "Number of submit for $user : " . $a_verifier . '<br />';
    $text .=  "Number of reject for $user : " . $rejete . '<br />';
    $text .=  "Number of accept for $user : " . $approuve . '<br />';
    if($a_verifier != 0){
      $quality_ratio[$user] = round(($approuve / $a_verifier) * 100 , 1);
      $text .=  "Quality ratio : " . $quality_ratio[$user] . '%'.'<br />';
    }
    $graphs[] = array('text' => $text);

    //Display graph
    $legend = array('rejete','approuve');
    $values = array($rejete , $approuve);
    $title = urlencode ('Quality ratio for '.$user);
    $values = urlencode(serialize($values));
    $legend = urlencode(serialize($legend));
    $graphs[] = array(
              'image' => "<img src='./inc/graphs/graphPie.php?title=$title&values=$values&legend=$legend' alt='no graph'/>" ,
              'text' => '');
  
  } //End of foreach $users


  return $graphs;
*/

}


