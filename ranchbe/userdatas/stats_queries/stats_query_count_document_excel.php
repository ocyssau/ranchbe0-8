<?php

function excel_render(&$graphs){

  require_once "Spreadsheet/Excel/Writer.php";
  // Creating a workbook
  $workbook = new Spreadsheet_Excel_Writer();
  // sending HTTP headers
  $workbook->send($container->getName().'_content.xls');
  // Creating a worksheet
  $worksheet =& $workbook->addWorksheet('sheet1');

  //The header
  $format =& $workbook->addFormat(array('Size' => 10,
                                        'bold'    => 1,
                                        'Align' => 'center'));

  $worksheet->write(0, 0, 'Number', $format);
  $worksheet->write(0, 1, 'Description', $format);
  $worksheet->write(0, 2, 'Category', $format);
  $worksheet->write(0, 3, 'CheckOut By', $format);
  $worksheet->write(0, 4, 'CheckOut Date', $format);
  $worksheet->write(0, 5, 'Create By', $format);
  $worksheet->write(0, 6, 'Create Date', $format);
  $worksheet->write(0, 7, 'Last Update By', $format);
  $worksheet->write(0, 8, 'Last Update Date', $format);
  $worksheet->write(0, 9, 'access', $format);
  $worksheet->write(0, 10, 'State', $format);
  $worksheet->write(0, 11, 'Indice', $format);
  $worksheet->write(0, 12, 'Version', $format);
  $worksheet->write(0, 13, 'Type', $format);

  //Get extends metadatas header
  $optionalFields =  Rb_Container_Relation_Docmetadata::get()
                                            ->getMetadatas($container->getId());
  if(is_array($optionalFields)){
    $col = 14;
    foreach($optionalFields as $field){
      $worksheet->write(0, $col, $field['property_description'], $format);
      $col++;
    }
  }
  
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('modifier','category');
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('modifier','username');
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('modifier','date_format');
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('modifier','indice');
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('modifier','type');
  require_once Ranchbe::getSmarty()->_get_plugin_filepath('function','filter_select');

  // The actual data
  $i = 0;
  while($lin < count($list)){
    $lin = $i+1;
    $worksheet->write($lin, 0, $list[$i]['document_number']);
    $worksheet->write($lin, 1, $list[$i]['description']);
    $worksheet->write($lin, 2, smarty_modifier_category($list[$i]['category_id']));
    $worksheet->write($lin, 3, smarty_modifier_username($list[$i]['check_out_by']));
    $worksheet->write($lin, 4, smarty_modifier_date_format($list[$i]['check_out_date']));
    $worksheet->write($lin, 5, smarty_modifier_username($list[$i]['open_by']));
    $worksheet->write($lin, 6, smarty_modifier_date_format($list[$i]['open_date']));
    $worksheet->write($lin, 7, smarty_modifier_username($list[$i]['update_by']));
    $worksheet->write($lin, 8, smarty_modifier_date_format($list[$i]['update_date']));
    $worksheet->write($lin, 9, $list[$i]['document_access_code']);
    $worksheet->write($lin, 10, $list[$i]['document_state']);
    $worksheet->write($lin, 11, smarty_modifier_indice($list[$i]['document_version']));
    $worksheet->write($lin, 12, $list[$i]['document_iteration']);
    $worksheet->write($lin, 13, smarty_modifier_type($list[$i]['doctype_id']));
    $col = 14;
    foreach($optionalFields as $field){
      $p = array( 'id'=>$list[$lin][$field['property_name']],
                  'type'=>$field['property_type'] );
      $worksheet->write($lin, $col, smarty_function_filter_select($p));
      $col++;
    }
    $i++;
  }
  // Let's send the file
  $workbook->close();
  return true;
}








//Count files
/*
$query = "SELECT file_id FROM $Manager->DOC_FILE_TABLE";
$Count_Doc_File = count_query($query);
*/
function stats_query_count_document_excel(){

  global $Manager;

  //Count documents
  $query = 'SELECT document_id , doctype_id , document_version, document_state, category_id 
            FROM '.$Manager->space->DOC_TABLE;

  if(!empty($_REQUEST['container_id']))
    $query = $query.' WHERE '.$Manager->space->CONT_FIELDS_MAP_ID.' = '.$_REQUEST['container_id'];
  
  $doctype=array(); $indice=array(); $state=array(); $category=array();
  
  if(!$Rset = Ranchbe::getDb()->Execute($query)){
    print 'error on query: '.Ranchbe::getDb()->ErrorMsg().'<br />'.$query;
    return false;
  }else{
    $Count_Doc = $Rset->RecordCount();
    while ($row = $Rset->FetchRow()) {
      $doctype[] = $row['doctype_id'];
      $indice[]  = $row['document_version'];
      $state[]  = $row['document_state'];
      if(!is_null($row['category_id']))
        $category[] = $row['category_id'];
    }
  }

  //Total of document
  $text =  "<li>Total of document : $Count_Doc</li>";
  $graphs[] = array(
            'text' => $text);
  
  //Count documents by type
  $Count_Type = array_count_values($doctype);
  //Display graph for document by type
  $values = array();$legend = array();
  $text =  "<li>Count documents by type :</li><ul>";
  foreach($Count_Type as $key=>$value ){
    $values[] = $value;
    $legend[] = typeName($key);
    $text .= '<li>'. typeName($key) .' : '. $value .'</li>';}
  $title = urlencode ('Documents by type');
  $values = urlencode(serialize($values));
  $legend = urlencode(serialize($legend));
  $text .= "</ul>";
  $graphs[] = array(
            'image' => "<img src='./inc/graphs/graphBar.php?title=$title&values=$values&legend=$legend' alt='no graph'/>" ,
            'text' => $text
            );


   //Count documents by indice
  $Count_Indice = array_count_values($indice);

  //Display graph for document by indice
  $values = array();$legend = array();
  $text =  "<li>Count documents by indice :</li><ul>";
  foreach($Count_Indice as $key=>$value ){
    $values[] = $value;
    $legend[] = indiceName($key);
    $text .= '<li>'. indiceName($key) .' : '. $value .'</li>';
  }
  $title = urlencode ('Documents by indice');
  $values = urlencode(serialize($values));
  $legend = urlencode(serialize($legend));
  $text .= "</ul>";
  $graphs[] = array(
            'image' => "<img src='./inc/graphs/graphBar.php?title=$title&values=$values&legend=$legend' alt='no graph'/>" ,
            'text' => $text);
  
  //Count documents by state
  $Count_State = array_count_values($state);
  //Display grap for document by state
  $values = array();$legend = array();
  $text =  "<li>Count documents by state :</li><ul>";
  foreach($Count_State as $key=>$value ){
    $values[] = $value;
    $legend[] = $key;
    $text .= '<li>'. $key .' : '. $value .'</li>';}
  $title = urlencode ('Documents by state');
  $values = urlencode(serialize($values));
  $legend = urlencode(serialize($legend));
  $text .= "</ul>";
  $graphs[] = array(
            'image' => "<img src='./inc/graphs/graphPie.php?title=$title&values=$values&legend=$legend' alt='no graph'/>" ,
            'text' => $text);
  
  //Count documents by categories
  $Count_Category = array_count_values($category);
  //Display grap for document by state
  $values = array();$legend = array();
  $text =  "<li>Count documents by category :</li><ul>";
  foreach($Count_Category as $key=>$value ){
    $values[] = $value;
    $legend[] = categoryName($key);
    $text .= '<li>'. categoryName($key) .' : '. $value .'</li>';
  }
  $title = urlencode ('Documents by category');
  $values = urlencode(serialize($values));
  $legend = urlencode(serialize($legend));
  $text .= "</ul>";
  $graphs[] = array(
            'image' => "<img src='./inc/graphs/graphBar.php?title=$title&values=$values&legend=$legend' alt='no graph'/>" ,
            'text' => $text);
  
  var_dump($graphs);

  return $graphs;
  
}

?>
