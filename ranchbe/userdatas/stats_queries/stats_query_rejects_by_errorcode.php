<?php

function stats_query_rejects_by_errorcode(Rb_Object_Permanent $ofObject) {
	
	// ---------------------- DISPLAY ERRORS BY TYPE ------------------------------- 
	$document_table = Rb_Document::get($ofObject->getSpaceName())->getDao()->getTableName();
	$document_history_table = Rb_Document::get($ofObject->getSpaceName())->getDao()->getTableName('history');
	

	//Query all reject from history table and galaxia_instance table
	$query = 'SELECT document_number,action_by,started,owner,status,properties ' 
			. ' FROM ' . $document_history_table . ' as histo ' 
			. ' JOIN galaxia_instances as instance ' 
			. ' ON histo.instance_id = instance.instanceId' 
			. ' WHERE action_name = \'ChangeState\'' 
			. ' AND document_state = \'rejete\'';
			
	if ( $ofObject->getId() )
		$query = $query . ' AND ' . $ofObject->getDao()->getFieldName() . ' = ' . $ofObject->getId();
			
	//Execute query and get the recordset in Rset var
	if ($Rset = Ranchbe::getDb ()->Execute ( $query )) {
		if ($Rset->RecordCount () === 0)
			return false;
		$cumul_errors = array ();
		$cumul_errors_by_user = array ();
		while ( $row = $Rset->FetchRow () ) {
			$props = unserialize ( $row ['properties'] ); //unserialize the properties wich are store in BLOB type field indatabase
			$users [] = $row ['owner']; // Users wich have submit a workflow
			if (is_null ( $props ['error_code'] ))
				$props ['error_code'] = array ('unknow' );
			if (is_array ( $props ['error_code'] )) {
				//Get all error code in a array to count it
				$cumul_errors = array_merge ( $cumul_errors, $props ['error_code'] );
				if (is_array ( $cumul_errors_by_user [$row ['owner']] ))
					$cumul_errors_by_user [$row ['owner']] = array_merge ( $cumul_errors_by_user [$row ['owner']], $props ['error_code'] );
				else
					$cumul_errors_by_user [$row ['owner']] = $props ['error_code'];
			}
		}
	} else {
		print 'error on query<br />' . $query;
		return false;
	}
	
	//Count number of each error
	$Count_error = array_count_values ( $cumul_errors );
	
	//Set the text to display
	$text = "<h1>Erreurs par type</h2><br />";
	foreach ( $Count_error as $error_type => $val ) {
		$text .= 'Nombre d\'erreurs "' . $error_type . '" :' . $val . '<br />';
	}
	$graphs [] = array ('text' => $text );
	
	//Display graph
	$values = array ();
	$legend = array ();
	foreach ( $Count_error as $key => $value ) {
		$values [] = $value;
		$legend [] = $key;
	}
	$title = urlencode ( 'Erreurs par type' );
	$values = urlencode ( serialize ( $values ) );
	$legend = urlencode ( serialize ( $legend ) );
	$graphs [] = array ('image' => "<img src='./graphs/graphBar.php?title=$title&values=$values&legend=$legend' alt='no graph'/>", 'text' => '' );
	
	// ---------------------- DISPLAY ERRORS BY TYPE AND BY USER -------------------------------
	

	$users = array_unique ( $users );
	
	foreach ( $users as $user ) {
		if (is_array ( $cumul_errors_by_user [$user] ))
			$Count_error = array_count_values ( $cumul_errors_by_user [$user] );
		else
			continue;
			
		//Set the text to display
		$text = "<h1>Erreurs par type pour l'utilisateur " . $user . "</h2><br />";
		foreach ( $Count_error as $error_type => $val ) {
			$text .= 'Nombre d\'erreurs "' . $error_type . '" :' . $val . '<br />';
		}
		$graphs [] = array ('text' => $text );
		
		//Display graph
		$values = array ();
		$legend = array ();
		foreach ( $Count_error as $key => $value ) {
			$values [] = $value;
			$legend [] = $key;
		}
		$title = urlencode ( "Erreurs par type pour " . $user );
		$values = urlencode ( serialize ( $values ) );
		$legend = urlencode ( serialize ( $legend ) );
		$graphs [] = array ('image' => "<img src='./graphs/graphBar.php?title=$title&values=$values&legend=$legend' alt='no graph'/>", 'text' => '' );
	}
	
	return $graphs;

}
