<?php

class containerScript_exemple{

  function doc_pre_updateProperties(&$document){
    //On affiche le nom du document a creer a l'utilisateur
    print '<hr />';
    print '<b>INFORMATIONS :</b><br />';
    print '<b>Les modifications que vous avez apport�s � la famille de document ou au num�ro seront annul�es</b>';
    print '<hr />';
  
    $document->UnSetDocProperty('docsier_famille');
    $document->UnSetDocProperty('document_number');
  
    return true;
  }

  //Ce scripts permet de renommer les documents cr��s dans DocSier.
  //Les documents sont cr��s tel que '[num�ro de famille]-[num�ro d'ordre incr�mental]'
  //Le numero d'ordre suit le num�ro qui vient a l'int�rieur de la famille
  function doc_pre_store(&$document){
  
    $doc_properties = $document->getProperties();
    //var_dump($doc_properties);
  
    //On r�cup�re le num�ro incr�mental de la famille
    $query = 'SELECT document_number FROM bookshop_documents WHERE docsier_famille = '.$doc_properties['docsier_famille'];
    if ($rset = $document->dbranchbe->Execute($query)){
      $rset->MoveLast();
      $last = $rset->FetchRow();
      $last_doc = $last['document_number'];
    }
    
    //On recupere l id du dernier document
    $last_doc = explode('-' , $last_doc);
    $id = $last_doc[1];
    //echo $id.'<br>';
    
    //on incr�ment l'id
    $id = $id + 1;
    //echo $id.'<br>';
    
    //on formate l'id pour avoir 2 chiffres minimum
    $id = sprintf('%02d' , $id);
    //echo $id.'<br>';
    
    //On nomme le document comme il faut
    if($doc_properties['docsier_format'] == 'catalogue')
      $document->setProperty('document_number', $doc_properties['docsier_famille'].'-'.$id.'-C');
    else
      $document->setProperty('document_number', $doc_properties['docsier_famille'].'-'.$id);
    
    //On affiche le nom du document a creer a l'utilisateur
    print '<hr />';
    print '<h2>Nouveau num�ro de document : </h2>';
    print '<b>'.$document->getProperty('document_number').'</b><br />';
    print '<hr />';
    
    return true;
  }
}

