<?php

require_once('doctypes_scripts/doctype_script_lib.php');

class doctypeScript_catdrawing_A350{
  function doc_post_store(&$document){
    $ret0 = checkAttachment($document, true);
    $ret2 = doctypeScript_doc_associate_qcseal($document);
    $ret1 = doctypeScript_doc_associate_pdf($document);
    if( $ret1==false && $ret2==false ) return false;
    return true;
  }

  function doc_post_update(&$document){
    $ret0 = checkAttachment($document, true);
    $ret2 = doctypeScript_doc_associate_qcseal($document);
    $ret1 = doctypeScript_doc_associate_pdf($document);
    if( $ret1==false && $ret2==false ) return false;
    return true;
  }

}
?>
