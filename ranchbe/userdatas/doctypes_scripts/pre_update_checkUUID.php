<?php

//Verifie que le UUID de chaque fichier associ� au document est = au
//uuid des fichiers sotck�s dans le vault
// Remarque : si le fichier n'existe pas encore dans le vault alors le uuid sera 
// d�t�ct� comme faux!!
//Il verifie aussi que les nom internes (internal name) soient corrects.
class doctypeScript_pre_update_checkUUID{
  function doc_pre_update(&$document){
    require_once('lib/CheckUUID.php');
  
    $docfiles =& $document->GetDocfiles(); //Get all docfiles
  
    if( is_array($docfiles) ){
      $wildspace = $docfiles[0]->GetWildspace();
      $wildspace_path = $wildspace->GetPath();
      foreach( $docfiles as $docfile ){
        $file_name = $docfile->GetFileName();
        $file_path = $docfile->GetProperty('file_path');
  
        //Get the uuid of the new file
        $newUUID = checkUUID( $wildspace_path .'/'. $file_name );
        echo $newUUID['UUID'] .'<br />';
  
        //Get the uuid of the file in vault
        $oldUUID = checkUUID( $file_path . '/'. $file_name );
        echo $oldUUID['UUID'] .'<br />';
  
        if( $newUUID['UUID'] !== $oldUUID['UUID']){
          print 'error : the UUID is not concordant <br />';
          return false;
        }
        
        if( $newUUID['CATIA']['InternalName'] !== $oldUUID['CATIA']['InternalName']){
          print 'error : the internal name is not concordant <br />';
          return false;
        }
      }
    }
    return true;
    
  }
}
?>
