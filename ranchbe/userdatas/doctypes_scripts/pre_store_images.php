<?php
class doctypeScript_pre_store_images{
  /* Renome le document pour eviter un conflit avec le draw */
  function pre_store(&$document){
    echo '<b>Image pre store scripts : </b><br />';
    
    $docfiles =& $document->GetDocfiles();
    $docfile =& end($docfiles);
    $fsdata =& $docfile->GetFsdata();
    $document_number = $fsdata->getProperty('file_root_name').'_image';
  
    $document->SetDocProperty('document_number', $document_number);
  
    return true;
  }
}
?>
