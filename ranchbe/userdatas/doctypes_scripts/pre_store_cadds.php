<?php

class doctypeScript_pre_store_cadds{

//Example of pre store scripts
function doc_pre_store(&$document){
  echo 'pre store script example for cadds';

  $docfile =& $document->GetDocfile(0); //Get the main docfile
  if( !is_object($docfile) ){
    return false;
  }

  $fsdata =& $docfile->GetFsdata();
  $file = $fsdata->getProperty('file_path'); //full path to cadds directory

  //check presence of temp files
  if (!empty(glob( "$file" .'/'. "*LOCK*" ))) $ret = "presence de fichiers LOCK, <br/>";
  if (!empty(glob( "$file" .'/'. "*TEMP*" ))) $ret .= "presence de fichiers TEMP, <br/>";
  if (!empty(glob( "$file" .'/'. "*nfs*" ))) $ret .= "presence de fichiers nfs, <br/>";
  if (!empty(glob( "$file" .'/'. "_fd*VP*" ))) $ret .= "presence de fichiers _fd.VP, <br/>";
  if (!empty(glob( "$file" .'/'. "_pd*VP*" ))) $ret .= "presence de fichiers _pd.VP, <br/>";

  //check presence of gbf
  if ( !is_file("$file" .'/'. "_pd.gbf") ) $ret .= "le gbf est abscent, <br/>";
  
  //check gbf time
  if ( filemtime("$file" .'/'. "_pd.gbf") < filemtime("$file" .'/'. "_pd") ) $ret .= "le gbf n'est pas � jour, <br/>";
  
  if ( isset ($ret) ) {
    echo "$ret";
    return false;
  }

  return true;

}

}//end of class

?>
