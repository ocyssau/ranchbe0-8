<?php
//This scripts is used with doctype for pre_update and pre_store scripts.
//Check if attachments files are availables and check dates between mail file and attachment file
//Attach files to document

class doctypeScript_checkAttachment{
  function doc_pre_update(&$document){
  	$scriptsPath = Ranchbe::getConfig()->path->scripts->doctype;
    require_once($scriptsPath . '/doctype_script_lib.php');
    return checkAttachment($document, true, false);
  }

  function doc_pre_store(&$document){
  	$scriptsPath = Ranchbe::getConfig()->path->scripts->doctype;
    require_once($scriptsPath . '/doctype_script_lib.php');
    return checkAttachment(&$document, true, false);
  }
}
