<?php

//Associe le pdf s'il existe dans le wildspace et s'il n'est pas d�ja associ�.
function doctypeScript_doc_associate_pdf(&$document){
	$docfile =& $document->getDocfile('main'); //Get the main file
	if(!is_object($docfile) ) return true; //s'il n'y a pas de fichier associ�s on sort
	$file_name = $docfile->getProperty('file_root_name');
	$wildspace =& Rb_User::getCurrentUser()->getWildspace();

	//Chemin du pdf dans le wildspace
	$pdf_file = $file_name.'.pdf';
	$pdf = $wildspace->getPath().'/'.$pdf_file;

	//Verifie que le pdf n'est pas deja attache
	$afiles = $document->getAssociatedFiles();
	if( is_array($afiles) ){
		foreach($afiles as $f ){
			if ($f['file_name'] == $pdf_file){
				return true; //si le pdf est deja associe on sort
			}
		}
	}

	if( is_file($pdf) ){
		$document->associateFile($pdf , true, 1); //Associe le .pdf au document
	}

	return true;
}

//Associe le qcseal s'il existe dans le wildspace et s'il n'est pas d�ja associ�.
function doctypeScript_doc_associate_qcseal(&$document){
	$docfile =& $document->getDocfile('main'); //Get the main file
	if(!is_object($docfile) ) return true; //s'il n'y a pas de fichier associ�s on sort
	$file_name = $docfile->getProperty('file_name');
	$wildspace =& Rb_User::getCurrentUser()->getWildspace();

	//Chemin du qcseal dans le wildspace
	$qcseal_file = $file_name.'.qcseal';
	$qcseal = $wildspace->getPath().'/'.$qcseal_file;

	//Verifie que le qcseal n'est pas deja attache
	$afiles = $document->getAssociatedFiles();
	if( is_array($afiles) ){
		foreach($afiles as $f ){
			if ($f['file_name'] == $qcseal_file){
				return true; //si le qcseal est deja associe on sort
			}
		}
	}

	if( is_file($qcseal) ){
		$document->associateFile($qcseal , true); //Associe le .qcseal au document
	}

	return true;
}

/* brief! create an attachment
 param $document, object
 */
function checkAttachment(&$document, $check_date=true, $check_file=false){
	//to doing this scripts valid for pre-update and pre-store
	$wildspace =& Rb_User::getCurrentUser()->getWildspace();
	$mainfile =& $document->getDocfile('main');

	if(!isset($fsdata)){
		//require_once('core/fsdata.php');
		$fsdata = new Rb_Fsdata($wildspace->getPath().'/'.$mainfile->getProperty('file_name'));
	}

	$pfile = $wildspace->getPath().'/'.$document->getProperty('document_number').'.jpg'; //picture file path

	//If a visualisation file type is set for doctype get visu file
	$doctype =& $document->getDoctype();
	$vfile_ext = $doctype->getProperty('visu_file_extension'); //Get visu file extension from doctype property
	if(!empty($vfile_ext)){
		$vfile = $wildspace->getPath().'/'.$document->getProperty('document_number') . $vfile_ext; //visufile path
	}

	$mainfile_mtime = $fsdata->getProperty('file_mtime');
	foreach(array($vfile, $pfile) as $afile){
		if(empty($afile)) continue; //astuce to choose test or not
		if( is_file($afile) && $check_date ){
			//Compute the shift time :
			$time_shift = filemtime($afile) - $mainfile->getProperty('file_mtime');
			//Check shift time. Attachment can not be more old of 5min that main file
			if ( $time_shift < -300 ) {
				$document->error_stack->push(Rb_Error::WARNING, array('element1'=>$afile,'element2'=>$time_shift),
        'Attachment file %element1% is not todate'.'<br />'.
        'You must run catia scripts and retry'.'<br />'.
        '<i>shift time:</i> %element2% sec <br />'
        );
			}
		}else if ( $check_file ){
			$document->error_stack->push(Rb_Error::WARNING, array('element1'=>$afile),
      'Attachment file %element1% is not reachable'.'<br />');
		}
		/*
		 else{
		 $document->error_stack->push(Rb_Error::INFO, array('element1'=>$afile),
		 'Attachment file %element1% is not reachable'.'<br />'.
		 'You must run catia scripts and retry'
		 );
		 //return false;
		 }
		 */
	} //end foreach

	//require_once('core/attachment.php');
	//require_once('core/viewer.php');

	if(is_file($vfile)){
		$document->associateFile($vfile, true, Rb_Docfile_Role::VISU, 0);
		/*
		 $ovisu = new visu($document); //Construit un objet visu
		 $ovisu->setExtension($vfile_ext); //D�finie l'extension du fichier de visualisation
		 $ovisu->init_filepath(); //initialise le chemin vers le fichier de visualisation (ie: ./deposit_dir/AFF001/_attachments/visu/1.wrl)
		 if($ovisu->attach($vfile)){ //Copie le fichier $vfile vers le path d�finis par la m�thode init_filepath
		 //unlink($vfile); //Supprime le fichier d'origine pour nettoyer le wildspace
		 }
		 */
	}
	if(is_file($pfile)){
		$document->associateFile($pfile, true, Rb_Docfile_Role::PICTURE, 0);
		/*
		 $opicture = new picture($document);
		 $opicture->setExtension('.jpg');
		 $opicture->init_filepath();
		 if($opicture->attach($pfile)){
		 $opicture->generateThumb();
		 //unlink($pfile);
		 }
		 */
	}

	return true;
}

//Verifie que le UUID de chaque fichier associ� au document est = au
//uuid des fichiers sotck�s dans le vault
// Remarque : si le fichier n'existe pas encore dans le vault alors le uuid sera
// d�t�ct� comme faux!!
//Il verifie aussi que les nom internes (internal name) soient corrects.
function doctypeScript_doc_checkuuid(&$document){
	require_once('./lib/CheckUUID.php');

	$docfiles =& $document->GetDocfiles(); //Get all docfiles

	if( is_array($docfiles) ){
		$wildspace =& Rb_User::getCurrentUser()->getWildspace();
		foreach( $docfiles as $docfile ){
			$file_name = $docfile->getNumber();
			$file_path = $docfile->getProperty('file_path');

			//Get the uuid of the new file
			$newUUID = checkUUID( $wildspace->GetPath() .'/'. $file_name );
			echo $newUUID['UUID'] .'<br />';

			//Get the uuid of the file in vault
			$oldUUID = checkUUID( $file_path . '/'. $file_name );
			echo $oldUUID['UUID'] .'<br />';

			if( $newUUID['UUID'] !== $oldUUID['UUID']){
				print 'error : the UUID is not concordant <br />';
				return false;
			}

			if( $newUUID['CATIA']['InternalName'] !== $oldUUID['CATIA']['InternalName']){
				print 'error : the internal name is not concordant <br />';
				return false;
			}
		}
	}
	return true;
}


