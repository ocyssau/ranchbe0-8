<?php
class doctypeScript_pre_store_renameDrawings{
  //Example of pre store scripts
  function doc_pre_store(&$document){
    echo 'pre store script example rename';
  
    $docfile =& $document->GetDocfile(0); //Get the main docfile
    if( is_object($docfile) ){
      $fsdata =& $docfile->GetFsdata();
      $document_number = $fsdata->getProperty('file_root_name').'_drawing';
    }else {
      $document_number = $document->GetDocProperty('document_number').'_drawing';
    }
  
    $document->SetDocProperty('document_number', $document_number);
  
    return true;
  }
}
?>
