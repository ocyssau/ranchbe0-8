<?php
class doctypeScript_exemples{

  function doc_pre_store(&$document){
    echo 'pre store script example';
    $docfile =& $document->GetDocfile(0); //Get the main docfile
    if( is_object($docfile) ){
      $fsdata =& $docfile->GetFsdata();
      $document_number = $fsdata->getProperty('file_root_name').'_suffix';
    }else {
      $document_number = $document->GetDocProperty('document_number').'_suffix';
    }
    $document->SetDocProperty('document_number', $document_number);
    return true;
  }

  function doc_post_store(&$document){
    echo "<em>begin post store scripts"  .'</em><br />';
    require_once('doctypes_scripts/checkAttachment.php');
    return checkAttachment($document);
  }

  function doc_pre_update(&$document){
    echo 'pre update script example';
  
    $docfiles =& $document->GetDocfiles(); //Get all docfiles
  
    //Associated files to checkOut
    if( is_array($docfiles) )
      foreach( $docfiles as $docfile ){
        $file_name = $docfile->GetFileName();
        echo 'File name : '.$file_name.'<br />';
        $file_path = $docfile->GetProperty('file_path');
        echo 'File path : '.$file_path.'<br />';
      }
  
    return true;
  }

  function doc_post_update(&$document){
    echo "<em>begin post store scripts"  .'</em><br />';
    require_once('doctypes_scripts/checkAttachment.php');
    return checkAttachment($document);
  }


}
?>
