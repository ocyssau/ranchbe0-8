<?php

class datatypeScript_zipadraw{
  function recordfile_post_PutInWildspace(recordfile &$recordFile){
  
    $wildspace = $recordFile->GetWildspace();
    $file = $wildspace->GetPath().'/'.$recordFile->GetProperty('file_name');
    
    $zip = new ZipArchive;
    if ($zip->open($file) === TRUE) {
        $zip->extractTo( dirname($file) );
        $zip->close();
    }else{
      $recordFile->error_stack->push(Rb_Error::ERROR, array('file'=>$file), 'WARNING : can not uncompress file : %file%' );
    }
    return true;
  
  }
  
  function docfile_post_checkout(recordfile &$recordFile){
  
    $wildspace = $recordFile->GetWildspace();
    $file = $wildspace->GetPath().'/'.$recordFile->GetProperty('file_name');
    
    $zip = new ZipArchive;
    if ($zip->open($file) === TRUE) {
        $zip->extractTo( dirname($file) );
        $zip->close();
    }else{
      $recordFile->error_stack->push(Rb_Error::ERROR, array('file'=>$file), 'WARNING : can not uncompress file : %file%' );
    }
  
    unlink($file);
    return true;
  }
  
  function docfile_pre_checkout(recordfile &$recordFile){
  
    $wildspace = $recordFile->GetWildspace();
    $natif_file_name = str_replace('.', '/', $recordFile->GetProperty('file_root_name') );
    $natif_file = $wildspace->GetPath().'/'.$natif_file_name;
  
    if(is_dir( $natif_file ) ) {
      $recordFile->error_stack->push(Rb_Error::ERROR, array('file'=>$natif_file), 'WARNING : the adraw directory : %file% is yet existing in wildspace' );
      $recordFile->stopIt = true;
      return false;
    }
  
    return true;
  
  }
}

?>
