//=====================================================================================
//From tiki-js.js
//=====================================================================================

var feature_no_cookie = 'n'; 

/**
 * DD_roundies, this adds rounded-corner CSS in standard browsers and VML sublayers in IE that accomplish a similar appearance when comparing said browsers.
 * Author: Drew Diller
 * Email: drew.diller@gmail.com
 * URL: http://www.dillerdesign.com/experiment/DD_roundies/
 * Version: 0.0.2a
 * Licensed under the MIT License: http://dillerdesign.com/experiment/DD_roundies/#license
 *
 * Usage:
 * DD_roundies.addRule('#doc .container', '10px 5px'); // selector and multiple radii
 * DD_roundies.addRule('.box', 5, true); // selector, radius, and optional addition of border-radius code for standard browsers.
 * 
 * Just want the PNG fixing effect for IE6, and don't want to also use the DD_belatedPNG library?  Don't give any additional arguments after the CSS selector.
 * DD_roundies.addRule('.your .example img');
 **/





var DD_roundies = {

		ns: 'DD_roundies',

		IE6: false,
		IE7: false,
		IE8: false,
		IEversion: function() {
	if (document.documentMode != 8 && document.namespaces && !document.namespaces[this.ns]) {
		this.IE6 = true;
		this.IE7 = true;
	}
	else if (document.documentMode == 8) {
		this.IE8 = true;
	}
},
querySelector: document.querySelectorAll,
selectorsToProcess: [],
imgSize: {},

createVmlNameSpace: function() { /* enable VML */
	if (this.IE6 || this.IE7) {
		document.namespaces.add(this.ns, 'urn:schemas-microsoft-com:vml');
	}
	if (this.IE8) {
		document.writeln('<?import namespace="' + this.ns + '" implementation="#default#VML" ?>');
	}
},

createVmlStyleSheet: function() { /* style VML, enable behaviors */
	/*
			Just in case lots of other developers have added
			lots of other stylesheets using document.createStyleSheet
			and hit the 31-limit mark, let's not use that method!
			further reading: http://msdn.microsoft.com/en-us/library/ms531194(VS.85).aspx
	 */
	var style = document.createElement('style');
	document.documentElement.firstChild.insertBefore(style, document.documentElement.firstChild.firstChild);
	if (style.styleSheet) { /* IE */
		try {
			var styleSheet = style.styleSheet;
			styleSheet.addRule(this.ns + '\\:*', '{behavior:url(#default#VML)}');
			this.styleSheet = styleSheet;
		} catch(err) {}
	}
	else {
		this.styleSheet = style;
	}
},

/**
 * Method to use from afar - refer to it whenever.
 * Example for IE only: DD_roundies.addRule('div.boxy_box', '10px 5px');
 * Example for IE, Firefox, and WebKit: DD_roundies.addRule('div.boxy_box', '10px 5px', true);
 * @param {String} selector - REQUIRED - a CSS selector, such as '#doc .container'
 * @param {Integer} radius - REQUIRED - the desired radius for the box corners
 * @param {Boolean} standards - OPTIONAL - true if you also wish to output -moz-border-radius/-webkit-border-radius/border-radius declarations
 **/
addRule: function(selector, rad, standards) {
	if (typeof rad == 'undefined' || rad === null) {
		rad = 0;
	}
	if (rad.constructor.toString().search('Array') == -1) {
		rad = rad.toString().replace(/[^0-9 ]/g, '').split(' ');
	}
	for (var i=0; i<4; i++) {
		rad[i] = (!rad[i] && rad[i] !== 0) ? rad[Math.max((i-2), 0)] : rad[i];
	}
	if (this.styleSheet) {
		if (this.styleSheet.addRule) { /* IE */
			var selectors = selector.split(','); /* multiple selectors supported, no need for multiple calls to this anymore */
			for (var i=0; i<selectors.length; i++) {
				this.styleSheet.addRule(selectors[i], 'behavior:expression(DD_roundies.roundify.call(this, [' + rad.join(',') + ']))'); /* seems to execute the function without adding it to the stylesheet - interesting... */
			}
		}
		else if (standards) {
			var moz_implementation = rad.join('px ') + 'px';
			this.styleSheet.appendChild(document.createTextNode(selector + ' {border-radius:' + moz_implementation + '; -moz-border-radius:' + moz_implementation + ';}'));
			this.styleSheet.appendChild(document.createTextNode(selector + ' {-webkit-border-top-left-radius:' + rad[0] + 'px ' + rad[0] + 'px; -webkit-border-top-right-radius:' + rad[1] + 'px ' + rad[1] + 'px; -webkit-border-bottom-right-radius:' + rad[2] + 'px ' + rad[2] + 'px; -webkit-border-bottom-left-radius:' + rad[3] + 'px ' + rad[3] + 'px;}'));
		}
	}
	else if (this.IE8) {
		this.selectorsToProcess.push({'selector':selector, 'radii':rad});
	}
},

readPropertyChanges: function(el) {
	switch (event.propertyName) {
	case 'style.border':
	case 'style.borderWidth':
	case 'style.padding':
		this.applyVML(el);
		break;
	case 'style.borderColor':
		this.vmlStrokeColor(el);
		break;
	case 'style.backgroundColor':
	case 'style.backgroundPosition':
	case 'style.backgroundRepeat':
		this.applyVML(el);
		break;
	case 'style.display':
		el.vmlBox.style.display = (el.style.display == 'none') ? 'none' : 'block';
		break;
	case 'style.filter':
		this.vmlOpacity(el);
		break;
	case 'style.zIndex':
		el.vmlBox.style.zIndex = el.style.zIndex;
		break;
	}
},

applyVML: function(el) {
	el.runtimeStyle.cssText = '';
	this.vmlFill(el);
	this.vmlStrokeColor(el);
	this.vmlStrokeWeight(el);
	this.vmlOffsets(el);
	this.vmlPath(el);
	this.nixBorder(el);
	this.vmlOpacity(el);
},

vmlOpacity: function(el) {
	if (el.currentStyle.filter.search('lpha') != -1) {
		var trans = el.currentStyle.filter;
		trans = parseInt(trans.substring(trans.lastIndexOf('=')+1, trans.lastIndexOf(')')), 10)/100;
		for (var v in el.vml) {
			el.vml[v].filler.opacity = trans;
		}
	}
},

vmlFill: function(el) {
	if (!el.currentStyle) {
		return;
	} else {
		var elStyle = el.currentStyle;
	}
	el.runtimeStyle.backgroundColor = '';
	el.runtimeStyle.backgroundImage = '';
	var noColor = (elStyle.backgroundColor == 'transparent');
	var noImg = true;
	if (elStyle.backgroundImage != 'none' || el.isImg) {
		if (!el.isImg) {
			el.vmlBg = elStyle.backgroundImage;
			el.vmlBg = el.vmlBg.substr(5, el.vmlBg.lastIndexOf('")')-5);
		}
		else {
			el.vmlBg = el.src;
		}
		var lib = this;
		if (!lib.imgSize[el.vmlBg]) { /* determine size of loaded image */
			var img = document.createElement('img');
			img.attachEvent('onload', function() {
				this.width = this.offsetWidth; /* weird cache-busting requirement! */
				this.height = this.offsetHeight;
				lib.vmlOffsets(el);
			});
			img.className = lib.ns + '_sizeFinder';
			img.runtimeStyle.cssText = 'behavior:none; position:absolute; top:-10000px; left:-10000px; border:none;'; /* make sure to set behavior to none to prevent accidental matching of the helper elements! */
			img.src = el.vmlBg;
			img.removeAttribute('width');
			img.removeAttribute('height');
			document.body.insertBefore(img, document.body.firstChild);
			lib.imgSize[el.vmlBg] = img;
		}
		el.vml.image.filler.src = el.vmlBg;
		noImg = false;
	}
	el.vml.image.filled = !noImg;
	el.vml.image.fillcolor = 'none';
	el.vml.color.filled = !noColor;
	el.vml.color.fillcolor = elStyle.backgroundColor;
	el.runtimeStyle.backgroundImage = 'none';
	el.runtimeStyle.backgroundColor = 'transparent';
},

vmlStrokeColor: function(el) {
	el.vml.stroke.fillcolor = el.currentStyle.borderColor;
},

vmlStrokeWeight: function(el) {
	var borders = ['Top', 'Right', 'Bottom', 'Left'];
	el.bW = {};
	for (var b=0; b<4; b++) {
		el.bW[borders[b]] = parseInt(el.currentStyle['border' + borders[b] + 'Width'], 10) || 0;
	}
},

vmlOffsets: function(el) {
	var dims = ['Left', 'Top', 'Width', 'Height'];
	for (var d=0; d<4; d++) {
		el.dim[dims[d]] = el['offset'+dims[d]];
	}
	var assign = function(obj, topLeft) {
		obj.style.left = (topLeft ? 0 : el.dim.Left) + 'px';
		obj.style.top = (topLeft ? 0 : el.dim.Top) + 'px';
		obj.style.width = el.dim.Width + 'px';
		obj.style.height = el.dim.Height + 'px';
	};
	for (var v in el.vml) {
		var mult = (v == 'image') ? 1 : 2;
		el.vml[v].coordsize = (el.dim.Width*mult) + ', ' + (el.dim.Height*mult);
		assign(el.vml[v], true);
	}
	assign(el.vmlBox, false);

	if (DD_roundies.IE8) {
		el.vml.stroke.style.margin = '-1px';
		if (typeof el.bW == 'undefined') {
			this.vmlStrokeWeight(el);
		}
		el.vml.color.style.margin = (el.bW.Top-1) + 'px ' + (el.bW.Left-1) + 'px';
	}
},

vmlPath: function(el) {
	var coords = function(direction, w, h, r, aL, aT, mult) {
		var cmd = direction ? ['m', 'qy', 'l', 'qx', 'l', 'qy', 'l', 'qx', 'l'] : ['qx', 'l', 'qy', 'l', 'qx', 'l', 'qy', 'l', 'm']; /* whoa */
		aL *= mult;
		aT *= mult;
		w *= mult;
		h *= mult;
		var R = r.slice(); /* do not affect original array */
		for (var i=0; i<4; i++) {
			R[i] *= mult;
			R[i] = Math.min(w/2, h/2, R[i]); /* make sure you do not get funky shapes - pick the smallest: half of the width, half of the height, or current value */
		}
		var coords = [
		              cmd[0] + Math.floor(0+aL) +','+ Math.floor(R[0]+aT),
		              cmd[1] + Math.floor(R[0]+aL) +','+ Math.floor(0+aT),
		              cmd[2] + Math.ceil(w-R[1]+aL) +','+ Math.floor(0+aT),
		              cmd[3] + Math.ceil(w+aL) +','+ Math.floor(R[1]+aT),
		              cmd[4] + Math.ceil(w+aL) +','+ Math.ceil(h-R[2]+aT),
		              cmd[5] + Math.ceil(w-R[2]+aL) +','+ Math.ceil(h+aT),
		              cmd[6] + Math.floor(R[3]+aL) +','+ Math.ceil(h+aT),
		              cmd[7] + Math.floor(0+aL) +','+ Math.ceil(h-R[3]+aT),
		              cmd[8] + Math.floor(0+aL) +','+ Math.floor(R[0]+aT)
		              ];
		if (!direction) {
			coords.reverse();
		}
		var path = coords.join('');
		return path;
	};

	if (typeof el.bW == 'undefined') {
		this.vmlStrokeWeight(el);
	}
	var bW = el.bW;
	var rad = el.DD_radii.slice();

	/* determine outer curves */
	var outer = coords(true, el.dim.Width, el.dim.Height, rad, 0, 0, 2);

	/* determine inner curves */
	rad[0] -= Math.max(bW.Left, bW.Top);
	rad[1] -= Math.max(bW.Top, bW.Right);
	rad[2] -= Math.max(bW.Right, bW.Bottom);
	rad[3] -= Math.max(bW.Bottom, bW.Left);
	for (var i=0; i<4; i++) {
		rad[i] = Math.max(rad[i], 0);
	}
	var inner = coords(false, el.dim.Width-bW.Left-bW.Right, el.dim.Height-bW.Top-bW.Bottom, rad, bW.Left, bW.Top, 2);
	var image = coords(true, el.dim.Width-bW.Left-bW.Right+1, el.dim.Height-bW.Top-bW.Bottom+1, rad, bW.Left, bW.Top, 1);

	/* apply huge path string */
	el.vml.color.path = inner;
	el.vml.image.path = image;
	el.vml.stroke.path = outer + inner;

	this.clipImage(el);
},

nixBorder: function(el) {
	var s = el.currentStyle;
	var sides = ['Top', 'Left', 'Right', 'Bottom'];
	for (var i=0; i<4; i++) {
		el.runtimeStyle['padding' + sides[i]] = (parseInt(s['padding' + sides[i]], 10) || 0) + (parseInt(s['border' + sides[i] + 'Width'], 10) || 0) + 'px';
	}
	el.runtimeStyle.border = 'none';
},

clipImage: function(el) {
	var lib = DD_roundies;
	if (!el.vmlBg || !lib.imgSize[el.vmlBg]) {
		return;
	}
	var thisStyle = el.currentStyle;
	var bg = {'X':0, 'Y':0};
	var figurePercentage = function(axis, position) {
		var fraction = true;
		switch(position) {
		case 'left':
		case 'top':
			bg[axis] = 0;
			break;
		case 'center':
			bg[axis] = 0.5;
			break;
		case 'right':
		case 'bottom':
			bg[axis] = 1;
			break;
		default:
			if (position.search('%') != -1) {
				bg[axis] = parseInt(position, 10) * 0.01;
			}
			else {
				fraction = false;
			}
		}
		var horz = (axis == 'X');
		bg[axis] = Math.ceil(fraction ? (( el.dim[horz ? 'Width' : 'Height'] - (el.bW[horz ? 'Left' : 'Top'] + el.bW[horz ? 'Right' : 'Bottom']) ) * bg[axis]) - (lib.imgSize[el.vmlBg][horz ? 'width' : 'height'] * bg[axis]) : parseInt(position, 10));
		bg[axis] += 1;
	};
	for (var b in bg) {
		figurePercentage(b, thisStyle['backgroundPosition'+b]);
	}
	el.vml.image.filler.position = (bg.X/(el.dim.Width-el.bW.Left-el.bW.Right+1)) + ',' + (bg.Y/(el.dim.Height-el.bW.Top-el.bW.Bottom+1));
	var bgR = thisStyle.backgroundRepeat;
	var c = {'T':1, 'R':el.dim.Width+1, 'B':el.dim.Height+1, 'L':1}; /* these are defaults for repeat of any kind */
	var altC = { 'X': {'b1': 'L', 'b2': 'R', 'd': 'Width'}, 'Y': {'b1': 'T', 'b2': 'B', 'd': 'Height'} };
	if (bgR != 'repeat') {
		c = {'T':(bg.Y), 'R':(bg.X+lib.imgSize[el.vmlBg].width), 'B':(bg.Y+lib.imgSize[el.vmlBg].height), 'L':(bg.X)}; /* these are defaults for no-repeat - clips down to the image location */
		if (bgR.search('repeat-') != -1) { /* now let's revert to dC for repeat-x or repeat-y */
			var v = bgR.split('repeat-')[1].toUpperCase();
			c[altC[v].b1] = 1;
			c[altC[v].b2] = el.dim[altC[v].d]+1;
		}
		if (c.B > el.dim.Height) {
			c.B = el.dim.Height+1;
		}
	}
	el.vml.image.style.clip = 'rect('+c.T+'px '+c.R+'px '+c.B+'px '+c.L+'px)';
},

pseudoClass: function(el) {
	var self = this;
	setTimeout(function() { /* would not work as intended without setTimeout */
		self.applyVML(el);
	}, 1);
},

reposition: function(el) {
	this.vmlOffsets(el);
	this.vmlPath(el);
},

roundify: function(rad) {
	this.style.behavior = 'none';
	if (!this.currentStyle) {
		return;
	}
	else {
		var thisStyle = this.currentStyle;
	}
	var allowed = {BODY: false, TABLE: false, TR: false, TD: false, SELECT: false, OPTION: false, TEXTAREA: false};
	if (allowed[this.nodeName] === false) { /* elements not supported yet */
		return;
	}
	var self = this; /* who knows when you might need a setTimeout */
	var lib = DD_roundies;
	this.DD_radii = rad;
	this.dim = {};

	/* attach handlers */
	var handlers = {resize: 'reposition', move: 'reposition'};
	if (this.nodeName == 'A') {
		var moreForAs = {mouseleave: 'pseudoClass', mouseenter: 'pseudoClass', focus: 'pseudoClass', blur: 'pseudoClass'};
		for (var a in moreForAs) {
			handlers[a] = moreForAs[a];
		}
	}
	for (var h in handlers) {
		this.attachEvent('on' + h, function() {
			lib[handlers[h]](self);
		});
	}
	this.attachEvent('onpropertychange', function() {
		lib.readPropertyChanges(self);
	});

	/* ensure that this elent and its parent is given hasLayout (needed for accurate positioning) */
	var giveLayout = function(el) {
		el.style.zoom = 1;
		if (el.currentStyle.position == 'static') {
			el.style.position = 'relative';
		}
	};
	giveLayout(this.offsetParent);
	giveLayout(this);

	/* create vml elements */
	this.vmlBox = document.createElement('ignore'); /* IE8 really wants to be encased in a wrapper element for the VML to work, and I don't want to disturb getElementsByTagName('div') - open to suggestion on how to do this differently */
	this.vmlBox.runtimeStyle.cssText = 'behavior:none; position:absolute; margin:0; padding:0; border:0; background:none;'; /* super important - if something accidentally matches this (you yourseld did this once, Drew), you'll get infinitely-created elements and a frozen browser! */
	this.vmlBox.style.zIndex = thisStyle.zIndex;
	this.vml = {'color':true, 'image':true, 'stroke':true};
	for (var v in this.vml) {
		this.vml[v] = document.createElement(lib.ns + ':shape');
		this.vml[v].filler = document.createElement(lib.ns + ':fill');
		this.vml[v].appendChild(this.vml[v].filler);
		this.vml[v].stroked = false;
		this.vml[v].style.position = 'absolute';
		this.vml[v].style.zIndex = thisStyle.zIndex;
		this.vml[v].coordorigin = '1,1';
		this.vmlBox.appendChild(this.vml[v]);
	}
	this.vml.image.fillcolor = 'none';
	this.vml.image.filler.type = 'tile';
	this.parentNode.insertBefore(this.vmlBox, this);

	this.isImg = false;
	if (this.nodeName == 'IMG') {
		this.isImg = true;
		this.style.visibility = 'hidden';
	}

	setTimeout(function() {
		lib.applyVML(self);
	}, 1);
}

};

try {
	document.execCommand("BackgroundImageCache", false, true);
} catch(err) {}
DD_roundies.IEversion();
DD_roundies.createVmlNameSpace();
DD_roundies.createVmlStyleSheet();

if (DD_roundies.IE8 && document.attachEvent && DD_roundies.querySelector) {
	document.attachEvent('onreadystatechange', function() {
		if (document.readyState == 'complete') {
			var selectors = DD_roundies.selectorsToProcess;
			var length = selectors.length;
			var delayedCall = function(node, radii, index) {
				setTimeout(function() {
					DD_roundies.roundify.call(node, radii);
				}, index*100);
			};
			for (var i=0; i<length; i++) {
				var results = document.querySelectorAll(selectors[i].selector);
				var rLength = results.length;
				for (var r=0; r<rLength; r++) {
					if (results[r].nodeName != 'INPUT') { /* IE8 doesn't like to do this to inputs yet */
						delayedCall(results[r], selectors[i].radii, r);
					}
				}
			}
		}
	});
}

DD_roundies.addRule('div.rbbox', '10px'); 
DD_roundies.addRule('ul#tabnav li', '0px 10px 0 0'); 
DD_roundies.addRule('#tabnav a', '0px 10px 0 0'); 
DD_roundies.addRule('.messureadbody,.messureadhead,.messureadflag', '10px'); 
DD_roundies.addRule('div#action-menu', '10px'); 
DD_roundies.addRule('.button3', '5px 5px 0 0'); 
DD_roundies.addRule('.button3.linkbut', '5px 5px 0 0'); 
DD_roundies.addRule('div#action-menu', '10px'); 
DD_roundies.addRule('#action-menu:hover', '10px'); 


function browser() {
	var b = navigator.appName
	if (b=="Netscape") this.b = "ns"
		else this.b = b
		this.version = navigator.appVersion
		this.v = parseInt(this.version)
		this.ns = (this.b=="ns" && this.v>=5)
		this.op = (navigator.userAgent.indexOf('Opera')>-1)
		this.safari = (navigator.userAgent.indexOf('Safari')>-1)
		this.op7 = (navigator.userAgent.indexOf('Opera')>-1 && this.v>=7)
		this.ie56 = (this.version.indexOf('MSIE 5')>-1||this.version.indexOf('MSIE 6')>-1)
		this.iewin = (this.ie56 && navigator.userAgent.indexOf('Windows')>-1)
		this.iemac = (this.ie56 && navigator.userAgent.indexOf('Mac')>-1)
		this.moz = (navigator.userAgent.indexOf('Mozilla')>-1)
		this.moz13 = (navigator.userAgent.indexOf('Mozilla')>-1 && navigator.userAgent.indexOf('1.3')>-1)
		this.oldmoz = (navigator.userAgent.indexOf('Mozilla')>-1 && navigator.userAgent.indexOf('1.4')>-1 || navigator.userAgent.indexOf('Mozilla')>-1 && navigator.userAgent.indexOf('1.5')>-1 || navigator.userAgent.indexOf('Mozilla')>-1 && navigator.userAgent.indexOf('1.6')>-1)
		this.ns6 = (navigator.userAgent.indexOf('Netscape6')>-1)
		this.docom = (this.ie56||this.ns||this.iewin||this.op||this.iemac||this.safari||this.moz||this.oldmoz||this.ns6)
}

function toggle_dynamic_var($name) {
	name1 = 'dyn_'+$name+'_display';
	name2 = 'dyn_'+$name+'_edit';
	if(document.getElementById(name1).style.display == "none") {
		document.getElementById(name2).style.display = "none";
		document.getElementById(name1).style.display = "inline";
	} else {
		document.getElementById(name1).style.display = "none";
		document.getElementById(name2).style.display = "inline";

	}
}

function chgArtType() {
	articleType = document.getElementById('articletype').value;
	typeProperties = articleTypes[articleType];

	propertyList = new Array('show_topline','y',
			'show_subtitle','y',
			'show_linkto','y',
			'show_lang','y',
			'show_author','y',
			'use_ratings','y',
			'heading_only','n',
			'show_image_caption','y',
			'show_pre_publ','y',
			'show_post_expire','y',
			'show_image','y'
	);

	var l = propertyList.length;
	for (var i=0; i<l; i++) {
		property = propertyList[i++];
		value = propertyList[i];

		if (typeProperties[property] == value) {
			display = "";
		} else {
			display = "none";
		}

		if (document.getElementById(property)) {
			document.getElementById(property).style.display = display;
		} else {
			j = 1;
			while (document.getElementById(property+'_'+j)) {
				document.getElementById(property+'_'+j).style.display=display;
				j++;
			}
		}

	}
}

function chgMailinType() {
	if (document.getElementById('mailin_type').value != 'article-put') {
		document.getElementById('article_topic').style.display = "none";
		document.getElementById('article_type').style.display = "none";
	} else {
		document.getElementById('article_topic').style.display = "";
		document.getElementById('article_type').style.display = "";
	}
}

function toggleSpan(id) {
	if (document.getElementById(id).style.display == "inline") {
		document.getElementById(id).style.display = "none";
	} else {
		document.getElementById(id).style.display = "inline";
	}
}

function toggleBlock(id) {
	if (document.getElementById(id).style.display == "block") {
		document.getElementById(id).style.display = "none";
	} else {
		document.getElementById(id).style.display = "block";
	}
}

/**
 * 
 * @param animDivId 	id of div to show/hide
 * @param toggleText 	text to display when show
 * @param untoggleText	text to display when hide
 * @param imgId			id of toggle image
 * @param labelId		id of the label text
 * @param init			show|hide|none	if set init the label and image
 * @return
 */
function toggleAnim(animDivId, toggleText, untoggleText, imgId, labelId, init){
	dojo.require("dojo.fx");
	var img = document.getElementById(imgId);
	var label = document.getElementById(labelId);
	if(init == 'hide'){
		document.getElementById(animDivId).style.display = 'none';
		img.src = 'img/icons/untoggle.png';
		label.innerHTML = '<b style="color:#08639C;" >' + toggleText + '</b>';
		return;
	}
	if(init == 'show'){
		document.getElementById(animDivId).style.display = 'block';
		img.src = 'img/icons/toggle.png';
		label.innerHTML = '<b style="color:#08639C;" >' + untoggleText + '</b>';
		return;
	}
	
	if( document.getElementById(animDivId).style.display == 'none'){
		img.src = 'img/icons/toggle.png';
		label.innerHTML = '<b style="color:#08639C;" >' + untoggleText + '</b>';
		var wipe = dojo.fx.wipeIn({node: animDivId, duration: 250});
	}else{
		img.src = 'img/icons/untoggle.png';
		label.innerHTML = '<b style="color:#08639C;" >' + toggleText + '</b>';
		var wipe = dojo.fx.wipeOut({node: animDivId, duration: 250});
	}
	wipe.play();
}

//function pour afficher cacher un div avec img mouvante
function toggleLine(id,imgId){
	element = document.getElementById(id);
	img = document.getElementById(imgId);
	if(element.style.display == 'none'){
		element.style.display = 'block';
		element.style.height = 'auto';
		img.src = 'img/icons/toggle.png';
	}else{
		element.style.display = 'none';
		element.style.height = '10px';
		img.src = 'img/icons/untoggle.png';
	}
	return true;
}


function showTocToggle() {
	if (document.createTextNode) {
		// Uses DOM calls to avoid document.write + XHTML issues

		var linkHolder = document.getElementById('toctitle')
		if (!linkHolder) return;

		var outerSpan = document.createElement('span');
		outerSpan.className = 'toctoggle';

		var toggleLink = document.createElement('a');
		toggleLink.id = 'togglelink';
		toggleLink.className = 'internal';
		toggleLink.href = 'javascript:toggleToc()';
		toggleLink.appendChild(document.createTextNode(tocHideText));

		outerSpan.appendChild(document.createTextNode('['));
		outerSpan.appendChild(toggleLink);
		outerSpan.appendChild(document.createTextNode(']'));

		linkHolder.appendChild(document.createTextNode(' '));
		linkHolder.appendChild(outerSpan);
		if (getCookie("hidetoc") == "1" ) toggleToc();
	}
}

function changeText(el, newText) {
	// Safari work around
	if (el.innerText)
		el.innerText = newText;
	else if (el.firstChild && el.firstChild.nodeValue)
		el.firstChild.nodeValue = newText;
}

function toggleToc() {
	var toc = document.getElementById('toc').getElementsByTagName('ul')[0];
	var toggleLink = document.getElementById('togglelink')

	if (toc && toggleLink && toc.style.display == 'none') {
		changeText(toggleLink, tocHideText);
		toc.style.display = 'block';
		setCookie("hidetoc","0");
	} else {
		changeText(toggleLink, tocShowText);
		toc.style.display = 'none';
		setCookie("hidetoc","1");
	}
}

function chgTrkFld(f,o) {
	var opt = 0;
	document.getElementById('z').style.display = "none";
	for (var i = 0; i < f.length; i++) {
		var c = f.charAt(i);
		if (document.getElementById(c)) { 
			if (c == o) {
				document.getElementById(c).style.display = "block";
				document.getElementById('z').style.display = "block";
			} else {
				document.getElementById(c).style.display = "none";
			}
		}
	}
}

function multitoggle(f,o) {
	for (var i = 0; i < f.length; i++) {
		if (document.getElementById('fid'+f[i])) { 
			if (f[i] == o) {
				document.getElementById('fid'+f[i]).style.display = "block";
			} else {
				document.getElementById('fid'+f[i]).style.display = "none";
			}
		}
	}
}

function setMenuCon(foo) {
	var it = foo.split(",");
	document.getElementById('menu_url').value = it[0];
	document.getElementById('menu_name').value = it[1];
	if (it[2]) {
		document.getElementById('menu_section').value = it[2];
	} else {
		document.getElementById('menu_section').value = '';
	}
	if (it[3]) {
		document.getElementById('menu_perm').value = it[3];
	} else {
		document.getElementById('menu_perm').value = '';
	}
}

function genPass(w1, w2, w3) {
	vo = "aeiouAEU";

	co = "bcdfgjklmnprstvwxzBCDFGHJKMNPQRSTVWXYZ0123456789_$%#";
	s = Math.round(Math.random());
	l = 8;
	p = '';

	for (i = 0; i < l; i++) {
		if (s) {
			letter = vo.charAt(Math.round(Math.random() * (vo.length - 1)));

			s = 0;
		} else {
			letter = co.charAt(Math.round(Math.random() * (co.length - 1)));

			s = 1;
		}

		p = p + letter;
	}

	document.getElementById(w1).value = p;
	document.getElementById(w2).value = p;
	document.getElementById(w3).value = p;
}

function setUserModule(foo1) {
	document.getElementById('usermoduledata').value = foo1;
}

function setSomeElement(fooel, foo1) {
	document.getElementById(fooel).value = document.getElementById(fooel).value + foo1;
}

function replaceSome(fooel, what, repl) {
	document.getElementById(fooel).value = document.getElementById(fooel).value.replace(what, repl);
}

function replaceLimon(vec) {
	document.getElementById(vec[0]).value = document.getElementById(vec[0]).value.replace(vec[1], vec[2]);
}

function replaceImgSrc(imgName,replSrc) {
	document.getElementById(imgName).src = replSrc;
}

function setSelectionRange(textarea, selectionStart, selectionEnd) {
	if (textarea.setSelectionRange) {
		textarea.focus();
		textarea.setSelectionRange(selectionStart, selectionEnd);
	}
	else if (textarea.createTextRange) {
		var range = textarea.createTextRange();
		textarea.collapse(true);
		textarea.moveEnd('character', selectionEnd);
		textarea.moveStart('character', selectionStart);
		textarea.select();
	}
}

function setCaretToPos (textarea, pos) {
	setSelectionRange(textarea, pos, pos);
}

function insertAt(elementId, replaceString) {
	//inserts given text at selection or cursor position
	textarea = getElementById(elementId);
	var toBeReplaced = /text|page|area_name/;//substrings in replaceString to be replaced by the selection if a selection was done
	if (textarea.setSelectionRange) {
		//Mozilla UserAgent Gecko-1.4
		var selectionStart = textarea.selectionStart;
		var selectionEnd = textarea.selectionEnd;
		var scrollTop=textarea.scrollTop;
		if (selectionStart != selectionEnd) { // has there been a selection
			var newString = replaceString.replace(toBeReplaced, textarea.value.substring(selectionStart, selectionEnd));
			textarea.value = textarea.value.substring(0, selectionStart)
			+ newString
			+ textarea.value.substring(selectionEnd);
			setSelectionRange(textarea, selectionStart, selectionStart + newString.length);
		}
		else  {// set caret
			textarea.value = textarea.value.substring(0, selectionStart)
			+ replaceString
			+ textarea.value.substring(selectionEnd);
			setCaretToPos(textarea, selectionStart + replaceString.length);
		}
		textarea.scrollTop=scrollTop;
	}
	else if (document.selection) {
		//UserAgent IE-6.0
		textarea.focus();
		var range = document.selection.createRange();
		if (range.parentElement() == textarea) {
			var isCollapsed = range.text == '';
			if (! isCollapsed)  {
				range.text = replaceString.replace(toBeReplaced, range.text);
				range.moveStart('character', -range.text.length);
				range.select();
			}
			else {
				range.text = replaceString;
			}
		}
	}
	else { //UserAgent Gecko-1.0.1 (NN7.0)
		setSomeElement(elementId, replaceString)
		//alert("don't know yet how to handle insert" + document);
	}
}

function setUserModuleFromCombo(id) {
	document.getElementById('usermoduledata').value = document.getElementById('usermoduledata').value
	+ document.getElementById(id).options[document.getElementById(id).selectedIndex].value;
//	document.getElementById('usermoduledata').value='das';
}


function show(foo,f,section) {
	document.getElementById(foo).style.display = "block";
	if (f) { setCookie(foo, "o", section); }
}

function hide(foo,f, section) {
	if (document.getElementById(foo)) {
		document.getElementById(foo).style.display = "none";
		if (f) {
			var wasnot = getCookie(foo, section, 'x') == 'x';
			setCookie(foo, "c", section);
			if (wasnot) {
				history.go(0);
			}
		}
	}
}

function flip(foo) {
	if (document.getElementById(foo).style.display == "none") {
		show(foo);
	} else {
		if (document.getElementById(foo).style.display == "block") {
			hide(foo);
		} else {
			show(foo);
		}
	}
}

function toggle(foo) {
	if (document.getElementById(foo).style.display == "none") {
		show(foo, true, "menu");

		setCookie(foo, "o");
	} else {
		if (document.getElementById(foo).style.display == "block") {
			hide(foo, true, "menu");
			setCookie(foo, "c");
		} else {
			show(foo, true, "menu");
		}
	}
}

function setopacity(obj,opac){
	if (document.all && !is.op){ //ie
		obj.filters.alpha.opacity = opac * 100;
	}else{
		obj.style.MozOpacity = opac;
		obj.style.opacity = opac;
	}
}

function tikitabs(focus,max) {
	for (var i = 1; i < max; i++) {
		var tabname = 'tab' + i;
		var content = 'content' + i;
		//if (document.getElementById(tabname) != 'undefined') {
		if (i == focus) {
			//show(tabname);
			show(content);
			setCookie('tab',focus);
			document.getElementById(tabname).style.borderColor="black";
		} else {
			//hide(tabname);
			hide(content);
			document.getElementById(tabname).style.borderColor="white";
		}
		//}
	}
}

function setfoldericonstate(foo) {
	if (getCookie(foo, "menu", "o") == "o") {
		src = "ofo.gif";
	} else {
		src = "fo.gif";
	}
	document.getElementsByName(foo + 'icn')[0].src = document.getElementsByName(foo + 'icn')[0].src.replace(/[^\\\/]*$/, src);
}
/* foo: name of the menu
 * def: menu type (e:extended, c:collapsed, f:fixed)
 * the menu is collapsed function of its cookie: if no cookie is set, the def is used
 */

function setfolderstate(foo, def) {
	var status = getCookie(foo, "menu", "o");
	var img = "fo.gif";
	var src = img; // default
	if (status == "o") {
		show(foo);
		src = "o" + img;
	} else if (status != "c"  && def != 'd') {
		show(foo);
		src = "o" + img;
	}
	else {
		hide(foo);
	}
	document.getElementsByName(foo + 'icn')[0].src = document.getElementsByName(foo + 'icn')[0].src.replace(/[^\\\/]*$/, src);
}

function setsectionstate(foo, def, img) {
	var status = getCookie(foo, "menu", "o");
	if (status == "o") {
		show(foo);
		if (img) src = "o" + img;
	} else if (status != "c" && def != 'd') {
		show(foo);
		if (img) src = "o" + img;
	} else /*if (status == "c")*/ {
		hide(foo);
		if (img) src = img;
	}
	if (img) document.getElementsByName(foo + 'icn')[0].src = document.getElementsByName(foo + 'icn')[0].src.replace(/[^\\\/]*$/, src);
}

function icntoggle(foo, img) {
	if (!img) img = "fo.gif";
	if (document.getElementById(foo).style.display == "none") {
		show(foo, true, "menu");
		document.getElementsByName(foo + 'icn')[0].src = document.getElementsByName(foo + 'icn')[0].src.replace(/[^\\\/]*$/, 'o' + img);

	} else {
		hide(foo, true, "menu");
		document.getElementsByName(foo + 'icn')[0].src = document.getElementsByName(foo + 'icn')[0].src.replace(/[^\\\/]*$/, img);
	}
}


//set folder icon state during page load

function setFolderIcons() {
	var elements = document.forms[the_form].elements[elements_name];

	var elements_cnt = ( typeof (elements.length) != 'undefined') ? elements.length : 0;

	if (elements_cnt) {
		for (var i = 0; i < elements_cnt; i++) {
			elements[i].checked = document.forms[the_form].elements[switcher_name].checked;
		}
	} else {
		elements.checked = document.forms[the_form].elements[switcher_name].checked;

		;
	} // end if... else

	return true;
}     // setFolderIcons()

//Initialize a cross-browser XMLHttpRequest object.
//The object return has to be sent using send(). More parameters can be
//given.
//callback - The function that will be called when the response arrives
//First parameter will be the status 
//(HTTP Response Code [200,403, 404, ...])
//method - GET or POST
//url - The URL to open
function getHttpRequest( method, url )
{
	var request;

	if( window.XMLHttpRequest )
		request = new XMLHttpRequest();
	else if( window.ActiveXObject )
	{
		try
		{
			request = new ActiveXObject( "Microsoft.XMLHTTP" );
		}
		catch( ex )
		{
			request = new ActiveXObject("MSXML2.XMLHTTP");
		}
	}
	else
		return false;

	if( !request )
		return false;

	request.open( method, url, false );

	return request;
}

//name - name of the cookie
//value - value of the cookie
//[expires] - expiration date of the cookie (defaults to end of current session)
//[path] - path for which the cookie is valid (defaults to path of calling document)
//[domain] - domain for which the cookie is valid (defaults to domain of calling document)
//[secure] - Boolean value indicating if the cookie transmission requires a secure transmission
//* an argument defaults when it is assigned null as a placeholder
//* a null placeholder is not required for trailing omitted arguments
function setCookie(name, value, section, expires, path, domain, secure) {
	if (!expires) {
		expires = new Date();
		expires.setFullYear(expires.getFullYear() + 1);
	}
	if (feature_no_cookie == 'y') {
		var request = getHttpRequest( "GET", "tiki-cookie-jar.php?" + name + "=" + escape( value ) )
		try {
			request.send('');
			//alert("XMLHTTP/set"+request.readyState+request.responseText);
			tiki_cookie_jar[name] = value;
			return true;
		}
		catch( ex )	{
			setCookieBrowser(name, value, section, expires, path, domain, secure);
			return false;
		}
	}
	else {
		setCookieBrowser(name, value, section, expires, path, domain, secure);
		return true;
	}
}
function setCookieBrowser(name, value, section, expires, path, domain, secure) {
	if (section) {
		valSection = getCookie(section);
		name2 = "@" + name + ":";
		if (valSection) {
			if (new RegExp(name2).test(valSection))
				valSection  = valSection.replace(new RegExp(name2 + "[^@;]*"), name2 + value);
			else
				valSection = valSection + name2 + value;
			setCookieBrowser(section, valSection, null, expires, path, domain, secure);
		}
		else {
			valSection = name2+value;
			setCookieBrowser(section, valSection, null, expires, path, domain, secure);
		}

	}
	else {
		var curCookie = name + "=" + escape(value) + ((expires) ? "; expires=" + expires.toGMTString() : "")
		+ ((path) ? "; path=" + path : "") + ((domain) ? "; domain=" + domain : "") + ((secure) ? "; secure" : "");
		document.cookie = curCookie;
	}
}

//name - name of the desired cookie
//section - name of group of cookies or null
//* return string containing value of specified cookie or null if cookie does not exist
function getCookie(name, section, defval) {
	if( feature_no_cookie == 'y' && (window.XMLHttpRequest || window.ActiveXObject) && typeof tiki_cookie_jar != "undefined" && tiki_cookie_jar.length > 0) {
		if (typeof tiki_cookie_jar[name] == "undefined")
			return defval;
		return tiki_cookie_jar[name];
	}
	else {
		return getCookieBrowser(name, section, defval);
	}
}
function getCookieBrowser(name, section, defval) {
	if (section) {
		var valSection = getCookieBrowser(section);
		if (valSection) {
			var name2 = "@"+name+":";
			var val = valSection.match(new RegExp(name2 + "([^@;]*)"));
			if (val)
				return unescape(val[1]);
			else
				return null;
		} else {
			return defval;
		}
	} else {
		var dc = document.cookie;

		var prefix = name + "=";
		var begin = dc.indexOf("; " + prefix);

		if (begin == -1) {
			begin = dc.indexOf(prefix);

			if (begin != 0)
				return null;

		} else begin += 2;

		var end = document.cookie.indexOf(";", begin);

		if (end == -1)
			end = dc.length;

		return unescape(dc.substring(begin + prefix.length, end));
	}
}

//name - name of the cookie
//[path] - path of the cookie (must be same as path used to create cookie)
//[domain] - domain of the cookie (must be same as domain used to create cookie)
//* path and domain default if assigned null or omitted if no explicit argument proceeds
function deleteCookie(name, section, expires, path, domain, secure) {
	if (section) {
		valSection = getCookieBrowser(section);
		name2 = "@" + name + ":";
		if (valSection) {
			if (new RegExp(name2).test(valSection)) {
				valSection  = valSection.replace(new RegExp(name2 + "[^@;]*"), "");
				setCookieBrowser(section, valSection, null, expires, path, domain, secure);
			}
		}
	}
	else {

//		if( !setCookie( name, '', 0, path, domain ) ) {
//		if (getCookie(name)) {
		document.cookie = name + "="
		+ ((path) ? "; path=" + path : "") + ((domain) ? "; domain=" + domain : "") + "; expires=Thu, 01-Jan-70 00:00:01 GMT";
//		}
	}
}

//date - any instance of the Date object
//* hand all instances of the Date object to this function for "repairs"
function fixDate(date) {
	var base = new Date(0);

	var skew = base.getTime();

	if (skew > 0)
		date.setTime(date.getTime() - skew);
}


//Expand/collapse lists

function flipWithSign(foo) {
	if (document.getElementById(foo).style.display == "none") {
		show(foo);

		collapseSign("flipper" + foo);
		setCookie(foo, "o");
	} else {
		hide(foo);

		expandSign("flipper" + foo);
		setCookie(foo, "c");
	}
}

//set the state of a flipped entry after page reload
function setFlipWithSign(foo) {
	if (getCookie(foo) == "o") {
		collapseSign("flipper" + foo);

		show(foo);
	} else {
		expandSign("flipper" + foo);

		hide(foo);
	}
}

function expandSign(foo) {
	document.getElementById(foo).firstChild.nodeValue = "[+]";
}

function collapseSign(foo) {
	document.getElementById(foo).firstChild.nodeValue = "[-]";
} // flipWithSign()


//
//Check / Uncheck all Checkboxes
//
function switchCheckboxes(tform, elements_name, state) {
	// checkboxes need to have the same name elements_name
	// e.g. <input type="checkbox" name="my_ename[]">, will arrive as Array in php.
	for (var i = 0; i < tform.length; i++) {
		if (tform.elements[i].name == elements_name) {
			tform.elements[i].checked = state;
		}
	}
	PMA_markRowsInit();
	return true;
}

//Set client offset (in minutes) to a cookie to avoid server-side DST issues
//Added 7/25/03 by Jeremy Jongsma (jjongsma@tickchat.com)

var expires = new Date();
var offset = -(expires.getTimezoneOffset() * 60);
expires.setFullYear(expires.getFullYear() + 1);
setCookie("tz_offset", offset, null, expires, "/");

//function added for use in navigation dropdown
//example :
//<select name="anything" onchange="go(this);">
//<option value="http://tikiwiki.org">tikiwiki.org</option>
//</select>
function go(o) {
	if (o.options[o.selectedIndex].value != "") {
		location = o.options[o.selectedIndex].value;

		o.options[o.selectedIndex] = 1;
	}

	return false;
}


//function:	targetBlank
//desc:	opens a new window, XHTML-compliant replacement of the "TARGET" tag
//added by: 	Ralf Lueders (lueders@lrconsult.com)
//date:	Sep 7, 2003
//params:	url: the url for the new window
//mode='nw': new, full-featured browser window
//mode='popup': new windows, no features & buttons

function targetBlank(url,mode) {
	var features = 'menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes';
	switch (mode) {
	// new full-equipped browser window
	case 'nw':
		break;
		// new popup-window
	case 'popup':
		features = 'menubar=no,toolbar=no,location=no,directories=no,fullscreen=no,titlebar=no,hotkeys=no,status=no,scrollbars=yes,resizable=yes';
		break;
	default:
		break;
	}
	blankWin = window.open(url,'_blank',features);
}

//function:	confirmTheLink
//desc:	pop up a dialog box to confirm the action
//added by: 	Franck Martin
//date:	Oct 12, 2003
//params:	theLink: The link where it is called from
//params: theMsg: The message to display
function confirmTheLink(theLink, theMsg)
{
	// Confirmation is not required if browser is Opera (crappy js implementation)
	if (typeof(window.opera) != 'undefined') {
		return true;
	}

	var is_confirmed = confirm(theMsg);
	//if (is_confirmed) {
	//    theLink.href += '&amp;is_js_confirmed=1';
	//}

	return is_confirmed;
} 

/** \brief: modif a textarea dimension
 * \elementId = textarea idea
 * \height = nb pixels to add to the height (the number can be negative)
 * \width = nb pixels to add to the width
 * \formid = form id (needs to have 2 input rows and cols
 **/
function textareasize(elementId, height, width, formId) {
	textarea = document.getElementById(elementId);
	form1 = document.getElementById(formId);
	if (textarea && height != 0 && textarea.rows + height > 5) {
		textarea.rows += height;
		if (form1.rows)
			form1.rows.value = textarea.rows;
	}
	if (textarea && width != 0 && textarea.cols + width > 10) {
		textarea.cols += width;
		if (form1.cols)
			form1.cols.value = textarea.cols;
	}
}


/** \brief: insert img tag in textarea
 *	
 */	
function insertImg(elementId, fileId, oldfileId) {
	textarea = getElementById(elementId);
	fileup   = getElementById(fileId);    
	oldfile  = getElementById(oldfileId);    
	prefixEl = getElementById("prefix");    
	prefix   = "img/wiki_up/";

	if (!textarea || ! fileup) 
		return;

	if ( prefixEl) { prefix= prefixEl.value; }

	filename = fileup.value;
	oldfilename = oldfile.value;

	if (filename == oldfilename ||
			filename == "" ) { // insert only if name really changed
		return;
	}
	oldfile.value = filename;

	if (filename.indexOf("/")>=0) { // unix
		dirs = filename.split("/"); 
		filename = dirs[dirs.length-1];
	}
	if (filename.indexOf("\\")>=0) { // dos
		dirs = filename.split("\\"); 
		filename = dirs[dirs.length-1];
	}
	if (filename.indexOf(":")>=0) { // mac
		dirs = filename.split(":"); 
		filename = dirs[dirs.length-1];
	}
	// @todo - here's a hack: we know its ending up in img/wiki_up. 
	//      replace with dyn. variable once in a while to respect the tikidomain 
	str = "{img src=\"img/wiki_up/" + filename + "\" }";
	insertAt(elementId, str);
}

/*
 * opens wiki 3d browser
 */
function wiki3d_open (page, width, height) {
	window.open('tiki-wiki3d.php?page='+page,'wiki3d','width='+width+',height='+height+',scrolling=no');
}

//--- begin of sorttable, written by Stuart Langridge, November 2003, MIT license ---
addEvent(window, "load", sortables_init);

var SORT_COLUMN_INDEX;

function sortables_init() {
	// Find all tables with class sortable and make them sortable
	if (!document.getElementsByTagName) return;
	tbls = document.getElementsByTagName("table");
	for (ti=0;ti<tbls.length;ti++) {
		thisTbl = tbls[ti];
		if (((' '+thisTbl.className+' ').indexOf("sortable") != -1) && (thisTbl.id)) {
			//initTable(thisTbl.id);
			ts_makeSortable(thisTbl);
		}
	}
}

function ts_makeSortable(table) {
	if (table.rows && table.rows.length > 0) {
		var firstRow = table.rows[0];
	}
	if (!firstRow) return;

	// We have a first row: assume it's the header, and make its contents clickable links
	for (var i=0;i<firstRow.cells.length;i++) {
		var cell = firstRow.cells[i];
		var txt = ts_getInnerText(cell);
		cell.innerHTML = '<a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">'+txt+'<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a>';
	}
}

function ts_getInnerText(el) {
	if (typeof el == "string") return el;
	if (typeof el == "undefined") { return el };
	if (el.innerText) return el.innerText;	//Not needed but it is faster
	var str = "";

	var cs = el.childNodes;
	var l = cs.length;
	for (var i = 0; i < l; i++) {
		switch (cs[i].nodeType) {
		case 1: //ELEMENT_NODE
			str += ts_getInnerText(cs[i]);
			break;
		case 3:	//TEXT_NODE
			str += cs[i].nodeValue;
			break;
		}
	}
	return str;
}

function ts_resortTable(lnk) {
	// get the span
	var span;
	for (var ci=0;ci<lnk.childNodes.length;ci++) {
		if (lnk.childNodes[ci].tagName && lnk.childNodes[ci].tagName.toLowerCase() == 'span') span = lnk.childNodes[ci];
	}
	var spantext = ts_getInnerText(span);
	var td = lnk.parentNode;
	var column = td.cellIndex;
	var table = getParent(td,'TABLE');

	// Work out a type for the column
	if (table.rows.length <= 1) return;
	var itm = ts_getInnerText(table.rows[1].cells[column]);
	sortfn = ts_sort_caseinsensitive;
	if (itm.match(/^\d\d[\/-]\d\d[\/-]\d\d\d\d$/)) sortfn = ts_sort_date;
	if (itm.match(/^\d\d[\/-]\d\d[\/-]\d\d$/)) sortfn = ts_sort_date;
	if (itm.match(/^[�$]/)) sortfn = ts_sort_currency;
	if (itm.match(/^[\d\.]+$/)) sortfn = ts_sort_numeric;
	SORT_COLUMN_INDEX = column;
	var firstRow = new Array();
	var newRows = new Array();
	for (i=0;i<table.rows[0].length;i++) { firstRow[i] = table.rows[0][i]; }
	for (j=1;j<table.rows.length;j++) { newRows[j-1] = table.rows[j]; }

	newRows.sort(sortfn);

	if (span.getAttribute("sortdir") == 'down') {
		ARROW = '&nbsp;&nbsp;<img src="img/icons2/up.gif" border="0">';
		newRows.reverse();
		span.setAttribute('sortdir','up');
	} else {
		ARROW = '&nbsp;&nbsp;<img src="img/icons2/down.gif" border="0">';
		span.setAttribute('sortdir','down');
	}

	// We appendChild rows that already exist to the tbody, so it moves them rather than creating new ones
	// don't do sortbottom rows
	for (i=0;i<newRows.length;i++) { if (!newRows[i].className || (newRows[i].className && (newRows[i].className.indexOf('sortbottom') == -1))) table.tBodies[0].appendChild(newRows[i]);}
	// do sortbottom rows only
	for (i=0;i<newRows.length;i++) { if (newRows[i].className && (newRows[i].className.indexOf('sortbottom') != -1)) table.tBodies[0].appendChild(newRows[i]);}

	// Delete any other arrows there may be showing
	var allspans = document.getElementsByTagName("span");
	for (var ci=0;ci<allspans.length;ci++) {
		if (allspans[ci].className == 'sortarrow') {
			if (getParent(allspans[ci],"table") == getParent(lnk,"table")) { // in the same table as us?
				allspans[ci].innerHTML = '&nbsp;&nbsp;&nbsp;';
			}
		}
	}

	span.innerHTML = ARROW;
}

function getParent(el, pTagName) {
	if (el == null) return null;
	else if (el.nodeType == 1 && el.tagName.toLowerCase() == pTagName.toLowerCase())	// Gecko bug, supposed to be uppercase
		return el;
	else
		return getParent(el.parentNode, pTagName);
}
function ts_sort_date(a,b) {
	// y2k notes: two digit years less than 50 are treated as 20XX, greater than 50 are treated as 19XX
	aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]);
	bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]);
	if (aa.length == 10) {
		dt1 = aa.substr(6,4)+aa.substr(3,2)+aa.substr(0,2);
	} else {
		yr = aa.substr(6,2);
		if (parseInt(yr) < 50) { yr = '20'+yr; } else { yr = '19'+yr; }
		dt1 = yr+aa.substr(3,2)+aa.substr(0,2);
	}
	if (bb.length == 10) {
		dt2 = bb.substr(6,4)+bb.substr(3,2)+bb.substr(0,2);
	} else {
		yr = bb.substr(6,2);
		if (parseInt(yr) < 50) { yr = '20'+yr; } else { yr = '19'+yr; }
		dt2 = yr+bb.substr(3,2)+bb.substr(0,2);
	}
	if (dt1==dt2) return 0;
	if (dt1<dt2) return -1;
	return 1;
}

function ts_sort_currency(a,b) { 
	aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]).replace(/[^0-9.]/g,'');
	bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]).replace(/[^0-9.]/g,'');
	return parseFloat(aa) - parseFloat(bb);
}

function ts_sort_numeric(a,b) { 
	aa = parseFloat(ts_getInnerText(a.cells[SORT_COLUMN_INDEX]));
	if (isNaN(aa)) aa = 0;
	bb = parseFloat(ts_getInnerText(b.cells[SORT_COLUMN_INDEX])); 
	if (isNaN(bb)) bb = 0;
	return aa-bb;
}

function ts_sort_caseinsensitive(a,b) {
	aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]).toLowerCase();
	bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]).toLowerCase();
	if (aa==bb) return 0;
	if (aa<bb) return -1;
	return 1;
}

function ts_sort_default(a,b) {
	aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]);
	bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]);
	if (aa==bb) return 0;
	if (aa<bb) return -1;
	return 1;
}


function addEvent(elm, evType, fn, useCapture)
//addEvent and removeEvent
//cross-browser event handling for IE5+,  NS6 and Mozilla
//By Scott Andrew
{
	if (elm.addEventListener){
		elm.addEventListener(evType, fn, useCapture);
		return true;
	} else if (elm.attachEvent){
		var r = elm.attachEvent("on"+evType, fn);
		return r;
	} else {
		alert("Handler could not be removed");
	}
} 
//--- end of sorttable ---

//This was added to allow wiki3d to change url on tiki's window
window.name = 'tiki';

//=====================================================================================
//End of tiki-js.js
//=====================================================================================


















//Fonction pour afficher une fenetre pop-up
function popup(page_name, window_name)
{
	page_name = getBaseUrl() + '/' + trimSlash(page_name);
	window.open (page_name, window_name, config='height=300, width=900, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no')
}

//Fonction pour afficher une fenetre pop-up large
function popup2(page_name, window_name)
{
	page_name = getBaseUrl() + '/' + trimSlash(page_name);
	window.open (page_name, window_name, config='height=300, width=800, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no')
}

//Fonction pour afficher une fenetre pop-up large
function popup3(page_name, window_name, window_width)
{
	page_name = getBaseUrl() + '/' + trimSlash(page_name);
	window.open (page_name, window_name, "height=200, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no")
}

//Fonction pour afficher une fenetre pop-up
function popup8x6(page_name, window_name)
{
	page_name = getBaseUrl() + '/' + trimSlash(page_name);
	window.open (page_name, window_name, config='height=600, width=800, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no');
}

//Fonction pour afficher une fenetre pop-up dont la taille est parametrable
function popupP(page_name, window_name , height , width)
{
	page_name = getBaseUrl() + '/' + trimSlash(page_name);
	//alert(baseUrl);
	var option = "height=" + height + ", width=" + width + ", toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=yes, directories=no, status=no";
	window.open (page_name, window_name, config=option);
}

//Recuperer le base href depuis la balise base
function getBaseUrl()
{
	var baseUrl = null;
	if(document.getElementsByTagName){
		var elements = document.getElementsByTagName('base');
		if(elements.length){
			baseUrl = elements[0].href;
		}
	}
	return trimSlash(baseUrl);
}

//supprime les / de d�but et fin
function trimSlash(astring)
{
	var regExpBeginning = /^\/+/;
	var regExpEnd = /\/+$/;
	astring = astring.replace(regExpBeginning, "").replace(regExpEnd, "");
	//alert(astring);
	return astring;
}

//fonction pour selections multiples d'options
function SversD()
{
	indexS=document.trans.source.options.selectedIndex;
	if (indexS < 0) return;
	valeur=document.trans.source.options[indexS].text;
	document.trans.source.options[indexS]=null;
	a = new Option(valeur);
	indexD=document.trans.MetaAffProjectContentList.options.length;
	document.trans.MetaAffProjectContentList.options[indexD]=a;
}

function DversS() 
{
	indexD=document.trans.MetaAffProjectContentList.options.selectedIndex;
	if (indexD < 0) return;
	valeur=document.trans.MetaAffProjectContentList.options[indexD].text;
	document.trans.MetaAffProjectContentList.options[indexD]=null;
	a = new Option(valeur);
	indexS=document.trans.source.options.length;
	document.trans.source.options[indexS]=a;
}

//fonction pour clore une fenetre
function closeWindow()
{
	window.close();
}

//fonction pour ouvrir un formulaire dans un popup
//http://www.asp-php.net/ressources/codes/JavaScript-Ouvrir+un+popup+avec+un+envoi+POST.aspx
//Merci CrazyCat
function pop_it(the_form , height , width) {
	if(typeof height == 'undefined') height=300;
	if(typeof width == 'undefined') width=900;
	my_form = eval(the_form);
	popupP("./wait.php", "popup" , height , width);
	my_form.target = "popup";
	my_form.submit();
}

function pop_no(my_form) {
	my_form.target = '';
	my_form.submit();
}

/**
 * This array is used to remember mark status of rows in browse mode
 */
var marked_row = new Array;

/**
 * enables highlight and marking of rows in data tables
 * Reused from function.js scripts of phpmyadmin : @version $Id: lib.js,v 1.4 2008/05/15 13:22:06 ranchbe Exp $
 * http://www.phpmyadmin.net
 *
 */
function PMA_markRowsInit() {
    // for every table row ...
    var rows = document.getElementsByTagName('tr');
    var count = Rb_countSelectedRowsInit();
    var displaybox = document.getElementById( 'displaySelectedRowCount' );

    for ( var i = 0; i < rows.length; i++ ) {
        if ( 'odd' != rows[i].className.substr(0,3) && 'even' != rows[i].className.substr(0,4) ) {
            continue;
        }
        var checkbox = rows[i].getElementsByTagName('input')[0];
        if ( checkbox && checkbox.type == 'checkbox' ) {
            unique_id = checkbox.name + checkbox.value;
            marked_row[unique_id] = checkbox.checked;
            if(checkbox.checked){
            	rows[i].className = rows[i].className.replace(' marked', '');
                rows[i].className += ' marked';
            }else{
            	rows[i].className = rows[i].className.replace(' marked', '');
            }
        }
    }
    
    for ( var i = 0; i < rows.length; i++ ) {
        // ... with the class 'odd' or 'even' ...
        if ( 'odd' != rows[i].className.substr(0,3) && 'even' != rows[i].className.substr(0,4) ) {
            continue;
        }
        // ... add event listeners ...
        // ... to highlight the row on mouseover ...
        if ( navigator.appName == 'Microsoft Internet Explorer' ) {
            // but only for IE, other browsers are handled by :hover in css
            rows[i].onmouseover = function() {
                this.className += ' hover';
            }
            rows[i].onmouseout = function() {
                this.className = this.className.replace( ' hover', '' );
            }
        }
        // Do not set click events if not wanted
        if (rows[i].className.search(/noclick/) != -1) {
            continue;
        }
        // ... and to mark the row on click ...
        rows[i].onmousedown = function() {
            var unique_id;
            var checkbox;

            checkbox = this.getElementsByTagName( 'input' )[0];
            if ( checkbox && checkbox.type == 'checkbox' ) {
                unique_id = checkbox.name + checkbox.value;
            } else if ( this.id.length > 0 ) {
                unique_id = this.id;
            } else {
                return;
            }

            if ( typeof(marked_row[unique_id]) == 'undefined' || !marked_row[unique_id] ) {
                marked_row[unique_id] = true;
            } else {
                marked_row[unique_id] = false;
            }

            if ( marked_row[unique_id] ) {
                this.className += ' marked';
            } else {
                this.className = this.className.replace(' marked', '');
            }

            if ( checkbox && checkbox.disabled == false ) {
                checkbox.checked = marked_row[unique_id];
            }
            
            // update count
            if(displaybox){
                if(checkbox.checked == true){
                	count = count + 1;
            	}else{
                	count = count - 1;
                }
                displaybox.innerHTML = count;
            }
            
        }

        // ... and disable label ...
        var labeltag = rows[i].getElementsByTagName('label')[0];
        if ( labeltag ) {
            labeltag.onclick = function() {
                return false;
            }
        }
        
        // .. and checkbox clicks
        var checkbox = rows[i].getElementsByTagName('input')[0];
        if ( checkbox ) {
            checkbox.onclick = function() {
                // opera does not recognize return false;
                this.checked =! this.checked;
            }
        }
        
        //... and update the display ...
        if(displaybox){
            displaybox.innerHTML = count;
        }
        
    }
}
window.onload=PMA_markRowsInit;


/**
 * Display the count of row selected by PMA_markRowsInit
 */
function Rb_countSelectedRowsInit() {
    // for every table row ...
    var count = 0;
    var rows = document.getElementsByTagName('tr');
    for ( var i = 0; i < rows.length; i++ ) {
        // ... with the class 'odd' or 'even' ...
        if ( 'odd' != rows[i].className.substr(0,3) && 'even' != rows[i].className.substr(0,4) ) {
            continue;
        }
        var checkbox;
        checkbox = rows[i].getElementsByTagName( 'input' )[0];
        if ( checkbox && checkbox.disabled == false && checkbox.checked == true ) {
        	count++;
        }
    }
    return count;
}


//==============================================================================
//Fonctions pour assigner un ensemble de champs avec la valeur d'un champ de reference.
//Voir le smartStore...

//Recupere le nom de l'element en supprimant le '[n]'
function setClassName(element){
	name=element.name;
	//name = 'toto_id[1][2][3]';
	//name="propa[1]";
	var reg = new RegExp("\\[[0-9a-zA-Z]+\\]$", "g");
	var ret = name.split(reg);
	return ret[0];
}

//Recupere tous les elements appartenant � la meme classe
function getElementByClassName(formName,className) {
	var ret = new Array();
	for (i=0;i<document.forms[formName].elements.length;i++) { //Calcul le nombre d'element dans le formulaire
		var tclassName = setClassName(document.forms[formName].elements[i]);
		if ( tclassName == className) {
			ret.push( document.forms[formName].elements[i] );
		}
	}
	return ret;
}

//Recupere la valeur de l'element selon son type
function getValue(element){
	switch(element.type){
	case 'text':
	case 'textarea':
		return element.value;
		break;
	case 'select-one':
		return element.selectedIndex;
		break;
	case 'checkbox':
		return element.checked;
		break;
	}
}

//Set value of all element with same class name to the current value of element
//ClassName is the name of element without ending '[n]'
function setValue(elementId){
	element = document.getElementById(elementId);
	var formName = element.form.name;
	var className = setClassName(element);
	var elements = getElementByClassName(formName,className);
	//alert(element.name+"-"+className);
	//alert(elementId);
	switch(element.type){
	case 'text':
	case 'textarea':
		for (i=0;i<elements.length;i++) {
			elements[i].value = getValue(element);
		}
		break;
	case 'select-one':
		for (i=0;i<elements.length;i++) {
			elements[i].selectedIndex = getValue(element);
		}
		break;
	case 'checkbox':
		for (i=0;i<elements.length;i++) {
			elements[i].checked = getValue(element);
		}
		break;
	}
}

function InputPostit(title, url){
	var newComment = prompt( title, '' );
	if(newComment == null){
		return false;
	}
	href = url + '/comment/' + newComment;
	document.location.href = href;
	return true;
}

function initTbody(){
	if(screen.height<900){
		if( dojo.byId("contentTbody")){
			dojo.removeClass("contentTbody","scrollContent");
			dojo.addClass("contentTbody","MiniScrollContent");
		}
	}
}

