<?php
session_start();
/**
 * This scripts is use to get the content of a file.
 * To ensure security, the session array var "ticket_gf" must container a key of name =
 * md5 hash of the full file path.
 * This hash is compare before delivery of the file.
 * At end of execution the session ticket is suppressed
 */
$file = htmlspecialchars_decode($_REQUEST['file']);
$ticket = md5( $file );
if($_SESSION['ticket_gf'])
	$ticketKey = array_search($ticket, $_SESSION['ticket_gf']);

if( $ticketKey === false ){
	echo 'bad security ticket. You have no permission to display this file';
	die;
}

/*
 else{
 unset($_SESSION['ticket_gf'][$ticketKey]);
 }
 */

$filename = basename($file);

if( !empty($_REQUEST['file_mime_type']) ){
	header( 'Content-type: '.$_REQUEST['file_mime_type'] );
}else{
	header( 'Content-type: application/data' );
}
header("Content-Transfer-Encoding: $filename\n"); // Surtout ne pas enlever le \n
header("Content-Length: ".filesize($file));
header("Pragma: no-cache");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
header("Expires: 0");
echo file_get_contents( $file );
die;

