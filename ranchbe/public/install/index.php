<?php 
include('source/boot.inc.php');
session_destroy();
?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" >
<head>
  <title>RanchBE Installation</title>
</head>
<body style="background-color:#BFBFBF;">

<center><div id="test" style="position:center;background-position:center;background-color:#EFEFE5;color:purple;width:90%;height:100%;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
<h1>RanchBE Installation</h1>
<form method='POST' name="install">
	<h3><div style="text-align:justify; font-size: 80%; color: purple; width:80%">
	<p >RanchBE est un logiciel de gestion de documents � d�stination des bureaux d��tude utilisant les outils de CAO (architecture, m�canique, infographie 3D, assainissement, charpente m�tallique, BTP... etc).</p>
<p>Mais il peut �galement satisfaire toutes les organisations qui souhaitent mettre en place un syst�me de gestion documentaire simple, efficace, facile � mettre en oeuvre, s�curis� par une fine gestion des permissions mais cependant hautement extensible gr�ce � la possibilit� d��crire ces propres scripts et qui offre en plus des outils de travail collaboratif.<br/><br/><br/>
<span style="float: left; width: 200px;">
<img src="img/ranchBe_logo.png" alt="" ></span></p>

<p >L�utilisation de technologies libres et standards et d�un langage des plus r�pandu tel que PHP vous assure de n�avoir aucunes difficult�s � trouver les comp�tences n�cessaires pour d�velopper vos scripts ou ajouter des modules � RanchBE (si vous ne souhaitez pas le faire vous m�me).</p>

<p >Il est dans sa version actuelle apparent� � une GED mais il est destin� � �voluer pour int�grer la gestion de produits et devenir ainsi une GED/PDM.</p>

<p >ATTENTION&nbsp;: RanchBE est toujours en d�veloppement. Ce qui signifie que la structure du code source �volue sans n�cessairement conserver la compatibilit� avec les versions pr�c�dentes.</p>

<p ><span style="float: left; width: 31px;">
</div>
<input type='button' value='Begin installation' onclick="document.location.href='source/checkSystem.php'"/>

</div>

</form>

</div></center>
</body>
</html>

