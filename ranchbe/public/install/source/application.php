<?php
include('boot.inc.php');
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
	$_SESSION['typeServer'] = 'windows';
}else{
	$_SESSION['typeServer'] = 'linux';
}
$ranchbeVersion = trim(file_get_contents('./../version.ini'));
$_SESSION['ranchbeVersion'] = $ranchbeVersion;

if($_SESSION['typeServer'] == 'windows'){
	$appDir = 'C:/ranchbe';
}else{
	$appDir = '/usr/ranchbe';
}
$fullAppDir = 'rb'.$ranchbeVersion;
?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" >
<head>
  <title>Ranchbe install</title>
</head>
<body style="background-color:#BFBFBF;">
<center>
	<div id="test" style="position:center;background-position:center;background-color:#EFEFE5;color:purple;width:90%;height:100%;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
		<br />
		<style type="text/css">
			body {font-family: Georgia, "Times New Roman", Times, serif;}
			.tumevoispas{ visibility: hidden;}
			.tumevois{ visibility: visible;}
			.next{
				 border:none;
				 color:#800080;
				 background: transparent url('../img/next.png') no-repeat top left;
				 width:75px;
				 text-decoration:none;
				 font-family:monospace;
				 cursor: pointer;
				 text-indent:2ex;
			}
			.prev{
				 border:none;
				 color:#800080;
				 background: transparent url('../img/prev.png') no-repeat top left;
				 width:75px;
				 text-decoration:none;
				 cursor: pointer;
				 text-indent:2ex;
				 font-family:monospace;
			}
		</style>
		<h1>Application folder</h1>
		Application folder is the directory wich contains all applications files. This directory should not be shared on the web and contains
		only 'statics' datas, so this directory can be put in a little disk partition.
		It must be writable by the ranchbe web server just for the time of installation (probably the user: <?php echo get_current_user() ?>).
		
		<form method='POST' name="install">
			<table style='text-align:right;'>
				<?php if(isset($_SESSION['appId'])){ ?>
					<tr><th style='color:red'>Application folder has been previously created in <?php echo $_SESSION['appId'] ?></th>
					</tr>
				<?php }else{ ?>
					<tr><th>Choice your emplacement for the application folder : 
							<br /><i>A subdirectory with name "<?php echo $fullAppDir ?>" in this path will be created</i>
					</th><th><input id='appId' type='text' size='60px' name='appId' value='<?php echo $appDir ?>'/></th></tr>
				<?php
					}
				?>
			</table>
			<br/>
			<br/>
			<input id="next" name="next" class="next" type="submit" value="next"><br/><br/>
		</form>

		<div style="overflow:auto;color:black;background-color:#EFEFE5;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
			<div style="left:1cm;top:1cm;text-align:left;">
				<style type="text/css">
					body {font-family: Georgia, "Times New Roman", Times, serif;}
					.tumevoispas{ visibility: hidden;}
					.tumevois{ visibility: visible;}
				</style>

				<?php
					if(!empty($_POST)){
						if( !$_SESSION['appId'] ){
							$destination = dezipApplication($ranchbeVersion);
							$_SESSION['appId']= $destination;
							header('Location: rbdatas.php');
						}else{
							$destination = dezipApplication($ranchbeVersion);
							if($_POST['appId'] == "" || $destination == $_SESSION['appId']){
								header('Location: rbdatas.php');
							}else{
								$_SESSION['appId']= $destination;
								header('Location: rbdatas.php');
							}
						}
					}
				?>
			</div>
		</div>
	</div>
</center>
</body>
</html>

<?php
function dezipApplication($ranchbeVersion){
	include('./Directory.php');
	set_time_limit  (  200000 );
	$current_dir = getcwd();
	$current_dir = str_replace("\\", "/", $current_dir);
	if(!empty($_POST['appId'])){
		$Path = $_POST['appId'].'/';
		$Path = str_replace("\\", "/", $Path);
		$Path = str_replace("//", "/", $Path);
		$Path = trim($Path);
		$pathApplidep = str_replace("//", "/", $Path);
	}
	/*
	if(is_dir($_SESSION['appId'] ."/$ranchbeVersion")){
		$res1 = dezipDirectory::rmdir_All($_SESSION['appId']."/$ranchbeVersion");
	}
	*/
	
	$origine = realpath ("$current_dir/../application.zip");
	$destination = $pathApplidep;
	if(substr($destination, -1)=='/'){
		$destination = substr($destination, 0, -1);
		$destination = trim($destination);
	}
	
	$destinationFull = "$destination/rb$ranchbeVersion";
	$_SESSION['fullAppId'] = $destinationFull;
	$zip = new ZipArchive;
	$ok = $zip->open($origine);
	if ( $ok === true | !$origine ) {
		if(!is_dir( $destinationFull ) ){
			if (!mkdir($destinationFull, 0755, true)) {
				die('<b style=\'color:red\'>Failed to create the directory of the Application!! Look at the permission of the path: '.$destination);
			}else{
				$zip->extractTo($destinationFull);
				$zip->close();
				echo 'ok';
			}
		}else{
			echo "<b style='color:red'>The application directory $destinationFull is existing</b>";
		}
		return $destination;
	} else {
		echo 'Package Application Not Found...';
		return false;
	}
	
}
