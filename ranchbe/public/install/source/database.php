<?php 
include('boot.inc.php');
?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" >
<head>
  <title>Ranchbe Installation</title>
</head>
<body style="background-color:#BFBFBF;">
<center>
	<style type="text/css">
		body {font-family: Georgia, "Times New Roman", Times, serif;}
		.tumevoispas{ visibility: hidden;}
		.tumevois{ visibility: visible;}

		.next{
			 border:none;
			 color:#800080;
			 background: transparent url('../img/next.png') no-repeat top left;
			 width:75px;
			 text-decoration:none;
			 font-family:monospace;
			 cursor: pointer;
			 text-indent:2ex;
		}
		.prev{
			 border:none;
			 color:#800080;
			 background: transparent url('../img/prev.png') no-repeat top left;
			 width:75px;
			 text-decoration:none;
			 cursor: pointer;
			 text-indent:2ex;
			 font-family:monospace;
		}
	</style>
	<div id="test" style="position:center;background-position:center;background-color:#EFEFE5;color:purple;width:90%;height:100%;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
		<br/>
		<h1>Ranchbe Installation</h1>
		<form method='POST' name="install">
		<input type="hidden" id="todo" name="todo" value="" />
			<h3>Language:</h3>
			<div class='choice_langage'>
				<table style='text-align:right;'>
					<tr><th>Choice your language:</th><th>
					<select id='langage' name='langage'>
						<option value='fr' selected>Fran�ais
						<option value='en'>English
					</select></th></tr>
				</table>
			</div>
			<br/>
			<br/>
			<h3>Data Base Configuration:</h3>
			<div class='dataBase'>
				<table style='text-align:right;'>
					<tr><th>Enter your Hostname : </th><th><input type='text' size='35px' id='host' name='host' <?php if (isset($_SESSION['host'])) { echo "value='".$_SESSION['host']."'"; }else { echo "value='localhost'"; }?> /></th></tr>
					<tr><th>Enter your data base name : </th><th><input type='text' size='35px' id='databaseName'  name='databaseName' <?php if (isset($_SESSION['databaseName'])) { echo "value='".$_SESSION['databaseName']."'"; }else { echo "value='ranchbe'"; }?>/></th></tr>
					<tr><th>Enter your mysql root password : </th><th><input type='text' size='35px' id='mdpRoot'  name='mdpRoot' <?php if (isset($_SESSION['mdpRoot'])) { echo "value='".$_SESSION['mdpRoot']."'"; }else { echo "value=''";} ?> /></th></tr>
					<tr><th>Enter your mysql identify RanchBE : </th><th><input type='text' size='35px' id='usrRchbe' name='usrRchbe'  <?php if (isset($_SESSION['usrRchbe'])) { echo "value='".$_SESSION['usrRchbe']."'"; }else { echo "value='ranchbe'";} ?>/></th></tr>
					<tr><th>Enter your mysql password RanchBE : </th><th><input type='text' size='35px' id='mdpRchbe' name='mdpRchbe' value="mypassword" value='rbpsswd' <?php if (isset($_SESSION['mdpRchbe'])) { echo "value='".$_SESSION['mdpRchbe']."'"; }else { echo "value='mypassword'";} ?>/></th></tr>
					<tr class="tumevoispas" id="message"><br/><th style='color:red'>Keep this database : <input name="dbconf" class="next" type="submit" value="next" /></a> </th></tr>
				</table>
			</div>
			<br/>
			<br/>
			<input name="prev" type="submit" class="prev" value="previous">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a name="next" class="next" value="next" href="javascript:ajax()">&nbsp;&nbsp;&nbsp;next</a><br/><br/>
		</form>
		<div style="overflow:auto;color:black;background-color:#EFEFE5;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
			<div style="left:1cm;top:1cm;text-align:left;">
				
				<?php
					if(!empty($_POST)){
						if($_POST['prev'] == "previous"){
							header('Location: environement.php');
						}else{
							$ranchbeVersion = file_get_contents("../version.ini");
							$dbhost = $_POST['host'];
							$dbname = $_POST['databaseName'];
							$mdpRoot= $_POST['mdpRoot'];
							$usrRchbe = $_POST['usrRchbe'];
							$mdpRchbe = $_POST['mdpRchbe'];
							$lang = $_POST['langage'];
							$langMaj = strtoupper($lang);
							$current_dir = getcwd();
							$current_dir = str_replace("\\", "/", $current_dir);
							$_SESSION['host'] = $dbhost;
							$_SESSION['databaseName'] = $dbname;
							$_SESSION['mdpRoot'] = $mdpRoot;
							$_SESSION['usrRchbe'] = $usrRchbe;
							$_SESSION['mdpRchbe'] = $mdpRchbe ;
							$_SESSION['langage'] = $lang;
							
							if( $_POST['dbconf']=="next" || $_POST['todo']=='skip' ){
								header('Location: depot.php');
							}else{
								set_time_limit  (  3600 );
								
								$dbFile = "ranchbe_$lang.sql";
								if ( !is_file($dbFile) ) {
									echo "<b style='color:red'>The data base file isn't present in this folder: </b>".$current_dir.'/<br/>';
									die;
								}
								
								//data base creation-------------------------------
								$connect = mysql_connect($dbhost,'root',$mdpRoot) or die("<b style='color:red'>Data Base connection Failed.</b><br/>");
								$sql = 'DROP DATABASE IF EXISTS `'.$dbname.'`;';
								mysql_query($sql) or die('SQL Error!<br/>'.$sql.'<br/>'.mysql_error());
								$sql = 'CREATE DATABASE IF NOT EXISTS `'.$dbname.'` DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;';
								mysql_query($sql) or die('SQL Error!<br/>'.$sql.'<br/>'.mysql_error());
								$sql = 'GRANT ALL PRIVILEGES ON ranchbe.* TO "'.$usrRchbe.'"@localhost IDENTIFIED BY "'.$mdpRchbe.'";';
								mysql_query($sql) or die('SQL Error!<br/>'.$sql.'<br/>'.mysql_error());
								$sql = 'GRANT SELECT ON ranchbe.* TO "rbconsult"@localhost IDENTIFIED BY "'.$mdpRchbe.'";';
								mysql_query($sql) or die('SQL Error!<br/>'.$sql.'<br/>'.mysql_error());
								$sql = 'GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, ALTER, INDEX, DROP, CREATE TEMPORARY TABLES, CREATE VIEW, SHOW VIEW, EXECUTE ON ranchbe.* TO "rb"@localhost IDENTIFIED BY "'.$mdpRchbe.'";';
								mysql_query($sql) or die('SQL Error!<br/>'.$sql.'<br/>'.mysql_error());
								
								// connection to the DB
								mysql_select_db($dbname) or die (mysql_error());
								$dbFile = "ranchbe_$lang.sql";
								if ( is_file($dbFile) ) {
									$file = fopen($dbFile, 'r');
									$data='';
									while(!feof($file)){
										$var=fread($file,1);
										if ($var!=';'){
											$data.= $var;
										}else{
											$data.= $var;
											mysql_query( $data ) or die("<b style='color:red'>SQL Error: <br/>".mysql_error().'</b><br/>');
											$data = '';
										}
									}
									fclose($file);
								}
								header('Location: depot.php');
							}
						}
					}
				?>
			</div>
		</div>
	</div>
</center>

<script type="text/javascript" src="./jQuery.js"></script>
<script type="text/javascript" >
function hasClass(ele,cls) {
	return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}

function addClass(ele,cls) {
	if (!this.hasClass(ele,cls)) ele.className += " "+cls;
}

function removeClass(ele,cls) {
	if (hasClass(ele,cls)) {
		var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		ele.className=ele.className.replace(reg,' ');
	}
}

function ajax()
{	
	var databaseName = document.getElementById("databaseName").value;
	var host = document.getElementById("host").value;
	var mdpRoot = document.getElementById("mdpRoot").value;
	$.ajax({
			type: "GET",
			url: './testBase.php',
			dataType: 'html',
			data: 'databaseName='+databaseName+'&host='+host+'&mdpRoot='+mdpRoot,
			global: true,
			error : function(XMLHttpRequest, textStatus, errorThrown) { alert(textStatus); } ,
			success: function(result) { 
				if(result.indexOf('con_non_ok') != -1){
					alert("Error root password connection");
				}else if(result.indexOf('db_to_create') != -1){
					alert("Db must be created");
					document.getElementById('todo').value = 'createdb';
					document.install.submit();
					return;
				}else if(result.indexOf('database_non_ok') != -1){
					if(confirm("error database created. do you want erase it?")){
						//submit form "install"
						if(document.install.onsubmit && !document.install.onsubmit()){
							return;
						}
						document.getElementById('todo').value = 'createdb';
						document.install.submit();
					}else{
						document.getElementById('todo').value = 'skip';
						removeClass(document.getElementById("message"),"tumevoispas");
						addClass(document.getElementById("message"),"tumevois");
					}
				}
			}
	});
}
</script>
</body>
</html>
 