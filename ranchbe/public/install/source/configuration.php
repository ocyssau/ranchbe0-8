<?php
	include('boot.inc.php');
	$ranchbeVersion = $_SESSION['ranchbeVersion'];
	$rbinifile = $_SESSION['fullAppId'] . "/application/configs/rb.ini";
	$rbsamplefile = $_SESSION['fullAppId'] . "/application/configs/rb.sample";

	$publicpath = realpath(dirname ( __FILE__ ) . '../../../.' );
	$publicpath = str_replace ( '\\', '/', $publicpath);

	if(is_file($rbinifile)){
		rename($rbinifile, $rbinifile.'.'.time());
	}

	//fr-FR|en-EN
	$htmlLang = strtolower($_SESSION['langage']) . '-' . strtoupper($_SESSION['langage']);
	//fr|en
	$mainlang = strtolower($_SESSION['langage']);
	
	if(is_file($rbsamplefile)){
        $rbdatas = 'rbdatas';
		$rbini = file_get_contents($rbsamplefile);
		$rbini = str_replace('%mainlang%', $mainlang , $rbini);
		$rbini = str_replace('%htmllang%', $htmlLang , $rbini);
		$rbini = str_replace('%host%', $_SESSION['host'] , $rbini);
		$rbini = str_replace('%dbname%', $_SESSION['databaseName'] , $rbini);
		$rbini = str_replace('%rootpsswd%', $_SESSION['mdpRoot'] , $rbini);
		$rbini = str_replace('%userRB%', $_SESSION['usrRchbe'] , $rbini);
		$rbini = str_replace('%userpsswdRB%', $_SESSION['mdpRchbe'] , $rbini);
		$rbini = str_replace('%rbdatapath%', $_SESSION['rbId'] ."/$rbdatas/" , $rbini);
		$rbini = str_replace('%rbpublicpath%', "/$publicpath/" , $rbini);
		$rbini = str_replace('%path.reposit.workitem%', $_SESSION['aff'].'/workitem' , $rbini);
		$rbini = str_replace('%path.reposit.mockup%', $_SESSION['maquette'].'/mockup' , $rbini);
		$rbini = str_replace('%path.reposit.cadlib%', $_SESSION['librairies'].'/cadlib' , $rbini);
		$rbini = str_replace('%path.reposit.bookshop%', $_SESSION['bibli'].'/bookshop' , $rbini);
		$rbini = str_replace('%path.reposit.wildspace%', $_SESSION['wildspaces'].'/wild_space' , $rbini);
		$rbini = str_replace('%path.read.workitem%', $_SESSION['raff'].'/read/workitem' , $rbini);
		$rbini = str_replace('%path.read.mockup%', $_SESSION['rmaquette'].'/read/mockup' , $rbini);
		$rbini = str_replace('%path.read.cadlib%', $_SESSION['rlibrairies'].'/read/cadlib' , $rbini);
		$rbini = str_replace('%path.read.bookshop%', $_SESSION['rbibli'].'/read/bookshop' , $rbini);
		file_put_contents($rbinifile, $rbini);
	}else{
		die("$rbsamplefile not found...");
	}
	//$rootUrl = $host . ':' . $port .' / '. dirname((dirname( $_SERVER['SCRIPT_NAME'] ));
	
	header('Location: finInstallation.php');
?>