<?php 
include('boot.inc.php');

/*
if($_SESSION['typeServer']=="windows"){
	$rbDir = 'C:/ranchbe';
}else{
	$rbDir = '/var/ranchbe';
}
*/
$rbDir = $_SESSION['appId'];
$subDir = 'rbdatas';

?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" >
<head>
  <title>RanchBE Installation</title>
</head>
<body style="background-color:#BFBFBF;">
<center>
	<div id="test" style="position:center;background-position:center;background-color:#EFEFE5;color:purple;width:90%;height:100%;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
		<br/>
		<h1>Rbdatas folder</h1>
		Rbdatas folder is the directory wich contains all files put by users and other customizable files. This directory should not be shared on the web.
		It should be on a big disk partition.
		It must be writable by the ranchbe web server (probably the user: <?php echo get_current_user() ?>).
		<form method='POST' name="install">
			<br/>
			<br/>
			<table style='text-align:right;'>
				<?php 	if(isset($_SESSION['rbId'])){ ?>
					<tr><th style='color:red'>Rbdatas folder has been previously created in <?php echo $_SESSION['rbId'] ?></th>
					</tr>
				<?php }else{ ?>
					<tr><th>Choice your emplacement for the folder rbdatas : 
						<br /><i>A subdirectory with name "<?php echo $subDir ?>" in this path will be created</i>
					</th>
					<th><input id='rbId' type='text' size='60px' name='rbId' value='<?php echo $rbDir ?>'/></th></tr> 
				<?php } ?>
			</table>
			<br/>
			<br/>
			<input id="prev" name="prev" type="submit" class="prev" value="previous">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="next" class="next" name="next" type="submit" value="next"><br/><br/>
		</form>

		<div style="overflow:auto;color:black;background-color:#EFEFE5;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
			<div style="left:1cm;top:1cm;text-align:left;">
				<style type="text/css">
					body {font-family: Georgia, "Times New Roman", Times, serif;}
					.tumevoispas{ visibility: hidden;}
					.tumevois{ visibility: visible;}
					.next{
						 border:none;
						 color:#800080;
						 background: transparent url('../img/next.png') no-repeat top left;
						 width:75px;
						 text-decoration:none;
						 font-family:monospace;
						 cursor: pointer;
						 text-indent:2ex;
					}
					.prev{
						 border:none;
						 color:#800080;
						 background: transparent url('../img/prev.png') no-repeat top left;
						 width:75px;
						 text-decoration:none;
						 cursor: pointer;
						 text-indent:2ex;
						 font-family:monospace;
					}
				</style>

				<?php
					if(!empty($_POST)){
						$rbdatas = "rbdatas";
						if($_POST['prev'] == "previous"){
							header('Location: application.php');
						}else{
							if(!isset($_SESSION['rbId'])){
								include('./Directory.php');
								set_time_limit  (  100000 );
								$current_dir = getcwd();
								$current_dir = str_replace("\\", "/", $current_dir);
								if(!empty($_POST['rbId'])){
									$rbDatasPath = $_POST['rbId'].'/';
									$pathRbdatasdep = str_replace("\\", "/", $rbDatasPath);
									$pathRbdatasdep = str_replace("//", "/", $pathRbdatasdep);
									$pathRbdatasdep = trim($pathRbdatasdep);
								}
								$origine = "$current_dir/../rbdata.zip";
								$destination = $pathRbdatasdep;
								if(substr($destination, -1)=='/'){
									$destination = substr($destination, 0, -1);
									$destination = trim($destination);
								}
								$zip = new ZipArchive;
								$res = $zip->open($origine);
								if ($res === TRUE) {
									if(!is_dir($destination."/$rbdatas")){
										if (!mkdir($destination ."/$rbdatas", 0755, true)) {
											die('<b style=\'color:red\'>Failed to create the directory of the rbdatas!! Look at the permission of the path: '.$destination);//.'<br/>And get the create permissions at the user :'.get_current_user().'</b>');
										}else{
											$zip->extractTo($destination ."/$rbdatas/");
											$zip->close();
											echo 'ok';
										}
									}
								} else {
									echo 'Package RbDatas Not Found';
								}
								$_SESSION['rbId']= $destination;
								header('Location: environement.php');
							}else{
								if(!empty($_POST['rbId'])){
									$rbDatasPath = $_POST['rbId'].'/';
									$pathRbdatasdep = str_replace("\\", "/", $rbDatasPath);
									$pathRbdatasdep = str_replace("//", "/", $pathRbdatasdep);
									$pathRbdatasdep = trim($pathRbdatasdep);
								}
								$current_dir = getcwd();
								$current_dir = str_replace("\\", "/", $current_dir);
								$origine = "$current_dir/../rbdata.zip";
								$destination = $pathRbdatasdep;
								if(substr($destination, -1)=='/'){
									$destination = substr($destination, 0, -1);
									$destination = trim($destination);
								}

								if($_POST['rbId']=="" || $_POST['rbId'] == $_SESSION['rbId']){
									header('Location: environement.php');
								}else{
									include('./Directory.php');
									set_time_limit  (  200000 );
									if(is_dir($_SESSION['rbId'] ."/$rbdatas")){
										$res1 = dezipDirectory::rmdir_All($_SESSION['rbId'] ."/$rbdatas");
									}
									
									$zip = new ZipArchive;
									$res = $zip->open($origine);
									if ($res === TRUE) {
										if(!is_dir($destination."/$rbdatas")){
											if (!mkdir($destination ."/$rbdatas", 0755, true)) {
												die('<b style=\'color:red\'>Failed to create the directory of the rbdatas!! Look at the permission of the path: '.$destination);//.'<br/>And get the create permissions at the user :'.get_current_user().'</b>');
											}else{
												$zip->extractTo($destination ."/$rbdatas/");
												$zip->close();
												echo 'ok';
											}
										}
									} else {
										echo 'Package RbDatas Not Found';
									}
									$destination = trim($destination);
									$_SESSION['rbId']= $destination;
									header('Location: environement.php');
								}
							}
						}
					}
				?>
			</div>
		</div>
	</div>
</center>
</body>
</html>

