<?php 
include('boot.inc.php');
$repositDir = $_SESSION['appId'] . '/reposits';
$wsDir = $_SESSION['appId'] . '/wildspace';
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" >
<head>
  <title>RanchBE Installation</title>
</head>
<body style="background-color:#BFBFBF;">
<center>
	<div id="test" style="position:center;background-position:center;background-color:#EFEFE5;color:purple;width:90%;height:100%;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
		<br/>
		<h1>RanchBE Installation</h1>
		<form method='POST' name="install">
			<br/>
			<br/>
			<table style='text-align:right;'>
			
				<?php 	if(isset($_SESSION['aff'])){ ?>
					<tr><th style='color:red'>Reposit has been previously created in <?php echo $_SESSION['aff'] ?></th>
					</tr>
				<?php }else{ ?>
					<tr><th>Choice your emplacement for the workitems reposits : 
						<br /><i>A subdirectory with name "workitem" in this path will be created</i>
					</th>
					<th><input id='aff' type='text' size='60px' name='aff' value='<?php echo $repositDir ?>'/></th></tr>
				<?php } ?>
				
				<?php 	if(isset($_SESSION['maquette'])){ ?>
					<tr><th style='color:red'>Reposit has been previously created in <?php echo $_SESSION['maquette'] ?></th>
					</tr>
				<?php }else{ ?>
					<tr><th>Choice your emplacement for the mockups reposits : 
						<br /><i>A subdirectory with name "mockup" in this path will be created</i>
					</th>
					<th><input id='maquette' type='text' size='60px' name='maquette' value='<?php echo $repositDir ?>'/></th></tr> 
				<?php } ?>
				
				
				<?php 	if(isset($_SESSION['librairies'])){ ?>
					<tr><th style='color:red'>Reposit has been previously created in <?php echo $_SESSION['librairies'] ?></th>
					</tr>
				<?php }else{ ?>
					<tr><th>Choice your emplacement for the libraries reposits : 
						<br /><i>A subdirectory with name "library" in this path will be created</i>
					</th>
					<th><input id='librairies' type='text' size='60px' name='librairies' value='<?php echo $repositDir ?>'/></th></tr> 
				<?php } ?>
				
			
				<?php 	if(isset($_SESSION['bibli'])){ ?>
					<tr><th style='color:red'>Reposit has been previously created in <?php echo $_SESSION['bibli'] ?></th>
					</tr>
				<?php }else{ ?>
					<tr><th>Choice your emplacement for the bookshops reposits : 
						<br /><i>A subdirectory with name "bookshop" in this path will be created</i>
					</th>
					<th><input id='bibli' type='text' size='60px' name='bibli' value='<?php echo $repositDir ?>'/></th></tr> 
				<?php } ?>


				<?php 	if(isset($_SESSION['wildspaces'])){ ?>
					<tr><th style='color:red'>Reposit has been previously created in <?php echo $_SESSION['wildspaces'] ?></th>
					</tr>
				<?php }else{ ?>
					<tr><th>Choice your emplacement for the user wildspaces directories : 
						<br /><i>A subdirectory with name "bookshop" in this path will be created</i>
					</th>
					<th><input id='wildspaces' type='text' size='60px' name='wildspaces' value='<?php echo $wsDir ?>'/></th></tr>
				<?php } ?>
			</table>
			<br/>
			<br/>
			<input id="prev" name="prev" class="prev" type="submit" value="previous">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="next"  id="next" name="next" type="submit" value="next"><br/><br/>
		</form>

		<div style="overflow:auto;color:black;background-color:#EFEFE5;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
			<div style="left:1cm;top:1cm;text-align:left;">
				<style type="text/css">
					body {font-family: Georgia, "Times New Roman", Times, serif;}
					.tumevoispas{ visibility: hidden;}
					.tumevois{ visibility: visible;}
					.next{
						 border:none;
						 color:#800080;
						 background: transparent url('../img/next.png') no-repeat top left;
						 width:75px;
						 text-decoration:none;
						 font-family:monospace;
						 cursor: pointer;
						 text-indent:2ex;
					}
					.prev{
						 border:none;
						 color:#800080;
						 background: transparent url('../img/prev.png') no-repeat top left;
						 width:75px;
						 text-decoration:none;
						 cursor: pointer;
						 text-indent:2ex;
						 font-family:monospace;
					}
				</style>

				<?php
					if(!empty($_POST)){
						if($_POST['prev']=="previous"){
							header('Location: database.php');
						}else{
							if(!isset($_POST['aff'])){
								echo "<b style='color:red'>Error: reposit affaire path missing!</b>";
							}else{
								if(!isset($_POST['maquette'])){
									echo "<b style='color:red'>Error: reposit maquette path missing!</b>";
								}else{
									if(!isset($_POST['librairies'])){
										echo "<b style='color:red'>Error: reposit librairies path missing!</b>";
									}else{
										if(!isset($_POST['bibli'])){
											echo "<b style='color:red'>Error: reposit bibliothèques path missing!</b>";
										}else{
											if(!isset($_POST['wildspaces'])){
												echo "<b style='color:red'>Error: reposit wildspaces path missing!</b>";
											}else{
												$aff = $_POST['aff'];
												$aff = str_replace("\\", "/", $aff);
												$aff = str_replace("//", "/", $aff);
												$aff = str_replace("//", "/", $aff);
												if(substr($aff, -1)=='/'){
													$aff = substr($aff, 0, -1);
												}
												if(!is_dir($aff.'/workitem')){
													if (!mkdir($aff.'/workitem', 0755, true)) {
														die('<b style=\'color:red\'>Failed to create reposit affaire!! Look at the permission of the path: '.$aff.'/workitem <br/>');
													}
												}
												
												$maquette= $_POST['maquette'];
												$maquette = str_replace("\\", "/", $maquette);
												$maquette = str_replace("//", "/", $maquette);
												$maquette = str_replace("//", "/", $maquette);
												if(substr($maquette, -1)=='/'){
													$maquette = substr($maquette, 0, -1);
												}
												if(!is_dir($maquette.'/mockup')){
													if (!mkdir($maquette.'/mockup', 0755, true)) {
														die('<b style=\'color:red\'>Failed to create reposit maquette!! Look at the permission of the path: '.$maquette.'/mockup <br/>');//And get the create permissions at the user :'.get_current_user().'</b>');
													}
												}
													
												$librairies = $_POST['librairies'];
												$librairies = str_replace("\\", "/", $librairies);
												$librairies = str_replace("//", "/", $librairies);
												$librairies = str_replace("//", "/", $librairies);
												if(substr($librairies, -1)=='/'){
													$librairies = substr($librairies, 0, -1);
												}
												if(!is_dir($librairies.'/cadlib')){
													if (!mkdir($librairies.'/cadlib', 0755, true)) {
														die('<b style=\'color:red\'>Failed to create reposit librairies!! Look at the permission of the path: '.$librairies.'/cadlib <br/>');//And get the create permissions at the user :'.get_current_user().'</b>');
													}
												}
												
												$bibli = $_POST['bibli'];
												$bibli = str_replace("\\", "/", $bibli);
												$bibli = str_replace("//", "/", $bibli);
												$bibli = str_replace("//", "/", $bibli);
												if(substr($bibli, -1)=='/'){
													$bibli = substr($bibli, 0, -1);
												}
												if(!is_dir($bibli.'/bookshop')){
													if (!mkdir($bibli.'/bookshop', 0755, true)) {
														die('<b style=\'color:red\'>Failed to create reposit bibliothèques!! Look at the permission of the path: '.$bibli.'/bookshop<br/>');//And get the create permissions at the user :'.get_current_user().'</b>');
													}
												}
												
												$wildspaces = $_POST['wildspaces'];
												$wildspaces = str_replace("\\", "/", $wildspaces);
												$wildspaces = str_replace("//", "/", $wildspaces);
												$wildspaces = str_replace("//", "/", $wildspaces);
												if(substr($wildspaces, -1)=='/'){
													$wildspaces = substr($wildspaces, 0, -1);
												}
												if(!is_dir($wildspaces.'/wild_space')){
													if (!mkdir($wildspaces.'/wild_space', 0755, true)) {
														die('<b style=\'color:red\'>Failed to create reposit wildspaces!! Look at the permission of the path: '.$wildspaces.'/wild_space<br/>');//And get the create permissions at the user :'.get_current_user().'</b>');
													}
												}


												$_SESSION['aff']= $aff;
												$_SESSION['maquette']= $maquette;
												$_SESSION['librairies']= $librairies;
												$_SESSION['bibli']= $bibli;
												$_SESSION['wildspaces']= $wildspaces;

												header('Location: read.php');
											}
										}
									}
								}
							}
						}
					}
				?>
			</div>
		</div>
	</div>
</center>
</body>
</html>

