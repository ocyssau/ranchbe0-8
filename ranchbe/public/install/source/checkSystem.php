<?php 
include('boot.inc.php');
?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" >
<head>
  <title>RanchBE Installation</title>
</head>
<body style="background-color:#BFBFBF;">

<center><div id="test" style="position:center;background-position:center;background-color:#EFEFE5;color:purple;width:90%;height:100%;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
<h1>Check system</h1>
<form method='POST' name="install">

<?php
$errors = array();
$publicPath = realpath('../');
if (!is_writable($publicPath)) {
	$errors[] = "No write access on $publicPath";
}
if (version_compare(phpversion(), '5.3.0') < 0){
	$errors[] = "Actual version of php is" . phpversion();
	$errors[] = "Ranchbe require php v5.3.0 minimum";
} 

$extensions = array();
if (!extension_loaded('gd')){
	$extensions[] = '<a href="http://fr2.php.net/manual/fr/image.installation.php">php_gd</a>';
}
if(!extension_loaded('zip')){
	$extensions[] = '<a href="http://fr2.php.net/manual/fr/zip.installation.php">php_zip</a>';
}
/*
if(!extension_loaded('mysqli')){
	$extensions[] = "php_mysqli";
}
*/
if(!extension_loaded('pdo')){
	$extensions[] = '<a href="http://fr2.php.net/manual/fr/pdo.installation.php">php_pdo</a>';
}
if(!extension_loaded('pdo_mysql')){
	$extensions[] = '<a href="http://fr2.php.net/manual/fr/ref.pdo-mysql.php">php_pdo_mysql</a>';
}

if(count($extensions) > 0 ){
	$error = 'Followings php extensions are not installed';
	$phpIniFile = php_ini_loaded_file();
	$error .= 'You must install it and/or activate from php.ini file('.$phpIniFile.')';
	$error .= '<ul>';
	foreach($extensions as $extension){
		$error .= "<li>$extension</li>";
	}
	$error .= '</ul>';
	$errors[] = $error;
}

	
if(count($errors) > 0){
	foreach($errors as $error){
		echo '<div class="check">';
		echo '<span class="check_error">'.$error.'</span>';
		echo '</div>';
	}
}else{
	echo 'Your configuration is OK!<br />';
	echo "<input type='button' value='Create application' onclick=\"document.location.href='application.php'\"/><br/><br/>";
}

