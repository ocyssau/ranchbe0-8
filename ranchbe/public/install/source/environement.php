<?php 
include('boot.inc.php');
$htfilepath = realpath('./../../.') . '/.htaccess';
?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" >
<head>
  <title>RanchBE Installation</title>
</head>
<body style="background-color:#BFBFBF;">
<center>
	<div id="test" style="position:center;background-position:center;background-color:#EFEFE5;color:purple;width:90%;height:100%;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
		<br/>
		<h1>Environnement</h1>
		This environnement set the file <?php echo $htfilepath ?> to set the environnement. Environnement can help you
		to create differents configurations for differents use case. Each environnement is define by a section in the file [RANCHBE APPLICATION]/application/config/rb.ini
		For a production server, or if you do not know what choose, select 'production'.
		<form method='POST' name="install">
			<br/>
			<br/>
			select the environement : 	<select  name="selectEnv">
											  <option selected value="production">production</option>
											  <option value="testing">testing</option>
											  <option value="development">development</option>
										</select> 
			<br/>
			<br/>
			<input id="prev" name="prev" class="prev" type="submit" value="previous">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="next" name="next" class="next" type="submit" value="next"><br/><br/>
		</form>

		<div style="overflow:auto;color:black;background-color:#EFEFE5;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
			<div style="left:1cm;top:1cm;text-align:left;">
				<style type="text/css">
					body {font-family: Georgia, "Times New Roman", Times, serif;}
					.tumevoispas{ visibility: hidden;}
					.tumevois{ visibility: visible;}
					.next{
						 border:none;
						 color:#800080;
						 background: transparent url('../img/next.png') no-repeat top left;
						 width:75px;
						 text-decoration:none;
						 font-family:monospace;
						 cursor: pointer;
						 text-indent:2ex;
					}
					.prev{
						 border:none;
						 color:#800080;
						 background: transparent url('../img/prev.png') no-repeat top left;
						 width:75px;
						 text-decoration:none;
						 cursor: pointer;
						 text-indent:2ex;
						 font-family:monospace;
					}
				</style>

				<?php
					if(!empty($_POST)){
						if(!empty($_POST['prev'])){
							header('Location: rbdatas.php');
						}else{
							$ranchbeVersion = $_SESSION['ranchbeVersion'];
							$htaccessFile = "../../.htaccess";
							if(is_file($htaccessFile)){
								unlink($htaccessFile);
							}
							$htcontent = file_get_contents("./../htaccess.sample" );
						
							$htcontent = str_replace('%APPLICATION_ENV%', $_POST['selectEnv'] , $htcontent);
							$htcontent = str_replace('%APPLICATION_PATH%', $_SESSION['fullAppId'] ."/application", $htcontent);
							$baseUrl = dirname((dirname( dirname( $_SERVER['SCRIPT_NAME'] ))));
							$baseUrl = trim($baseUrl, '/');
							if($baseUrl){
								$htcontent = str_replace('%BASE_URL%', $baseUrl, $htcontent);
								$INDEX = '/' . $baseUrl . '/index.php';
							}else{
								$htcontent = str_replace('%BASE_URL%', '', $htcontent);
								$INDEX = '/index.php';
							}
							$htcontent = str_replace('%INDEX%', $INDEX, $htcontent);
							file_put_contents($htaccessFile, $htcontent);
							header('Location: database.php');
						}
					}
				?>
			</div>
		</div>
	</div>
</center>
</body>
</html>

