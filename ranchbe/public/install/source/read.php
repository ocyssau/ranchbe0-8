<?php 
include('boot.inc.php');
$repositDir = $_SESSION['appId'] . '/read';
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" >
<head>
  <title>RanchBE Installation</title>
</head>
<body style="background-color:#BFBFBF;">
<center>
	<div id="test" style="position:center;background-position:center;background-color:#EFEFE5;color:purple;width:90%;height:100%;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
		<h1>RanchBE Installation</h1>
		<form method='POST' name="install">
			<table style='text-align:right;'>
				<?php 	if(isset($_SESSION['raff'])){ ?>
					<tr><th style='color:red'>Read has been previously created in <?php echo $_SESSION['raff'] ?></th>
					</tr>
				<?php }else{ ?>
					<tr><th>Choice your emplacement for the workitems reads : 
						<br /><i>A subdirectory with name "workitem" in this path will be created</i>
					</th>
					<th><input id='workitem' type='text' size='60px' name='workitem' value='<?php echo $repositDir ?>'/></th></tr> 
				<?php } ?>
				
				<?php 	if(isset($_SESSION['rmaquette'])){ ?>
					<tr><th style='color:red'>Read has been previously created in <?php echo $_SESSION['rmaquette'] ?></th>
					</tr>
				<?php }else{ ?>
					<tr><th>Choice your emplacement for the mockup reads : 
						<br /><i>A subdirectory with name "mockup" in this path will be created</i>
					</th>
					<th><input id='maquette' type='text' size='60px' name='maquette' value='<?php echo $repositDir ?>'/></th></tr> 
				<?php } ?>
				
			
				<?php 	if(isset($_SESSION['rlibrairies'])){ ?>
					<tr><th style='color:red'>Read has been previously created in <?php echo $_SESSION['rlibrairies'] ?></th>
					</tr>
				<?php }else{ ?>
					<tr><th>Choice your emplacement for the libraries reads : 
						<br /><i>A subdirectory with name "library" in this path will be created</i>
					</th>
					<th><input id='librairies' type='text' size='60px' name='librairies' value='<?php echo $repositDir ?>'/></th></tr> 
				<?php } ?>
			
				<?php 	if(isset($_SESSION['rbibli'])){ ?>
					<tr><th style='color:red'>Read has been previously created in <?php echo $_SESSION['rbibli'] ?></th>
					</tr>
				<?php }else{ ?>
					<tr><th>Choice your emplacement for the bookshops reads : 
						<br /><i>A subdirectory with name "bookshop" in this path will be created</i>
					</th>
					<th><input id='bibli' type='text' size='60px' name='bibli' value='<?php echo $repositDir ?>'/></th></tr> 
				<?php } ?>
				
			</table>
			<br/>
			<br/>
			<input id="prev" name="prev" class="prev" type="submit" value="previous">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="next" class="next" name="next" type="submit" value="next"><br/><br/>
		</form>

		<div style="overflow:auto;color:black;background-color:#EFEFE5;-moz-border-radius:18px 18px 18px 18px;-msie-border-radius:18px 18px 18px 18px;">
			<div style="left:1cm;top:1cm;text-align:left;">
				<style type="text/css">
					body {font-family: Georgia, "Times New Roman", Times, serif;}
					.tumevoispas{ visibility: hidden;}
					.tumevois{ visibility: visible;}
					.next{
						 border:none;
						 color:#800080;
						 background: transparent url('../img/next.png') no-repeat top left;
						 width:75px;
						 text-decoration:none;
						 font-family:monospace;
						 cursor: pointer;
						 text-indent:2ex;
					}
					.prev{
						 border:none;
						 color:#800080;
						 background: transparent url('../img/prev.png') no-repeat top left;
						 width:75px;
						 text-decoration:none;
						 cursor: pointer;
						 text-indent:2ex;
						 font-family:monospace;
					}
				</style>

				<?php
					$errors = array();
					if(!empty($_POST)){
						if(!empty($_POST['prev'])){
							header('Location: depot.php');
						}
						if(!isset($_POST['maquette'])){
							$errors[] = "<b style='color:red'>Error: read maquette path missing!</b>";
						}
						if(!isset($_POST['librairies'])){
							$errors[] = "<b style='color:red'>Error: read librairies path missing!</b>";
						}
						if(!isset($_POST['bibli'])){
							$errors[] = "<b style='color:red'>Error: read bibliothèques path missing!</b>";
						}
						if($errors){
							foreach($errors as $error){
								echo $error . '<br />';
							}
						}else{
							$maquette= $_POST['maquette'];
							$maquette = str_replace("\\", "/", $maquette);
							$maquette = str_replace("//", "/", $maquette);
							$maquette = str_replace("//", "/", $maquette);
							if(substr($maquette, -1)=='/'){
								$maquette = substr($maquette, 0, -1);
							}
							if(!is_dir($maquette.'/read/mockup')){
								if (!mkdir($maquette.'/read/mockup', 0755, true)) {
									die('<b style=\'color:red\'>Failed to create read maquette!! Look at the permission of the path: '.$maquette.'/read/mockup <br/>');//And get the create permissions at the user :'.get_current_user().'</b>');
								}
							}
								
							$librairies = $_POST['librairies'];
							$librairies = str_replace("\\", "/", $librairies);
							$librairies = str_replace("//", "/", $librairies);
							$librairies = str_replace("//", "/", $librairies);
							if(substr($librairies, -1)=='/'){
								$librairies = substr($librairies, 0, -1);
							}
							if(!is_dir($librairies.'/read/cadlib')){
								if (!mkdir($librairies.'/read/cadlib', 0755, true)) {
									die('<b style=\'color:red\'>Failed to create read librairies!! Look at the permission of the path: '.$librairies.'/read/cadlib <br/>');//And get the create permissions at the user :'.get_current_user().'</b>');
								}
							}
							
							$bibli = $_POST['bibli'];
							$bibli = str_replace("\\", "/", $bibli);
							$bibli = str_replace("//", "/", $bibli);
							$bibli = str_replace("//", "/", $bibli);
							if(substr($bibli, -1)=='/'){
								$bibli = substr($bibli, 0, -1);
							}
							if(!is_dir($bibli.'/read/bookshop')){
								if (!mkdir($bibli.'/read/bookshop', 0755, true)) {
									die('<b style=\'color:red\'>Failed to create read bibliothèques!! Look at the permission of the path: '.$bibli.'/read/bookshop<br/>');//And get the create permissions at the user :'.get_current_user().'</b>');
								}
							}
	
							$_SESSION['raff']= $aff;
							$_SESSION['rmaquette']= $maquette;
							$_SESSION['rlibrairies']= $librairies;
							$_SESSION['rbibli']= $bibli;
	
							header('Location: configuration.php');
						}
					}
					
				?>
			</div>
		</div>
	</div>
</center>
</body>
</html>

