<?php

class dezipDirectory{
	 function unzip($file, $path='', $effacer_zip=false)
 {/*M�thode qui permet de d�compresser un fichier zip $file dans un r�pertoire de destination $path
 et qui retourne un tableau contenant la liste des fichiers extraits
 Si $effacer_zip est �gal � true, on efface le fichier zip d'origine $file*/

 $tab_liste_fichiers = array(); //Initialisation

 $zip = zip_open($file);

 if ($zip)
 {
 while ($zip_entry = zip_read($zip)) //Pour chaque fichier contenu dans le fichier zip
 {
 if (zip_entry_filesize($zip_entry) > 0)
 {
 $complete_path = $path.dirname(zip_entry_name($zip_entry));

 /*On supprime les �ventuels caract�res sp�ciaux et majuscules*/
 $nom_fichier = zip_entry_name($zip_entry);
 $nom_fichier = strtr($nom_fichier,"�����������������������������������������������������","AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn");
 $nom_fichier = strtolower($nom_fichier);
 $nom_fichier = ereg_replace('[^a-zA-Z0-9.]','-',$nom_fichier);

 /*On ajoute le nom du fichier dans le tableau*/
 array_push($tab_liste_fichiers,$nom_fichier);

 $complete_name = $path.$nom_fichier; //Nom et chemin de destination

 if(!file_exists($complete_path))
 {
 $tmp = '';
 foreach(explode('/',$complete_path) AS $k)
 {
 $tmp .= $k.'/';

 if(!file_exists($tmp))
 { mkdir($tmp, 0755); }
 }
 }

 /*On extrait le fichier*/
 if (zip_entry_open($zip, $zip_entry, "r"))
 {
 $fd = fopen($complete_name, 'w');

 fwrite($fd, zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)));

 fclose($fd);
 zip_entry_close($zip_entry);
 }
 }
 }

 zip_close($zip);

 /*On efface �ventuellement le fichier zip d'origine*/
 if ($effacer_zip === true)
 unlink($file);
 }

 return $tab_liste_fichiers;
 } 
	
	function rmdir_recurse($path)
	{
		$path= rtrim($path, '/').'/';
		$handle = opendir($path);
		for (;false !== ($file = readdir($handle));)
			if($file != "." and $file != ".." )
			{
				$fullpath= $path.$file;
				if( is_dir($fullpath) )
				{
					self::rmdir_recurse($fullpath);
					rmdir($fullpath);
				}
				else
				  unlink($fullpath);
			}
		closedir($handle);
	}
	
	function rmdir_All($path)
	{
	  self::rmdir_recurse($path);
	  rmdir($path);
	}

}
