<?php
/*
 * This work is hereby released into the Public Domain.
 * To view a copy of the public domain dedication,
 * visit http://creativecommons.org/licenses/publicdomain/ or send a letter to
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.
 *
 */

if(!isset($_REQUEST['values'])) exit;

// Get the data from form and unzeralize it
$values = @unserialize(urldecode(stripslashes($_REQUEST['values'])));
$legend = @unserialize(urldecode(stripslashes($_REQUEST['legend'])));
$title = $_REQUEST['title'];

// Check data
if(!is_array($values) || !is_array($legend)) exit;
if(count($values) == 0) exit;

// Draw graph
require_once '../../external/Artichow/Pie.class.php';

$graph = new Graph(400, 250);
//$graph->setAntiAliasing(true);

$graph->title->set($title);

$colors = array(
    new LightOrange,
    new LightPurple,
    new LightBlue,
    new LightRed,
    new LightPink
);

//$plot = new Pie($values, Pie::EARTH);
$plot = new Pie($values, $colors);
$plot->setLegend($legend);

$plot->setCenter(0.35, 0.55);
$plot->setSize(0.7, 0.6);
$plot->set3D(12);

$plot->legend->setPosition(1.3);
$plot->legend->shadow->setSize(3);

$plot->setLabelPosition(-40);
$plot->label->setPadding(2, 2, 2, 2);
//$plot->label->setFont(new Tuffy(7));
$plot->label->setBackgroundColor(new White(60));

$graph->add($plot);
$graph->draw();

