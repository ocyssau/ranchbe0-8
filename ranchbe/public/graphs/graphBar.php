<?php
/*
 * This work is hereby released into the Public Domain.
 * To view a copy of the public domain dedication,
 * visit http://creativecommons.org/licenses/publicdomain/ or send a letter to
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.
 *
 */


if(!isset($_REQUEST['values'])) exit;

// Get the data from form and unzeralize it
$values = @unserialize(urldecode(stripslashes($_REQUEST['values'])));
$legend = @unserialize(urldecode(stripslashes($_REQUEST['legend'])));
$title = $_REQUEST['title'];

// Check data
if(!is_array($values) || !is_array($legend)) exit;
if(count($values) == 0) exit;

// Draw graph
require_once '../../external/Artichow/BarPlot.class.php';

$graph = new Graph(400, 300); //width, height px
//$graph->setAntiAliasing(true);

$graph->title->set($title); //Title definition
//$graph->title->setFont(new Font3(26)); //Font of title
//$graph->title->border->show(); //Border around title
//$graph->title->setBackgroundColor(new Color(255, 255, 255, 25)); //Background color back title
//$graph->title->setPadding(4, 4, 4, 4); //Margin of title
$graph->title->move(0,-5); //Position of title x,y

$plot = new BarPlot($values);
$plot->setSize(1, 0.9); //Change la largeur $width et la hauteur $height du composant. Les nouvelles valeurs doivent �tre comprises entre 0 et 1 et correspondent � une fraction des largeur et hauteur de l'image � laquelle le composant appartient.
$plot->setAbsPosition(0, 0); //Change la position du composant sur l'image. Contrairement � setCenter(), cette m�thode ne place pas le composant par rapport � son centre, mais par rapport � son coin haut-gauche. Les positions $left � gauche et $top pour la hauteur doivent �tre donn�es en pixels. Attention, la position 0 pour $top place le composant en haut de l'image.

$plot->xAxis->setLabelText($legend);
$plot->xAxis->label->setAngle(90);
//$plot->xAxis->title->set("Axe des X");
//$plot->xAxis->title->setFont(new TuffyBold(10));
//$plot->xAxis->setTitleAlignment(Label::RIGHT);

$plot->yAxis->title->set("Count");
$plot->yAxis->title->setFont(new TuffyBold(10));
$plot->yAxis->title->move(-4, 0);
$plot->yAxis->setTitleAlignment(Label::TOP);

$plot->setBarColor(new Color(250, 255, 0));
//$plot->setSpace(20, 20, 20, 40); //public setSpace(int $left := NULL, int $right := NULL, int $top := NULL, int $bottom := NULL)

/*
$plot->barShadow->setSize(3);
$plot->barShadow->setPosition(Shadow::RIGHT_TOP);
$plot->barShadow->setColor(new Color(180, 180, 180, 10));
$plot->barShadow->smooth(TRUE);
*/

$plot->setBackgroundGradient(
    new LinearGradient(
        new Color(230, 230, 230),
        new Color(255, 255, 255),
        0
    )
);

$plot->barBorder->setColor(new Color(0, 0, 150, 20));

$plot->setBarGradient(
    new LinearGradient(
        new Color(150, 150, 210, 0),
        new Color(230, 230, 255, 30),
        0
    )
);

$plot->setBarPadding(0.3, 0.3); //Width of bars
//$plot->setBarSpace(10);

$graph->add($plot);
$graph->draw();


