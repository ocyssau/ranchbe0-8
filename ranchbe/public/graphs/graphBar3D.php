<?php
/*
 * This work is hereby released into the Public Domain.
 * To view a copy of the public domain dedication,
 * visit http://creativecommons.org/licenses/publicdomain/ or send a letter to
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.
 *
 */

if(!isset($_REQUEST['values'])) exit;
// Get the data from form and unzeralize it
$values = @unserialize(urldecode(stripslashes($_REQUEST['values'])));
$legend = @unserialize(urldecode(stripslashes($_REQUEST['legend'])));
$title = $_REQUEST['title'];

// Check data
if(!is_array($values) || !is_array($legend)) exit;
if(count($values) == 0) exit;

// Draw graph

require_once '../../external/Artichow/BarPlot.class.php';

function color($a = NULL) {
    if($a === NULL) {
        $a = 0;
    }
    return new Color(mt_rand(20, 180), mt_rand(20, 180), mt_rand(20, 180), $a);
}

$graph = new Graph(300, 200);
$graph->setAntiAliasing(false);
$graph->border->hide();

$group = new PlotGroup;
$group->setSpace(5, 10, 20, 15);
$group->setPadding(40, 10, NULL, 20);
$group->setXAxisZero(FALSE);

$group->axis->left->setLabelPrecision(2);

$colors = array(
    new Color(100, 180, 154, 12),
    new Color(100, 154, 180, 12),
    new Color(154, 100, 180, 12),
    new Color(180, 100, 154, 12)
);

$n=0;
foreach($values as $value) {

    $plot = new BarPlot($value);
    $plot->barBorder->setColor(new Color(0, 0, 0));
    $plot->setBarSize(0.54);
    $plot->barShadow->setSize(3);
    $plot->barShadow->setPosition(Shadow::RIGHT_TOP);
    $plot->barShadow->setColor(new Color(160, 160, 160, 10));
    $plot->barShadow->smooth(true);
    $plot->setBarColor($colors[$n]);
    $n++;if($n >= 3) $n=0;

    $plot->xAxis->setLabelText($legend);
    //$plot->xAxis->title->set("Axe des X");
    //$plot->xAxis->title->setFont(new TuffyBold(10));
    //$plot->xAxis->setTitleAlignment(Label::RIGHT);
    
    $plot->yAxis->title->set("Count");
    $plot->yAxis->title->setFont(new TuffyBold(10));
    $plot->yAxis->title->move(-4, 0);
    $plot->yAxis->setTitleAlignment(Label::TOP);
    
    $plot->setBarColor(new Color(250, 255, 0));
    $plot->setSpace(5, 5, NULL, NULL);
    
    $plot->setBackgroundGradient(
        new LinearGradient(
            new Color(230, 230, 230),
            new Color(255, 255, 255),
            0
        )
    );
    
    $plot->barBorder->setColor(new Color(0, 0, 150, 20));
    
    $plot->setBarGradient(
        new LinearGradient(
            new Color(150, 150, 210, 0),
            new Color(230, 230, 255, 30),
            0
        )
    );
    
    $plot->setBarPadding(0.3, 0.3); //Width of bars

    $group->add($plot);
    $group->legend->add($plot, "Barre #".$n, Legend::BACKGROUND);
}

$group->legend->shadow->setSize(0);
$group->legend->setAlign(Legend::CENTER);
$group->legend->setSpace(6);
$group->legend->setTextFont(new Tuffy(8));
$group->legend->setPosition(0.50, 0.12);
$group->legend->setBackgroundColor(new Color(255, 255, 255, 25));
$group->legend->setColumns(2);

$graph->add($group);
$graph->draw();


