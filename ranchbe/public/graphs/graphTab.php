<?php
/*
 * This work is hereby released into the Public Domain.
 * To view a copy of the public domain dedication,
 * visit http://creativecommons.org/licenses/publicdomain/ or send a letter to
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.
 *
 */


function graphTab($table, $title){
/*
*$table is a array like this :
*table_
*      |lign1
*           |col1
*                 |valeur
*           |col2
*                 | valeur
*       |lign2
*            |...
*table_
*      |col1
*           |lign1
*                 |valeur
*           |lign2
*                 | valeur
*       |col2
*            |...
*/


  // Check data
  //if(!is_array($values) || !is_array($legend)) exit;
  //if(count($values) == 0) exit;
  
  //Get header
  $htmlCols = '<th></th>';
  foreach($table as $colname=>$row){
    $htmlCols .= '<th>'.$colname.'</th>';
    foreach($row as $rowTitle=>$val){
      $rows[$rowTitle]=$rowTitle;
    }
  }

  $i=0;
  foreach($table as $colname=>$row){
    $htmlRowBody.='<tr>';
    foreach($rows as $rowTitle){
      //Get fisrt col titles
      if($i===0)$htmlRowBody .= '<th>'.$rowTitle.'</th>';
      //Get body
      $htmlRowBody .= '<td>'.$table[$colname][$rowTitle].'</td>';
      $i++;
    }
    $htmlRowBody.='</tr>';
  }

  $html= '<table border=1>';
  $html.= '<tr>'.$htmlCols.'</tr>';
  $html.= $htmlRowBody;
  $html.='</table>';
  
  return $html;

} //End of function


