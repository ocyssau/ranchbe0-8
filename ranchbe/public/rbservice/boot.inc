<?php
/*
session_name('PSESSION');
if ( $_COOKIE['PSESSION'] ) {
	session_id($_COOKIE['PSESSION']);
}
*/


// Define path to application directory
if( getenv('APPLICATION_PATH') ){
	define('APPLICATION_PATH', (getenv('APPLICATION_PATH') ) );
}else{
	define('APPLICATION_PATH',
		str_replace ( '\\', '/', realpath ( dirname ( __FILE__ ) . '/../application' ) )
	);
}

include( APPLICATION_PATH . '/configs/Boot.php');

$serviceClassFile = str_replace('_', '/', $serviceClass) . '.php';
require_once ($serviceClassFile);

ini_set("soap.wsdl_cache_enabled", "0");
ini_set("soap.wsdl_cache_ttl", 0);

if(isset($_GET['wsdl'])){
	require_once('Zend/Soap/AutoDiscover.php');
	$wsdl = new Zend_Soap_AutoDiscover();
	$wsdl->setClass($serviceClass);
	header('Content-type: text/xml; charset=UTF-8');
	/* Bug on wsdl length. 
	* See http://bugs.php.net/bug.php?id=49226
	* Probably not useful for the latest php version
	*/
	/* Bug on wsdl length. 
	 * See http://bugs.php.net/bug.php?id=49226
	$url = ROOT_URL . "rbservice/$serviceName/?wsdl";
	$data = file_get_contents($url);
	file_put_contents("wsdl.xml", $data);
	$server = new SoapServer("wsdl.xml"); // works
	 */
	header('Content-Length: '.strlen( $wsdl->toXml() ));
	$wsdl->handle();
}else{
	//header('Content-type: text/xml; charset=UTF-8');
	/** Zend_Application */
	// Create application, bootstrap, and run
	$application = new Zend_Application ( APPLICATION_ENV, Ranchbe::getConfig() );
	$application->bootstrap('php');
	$application->bootstrap('adodb');
	$application->bootstrap('translate');
	$application->bootstrap('galaxia');
	$application->bootstrap('ranchbe');

    $options = array(
    				'trace' => true,
					'soap_version' => SOAP_1_2,
					'encoding' => 'UTF-8');
    
    /*
    $options = array(
				    'uri' => ROOT_URL . "rbservice/$serviceName/?wsdl",
    				'trace' => true,
					'soap_version' => SOAP_1_2,
					'encoding' => 'UTF-8'    
    );
	*/
    
    //$server = new Zend_Soap_Server( ROOT_URL . "rbservice/$serviceName/?wsdl" , $options );
	$server = new SoapServer(ROOT_URL . "rbservice/$serviceName/?wsdl", $options);
	//$server = new SoapServer(NULL, $options);
	$server->setClass($serviceClass);
}

