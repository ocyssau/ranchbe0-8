<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_WARNING);
ini_set ( 'display_errors', 0 );
ini_set ( 'display_startup_errors', 0 );

$serviceName = 'reposit';
$serviceClass = 'RbService_Reposit';

require_once 'Zend/Loader/Autoloader.php';
$loader = Zend_Loader_Autoloader::getInstance();
$loader->registerNamespace('Rb_');
$loader->registerNamespace('Galaxia_');
$loader->registerNamespace('ZendRb_');
$loader->setFallbackAutoloader(true);

include('../boot.inc');

if(!isset($_GET['wsdl'])){
	session_name('PSESSION');
	if ( $_COOKIE['PSESSION'] ) {
		session_id($_COOKIE['PSESSION']);
	}
	new Zend_Session_Namespace('SOAP');
	$server->setPersistence( SOAP_PERSISTENCE_SESSION );
	
	try {
		$server->handle();
	}catch(Exception $e){
		$server->fault(RBS_SERVER, $e->getMessage());
	}
}
