<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_WARNING);
ini_set ( 'display_errors', 0 );
ini_set ( 'display_startup_errors', 0 );

$serviceName = 'container/list.php';
$serviceClass = 'RbService_Container_List';

include('../boot.inc');

if(!isset($_GET['wsdl'])){
	try {
		$server->handle();
	}catch(Exception $e){
		throw new SoapFault( RBS_SERVER, $e->getTraceAsString() );
	}
}


