<?php

// Define path to application directory
if( getenv('APPLICATION_PATH') ){
	define('APPLICATION_PATH', (getenv('APPLICATION_PATH') ) );
}else{
	define('APPLICATION_PATH',
	str_replace ( '\\', '/', realpath ( dirname ( __FILE__ ) . '/../application' ) )
	);
}

include( APPLICATION_PATH . '/configs/Boot.php');

require_once ('Zend/Session.php');
$session = new Zend_Session_Namespace('rbsession');
if($session->config && APPLICATION_ENV == 'production'){
	$config = $session->config;
}else{
	$config = new Zend_Config_Ini ( APPLICATION_PATH . '/configs/rb.ini',
								    APPLICATION_ENV , true);
	$session->config = $config;
}
Ranchbe::setConfig ($config);
unset($config);


/** Zend_Application 
 *  Create application, bootstrap, and run
 */
$application = new Zend_Application ( APPLICATION_ENV, Ranchbe::getConfig() );


//$front = Zend_Controller_Front::getInstance();
//echo get_include_path();
//echo '<pre>';var_dump($application->getOptions());die;
//echo ini_get('display_errors');echo '--';echo ini_get('error_reporting');die;
//echo get_include_path();
//die;

$application->bootstrap()
            ->run();
