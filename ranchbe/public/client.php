<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_WARNING);
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );

defined ( 'APPLICATION_PATH' )
	|| define ( 'APPLICATION_PATH', 
				str_replace ( '\\', '/', realpath ( dirname ( __FILE__ ) . '/../application' ) ) );

if( getenv('BASE_URL') ){
	define('BASE_URL', '/'.getenv('BASE_URL').'/' );
}
define ( 'ROOT_URL', 'http://'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . BASE_URL );

set_include_path ( implode ( PATH_SEPARATOR, 
					array (realpath ( APPLICATION_PATH . '/../library' ),
						   realpath ( APPLICATION_PATH . '/../external' ), 
						   realpath ( APPLICATION_PATH ), 
						   get_include_path () ) ) );

						   
						   
/* Class for transmit user name and password to soap server
 * 
 */
class UserPass
{
    public $Username;
    public $Password;
	
    function __construct($Username, $Password)
    {
        $this->Username = $Username;
        $this->Password = $Password;
    }
}



//Create a soap var from UserPass class
$soapAuthenticator = new SoapVar(
							new UserPass("user", "user00"),
							SOAP_ENC_OBJECT,
							"UserPass");
$authHeader = new SoapHeader("http://schemas.xmlsoap.org/ws/2002/07/utility","headerAuthentify",$soapAuthenticator,false);

try {
	ini_set("soap.wsdl_cache_enabled", 0);
	ini_set("soap.wsdl_cache_ttl", 0);
	
    $options = array(
    				'trace' => true,
					'soap_version' => SOAP_1_2,
    				'cache_wsdl' => WSDL_CACHE_NONE,
					'encoding' => 'UTF-8');
    
	$url = ROOT_URL . 'rbservice/test/?wsdl';
	
	/* Bug on wsdl length. 
	 * See http://bugs.php.net/bug.php?id=49226
	$data = file_get_contents($url);
	file_put_contents("wsdl.xml", $data);
	$client = new SoapClient("wsdl.xml"); // works	
	 */
	
    $client = new SoapClient($url, $options);
	$client->__setSoapHeaders( $authHeader );
	$ret = $client->getTest();
	var_dump($ret);
	
	
	$url = ROOT_URL . 'rbservice/document/?wsdl';
    $client = new SoapClient($url, $options);
	$client->__setSoapHeaders( $authHeader );
	
	$ret = $client->getServerInfo();
	var_dump($ret);
	
	$ret = $client->getCurrentUser();
	var_dump($ret);

	$ret = $client->get(102);
	var_dump($ret);
	
}
catch(SoapFault $e){
  	print $e;
}


