<?php

function removeDir( $path, $recursive = false, $verbose = false ){
	// Add trailing slash to $path if one is not there
	if (substr($path, -1, 1) != "/"){
		$path .= "/";
	}
	if($curdir = opendir($path)) {
		while($file = readdir($curdir)) {
			if($file != '.' && $file != '..') {
				$file = $path . $file;
				if (is_file($file) === true){
					if (!unlink($file)){echo "failed to removed File: " . $file . "\n\r"; return false;
					}else {if($verbose) echo "Removed File: " . $file . "\n\r";}
				}
				else if (is_dir($file) === true && $recursive === true){
					// If this Directory contains a Subdirectory and if recursivity is requiered, run this Function on it
					if (!removeDir($file, $recursive, $verbose)){
						if($verbose) echo "failed to removed File: " . $file . "\n\r";
						return false;
					}
				}
				
			}
		}
		closedir($curdir);
	}

	// Remove Directory once Files have been removed (If Exists)
	if(is_dir($path)){
		if(@rmdir($path)){
			if($verbose) echo "Removed Directory: " . $path . "\n\r";
			return true;
		}
	}
	return false;
}//End of function


function dircopy($srcdir, $dstdir, $verbose = false, $initial=true, $recursive=true, $mode=0755){
	if(!is_dir($dstdir)) mkdir($dstdir, $mode, true);
	if($curdir = opendir($srcdir)) {
		while($file = readdir($curdir)) {
			if($file == '.' || $file == '..' 
				|| $file == 'jsdev' || $file == 'experimental'
				) continue;
			
			$srcfile = "$srcdir" . '/' . "$file";
			$dstfile = "$dstdir" . '/' . "$file";
			if(is_file("$srcfile")) {
				if($verbose) echo "Copying '$srcfile' to '$dstfile'...";
				if(copy("$srcfile", "$dstfile")) {
					touch("$dstfile", filemtime("$srcfile"));
					if($verbose) echo "OK\n";
				}
				else {print "Error: File '$srcfile' could not be copied!\n"; return false;}
			}
			else{
				if(is_dir("$srcfile") && $recursive){
					dircopy("$srcfile", "$dstfile", $verbose, false);
				}
			}
		}
		closedir($curdir);
	}
	return true;
}//End of function


function cleansvn($dir, $verbose = true){
	if(!is_dir($dir)) return false;
	if($curdir = opendir($dir)) {
		while($file = readdir($curdir)) {
			if($file == '.' || $file == '..') continue;
			if($file == '.svn' || $file == 'Thumbs.db') {
				$file = $dir . '/' . $file;
				if(is_dir("$file")) {
					if( removeDir($file, true, true) ){
						echo "Delete directory $file \n\r";
					}else {
						print "Error: $file cant be suppressed \n";
					}
				}
			}else{
				$file = $dir . '/' . $file;
				if( is_dir($file) ) {
					//echo "$file \n\r";
					cleansvn($file, $verbose);
				}
			}
		}
		closedir($curdir);
	}
	return true;
}//End of function


function emptyDir( $dir , $verbose = true){
	if($curdir = opendir( $dir ))
	while($file = readdir($curdir)) {
		if($file == '.' || $file == '..' || $file == 'readme' ) continue;
		$file = $dir . '/' . $file;
		if (is_file($file) === true){
			if (!unlink($file)){echo "failed to removed File: " . $file . "\n\r"; return false;
			}else {if($verbose) echo "Removed File: " . $file . "\n\r";}
		}
		if (is_dir($file) === true){
			if (!removeDir( $file, true, true )){echo "failed to removed directory: " . $file . "\n\r"; return false;
			}else {if($verbose) echo "Removed directory: " . $file . "\n\r";}
		}
	}
	closedir($curdir);
	return true;
}

/**
 * @todo : add original directory structure
 * 
 * @param String $dir
 * @param String $zipfile
 * @param Boolean $recursive
 * @param Boolean $verbose
 */
function zipDir( $dir , ZipArchive $zip, $recursive=true, $verbose = true){
	if($curdir = opendir( $dir ))
	while($localfile = readdir($curdir)) {
		if($localfile == '.' || $localfile == '..' ) continue;
		if($dir == './' || $dir == '.' ) $file = $localfile;
		else $file = $file = $dir . '/' . $localfile;
		if (is_file($file) === true){
			$zip->addFile($file, $file);
			if($verbose) echo "Add File: $file to zip file \n\r";
		}
		if (is_dir($file) === true){
			zipDir( $file , $zip, $recursive, $verbose);
		}
	}
	closedir($curdir);
	return true;
}

/**
 * 
 * @param string $tmpdir	path to build temporary directory. Its where the result will be
 * @param array $include	array of string, paths to library to include in external
 * @param boolean $verbose		if true display all messages
 */
function build($tmpdir, array $include, $verbose = false){
	//Create build directory
	$sourceDir = realpath ( dirname ( __FILE__ ) );
	require_once($sourceDir . '/library/Rb/Conf/Ranchbe.php');
	
	$version = Ranchbe::getVersion(); //array
	$build = time();
	$buildName = 'ranchbe_' . $version['ranchbe_ver'] . '_build' . $build;
	$buildDir = $tmpdir . '/' . $buildName;
	
	//var_dump($sourceDir, $buildDir, $sourceDir );die;

	//Copier les donn�es
	mkdir($buildDir, 0755, true);
	
	if(!dircopy($sourceDir, $buildDir, $verbose)) die("ne peut pas copier $sourceDir vers $buildDir");
	
	//Copier les librairy
	mkdir($buildDir . '/external', 0755, true);
	foreach($include as $libPath){
		$libname = basename($libPath);
		$buildLibPath = $buildDir.'/external/'.$libname;
		if(!dircopy($libPath, $buildLibPath, $verbose)) die("ne peut pas copier $libPath vers $buildLibPath");
	}
	
	//clean svn
	cleansvn($buildDir);

	//nettoyage
	emptyDir( $buildDir . '/application/views/templates_c' , $verbose);

	emptyDir( $buildDir . '/application/views/templates/processes' , $verbose);

	emptyDir( $buildDir . '/public/img/thumbnails/bookshop' );
	emptyDir( $buildDir . '/public/img/thumbnails/cadlib' );
	emptyDir( $buildDir . '/public/img/thumbnails/mockup' );
	emptyDir( $buildDir . '/public/img/thumbnails/workitem' );

	emptyDir( $buildDir . '/public/img/filetypes32/C' );


	if( is_dir($buildDir . '/public/jsdev') )
	removeDir( $buildDir . '/public/jsdev', true, $verbose );
	
	if( is_file($buildDir . '/public/.htaccess') )
		unlink( $buildDir . '/public/.htaccess' );

	if( is_dir($buildDir . '/experimental') )
	removeDir( $buildDir . '/experimental', true, $verbose );

	if( is_dir($buildDir . '/src') )
	removeDir( $buildDir . '/src', true, $verbose );

	if( is_file($buildDir . '/application/configs/rb.ini') )
	unlink( $buildDir . '/application/configs/rb.ini' );

	if( is_file($buildDir . '/build.bat') )
	unlink( $buildDir . '/build.bat' );

	if( is_file($buildDir . '/build.php') )
	unlink( $buildDir . '/build.php' );
	
	if( is_dir($buildDir . '/.settings') )
	removeDir( $buildDir . '/.settings', true, $verbose );

	if( is_file($buildDir . '/.buildpath') )
	unlink( $buildDir . '/.buildpath' );
	
	if( is_file($buildDir . '/.project') )
	unlink( $buildDir . '/.project' );
	
	if( is_file($buildDir . '/.zfproject.xml') )
	unlink( $buildDir . '/.zfproject.xml' );
	
	if( is_file($buildDir . '/build.log') )
	unlink( $buildDir . '/build.log' );
	
	//copy install doc to install directory
	if( is_file($buildDir . '/docs/DocRanchBE_install.pdf') )
	copy($buildDir . '/docs/DocRanchBE_install.pdf', $buildDir . '/public/install/install.pdf');
	
	
	//Copy install auto doc
	if( is_file($buildDir . '/docs/DocRanchBE_install_auto.pdf') )
	copy($buildDir . '/docs/DocRanchBE_install_auto.pdf', $buildDir . '/public/install/install_auto.pdf');
	
	//Copy db scripts
	if( is_file($buildDir . '/docs/db/mysql/ranchbe_en.sql') )
	copy($buildDir . '/docs/db/mysql/ranchbe_en.sql', $buildDir . '/public/install/source/ranchbe_en.sql');
	if( is_file($buildDir . '/docs/db/mysql/ranchbe_fr.sql') )
	copy($buildDir . '/docs/db/mysql/ranchbe_fr.sql', $buildDir . '/public/install/source/ranchbe_fr.sql');
	
	//Set the build number
	$class_ranchbe_file = $buildDir . '/library/Rb/Conf/Ranchbe.php';
	$class_ranchbe_content = file_get_contents( $class_ranchbe_file );
	$class_ranchbe_content = str_replace('%RANCHBE_BUILD%', $build, $class_ranchbe_content);
	file_put_contents($class_ranchbe_file, $class_ranchbe_content);
	
	//remove public
	chdir( $buildDir );
	
	//Generate zip for application
	$zipapp = new ZipArchive();
	$app_zipfile = $tmpdir . '/application.zip';
	if($zipapp->open($app_zipfile, ZIPARCHIVE::CREATE) !== true ){
		die("Impossible de cr�er le zip $zipfile");
	}
	chdir( $buildDir );
	zipDir( 'application' , $zipapp, true, $verbose);
	zipDir( 'docs' , $zipapp, true, $verbose);
	zipDir( 'library' , $zipapp, true, $verbose);
	zipDir( 'external' , $zipapp, true, $verbose);
	zipDir( 'userdatas' , $zipapp, true, $verbose);
	zipDir( 'tests' , $zipapp, true, $verbose);
	$zipapp->close();
	
	//Generate zip file for rbdata
	$rbdatazip = new ZipArchive();
	$rbdata_zipfile = $tmpdir . '/rbdata.zip';
	if($rbdatazip->open($rbdata_zipfile, ZIPARCHIVE::CREATE) !== true ){
		die("Impossible de cr�er le zip $zipfile");
	}
	chdir($buildDir . '/userdatas');
	zipDir( '.' , $rbdatazip, true, $verbose );
	$rbdatazip->close();
	
	
	//Generate zip file for public
	$publiczip = new ZipArchive();
	$zipfile = $tmpdir . '/' . $buildName . '.zip';
	if($publiczip->open($zipfile, ZIPARCHIVE::CREATE) !== true ){
		die("Impossible de cr�er le zip $zipfile");
	}
	chdir($buildDir);
	rename('public', 'ranchbe');
	
	zipDir( 'ranchbe' , $publiczip, true, $verbose );
	$publiczip->addFile($app_zipfile, 'ranchbe/install/application.zip');
	$publiczip->addFile($rbdata_zipfile, 'ranchbe/install/rbdata.zip');
	$publiczip->addFromString('ranchbe/install/version.ini', $version['ranchbe_ver'] . "\n" );
	$publiczip->addFromString('ranchbe/install/build.ini', $build . "\n" );
	$publiczip->close();
	
	//clean the temp
	chdir($tmpdir);
	removeDir( $buildDir, true, $verbose );
	unlink( $app_zipfile );
	unlink( $rbdata_zipfile );
	
}

$verbose = false;

$include[] = 'C:/usr/library/Adodb';
$include[] = 'C:/usr/library/Adodb5';
$include[] = 'C:/usr/library/Artichow';
$include[] = 'C:/usr/library/gzip';
$include[] = 'C:/usr/library/PEAR';
$include[] = 'C:/usr/library/Smarty';
$include[] = 'C:/usr/ZF/library/Zend';

build( 'c:/tmp', $include, $verbose);

