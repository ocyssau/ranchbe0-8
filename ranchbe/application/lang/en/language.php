<?php // -*- coding:iso8859-1
// parameters:
// lang=xx    : only tranlates language 'xx',
//              if not given all languages are translated
// comments   : generate all comments (equal to close&module)
// close      : look for similar strings that are already translated and
//              generate a comment if a 'match' is made
// module     : generate comments that describe in which .php and/or .tpl
//              module(s) a certain string was found (useful for checking
//              translations in context)
// patch      : looks for the file 'language.patch' in the same directory
//              as the corresponding language.php and overrides any strings
//              in language.php - good if a user does not agree with
//              some translations or if only changes are sent to the maintainer
// spelling   : generates a file 'spellcheck_me.txt' that contains the
//              words used in the translation. It is then easy to check this
//              file for spelling errors (corrections must be done in
//              'language.php, however)
// groupwrite : Sets the generated files permissions to allow the generated
//              language.php also be group writable. This is good for
//              translators if they do not have root access to tiki but
//              are in the same group as the webserver. Please remember
//              to have write access removed when translation is finished
//              for security reasons. (Run script again without this
//              parameter)
// Examples:
// http://www.neonchart.com/get_strings.php?lang=sv
// Will translate langauage 'sv' and (almost) avoiding comment generation

// http://www.neonchart.com/get_strings.php?lang=sv&comments
// Will translate langauage 'sv' and generate all possible comments.
// This is the most usefull mode when working on a translation.

// http://www.neonchart.com/get_strings.php?lang=sv&nohelp&nosections
// These options will only provide the minimal amout of comments.
// Usefull mode when preparing a translation for distribution.

// http://www.neonchart.com/get_strings.php?nohelp&nosections
// Prepare all languages for release 

$lang=Array(
//Divers

//History

//Projects

//Conteneurs manager

//documents manager

//metadataManager
"metadata_help1"=>"You can input here a regular expression to check input information",
"metadata_help2"=>"Max size of the field or number of line to display for select field",
"metadata_field_list_help1"=>"You must separate each word/sentence by '#' without space.",
"metadata_multiple_help"=>"Enable the multi selection with select field",
"metadata_date_format_help1"=>"Format of date. i.e: %Y/%m/%d display 2005/05/28 i.e",
"metadata_selectdb_help1"=>"You can indicate here a SQL where close to limit list of the select element<br />exemple: partner_type = 'customer' to limit the select to the customers partner type",

//Detail window
"documentDetailWindow_message1"=>"no embeded viewer associate to this file, try to select an other file",

//Documents files manager
"Documents files manager"=>"Gestionnaire de fichiers",
"Hide file manager"=>"Cacher le gestionnaire de fichier",

//File manager
"file_name"=>"File name",
"file_type"=>"File type",
"file_path"=>"File path",
"file_size"=>"File size",
"file_mtime"=>"Mtime",
"file_md5"=>"Md5",
"file_iteration"=>"Iteration",
"file_extension"=>"Extension",
"document_number"=>"Number",
"document_version"=>"Indice",
"document_iteration"=>"Version",

//Wildspace manager

//import manager
"package_file_name"=>'File name',
"package_file_extension"=>'Extension',
"import_errors_report"=>'Errors file',
"import_logfiles_file"=>'Log file',
"package_file_path"=>'File path',
"package_file_mtime"=>'Mtime',
"package_file_size"=>'File size',
"package_file_extension"=>'Extension',
"package_file_md5"=>'Md5',
"import_order"=>'Order',

//Filters
"searchBar_help_1"=>"You can use the word OR (with a space on each side) to define OR close. It is valid for all textual search fields",

//Mockups

//Cadlibs

//Bookshops

//Partner
"first_name"=>"Fisrt name",
"last_name"=>"Last name",
"adress"=>"Adresse",
"city"=>"City",
"zip_code"=>"Zip code",
"phone"=>"Phone",
"cell_phone"=>"Cell phone",
"mail"=>"Mail",
"web_site"=>"Website",
"Company" => "Company",
"company" => "company",

//Doctypes
"recognition_regexp"=>"Recognition regular expression",
"script_pre_store"=>"Pre store script",
"script_post_store"=>"Post store script",
"script_pre_update"=>"Pre update script",
"script_post_update"=>"Post update script",
"icon"=>"Icon",
"can_be_composite"=>"Multi-file",

//Tools

//Galaxia

//user prefs
"default" => "Default",
"long_date_format" => "Long date format",
"short_date_format" => "Short date format",
"css_sheet"  => "Style sheet",
"max_record" => "Max record to display",
"lang" => "Language",
"time_zone" => "Time zone",
"charset" => "Charset",
"hour_format" => "Hour format",
"wildspace_path" => "Wildspace path",

//Smarty modifiers

//Permissions
'container_document_get'=> 'List documents',
'container_document_manage'=> 'Create/modify documents',
'container_document_suppress'=> 'Suppress documents',
'container_document_move' => 'Move document between container',
'container_document_copy' => 'Copy document',
'container_document_assoc'=> 'Associate manually a file to a document',
'container_document_unlock'=> 'Unlock documents',
'container_document_archive'=> 'Manage documents archive',
'container_document_change_indice'=> 'Change indice of a documents',
'container_document_change_state'=> 'Change state of a documents',
'container_document_change_number'=> 'Change number of a documents',
'container_document_link_file'=> 'Manage files linked to documents',
'container_document_history'=> 'Manage documents history',
'container_document_iteration'=> 'Manage documents versions',
'container_document_doctype_reset' => 'Recalculate the doctype',

//lands
"German" => "Allemand",
"English" => "Anglais",
"French" => "Fran�ais",
"###end###"=>"###end###");
?>
