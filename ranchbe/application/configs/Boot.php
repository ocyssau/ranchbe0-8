<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );

// Define application environment
define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

if( getenv('BASE_URL') ){
	define('BASE_URL', '/'.getenv('BASE_URL').'/' );
}

// Ensure library/ is on include_path
set_include_path ( implode ( PATH_SEPARATOR,
					array (	realpath ( APPLICATION_PATH . '/../library' ),
							realpath ( APPLICATION_PATH . '/../external' ),
							get_include_path () ) 
					) 
				);

require_once ('Zend/Config/Ini.php');
require_once ('Zend/Application.php');
require_once ('Rb/Conf/Ranchbe.php');

//define ( 'BASE_URL', '/ranchbe/' );
//define ( 'ROOT_URL', 'http://192.9.200.100/ranchbe/' );
//define ( 'ROOT_URL', 'http://localhost/ranchbe/' );

$Schema = "http";
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
	$Schema = 'https';
}

$ServerPort = (getenv('MY_SERVER_PORT') ? getenv('MY_SERVER_PORT') : $_SERVER['SERVER_PORT']);
$ServerHost = (getenv('MY_SERVER_HOST') ? getenv('MY_SERVER_HOST') : $_SERVER['SERVER_NAME']);
$Schema = (getenv('MY_SCHEMA') ? getenv('MY_SCHEMA') : $Schema);
//var_dump(getenv('MY_SERVER_PORT'), getenv('MY_SERVER_HOST'), getenv('MY_SCHEMA'));die;

define ( 'ROOT_URL', $Schema.'://'.$ServerHost . ':' . $ServerPort . BASE_URL );
//define ( 'ROOT_URL', 'http://'.$_SERVER['SERVER_NAME'] . BASE_URL );
//http_referer is not send by IE???!!!! correct it! with a bad value??!!
$_SERVER['HTTP_REFERER'] = ROOT_URL;
//var_dump($_SERVER);die;
//var_dump(ROOT_URL);die;

/*
require_once ('Zend/Session.php');
$session = new Zend_Session_Namespace('rbsession');
$session->config = null;
if($session->config && APPLICATION_ENV == 'production'){
	$config = $session->config;
}else{
	$config = new Zend_Config_Ini ( APPLICATION_PATH . '/configs/rb.ini',
								APPLICATION_ENV , true);
	$session->config = $config;
}
Ranchbe::setConfig ($config);
unset($config);
*/

Ranchbe::setConfig ( new Zend_Config_Ini ( APPLICATION_PATH . '/configs/rb.ini',
					 APPLICATION_ENV , true) );

