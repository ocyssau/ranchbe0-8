<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


class Tools_IndexController extends Zend_Controller_Action {
	protected $smarty;
	protected $page_id = 'tools'; //(string)
	

	//-----------------------------------------------------------------
	public function init() {
		$this->smarty = & Ranchbe::getSmarty ();
		if (! Ranchbe::checkPerm ( 'admin', Ranchbe::getAcl ()->getRootResource (), false )) {
			RbView_Menu::get ()->init ()->getAdmin ();
			RbView_Tab::get ( 'toolsTab' )->activate ();
			Ranchbe::getSmarty ()->display ( 'ranchbe.tpl' );
			Ranchbe::getError()->checkErrors ();
			die ();
		}
	} //End of method
	

	//-----------------------------------------------------------------
	public function indexAction() {
		// Display the template
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'toolsTab' )->activate ();
	} //End of method
	

	//-----------------------------------------------------------------
	//Profiling
	public function sqlprofilingAction() {
		ob_end_flush ();
		//$status = ob_get_status  ( true );
		//var_dump($status);die;
		$perf = NewPerfMonitor ( Ranchbe::getDb () );
		$perf->UI ( $pollsecs = 5 );
		echo '<a href="./"><b>QUIT AND RETURN TO TOOLS PAGE</b></a>';
		die ();
	} //End of method
	

	//-----------------------------------------------------------------
	//PHP infos
	public function phpinfoAction() {
		phpinfo ();
		die ();
	} //End of method
	

	//-----------------------------------------------------------------
	//Backup of Database
	public function backupAction() {
		$host_ranchbe = Ranchbe::getDb ()->host;
		$user_ranchbe = Ranchbe::getDb ()->user;
		$pass_ranchbe = Ranchbe::getDb ()->password;
		$dbs_ranchbe = Ranchbe::getDb ()->database;
		$date = date ( "Y-m-d_H-i-s" );
		$type = $this->getRequest ()->getParam ( 'type' );
		$dumpcmd = Ranchbe::getConfig()->cmd->mysqldump;

		// Utilise les fonctions syst�me : MySQLdump
		if ($type == 'XML') {
			$backup_file = $dbs_ranchbe . '_backup_' . $date . '.xml';
			$command = $dumpcmd . " --xml -h$host_ranchbe -u$user_ranchbe --password=$pass_ranchbe $dbs_ranchbe";
		} else {
			$backup_file = $dbs_ranchbe . '_backup_' . $date . '.sql';
			$command = $dumpcmd . " --opt -h$host_ranchbe -u$user_ranchbe --password=$pass_ranchbe $dbs_ranchbe";
		}
		
		$backup = shell_exec ( $command );

		header ( "Content-disposition: attachment; filename=$backup_file" );
		header ( "Content-Type: text/rtf" );
		header ( "Content-Transfer-Encoding: $backup_file\n" ); // Surtout ne pas enlever le \n
		header ( "Content-Length: " . strlen ( $backup ) );
		header ( "Pragma: no-cache" );
		header ( "Cache-Control: must-revalidate, post-check=0, pre-check=0, public" );
		header ( "Expires: 0" );
		echo $backup;
		die ();
	} //End of method
	

	//-----------------------------------------------------------------
	//Get files without documents
	public function fileswithoutdocumentAction() {
	} //End of method
	

	//-----------------------------------------------------------------
	//Get files record without real file
	public function recordfilewithoutfsdataAction() {
	} //End of method
	

	//-----------------------------------------------------------------
	//Get documents without files
	public function documentwithoutfileAction() {
	} //End of method
	

	//-----------------------------------------------------------------
	//Get documents without container
	public function documentwithoutcontainerAction() {
	} //End of method
	

	//-----------------------------------------------------------------
	//Get documents without container
	public function gettrashAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$dir = new Rb_Directory ( );
		$result = $dir->listDir ( DEFAULT_TRASH_DIR, false );
		if (is_array ( $result ))
			foreach ( $result as $key => $file ) {
				//Get the original directory
				$oriPath = Rb_Filesystem::decodePath ( $file ['file_name'] );
				$oriPath = str_replace ( '%&_', '/', "$oriPath" );
				$oriPath = ereg_replace ( "\([0-9]{1,2}\).", ".", $oriPath );
				$result [$key] ['original_file'] = $oriPath;
			}
		$this->view->list = $result;
	} //End of method
	

	//-----------------------------------------------------------------
	//Update doctypes icons
	public function updatedoctypeiconAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$p ['select'] = array ('doctype_id', 'icon', 'doctype_number' );
		$result = Rb_Doctype::get ()->getAll ( $p );
		$success = array ();
		$failed = array ();
		if (is_array ( $result ))
			foreach ( $result as $doctype ) {
				$odoctype = & Rb_Doctype::get ( $doctype ['doctype_id'] );
				if ($odoctype->compileIcon ( $doctype ['icon'], $doctype ['doctype_id'] ))
					$success [] = $doctype;
				else
					$failed [] = $doctype;
			}
		$this->view->success = $success;
		$this->view->failed = $failed;
	} //End of method
	

	//-----------------------------------------------------------------
	//To generate doctypes table
	public function gendoctypesAction() {
		echo 'INSERT INTO `doctypes` (`doctype_id`, `doctype_number`, `doctype_description`, `can_be_composite`, `script_post_store`, `script_pre_store`, `script_post_update`, `script_pre_update`, `recognition_regexp`, `file_extension`, `file_type`, `icon`) VALUES ' . '<br>';
		$file = APPLICATION_PATH . '/Docs/doctypes.csv';
		$handle = fopen ( $file, 'r' );
		if (! $handle)
			return false;
		$linecount = count ( file ( $file ) );
		$row = 1;
		while ( ($data = fgetcsv ( $handle, 1000, ';' )) !== FALSE ) {
			$doctype_number = $data [0];
			$doctype_description = $data [1];
			$file_extension = $data [2];
			$file_type = $data [3];
			$recognition_regexp = $data [4];
			$icon = $data [5];
			$file_extension_list [] = $file_extension;
			echo "($row, '$doctype_number', '$doctype_description','0','','','','','$recognition_regexp', '$file_extension', '$file_type', '$icon')";
			if ($linecount !== $row)
				echo ",<br>";
			else
				echo ";<br>";
			$row ++;
		}
		fclose ( $handle );
		
		$rows = $row - 1;
		print "
          DROP TABLE `doctypes_seq`; <br>
          CREATE TABLE `doctypes_seq` (
            `sequence` int(11) NOT NULL auto_increment,
            PRIMARY KEY  (`sequence`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=$row ;
          <br>
          INSERT INTO `doctypes_seq` (`sequence`) VALUES 
          ($rows);
          ";
		
		echo '<br />';
		foreach ( $file_extension_list as $extension ) {
			echo '$valid_file_ext["' . $extension . '"] = "' . $extension . '";';
			echo '<br>';
		}
	} //End of method
	

	//-----------------------------------------------------------------
	//
	public function updatethumbnailsAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		//get documents of workitem
		$this->view->results = Rb_Document::get ( 'workitem', - 1 )
			->getDocuments ( array ('select' => array ('document_id', 'document_number' ) ) );
		foreach ( $this->view->results as $dp ) {
			$thumbnail = new Rb_Document_Thumbnail ( Rb_Document::get ( 'workitem', $dp ['document_id'] ) );
			$thumbnail->generate ();
		}
	} //End of method

	
	//---------------------------------------------------------------------
	/** Display and save config
	 * 
	 */
	public function configAction() {
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'toolsTab' )->activate ();
		Ranchbe::getError()->checkErrors ();
	} 	//End of method
	
	
	//---------------------------------------------------------------------
	/** Display and save config
	 * 
	 */
	public function editconfigAction() {
		
		//config element to edit
		$indice_file = $this->getRequest()->getParam('indice_file');
		
		//from config section
		$rbIniSection = $this->getRequest()->getParam('mainSection');
		if(!$rbIniSection){
			$rbIniSection = 'production';
		}
		
		if( $this->getRequest()->getParam('cancel') ){
			return $this->_forward('config', 'index', 'tools', array() );
		}
		
		$rbConfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/rb.ini', 
						$rbIniSection, array( 'allowModifications' => true)
						);
						
		$formIniFile = basename($indice_file) . 'form.ini';
		$formIniFile = APPLICATION_PATH . '/Form/tools/index/editconfig/'. $formIniFile;
		if(!is_file($formIniFile)){
			Ranchbe::getError()->error(tra('file %file% dont exist'), array('file'=>$formIniFile));
			Ranchbe::getError()->checkErrors ();
			return false;
		}
		
		$this->view->form = new ZendRb_Form('masterForm');
		$this->view->form->setMethod('post');
		$this->view->form->setAction('tools/index/editconfig');
		
		$mainSubform = new ZendRb_SubForm('mainSubform');
		$mainSubform->setMethod('post');
		$mainSubform->setAction('tools/index/editconfig');
        $mainSubform->setDecorators(array(
            'FormElements',
        	array('TabContainer', array(
                'id' => 'tabContainer',
                'style' => 'width: 700px; height: 400px;',
                'dijitParams' => array(
                    'tabPosition' => 'top'
                ),
            )),
            'DijitForm',
        ));
		
		$formConfig = new Zend_Config_Ini($formIniFile);
		
		//Create subforms
		foreach($formConfig as $formName=>$subformConfig){
			//Create the subform
			$subform  = new ZendRb_SubForm( $subformConfig  );
			//Get subform elements
			foreach( $subformConfig->elements as $elementName=>$element){
				//Extract path from element name
				$rbConfigPath = self::_parsePath($elementName);
				//Get element default value
				$myRbConfigParameter = $rbConfig;
				foreach($rbConfigPath as $rbcp_t){
					$myRbConfigParameter = $myRbConfigParameter->$rbcp_t;
				}
				$subform->setDefault($elementName , $myRbConfigParameter);
			}
	        $subform->setAttribs(array(
	            'name'   => $formName,
	            'legend' => $formName,
	        ));
			$mainSubform->addSubForm($subform, $formName);
		}
		
		
		//Add common elements
		$this->view->form->addSubForm($mainSubform, 'mainSubform');
		$this->view->form->addCancel();
		$this->view->form->getElement('save')->setDecorators(
			array(
				'DijitElement',
				array('HtmlTag', array('tag' => 'div', 'openOnly' => true))
				)
		);
		
		$this->view->form->getElement('cancel')->setDecorators(
			array(
				'DijitElement',
				array('HtmlTag', array('tag' => 'div', 'closeOnly' => true))
				)
		);
		
		$this->view->form->addElement('hidden', 'indice_file', array('value'=>$indice_file));
		$this->view->form->addElement('hidden', 'mainSection', array('value'=>$rbIniSection));
		
		
		//Submit and save forms
		if ( $this->getRequest()->isPost() ) {
			if ( $this->view->form->isValid( $this->getRequest()->getPost() ) ) {
				//populate the config object from submit value
				foreach($mainSubform->getSubforms() as $subform){
					$values = $subform->getValues();
					$values = $values[ $subform->getName() ];
					foreach($values as $elementName=>$value){
						$rbConfigPath = self::_parsePath($elementName);
						$myRbConfig =& $rbConfig;
						foreach($rbConfigPath as $param){
							if(is_object($myRbConfig->$param)){
								$myRbConfig =& $myRbConfig->$param;
							}else{
								$myRbConfig->$param = trim((string) $value);
							}
						}
					}
				}
				if( $this->_saveConfig($rbConfig, $rbIniSection) ){
					$this->view->successMessage = tra('Save is successfull');
				}else{
					$this->view->failedMessage = tra('Save has failed');
				}
			} else {
				$this->view->failedMessage = tra('No changement');
			}
		}
		
		//Display form
		Ranchbe::getError()->checkErrors ();
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'toolsTab' )->activate ();
		$this->render ( 'default/editform', null, true );
		
		return;
		die;
		
	} 	//End of method
	

	/**
	 * 
	 * @param Zend_Config $myConfig
	 * @param string $mySection
	 * @return boolean
	 */
	protected function _saveConfig(Zend_Config $myConfig, $mySection) {
		//Backup previous rb.ini file
		copy(APPLICATION_PATH . '/configs/rb.ini' , APPLICATION_PATH . '/configs/rb.ini.back');
		
		//Create a new config object
		$config = new Zend_Config(array(), true);
		$avalaibleSections = array();

		//Rename sections from rb.ini content
		$iniFileContent = parse_ini_file(APPLICATION_PATH . '/configs/rb.ini', true);
		foreach($iniFileContent as $section=>$params){
			unset($iniFileContent[$section]);
			if(stripos($section, ':'))
				$section = trim(substr($section, 0, stripos($section, ':')));
			else
				$section = trim($section);
			//var_dump($section);
			$iniFileContent[$section] = $params;
			$avalaibleSections[] = $section;
		}
		//var_dump($iniFileContent);die;
		//var_dump($avalaibleSections);die;

		//populate the new config
		foreach($avalaibleSections as $section){
			$config->$section = array();
			if($section != 'production')
				$config->setExtend($section, 'production');
			if($section == $mySection){
				$config->$mySection = $myConfig->toArray();
			}else{
				$config->$section = $iniFileContent[$section];
			}
		}
		
		//Write the config
		$writer = new ZendRb_Config_Writer_Ini();
		$writer->setFilename(APPLICATION_PATH . '/configs/rb.ini');
		$writer->setConfig($config);
		//$writer->setNestSeparator('.');
		//echo '<pre>' . $writer->render() . '</pre>';
		$writer->write();
		
		return true;
	} //End of method
	
	
	/**
	 * 
	 * @param unknown_type $name
	 * @return array
	 */	
	protected static function _parsePath($name) {
		return explode('__', $name);
	} //End of method
	
	//-----------------------------------------------------------------
	//
	public function generatesqlAction() {
	} //End of method


} //End of class
