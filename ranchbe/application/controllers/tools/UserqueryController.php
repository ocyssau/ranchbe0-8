<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


class Tools_UserqueryController extends Zend_Controller_Action {
	//protected $smarty;
	protected $page_id = 'tools'; //(string)
	protected $ticket = ''; //(string)
	
	/** Path to reposit where are stored the sql queries scripts
	 * 
	 * @var string
	 */
	protected $_user_query_reposit_dir;
	
	
	protected $ifSuccessForward = array ('module' => 'tools', 
											'controller' => 'userquery', 
											'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'tools', 
										'controller' => 'userquery', 
										'action' => 'index' );
	
	
	

	//-----------------------------------------------------------------
	public function init() {
		//$this->smarty = & Ranchbe::getSmarty();
		$this->view->dojo()->enable();
		$this->ticket = $this->getRequest ()->getParam ( 'ticket' );
		$this->dbranchbe = & Ranchbe::getDb();
		
		$this->_user_query_reposit_dir = Ranchbe::getConfig()->path->scripts->queries;
		
		if (! is_dir ( $this->_user_query_reposit_dir )) {
			trigger_error("Error : $this->_user_query_reposit_dir  do not exits", E_USER_ERROR);
		}
	} //End of method

	//-----------------------------------------------------------------
	public function indexAction() {
		$search = new Rb_Search_Filesystem ( );
		$filter = new RbView_Helper_Filefilter ( $this->getRequest (), $search, $this->page_id );
		$filter->setUrl ( './tools/userquery/index' );
		$filter->setDefault ( 'sort_field', 'file_name' );
		$filter->setDefault ( 'sort_order', 'ASC' );
		$filter->setTemplate ( 'wildspace/searchBar.tpl' );
		$filter->finish ();
		
		//get list of files
		$this->view->list = Rb_Directory::getDatas ( $this->_user_query_reposit_dir, $filter->getParams () );

		// Display the template
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'toolsTab' )->activate ();
		
		$this->view->assign ( 'sameurl', './tools/userquery/index' ); //important: is first assign
		$this->view->assign ( 'views_helper_filter_form', 
				$filter->fetchForm ( $this->view->getEngine(), $this->container ) ); //generate code for the filter form
		//$this->view->assign('views_helper_pagination' , $pagination->fetchForm($this->view->getEngine()) ); //generate code for the filter form
		
		Ranchbe::getError()->checkErrors ();
		
	} //End of method
	

	//-----------------------------------------------------------------
	// ---- Suppress file
	public function suppressfileAction() {
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		$file_names = $this->getRequest ()->getParam ( 'file_name' );
		if (! $file_names)
			return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		
		if (! is_array ( $file_names )) { //generate array if parameter is string
			$file_names = array ($file_names );
		}
		
		foreach ( $file_names as $file_name ) {
			$file_name = basename($file_name);
			$file = new Rb_File ( $this->_user_query_reposit_dir . '/' . $file_name );
			//@todo : Add confirmation
			if ( $file->suppress () )
				Ranchbe::getError()->info('<em>' . tra ( 'file %element% is suppressed' ) . '</em><br />', array ('element' => $file_name ));
		}
		
		return $this->_forward ( $this->ifSuccessForward ['action'], 
								$this->ifSuccessForward ['controller'], 
								$this->ifSuccessForward ['module'], 
								$this->ifSuccessForward ['params'] );
		
	} //End of method
	

	//-----------------------------------------------------------------
	// ---- Rename file
	public function renamefileAction() {
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		$file_name = basename( $this->getRequest ()->getParam ( 'file_name' ) );
		$newName = basename( $this->getRequest ()->getParam ( 'newName' ) );
		
		if (! $newName || ! $file_name)
			$this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
		
		$file = new Rb_File ( $this->_user_query_reposit_dir . '/' . $file_name );
		$file->rename ( $this->_user_query_reposit_dir . '/' . $newName );
		
		return $this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
	} //End of method

	
	//---------------------------------------------------------------------
	public function copyfileAction() {
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		$file_name = basename($this->getRequest ()->getParam ( 'file_name' ));
		$newName = basename($this->getRequest ()->getParam ( 'newName' ));
		
		if (! $newName || ! $file_name)
			$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		
		$file = new Rb_File ( $this->_user_query_reposit_dir . '/' . $file_name );
		$file->copy ( $this->_user_query_reposit_dir . '/' . $newName, 0666, false );
		
		return $this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
	} //End of method
	

	//-----------------------------------------------------------------
	// ---- Upload file
	public function uploadAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		Rb_File::uploadFile ( $_FILES ['uploadFile'], $_REQUEST ['overwrite'], $this->_user_query_reposit_dir );
		return $this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
	} //End of method
	

	//-----------------------------------------------------------------
	// ----- Download a file
	public function downloadAction() {
		$file_name = basename($this->getRequest ()->getParam ( 'file_name' ));
		if (! $file_name )
			return $this->_forward ( $this->ifSuccessForward ['action'], 
						$this->ifSuccessForward ['controller'], 
						$this->ifSuccessForward ['module'], 
						$this->ifSuccessForward ['params'] );
		$viewer = new Rb_Viewer ();
		$viewer->init ( $this->_user_query_reposit_dir . '/' . $file_name );
		$viewer->push ();
	} //End of method
	

	//-----------------------------------------------------------------
	// ----- Download multiples files in a zip
	public function zipdownloadAction() {
		$file_names = $this->getRequest ()->getParam ( 'file_name' );
		if (! $file_names)
			return $this->_forward ( $this->ifSuccessForward ['action'], 
									$this->ifSuccessForward ['controller'], 
									$this->ifSuccessForward ['module'], 
									$this->ifSuccessForward ['params'] );
		
		if (! is_array ( $file_names )) { //generate array if parameter is string
			$file_names = array ($file_names );
		}
		
		require_once ('File/Archive.php');
		File_Archive::setOption ( 'zipCompressionLevel', 9 );
		foreach ( $file_names as $file_name ) {
			$file_name = basename($file_name);
			$multiread [] = File_Archive::read ( $this->_user_query_reposit_dir 
												. '/' . $file_name, $file_name );
		}
		
		$reader = File_Archive::readMulti ( $multiread );
		$writer = File_Archive::toArchive ( 'userqueries.zip', File_Archive::toOutput () );
		File_Archive::extract ( $reader, $writer );
		die ();
	} //End of method
	

	//-----------------------------------------------------------------
	public function editAction() {
		$file_name = basename($this->getRequest ()->getParam ( 'file_name' ));
		if (! $file_name)
			return $this->_forward ( $this->ifSuccessForward ['action'], 
									$this->ifSuccessForward ['controller'], 
									$this->ifSuccessForward ['module'], 
									$this->ifSuccessForward ['params'] );
		
		$this->view->file_name = $file_name;
		$this->view->sql_query = file_get_contents ( $this->_user_query_reposit_dir . '/' . $file_name );
	} //End of method
	

	//-----------------------------------------------------------------
	public function saveAction() {
		$file_name = basename($this->getRequest ()->getParam ( 'file_name' ));
		$sql_query = $this->getRequest ()->getParam ( 'sql_query' );
		$cancel = $this->getRequest ()->getParam ( 'cancel' );
		$this->_helper->viewRenderer->setNoRender(true);
		
		if($cancel){
			return $this->_forward ( $this->ifSuccessForward ['action'], 
									$this->ifSuccessForward ['controller'], 
									$this->ifSuccessForward ['module'], 
									$this->ifSuccessForward ['params'] );
		}
		
		if ($file_name){
			$handle = fopen ( $this->_user_query_reposit_dir . '/' . $file_name, w );
			if ($handle){
				fputs ( $handle, $sql_query );
				fclose ( $handle );
			}
		}
		return $this->_forward ( $this->ifSuccessForward ['action'], 
									$this->ifSuccessForward ['controller'], 
									$this->ifSuccessForward ['module'], 
									$this->ifSuccessForward ['params'] );
		
	} //End of method
	

	//-----------------------------------------------------------------
	public function executeAction() {
		$file_name = basename($this->getRequest ()->getParam ( 'file_name' ));
		$cancel = $this->getRequest ()->getParam ( 'cancel' );
		$render = $this->getRequest ()->getParam ( 'render' );
		
		//$this->_helper->viewRenderer->setNoRender(true);
		
		if( $cancel || !$file_name ){
			return $this->_forward ( $this->ifSuccessForward ['action'], 
									$this->ifSuccessForward ['controller'], 
									$this->ifSuccessForward ['module'], 
									$this->ifSuccessForward ['params'] );
		}

		//$this->view->PageTitle = sprintf ( tra ( 'Execute sql query %s' ), $file_name );
		//$form = new ZendRb_Form();
		
		
		//Select the render form
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = & new RbView_Pear_Html_QuickForm ( 'execute', 'POST', './tools/userquery/execute' );
		$form->setDefaults ( array ('result_format' => 'xls' ) );
		
		//Select xls render
		$sel = & $form->addElement ( 'hierselect', 'render', tra ( 'result_format' ) );
		$sel->setMainOptions ( array ('direct' => tra ( 'direct display' ), 'xls' => 'excel' ) );
			$renderPath = $this->_user_query_reposit_dir . '/renders';
			$renderfiles = glob ( $renderPath . '/*.php' );
			$t = array (); //Temp var
			foreach ( $renderfiles as $renderfile ) {
				$t [] = basename ( $renderfile );
			}
			$renderfiles = array_combine ( $t, $t );
			$renders = array();
			$renders ['xls'] = $renderfiles;
			$renders ['direct'] = array ();
		$sel->setSecOptions ( $renders );
		
		//Submit
		$form->addElement ( 'submit', 'submit', tra ( 'show' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		$form->addElement ( 'hidden', 'file_name', $file_name );
		
		// Tries to validate the form
		if ($form->validate ()) {
			//execute query
			$query = file_get_contents ( $this->_user_query_reposit_dir . '/' . $file_name );
			if (! $rs = $this->dbranchbe->execute ( $query )) {
				Ranchbe::getError()->errorDb( $this->dbranchbe->ErrorMsg (), $query );
				break;
			}
			
			//Select render
			//if ($render[0] === 'csv') {
			if ($render[0] === 'xls') {
				require_once ( $renderPath . '/' . basename($render[1]) );
				$render = new RbView_Adodb_ExcelRender ( $rs, 'user_query' );
				$render->render ();
				die ();
			} else if ($render[0] === 'direct') {
				require_once ('toRBhtml.inc.php');
				rs2html ( $rs, 'border=2 cellpadding=3' );
				die ();
			}
		}
		
		$this->view->render_select_form = $form->toHtml ();

	} //End of method


} //End of class
