<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


//require_once 'GUI/tab.php';


abstract class controllers_abstract extends Zend_Controller_Action {

	protected $error_stack; //error handler
	protected $space; //(object)
	protected $space_name; //(string)

	//---------------------------------------------------------------------
	/** 
	 * To get the default success forward
	 *
	 * @return void
	 */
	protected function _cancel() {
		return $this->_forward ( $this->ifFailedForward ['action'], 
								 $this->ifFailedForward ['controller'], 
								 $this->ifFailedForward ['module'], 
								 $this->ifFailedForward ['params'] );
	} //End of method


	/**
	 * Init form hidden param to set the values for forwards
	 *
	 * @param HTML_QuickForm | Zend_Form $form
	 * @return void
	 */
	protected function _initFormForward(&$form) {
		if(is_a($form, 'HTML_QuickForm')){
			$form->addElement ( 'hidden', 'ifSuccessModule', $this->ifSuccessForward ['module'] );
			$form->addElement ( 'hidden', 'ifSuccessController', $this->ifSuccessForward ['controller'] );
			$form->addElement ( 'hidden', 'ifSuccessAction', $this->ifSuccessForward ['action'] );
			$form->addElement ( 'hidden', 'ifFailedModule', $this->ifFailedForward ['module'] );
			$form->addElement ( 'hidden', 'ifFailedController', $this->ifFailedForward ['controller'] );
			$form->addElement ( 'hidden', 'ifFailedAction', $this->ifFailedForward ['action'] );
		}else if(is_a($form, 'Zend_Form')){
			$form->addElement ( 'hidden', 'ifSuccessModule', array('value'=>$this->ifSuccessForward ['module'] ));
			$form->addElement ( 'hidden', 'ifSuccessController', array('value'=>$this->ifSuccessForward ['controller'] ));
			$form->addElement ( 'hidden', 'ifSuccessAction', array('value'=>$this->ifSuccessForward ['action'] ));
			$form->addElement ( 'hidden', 'ifFailedModule', array('value'=>$this->ifFailedForward ['module'] ));
			$form->addElement ( 'hidden', 'ifFailedController', array('value'=>$this->ifFailedForward ['controller'] ));
			$form->addElement ( 'hidden', 'ifFailedAction', array('value'=>$this->ifFailedForward ['action'] ));
		}
	}
	
	/**
	 * 
	 * @return void
	 */
	protected function _initForward() {
		$ifSuccessModule = $this->getRequest ()->getParam ( 'ifSuccessModule' );
		$ifSuccessController = $this->getRequest ()->getParam ( 'ifSuccessController' );
		$ifSuccessAction = $this->getRequest ()->getParam ( 'ifSuccessAction' );
		$ifSuccessParams = $this->getRequest ()->getParam ( 'ifSuccessParams' );

		$ifFailedModule = $this->getRequest ()->getParam ( 'ifFailedModule' );
		$ifFailedController = $this->getRequest ()->getParam ( 'ifFailedController' );
		$ifFailedAction = $this->getRequest ()->getParam ( 'ifFailedAction' );
		$ifFailedParams = $this->getRequest ()->getParam ( 'ifFailedParams' );

		if ($ifSuccessModule)
			$this->ifSuccessForward ['module'] = $ifSuccessModule;
		if ($ifSuccessController)
			$this->ifSuccessForward ['controller'] = $ifSuccessController;
		if ($ifSuccessAction)
			$this->ifSuccessForward ['action'] = $ifSuccessAction;
		if ($ifSuccessParams)
			$this->ifSuccessForward ['params'] = $ifSuccessParams;

		if ($ifFailedModule)
			$this->ifFailedForward ['module'] = $ifFailedModule;
		if ($ifFailedController)
			$this->ifFailedForward ['controller'] = $ifFailedController;
		if ($ifFailedAction)
			$this->ifFailedForward ['action'] = $ifFailedAction;
		if ($ifFailedParams)
			$this->ifFailedForward ['params'] = $ifFailedParams;

		$this->view->assign ( 'ifSuccessModule', $this->ifSuccessForward ['module'] );
		$this->view->assign ( 'ifSuccessController', $this->ifSuccessForward ['controller'] );
		$this->view->assign ( 'ifSuccessAction', $this->ifSuccessForward ['action'] );
		$this->view->assign ( 'ifSuccessParams', $this->ifSuccessForward ['params'] );

		$this->view->assign ( 'ifFailedModule', $this->ifFailedForward ['module'] );
		$this->view->assign ( 'ifFailedController', $this->ifFailedForward ['controller'] );
		$this->view->assign ( 'ifFailedAction', $this->ifFailedForward ['action'] );
		$this->view->assign ( 'ifFailedParams', $this->ifFailedForward ['params'] );
	}
	
	/**
	 * 
	 * @return void
	 */
	protected function _putContextInView() {
		$this->context = & Ranchbe::getContext ();
		$this->view->project_number = $this->context->getProperty ( 'project_number' );
		$this->view->project_id = $this->context->getProperty ( 'project_id' );
		$this->view->container_number = $this->context->getProperty ( 'container_number' );
		$this->view->container_id = $this->context->getProperty ( 'container_id' );
		$this->view->SPACE_NAME = $this->context->getProperty ( 'space_name' );
	}

	//---------------------------------------------------------------------
	/** 
	 * To get the default success forward
	 * 
	 * @param string $action
	 * @param string $controller
	 * @param string $module
	 * @return void
	 */
	protected function _successForward($action = 0, $controller = 0, $module = 0) {
		if ($action)
			$this->ifSuccessForward ['action'] = $action;
		if ($controller)
			$this->ifSuccessForward ['controller'] = $controller;
		if ($module)
			$this->ifSuccessForward ['module'] = $module;
		return $this->_forward ( $this->ifSuccessForward ['action'],
 								$this->ifSuccessForward ['controller'],
								$this->ifSuccessForward ['module'],
								$this->ifSuccessForward ['params'] );
	} //End of method

	//---------------------------------------------------------------------
	/**
	 * 
	 * @param HTML_QuickForm $form
	 * @return void
	 */
	protected function _quickFormRendererSet(HTML_QuickForm &$form) {
		require_once ('HTML/QuickForm/Renderer/ArraySmarty.php'); //Lib to use Smarty with QuickForm
		//Set the renderer for display QuickForm form in a smarty template
		$renderer = new HTML_QuickForm_Renderer_ArraySmarty ( Ranchbe::getSmarty(), true );
		$renderer->setRequiredTemplate ( '{if $error}
	      <font color="red">{tr}{$label|upper}{/tr}</font>
	      {else}
	      {$label}
	      {if $required}
	      <font color="red" size="1">*</font>
	      {/if}
	      {/if}' );

		$renderer->setErrorTemplate ( '{if $error}
	      <font color="orange" size="1">{$error}</font><br />
	      {/if}{$html}' );

		$form->accept ( $renderer );

		// assign array with form data
		$this->view->assign ( 'form', $renderer->toArray () );
	} //End of function

	//---------------------------------------------------------------------
	/**
	 * Help to create zend path to current controller/action
	 * 
	 * @param string $action
	 * @return string
	 */
	protected function actionUrl($action) {
		return Zend_Controller_Front::getInstance ()->getBaseUrl ()
								. '/' . $this->getRequest ()->getModuleName ()
								. '/' . $this->getRequest ()->getControllerName ()
								. '/' . $action;
	} //End of function

	//---------------------------------------------------------------------
	/**
	 * To get the default success forward
	 * 
	 * @return void
	 */
	public function cancelAction() {
		return $this->_cancel ();
	} //End of method


	//---------------------------------------------------------------------
	/* Its difficult to get parameters with '/' in content, so if necessary,
	 * send url type module=name&action=name&controller=name&param_1=value&param_n=value 
	 * and rewrite it in path with zend convention
	 * 
	 * @param  string		url type module=name&action=name&controller=name&param_1=value&param_n=value
	 * @return array
	 */
	protected function parseUrl($string) {
		//$string = $this->getRequest ()->getParam ( 'ifSuccessForward' );
		parse_str ( $string, $Parsed );
		$ret ['module'] = $Parsed ['module'];
		$ret ['action'] = $Parsed ['action'];
		$ret ['controller'] = $Parsed ['controller'];
		unset ( $Parsed ['module'], $Parsed ['action'], $Parsed ['controller'] );
		$ret ['params'] = $Parsed;
		return $ret;
	} //End of method

	//---------------------------------------------------------------------
	/* Convert url to zend path
	 * (eg : module=module&action=action&controller=controller&p1=v1&p2=v2
	 * 			->/module/controller/action/p1/v1/p2/v2 )
	 * 
	 * @param  string		url type module=module&action=action&controller=controller&p1=v1&p2=v2
	 * @return array
	 */
	protected function _urlToZend($string) {
		$hash = $this->parseUrl($string);
		$Zpath = '/'. $hash['module']
				.'/'.$hash['controller']
				.'/'.$hash['action'];
		foreach($hash['params'] as $key=>$value){
			$Zpath .= '/'. $key .'/'. $value;
		}
		return $Zpath;
	} //End of method

	//---------------------------------------------------------------------
	/* Convert zend path to url
	 * (eg : /module/controller/action/p1/v1/p2/v2 
	 * 		-> module=module&action=action&controller=controller&p1=v1&p2=v2)
	 * 
	 * @param  string		url type /module/controller/action/p1/v1/p2/v2
	 * @return array
	 */
	//protected function _zendToUrl($string) {
	//} //End of method
	
	//---------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see external/Zend/Controller/Zend_Controller_Action#preDispatch()
	 */
	function preDispatch() {
		if (! Ranchbe::getAuth ()->hasIdentity ()) {
			$params = $this->getRequest()->getParams();
			$forwardUrl[] = 'module='.$params['module'];
			$forwardUrl[] = 'controller='.$params['controller'];
			$forwardUrl[] = 'action='.$params['action'];
			unset($params['module'], $params['controller'], $params['action']);
			foreach($params as $key=>$value){
				$forwardUrl[] = $key . '='. $value;
			}
			$forwardUrl = implode('&', $forwardUrl);
			$this->_forward ( 'login', 'auth', 'auth' , array('forwardUrl'=>$forwardUrl) );
			//$this->_redirect('auth/auth/login');
		} else {
			$this->view->current_user_name = Rb_User::getCurrentUser ()->getName ();
			$this->view->current_user_id = Rb_User::getCurrentUser ()->getId ();
		}
	}

	//---------------------------------------------------------------------
	/**
	 * Serialize the $_request and create hidden form
	 * $select(array) permet de selectionner les cle a serialiser
	 * si $form est fournis, genere des elements hidden quick_form
	 * 
	 * @param HTML_QuickForm $form
	 * @param array $select
	 * @return string
	 */
	protected function serialize_request_post(HTML_QuickForm &$form = null, $select = '') {
		//@todo: revise cette function pour les tableau > 2 dimension
		$request = $this->getRequest ()->getParams ();

		if (is_array ( $select )) {
			$select = array_flip ( $select );
			$request = array_intersect_key ( $request, $select );
		}
		$html = '';
		foreach ( $request as $key => $val ) {
			if ($key == 'logout' || $key == 'handle' || $key == 'passwd' || $key == 'handle' || $key == 'tab' || $key == 'kt_language' || $key == 'tz_offset' || $key == 'username' || $key == 'ml_id' || $key == 'PHPSESSID' || $key == 'LuSession') {
				continue;
			}
			if (is_array ( $val )) {
				foreach ( $val as $sub_key => $sub_sval ) {
					$html .= '<input type="hidden" name="' . $key . '[' . $sub_key . ']' . '" value="' . $sub_sval . '" />' . "\n";
				}
			} else {
				$html .= '<input type="hidden" name="' . $key . '" value="' . $val . '" />' . "\n";
			}
		}

		if (is_object ( $form ))
			$form->addElement ( 'static', '', $html );

		return $html;

	} //End of function

	//---------------------------------------------------------------------
	/**
	 * put tool bar in layout var toolBar from template module/actions.tpl
	 * 
	 * @return void
	 */
	public function toolbarAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'toolBar' );
		$this->render($this->getRequest()->getModuleName().'/actions', null, true);
	}
	

}//End of class

