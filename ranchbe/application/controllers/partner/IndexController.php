<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');
class Partner_IndexController extends controllers_abstract {
	protected $partner; //(object)
	protected $page_id = 'partnerManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'partner', 'controller' => 'index', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'partner', 'controller' => 'index', 'action' => 'index' );
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->view->dojo()->enable();
		if ($this->getRequest ()->getParam ( 'ifSuccessForward' )) {
			$this->ifSuccessForward = $this->getRequest ()->getParam ( 'ifSuccessForward' );
			$this->ifSuccessForward = $this->parseUrl ( $this->ifSuccessForward );
		}
		
		if ($this->getRequest ()->getParam ( 'ifFailedForward' )) {
			$this->ifSuccessForward = $this->getRequest ()->getParam ( 'ifFailedForward' );
			$this->ifSuccessForward = $this->parseUrl ( $this->ifFailedForward );
		}
		
		$cancel = $this->getRequest ()->getParam ( 'cancel' );
		if ($cancel)
			$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		
		$this->ticket = $this->view->ticket;
		
		//Zend_Registry::set('space',$this->space);
		//Zend_Registry::set('container',$this->container);
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward ('get');
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		$this->_helper->actionStack ( 'toolbar' );
		//Include generic definition of the code for manage filters
		$search = new Rb_Search_Db ( Rb_Partner::get ()->getDao () );
		$filter = new RbView_Helper_Filter ( $this->getRequest (), $search, $this->page_id );
		$filter->setUrl ( './partner/index/get' );
		$filter->setDefault ( 'sort_field', 'partner_number' );
		$filter->setDefault ( 'sort_order', 'ASC' );
		$filter->setFindElements ( array ('partner_number' => tra ( 'partner_number' ), 'first_name' => tra ( 'first_name' ), 'last_name' => tra ( 'last_name' ), 'partner_type' => tra ( 'partner_type' ), 'adress' => tra ( 'adress' ), 'city' => tra ( 'city' ), 'zip_code' => tra ( 'zip_code' ), 'phone' => tra ( 'phone' ), 'cell_phone' => tra ( 'cell_phone' ), 'mail' => tra ( 'mail' ), 'web_site' => tra ( 'web_site' ), 'activity' => tra ( 'activity' ), 'company' => tra ( 'company' ) ) );
		$filter->setTemplate ( 'partner/searchBar.tpl' );
		$filter->finish ();
		
		//get all partners
		$this->view->list = Rb_Partner::get ()->getAll ( $filter->getParams () );
		
		$pagination = new RbView_Helper_Pagination($filter);
	    $pagination->setPagination( count($this->view->list) );
	    $pagination->setRequest( $this->getRequest() );
	    $this->view->views_helper_pagination = $pagination->fetchForm( $this->view->getEngine () );
				
		$this->error_stack->checkErrors ();
		
		// Display the template
		RbView_Menu::get ();
		RbView_Tab::get ( 'partnersTab' )->activate ();
		$this->view->assign ( 'PageTitle', tra ( 'Partners manager' ) );
		$this->view->assign ( 'views_helper_filter_form', $filter->fetchForm ( $this->view->getEngine() ) ); //generate code for the filter form
	} //End of method
	

	//---------------------------------------------------------------------
	public function createAction() {
		//if (!$check_flood) return false;
		if (! Ranchbe::checkPerm ( 'admin_partner', Ranchbe::getAcl ()->getRootResource (), false ))
			return $this->_cancel ();
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new HTML_QuickForm ( 'createPartner', 'post', $this->actionUrl ( 'create' ) );
		$form->addElement ( 'header', 'editheader', tra ( 'Create a partner' ) );
		$this->_helper->viewRenderer->setScriptAction('edit');
		return $this->_finishEditForm ( $form, array (), 'create' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function editAction() {
		$this->partner_id = $this->getRequest ()->getParam ( 'partner_id' );
		
		if( is_array($this->partner_id)) $this->partner_id = (int) $this->partner_id[0];
		if (! $this->partner_id)
			return $this->_forward ( $this->ifSuccessForward ['action'], 
									$this->ifSuccessForward ['controller'], 
									$this->ifSuccessForward ['module'], 
									$this->ifSuccessForward ['params'] );
		if (! Ranchbe::checkPerm ( 'admin_partner', Ranchbe::getAcl ()->getRootResource (), false ))
			return $this->_cancel ();
			
		$this->partner = Rb_Partner::get ( $this->partner_id );
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new HTML_QuickForm ( 'editPartner', 'post', $this->actionUrl ( 'edit' ) );
		//Get infos
		$Infos = $this->partner->getProperties ();
		$form->setDefaults ( array (
			'partner_number' => $Infos ['partner_number'], 
			'partner_type' => $Infos ['partner_type'], 
			'first_name' => $Infos ['first_name'], 
			'last_name' => $Infos ['last_name'], 
			'adress' => $Infos ['adress'], 
			'city' => $Infos ['city'], 
			'zip_code' => $Infos ['zip_code'], 
			'phone' => $Infos ['phone'], 
			'cell_phone' => $Infos ['cell_phone'], 
			'mail' => $Infos ['mail'], 
			'web_site' => $Infos ['web_site'], 
			'activity' => $Infos ['activity'], 
			'company' => $Infos ['company'], 
			'fsCloseDate' => array ('d' => $fsCloseDate [d], 'm' => $fsCloseDate [M], 'Y' => $fsCloseDate [Y] ), 
			'submit' => 'modify' ) );
		$form->addElement ( 'header', 'editheader', vsprintf ( tra ( 'Edit partner %s' ), array ($Infos ['partner_number'] ) ) );
		//Add hidden fields
		$form->addElement ( 'hidden', 'partner_id', $this->partner_id );
		return $this->_finishEditForm ( $form, $Infos, 'edit' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function suppressAction() {
		if (! Ranchbe::checkPerm ( 'admin_partner', Ranchbe::getAcl ()->getRootResource (), false ))
			return $this->_cancel ();
		
		$partner_ids = $this->getRequest ()->getParam ( 'partner_id' );
		if (! $partner_ids)
			return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		
		if (! is_array ( $partner_ids )) //generate array if parameter is string
			$partner_ids = array ($partner_ids );
		
		foreach ( $partner_ids as $partner_id ) {
			Rb_Partner::get ( $partner_id )->suppress ();
		}
		$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
	} //End of method
	

	//---------------------------------------------------------------------
	public function importAction() {
		if (! Ranchbe::checkPerm ( 'admin_partner', Ranchbe::getAcl ()->getRootResource (), false ))
			return $this->_cancel ();
		Rb_Partner::get ()->ImportPartnerCsv ( $_FILES ['csvlist'] ['tmp_name'], $_REQUEST ['overwrite'] );
	} //End of method
	

	//---------------------------------------------------------------------
	public function exportAction() {
		$this->view->layout()->disableLayout();
		//$this->_helper->viewRenderer->setNoRender(true);
		$this->getResponse ()->setHeader ( 'Content-Type', 'application/csv' )
							->setHeader ( 'Content-Transfer-Encoding', Binary )
							->setHeader ( 'Content-Disposition', 'attachment; filename=export_partners.csv' );
		$this->view->list = Rb_Partner::get ()->getAll ();
	} //End of method

	
	//---------------------------------------------------------------------
	public function _modify($values) {
		$values ['partner_number'] = Rb_Partner::composeNumber ( $values ['first_name'], $values ['last_name'] );
		foreach ( $values as $name => $val ) {
			$this->partner->setProperty ( $name, $val );
		}
		$this->partner->save ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function _create($values) {
		$partner = new Rb_Partner ( 0 );
		foreach ( $values as $name => $val ) {
			$partner->setProperty ( $name, $val );
		}
		$partner->save ();
		return $partner->getId ();
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	protected function _finishEditForm(&$form, $Infos, $mode) {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		//Add fields for input informations in all case
		$form->addElement ( 'text', 'partner_number', tra ( 'Number' ), array ('readonly', 'disabled', 'value' => $Infos ['partner_number'], 'size' => 32 ) );
		$form->addElement ( 'text', 'first_name', tra ( 'first_name' ) );
		$form->addElement ( 'text', 'last_name', tra ( 'last_name' ) );
		$form->addElement ( 'text', 'adress', tra ( 'adress' ) );
		$form->addElement ( 'text', 'city', tra ( 'city' ) );
		$form->addElement ( 'text', 'zip_code', tra ( 'zip_code' ) );
		$form->addElement ( 'text', 'phone', tra ( 'phone' ) );
		$form->addElement ( 'text', 'cell_phone', tra ( 'cell_phone' ) );
		$form->addElement ( 'text', 'fax', tra ( 'fax' ) );
		$form->addElement ( 'text', 'mail', tra ( 'mail' ) );
		$form->addElement ( 'text', 'web_site', tra ( 'web_site' ) );
		$form->addElement ( 'text', 'activity', tra ( 'activity' ) );
		$form->addElement ( 'text', 'company', tra ( 'company' ) );
		$form->addElement ( 'reset', 'reset', 'reset' );
		$form->addElement ( 'submit', 'submit', tra ( 'Validate' ) );
		
		//Construct array for selection set
		$SelectSet [NULL] = ''; //Leave a blank option for default none selected
		$SelectSet ['customer'] = tra ( 'customer' );
		$SelectSet ['supplier'] = tra ( 'supplier' );
		$SelectSet ['staff'] = tra ( 'staff' );
		$select = & $form->addElement ( 'select', 'partner_type', tra ( 'partner_type' ), $SelectSet );
		
		//Add hidden fields
		$form->addElement ( 'hidden', 'space', $this->space_name );
		
		//Add validation rules to check input data
		$form->addRule ( 'partner_type', tra ( 'is required' ), 'required' );
		$form->applyFilter ( '__ALL__', 'trim' );
		$form->applyFilter ( 'last_name', 'mb_strtoupper' );
		$form->applyFilter ( 'first_name', 'mb_strtolower' );
		$form->applyFilter ( 'first_name', 'ucfirst' );
		$form->applyFilter ( 'partner_number', 'mb_strtoupper' );
		$form->applyFilter ( 'partner_number', 'no_accent' );
		
		//$mask = DEFAULT_PARTNER_MASK;
		//$form->addRule('partner_number', tra('This number is not valid'), 'regex', "/$mask/" , 'server');
		

		//Get the forms values
		/*
		  $proj[partner_id]   = $form->getSubmitValue('partner_id');
		  $proj[partner_number]   = $form->getSubmitValue('partner_number');
		  $proj[first_name]   = $form->getSubmitValue('first_name');
		  $proj[last_name]   = $form->getSubmitValue('last_name');
		  $proj[adress]   = $form->getSubmitValue('adress');
		  $proj[city]   = $form->getSubmitValue('city');
		  $proj[postal_code]   = $form->getSubmitValue('postal_code');
		  $proj[phone]   = $form->getSubmitValue('phone');
		  $proj[mail]   = $form->getSubmitValue('mail');
		  $proj[web_site]   = $form->getSubmitValue('web_site');
		  $proj[activity]   = $form->getSubmitValue('activity');
		  */
		
		// Try to validate the form
		if ($form->validate ()) {
			$form->freeze (); //and freeze it
			// Form is validated, then processes the create request
			if ($mode == 'create') {
				$form->process ( array ($this, '_create' ), true );
			} else if ($mode == 'edit') {
				$form->process ( array ($this, '_modify' ), true );
			}
		} //End of validate form
		
		//Set the renderer for display QuickForm form in a smarty template
		$this->_quickFormRendererSet ( $form );
		
		$this->error_stack->checkErrors ();
		
	} //End of method


} //End of class
