<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');
class Doctype_IndexController extends controllers_abstract {
	protected $partner; //(object)
	protected $page_id = 'doctype'; //(string)
	protected $ifSuccessForward = array ('module' => 'doctype', 'controller' => 'index', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'doctype', 'controller' => 'index', 'action' => 'index' );
	
	protected $_privileges = array ('create', 'edit', 'suppress' ); //(array)default privileges name
	

	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->view->dojo()->enable();
		if ($this->getRequest ()->getParam ( 'ifSuccessForward' )) {
			$this->ifSuccessForward = $this->getRequest ()->getParam ( 'ifSuccessForward' );
			$this->ifSuccessForward = $this->parseUrl ( $this->ifSuccessForward );
		}
		
		if ($this->getRequest ()->getParam ( 'ifFailedForward' )) {
			$this->ifSuccessForward = $this->getRequest ()->getParam ( 'ifFailedForward' );
			$this->ifSuccessForward = $this->parseUrl ( $this->ifFailedForward );
		}
		
		$cancel = $this->getRequest ()->getParam ( 'cancel' );
		if ($cancel)
			$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		
		$this->ticket = $this->view->ticket;
		//$this->doctype = Rb_Doctype::get(0); //Create new manager
		//$this->check_flood = check_flood($_REQUEST['page_id']);
		$this->view->assign ( 'icons_dir', DEFAULT_DOCTYPES_ICONS_URL );
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward ( 'get' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		//Include generic definition of the code for manage filters
		$search = new Rb_Search_Db ( Rb_Doctype::get ()->getDao () );
		$filter = new RbView_Helper_Doctypefilter ( $this->getRequest (), $search, $this->page_id );
		$filter->setUrl ( './doctype/index/get' );
		$filter->setDefault ( 'sort_field', 'doctype_number' );
		$filter->setDefault ( 'sort_order', 'ASC' );
		$filter->setTemplate ( 'doctype/searchBar.tpl' );
		$filter->setFindElements ( array ('doctype_number' => tra ( 'doctype_number' ), 'doctype_description' => tra ( 'doctype_description' ), 'script_post_store' => tra ( 'script_post_store' ), 'script_pre_store' => tra ( 'script_pre_store' ), 'script_pre_update' => tra ( 'script_pre_update' ), 'script_post_update' => tra ( 'script_post_update' ), 'recognition_regexp' => tra ( 'recognition_regexp' ), 'file_extension' => tra ( 'file_extension' ), 'file_type' => tra ( 'file_type' ), 'can_be_composite' => tra ( 'can_be_composite' ) ) );
		$filter->finish ();
		
		//get all
		$this->view->list = Rb_Doctype::get ()->getAll ( $filter->getParams () );
		
		$pagination = new RbView_Helper_Pagination($filter);
	    $pagination->setPagination( count($this->view->list) );
	    $pagination->setRequest( $this->getRequest() );
	    $this->view->views_helper_pagination = $pagination->fetchForm( $this->view->getEngine () );
				
		$this->error_stack->checkErrors ();
		
		// Display the template
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'doctypesTab' )->activate ();
		$this->view->assign ( 'PageTitle', tra ( 'Doctypes manager' ) );
		$this->view->assign ( 'views_helper_filter_form', $filter->fetchForm ( $this->view->getEngine() ) ); //generate code for the filter form
		$this->_helper->actionStack ( 'toolbar' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function createAction() {
		Ranchbe::checkPerm ( 'admin_doctype', Ranchbe::getAcl ()->getRootResource (), true );
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new HTML_QuickForm ( 'createDoctype', 'post', $this->actionUrl ( 'create' ) );
		$form->addElement ( 'header', 'editheader', tra ( 'Create a doctype' ) );
		$form->setDefaults ( array ('visu_file_extension' => '.pdf', 'icon' => '_default.gif' ) );
		$this->_helper->viewRenderer->setScriptAction('edit');
		return $this->_finishEditForm ( $form, array (), 'create' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function editAction() {
		Ranchbe::checkPerm ( 'admin_doctype', Ranchbe::getAcl ()->getRootResource (), true );
		
		$this->doctype_id = $this->getRequest ()->getParam ( 'doctype_id' );
		if (! $this->doctype_id)
			return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		
		$this->doctype = & Rb_Doctype::get ( $this->doctype_id );
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new HTML_QuickForm ( 'editPartner', 'post', $this->actionUrl ( 'edit' ) );
		
		//Get infos
		$Infos = $this->doctype->getProperties ();
		foreach ( explode ( ' ', $Infos ['file_type'] ) as $type ) { //Create a array for set default values of the file_type field
			$file_type [$type] = true;
		}
		if (isset ( $file_type ['file'] ))
			$this->view->assign ( 'fileselect', 1 );
		
		$form->setDefaults ( array (
			'doctype_number' => $Infos ['doctype_number'], 
			'doctype_description' => $Infos ['doctype_description'], 
			'can_be_composite' => $Infos ['can_be_composite'], 
			'script_post_store' => $Infos ['script_post_store'], 
			'script_pre_store' => $Infos ['script_pre_store'], 
			'script_pre_update' => $Infos ['script_pre_update'], 
			'script_post_update' => $Infos ['script_post_update'], 
			'recognition_regexp' => $Infos ['recognition_regexp'], 
			'file_type' => $file_type, 
			'file_extension' => explode ( ' ', $Infos ['file_extension'] ), 
			'visu_file_extension' => explode ( ' ', $Infos ['visu_file_extension'] ), 
			'icon' => $Infos ['icon'] ) );
		//'file_type_ext' => array( explode(' ' , $Infos['file_type']) , explode(' ' , $Infos['file_extension'])),
		
		$form->addElement ( 'header', 'editheader', vsprintf ( tra ( 'Edit doctype %s' ), array ($Infos ['doctype_number'] ) ) );
		
		//Add hidden fields
		$form->addElement ( 'hidden', 'doctype_id', $this->doctype_id );
		
		return $this->_finishEditForm ( $form, $Infos, 'edit' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function suppressAction() {
		if (! Ranchbe::checkPerm ( 'admin_doctype', Ranchbe::getAcl ()->getRootResource (), false ))
			return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		
		$doctype_ids = $this->getRequest ()->getParam ( 'doctype_id' );
		if (! $doctype_ids)
			return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		
		if (! is_array ( $doctype_ids )) //generate array if parameter is string
			$doctype_ids = array ($doctype_ids );
		
		foreach ( $doctype_ids as $doctype_id ) {
			Rb_Doctype::get ( $doctype_id )->suppress ();
		}
		$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
	} //End of method
	

	//---------------------------------------------------------------------
	public function importAction() {
		if (! Ranchbe::checkPerm ( 'admin_doctype', Ranchbe::getAcl ()->getRootResource (), true ))
			return $this->_cancel ();
		
		$csvfile = $_FILES ['csvlist'] ['tmp_name'];
		$overwrite = $_REQUEST ['overwrite'];
		
		$records = importManager::importCsv ( $csvfile );
		foreach ( $records as $u ) {
			if (! empty ( $u ['doctype_number'] ))
				if (! $this->create ( $u, $overwrite ))
					$err [] = $u;
		}
	
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	protected function _finishEditForm(&$form, $Infos, $mode) {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		//Add fields for input informations in all case
		$form->addElement ( 'text', 'doctype_number', tra ( 'doctype_number' ) );
		$this->view->assign ( 'number_help', Ranchbe::getConfig()->doctypes->maskhelp );
		$form->addElement ( 'text', 'doctype_description', tra ( 'doctype_description' ), array ('size' => 20 ) );
		$form->addElement ( 'textarea', 'recognition_regexp', tra ( 'recognition_regexp' ), array ('cols' => 60 ) );
		
		//Select can_be_composite
		$select = & $form->addElement ( 'select', 'can_be_composite', 
				array (tra ( 'can_be_composite' ), 'note' => '' ), 
				array (1 => 'Yes', 0 => 'No' ) );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		//RanchBE0.4.3
		// Creates a checkboxes group to select type of document
		// $FILE_TYPE_LIST is defined in ranchbe_setup.php;
		foreach ( Ranchbe::getFileTypes () as $filetype ) {
			$checkbox [] = &HTML_QuickForm::createElement ( 'checkbox', $filetype, null, $filetype, array ('onclick' => "afficherExtensions('$filetype');" ) );
		}
		$form->addGroup ( $checkbox, 'file_type', 'File type', '<br />' );
		
		$FILE_EXTENSION_LIST = Ranchbe::getFileExtensions ();
		//Create a select list to select extension of file
		$fileExtSelectSet = array_combine ( $FILE_EXTENSION_LIST ['file'], 
											$FILE_EXTENSION_LIST ['file'] );
		$select = & $form->addElement ( 'select', 'file_extension', array ('File extension', 'note' => '' ), $fileExtSelectSet );
		$select->setSize ( 5 );
		$select->setMultiple ( true );
		
		//Create a select list to select visu file extension
		//$vfileExtSelectSet = array_combine ( Ranchbe::getVisuFileExtensions (), Ranchbe::getVisuFileExtensions () );
		$vfile_extension = Ranchbe::getConfig()->visu->file_extensions->toArray();
		$vfileExtSelectSet = array_combine ($vfile_extension,$vfile_extension);
		$select = & $form->addElement ( 'select', 
										'visu_file_extension', 
										array ('Visualisation file extension', 'note' => '' ), 
										$vfileExtSelectSet );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		//Construct array for set script_pre_store
		$scriptsList = glob ( Ranchbe::getConfig()->path->scripts->doctype . '/' . "*.php" );
		$pre_store_SelectSet [NULL] = "";
		foreach ( $scriptsList as $scripts ) {
			$scripts = basename ( $scripts );
			$pre_store_SelectSet ["$scripts"] = "$scripts";
		}
		$select = & $form->addElement ( 'select', 'script_pre_store', array (tra ( 'script_pre_store' ), 'note' => '' ), $pre_store_SelectSet );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		//Construct array for set script_post_store
		$post_store_SelectSet [NULL] = "";
		foreach ( $scriptsList as $scripts ) {
			$scripts = basename ( $scripts );
			$post_store_SelectSet ["$scripts"] = "$scripts";
		}
		$select = & $form->addElement ( 'select', 'script_post_store', array (tra ( 'script_post_store' ), 'note' => '' ), $post_store_SelectSet );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		//Construct array for set script_pre_update
		$pre_update_SelectSet [NULL] = "";
		foreach ( $scriptsList as $scripts ) {
			$scripts = basename ( $scripts );
			$pre_update_SelectSet ["$scripts"] = "$scripts";
		}
		$select = & $form->addElement ( 'select', 'script_pre_update', array (tra ( 'script_pre_update' ), 'note' => '' ), $pre_update_SelectSet );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		//Construct array for set script_post_update
		$post_update_SelectSet [NULL] = "";
		foreach ( $scriptsList as $scripts ) {
			$scripts = basename ( $scripts );
			$post_update_SelectSet ["$scripts"] = "$scripts";
		}
		$select = & $form->addElement ( 'select', 'script_post_update', array (tra ( 'script_post_update' ), 'note' => '' ), $post_update_SelectSet );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		//Construct array for set icon file
		$iconExt = '.' . trim(Ranchbe::getConfig()->icons->doctype->type, '.');
		$iconsList = glob ( Ranchbe::getConfig()->icons->doctype->source->path
							 . '/'
							 . "*" . $iconExt );
		sort ( $iconsList );
		
		$icon_SelectSet [NULL] = "";
		foreach ( $iconsList as $icon ) {
			$icon = basename ( $icon );
			$icon_SelectSet ["$icon"] = "$icon";
		}
		$select = & $form->addElement ( 
						'select', 
						'icon', 
						array (tra ( 'icon' ), 'note' => '' ), 
						$icon_SelectSet, 
						array (
								'id' => 'icon_select', 
								'onChange=javascripts:updateIcon(this);return false;' 
						) );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		$form->addElement ( 'submit', 'submit', tra ( 'Validate' ) );
		
		//Construct array for selection set
		$SelectSet [NULL] = ''; //Leave a blank option for default none selected
		$SelectSet ['customer'] = tra ( 'customer' );
		$SelectSet ['supplier'] = tra ( 'supplier' );
		$SelectSet ['staff'] = tra ( 'staff' );
		$select = & $form->addElement ( 'select', 'partner_type', tra ( 'partner_type' ), $SelectSet );
		
		//Add hidden fields
		$form->addElement ( 'hidden', 'space', $this->space_name );
		
		//Add validation rules to check input data
		$mask = DEFAULT_DOCTYPE_MASK;
		$form->addRule ( 'doctype_number', tra ( 'is required' ), 'required' );
		$form->addRule ( 'doctype_number', 
							Ranchbe::getConfig()->doctypes->maskhelp,
							'regex', "/".Ranchbe::getConfig()->doctypes->mask."/", 'server' );
		//$form->addRule('file_extension', tra('Extension is required'), 'required');
		$form->addRule ( 'file_type', tra ( 'is required' ), 'required' );
		$form->applyFilter ( '__ALL__', 'trim' );
		
		// Try to validate the form
		if ($form->validate ()) {
			$form->freeze (); //and freeze it
			// Form is validated, then processes the create request
			if ($mode == 'create') {
				$form->process ( array ($this, '_create' ), true );
			} else if ($mode == 'edit') {
				$form->process ( array ($this, '_modify' ), true );
			}
		} //End of validate form
		

		//Set the renderer for display QuickForm form in a smarty template
		$this->_quickFormRendererSet ( $form );
		
		$this->error_stack->checkErrors ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function _modify($values) {
		$values = $this->_getValues ( $values );
		foreach ( $values as $name => $val ) {
			$this->doctype->setProperty ( $name, $val );
		}
		return $this->doctype->save ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function _create($values) {
		$doctype = new Rb_Doctype ( 0 );
		$values = $this->_getValues ( $values );
		foreach ( $values as $name => $val ) {
			$doctype->setProperty ( $name, $val );
		}
		$doctype->save ();
		return $doctype->getId ();
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _getValues($values) { // Get the data from the form and format it
		

		$FILE_EXTENSION_LIST = Ranchbe::getFileExtensions ();
		
		//unselect file extension list if type file is not selected
		if (! isset ( $values ['file_type'] ['file'] ))
			$values ['file_extension'] = array ();
			
		//if type file is selected check if file_extension is set
		if (isset ( $values ['file_type'] ['file'] ) && ! is_array ( $values ['file_extension'] )) {
			print (tra ( 'Extension is required' )) ;
			return false;
		}
		
		//Reformat the array send by form from the checkbox selection for file_type
		$values ['file_type'] = array_keys ( $values ['file_type'] );
		
		//Add valid extension to file_extension list for type cadds, ps ...etc
		foreach ( $values ['file_type'] as $filetype ) {
			if ($filetype != 'file') {
				$values ['file_extension'] = array_merge ( $values ['file_extension'], array_values ( $FILE_EXTENSION_LIST [$filetype] ) );
			}
		}
		
		return $values;
	} //End of method


} //End of class
