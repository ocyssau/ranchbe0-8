<?php
class Auth_AuthController extends controllers_abstract {
	
	//------------------------------------------------------------------------------
	public function init() {
	} //End of method

	//---------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see external/Zend/Controller/Zend_Controller_Action#preDispatch()
	 */
	function preDispatch() {
	}
	
	//------------------------------------------------------------------------------
	public function loginAction() {
		Zend_Loader::loadClass ( 'Zend_Filter_StripTags' );
		
		$f = new Zend_Filter_StripTags ( );
		$username = $f->filter ( $this->_request->getPost ( 'username' ) );
		$password = $f->filter ( $this->_request->getPost ( 'passwd' ) );
		
		$this->view->forwardUrl = $this->_request->getParam('forwardUrl');
		$Zforward = $this->_urlToZend($this->view->forwardUrl);

		//var_dump($this->view->forwardUrl, $forwardUrl, $Zforward);
		
		$this->view->current_user_name = $username;
		if (empty ( $username )) {
			$this->view->messages = array (tra ( 'Please provide a username' ) );
		} else {
			$myAdapter = & Ranchbe::getAuthAdapter ();
			$myAdapter->setIdentity ( $username )
					  ->setPassword ( $password );
			$auth = Zend_Auth::getInstance ();
			$result = $auth->authenticate ( $myAdapter );
			if ($result->isValid ()) {
				$auth->getStorage ()->write ( array (
										'user_id' => $myAdapter->getId (), 
										'user_name' => $username, 
										'email' => $myAdapter->getEmail (), 
										'lastlogin' => time () ) 
				);
				$this->_redirect($Zforward); //use redirect here and not forward
			} else {
				$this->view->messages = $result->getMessages ();
				$this->view->message = 'Please provide a username';
			}
		}
		$this->view->PageTitle = 'Login';
	} //End of method
	
	//------------------------------------------------------------------------------
	function logoutAction() {
		//var_dump($_SESSION);
		Zend_Auth::getInstance ()->clearIdentity ();
		foreach ( array_keys ( $_SESSION ) as $key ) {
			unset ( $_SESSION [$key] );
		}
		return $this->_forward('login','auth','auth');
	} //End of method

} //End of class
