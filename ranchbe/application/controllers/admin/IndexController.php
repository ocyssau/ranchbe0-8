<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


class Admin_IndexController extends controllers_abstract {
	protected $page_id = 'adminManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'admin', 'controller' => 'index', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'admin', 'controller' => 'index', 'action' => 'index' );
	
	//---------------------------------------------------------------------
	public function indexAction() {
		$this->_preparedisplay();
	} //End of method

	//---------------------------------------------------------------------
	protected function _preparedisplay() {
		// Display the template
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'adminTab' )->activate ();
		$this->view->PageTitle = 'System administration';
	} //End of method
	
} //End of class
