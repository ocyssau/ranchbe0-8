<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');
class Recordfile_IndexController extends controllers_abstract {
	protected $page_id = 'recordfileManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'content', 'controller' => 'get', 'action' => 'recordfile' );
	protected $ifFailedForward = array ('module' => 'content', 'controller' => 'get', 'action' => 'recordfile' );

	public function init() {
		$this->error_stack = & Ranchbe::getError();

		$this->space_name = $this->getRequest ()->getParam ( 'space' );
		$this->page_id = $this->space_name . $this->page_id;
		$this->ticket = $this->getRequest ()->getParam ( 'ticket' );

		//id of the version file
		$this->file_id = $this->getRequest ()->getParam ( 'file_id' );

		if (is_array ( $this->file_id )) {
			$this->file_ids = $this->file_id;
			$this->file_id = 0; //to get a not initialized container object
		} else if (! is_null ( $this->file_id )) {
			$this->file_id = ( int ) $this->file_id;
			$this->file_ids = array ($this->file_id );
		} else {
			$this->file_id = 0; //to get a not initialized container object
			$this->file_ids = array ();
		}
		$this->view->assign ( 'isVersion', true ); // to hide version get button
		$this->_initForward ();
	} //End of method


	//---------------------------------------------------------------------
	public function suppressAction() {
		if (! Ranchbe::checkPerm ( 'document_suppress', $this->container, false ))
		return $this->_cancel ();

		if (RbView_Flood::checkFlood ( $this->ticket ))
		foreach ( $this->file_ids as $file_id ) {
			Rb_Recordfile::get ( $this->space_name, $file_id )->removeFile ( true );
		}

		return $this->_successForward ();
	} //End of method


	//---------------------------------------------------------------------
	public function putinwsAction() {
		if (! Ranchbe::checkPerm ( 'document_get', $this->container, false ))
		return $this->_cancel ();

		if (RbView_Flood::checkFlood ( $this->ticket ))
		foreach ( $this->file_ids as $file_id ) {
			Rb_Recordfile::get ( $this->space_name, $file_id )->putFileInWildspace ();
		}

		return $this->_successForward ();
	} //End of method


} //End of class
