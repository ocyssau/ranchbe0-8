<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');
class Recordfile_DetailController extends controllers_abstract {
	protected $page_id = 'recordfiledetail'; //(string)
	protected $ifSuccessForward = array ('module' => 'recordfile', 'controller' => 'detail', 'action' => 'get' );
	protected $ifFailedForward = array ('module' => 'recordfile', 'controller' => 'detail', 'action' => 'get' );
	
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->space_name = $this->getRequest ()->getParam ( 'space' );
		$this->ticket = $this->view->get_template_vars ( 'ticket' );
		$this->file_id = $this->getRequest ()->getParam ( 'file_id' );
		$this->_initForward ();
	} //End of method
	
	
	function indexAction() {
		return $this->getAction ();
	} //End of method
	
	
	function getAction() {
		$this->view->fileinfos = Rb_Recordfile::get ( $this->space_name, $this->file_id )->getProperties ();
		$container_id = Rb_Recordfile::get ( $this->space_name, $this->file_id )->getProperty ('container_id');

		if (! Ranchbe::checkPerm ( 'document_get', 
			Rb_Container::get ( $this->space_name, $container_id, false ) ) 
			){
			$this->error_stack->checkErrors ();
			return;
		}
		$this->error_stack->checkErrors ();
		$this->view->SPACE_NAME = $this->space_name;
		$this->view->file_id = $this->file_id;
		$this->view->sameurl = './recordfile/detail/get';
	} //End of method
	
	
} //End of class
