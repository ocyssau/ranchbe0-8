<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


abstract class controllers_history_abstract extends controllers_abstract {
	protected $history; //object
	protected $object; //object of class basic
	protected $object_id; //(int)
	protected $page_id = 'history'; //(string)
	protected $ifSuccessForward = array ('controller' => 'history', 'action' => 'index' );
	protected $ifFailedForward = array ('controller' => 'history', 'action' => 'index' );
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->ticket = $this->view->ticket;
		$this->object_id = $this->getRequest ()->getParam ( 'object_id' );
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function indexAction() {
		$this->getAction ();
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function getAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		Ranchbe::checkPerm ( 'get', $this->object, true );
		$search = new Rb_Search_Db ( $this->history->getDao () );
		$filter = new RbView_Helper_Histofilter ( $this->getRequest (), $search, $this->page_id );
		
		$url = './' . $this->ifSuccessForward ['module'] . '/history/get/space/' . $this->space_name;
		if ( $this->object_id ) {
			$url .= '/object_id/'.$this->object_id;
		}
		$this->view->sameurl = $url;
		$filter->setUrl ( $url );
		$filter->setDefault ( 'sort_field', 'histo_order' );
		$filter->setDefault ( 'sort_order', 'DESC' );
		$filter->setFindElements ( array (
			'action_name' => tra ( 'action_name' ), 
			'action_by' => tra ( 'action_by' ), 
			$this->object->getDao ()->getFieldName ( 'number' ) => tra ( 'Number' ), 
			$this->object->getDao ()->getFieldName ( 'description' ) => tra ( 'Description' ), 
			'open_by' => tra ( 'open_by' ), 
			'close_by' => tra ( 'close_by' ), 
			$this->object->getDao ()->getFieldName ( 'state' ) => tra ( 'State' ), 
			$this->object->getDao ()->getFieldName ( 'indice' ) => tra ( 'Indice' ) 
		) );
		$filter->finish ();
		
		if (! empty ( $this->object_id )) {
			$this->view->list = $this->history->getHistory ( $this->object_id, $filter->getParams () );
			$this->view->object_id = $this->object_id;
		} else {
			$this->view->list = $this->history->getAllHistory ( $filter->getParams () );
		}
		
		$pagination = new RbView_Helper_Pagination($filter);
	    $pagination->setPagination( count($this->view->list) );
	    $pagination->setRequest( $this->getRequest() );
	    $this->view->views_helper_pagination = $pagination->fetchForm( $this->view->getEngine () );
	    
		$this->error_stack->checkErrors ();
		
		// Active the tab
		// Display the template
		//Assign name to particular fields
		$this->view->assign ( 'number_map', $this->object->getDao ()->getFieldName ( 'number' ) );
		$this->view->assign ( 'id_map', $this->object->getDao ()->getFieldName ( 'id' ) );
		$this->view->assign ( 'description_map', $this->object->getDao ()->getFieldName ( 'description' ) );
		$this->view->assign ( 'state_map', $this->object->getDao ()->getFieldName ( 'state' ) );
		$this->view->assign ( 'indice_map', $this->object->getDao ()->getFieldName ( 'indice' ) );
		$this->view->assign ( 'version_map', $this->object->getDao ()->getFieldName ( 'version' ) );
		$this->view->assign ( 'father_map_id', $this->object->getDao ()->getFieldName ( 'father' ) );
		$this->view->assign ( 'SPACE_NAME', $this->object->getSpaceName () );
		$this->view->assign ( 'CONTAINER_NUMBER', $this->object->getSpaceName () . '_number' );
		$this->view->assign ( 'CONTAINER_DESCRIPTION', $this->object->getSpaceName () . '_description' );
		$this->view->assign ( 'CONTAINER_STATE', $this->object->getSpaceName () . '_state' );
		$this->view->assign ( 'CONTAINER_ID', $this->object->getSpaceName () . '_id' );
		$this->view->assign ( 'samemodule', $this->ifSuccessForward ['module'] );
		$this->view->assign ( 'views_helper_filter_form', $filter->fetchForm ( $this->view->getEngine () ) ); //generate code for the filter form
		$this->render ( 'history/get', null, true );
		return;
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function suppressAction() {
		if (! Ranchbe::checkPerm ( 'admin', $this->object, false )) {
			return $this->_cancel ();
		}
		$histo_order = $this->getRequest ()->getParam ( 'histo_order' );
		if (! is_array ( $histo_order ))
			$histo_order = array ($histo_order );
		foreach ( $histo_order as $id )
			$this->history->suppressHistory ( $id );
		$this->indexAction ();
	} //End of method
	
} //End of class

