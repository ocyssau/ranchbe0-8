<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

class Project_HistoryController extends controllers_history_abstract{
  protected $page_id='projectHistory'; //(string)
  protected $ifSuccessForward = array('module'=>'project','controller'=>'history','action'=>'index');
  protected $ifFailedForward = array('module'=>'project','controller'=>'history','action'=>'index');

public function init(){
  parent::init();
  //$this->check_flood = check_flood($_REQUEST['page_id']);

  $this->object = Rb_Project::get();
  $this->history = new Rb_History($this->object);

  if(!$this->object_id)
    $this->object_id = $this->getRequest()->getParam('project_id');

} //End of method

} //End of class
