<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

abstract class controllers_project_abstract extends controllers_abstract{
  protected $project; //object
  protected $project_id; //(int)
  protected $page_id; //(string)
  protected $ticket=''; //(String)
  protected $ifSuccessForward = array('module'=>'projectManager');
  protected $ifFailedForward = array('module'=>'projectManager');

public function init(){
  $this->error_stack =& Ranchbe::getError();
  $this->_initForward();
  $this->ticket = $this->getRequest()->getParam('ticket');

  $this->project_id = $this->getRequest()->getParam('project_id');
  if(is_array($this->project_id)){
    $this->project_ids = $this->project_id;
    $this->project_id  = -1; //to get a not initialized container object
  }else{
    $this->project_ids = array($this->project_id);
  }
  if($this->project_id)
    $this->view->project_id = $this->project_id;
} //End of method

} //End of class
