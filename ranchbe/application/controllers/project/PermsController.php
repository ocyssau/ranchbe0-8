<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

class Project_PermsController extends controllers_perms_abstract{
	protected $project; //object
	protected $page_id='projectPerms'; //(string)
	protected $ifSuccessForward = array('module'=>'project','controller'=>'perms','action'=>'index');
	protected $ifFailedForward = array('module'=>'project','controller'=>'perms','action'=>'index');

	public function init(){
		$this->error_stack =& Ranchbe::getError();

		$this->_initForward();

		$this->ticket = $this->view->ticket;
		$this->space_name = $this->getRequest()->getParam('space');

		$this->project_id = $this->getRequest()->getParam('project_id');
		if(is_array($this->project_id)){
			$this->project_ids = $this->project_id;
			$this->project_id  = 0; //to get a not initialized container object
		}else{
			$this->project_ids = array($this->project_id);
		}

		$this->project = Rb_Project::get($this->project_id);
		$this->space =& $this->project->space;

		if($this->project_id){
			$this->view->assign('project_id', $this->project_id);
		}

		$this->object_id = $this->getRequest()->getParam('object_id');

		$this->project = Rb_Project::get($this->object_id);

		$this->role_id = $this->getRequest()->getParam('role_id');
		$this->resource_id = $this->getRequest()->getParam('resource_id');
		if( empty($this->resource_id) ){
			$this->resource_id = Rb_Project::getStaticResourceId( $this->object_id );
		}

		if(empty($this->resource_id)) {
			print 'none ressource<br>';die;
		}
		if($this->object_id){
			$this->view->resource_name = $this->project->getNumber();
		}else{
			$this->view->resource_name = tra('Projects');
		}
		
		$this->view->samecontroller = './project/perms'; //important: is first assign
	} //End of method

} //End of class
