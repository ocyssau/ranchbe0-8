<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+





class Project_IndexController extends controllers_project_abstract {
	protected $page_id = 'projectManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'project', 
										'controller' => 'index', 
										'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'project', 
										'controller' => 'index', 
										'action' => 'index' );
	
	//------------------------------------------------------------------------------
	protected function _getForm() {
		$this->view->dojo()->enable();
		
		//$view->addHelperPath("ZendX/JQuery/View/Helper", "ZendX_JQuery_View_Helper");
		$form = new ZendRb_Form();
		//$this->view->headScript()->appendFile(Ranchbe::getConfig()->js->baseurl.'qfamsHandler.js');
		
		$form->addElement('ValidationTextBox','project_number', array(
				'label' => tra ( 'Number' ),
				'size' =>50,
				'value' => '',
				'required' => true,
				'propercase' => true,
				'regExp' => Ranchbe::getConfig ()->project->mask,
				'invalidMessage' => utf8_encode(Ranchbe::getConfig ()->project->maskhelp),
				'filters' => array('StringTrim', 'StringToUpper'),
				)
		);
		
		$form->addElement('Textarea', 'project_description', array(
					'label' => tra ( 'Description' ),
					'value' => '',
					'trim'  => true,
					'required' => true,
					'style' => 'width: 400px;',
					'filters' => array('StringTrim', 'StringToLower'),
					'validators' => array(
									    array('NotEmpty', true),
									    array('stringLength', false, array(0,255)),
									),
					));				
		
		$form->addElement('DateTextBox','forseen_close_date',array(
                    'label' => tra('forseen_close_date'),
                    'required'  => true,
					)
		);
		
		$form->addElement('hidden', 'ticket', array('value'=>RbView_Flood::getTicket() ));
		
		return $form;
		
	} //End of method
	
	//------------------------------------------------------------------------------
	protected function _create($values) {
		$this->project = new Rb_Project ();
		if (! is_numeric ( $values ['forseen_close_date'] ))
			$values ['forseen_close_date'] = Rb_Date::makeTimestamp ( $values ['forseen_close_date'] );
		foreach ( $values as $property => $value ) {
			$this->project->setProperty ( $property, $value );
		}
		return $this->project->save ();
	} //End of method
	
	//------------------------------------------------------------------------------
	protected function _edit($values) {
		if (! is_numeric ( $values ['forseen_close_date'] ))
			$values ['forseen_close_date'] = Rb_Date::makeTimestamp ( $values ['forseen_close_date'] );
		foreach ( $values as $property => $value ) {
			$this->project->setProperty ( $property, $value );
		}
		return $this->project->save ();
	} //End of method
	
	
	
	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward ( 'get' );
	} //End of method
	
	//---------------------------------------------------------------------
	public function getAction() {
		if (! Ranchbe::checkPerm ( 'get', Rb_Project::get (), false )) {
			$this->error_stack->checkErrors ();
			return $this->_forward ( 'index', 'index', 'index' );
		}
		
		$this->view->dojo()->enable();

//echo get_include_path();die;


		$search = new Rb_Search_Db ( Rb_Project::get ()->getDao () );
		$filter = new RbView_Helper_Projectfilter ( $this->getRequest (), $search, $this->page_id );
		$filter->setTemplate ( 'project/searchBar.tpl' );
		$filter->setUrl ( $this->view->baseUrl ( '/project/index/get' ) );
		$filter->setDefault ( 'sort_field', 'project_number' );
		$filter->setDefault ( 'sort_order', 'ASC' );
		$filter->setDateElements ( array ('open_date', 'close_date', 'forseen_close_date' ) );
		$filter->setUserElements ( array ('open_by', 'close_by' ) );
		$filter->setFindElements ( array (
						'project_number' => tra ( 'project_number' ),
						'project_description' => tra ( 'project_description' ),
						'project_state' => tra ( 'State' ) ) );
		$filter->finish ();
		
		//get all projects
		$this->view->list = Rb_Project::get ()->getAll ( $filter->getParams () );
		
// convert a complex value to JSON notation
		$pagination = new RbView_Helper_Pagination($filter);
	    $pagination->setPagination( count($this->view->list) );
	    $pagination->setRequest( $this->getRequest() );
	    $this->view->views_helper_pagination = $pagination->fetchForm( $this->view->getEngine () );
				
		$this->error_stack->checkErrors();

		$this->_helper->actionStack ( 'toolbar' );
		
		// Display the template
		RbView_Menu::get ();
		RbView_Tab::get ( 'projectsTab' )->activate ();
		$this->view->PageTitle = 'Project manager';
		$this->view->views_helper_filter_form = $filter->fetchForm ( $this->view->getEngine () );
	} //End of method
	
	//---------------------------------------------------------------------
	public function createAction() {
		Ranchbe::checkPerm ( 'create', Rb_Project::get ( $this->space_name, - 1 ), true );
		
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->view->PageTitle = tra ( 'Create a project' );
		$this->view->form = $this->_getForm();
		$this->view->form->setAction( $this->_helper->url('create') );
		
		$this->view->form->setDefaults(array (
			'project_description' => 'New project', 
			'forseen_close_date' => Rb_Date::formatDate( time () + Ranchbe::getConfig()->date->object->lifetime, 'dojo' ),
		));
		
		if ( $this->getRequest ()->isPost() ) {
			if ($this->view->form->isValid($this->getRequest ()->getPost())) {
				if (!RbView_Flood::checkFlood ( $this->ticket )) {return false;}
				if( $this->_create($this->view->form->getValues()) ){
					$this->view->successMessage = tra('Save is successfull');
					$this->view->form = null;
				}else{
					$this->view->failedMessage = tra('Save has failed');
				}
			} else {
				$this->view->failedMessage = tra('Save has failed');
			}
		}
		Ranchbe::getError()->checkErrors ();
		$this->render ( 'default/editform', null, true );
		return;
	} //End of method
	
	//---------------------------------------------------------------------
	public function editAction() {
		$this->project = & Rb_Project::get ( $this->project_id );
		Ranchbe::checkPerm ( 'edit', $this->project, true );
		
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->view->PageTitle = sprintf(tra ( 'Edit project %s' ), $this->project->getName() );
		$this->view->form = $this->_getForm();
		$this->view->form->setAction( $this->_helper->url('edit') );
		
		$this->view->form->setDefaults(array (
			'project_id' => $this->project_id,
			'project_number' => $this->project->getProperty('project_number'), 
			'project_description' => $this->project->getProperty('project_description'), 
			'forseen_close_date' => Rb_Date::formatDate($this->project->getProperty ( 'forseen_close_date' ) , 'dojo' ),
		));
		
		//Add hidden fields
		$this->view->form->addElement ( 'hidden', 'project_id', array('value'=>$this->project_id ) );
		
		if ( $this->getRequest ()->isPost() ) {
			if ($this->view->form->isValid($this->getRequest ()->getPost())) {
				if (!RbView_Flood::checkFlood ( $this->ticket )) {return false;}
				if( $this->_edit($this->view->form->getValues()) ){
					$this->view->successMessage = tra('Save is successfull');
					$this->view->form = null;
				}else{
					$this->view->failedMessage = tra('Save has failed');
				}
			} else {
				$this->view->failedMessage = tra('Save has failed');
			}
		}
		Ranchbe::getError()->checkErrors ();
		$this->render ( 'default/editform', null, true );
		return;
	} //End of method
	
	//---------------------------------------------------------------------
	public function suppressAction() {
		if (RbView_Flood::checkFlood ( $this->ticket ))
			foreach ( $this->project_ids as $project_id ) {
				$project = & Rb_Project::get ( $project_id );
				if (! Ranchbe::checkPerm ( 'suppress', $project, false ))
					continue;
				else
					$project->suppress ();
			}
		return $this->_successForward ();
	} //End of method
	

	public function getmenuAction() {
		//$this->_helper->removeHelper('viewRenderer');

		
		$this->view->projectid = $this->getRequest()->get('project_id');
		$this->_helper->layout()->disableLayout();
        $this->view->dojo()->enable();
		$data = "
		    <div dojoType='dijit.PopupMenuItem' iconClass='dijitEditorIcon dijitEditorIconCopy'>
		      <span>Documents</span>
		      <div dojoType='dijit.Menu'>
		        <div dojoType='dijit.MenuItem' onClick=\"javascript:popupP('./project/links/getdoctypes/project_id/".$this->getRequest()->getPost('project_id')."/space/project/ticket/".$this->ticket."',900,450,'Doctypes')'>
		        <img border='0' alt='' src='img/icons/link_edit.png' />".tra("Doctypes")."
		        </div>
		        <div dojoType='dijit.MenuItem' onClick='javascript:popupP('./project/links/getmockups/project_id/".$this->getRequest()->getPost('project_id')."/space/project/ticket/".$this->ticket."',900,450,'Mockups')' >
		        <img border='0' alt='' src='img/icons/link_edit.png' />".tra("Mockups")."
		        </div>
		        <div dojoType='dijit.MenuItem' onClick='javascript:popupP('./project/links/getcadlibs/project_id/".$this->getRequest()->getPost('project_id')."/space/project/ticket/".$this->ticket."',900,450,'Mockups')' >
		        <img border='0' src='img/icons/link_edit.png' />".tra("Cadlibs")."
		        </div>
		        <div dojoType='dijit.MenuItem' onClick='javascript:popupP('./project/links/getbookshops/project_id/".$this->getRequest()->getPost('project_id')."/space/project/ticket/".$this->ticket."',900,450,'Mockups')' >
		        <img border='0' alt='' src='img/icons/link_edit.png' />".tra("Bookshops")."
		        </div>
		      </div>
		    </div>
		    <div dojoType='dijit.MenuSeparator'></div>
			<div dojoType='dijit.MenuItem'  onClick='javascript:popupP('project/perms/get/object_id/".$this->getRequest()->getPost('project_id')."','projectPerms',550,650 )' >
		        <img border='0' alt='' src='img/icons/key.png' />".tra("permissions")."
		    </div>
		    <div dojoType='dijit.MenuItem'  onClick='project/index/suppress/project_id/".$this->getRequest()->getPost('project_id')."/ticket/".$this->ticket."'>
		        <img border='0' alt='' src='img/icons/trash.png' />".tra("Suppress project")."
		    </div>
		    <div dojoType='dijit.MenuItem'  onClick='javascript:popup('project/index/edit/project_id/".$this->getRequest()->getPost('project_id')."','editproject')'>
		        <img border='0' alt='".tra("edit project")."' src='img/icons/edit.png' />".tra("edit")."
		    </div>
		    <div dojoType='dijit.MenuSeparator'></div>
		    <div dojoType='dijit.MenuItem' onClick='javascript:popupP('project/history/get/project_id/".$this->getRequest()->getPost('project_id')."','projetHistory', 1180, 1180)'>
		        <img border='0' alt='' src='img/icons/history.png' />".tra("Get history")."
		    </div>
		";
		//echo $data;
	} //End of method
	
} //End of class



