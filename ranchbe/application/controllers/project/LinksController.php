<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


class Project_LinksController extends controllers_project_abstract {
	protected $page_id = 'projectLinks'; //(string)
	protected $ifSuccessForward = array ('module' => 'project', 'controller' => 'links', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'project', 'controller' => 'links', 'action' => 'index' );
	
	public function init() {
		parent::init ();
		$this->project = & Rb_Project::get ( $this->project_id );
		$this->view->sameModule = $this->ifSuccessForward ['module'];
		$this->view->sameController = $this->ifSuccessForward ['controller'];
		$this->view->sameAction = $this->ifSuccessForward ['action'];
		//scripts for quickform_advmultiselect
    	$this->view->headScript()->appendFile(Ranchbe::getConfig()->js->baseurl.'qfamsHandler.js');
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
	} //End of method
	

	//---------------------------------------------------------------------
	public function getdoctypesAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		if (! empty ( $_REQUEST ['sort_field'] )) {
			$params ['sort_field'] = $_REQUEST ['sort_field'];
			$params ['sort_order'] = $_REQUEST ['sort_order'];
		}
		$this->view->list = Rb_Project_Relation_Doctype::get ()->getDoctypes ( NULL, NULL, $this->project_id, $params );
		$this->error_stack->checkErrors ();
		$this->view->assign ( 'PageTitle', vsprintf ( tra ( 'Linked doctypes to %s' ), array ($this->project->getNumber () ) ) );
		$this->view->assign('sameurl', './project/links/getdoctypes');
		//$this->_helper->viewRenderer->setScriptAction ( 'get' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function linkdoctypeAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_helper->viewRenderer->setNoRender(true);
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$doctypes_ids = $this->getRequest ()->getParam ( 'doctype_id' );
		
		$this->ifSuccessForward ['action'] = 'getdoctypes';
		$this->ifFailedForward ['action'] = 'getdoctypes';
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		if (! Ranchbe::checkPerm ( 'admin', $this->project, false ))
			return $this->_cancel ();
			
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'selectDoctype', 'post', 
						$this->view->baseUrl ( 'project/links/linkdoctype' ) );
		$form->addElement ( 'header', 'title1', tra ( 'Doctypes list' ) );
		$form->addElement ( 'hidden', 'project_id', $this->project_id );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', $this->view->ticket);
		$form->addElement ( 'hidden', 'ifSuccessAction', 
										$this->ifSuccessForward ['action'] );
		
		//Get the current doctypes
		//$_default = $this->project->getDoctype()->getDoctypes(NULL , NULL, $params);
		$relation = new Rb_Project_Relation_Doctype ( - 1 );
		$_default = $relation->getDoctypes ( NULL, NULL, $this->project_id, array () );
		$default = array ();
		if (is_array ( $_default ))
			foreach ( $_default as $d ) {
				$default [] = $d ['doctype_id'];
			}
		unset ( $_default );
		$p = array (
			'property_name' => 'doctype_id', 
			'property_description' => tra ( 'Select doctype to add' ), 
			'default_value' => $default, 
			'is_multiple' => true, 
			'return_name' => false, 
			'property_length' => 5, 
			'adv_select' => true );
		construct_select_doctype ( $p, $form, Rb_Doctype::get ( 0 ) );
		
		if (isset ( $validate )) {
			if ($form->validate ()) { //Validate the form...
				//if(!$check_flood) break;
				//Add the property if necessary
				if (is_array ( $doctypes_ids ))
					foreach ( $doctypes_ids as $doctype_id ) {
						if (is_array ( $default )) { //Previous selection exist
							if (! in_array ( $doctype_id, $default )) {
								//$this->project->getDoctype()->addLink( $doctype_id );
								$link = new Rb_Object_Relation ( 0 );
								$link->setParent ( $this->project_id );
								$link->setChild ( $doctype_id );
								$link->save ();
							}
						} else { //there is no previous selection, add all
							//$this->project->getDoctype()->addLink( $doctype_id );
							$link = new Rb_Object_Relation ( 0 );
							$link->setParent ( $this->project_id );
							$link->setChild ( $doctype_id );
							$link->save ();
						}
					}
					//Suppress the properties if necessary
				if (is_array ( $default ))
					foreach ( $default as $doctypes_id ) {
						if (is_array ( $doctypes_ids )) { //Selection exist
							if (! in_array ( $doctypes_id, $doctypes_ids )) {
								//$this->project->getDoctype()->SuppressDoctype( $doctypes_id, $this->project_id );
								$relation->suppressLink ( $this->project_id, $doctypes_id );
							}
						} else { //its a empty selection, suppress all
							//$this->project->getDoctype()->SuppressDoctype( $doctypes_id, $this->project_id );
							$relation->suppressLink ( $this->project_id, $doctypes_id );
						}
					}
			}
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			return $form->display ();
		}
		return $this->_successForward ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function unlinkdoctypeAction() {
		
		$this->ifSuccessForward ['action'] = 'getdoctypes';
		$this->ifFailedForward ['action'] = 'getdoctypes';
		
		if (! Ranchbe::checkPerm ( 'admin', $this->project, false ))
			return $this->_cancel ();
		
		$link_ids = $this->getRequest ()->getParam ( 'link_id' );
		if (empty ( $link_ids )) {
			$this->error_stack->push ( Rb_Error::WARNING, array (), tra ( 'You must select at least one item' ) );
			return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		}
		
		foreach ( $link_ids as $link_id ) {
			//$this->project->getDoctype()->suppressLink($link_id);
			$link = new Rb_Object_Relation ( $link_id );
			$link->suppress ();
		}
		$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
	} //End of method
	

	//---------------------------------------------------------------------
	public function linkprocessAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_helper->viewRenderer->setNoRender(true);
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$link_ids = $this->getRequest ()->getParam ( 'link_id' );
		
		$this->ifSuccessForward ['action'] = 'getdoctypes';
		$this->ifFailedForward ['action'] = 'getdoctypes';
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		if (! Ranchbe::checkPerm ( 'admin', $this->project, false ))
			return $this->_cancel ();
		
		if (empty ( $link_ids )) {
			$this->error_stack->push ( Rb_Error::WARNING, array (), tra ( 'You must select at least one item' ) );
			return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		}
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'selectProcess', 'post', $this->actionUrl ( 'linkprocess' ) );
		$form->addElement ( 'header', 'title1', tra ( 'Process list' ) );
		
		$form->addElement ( 'hidden', 'project_id', $this->project_id );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', $this->view->ticket );
		$form->addElement ( 'hidden', 'ifSuccessAction', $this->ifSuccessForward ['action'] );
		foreach ( $link_ids as $link_id ) {
			$form->addElement ( 'hidden', 'link_id[]', $link_id );
		}
		
		//Get list
		$p = array ('property_name' => 'process_id', 'property_description' => tra ( 'Select process to add' ), 'is_multiple' => false, 'return_name' => false, 'property_length' => 5, 'adv_select' => false );
		construct_select_process ( $p, $form );
		
		if (isset ( $validate )) {
			$process_id = $this->getRequest ()->getParam ( 'process_id' );
			if ($form->validate ()) { //Validate the form...
				//if(!$check_flood) break;
				foreach ( $link_ids as $link_id ) {
					//$this->project->getDoctype()->modifyLink($link_id, array('process_id'=>$process_id ) );
					$link = new Rb_Object_Relation ( $link_id );
					$link->setExtend ( $process_id );
					$link->save ();
				}
			}
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			return $form->display ();
		}
		return $this->_successForward ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function unlinkprocessAction() {
		//if(!$check_flood) {$_REQUEST['action'] = 'getDoctypeLinks'; break;}
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$link_ids = $this->getRequest ()->getParam ( 'link_id' );
		
		$this->ifSuccessForward ['action'] = 'getdoctypes';
		$this->ifFailedForward ['action'] = 'getdoctypes';
		
		if (! Ranchbe::checkPerm ( 'admin', $this->project, false ))
			return $this->_cancel ();
		
		if (empty ( $link_ids )) {
			$this->error_stack->push ( Rb_Error::WARNING, array (), tra ( 'You must select at least one item' ) );
			return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		}
		
		foreach ( $link_ids as $link_id ) {
			//$this->project->getDoctype()->modifyLink($link_id, array('process_id'=>null) );
			$link = new Rb_Object_Relation ( $link_id );
			$link->setExtend ( null );
			$link->save ();
		}
		return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
	} //End of method
	

	//---------------------------------------------------------------------
	public function getmockupsAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$params = array ();
		if (! empty ( $_REQUEST ['sort_field'] )) {
			$params ['sort_field'] = $_REQUEST ['sort_field'];
			$params ['sort_order'] = $_REQUEST ['sort_order'];
		}
		//$list = $this->project->getContainerLink('mockup')->getContainers( $params );
		$relation = new Rb_Project_Relation_Container ( - 1 );
		$this->view->list = $relation->getMockups ( $this->project_id, $params );
		$this->error_stack->checkErrors ();
		//Assign name to particular fields
		$this->view->assign ( 'SPACE_NAME', 'mockup' );
		$this->view->assign ( 'CONTAINER_NUMBER', 'mockup_number' );
		$this->view->assign ( 'CONTAINER_DESCRIPTION', 'mockup_description' );
		$this->view->assign ( 'PageTitle', vsprintf ( tra ( 'Linked mockups to %s' ), array ($this->project->getNumber () ) ) );
		$this->view->assign ( 'sameurl', './project/links/getmockups' );
		$this->_helper->viewRenderer->setScriptAction ( 'getcontainers' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function getcadlibsAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$params = array ();
		if (! empty ( $_REQUEST ['sort_field'] )) {
			$params ['sort_field'] = $_REQUEST ['sort_field'];
			$params ['sort_order'] = $_REQUEST ['sort_order'];
		}
		//$list = $this->project->getContainerLink('cadlib')->getContainers( $params );
		$relation = new Rb_Project_Relation_Container ( - 1 );
		$this->view->list = $relation->getCadlibs ( $this->project_id, $params );
		$this->error_stack->checkErrors ();
		//Assign name to particular fields
		$this->view->assign ( 'SPACE_NAME', 'cadlib' );
		$this->view->assign ( 'CONTAINER_NUMBER', 'cadlib_number' );
		$this->view->assign ( 'CONTAINER_DESCRIPTION', 'cadlib_description' );
		$this->view->assign ( 'PageTitle', vsprintf ( tra ( 'Linked cadlibs to %s' ), array ($this->project->getNumber () ) ) );
		$this->view->assign ( 'sameurl', './project/links/getcadlibs' );
		$this->_helper->viewRenderer->setScriptAction ( 'getcontainers' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function getbookshopsAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$params = array ();
		if (! empty ( $_REQUEST ['sort_field'] )) {
			$params ['sort_field'] = $_REQUEST ['sort_field'];
			$params ['sort_order'] = $_REQUEST ['sort_order'];
		}
		//$list = $this->project->getContainerLink('bookshop')->getContainers( $params );
		$relation = new Rb_Project_Relation_Container ( - 1 );
		$this->view->list = $relation->getBookshops ( $this->project_id, $params );
		$this->error_stack->checkErrors ();
		//Assign name to particular fields
		$this->view->assign ( 'SPACE_NAME', 'bookshop' );
		$this->view->assign ( 'CONTAINER_NUMBER', 'bookshop_number' );
		$this->view->assign ( 'CONTAINER_DESCRIPTION', 'bookshop_description' );
		$this->view->assign ( 'PageTitle', vsprintf ( tra ( 'Linked bookshops to %s' ), array ($this->project->getNumber () ) ) );
		$this->view->assign ( 'sameurl', './project/links/getbookshops' );
		$this->_helper->viewRenderer->setScriptAction ( 'getcontainers' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function linkcontainerAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_helper->viewRenderer->setNoRender(true);
		$actionUrl = 'linkcontainer';
		$space_name = $this->getRequest ()->getParam ( 'space' );
		switch ($space_name) {
			case 'bookshop' :
				$title = tra ( 'Bookshop list' );
				$this->ifSuccessForward ['action'] = 'getbookshops';
				$this->ifFailedForward ['action'] = 'getbookshops';
				break;
			case 'cadlib' :
				$title = tra ( 'Cadlib list' );
				$this->ifSuccessForward ['action'] = 'getcadlibs';
				$this->ifFailedForward ['action'] = 'getcadlibs';
				break;
			case 'mockup' :
				$title = tra ( 'Mockup list' );
				$this->ifSuccessForward ['action'] = 'getmockups';
				$this->ifFailedForward ['action'] = 'getmockups';
				break;
		}
		if (! Ranchbe::checkPerm ( 'admin', $this->project, false ))
			return $this->_cancel ();
		return $this->_linkcontainer ( $actionUrl, $title, $space_name );
	} //End of method
	

	//---------------------------------------------------------------------
	public function unlinkcontainerAction() {
		$space_name = $this->getRequest ()->getParam ( 'space' );
		switch ($space_name) {
			case 'bookshop' :
				$this->ifSuccessForward ['action'] = 'getbookshops';
				$this->ifFailedForward ['action'] = 'getbookshops';
				break;
			case 'cadlib' :
				$this->ifSuccessForward ['action'] = 'getcadlibs';
				$this->ifFailedForward ['action'] = 'getcadlibs';
				break;
			case 'mockup' :
				$this->ifSuccessForward ['action'] = 'getmockups';
				$this->ifFailedForward ['action'] = 'getmockups';
				break;
		}
		if (! Ranchbe::checkPerm ( 'admin', $this->project, false ))
			return $this->_cancel ();
		return $this->_unlinkcontainer ( $space_name );
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _unlinkcontainer($space_name) {
		$link_ids = $this->getRequest ()->getParam ( 'link_id' );
		if (empty ( $link_ids )) {
			$this->error_stack->push ( Rb_Error::WARNING, array (), tra ( 'You must select at least one item' ) );
			return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		}
		foreach ( $link_ids as $link_id ) {
			//$this->project->getContainerLink($space_name)->suppressLink($link_id);
			$link = new Rb_Object_Relation ( $link_id );
			$link->suppress ();
		}
		$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _linkcontainer($actionUrl, $title, $space_name) {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$container_ids = $this->getRequest ()->getParam ( 'container_ids' );
		
		$container = & Rb_Container::get ( $space_name );
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'selectContainer', 'post', $this->actionUrl ( $actionUrl ) );
		$form->addElement ( 'header', 'title1', $title );
		
		$form->addElement ( 'hidden', 'project_id', $this->project_id );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', $this->view->ticket );
		//add this param to return to correct action when cancel is request
		$form->addElement ( 'hidden', 'ifSuccessAction', $this->ifSuccessForward ['action'] );
		
		//Get the current containers
		$relation = new Rb_Project_Relation_Container ( - 1 );
		//$_default = $this->project->getContainerLink($space_name)->getContainers();
		switch ($this->space_name) {
			case 'cadlib' :
				$_default = getCadlibs ( $this->project_id, $params );
				break;
			case 'bookshop' :
				$_default = getBookshops ( $this->project_id, $params );
				break;
			case 'mockup' :
				$_default = getMockups ( $this->project_id, $params );
				break;
			case 'workitem' :
				$_default = getWorkitems ( $this->project_id, $params );
				break;
		}
		$default = array ();
		if (is_array ( $_default ))
			foreach ( $_default as $d ) {
				$default [] = $d [$container->getFieldName ( 'id' )];
			}
		unset ( $_default );
		$p = array ('property_name' => 'container_ids', 'property_description' => tra ( 'Select bookshop to add' ), 'default_value' => $default, 'is_multiple' => true, 'return_name' => false, 'property_length' => 5, 'adv_select' => true );
		construct_select_container ( $p, $form, $container );
		
		if (isset ( $validate )) {
			if ($form->validate ()) { //Validate the form...
				//Add the links if necessary
				if (is_array ( $container_ids ))
					foreach ( $container_ids as $container_id ) {
						if (is_array ( $default )) { //Previous selection exist
							if (! in_array ( $container_id, $default )) {
								//$this->project->getContainerLink($space_name)->addLink( $container_id );
								$link = new Rb_Object_Relation ( 0 );
								$link->setParent ( $this->project_id );
								$link->setChild ( $container_id );
								$link->save ();
							}
						} else { //there is no previous selection, add all
							//$this->project->getContainerLink($space_name)->addLink( $container_id );
							$link = new Rb_Object_Relation ( 0 );
							$link->setParent ( $this->project_id );
							$link->setChild ( $container_id );
							$link->save ();
						}
					}
				//Suppress the links if necessary
				if (is_array ( $default ))
					foreach ( $default as $container_id ) {
						if (is_array ( $container_ids )) { //Selection exist
							if (! in_array ( $container_id, $container_ids )) {
								//$this->project->getContainerLink($space_name)->suppress( $container_id );
								$relation->suppressLink ( $this->project_id, $container_id );
							}
						} else { //its a empty selection, suppress all
							//$this->project->getContainerLink($space_name)->suppress( $container_id );
							$relation->suppressLink ( $this->project_id, $container_id );
						}
					}
			}
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			return $form->display ();
		}
		$this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
	} //End of method

} //End of class
