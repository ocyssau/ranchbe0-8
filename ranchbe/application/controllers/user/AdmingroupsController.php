<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


//@todo : changer controllers_perms_abstract herite de controllers_projectManager_abstract


require_once ('controllers/perms/abstract.php');
class User_AdmingroupsController extends controllers_perms_abstract {
	protected $page_id = 'userManager_admingroups'; //(string)
	protected $ifSuccessForward = array ('module' => 'user', 
					'controller' => 'admingroups', 'action' => 'get' );
	protected $ifFailedForward = array ('module' => 'user', 
					'controller' => 'admingroups', 'action' => 'get' );
	
	//--------------------------------------------------------------------
	public function init() {
		parent::init ();
		$this->error_stack = & Ranchbe::getError();
		$this->_initForward ();
	} //End of method
	

	//--------------------------------------------------------------------
	public function indexAction() {
		return $this->getAction ();
	} //End of method
	

	//--------------------------------------------------------------------
	public function getAction() {
		if (Ranchbe::checkPerm ( 'admin_users', Ranchbe::getAcl ()->getRootResource (), false )) {
			$this->view->groups = $this->_getGroups ();
		}
		// Display the template
		$this->error_stack->checkErrors ();
		// Active the tab
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'usersTab' )->activate ();
		$this->view->assign ( 'sameurl', './user/admingroups/get' ); //important: is first assign
		//$this->view->assign('views_helper_filter_form' , $filter->fetchForm($this->view, $this->container) ); //generate code for the filter form
		//$this->view->assign('views_helper_pagination' , $pagination->fetchForm($this->view) ); //generate code for the filter form
		$this->_helper->actionStack ( 'toolbar' );
	} //End of method
	
	//--------------------------------------------------------------------
	// Process the form to add a group here
	public function createAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		if (! Ranchbe::checkPerm ( 'admin_users', Ranchbe::getAcl ()->getRootResource (), false ))
			return $this->_cancel ();
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'createUser', 'post', $this->actionUrl ( 'create' ) );
		$form->addElement ( 'header', 'editheader', tra ( 'Create a group' ) );
		// Active the tab
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'usersTab' )->activate ();
		$this->_helper->actionStack ( 'toolbar' );
		return $this->_finishEditForm ( $form, array (), 'create' );
	} //End of method
	

	//--------------------------------------------------------------------
	//Process the form to edit a group
	public function editAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		if (! Ranchbe::checkPerm ( 'admin_users', Ranchbe::getAcl ()->getRootResource (), false ))
			return $this->_cancel ();
		
		$group_id = $this->getRequest ()->getParam ( 'group_id' );
		$group = Rb_Group::get ( $group_id );
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'editGroup', 'post', $this->actionUrl ( 'edit' ) );
		$form->addElement ( 'header', 'editheader', tra ( 'Edit group' ) . ' ' . $group->getGroupname () . ' (id' . $group->getId () . ')' );
		//Set defaults values of elements if create request
		$form->setDefaults ( array ('group_define_name' => $group->getGroupname (), 'group_description' => $group->getProperty ( 'group_description' ), 'is_active' => $group->getProperty ( 'is_active' ) ) );
		$form->addElement ( 'hidden', 'group_id', $group->getId () );
		$this->_helper->actionStack ( 'toolbar' );
		return $this->_finishEditForm ( $form, array (), 'edit' );
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	protected function _finishEditForm(&$form, $Infos, $mode) {
		//Add fields for input informations in all case
		$form->addElement ( 'text', 'group_define_name', tra ( 'Groupe name' ) );
		$form->addElement ( 'text', 'group_description', tra ( 'Description' ), array ('size' => 50 ) );
		$form->addElement ( 'checkbox', 'is_active', tra ( 'Is active' ) );
		
		$form->addRule ( 'group_define_name', tra ( 'Should be alphanumeric' ), 'alphanumeric', null, 'server' );
		$form->addRule ( 'group_define_name', tra ( 'is required' ), 'required', null, 'server' );
		
		// Try to validate the form
		if ($form->validate ()) {
			//$form->freeze(); //and freeze it
			// Form is validated, then processes the create request
			if ($mode == 'create') {
				$form->process ( array ($this, '_create' ), true );
			} else if ($mode == 'edit') {
				$form->process ( array ($this, '_edit' ), true );
			}
			return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		} //End of validate form
		

		$this->error_stack->checkErrors ();
		$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		$form->applyFilter ( '__ALL__', 'trim' );
		$form->display ();
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function _create($values) {
		$group = new Rb_Group ( );
		$group->setGroupname ( $values ['group_define_name'] );
		$group->setProperty ( 'group_description', $values ['group_description'] );
		$group->setProperty ( 'is_active', $values ['is_active'] );
		$group->save ();
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function _edit($values) {
		$group = Rb_Group::get ( $values ['group_id'] );
		$group->setGroupname ( $values ['group_define_name'] );
		$group->setProperty ( 'group_description', $values ['group_description'] );
		$group->setProperty ( 'is_active', $values ['is_active'] );
		$group->save ();
	} //End of method
	

	//--------------------------------------------------------------------
	//Process the form to suppress user actions request
	public function suppressAction() {
		if (! Ranchbe::checkPerm ( 'admin_users', Ranchbe::getAcl ()->getRootResource (), false ))
			return $this->_cancel ();
		
		$group_id = $this->getRequest ()->getParam ( 'group_id' );
		if ($group_id == 1 || $group_id == 99999999999) {
			$this->error_stack->push ( Rb_Error::ERROR, array (), 
					tra ( 'you can not suppress this special group' ) );
		} else {
			Rb_Group::get ( $group_id )->suppress ();
		}
		return $this->_forward ( 
								$this->ifSuccessForward ['action'], 
								$this->ifSuccessForward ['controller'], 
								$this->ifSuccessForward ['module'], 
								$this->ifSuccessForward ['params'] );
	} //End of method
	

	//--------------------------------------------------------------------
	//Process the form to suppress user from group
	public function assigngroupAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		if (! Ranchbe::checkPerm ( 'admin_users', Ranchbe::getAcl ()->getRootResource (), false ))
			return $this->_cancel ();
		
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$group_id = $this->getRequest ()->getParam ( 'group_id' );
		$group = Rb_Group::get ( $group_id );
		$subgroup_ids = $this->getRequest ()->getParam ( 'subgroup_id' );
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'assignGroup', 'post', $this->actionUrl ( 'assigngroup' ) );
		
		$form->addElement ( 'header', 'editheader', tra ( 'Assign group to' ) . ' ' . $group->getGroupname () );
		$form->addElement ( 'hidden', 'group_id', $group->getId () );
		
		//Get the current groups
		foreach ( Rb_Group::getGroups () as $id => $properties ) {
			if ($id == $group_id)
				continue;
			$list [$id] = $properties ['group_define_name'];
		}
		foreach ( $group->getSubGroups () as $group_id => $subgroup_id ) {
			$default_group_ids [] = $subgroup_id;
		}
		
		$p = array ('property_name' => 'subgroup_id', 'property_description' => tra ( 'Select group' ), 'default_value' => $default_group_ids, 'is_multiple' => true, 'return_name' => false, 'property_length' => 5, 'adv_select' => true );
		construct_select ( $list, $p, $form, $validation = 'client' );
		
		//var_dump($group_id);
		//var_dump($subgroup_ids);
		//var_dump($default_group_ids);
		//var_dump($validate);die;
		

		if (isset ( $validate )) {
			if ($form->validate ()) { //Validate the form...
				//if(!$check_flood) break;
				//Add the property if necessary
				if (is_array ( $subgroup_ids ))
					foreach ( $subgroup_ids as $subgroup_id ) {
						if (is_array ( $default_group_ids )) { //Previous selection exist
							if (! in_array ( $subgroup_id, $default_group_ids )) {
								$group->setSubGroup ( Rb_Group::get ( $subgroup_id ) );
							}
						} else { //there is no previous selection, add all
							$group->setSubGroup ( Rb_Group::get ( $subgroup_id ) );
						}
					}
					//Suppress the properties if necessary
				if (is_array ( $default_group_ids ))
					foreach ( $default_group_ids as $subgroup_id ) {
						if (is_array ( $subgroup_ids )) { //Selection exist
							if (! in_array ( $subgroup_id, $subgroup_ids )) {
								$group->unsetSubGroup ( Rb_Group::get ( $subgroup_id ) );
							}
						} else { //its a empty selection, suppress all
							$group->unsetSubGroup ( Rb_Group::get ( $subgroup_id ) );
						}
					}
			}
			$group->saveGroups ();
			$this->_forward ( $this->ifSuccessForward ['action'], 
					$this->ifSuccessForward ['controller'], 
					$this->ifSuccessForward ['module'], 
					$this->ifSuccessForward ['params'] );
			
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			$form->display ();
			// Active the tab
			RbView_Menu::get ()->getAdmin ();
			RbView_Tab::get ( 'usersTab' )->activate ();
		}
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _getgroups() {
		//Display the group list
		foreach ( Rb_Group::getGroups () as $group_id => $group ) {
			$group = Rb_Group::get ( $group_id );
			$groups [$group_id] = $group->getProperties ();
			$groups [$group_id] ['role_id'] = $group->getRoleId ();
			foreach ( $group->getSubGroups () as $subgroup_id ) {
				$groups [$group_id] ['groups'] [$subgroup_id] = Rb_Group::get ( $subgroup_id )->getProperties ();
			}
		}
		$this->view->groups = $groups ;
		return $this->view->groups;
	} //End of method


} //End of class
