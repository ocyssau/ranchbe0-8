<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once ('RbView/Smarty/Form/ElementCreators.php');

class User_PreferencesController extends controllers_abstract {
	protected $page_id = 'preferences'; //(string)
	protected $ifSuccessForward = array ('module' => 'user', 
										'controller' => 'preference', 
										'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'user', 
										'controller' => 'preference', 
										'action' => 'index' );
	
	public function init() {
	} //End of method

	public function editAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_helper->viewRenderer->setNoRender(true);
		//Construct the form with QuickForm lib
		$form = new RbView_Pear_Html_QuickForm ( 'form', 
				'post' , $this->view->baseUrl ( 'user/preferences/edit' ) );
		$form->addElement ( 'header', null, tra ( 'User preferences' ) );
		$this->preference = & Rb_User::getCurrentUser ()->getPreference ();
		$prefs = $this->preference->getUserPreferences ();
		$form->setDefaults ( array (
			'css_sheet' => $prefs ['css_sheet'], 
			'lang' => $prefs ['lang'], 
			'long_date_format' => $prefs ['long_date_format'], 
			'short_date_format' => $prefs ['short_date_format'], 
			'hour_format' => $prefs ['hour_format'], 
			//'time_zone'               => $prefs['time_zone'],
			'wildspace_path' => $prefs ['wildspace_path'], 
			'max_record' => $prefs ['max_record'], 
			'date_input_method' => $prefs ['date_input_method'] ) );
		
		//Construct select long_date_format
		$long_date_format_list = array (
				'%d-%m-%Y %H:%M:%S' => '28-12-1969 12:45:20', 
				'%d/%m/%Y %Hh%Mmn%Ss' => '28/12/1969 12h45mn20s', 
				'%d/%m/%Y %Hh%M' => '28/12/1969 12h45', 
				'%d/%m/%y %Hh%M' => '28/12/69 12h45', 
				'%Y-%m-%d %H:%M:%S' => '1969-12-28 12:45:20', 
				'%d-%m-%Y' => '29-12-1969', 
				'%Y-%m-%d' => '1969-12-28', 
				'default' => tra ( 'default' ) );
		$select = & $form->addElement ( 'select', 
										'long_date_format', 
										tra ( 'long_date_format' ), 
										$long_date_format_list );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		//Construct select short_date_format
		$short_date_format_list = array ('%d-%m-%Y' => '28-12-1969', 
										'%Y-%m-%d' => '1969-12-28', 
										'%d-%m-%y' => '28-12-69', 
										'%y-%m-%d' => '69-12-28', 
										'default' => tra ( 'default' ) );
		$select = & $form->addElement ( 'select', 'short_date_format', 
					tra ( 'short_date_format' ), $short_date_format_list );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		//Construct select jscalendar
		$date_input_method = array ('jscalendar' => tra ( 'jscalendar' ), 
									'qfdate' => tra ( 'qfdate' ), 
									'default' => tra ( 'default' ) );
		$select = & $form->addElement ( 'select', 
										'date_input_method', 
										tra ( 'date_input_method' ), 
										$date_input_method );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		//Construct select css_sheet
		$css_sheet_list = glob ( 'styles/*.css' );
		$css_sheet_list = array_combine ( $css_sheet_list, $css_sheet_list );
		$css_sheet_list ['default'] = tra ( 'default' );
		//$select = & $form->addElement ( 'select', 'css_sheet', 
			//							tra ( 'css_sheet' ), $css_sheet_list );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		//Construct select lang
		$lang_list = array ('fr' => tra ( 'French' ), 
							'en' => tra ( 'English' ), 
							'default' => tra ( 'default' ) );
		$select = & $form->addElement ( 'select', 'lang', 
							tra ( 'lang' ), $lang_list );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		/*
		//Construct select timezone
		$time_zone_list = array('UMT+1','UMT+2','UMT+3');
		$time_zone_list = array_combine($time_zone_list,$time_zone_list);
		$time_zone_list['default'] = tra('default');
		$select =& $form->addElement('select', 'time_zone', tra('time_zone'), $time_zone_list );
		$select->setSize(1);
		$select->setMultiple(false);
		*/
		
		//Construct select max_record
		$max_record_list = array (50, 100, 200, 300, 400, 500 );
		$max_record_list = array_combine ( $max_record_list, $max_record_list );
		$max_record_list ['default'] = tra ( 'default' );
		$select = & $form->addElement ( 'select', 'max_record', 
										tra ( 'max_record' ), $max_record_list );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		$form->addElement ( 'submit', 'submit', 'Validate' );
		
		// Try to validate the form
		if ($form->validate ()) {
			$form->process ( array ($this, '_save' ), true );
		} //End of validate form
		
		$form->display ();
		Ranchbe::getError()->checkErrors ();
		return;
	} //End of method
	
	public function _save($prefs) {
		$this->preference->setPreference ( 'css_sheet', $prefs ['css_sheet'] );
		$this->preference->setPreference ( 'lang', $prefs ['lang'] );
		$this->preference->setPreference ( 'long_date_format', $prefs ['long_date_format'] );
		$this->preference->setPreference ( 'short_date_format', $prefs ['short_date_format'] );
		$this->preference->setPreference ( 'date_input_method', $prefs ['date_input_method'] );
		//$preference->setPreference('time_zone', $prefs['time_zone']);
		$this->preference->setPreference ( 'max_record', $prefs ['max_record'] );
		$this->preference->save ();
		if($prefs ['css_sheet'] == 'default'){
			$this->view->headLink()->offsetSetStylesheet ( 'user', ROOT_URL.'/styles/default.css' );
		}else{
			$this->view->headLink()->offsetSetStylesheet ( 'user', ROOT_URL.$prefs ['css_sheet'] );
		}
	}
	
} //End of class
