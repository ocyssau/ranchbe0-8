<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('controllers/perms/abstract.php');
class User_PermsController extends controllers_perms_abstract {
	protected $page_id = 'userPerms'; //(string)
	protected $ifSuccessForward = array ('module' => 'user', 
							'controller' => 'perms', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'user', 
							'controller' => 'perms', 'action' => 'index' );
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		
		$this->ticket = $this->view->ticket;
		$this->space_name = $this->getRequest ()->getParam ( 'space' );
		$this->object_id = $this->getRequest ()->getParam ( 'object_id' );
		
		$this->_initForward ();
		
		$this->role_id = $this->getRequest ()->getParam ( 'role_id' );
		$this->resource_id = $this->getRequest ()->getParam ( 'resource_id' );
		
		if (empty ( $this->resource_id )) {
			print 'none ressource<br>';
			die ();
		}
		
		if ($this->resource_id == 1)
			$this->view->resource_name = tra ( 'default recource' );
		else {
			$this->view->resource_name = tra ( 'unknow resource' );
		}
		$this->view->assign ( 'samecontroller', './user/perms' ); //important: is first assign
	} //End of method


} //End of class
