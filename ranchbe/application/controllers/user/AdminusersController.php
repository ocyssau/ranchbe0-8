<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


//@todo : changer controllers_perms_abstract herite de controllers_projectManager_abstract

require_once ('controllers/perms/abstract.php');
class User_AdminusersController extends controllers_perms_abstract {
	protected $page_id = 'userManager_adminuser'; //(string)
	protected $ifSuccessForward = array ('module' => 'user', 'controller' => 'adminusers', 'action' => 'get' );
	protected $ifFailedForward = array ('module' => 'user', 'controller' => 'adminusers', 'action' => 'get' );
	
	//--------------------------------------------------------------------
	public function init() {
		parent::init ();
		$this->_initForward ();
	} //End of method
	

	//--------------------------------------------------------------------
	public function index() {
		return $this->_forward ( 'get' );
	} //End of method
	

	//--------------------------------------------------------------------
	public function getAction() {
		if (Ranchbe::checkPerm ( 'admin_users', Ranchbe::getAcl ()->getRootResource (), false )) {
			$this->view->users = $this->_getUsers ();
		}
		// Display the template
		Ranchbe::getError()->checkErrors ();
		
		// Active the tab
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'usersTab' )->activate ();
		
		$this->view->sameurl = 'user/adminusers/get'; //important: is first assign
		//$this->view->assign('views_helper_filter_form' , $filter->fetchForm($this->view->getEngine(), $this->container) ); //generate code for the filter form
		//$this->view->assign('views_helper_pagination' , $pagination->fetchForm($this->view->getEngine()) ); //generate code for the filter form
		$this->_helper->actionStack ( 'toolbar' );
	} //End of method
	

	//--------------------------------------------------------------------
	// Process the form to add a user here
	public function createAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		if (! Ranchbe::checkPerm ( 'admin_users', Ranchbe::getAcl ()->getRootResource (), false ))
			return $this->_cancel ();
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'createUser', 'post', $this->actionUrl ( 'create' ) );
		$form->addElement ( 'header', 'editheader', tra ( 'Create a user' ) );
		// Active the tab
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'usersTab' )->activate ();
		$this->_helper->actionStack ( 'toolbar' );
		return $this->_finishEditForm ( $form, array (), 'create' );
	} //End of method
	
	
	//--------------------------------------------------------------------
	//Process the form to edit a user
	public function editAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		$user_id = $this->getRequest ()->getParam ( 'user_id' );
		$user = Rb_User::get ( $user_id );
		if (Rb_User::getCurrentUser ()->getId () != $user_id) { //the current user can modify his property
			if (! Ranchbe::checkPerm ( 'admin_users', Ranchbe::getAcl ()->getRootResource (), false ))
				return $this->_cancel ();
		}
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'editUser', 'post', $this->actionUrl ( 'edit' ) );
		$form->addElement ( 'header', 'editheader', tra ( 'Edit user' ) . ' ' . $user->getUsername () . ' (id' . $user->getId () . ')' );
		//Set defaults values of elements if create request
		$form->setDefaults ( array ('username' => $user->getUsername (), 'email' => $user->getProperty ( 'email' ), 'is_active' => $user->getProperty ( 'is_active' ), 'passwd1' => '', 'passwd2' => '' ) );
		$form->addElement ( 'hidden', 'user_id', $user->getId () );
		// Active the tab
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'usersTab' )->activate ();
		$this->_helper->actionStack ( 'toolbar' );
		return $this->_finishEditForm ( $form, array (), 'edit' );
	} //End of method
	
	
	//--------------------------------------------------------------------
	//Change password
	public function changemypasswdAction() {
		$user = & Rb_User::getCurrentUser ();
		$user_id = $user->getId ();
		
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_helper->viewRenderer->setNoRender ( true );
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'editUser', 'post', $this->actionUrl ( 'changemypasswd' ) );
		
		$form->addElement ( 'header', 'editheader', tra ( 'Change password' ) . ' ' . $user->getUsername () . ' (id' . $user->getId () . ')' );
		
		//Set defaults values of elements if create request
		$form->setDefaults ( array ('email' => $user->getProperty ( 'email' ), 'passwd1' => '', 'passwd2' => '' ) );
		
		$form->addElement ( 'hidden', 'user_id', $user->getId () );
		
		$form->addElement ( 'text', 'email', tra ( 'User mail' ) );
		$form->addRule ( 'email', tra ( 'Should contain a valid email' ), 'email', null, 'server' );
		
		$this->_addPasswdFormInput ( $form );
		
		// Try to validate the form
		if ($form->validate ()) {
			$form->freeze (); //and freeze it
			// Form is validated, then processes the create request
			$form->process ( array ($this, '_edit' ), true );
			//return $this->_forward($this->ifSuccessForward['action'], $this->ifSuccessForward['controller'], $this->ifSuccessForward['module'], $this->ifSuccessForward['params'] );
		} else { //End of validate form
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		}
		Ranchbe::getError()->checkErrors ();
		//$form->addElement ( 'static', null, '<input type="button" onclick="window.close()" value="' . tra ( Close ) . '">' );
		$form->applyFilter ( '__ALL__', 'trim' );
		$form->display ();
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	protected function _finishEditForm(&$form, $Infos, $mode) {
		//Add fields for input informations in all case
		$form->addElement ( 'text', 'username', tra ( 'User name' ), array ('autocomplete' => 'off' ) );
		$form->addElement ( 'password', 'passwd1', tra ( 'Password' ), array ('autocomplete' => 'off' ) );
		$form->addElement ( 'password', 'passwd2', tra ( 'Repeat password' ), array ('autocomplete' => 'off' ) );
		$form->addElement ( 'text', 'email', tra ( 'User mail' ) );
		$form->addElement ( 'checkbox', 'is_active', tra ( 'Is active' ) );
		
		$form->addRule ( 'username', tra ( 'Should be alphanumeric' ), 'alphanumeric', null, 'server' );
		$form->addRule ( 'username', tra ( 'is required' ), 'required', null, 'server' );
		$form->addRule ( 'email', tra ( 'Should contain a valid email' ), 'email', null, 'server' );
		$form->addRule ( array ('passwd1', 'passwd2' ), tra ( 'The passwords do not match' ), 'compare', null, 'server' );
		$form->addRule ( 'passwd1', tra ( 'Password should be at least' ) . ' ' 
						. Ranchbe::getConfig()->password->minlength . ' ' . tra ( 'characters long' ),
						'minlength', Ranchbe::getConfig()->password->minlength, 'server' );
		$form->addRule ( 'passwd1', tra ( 'Password should be at least' ) . ' ' 
						. Ranchbe::getConfig()->password->minlength . ' ' . tra ( 'characters long' ), 
						'minlength', Ranchbe::getConfig()->password->minlength, 'server' );
		$form->addRule ( 'passwd1', 
						Ranchbe::getConfig()->password->help, 
						'regex', '/^' . Ranchbe::getConfig()->password->mask . '/', 'server' );
		
		// Try to validate the form
		if ($form->validate ()) {
			//$form->freeze(); //and freeze it
			// Form is validated, then processes the create request
			if ($mode == 'create') {
				$form->process ( array ($this, '_create' ), true );
			} else if ($mode == 'edit') {
				$form->process ( array ($this, '_edit' ), true );
			}
			return $this->_forward ( $this->ifSuccessForward ['action'], 
									$this->ifSuccessForward ['controller'], 
									$this->ifSuccessForward ['module'], 
									$this->ifSuccessForward ['params'] );
		} //End of validate form
		
		Ranchbe::getError()->checkErrors ();
		$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		$form->applyFilter ( '__ALL__', 'trim' );
		$form->display ();
	} //End of method
	

	protected function _addPasswdFormInput(&$form) {
		$form->addElement ( 'password', 'passwd1', tra ( 'Password' ), array ('autocomplete' => 'off' ) );
		$form->addElement ( 'password', 'passwd2', tra ( 'Repeat password' ), array ('autocomplete' => 'off' ) );
		$form->addRule ( array ('passwd1', 'passwd2' ), tra ( 'The passwords do not match' ), 'compare', null, 'server' );
		$form->addRule ( 'passwd1', tra ( 'Password should be at least' ) . ' ' . Ranchbe::getConfig()->password->minlength . ' ' . tra ( 'characters long' ), 'minlength', Ranchbe::getConfig()->password->minlength, 'server' );
		$form->addRule ( 'passwd1', tra ( 'Password should be at least' ) . ' ' . Ranchbe::getConfig()->password->minlength . ' ' . tra ( 'characters long' ), 'minlength', Ranchbe::getConfig()->password->minlength, 'server' );
		$form->addRule ( 'passwd1', Ranchbe::getConfig()->password->help, 'regex', '/^' . Ranchbe::getConfig()->password->mask . '/', 'server' );
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function _create($values) {
		$user = new Rb_User ( );
		$user->setUsername ( $values ['username'] );
		$user->setProperty ( 'passwd', $values ['passwd1'] );
		$user->setProperty ( 'email', $values ['email'] );
		$user->setProperty ( 'is_active', $values ['is_active'] );
		$user->save ();
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function _edit($values) {
		$user = Rb_User::get ( $values ['user_id'] );
		if (! empty ( $values ['username'] ))
			$user->setUsername ( $values ['username'] );
		if (! empty ( $values ['passwd1'] ))
			$user->setProperty ( 'passwd', $values ['passwd1'] );
		if (! empty ( $values ['email'] ))
			$user->setProperty ( 'email', $values ['email'] );
		if (isset ( $values ['is_active'] ))
			$user->setProperty ( 'is_active', $values ['is_active'] );
		$user->save ();
	} //End of method
	

	//--------------------------------------------------------------------
	//Process the form to suppress user actions request
	public function suppressuserAction() {
		if (! Ranchbe::checkPerm ( 'admin_users', Ranchbe::getAcl ()->getRootResource (), false ))
			return $this->_cancel ();
		$user_id = $this->getRequest ()->getParam ( 'user_id' );
		if ($user_id == 1 || $user_id == 99999999999) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array (), tra ( 'you can not suppress this special user' ) );
		} else {
			Rb_User::get ( $user_id )->suppress ();
		}
		return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
	} //End of method
	

	//--------------------------------------------------------------------
	//Process the form to suppress user from group
	public function assigngroupAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		if (! Ranchbe::checkPerm ( 'admin_users', Ranchbe::getAcl ()->getRootResource (), false )) {
			Ranchbe::getError()->checkErrors ();
			return $this->_cancel ();
		}
		
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$user_id = $this->getRequest ()->getParam ( 'user_id' );
		$user = Rb_User::get ( $user_id );
		$group_ids = $this->getRequest ()->getParam ( 'group_id' );
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'assignGroup', 'post', $this->actionUrl ( 'assigngroup' ) );
		
		$form->addElement ( 'header', 'editheader', tra ( 'Assign group to' ) . ' ' . $user->getUsername () );
		$form->addElement ( 'hidden', 'user_id', $user->getId () );
		
		//Get the current groups
		foreach ( Rb_Group::getGroups () as $group_id => $group_properties ) {
			$list [$group_id] = $group_properties ['group_define_name'];
		}
		foreach ( $user->getGroups () as $user_id => $group_id ) {
			$default_group_ids [] = $group_id;
		}
		
		$p = array (
				'property_name' => 'group_id', 
				'property_description' => tra ( 'Select group' ), 
				'default_value' => $default_group_ids, 
				'is_multiple' => true, 
				'return_name' => false, 
				'property_length' => 5, 
				'adv_select' => true );
		construct_select ( $list, $p, $form, $validation = 'client' );

		if (isset ( $validate )) {
			if ($form->validate ()) { //Validate the form...
				//if(!$check_flood) break;
				//Add the property if necessary
				if (is_array ( $group_ids ))
					foreach ( $group_ids as $group_id ) {
						if (is_array ( $default_group_ids )) { //Previous selection exist
							if (! in_array ( $group_id, $default_group_ids )) {
								$user->setGroup ( Rb_Group::get ( $group_id ) );
							}
						} else { //there is no previous selection, add all
							$user->setGroup ( Rb_Group::get ( $group_id ) );
						}
					}
					//Suppress the properties if necessary
				if (is_array ( $default_group_ids ))
					foreach ( $default_group_ids as $group_id ) {
						if (is_array ( $group_ids )) { //Selection exist
							if (! in_array ( $group_id, $group_ids )) {
								$user->unsetGroup ( Rb_Group::get ( $group_id ) );
							}
						} else { //its a empty selection, suppress all
							$user->unsetGroup ( Rb_Group::get ( $group_id ) );
						}
					}
			}
			$user->saveGroups ();
			return  $this->_forward ( 
					$this->ifSuccessForward ['action'], 
					$this->ifSuccessForward ['controller'], 
					$this->ifSuccessForward ['module'], 
					$this->ifSuccessForward ['params'] );
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			$form->display ();
			// Active the tab
			RbView_Menu::get ()->getAdmin ();
			RbView_Tab::get ( 'usersTab' )->activate ();
		}
	} //End of method
	

	//--------------------------------------------------------------------
	/**
	 * Import user definition from csv file
	 * @todo :  rewrite this method
	 * 
	 * @return void
	 */
	public function batchImportUsersAction() {
		if (! Ranchbe::checkPerm ( 'admin_users', Ranchbe::getAcl ()->getRootResource (), false ))
			return $this->_cancel ();
		
		global $userlib, $smarty, $user;
		
		$fname = $_FILES ['csvlist'] ['tmp_name'];
		$fhandle = fopen ( $fname, "r" );
		$fields = fgetcsv ( $fhandle, 1000 );
		if (! $fields [0]) {
			$smarty->assign ( 'msg', tra ( "The file is not a CSV file or has not a correct syntax" ) );
			$smarty->display ( "error.tpl" );
			die ();
		}
		while ( ! feof ( $fhandle ) ) {
			$data = fgetcsv ( $fhandle, 1000 );
			$temp_max = count ( $fields );
			for($i = 0; $i < $temp_max; $i ++) {
				if ($fields [$i] == "login" && function_exists ( "mb_detect_encoding" ) && mb_detect_encoding ( $data [$i], "ASCII, UTF-8, ISO-8859-1" ) == "ISO-8859-1") {
					$data [$i] = utf8_encode ( $data [$i] );
				}
				@$ar [$fields [$i]] = $data [$i];
			}
			$userrecs [] = $ar;
		}
		fclose ( $fhandle );
		
		if (! is_array ( $userrecs )) {
			$smarty->assign ( 'msg', tra ( "No records were found. Check the file please!" ) );
			$smarty->display ( "error.tpl" );
			die ();
		}
		$added = 0;
		$errors = array ();
		
		foreach ( $userrecs as $u ) {
			if (empty ( $u ['login'] )) {
				$discarded [] = discardUser ( $u, tra ( "User login is required" ) );
			} elseif (empty ( $u ['password'] )) {
				$discarded [] = discardUser ( $u, tra ( "Password is required" ) );
			} elseif (empty ( $u ['email'] )) {
				$discarded [] = discardUser ( $u, tra ( "Email is required" ) );
			} elseif ($userlib->userExists ( $u ['login'] ) and (! isset ( $_REQUEST ['overwrite'] ))) {
				$discarded [] = discardUser ( $u, tra ( "User is duplicated" ) );
			} else {
				if (! Ranchbe::getAuthAdapter ()->getUserIdFromName ( $u ['login'] )) {
					$userlib->add_user ( $u ['login'], $u ['password'], $u ['password'], $u ['email'] );
					//$logslib->add_log('users',sprintf(tra("Created account %s <%s>"),$u["login"], $u["email"]));
				}
				
				if (@$u ['groups']) {
					$grps = explode ( ",", $u ['groups'] );
					
					foreach ( $grps as $grp ) {
						$grp = preg_replace ( "/^ *(.*) *$/u", "$1", $grp );
						if (! $userlib->group_exists ( $grp )) {
							$err = tra ( "Unknown" ) . ": $grp";
							if (! in_array ( $err, $errors ))
								$errors [] = $err;
						} else {
							
							$userlib->assign_user_to_group ( $u ['login'], $grp );
							//$logslib->add_log('perms',sprintf(tra("Assigned %s in group %s"),$u["login"], $grp));
						}
					}
				}
				$added ++;
			}
		}
		$smarty->assign ( 'added', $added );
		if (@is_array ( $discarded )) {
			$smarty->assign ( 'discarded', count ( $discarded ) );
		}
		@$smarty->assign ( 'discardlist', $discarded );
		if (count ( $errors )) {
			array_unique ( $errors );
			$smarty->assign_by_ref ( 'errors', $errors );
		}
	} //End of method


} //End of class

