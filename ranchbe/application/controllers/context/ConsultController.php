<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


class Context_ConsultController extends Zend_Controller_Action {
	protected $context;
	protected $smarty;
	
	public function init() {
		$this->context = & Ranchbe::getContext ();
		$this->error_stack = & Ranchbe::getError();
		
		$this->space_name = $this->getRequest ()->getParam ( 'space' );
		$this->container_id = $this->getRequest ()->getParam ( 'container_id' );
		
		if (! $this->container_id) {
			$this->container_id = $_SESSION ['consult'] ['container_id'];
			$this->space_name = $_SESSION ['consult'] ['space_name'];
		} else {
			$_SESSION ['consult'] ['container_id'] = $this->container_id;
			$_SESSION ['consult'] ['space_name'] = $this->space_name;
		}
		
		//Assign name to particular fields
		$this->view->assign ( 'container_id', $this->container_id );
		$this->view->assign ( 'SPACE_NAME', $this->space_name );
	} //End of method
	

	public function indexAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		RbView_Menu::get ()->getMySpace ()->getConsult ();
		RbView_Tab::get ( 'consultTab' )->activate ();
		//$this->view->assign ( 'sameurl', './context/consult/index' ); //important: is first assign
	} //End of method
	

	public function consultAction() {
		$container = & Rb_Container::get ( $this->space_name, $this->container_id );
		if (! Ranchbe::checkPerm ( 'document_get', $container, false )) {
			$this->error_stack->checkErrors ();
			return $this->_forward ( 'index', 'index', 'container' );
		}
		
		if ($container->GetProperty ( 'file_only' ) == 1) {
			$search = new Rb_Search_Db ( Rb_Recordfile::get ( $this->space_name )->getDao () );
			$filter = new RbView_Helper_Recordfilefilter ( $this->getRequest (), $search, 'recordfile' );
		    $filter->setTemplate('recordfile/searchBar.tpl');
			$filter->setDefault ( 'sort_field', 'file_name' );
			$filter->setDefault ( 'sort_order', 'ASC' );
			$filter->finish ();
			$this->_helper->viewRenderer->setScriptAction ( 'recordfile' );
			//$this->view->assign ( 'mid', 'recordfile/consult.tpl' );
			//Zend_Registry::set ( 'space', $container->space );
			$this->view->assign ( 'object_class', 'recordfile' );
		} else {
			$search = new Rb_Search_Db ( Rb_Document::get ( $this->space_name )->getDao () );
			$filter = new RbView_Helper_Docfilter ( $this->getRequest (), $search, 'document' );
			$filter->setDefault ( 'sort_field', 'document_number' );
			$filter->setDefault ( 'sort_order', 'ASC' );
			$filter->finish ();
			$this->_helper->viewRenderer->setScriptAction ( 'document' );
			//$this->view->assign ( 'mid', 'document/consult.tpl' );
		}
		//$params = $filter->GetParams();
		//var_dump($params);
		
		//get all documents
		$this->view->list = $container->getContents ( $filter->getParams (), $filter->getOption ( 'displayHistory' ) );
		
		$pagination = new RbView_Helper_Pagination ( $filter );
		$pagination->setPagination ( count ( $this->view->list ) );
		
		$this->error_stack->checkErrors ();
		
		// Display the template
		RbView_Menu::get ()->getMySpace ()->getConsult ();
		$tab =& RbView_Tab::get ( $container->getName () . 'Tab' );
		if($tab) $tab->activate ();
		$this->view->assign ( 'sameurl', './context/consult/consult' ); //important: is first assign
		$this->view->assign ( 'views_helper_filter_form', $filter->fetchForm ( $this->view->getEngine(), $container ) ); //generate code for the filter form
		$this->view->assign ( 'RbViews_Helper_Pagination', $pagination->fetchForm ( $this->view->getEngine() ) ); //generate code for the filter form
	} //End of method


} //End of class
