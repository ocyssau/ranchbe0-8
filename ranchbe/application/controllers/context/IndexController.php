<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


class Context_IndexController extends Zend_Controller_Action {
	protected $context;
	protected $smarty;
	
	public function init() {
		$this->context = & Ranchbe::getContext ();
		$this->view = & Ranchbe::getSmarty ();
		
		//Assign name to particular fields
		$this->view->assign ( 'project_number', $this->context->getProperty ( 'project_number' ) );
		$this->view->assign ( 'project_id', $this->context->getProperty ( 'project_id' ) );
		$this->view->assign ( 'container_number', $this->context->getProperty ( 'container_number' ) );
		$this->view->assign ( 'container_id', $this->context->getProperty ( 'container_id' ) );
		$this->view->assign ( 'SPACE_NAME', $this->context->getProperty ( 'space_name' ) );
	} //End of method
	
	//------------------------------------------------------------------------------
	protected function _generateSearchOrder() {
		$wildspace = new Rb_Wildspace ( Rb_User::getCurrentUser () );
		$paths = array ($wildspace->getPath () );
		$links = $this->context->getProperty ( 'links' );

		$client_os = strtolower(CLIENT_OS);
		$path_mapping = Ranchbe::getConfig()->pathmapping->$client_os->toArray();
		//var_dump($path_mapping);die;
		
		$path_mapping = str_replace ( '%user%', Rb_User::getCurrentUser ()->getUsername (), $path_mapping );
		$path_mapping = array_flip ( $path_mapping );
		$path_mapping = str_replace ( '%user%', Rb_User::getCurrentUser ()->getUsername (), $path_mapping );
		$path_mapping = array_flip ( $path_mapping );
		
		$search = array_keys ( $path_mapping );
		$replace = array_values ( $path_mapping );
		
		foreach ( array ('workitem', 'mockup', 'cadlib' ) as $class ) {
			if (is_array ( $links [$class] ))
				foreach ( $links [$class] as $container ) {
					$paths [] = $container ['default_file_path'];
				}
		}
		$res = str_replace ( $search, $replace, $paths );
		if (CLIENT_OS == 'WINDOWS')
			$res = str_replace ( '/', '\\', $res );
		return $res;
	} //End of method
	

	//------------------------------------------------------------------------------
	protected function _generateSearchOrderCatia() {
		return implode ( PHP_EOL, $this->_generateSearchOrder () );
	} //End of method
	

	//------------------------------------------------------------------------------
	public function generatecatiasearchorderAction() {
		$this->view->assign ( 'searchOrder', $this->_generateSearchOrderCatia () );
		$this->view->assign ( 'CLIENT_OS', CLIENT_OS );
		$this->_forward ( 'index', 'index', 'context', array () );
	} //End of method
	
	//------------------------------------------------------------------------------
	public function saveAction() {
		if (CLIENT_OS == 'WINDOWS')
			$_REQUEST ['searchOrder'] = str_replace ( '\\\\', '\\', $_REQUEST ['searchOrder'] );
		header ( "Content-disposition: attachment; filename=" . $this->context->getProperty ( 'project_number' ) . '_SearchOrder.txt' );
		header ( "Content-Type: " . 'text/plain' );
		header ( "Content-Transfer-Encoding: searchOrder\n" ); // Surtout ne pas enlever le \n
		header ( "Content-Length: " . strlen ( $_REQUEST ['searchOrder'] ) );
		header ( "Pragma: no-cache" );
		header ( "Cache-Control: must-revalidate, post-check=0, pre-check=0, public" );
		header ( "Expires: 0" );
		print $_REQUEST ['searchOrder'] . PHP_EOL;
		die;
	} //End of method
	

	//------------------------------------------------------------------------------
	public function indexAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->view->list = array();
		
		//Get detail on each object
		if ($this->context->getProperty ( 'links' ))
			foreach ( $this->context->getProperty ( 'links' ) as $class_id => $objects ) {
				$class = Rb_Object_Permanent::getClassName ( $class_id );
				if (is_array ( $objects ))
					foreach ( $objects as $object ) {
						$this->view->list [] = array (
								'number' => $object [$class . '_number'], 
								'type' => $class, 
								'id' => $object [$class . '_id'], 
								'space' => $object ['space_id'] );
					}
			} // End of foreach
		$this->view->assign ( 'HeaderCol1', 'Type' );
		$this->view->assign ( 'HeaderCol2', 'Number' );
		$this->view->assign ( 'HeaderCol3', 'Id' );
		$this->view->assign ( 'id', 'id' );
		$this->view->assign ( 'col1', 'type' );
		$this->view->assign ( 'col2', 'number' );
		$this->view->assign ( 'col3', 'id' );
		if (Ranchbe::getConfig()->context->debug) {
			echo '<hr /><br/><b>DEBUG : Dump of the session :</b><br />';
			Zend_Debug::dump ( Ranchbe::getContext ()->getProperties () );
		}
	} //End of method


} //End of class
