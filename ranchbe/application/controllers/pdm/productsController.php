<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/pdm/abstract.php');
class Pdm_ProductsController extends controllers_pdm_abstract {
	protected $page_id = 'pdmProducts'; //(string)
	protected $ifSuccessForward = array ('module' => 'pdm', 'controller' => 'products', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'pdm', 'controller' => 'products', 'action' => 'index' );
	
	public function init() {
		parent::init ();
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
		/*
  if(!Ranchbe::checkPerm( 'get' , Rb_Pdm_Acl_Resource::get(-1) , false)){
    Ranchbe::getError()->checkErrors();
    return $this->_forward();
  }
  */
		
		$list = Rb_Pdm_Product_viewa::get ()->getAll ();
		$this->smarty->assign_by_ref ( 'list', $list );
		
		$this->smarty->assign ( 'sameurl', './pdm/products/index' ); //important: is first assign
		$this->smarty->assign ( 'PageTitle', tra ( 'Products manager' ) );
		
		RbView_Menu::get ()->init ()->getPdm ();
		RbView_Tab::get ( 'productsTab' )->activate ();
		Ranchbe::getSmarty ()->assign ( 'mid', 'pdm/products/get.tpl' );
		Ranchbe::getSmarty ()->display ( 'ranchbe.tpl' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function createAction() {
		//Ranchbe::checkPerm( 'create' , Rb_Pdm_Acl_Resource::get(-1) , true);
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'createPdmProduct', 'post', $this->actionUrl ( 'create' ) );
		$form->addElement ( 'header', 'createheader', tra ( 'Create a pdm product' ) );
		$form->setWidth ( '600px' );
		return $this->_finishEditForm ( $form, 'create' );
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	protected function _finishEditForm(&$form, $mode) {
		//Add fields for input informations in all case
		$form->addElement ( 'text', 'number', tra ( 'number' ) );
		$form->addElement ( 'text', 'name', tra ( 'name' ) );
		$form->addElement ( 'textarea', 'description', tra ( 'description' ), array ('size' => 40 ) );
		$form->addElement ( 'text', 'version', tra ( 'version' ) );
		
		$form->addRule ( 'name', 'is required', 'required', null, 'server' );
		$form->addRule ( 'number', 'is required', 'required', null, 'server' );
		
		//Select context
		foreach ( Rb_Pdm_Context_Productdefinition::get ( - 1 )->getAll () as $cdef ) {
			$contexts [$cdef ['cta_id']] = $cdef ['name'] . '/' . $cdef ['application'];
		}
		$params = array ('property_name' => 'cta_id', 'property_description' => 'context definition', 'property_length' => 1, 'is_multiple' => false, 'is_required' => true, 'return_name' => false );
		construct_select ( $contexts, $params, $form );
		
		$form->addElement ( 'reset', 'reset', 'reset' );
		$form->addElement ( 'submit', 'submit', tra ( 'Validate' ) );
		
		// Try to validate the form
		if ($form->validate ()) {
			$form->freeze (); //and freeze it
			// Form is validated, then processes the create request
			if ($mode == 'create') {
				$form->process ( array ($this, '_create' ), true );
			} else if ($mode == 'edit') {
				$form->process ( array ($this, '_modify' ), true );
			}
		} //End of validate form
		

		$form->display ();
		$this->error_stack->checkErrors ();
	
	} //End of method
	

	//---------------------------------------------------------------------
	public function _create($values) {
		//create basic definition
		$product = & Rb_Pdm_Product::get ( 0 );
		$product->setProperty ( 'number', $values ['number'] ); //number must be uniq, if empty or not set, number will be automaticly set
		$product->setProperty ( 'name', $values ['name'] );
		$product->setProperty ( 'description', $values ['description'] );
		$product->save ();
		
		//create product version
		$version = Rb_Pdm_Product_Version::get ( 0 );
		$version->setProperty ( 'version_id', $values ['version'] );
		$version->setProduct ( $product );
		$version->setProperty ( 'name', $values ['name'] );
		$version->setProperty ( 'description', $values ['description'] );
		//associate a existing document to this version
		$document = & Rb_Document::get ( 'workitem', 1 );
		$version->setDocument ( $document );
		//record version in database
		$version->save ();
		
		//create product definition
		$context1 = Rb_Pdm_Context_Productdefinition::get ( $values ['cta_id'] );
		$definition = & Rb_Pdm_Product_Definition::get ( 0 );
		$definition->setProductVersion ( $version );
		$definition->setContext ( $context1 ); //Set the primary context
		$definition->save ();
	} //End of method
	

	//---------------------------------------------------------------------
	//Create a product version
	public function _createpv($values) {
		$product = & Rb_Pdm_Product::get ( $values ['p_id'] );
		//create product version
		$version = Rb_Pdm_Product_Version::get ( 0 );
		$version->setProperty ( 'version_id', $values ['version'] );
		$version->setProduct ( $product );
		$version->setProperty ( 'name', $values ['name'] );
		$version->setProperty ( 'description', $values ['description'] );
		//associate a existing document to this version
		if ($values ['document_space'] && $values ['document_id']) {
			$document = & Rb_Document::get ( $values ['document_space'], $values ['document_id'] );
			$version->setDocument ( $document );
		}
		//record version in database
		$version->save ();
	} //End of method
	

	//---------------------------------------------------------------------
	//Create a product definition
	public function _createpd($values) {
		$version = Rb_Pdm_Product_Version::get ( $values ['pv_id'] );
		$context1 = Rb_Pdm_Context_Productdefinition::get ( $values ['cta_id'] );
		$definition = & Rb_Pdm_Product_Definition::get ( 0 );
		$definition->setProductVersion ( $version );
		$definition->setContext ( $context1 ); //Set the primary context
		$definition->setProperty ( 'description', $values ['description'] );
		$definition->save ();
	} //End of method


} //End of class
