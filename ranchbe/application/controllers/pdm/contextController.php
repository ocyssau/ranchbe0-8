<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/pdm/abstract.php');
class Pdm_ContextController extends controllers_pdm_abstract {
	protected $page_id = 'pdmContext'; //(string)
	protected $ifSuccessForward = array ('module' => 'pdm', 'controller' => 'context', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'pdm', 'controller' => 'context', 'action' => 'index' );
	
	public function init() {
		parent::init ();
		
		$this->cta_id = $this->getRequest ()->getParam ( 'cta_id' );
		if (is_array ( $this->cta_id )) {
			$this->cta_ids = $this->cta_id;
			$this->cta_id = - 1; //to get a not initialized container object
		} else {
			$this->cta_ids = array ($this->cta_id );
		}
		
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
		/*
  if(!Ranchbe::checkPerm( 'get' , Rb_Pdm_Acl_Resource::get(-1) , false)){
    Ranchbe::getError()->checkErrors();
    return $this->_forward();
  }
  */
		
		$context = Rb_Pdm_Context_Productdefinition::get ( - 1 );
		$list = $context->getAll ();
		
		$this->smarty->assign_by_ref ( 'list', $list );
		
		$this->smarty->assign ( 'sameurl', './pdm/context/index' ); //important: is first assign
		$this->smarty->assign ( 'PageTitle', tra ( 'Context manager' ) );
		
		RbView_Menu::get ()->init ()->getPdm ();
		RbView_Tab::get ( 'contextTab' )->activate ();
		Ranchbe::getSmarty ()->assign ( 'mid', 'pdm/context/get.tpl' );
		Ranchbe::getSmarty ()->display ( 'ranchbe.tpl' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function createAction() {
		//Ranchbe::checkPerm( 'create' , rb_pdm_acl_resource::get(-1) , true);
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'createPdmContext', 'post', $this->actionUrl ( 'create' ) );
		$form->addElement ( 'header', 'createheader', tra ( 'Create a pdm context' ) );
		$form->setWidth ( '600px' );
		return $this->_finishEditForm ( $form, 'create' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function editAction() {
		//if (!$check_flood) return false;
		$context = Rb_Pdm_Context_Productdefinition::get ( $this->cta_id );
		//Ranchbe::checkPerm( 'edit' , rb_pdm_acl_resource::get(-1) , true);
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'editPdmContext', 'post', $this->actionUrl ( 'edit' ) );
		$form->addElement ( 'header', 'editheader', tra ( 'Edit a pdm context' ) );
		$form->setWidth ( '600px' );
		
		//Set defaults values of elements if modify request
		$form->setDefaults ( array ('name' => $context->getProperty ( 'name' ), 'application' => $context->getProperty ( 'application' ), 'life_cycle_stage' => $context->getProperty ( 'life_cycle_stage' ) ) );
		
		//Add hidden fields
		$form->addElement ( 'hidden', 'cta_id', $context->getId () );
		$this->_finishEditForm ( $form, 'edit' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function suppressAction() {
		foreach ( $this->cta_ids as $cta_id ) {
			$context = Rb_Pdm_Context_Productdefinition::get ( $cta_id );
			$context->suppress ();
		}
		return $this->_successForward ();
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	protected function _finishEditForm(&$form, $mode) {
		//Add fields for input informations in all case
		$form->addElement ( 'text', 'name', tra ( 'name' ) );
		$form->addElement ( 'textarea', 'application', tra ( 'application' ), array ('size' => 40 ) );
		$form->addRule ( 'name', 'is required', 'required', null, 'server' );
		
		//Set select workflow stage
		$workflowStage = array ('asSold', 'asDefined', 'asBuild', 'asDelivred' );
		$params = array ('property_name' => 'life_cycle_stage', 'property_length' => 1, 'is_multiple' => false, 'is_required' => true );
		construct_select ( array_combine ( $workflowStage, $workflowStage ), $params, $form );
		
		$form->addElement ( 'reset', 'reset', 'reset' );
		$form->addElement ( 'submit', 'submit', tra ( 'Validate' ) );
		
		// Try to validate the form
		if ($form->validate ()) {
			$form->freeze (); //and freeze it
			// Form is validated, then processes the create request
			if ($mode == 'create') {
				$form->process ( array ($this, '_create' ), true );
			} else if ($mode == 'edit') {
				$form->process ( array ($this, '_modify' ), true );
			}
		} //End of validate form
		

		$form->display ();
		$this->error_stack->checkErrors ();
	
	} //End of method
	

	//---------------------------------------------------------------------
	public function _create($values) {
		$context = Rb_Pdm_Context_Productdefinition::get ( 0 );
		foreach ( $values as $property => $value ) {
			$context->setProperty ( $property, $value );
		}
		return $context->save ();
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function _modify($values) {
	} //End of method


} //End of class
