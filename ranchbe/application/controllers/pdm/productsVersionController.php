<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/pdm/abstract.php');
class Pdm_ProductsVersionController extends controllers_pdm_abstract {
	protected $page_id = 'pdmProductsVersion'; //(string)
	protected $ifSuccessForward = array ('module' => 'pdm', 'controller' => 'products', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'pdm', 'controller' => 'products', 'action' => 'index' );
	
	public function init() {
		parent::init ();
		
		$this->p_id = $this->getRequest ()->getParam ( 'p_id' );
		if (is_array ( $this->p_id )) {
			$this->p_ids = $this->p_id;
			$this->p_id = - 1; //to get a not initialized container object
		} else {
			$this->p_ids = array ($this->p_id );
		}
		
		return true;
	
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
	} //End of method
	

	//---------------------------------------------------------------------
	public function createAction() {
		//Ranchbe::checkPerm( 'create' , rb_pdm_acl_resource::get(-1) , true);
		

		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'createPdmProductVersion', 'post', $this->actionUrl ( 'create' ) );
		$form->addElement ( 'header', 'createheader', sprintf ( tra ( 'Create a pdm product version of %s' ), Rb_Pdm_Product::get ( $this->p_id )->getNumber () ) );
		$form->setWidth ( '600px' );
		
		//Set defaults values of elements if create request
		$form->setDefaults ( array ('name' => Rb_Pdm_Product::get ( $this->p_id )->getNumber (), 'description' => Rb_Pdm_Product::get ( $this->p_id )->getProperty ( 'description' ) ) );
		
		return $this->_finishEditForm ( $form, 'create' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function suppressAction() {
		//Ranchbe::checkPerm( 'suppress' , rb_pdm_acl_resource::get(-1) , true);
		$pv_id = $this->getRequest ()->getParam ( 'pv_id' );
		
		$version = Rb_Pdm_Product_Version::get ( $pv_id );
		//if(!Ranchbe::checkPerm( 'suppress' , $project , false)) return false;
		$version->suppress ();
		
		return $this->_successForward ();
	
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	protected function _finishEditForm(&$form, $mode) {
		//Add fields for input informations in all case
		$form->addElement ( 'text', 'name', tra ( 'name' ) );
		$form->addElement ( 'textarea', 'description', tra ( 'description' ), array ('size' => 40 ) );
		$form->addElement ( 'text', 'version', tra ( 'version' ) );
		
		$form->addRule ( 'name', 'is required', 'required', null, 'server' );
		$form->addRule ( 'version', 'is required', 'required', null, 'server' );
		$form->addRule ( 'cta_id', 'is required', 'required', null, 'server' );
		
		//Select context
		foreach ( Rb_Pdm_Context_Productdefinition::get ( - 1 )->getAll () as $cdef ) {
			$contexts [$cdef ['cta_id']] = $cdef ['name'] . '/' . $cdef ['application'];
		}
		$params = array ('property_name' => 'cta_id', 'property_description' => 'context definition', 'property_length' => 1, 'is_multiple' => false, 'is_required' => true, 'return_name' => false );
		construct_select ( $contexts, $params, $form );
		
		$form->addElement ( 'hidden', 'p_id', $this->p_id );
		$form->addElement ( 'submit', 'submit', tra ( 'Validate' ) );
		
		// Try to validate the form
		if ($form->validate ()) {
			//$form->freeze(); //and freeze it
			// Form is validated, then processes the create request
			if ($mode == 'create') {
				$form->process ( array ($this, '_create' ), true );
				return $this->_successForward ();
			} else if ($mode == 'edit') {
				$form->process ( array ($this, '_modify' ), true );
			}
		} //End of validate form
		

		$form->display ();
		$this->error_stack->checkErrors ();
	
	} //End of method
	

	//---------------------------------------------------------------------
	//Create a product version
	public function _create($values) {
		$product = & Rb_Pdm_Product::get ( $values ['p_id'] );
		//create product version
		$version = Rb_Pdm_Product_Version::get ( 0 );
		$version->setProperty ( 'version_id', $values ['version'] );
		$version->setProduct ( $product );
		$version->setProperty ( 'name', $values ['name'] );
		$version->setProperty ( 'description', $values ['description'] );
		//associate a existing document to this version
		if ($values ['document_space'] && $values ['document_id']) {
			$document = & Rb_Document::get ( $values ['document_space'], $values ['document_id'] );
			$version->setDocument ( $document );
		}
		//record version in database
		$version->save ();
		
		//Create a product definition
		$context1 = Rb_Pdm_Context_Productdefinition::get ( $values ['cta_id'] );
		$definition = & Rb_Pdm_Product_Definition::get ( 0 );
		$definition->setProductVersion ( $version );
		$definition->setContext ( $context1 ); //Set the primary context
		$definition->setProperty ( 'description', $values ['description'] );
		$definition->save ();
	} //End of method


} //End of class
