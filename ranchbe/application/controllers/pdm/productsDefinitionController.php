<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/pdm/abstract.php');
class Pdm_ProductsDefinitionController extends controllers_pdm_abstract {
	protected $page_id = 'pdmProductsDefinition'; //(string)
	protected $ifSuccessForward = array ('module' => 'pdm', 'controller' => 'Products', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'pdm', 'controller' => 'Products', 'action' => 'index' );
	
	public function init() {
		parent::init ();
		
		$this->pv_id = $this->getRequest ()->getParam ( 'pv_id' );
		if (is_array ( $this->pv_id )) {
			$this->pv_ids = $this->pv_id;
			$this->pv_id = - 1; //to get a not initialized container object
		} else {
			$this->pv_ids = array ($this->pv_id );
		}
		
		return true;
	
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
	} //End of method
	

	//---------------------------------------------------------------------
	public function createAction() {
		//Ranchbe::checkPerm( 'create' , rb_pdm_acl_resource::get(-1) , true);
		

		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'createPdmProductVersion', 'post', $this->actionUrl ( 'create' ) );
		$form->addElement ( 'header', 'createheader', sprintf ( tra ( 'Create a pdm product definition of %s' ), Rb_Pdm_Product_Version::get ( $this->pv_id )->getNumber () ) );
		$form->setWidth ( '600px' );
		
		//Set defaults values of elements if create request
		$form->setDefaults ( array ('description' => Rb_Pdm_Product_Version::get ( $this->pv_id )->getProperty ( 'description' ) ) );
		
		return $this->_finishEditForm ( $form, 'create' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function addchildAction() {
		//Ranchbe::checkPerm( 'create' , rb_pdm_acl_resource::get(-1) , true);
		

		$pd_id = $this->getRequest ()->getParam ( 'pd_id' );
		$definition = & Rb_Pdm_Product_Definition::get ( $pd_id );
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'addChild', 'post', $this->actionUrl ( 'addchild' ) );
		$form->addElement ( 'header', 'createheader', sprintf ( tra ( 'add a child to product %s' ), $definition->getProductVersion ()->getNumber () ) );
		$form->setWidth ( '600px' );
		
		//Set defaults values of elements if create request
		$form->setDefaults ( array ('description' => Rb_Pdm_Product_Version::get ( $this->pv_id )->getProperty ( 'description' ) ) );
		
		//Add fields for input informations in all case
		$form->addElement ( 'text', 'name', tra ( 'name' ), array ('size' => 40 ) );
		$form->addElement ( 'textarea', 'description', tra ( 'description' ), array ('size' => 40 ) );
		
		//Select product to add
		$products = Rb_Pdm_Product_viewa::get ()->getAll ();
		//  var_dump($products);die;
		foreach ( $products as $product_info ) {
			$selectSet [$product_info ['pd_id']] = $product_info ['p_number'] . '.' . $product_info ['version_id'];
		}
		$params = array ('property_name' => 'child_id', 'property_description' => 'products', 'property_length' => 5, 'is_multiple' => false, 'is_required' => true, 'return_name' => false );
		construct_select ( $selectSet, $params, $form );
		
		$form->addElement ( 'hidden', 'pv_id', $this->pv_id );
		$form->addElement ( 'hidden', 'pd_id', $pd_id );
		$form->addElement ( 'submit', 'submit', tra ( 'Validate' ) );
		
		// Try to validate the form
		if ($form->validate ()) {
			$form->process ( array ($this, '_addChild' ), true );
			return $this->_successForward ();
		} //End of validate form
		

		$form->display ();
		$this->error_stack->checkErrors ();
	
	} //End of method
	

	//---------------------------------------------------------------------
	public function getchildsAction() {
		$pd_id = $this->getRequest ()->getParam ( 'pd_id' );
		$p ['exact_find'] ['parent_id'] = $pd_id;
		$list = Rb_Pdm_Usage_View_Childs::get ()->getAll ( $p );
		$this->smarty->assign_by_ref ( 'list', $list );
		
		$this->smarty->assign ( 'sameurl', './pdm/productsDefinition/getChilds' ); //important: is first assign
		$this->smarty->assign ( 'PageTitle', sprintf ( tra ( 'Childs of %s' ), Rb_Pdm_Product_Definition::get ( $pd_id )->getNumber () ) );
		
		RbView_Menu::get ()->init ()->getPdm ();
		RbView_Tab::get ( 'productsTab' )->activate ();
		Ranchbe::getSmarty ()->assign ( 'mid', 'pdm/products/get.tpl' );
		Ranchbe::getSmarty ()->display ( 'ranchbe.tpl' );
	
	}
	
	//---------------------------------------------------------------------
	public function suppressAction() {
		//Ranchbe::checkPerm( 'suppress' , rb_pdm_acl_resource::get(-1) , true);
		$pd_id = $this->getRequest ()->getParam ( 'pd_id' );
		
		$definition = & Rb_Pdm_Product_Definition::get ( $pd_id );
		//if(!Ranchbe::checkPerm( 'suppress' , $project , false)) return false;
		$definition->suppress ();
		
		return $this->_successForward ();
	
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	protected function _finishEditForm(&$form, $mode) {
		//Add fields for input informations in all case
		$form->addElement ( 'textarea', 'description', tra ( 'description' ), array ('size' => 40 ) );
		
		$form->addRule ( 'cta_id', 'is required', 'required', null, 'server' );
		
		//Select context
		foreach ( Rb_Pdm_Context_Productdefinition::get ( - 1 )->getAll () as $cdef ) {
			$contexts [$cdef ['cta_id']] = $cdef ['name'] . '/' . $cdef ['application'];
		}
		$params = array ('property_name' => 'cta_id', 'property_description' => 'context definition', 'property_length' => 1, 'is_multiple' => false, 'is_required' => true, 'return_name' => false );
		construct_select ( $contexts, $params, $form );
		
		$form->addElement ( 'hidden', 'pv_id', $this->pv_id );
		$form->addElement ( 'submit', 'submit', tra ( 'Validate' ) );
		
		// Try to validate the form
		if ($form->validate ()) {
			//$form->freeze(); //and freeze it
			// Form is validated, then processes the create request
			if ($mode == 'create') {
				$form->process ( array ($this, '_create' ), true );
				return $this->_successForward ();
			} else if ($mode == 'edit') {
				$form->process ( array ($this, '_modify' ), true );
			}
		} //End of validate form
		

		$form->display ();
		$this->error_stack->checkErrors ();
	
	} //End of method
	

	//---------------------------------------------------------------------
	//Create a product version
	public function _create($values) {
		$version = & Rb_Pdm_Product_Version::get ( $values ['pv_id'] );
		//Create a product definition
		$context1 = Rb_Pdm_Context_Productdefinition::get ( $values ['cta_id'] );
		$definition = & Rb_Pdm_Product_Definition::get ( 0 );
		$definition->setProductVersion ( $version );
		$definition->setContext ( $context1 ); //Set the primary context
		$definition->setProperty ( 'description', $values ['description'] );
		$definition->save ();
	} //End of method
	

	//---------------------------------------------------------------------
	//Create a product version
	public function _addChild($values) {
		//get a product definition
		$product1 = & Rb_Pdm_Product_Definition::get ( $values ['pd_id'] );
		$product2 = & Rb_Pdm_Product_Definition::get ( $values ['child_id'] );
		//instanciate Rb_Pdm_Usage
		$usage = Rb_Pdm_Usage::get ( 0 );
		$usage->setParent ( $product1 ); //Set the parent
		$usage->setChild ( $product2 ); //Set the child
		//set the properties
		$usage->setProperty ( 'name', $values ['name'] );
		$usage->setProperty ( 'description', $values ['description'] );
		$usage->save ();
	} //End of method


} //End of class
