<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


class Document_DetailController extends controllers_document_abstract {
	protected $page_id = 'documentDetail'; //(string)
	protected $ifSuccessForward = array ('module' => 'document', 'controller' => 'detail', 'action' => 'get' );
	protected $ifFailedForward = array ('module' => 'document', 'controller' => 'detail', 'action' => 'get' );
	
	
	public function init() {
		parent::init ();
		$this->document =& Rb_Document::get ( $this->space_name, $this->document_id );
		$this->view->odocument = $this->document; //register object to use in smarty plugins
		$this->view->assign ( 'document_id', $this->document_id );
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward('getdetailwindow');
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		return $this->_forward('getdetailwindow');
	} //End of method
	

	//---------------------------------------------------------------------
	public function getdetailwindowAction() {
		Ranchbe::getLayout ()->setLayout ( 'document/detail/layout' );
		if (! Ranchbe::checkPerm ( 'document_get', $this->document, false )) {
			$this->error_stack->checkErrors ();
			return;
		}
		$this->_helper->actionStack ( 'getdocumentdetail' );
		$this->_helper->actionStack ( 'getassociatedfiles' );
		$this->_helper->actionStack ( 'getstructure' );
		$this->_helper->actionStack ( 'getinstances' );
		
		//To hide/show the tabs
		$this->view->assign ( 'content1display', 'block' );
		$this->view->assign ( 'content2display', 'none' );
		$this->view->assign ( 'content3display', 'none' );
		$this->view->assign ( 'content4display', 'none' );
		$this->error_stack->checkErrors ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function getdocumentdetailAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'document_detail' );
		$this->_getdocumentdetail ();
		$this->error_stack->checkErrors ();
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _getdocumentdetail() {
		require_once ('HTML/QuickForm.php'); //Librairy to easily create forms
		require_once ('HTML/QuickForm/Renderer/ArraySmarty.php'); //Lib to use Smarty with QuickForm
		$this->view->docinfos = $this->document->getProperties ();
		$this->view->document_name = $this->view->docinfos ['document_number'];
		$this->view->process_instance_id = $this->view->docinfos ['process_instance_id'];
		$this->view->assign ( 'optionalFields', $this->document->getMetadatas () );
		//$smarty->assign('file_icons_dir', DEFAULT_FILE_ICONS_DIR);
		//$this->view->assign('icons_dir', DEFAULT_DOCTYPES_ICONS_DIR);
		//$this->view->assign('reposit_dir', $this->container->getProperty('default_file_path'));
		//$this->view->assign('thumbs_dir', $this->container->DEFAULT_REPOSIT_DIR.'/__attachments/_thumbs');
		//$this->view->assign('thumbs_extension', '.png');
		//$this->view->assign ( 'documentDetail', $this->view->fetch ( 'documentManager/detail.tpl' ) );
	} //End of method
	

	//---------------------------------------------------------------------
	public function getassociatedfilesAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'associated_files' );
		$this->_getassociatedfiles ();
		$this->error_stack->checkErrors ();
		//$this->view->display ( 'documentManager/documentDetailLayout.tpl' );
	} //End of method
	

	//---------------------------------------------------------------------
	/* Monitor associated files
*/
	protected function _getassociatedfiles() {
		//get all file associated to the selected document
		$p = array ('exact_find' => array ('file_life_stage' => 1 ) );
		$this->view->list = Rb_Docfile::get ( $this->space_name )->getAll ( $p, $this->document->getId () );
		/*
		  $list = array();
		  $roles = $this->document->getDocfiles('byRole');
		  if(is_array($roles))
		  foreach($roles as $role_id=>$docfiles){
		    foreach($docfiles as $docfile){
		      $docfile_properties = array();
		      $docfile_properties = $docfile->getProperties();
		      $docfile_properties['role_id'][] = $role_id;
		      $list[] = $docfile_properties;
		    }
		  }
		  */
		
		//Assign additional var to add to URL when redisplay the same url
		//$sameurl_elements[] = 'documentDetail';
		$this->view->assign ( 'object_class', 'docfile' ); //used to view document from template
	} //End of method
	

	//---------------------------------------------------------------------
	public function getstructureAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'structure' );
		$this->_getstructure ();
		$this->error_stack->checkErrors ();
	} //End of method
	

	//---------------------------------------------------------------------
	/* Monitor Relationships
*/
	protected function _getstructure() {
		$docrel = new Rb_Doclink ( $this->document );
		$this->view->doclinked_list = $docrel->getSons ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function getalternativesAction() {
		$this->_getalternatives ();
		$this->error_stack->checkErrors ();
	} //End of method


	//---------------------------------------------------------------------
	/* Monitor Alternatives
	*/
	/*
	private function _getalternatives(){
	  $alternative = new alternative($odocument);
	  $alternative_list = $alternative->GetAlter();
	  $odocument->error_stack->checkErrors();
	  $this->view->assign_by_ref('alternative_list', $alternative_list);
	
	} //End of method
	*/

	//---------------------------------------------------------------------
	public function getinstancesAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'instances' );
		$this->_getinstances ();
		$this->error_stack->checkErrors ();
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _getinstances() {
		$workflow = new Rb_Workflow_Docflow($this->document);
		$processMonitor = new Galaxia_Monitor_Process ( Ranchbe::getDb() );
		$offset = 0;
		$maxRecords = 500;
		$sort_mode = 'instanceId_asc';
		$find = '';
		$where = "gi.name='" . $this->document->getProperty('normalized_name') . "'";
		$items = $processMonitor->monitor_list_instances($offset, $maxRecords, $sort_mode, $find, $where);
		$this->view->instances = $items['data'];
	} //End of method

	
	//---------------------------------------------------------------------
	public function getactivitiesAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'activities' );
		$this->_getactivities ();
		$this->error_stack->checkErrors ();
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _getactivities() {
		$aid = $this->getRequest()->getParam('aid');
		$workflow = new Rb_Workflow_Docflow($this->document);
		$workflow->initInstance();
		$workflow->getActivity();
		$this->view->instance_infos = $workflow->getInstanceInfos();
	} //End of method

	
} //End of class
