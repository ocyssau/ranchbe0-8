<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/document/abstract.php');
class Document_CreateController extends controllers_document_abstract {
	protected $page_id = 'Document_Create'; //(string)
	protected $ifSuccessForward = array ('module' => 'document', 
											'controller' => 'create', 
											'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'document', 
											'controller' => 'create', 
											'action' => 'index' );
	
	//----------------------------------------------------------------------------------------------------
	protected function _getFormBase($validate, $save) {
		$this->view->dojo()->enable();
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		
		$form = new ZendRb_Form();
		$form->getElement('save')->setLabel(tra('Next'));
		
		$space_name = $this->getRequest ()->getParam ( 'space' );
		if(!$space_name) $space_name = $this->session->space_name;
		if(!$space_name) $space_name = Ranchbe::getContext ()->getProperty ( 'space' );
		$this->session->space_name = $space_name;
		
		$container_id = $this->getRequest ()->getParam ( 'container_id' );
		if(!$container_id) $container_id = $this->session->container_id;
		if(!$container_id) $container_id = Ranchbe::getContext ()->getProperty ( 'container_id' );
		$this->session->container_id = $container_id;
		
		$container = & Rb_Container::get ( $space_name, $container_id );
		Ranchbe::checkPerm ( 'document_create', $container, true );
		
		$this->view->PageTitle = sprintf ( tra ( 'Create a document in %s' ), $container->getName () );
		
		$form->addElement('TextBox','document_name', array(
				'label' => tra ( 'Name' ),
				'value' => tra( 'Document name' ),
				'trim'  => true,
				'required' => false,
				'size' =>50,
				));
		
		$form->addElement('Textarea', 'description', array(
				'label' => tra ( 'Description' ),
				'value' => tra( 'New document' ),
				'trim'  => true,
				'required' => true,
				'style' => 'width: 400px;',
				'filters' => array('StringTrim', 'StringToLower'),
				'validators' => array(
								    array('NotEmpty', true),
								    array('stringLength', false, array(0,255)),
								),
				));
				
		$form->addElement( new ZendRb_Form_Element_SelectIndice('document_version', array(
				'label' => tra ( 'indice' ),
				'value' => 0,
				'allowEmpty'=> false,
				'returnName'=> false,
				'advSelect'=> false,
				'required' => true,
				'multiple' => false,
				'optionsList'=> 0,
				)));
		
		$form->addElement('SelectContainer', 'container_id', array(
				'label' => tra ( 'Container' ),
				'required' => true,
				'allowEmpty'=> false,
				'returnName'=> false,
				'advSelect'=> false,
				'multiple' => false,
				'source'=> Rb_Container::get($space_name),
		));
		
		$form->setAttrib('enctype', 'multipart/form-data');
		
		//Add select file
		$form->addElement('File','upload_file', array(
				'label' => tra ( 'Associated file' ),
				'size' =>50,
				'destination' => Rb_User::getCurrentUser()->getWildspace()->getPath(),
				'required' => false,
				));

		if($this->session->assoc_file_name){
			$form->addElement('StaticString','repeat_file_name', array(
					'label' => tra ( 'File name' ),
					'value' => basename($this->session->assoc_file_name),
					));
		}
		
		//$form->getElement('assoc_file_name')
		//	->addValidator('Count', false, 1);
		//	->addValidator('Size', false, 102400);
		//	->addValidator('Extension', false, 'jpg,png,gif');
		
		foreach( $form->getElements() as $element ){
			//var_dump( $element->getName() , $this->session->{$element->getName()} );
			if( $this->session->{$element->getName()} ){
				$form->setDefault( $element->getName(), $this->session->{$element->getName()} );
			}
		}
		
		if ($validate && $form->isValid($this->getRequest ()->getPost())) {
			$normalizeFilter = new ZendRb_Filter_NormalizeFileName();
			$form->upload_file->receive();
			$this->session->document_version = $this->getRequest()->getParam( 'document_version' );
			$this->session->description = $this->getRequest()->getParam( 'description' );
			$docName = $this->getRequest()->getParam( 'document_name' );
			$fileName = $form->upload_file->getFileName();
			if(!$fileName) $fileName = $this->session->assoc_file_name;
			if($docName){
				$this->session->document_name = $docName;
				$this->session->document_number = $normalizeFilter->filter( $docName );
			}
			if($fileName){
				$file = new Rb_File($fileName);
				$normalizedFileName = $normalizeFilter->filter( $file->getProperty('file_name') );
				if( $normalizedFileName != $file->getProperty('file_name')){
					$normalizedFullPath = $file->getProperty('file_path') .'/'. $normalizedFileName;
					if( is_file($normalizedFullPath) ){
						$file2 = new Rb_File($normalizedFullPath);
						$file2->putInTrash();
					}
					$file->rename($normalizedFullPath);
				}
				$this->session->assoc_file_name = $file->getProperty('file');
				$this->session->document_number = $file->getProperty('file_root_name');
			}
			if($fileName && $docName){
				$this->session->document_name = $docName;
			}
		}
		
		/*
		if( $save ){
			foreach($form->getElements() as $element){
				$this->session->{$element->getName()} = $this->getRequest()->getParam( $element->getName() );
			}
		}
		*/
		
		return $form;
	} //End of method

	
	//----------------------------------------------------------------------------------------------------
	protected function _getFormCategory($validate, $save) {
		$this->view->dojo()->enable();
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		
		$container_id = $this->session->container_id;
		$space_name = $this->session->space_name;
		$container = & Rb_Container::get ( $space_name, $container_id );
		
		$this->view->PageTitle = sprintf ( tra ( 'Create a document in %s' ), $container->getName () );
		
		$form = new ZendRb_Form();
		$form->getElement('save')->setLabel(tra('Next'));
		
		if($this->session->assoc_file_name){
			$form->addElement('StaticString','repeat_file_name', array(
					'label' => tra ( 'File name' ),
					'size' =>50,
					'value' => basename($this->session->assoc_file_name),
					'required' => false,
					));
			$form->addElement('StaticString','repeat_doc_name', array(
					'label' => tra ( 'Number' ),
					'value' => $this->session->document_number,
					));
		}else{
			//$alphafilter = new Zend_Filter_Alpha(false);
			$form->addElement('ValidationTextBox','document_number', array(
					'label' => tra ( 'Number' ),
					'size' =>50,
					'value' => '',
					'required' => true,
					'propercase' => true,
					'regExp' => Ranchbe::getConfig ()->document->mask,
					'invalidMessage' => utf8_encode(Ranchbe::getConfig ()->document->maskhelp),
					'filters' => array('StringTrim'),
			));
		}
		
		$form->addElement('TextBox','document_name', array(
				'label' => tra ( 'Name' ),
				'size' =>50,
				'value' => '',
				'required' => false,
				'propercase' => true,
				));
		
		//@TODO : get category from doctype/container links settings
		//$category_id = ;
		$form->addElement('SelectCategory','category_id', array(
				'label' => tra ( 'category' ),
				'value' => $category_id,
				'allowEmpty'=> true,
				'returnName'=>false,
				'advSelect'=>false,
				'required' => false,
				'multiple' => false,
				'source'=> Rb_Container::get('workitem'),
				));
				
		foreach( $form->getElements() as $element ){
			if( $this->session->{$element->getName()} ){
				$form->setDefault( $element->getName(), $this->session->{$element->getName()} );
			}
		}
		
		if ($validate && $form->isValid($this->getRequest ()->getPost())) {
		}
		
		if( $save ){
			foreach($form->getElements() as $element){
				$this->session->{$element->getName()} = $this->getRequest()->getParam( $element->getName() );
			}
		}
		
		return $form;
	} //End of method

	//----------------------------------------------------------------------------------------------------
	protected function _getFormExtend( $validate, $save) {
		$this->view->dojo()->enable();
		$form = new ZendRb_Form();
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		
		$category_id = $this->session->category_id;
		$container_id = $this->session->container_id;
		$space_name = $this->session->space_name;
		$container = & Rb_Container::get ( $space_name, $container_id );
		$document = Rb_Document::get($space_name, 0);
		$document->setNumber($this->session->document_number);
		$document->setContainer($container);
		
		if($this->session->assoc_file_name){
			$fsdata = new Rb_Fsdata( $this->session->assoc_file_name );
			$doctype =& $document->setDocType($fsdata->getProperty('file_extension'),
													$fsdata->getProperty('file_type'));
		}else{
			$doctype =& $document->setDocType('','nofile');
		}

		if($doctype){
			$form->addElement('StaticString','doctype_name', array(
					'label' => tra ( 'Reconize doctype' ),
					'value' => $doctype->getProperty('doctype_number'),
					));
		}else{
			$form->removeElement('save');
			return $form;
		}
		
		if ($category_id != 0) {
			$optionalFields = Rb_Category_Relation_Metadata::get ()->getMetadatas ( $category_id );
		} else { //--Get the metadatas from the container links
			$optionalFields = Rb_Container_Relation_Docmetadata::get ()->getMetadatas ( $container->getId () );
		}
		
		foreach ( $optionalFields as $property ) {
			$element = ZendRb_Form_Element_Creator::getElement( $property );
			//$element->setValue( $this->document->getProperty ( $field ['property_fieldname'] ) );
			$form->addElement( $element );
		}
		
		$form->addElement( new Zend_Form_Element_Checkbox('lock', array(
										'label' => tra ( 'Lock the document after store' ),
										'order' => 0,
										'value' => 0,
										)
									));
									
		$form->addElement( new Zend_Form_Element_Checkbox('suppress', array(
										'label' => tra ( 'Suppress files from wildspace after store' ),
										'order' => 1,
										'value' => 1,
										)
									));

		$this->view->optionalFields = $optionalFields;
		
		foreach( $form->getElements() as $element ){
			if( $this->session->{$element->getName()} ){
				$form->setDefault( $element->getName(), $this->session->{$element->getName()} );
			}
		}
		
		if( $save ){
			foreach($form->getElements() as $element){
				$this->session->extends->{$element->getName()} = $this->getRequest()->getParam( $element->getName() );
			}
		}
		
		if ($validate && $form->isValid($this->getRequest ()->getPost())) {
			if( $this->_create($this->session, $form ) ){
				$this->view->successMessage = tra('Save is successfull');
				$this->session->unsetAll();
				$form = false;
			}else{
				$this->view->failedMessage = tra('Save has failed');
			}
		}
		
		return $form;
	} //End of method
	
	
	//---------------------------------------------------------------------
	protected function _create(Zend_Session_Namespace $session, Zend_Form $form) {
		/*
		  $indice             = $values['indice'];
		  $description        = $values['description'];
		  $file               = $values['file'];
		  $SelectedContainer  = $values['SelectedContainer'];
		  $category           = $values['category'];
		  $lock               = $values['lock'];
		  */
		/*
		$lock = $values ['lock'];
		unset ( $values ['lock'] );
		unset ( $values ['ticket'], $values ['redisplay'], $values ['validate'] );
		
		$document = new Rb_Document( Rb_Space::get($values['space_name']) );
		foreach ( $values as $key => $val ) {
			if (is_a($form->getElement ( $key ), 'ZendRb_Form_Element_Date'))
				$values [$key] = Rb_Date::makeTimestamp ( $values [$key] );
			if (is_array ( $val )) {
				$values [$key] = implode ( '#', $val );
			}
			$document->setProperty ( $key, $val );
		}
		if ($lock) $document->lockDocument ( 11, false );
		return $document->save ();
		*/
		$fsdata = false;
		$space_name = $session->space_name;
		$container_id = $session->container_id;
		$container = & Rb_Container::get ( $space_name, $container_id );
		$document_number = $session->document_number;
		$assoc_file_name = $session->assoc_file_name;
		$category_id = (int) $session->category_id;
		$document_name = $session->document_name;
		$description = $session->description;
		$document_version = (int) $session->document_version;
		$lock = (boolean) $session->lock;
		$suppress = (boolean) $session->suppress;
		$extends = $session->extends;
		
		$smartStore = new RbView_SmartStore($space_name);
		if( $assoc_file_name ){
			$fsdata = new Rb_Fsdata($assoc_file_name);
			if( $smartStore->isDocfile($fsdata) ){
				$errors = $smartStore->getErrors();
				Ranchbe::getError()->info( $errors[0] );
				return false;
			}
		}
		if( $smartStore->isDocument($document_number) ){
			$errors = $smartStore->getErrors();
			Ranchbe::getError()->info( $errors[0] );
			return false;
		}
		$smartStore->initNewDocument($container, $fsdata, $document_number);
		$smartStore->getDocument()
			->setProperty( 'category_id', $category_id )
			->setProperty( 'document_name', $document_name )
			->setProperty( 'description', $description )
			->setProperty( 'document_version', $document_version );
			
		if($extends)
		foreach( $extends as $prop_name=>$prop_value){
			$smartStore->getDocument()->setProperty( $prop_name, $prop_value );
		}
		return $smartStore->getDocument()->store($lock, $suppress);
		//return false;
	} //End of function
	
	//---------------------------------------------------------------------
	public function indexAction() {
		if ($this->getRequest ()->getParam ( 'cancel' )) {
			return $this->_cancel ();
		}
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		
		$step = (int) $this->getRequest ()->getParam ( 'step' );
		
		$init = $this->getRequest ()->getParam ( 'init' );
		$session_name_space = $this->getRequest ()->getParam ( 'session_name_space' );
		
		if( $init || !$session_name_space ){
			$session_name_space = 'document_index_create_'.time();
			$step = 1;
		}
		
		$this->session = new Zend_Session_Namespace ( $session_name_space );
		
		$getToNext = $this->getRequest ()->getParam('save');
		$getToPrev = $this->getRequest ()->getParam('prev');
		if(!$step) $step = 1;
		$prev = $step - 1;
		$next = $step + 1;
		
		$ticket = $this->getRequest ()->getParam ( 'ticket' );
		if(!$ticket) $ticket = $this->session->ticket;
		
		$this->session->ticket = $ticket;
		
		$cnt = 0;
		while(true){
			if( $cnt++ > 10) break;
			if( ($getToNext || $getToPrev) && $step == $this->getRequest()->getParam('step') ){
				$save = true;
			}else{
				$save = false;
			}
			
			if ($step == 1) {
				if($getToNext) {
					$step = $next;
					$validate = true;
					$getToNext = false;
				}else $validate = false;
				$form = $this->_getFormBase ($validate, $save);
			}
			if ($step == 2){
				if($getToNext) {
					$step = $next;
					$validate = true;
					$getToNext = false;
				}else $validate = false;
				$form = $this->_getFormCategory ($validate, $save);
			}
			if ($step == 3){
				if($getToNext) {
					$step = 3;
					$validate = true;
					$getToNext = false;
				}else $validate = false;
				$form = $this->_getFormExtend ($validate, $save);
			}
			
			if(!$getToNext){
				break;
			}
		}

		if($form){
			if( $getToPrev ){
				return $this->_forward('index', 'create', 'document', array(
					'step'=>$prev,
					'prev'=>0,
					'session_name_space'=>$session_name_space,
				));
			}
			$form->setAction( $this->_helper->url('index') );
			$form->addElement( 'hidden', 'step', array('value'=>$step) );
			$form->addElement( 'hidden', 'session_name_space', array('value'=>$session_name_space) );
			if($step > 1){
				$form->addElement( 'SubmitButton', 'prev', array(
													'label' => tra('Previous'),
													'order'=>999));
			}
			$this->view->form = $form;
		}
		
		Ranchbe::getError()->checkErrors ();
		$this->render ( 'default/editform', null, true );
		return;
		
	} //End of method
	
} //End of class
