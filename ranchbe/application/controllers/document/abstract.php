<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');
class controllers_document_abstract extends controllers_abstract {
	protected $document; //(object)
	protected $document_id = 0; //(int)
	protected $ticket = ''; //(String)
	
	/**
	 * return void
	 */
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->space_name = $this->getRequest ()->getParam ( 'space' );
		$this->page_id = $this->space_name . $this->page_id;
		$this->ticket = $this->getRequest ()->getParam ( 'ticket' );
		$this->document_id = $this->getRequest ()->getParam ( 'document_id' );
		if (is_array ( $this->document_id )) {
			$this->document_ids = $this->document_id;
			$this->document_id = current ( $this->document_ids ); //set to first id so when only 1 selected if always possible to use
		} else if (! is_null ( $this->document_id )) {
			$this->document_id = ( int ) $this->document_id;
			$this->document_ids = array ($this->document_id );
		} else {
			$this->document_id = 0; //to get a not initialized container object
			$this->document_ids = array ();
		}
		if($this->space_name){
			$this->space = & Rb_Space::get ( $this->space_name );
			$this->view->assign ( 'SPACE_NAME', $this->space_name );
			Zend_Registry::set ( 'space', $this->space );
		}		
		$this->_initForward ();
	} //End of method


} //End of class
