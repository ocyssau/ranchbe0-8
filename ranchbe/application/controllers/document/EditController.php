<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/document/abstract.php');
class Document_EditController extends controllers_document_abstract {
	protected $page_id = 'Document_Edit'; //(string)
	protected $ifSuccessForward = array ('module' => 'document', 
											'controller' => 'edit', 
											'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'document', 
											'controller' => 'edit', 
											'action' => 'index' );
	
	//----------------------------------------------------------------------------------------------------
	protected function _getFormBase($validate, $save) {
		$this->view->dojo()->enable();
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		
		$form = new ZendRb_Form();
		$form->getElement('save')->setLabel(tra('Next'));
		
		$space_name = $this->session->space_name;
		
		$container_id = $this->getRequest ()->getParam ( 'container_id' );
		if(!$container_id) $container_id = $this->session->container_id;
		if(!$container_id) $container_id = Ranchbe::getContext ()->getProperty ( 'container_id' );
		$this->session->container_id = $container_id;
		
		$container = & Rb_Container::get ( $space_name, $container_id );
		Ranchbe::checkPerm ( 'document_Edit', $container, true );
		
		$this->view->PageTitle = sprintf ( tra ( 'Edit a document in %s' ), $container->getName () );
		
		$form->addElement('TextBox','document_name', array(
				'label' => tra ( 'Name' ),
				'value' => $this->document->getName(),
				'trim'  => true,
				'required' => false,
				'size' =>50,
				));
		
		$form->addElement('Textarea', 'description', array(
				'label' => tra ( 'Description' ),
				'value' => $this->document->getProperty('description'),
				'trim'  => true,
				'required' => true,
				'style' => 'width: 400px;',
				'filters' => array('StringTrim', 'StringToLower'),
				'validators' => array(
								    array('NotEmpty', true),
								    array('stringLength', false, array(0,255)),
								),
				));
				

		
		foreach( $form->getElements() as $element ){
			if( $this->session->{$element->getName()} ){
				$form->setDefault( $element->getName(), $this->session->{$element->getName()} );
			}
		}
		
		if ($validate && $form->isValid($this->getRequest ()->getPost())) {
			$normalizeFilter = new ZendRb_Filter_NormalizeFileName();
			$this->session->description = $this->getRequest()->getParam( 'description' );
			$this->session->document_name = $this->getRequest()->getParam( 'document_name' );
		}
		return $form;
	} //End of method

	
	//----------------------------------------------------------------------------------------------------
	protected function _getFormCategory($validate, $save) {
		$this->view->dojo()->enable();
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		
		$container_id = $this->session->container_id;
		
		
		$space_name = $this->session->space_name;
		
		$container = & Rb_Container::get ( $space_name, $container_id );
		
		$this->view->PageTitle = sprintf ( tra ( 'Edit a document in %s' ), $container->getName () );
		
		$form = new ZendRb_Form();
		$form->getElement('save')->setLabel(tra('Next'));
		$form->addElement('TextBox','document_name', array(
				'label' => tra ( 'Name' ),
				'size' =>50,
				'value' => $this->session->document_name,
				'required' => false,
				'propercase' => true,
				));
				
				
		//@@todo : get category from doctype/container links settings
		//$category_id = ;
		$form->addElement('SelectCategory','category_id', array(
				'label' => tra ( 'category' ),
				'value' => $category_id,
				'allowEmpty'=> true,
				'returnName'=>false,
				'advSelect'=>false,
				'required' => false,
				'multiple' => false,
				'source'=> Rb_Container::get('workitem'),
		));
		
				
		foreach( $form->getElements() as $element ){
			if( $this->session->{$element->getName()} ){
				$form->setDefault( $element->getName(), $this->session->{$element->getName()} );
			}
		}
		
		if ($validate && $form->isValid($this->getRequest ()->getPost())) {
		}
		
		if( $save ){
			foreach($form->getElements() as $element){
				$this->session->{$element->getName()} = $this->getRequest()->getParam( $element->getName() );
			}
		}
				
		return $form;
	} //End of method

	//----------------------------------------------------------------------------------------------------
	protected function _getFormExtend( $validate, $save) {
		$this->view->dojo()->enable();
		$form = new ZendRb_Form();
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		
		$category_id = $this->session->category_id;
		$container_id = $this->session->container_id;
		
		$space_name = $this->session->space_name;
		$container = & Rb_Container::get ( $space_name, $container_id );

		$mainDocfile = $this->document->getDocfile('main');
		
		if( $mainDocfile ){
			$doctype =& $this->document->setDocType($mainDocfile->getProperty('file_extension'),
													$mainDocfile->getProperty('file_type'));
		}else{
			$doctype =& $this->document->setDocType('','nofile');
			
		}

		if($doctype){
			$form->addElement('StaticString','doctype_name', array(
					'label' => tra ( 'Reconize doctype' ),
					'value' => $doctype->getProperty('doctype_number'),
					));
		}else{
			$form->removeElement('save');
			return $form;
		}
		

		
		if ($category_id != 0) {
			$optionalFields = Rb_Category_Relation_Metadata::get ()->getMetadatas ( $category_id );
		} else { //--Get the metadatas from the container links
			$optionalFields = Rb_Container_Relation_Docmetadata::get ()->getMetadatas ( $container->getId () );
		}
		
		foreach ( $optionalFields as $property ) {
			$element = ZendRb_Form_Element_Creator::getElement( $property );
			//$element->setValue( $this->document->getProperty ( $field ['property_fieldname'] ) );
			$form->addElement( $element );
		}
		
		
		
		$form->addElement( new Zend_Form_Element_Checkbox('lock', array(
										'label' => tra ( 'Lock the document after store' ),
										'order' => 0,
										'value' => 0,
										)
									));
									
		$form->addElement( new Zend_Form_Element_Checkbox('suppress', array(
										'label' => tra ( 'Suppress files from wildspace after store' ),
										'order' => 1,
										'value' => 1,
										)
									));

		$this->view->optionalFields = $optionalFields;
		
		foreach( $form->getElements() as $element ){
			if( $this->session->{$element->getName()} ){
				$form->setDefault( $element->getName(), $this->session->{$element->getName()} );
			}
		}
		
		if( $save ){
			foreach($form->getElements() as $element){
				$this->session->extends->{$element->getName()} = $this->getRequest()->getParam( $element->getName() );
			}
		}
		
		if ($validate && $form->isValid($this->getRequest ()->getPost())) {
			if( $this->_create($this->session, $form ) ){
				$this->view->successMessage = tra('Save is successfull');
				$this->session->unsetAll();
				$form = false;
			}else{
				$this->view->failedMessage = tra('Save has failed');
			}
		}
		
		return $form;
	} //End of method
	
	
	//---------------------------------------------------------------------
	protected function _create(Zend_Session_Namespace $session, Zend_Form $form) {
		$fsdata = false;
		
		$space_name = $this->session->space_name;
		$category_id = (int) $this->session->category_id;
		
		$document_name = $this->session->document_name;
		
		$description = $this->session->description;
		
		
		$document_version = (int) $this->session->document_version;
		
		$lock = (boolean) $this->session->lock;
		
		$suppress = (boolean) $this->session->suppress;
		
		$extends = $this->session->extends;
		
	
		$this->document->setProperty( 'category_id', $category_id )
			->setProperty( 'document_name', $document_name )
			->setProperty( 'description', $description );
			
		$this->document->setName( $document_name );		
		if($extends)
		foreach( $extends as $prop_name=>$prop_value){
			$this->document->setProperty( $prop_name, $prop_value );
		}
		return $this->document->save();
		//return false;
	} //End of function
	
	//---------------------------------------------------------------------
	public function indexAction() {
		if ($this->getRequest ()->getParam ( 'cancel' )) {
			return $this->_cancel ();
		}
		
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		
		$step = (int) $this->getRequest ()->getParam ( 'step' );
		
		$init = $this->getRequest ()->getParam ( 'init' );
		$session_name_space = $this->getRequest ()->getParam ( 'session_name_space' );
	
		if( $init || !$session_name_space ){
			$session_name_space = 'document_index_edit_'.time();
			$step = 1;
		}
		
		if(!$step) $step = 1;
		$prev = $step - 1;
		$next = $step + 1;
		
		$this->session = new Zend_Session_Namespace ( $session_name_space );
		
		if( $init ){
			$this->session->document_id = $this->getRequest ()->getParam ( 'document_id' );
			$this->session->space_name = $this->getRequest ()->getParam ( 'space' );
		}
		$this->document = Rb_Document::get ( $this->session->space_name, $this->session->document_id);

		$getToNext = $this->getRequest ()->getParam('save');
		$getToPrev = $this->getRequest ()->getParam('prev');
		
		
		$ticket = $this->getRequest ()->getParam ( 'ticket' );
		if(!$ticket) $ticket = $this->session->ticket;
		
		$this->session->ticket = $ticket;
		
		$cnt = 0;
		while(true){
			if( $cnt++ > 10) break;
			if( ($getToNext || $getToPrev) && $step == $this->getRequest()->getParam('step') ){
				$save = true;
			}else{
				$save = false;
			}
			if ($step == 1) {
				if($getToNext) {
					$step = $next;
					$validate = true;
					$getToNext = false;
				}else $validate = false;
				$form = $this->_getFormBase ($validate, $save);
			}

			if ($step == 2){
				if($getToNext) {
					$step = $next;
					$validate = true;
					$getToNext = false;
				}else $validate = false;
				$form = $this->_getFormCategory ($validate, $save);
			}
			if ($step == 3){
				if($getToNext) {
					$step = 3;
					$validate = true;
					$getToNext = false;
				}else $validate = false;
				$form = $this->_getFormExtend ($validate, $save);
			}
			
			if(!$getToNext){
				break;
			}
		}

		if($form){
			if( $getToPrev ){
				return $this->_forward('index', 'edit', 'document', array(
					'step'=>$prev,
					'prev'=>0,
					'session_name_space'=>$session_name_space,
				));
			}
			$form->setAction( $this->_helper->url('index') );
			$form->addElement( 'hidden', 'step', array('value'=>$step) );
			$form->addElement( 'hidden', 'session_name_space', array('value'=>$session_name_space) );
			if($step > 1){
				$form->addElement( 'submit', 'prev', array(
													'label' => tra('Previous'),
													'order'=>999));
			}
			$this->view->form = $form;
		}
		
		Ranchbe::getError()->checkErrors ();
		$this->render ( 'default/editform', null, true );
		return;
		
	} //End of method
	
	
	
} //End of class
