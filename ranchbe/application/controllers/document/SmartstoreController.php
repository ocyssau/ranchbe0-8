<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/content/abstract.php');
class Document_SmartstoreController extends controllers_content_abstract {
	protected $document; //(object)
	protected $docfile; //(object)
	protected $report = array (); //(array)
	protected $actions = array (); //(array)
	protected $page_id = 'documentManager_smartstore'; //(string)
	protected $ifSuccessForward = array ('module' => 'document', 
										'controller' => 'smartstore', 
										'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'document', 
										'controller' => 'smartstore', 
										'action' => 'index' );
	
	//---------------------------------------------------------------------
	public function init() {
		parent::init ();
		//Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$this->form_collection = new RbView_Smarty_Form_Collection ( 'form', 'post', $this->actionUrl ( 'store' ) );
		//Set hidden field
		$this->form_collection->addElement ( 'hidden', 'redisplay', 0, array ('id' => 'redisplay' ) ); //use to control validation or re-display of the form
		$this->form_collection->addElement ( 'hidden', 'step', 'init', array ('id' => 'step' ) ); //this prop is controled by submit or by choice of category
		$this->form_collection->addElement ( 'hidden', 'container_number', $this->container->getNumber () );
		$this->form_collection->addElement ( 'hidden', 'container_id', $this->container->getId () );
		$this->form_collection->addElement ( 'hidden', 'ticket', $this->ticket );
		//Add submit button
		//Add js to set the action and to reset the redisplay option state.
		//Redisplay hidden request option is used to prevent execution of validate the form when redisplay the form following change of category
		$this->form_collection->addElement ( 'submit', 'validate', tra ( 'Validate' ), array ('onClick' => 'javascript:getElementById(\'redisplay\').value=0;
                                  getElementById(\'step\').value=\'validate\';' ) );
		//Create a subForm to set the headers
		$form_header = new RbView_Smarty_Form_Sub ( 'form_header', 'post' );
		$form_header->addElement ( 'header', '', tra ( 'Store files in' ) . ' ' . $this->container->getNumber () );
		$form_header->addElement ( 'header', '', tra ( 'Selection review' ) );
		$this->form_collection->add ( $form_header );
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward('store');
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function storeAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$step = $this->getRequest ()->getParam ( 'step' );
		$redisplay = $this->getRequest ()->getParam ( 'redisplay' );
		if ($redisplay){
			$this->_setForm ();
		}else if ($step == 'validate'){
			$this->_setForm ( true );
		}else{
			$this->_setForm ();
		}
		
		//test reposit
		if( !Rb_Reposit::getActiveReposit($this->space) ){
			$this->error_stack->push ( Rb_Error::ERROR, array (), 
				tra('None active reposit is found') );
		}else{
			$this->view->createForm = $this->form_collection->toHtml ();
		}

		$this->error_stack->checkErrors ();
	} //End of method
	
	
	//---------------------------------------------------------------------
	protected function _setForm($validate = false) {
		//var_dump($this->getRequest()->getParams());
		$file_names = $this->getRequest ()->getParam ( 'file_name' );
		$docaction = $this->getRequest ()->getParam ( 'docaction' );
		//$loop = $this->getRequest()->getParam('loop');
		//$fields = $this->getRequest()->getParam('fields');
		
		if ( !$file_names ){
			//return $this->_successForward (); //inifinite loop
			$this->error_stack->push ( Rb_Error::WARNING, array (), 'Selection is empty' );
			return;
		}
		
		if (! is_array ( $file_names )) { //generate array if parameter is string
			$file_names = array ($file_names );
		}
		
		if (count ( $file_names ) > 30) {
			$chunk_select = array_chunk ( $file_names, 30, true );
			$file_names = $chunk_select [0];
			unset ( $chunk_select );
			$this->error_stack->push ( Rb_Error::WARNING, array (), 'Sorry but your selection is too large and has been trunk to 30 elements' );
		}
		
		$tmp_list = array (); //init the var for foreach loop
		$clean_list = array (); //init the var for foreach loop
		
		foreach ( $file_names as $file_name ) {
			$smartStore = new RbView_SmartStore ( $this->space_name );
			$i = md5 ( $file_name ); //loop id

			//---------- Check if file exist in wildspace
			$data = & $smartStore->testFile ( $file_name, Rb_User::getCurrentUser ()->getWildspace ()->getPath () );
			if (! $data) { //file dont exist so return error message to user
				$smartStore = new RbView_SmartStoreForm ( $this->space_name );
				$smartStore->setError ( $file_name . ' is not in your Wildspace.' );
				$this->form_collection->add ( $smartStore->setForm ( $i ) );
				continue;
			}
			
			//---------- Check if file is recorded in database and try to init document and docfile
			$isDocfile = $smartStore->isDocfile ( $data );
			
			//---------- Check if document is recorded in database and try to init document and docfile
			if (! $isDocfile)
				$isDocument = $smartStore->isDocument ( $data->getProperty ( 'doc_name' ) );
			
			//---------- if is not a docfile or document init a new document and a new docfile
			//---------- Check the doctype for the new document
			if (! $isDocfile && ! $isDocument) {
				$smartStore->initNewDocument ( $this->container, $data );
				$doctype = & $smartStore->setDoctype ();
			}
			
			//---------- Set the category
			$category_ids = $this->getRequest ()->getParam ( 'category_id' );
			if (is_array ( $category_ids ))
				$category_id = current ( $category_ids );
			if ($doctype)
				$smartStore->setCategory ( $doctype, $category_id );
			
			//---------- Check double documents
			if (in_array ( $smartStore->getDocument ()->getNumber (), $tmp_list )) {
				$smartStore->setError ( $smartStore->getDocument ()->getNumber () . ' is not unique in this package' );
				//$smartStore->setAction('add_file');
				return false;
			} else {
				$tmp_list [] = $smartStore->getDocument ()->getNumber ();
			}
			
			$this->form_collection->add ( $smartStore->setForm ( $i, $docaction [$i], $validate, $this->getRequest ()->getParams () ) );
		} //End of foreach
		return $this->form_collection;
	} //End of method
	
	
} //End of class
