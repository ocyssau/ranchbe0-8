<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');
class Document_ChangestateController extends controllers_abstract {
	protected $document; //(object)
	protected $page_id = 'documentManager_changestate'; //(string)
	protected $ifSuccessForward = array ('module' => 'content', 
							'controller' => 'get', 'action' => 'document' );
	protected $ifFailedForward = array ('module' => 
							'content', 'controller' => 'get', 'action' => 'document' );
	
	//---------------------------------------------------------------------
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		
		$this->session = new Zend_Session_Namespace ( 'changeStateCaddie' );
		
		$this->document_ids = $this->getRequest ()->getParam ( 'document_id' );
		
		//to retrieve previous selection -- nice.
		if (! $this->document_ids && $this->session->document_ids) {
			$this->document_ids = $this->session->document_ids;
			$this->space_name = $this->session->space_name; //get space from session, else melting document is possible
		} else {
			$this->session->step = 'step1';
			$this->space_name = $this->getRequest ()->getParam ( 'space' );
			$this->session->space_name = $this->space_name;
		}
		
		if (! is_array ( $this->document_ids ) && $this->document_ids) {
			$this->document_ids = array (( int ) $this->document_ids );
		}
		
		$this->page_id = $this->space_name . $this->page_id;
		$this->ticket = $this->getRequest ()->getParam ( 'ticket' );
		$this->space = & Rb_Space::get ( $this->space_name );
		$this->view->assign ( 'SPACE_NAME', $this->space_name );
		Zend_Registry::set ( 'space', $this->space );
		
		//init forward
		$this->_initForward ();
		
		if ($this->ifSuccessForward ['module'])
			$this->session->ifSuccessModule = $this->ifSuccessForward ['module'];
		else
			$this->ifSuccessForward ['module'] = $this->session->ifSuccessModule;
		
		if ($this->ifSuccessForward ['controller'])
			$this->session->ifSuccessController = $this->ifSuccessForward ['controller'];
		else
			$this->ifSuccessForward ['controller'] = $this->session->ifSuccessController;
		
		if ($this->ifSuccessForward ['action'])
			$this->session->ifSuccessAction = $this->ifSuccessForward ['action'];
		else
			$this->ifSuccessForward ['action'] = $this->session->ifSuccessAction;
	
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
		echo 'index action';
	} //End of method
	
	//---------------------------------------------------------------------
	public function changestateAction() {
		$this->_helper->viewRenderer->setScriptAction ( 'index' );
		if ($this->getRequest ()->getParam ( 'cancel' )) {
			return $this->_cancel ();
		}
		
		if (! $this->document_ids) {
			$msg = tra ( 'You must select at least one item' );
			$this->error_stack->push ( Rb_Error::ERROR, array (), $msg );
			return $this->_cancel ();
		}
		
		$step = $this->getRequest ()->getParam ( 'step' );
		if (empty ( $step )) {
			$step = $this->session->step;
		}
		
		if ($step == 'step1') {
			$this->_step1_selectActivity ();
			//form dont resend ticket, so conserve it in session for use it in next steps
			$this->session->ticket = $this->ticket;
		} else if ($step === 'step2') {
			$this->_step2_activityTemplate ();
		} else if ($step === 'step3') {
			if (! RbView_Flood::checkFlood ( $this->session->ticket ))
				return $this->_successForward ();
			$this->_step3_run ();
		} else if ($step === 'step4') {
			$this->_step4_comment ();
			return $this->_successForward ();
		} else
			$this->_step1_selectActivity ();
		$this->error_stack->checkErrors ();
		return;
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _step1_selectActivity() {
		$this->session->unsetAll ();
		//Construct the form with QuickForm lib
		$form_collection = new RbView_Smarty_Form_Collection ( 'form', 'post', $this->actionUrl ( 'changestate' ) );
		
		//Set hidden field
		$form_collection->addElement ( 'hidden', 'step', 'step2', array ('id' => 'step' ) );
		$form_collection->addElement ( 'hidden', 'space', $this->space_name );
		$form_collection->addElement ( 'hidden', 'container_id', $this->container_id );
		$form_collection->addElement ( 'hidden', 'ticket', $this->ticket );
		$this->_initFormForward ( $form_collection );
		
		//Create a subForm to set the headers
		$form_header = new RbView_Smarty_Form_Sub ( 'form_header', 'post' );
		$form_header->addElement ( 'header', '', tra ( 'Multi change state of documents' ) );
		$form_header->addElement ( 'header', '', tra ( 'Selection review' ) );
		$form_collection->add ( $form_header );
		
		//Create element a recap line in the select manager for each document
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			$changestate = new RbView_Helper_Changestate ( new Rb_Workflow_Docflow ( $document ) );
			if ($form_collection->add ( $changestate->setForm ( $document_id ) )) { //generate html code for one line
				if (! $changestate->getErrors ()) {
					$this->session->document_ids [] = $document_id; //add this document in caddie
					$docflow = $changestate->getDocflow (); //keep an instance of a valid docflow to set next operations
				}
			}
		}
		
		if ($docflow) { //create a activity select only if there is at least one valid document
			$selectSet = array ();
			//Get infos about activities
			$acts = $docflow->getInstanceActivityInfo ();
			if (is_array ( $acts ['standalone'] )) //merge to create the html select of activity to activiate
				$activities = array_merge ( $acts ['activities'], $acts ['standalone'] );
			else
				$activities = $acts ['activities'];
				
			//Generate select activity
			if (is_array ( $activities )) {
				//experimental: next code is maybe useful for no auto-routed activity case>
				if ($docflow->getInstance ()->getInstanceId ()) { //if an instance is in progress...
					if (! $docflow->getRunningActivity ()) { //...but there is none running activity
						//then select any activity
						foreach ( $activities as $activity ) {
							$selectSet [$activity ['activityId']] = $activity ['name'];
						}
					}
				} //< end of experimetal code
				foreach ( $activities as $activity ) {
					//if is start activity and none instance
					if (($activity ['type'] == 'standalone') || ($activity ['type'] == 'start' && ! $docflow->getDocument ()->getProperty ( 'instance_id' )) ||
							($activity ['actstatus'] == 'running')) {
						$selectSet [$activity ['activityId']] = $activity ['name'];
					}
				}
				$select = & $form_collection->addElement ( 'select', 'activityId', tra ( 'Next activity' ), $selectSet );
			}
			
			//Get paths for graph
			$proc_info = $docflow->getProcessInfos ();
			$graph = GALAXIA_PROCESSES . '/' . $proc_info ['normalized_name'] . '/graph/' . $proc_info ['normalized_name'] . '.png';
			RbView_Flood::setTicket ( $graph );
			$graphUrl = ROOT_URL . '/getFile.php?file=' . $graph;
			$this->view->assign ( 'graphUrl', $graphUrl );
			$this->session->step = 'step2';
			$form_collection->addElement ( 'submit', 'validate', tra ( 'next' ), array ('onClick' => 'javascript:getElementById(\'redisplay\').value=0;getElementById(\'step\').value=\'step2\';' ) );
		}
		
		//Add submit button
		$form_collection->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		
		return $this->_display ( $form_collection );
	} //End of method
	

	//---------------------------------------------------------------------
	//Step2: display activity template if necessary
	protected function _step2_activityTemplate() {
		$this->session->step = 'step2';
		$activityId = $this->getRequest ()->get ( 'activityId' );
		$this->session->activityId = $activityId;
		
		$workflow = new Rb_Workflow_Workflow ( );
		$template = $workflow->getActivityTemplate ( $activityId );
		
		if (! $template) { //its not interactive activity
			return $this->_step3_run ();
		} else {
			Ranchbe::getLogger()->log('Try to display template: ' . $template, LOG );
			//$this->error_stack->push ( LOG, 'Infos', array(), 'Try to display template: ' . $template );
			$this->_helper->viewRenderer->setNoRender(true);
			//Ranchbe::getLayout ()->setLayout ( 'popup' );
			echo $this->view->render($template);
			$this->session->step = 'step3';
			return;
		}	
	} //End of method
	

	//---------------------------------------------------------------------
	//Step3: run activity on each selected document
	protected function _step3_run() {
		$this->session->step = 'step3';
		$activityId = $this->getRequest ()->get ( 'activityId' );
		if ($activityId)
			$this->session->activityId = $activityId;
		else
			$activityId = $this->session->activityId; //get activityId from session.
			
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			$docflow = new Rb_Workflow_Docflow ( $document );
			$docflow->initInstance (); //init the instance from the document properties
			$docflow->initProcess (); //init the process from the document properties
			
			$instance = & $docflow->execute ( $activityId );
			
			if ($instance === false) { //error in execute activity instance
				$e = array ('element' => $document->getProperty ( 'document_number' ) );
				$msg = 'change state failed for document %element%';
				$this->error_stack->push ( Rb_Error::ERROR, $e, $msg );
			} else {
				$this->session->instance_ids [] = $instance->getInstanceId ();
				//$instances_name[] = $instance->getName();
			}
		}
		$activity = & $docflow->getActivity ();
		if ($activity->isComment == 'y') {
			return $this->_inputComment ( $docflow, $activityId ); //display comment page
		} else {
			$this->session->step = 'step1';
			return $this->_successForward (); //If _forward is call from sub-method, display blank page!!!???
		}
	} //End of method
	

	//---------------------------------------------------------------------
	//Step4: validate comment and update activity
	/* comments are same for all activity of the current selection */
	protected function _step4_comment() {
		$title = trim ( $this->getRequest ()->get ( '__title' ) );
		$comment = trim ( $this->getRequest ()->get ( '__comment' ) );
		$activityId = $this->session->activityId;
		if (empty ( $title ) && empty ( $comment ))
			return $this->_successForward ();
		foreach ( $this->document_ids as $document_id ) { //loop instances from the previous execution
			$workflow = new Rb_Workflow_Docflow ( Rb_Document::get ( $this->space_name, $document_id ) );
			$workflow->initInstance (); //init the instance from the document properties
			$workflow->initActivity ( $activityId ); //init current activity
			$workflow->setComment ( $title, $comment );
		}
		return $this->_successForward ();
	} //End of method
	

	//---------------------------
	/* Process comments
	*/
	protected function _inputComment(Rb_Workflow_Workflow &$docflow, $activity_id) {
		//$this->_helper->viewRenderer->setNoRender(true);
		if (empty ( $activity_id )) {
			$msg = 'activity_id is empty';
			$this->error_stack->push ( Rb_Error::INFO, array (), $msg );
			return false;
		}
		$this->session->step = 'step4';
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'inputComment', 'post', $this->actionUrl ( 'changestate' ) );
		$this->_initFormForward ( $form ); //init the hidden param to set the forward parameter

		$form->addElement ( 'header', 'title1', tra ( 'Activity completed' ) );
		$form->addElement ( 'text', '__title', tra ( 'Subject' ) );
		$form->addElement ( 'textarea', '__comment', '', array ('rows' => 5, 'cols' => 60 ) );
		$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'No comment' ) );
		$form->applyFilter ( '__ALL__', 'trim' );
		return $this->_display ( $form );
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _display(&$form) {
		$this->view->assign ( 'changestateForm', $form->toHtml () );
		$this->error_stack->checkErrors ();
	} //End of method


} //End of class
