<?php

class Document_IndexControllerTest extends  Zend_Test_PHPUnit_ControllerTestCase{

	public function setUp(){
		defined ( 'APPLICATION_PATH' ) 
			|| define ( 'APPLICATION_PATH', 
						str_replace ( '\\', '/', realpath ( dirname ( __FILE__ ) . '/../application' ) ) );
		
		// Define application environment
		if( getenv('BASE_URL') ){
			define('BASE_URL', '/'.getenv('BASE_URL').'/' );
		}

		require_once ('Rb/Conf/Ranchbe.php');

		define ( 'ROOT_URL', 'http://'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . BASE_URL );
		$_SERVER['HTTP_REFERER'] = ROOT_URL;
		//var_dump($_SERVER);die;

		$config = new Zend_Config_Ini ( APPLICATION_PATH . '/configs/rb.ini', 
											'testing' , true);
		$this->bootstrap = new Zend_Application(
        	'testing',
			$config
		);

		/** Zend_Application */
		// Create application, bootstrap, and run
		$application = new Zend_Application ( APPLICATION_ENV, $config );
		Ranchbe::setConfig ( $config );
		
		parent::setUp();
	}


	public function testIndexAction()
	{
		// Simule une requ�te vers la racine
		$this->dispatch('/');
		// On v�rifie le code de r�ponse HTTP
		$this->assertResponseCode(200, 'Code was ' . $this->_response->getHttpResponseCode());
		// Le nom du contr�leur est bien Index
		$this->assertController('index', 'Controller was ' . $this->_request->getControllerName());
		// Le nom de l'action est bien Index
		$this->assertAction('index', 'Action was ' . $this->_request->getActionName());
		// Teste le contenu de la r�ponse
		$this->assertQueryContentContains('h4', 'Pierre QUIMOUSSE');
	}

}