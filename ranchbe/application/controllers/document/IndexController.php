<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once ('controllers/document/abstract.php');
class Document_IndexController extends controllers_document_abstract {
	protected $page_id = 'documentManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'content', 
											'controller' => 'get', 
											'action' => 'document' );
	protected $ifFailedForward = array ('module' => 'content', 
											'controller' => 'get', 
											'action' => 'document' );
	
	//---------------------------------------------------------------------
	public function init() {
		parent::init ();
		return true;
	} //End of method
	
	//---------------------------------------------------------------------
	/**
	 * 
	 * @param Zend_Session_Namespace $session
	 * @param Zend_Form $form
	 * @return boolean
	 */	
	protected function _create(Zend_Session_Namespace $session, Zend_Form $form) {
		$fsdata = false;
		$space_name = $session->space_name;
		$container_id = $session->container_id;
		$container = & Rb_Container::get ( $space_name, $container_id );
		$document_number = $session->document_number;
		$assoc_file_name = $session->assoc_file_name;
		$category_id = (int) $session->category_id;
		$document_name = $session->document_name;
		$description = $session->description;
		$document_version = (int) $session->document_version;
		$lock = (boolean) $session->lock;
		$suppress = (boolean) $session->suppress;
		$extends = $session->extends;
		
		$smartStore = new RbView_SmartStore($space_name);
		if( $assoc_file_name ){
			$fsdata = new Rb_Fsdata($assoc_file_name);
			if( $smartStore->isDocfile($fsdata) ){
				return false;
			}
		}
		if( $smartStore->isDocument($document_number) ){
			return false;
		}
		$smartStore->initNewDocument($container, $fsdata, $document_number);
		$smartStore->getDocument()
			->setProperty( 'category_id', $category_id )
			->setProperty( 'document_name', $document_name )
			->setProperty( 'description', $description )
			->setProperty( 'document_version', $document_version );
			
		if($extends)
		foreach( $extends as $prop_name=>$prop_value){
			$smartStore->getDocument()->setProperty( $prop_name, $prop_value );
		}
		
		return $smartStore->getDocument()->store($lock, $suppress);
		//return false;
	} //End of function
	
	
	//---------------------------------------------------------------------
	/**
	 * @TODO : add hidden array to keep the original value of document properties,
	 * and use this to set if document is change or not. If not change, do not save it
	 * to prevent the write of unseful history line
	 */
	public function editAction() {
				return $this->_forward('index', 'edit', 'document',  array(
				    'document_id' =>  $this->getRequest ()->getParam ( 'document_id' ),
				));
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * Common code for all actions
	 * 
	 * @return boolean
	 */
	protected function _initdocAction() {
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return false;
		if (empty ( $this->document_ids )) {
			Ranchbe::getError()->warning( 'You must select at least one item' );
			return false;
		}
		return true;
	} //End of method
	

	
	//---------------------------------------------------------------------
	/**
	 * 
	 * @param array $values
	 */
	protected function _edit($values) {
		if( $this->document->getId() < 1 ){
			Ranchbe::getError()->error( tra('object is not set, you can not edit this object') );
		}
		if (! Ranchbe::checkPerm ( 'document_edit', $this->document, false )) {
			unset ( $values ['document_number'] );
		}
		unset ( $values ['ticket'], $values ['redisplay'], $values ['validate'] );
		unset ( $values ['action'], $values ['container_id'], $values ['container_number'] );
		//Set empty value to null else create conflict with sql constaints
		if (isset ( $values ['category_id'] ) && ! $values ['category_id'])
			$values ['category_id'] = null;
		foreach ( $values as $key => $val ) {
			if (is_a($this->view->form->getElement ( $key ), 'ZendRb_Form_Element_Date'))
				$values [$key] = Rb_Date::makeTimestamp ( $values [$key] );
			if (is_array ( $val )) {
				$values [$key] = implode ( '#', $val );
			}
			$this->document->setProperty ( $key, $val );
		}
		return $this->document->save ();
	} //End of function
	
	//---------------------------------------------------------------------
	/**
	 * 
	 * @param Rb_Document $document
	 * @param integer $code
	 */
	protected function _lockdoc(Rb_Document &$document, $code) {
		//Lock access to files too
		$docfiles = $document->getDocfiles ();
		$success = true;
		if (is_array ( $docfiles ))
			foreach ( $docfiles as $docfile ) {
				if (! $docfile->lockFile ( $code, true )) {
					$success = false;
					$document->history ['comments'] [] = 'lock failed on docfile ' . $docfile->getName ();
				}
			}
			//if change on each file is success then change lock status on document
		if ($success)
			$document->lockDocument ( $code, true );
		$document->writeHistory ( $document->getLockName ( $code ) );
		return true;
	} //End of method

	
	//---------------------------------------------------------------------
	/**
	 * 
	 * @param string $actionUrl
	 * @param string $title
	 * @param Rb_Document $document
	 * @param array $SelectSet
	 * @return boolean
	 */
	protected function _selectfile($actionUrl, $title, Rb_Document &$document, array $SelectSet) {
		
		require_once ('RbView/Smarty/Form/ElementCreators.php'); //Librairy to easily create forms
		$form = & new RbView_Pear_Html_QuickForm ( 'select_file', 'POST', $this->actionUrl ( $actionUrl ) ); //Construct the form with QuickForm lib
		$this->_initFormForward ( $form ); //init forward urls
		
		$form->setWidth ( '550px' );
		$form->addElement ( 'header', 'title1', $title );
		
		if (count ( $SelectSet ) == 0) {
			$html = '<p><b><i>' . tra ( 'no files to select' ) . '</i></b></p>';
			$form->addElement ( 'static', '', $html );
		} else {
			$select = & $form->addElement ( 'select', 'file_name', tra ( 'Select file' ), $SelectSet, array ('id' => 'file_name' ) );
			$select->setSize ( 5 );
			$select->setMultiple ( true );
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		}
		
		//Add hidden
		$form->addElement ( 'hidden', 'document_id', $document->getId () );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
		
		//Add submit
		$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		$form->applyFilter ( '__ALL__', 'trim' );
		$form->display ();
		return true;
	
	} //End of method
	
	
	//---------------------------------------------------------------------
	/**
	 * @return void
	 * 
	 */
	public function indexAction() {
	} //End of method

	

	
	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	function checkoutdocAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		
		require_once ('RbView/Smarty/Form/ElementCreators.php'); //Librairy to easily create forms
		$form = & new RbView_Pear_Html_QuickForm ( 'confirm_checkout', 'POST', $this->actionUrl ( 'validatecheckoutdoc' ) ); //Construct the form with QuickForm lib
		$this->_initFormForward ( $form ); //init forward urls
		
		$form->setWidth ( '550px' );
		$form->addElement ( 'header', 'title1', tra ( 'Confirm replace' ) );
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			if (! Ranchbe::checkPerm ( 'document_edit', $document, false )) {
				continue;
			}
			
			if ($document->checkAccess ()) {
				Ranchbe::getError()->error( tra ( 'this document is locked with code %element%, checkOut is not permit' ), array ('element' => $access_code ) );
				continue;
			}
			
			$form->addElement ( 'hidden', 'document_id[' . $document_id . ']', $document_id );
			$docfiles = & $document->getDocfiles ();
			$confirm_need = false;
			if (is_array ( $docfiles ))
				foreach ( $docfiles as $docfile ) {
					$existing_files_name = array ();
					if (! $fsdata = & $docfile->getFsdata ())
						return false;
					$target_fsdata = new Rb_Fsdata ( Rb_User::getCurrentUser ()->getWildspace ()->getPath () . '/' . $fsdata->getProperty ( 'file_name' ) );
					if ($target_fsdata->isExisting ()) {
						$existing_files_name [] = $fsdata->getProperty ( 'file_name' );
					}
					if (count ( $existing_files_name ) > 0) {
						$message_elements = array (implode ( ', ', $existing_files_name ), $document->getProperty ( 'document_number' ) );
						$message = vsprintf ( tra ( 'the file(s) <b>%s</b> of document <b>%s</b> exists in your wildspace' ), $message_elements );
						$form->addElement ( 'static', '', $message );
						$select_set = array ('keep' => tra ( 'keep' ), 'replace' => tra ( 'replace' ), 'cancel' => tra ( 'cancel' ) );
						
						$property_description = tra ( 'What do want to do with this files?' ) . '<br />';
						$property_description .= '<a href="#" onClick="javascript:setValue(\'ifExistmode[' . $document_id . ']\');return false;">' . tra ( 'same for all' ) . '</a>';
						
						$form->addElement ( 'select', 'ifExistmode[' . $document_id . ']', $property_description, $select_set, array ('id' => 'ifExistmode[' . $document_id . ']' ) );
						$confirm_need = true;
					}
				}
		}
		
		if ($confirm_need) {
			$this->serialize_request_post ( $form, array (
													'space', 
													'ifSuccessModule', 
													'ifSuccessController', 
													'ifSuccessAction', 
													'ifFailedModule',
													'ifFailedController', 
													'ifFailedAction', 
													'ifSuccessForward', 
													'ifFailedForward' ) );
			
			$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
			if (is_a ( $form->getElement ( 'ticket' ), 'HTML_QuickForm_element' ))
				$form->getElement ( 'ticket' )->setValue ( RbView_Flood::getTicket () );
			
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			$form->display ();
			$this->_helper->viewRenderer->setNoRender();
			return;
		} else {
			$this->_forward('validatecheckoutdoc');
		}
	} //End of method
	
	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	function validatecheckoutdocAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		$ifExistmode = $this->getRequest ()->getParam ( 'ifExistmode' );
		//var_dump($this->ticket, $_SESSION['ticket_gf']);
		if (RbView_Flood::checkFlood ( $this->ticket ))
			foreach ( $this->document_ids as $document_id ) {
				$document = & Rb_Document::get ( $this->space_name, $document_id );
				$document->checkOut ( true, $ifExistmode [$document_id] );
			}
		$this->_successForward ();
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function checkindocAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		
		if(!Ranchbe::getConfig()->document->comment->force)
			return $this->_forward('validatecheckindoc');
		
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = & new RbView_Pear_Html_QuickForm ( 'confirm_checkin', 
							'POST', $this->actionUrl ( 'validatecheckindoc' ) );

		$form->setWidth ( '550px' );
		$form->addElement ( 'header', 'title1', tra ( 'Checkin documents' ) );
		//$form->addElement('static', '', tra('checkinDocAddComment_help_1') );
		$form->addElement ( 'textarea', 'comment', tra ( 'checkinDocAddComment_help_1' ), array ('cols' => 30, 'id' => 'comment' ) );
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			$checkedlabel = '<b>' . $document->getProperty ( 'normalized_name' ) . '</b> ';
			$checkedlabel .= '<i>' . $document->getProperty ( 'description' ) . '</i>';
			//Il ne semble pas possible de creer des checkbox retournant autre chose que 1 avec Quickform???
			//$form->addElement('checkbox', 'checked[]' , $document_id , $checkedlabel , array('checked'=>'checked') );
			$checkElement = '<input type="checkbox" name="document_id[]" value="' . $document_id . '" id="checkbox_tbl_' . $document_id . '" checked="checked" />';
			$checkElement .= '<label for="checkbox_tbl_' . $document_id . '">';
			$checkElement .= $checkedlabel;
			$checkElement .= '</label>';
			$form->addElement ( 'static', '', $checkElement );
		}
		
		$this->serialize_request_post ( $form, array ('space', 'document_id[]', 'ticket', 'ifSuccessModule', 'ifSuccessController', 'ifSuccessAction', 'ifFailedModule', 'ifFailedController', 'ifFailedAction', 'ifSuccessForward', 'ifFailedForward' ) );
		
		if (is_a ( $form->getElement ( 'ticket' ), 'HTML_QuickForm_element' ))
			$form->getElement ( 'ticket' )->setValue ( RbView_Flood::getTicket () );
		
		$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		$form->applyFilter ( '__ALL__', 'trim' );
		$form->display ();
		$this->_helper->viewRenderer->setNoRender(true);
	} //End of method
	
	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function validatecheckindocAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		$comment = trim ( $this->getRequest ()->getParam ( 'comment' ) );
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			if (! empty ( $comment )) {
				//comment history
				$document->getHistory ()->setComment ( $comment );
				//Create a postit
				$ocomment = new Rb_Document_Comment ( $this->space );
				$ocomment->Add ( $document_id, $comment );
			}
			$document->checkInAndRelease ();
		}
		return $this->_successForward ();
	} //End of method

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function resetprocessAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		$document = & Rb_Document::get ( $this->space_name, $this->document_id );
		
		if (! Ranchbe::checkPerm ( 'admin', $document, false )) {
			return $this->_cancel ();
		}
		
		$docflow = new Rb_Workflow_Docflow ( $document );
		if ($docflow->resetProcess ())
			Ranchbe::getError()->info( tra ( 'process of document %element% is reset' ), array ('element' => $document->getName ()) );
	
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function suppressdocAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		
		$this->_helper->viewRenderer->setNoRender(true);
		
		require_once ('RbView/Smarty/Form/ElementCreators.php'); //Librairy to easily create forms
		$form = & new RbView_Pear_Html_QuickForm ( 'confirm_suppress', 'POST', $this->actionUrl ( 'validatesuppressdoc' ) ); //Construct the form with QuickForm lib
		$form->setWidth ( '550px' );
		$form->addElement ( 'header', 'title1', tra ( 'Are you sure that you want suppress this documents' ) );
		//$form->addElement('textarea', 'comment' , tra('checkinDocAddComment_help_1') ,array('cols'=>30, 'id'=>'comment' ) );

		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			if (! Ranchbe::checkPerm ( 'document_suppress', $document, false )) {
				continue;
			}
			$checkedlabel = '<b>' . $document->getProperty ( 'normalized_name' ) . '</b> ' . '<i>' . $document->getProperty ( 'description' ) . '</i>';
			//Il ne semble pas possible de creer des checkbox retournant autre chose que 1 avec Quickform???
			//$form->addElement('checkbox', 'checked[]' , $document_id , $checkedlabel , array('checked'=>'checked') );
			$checkElement = '<input type="checkbox" name="document_id[]" value="' . $document_id . '" id="checkbox_tbl_' . $document_id . '" checked="checked" />';
			$checkElement .= '<label for="checkbox_tbl_' . $document_id . '">';
			$checkElement .= $checkedlabel;
			$checkElement .= '</label>';
			$form->addElement ( 'static', '', $checkElement );
		}
		
		$this->serialize_request_post ( $form, array ('space', 'document_id[]', 'ifSuccessModule', 'ifSuccessAction', 'ifSuccessController', 'ifFailedModule', 'ifFailedAction', 'ifFailedController' ) );
		
		$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
		if (is_a ( $form->getElement ( 'ticket' ), 'HTML_QuickForm_element' ))
			$form->getElement ( 'ticket' )->setValue ( RbView_Flood::getTicket () );
		
		$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		$form->applyFilter ( '__ALL__', 'trim' );
		$form->display ();
		return;
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function validatesuppressdocAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		$comment = $this->getRequest ()->get ( 'comment' );
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			if (! Ranchbe::checkPerm ( 'document_suppress', $document, false )) {
				continue;
			}
			$document->suppress ();
		}
		
		return $this->_successForward ();
	
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function marktosuppressAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			if (! Ranchbe::checkPerm ( 'document_edit', $document, false )) {
				continue;
			}
			//Check if access is free
			$access = $document->checkAccess ();
			if ($access > 0) {
				Ranchbe::getError()->error(tra ( 'You cant mark to suppress %element%' ) , array ('element' => $document->getName () ) );
				continue;
			} else {
				$this->_lockdoc ( $document, 12 );
			}
		}
		$this->_successForward ();
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function unmarktosuppressAction() {
		$this->_forward ( 'unlock', 'index', 'document' );
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function unlockAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			if (! Ranchbe::checkPerm ( 'document_unlock', $document, false )) {
				continue;
			}
			//Check if access is free
			$access = $document->checkAccess ();
			if ($access < 5 || $access > 14) {
				Ranchbe::getError()->error (tra ( 'You cant unlock document %element%' ), array ('element' => $document->getName () ) );
				continue;
			} else {
				$this->_lockdoc ( $document, 0 );
			}
		}
		$this->_successForward ();
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function lockAction() {
		if ($this->getRequest ()->getParam ( 'cancel' )) return $this->_cancel ();
		$this->view->dojo()->enable();
		$isValidate = $this->getRequest ()->get ( 'save' );

		$this->view->PageTitle = tra ( 'Lock documents' );
		$form = new ZendRb_Form();
		$form->setDecorators(array(
			array('FormElements'),
			array('HtmlTag', array('tag' => 'table', 'class' => 'rb_form')),
			array('DijitForm'),
		));
		$form->getElement('save')->addDecorator(
			'HtmlTag', array('tag' => 'td')
		);
		$form->getElement('cancel')->addDecorator(
			'HtmlTag', array('tag' => 'td')
		);
		//var_dump( $form->getElement('save')->getDecorators() );
		
		//var_dump($form->getDecorators());
		//var_dump( $this->document_ids );
		
		$form->setAction( $this->_helper->url('lock') );
		$form->addElement('multihidden', 'document_id', array());
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			if (! Ranchbe::checkPerm ( 'document_edit', $document, false )) {
				continue;
			}
			$subForm = new ZendRb_SubForm();
			//var_dump($subForm->getDecorators());
			$subForm->setDecorators(array(
				array('FormElements'),
				array('ContentPane'),
				array('HtmlTag', array('tag' => 'tr', 'class' => 'rb_subform')),
			));
			
			$subForm->addElement('StaticString', 'document', array(
                    'label' => $document->getName(),
					));
			$subForm->getElement('document')->setDecorators( array(
						array('Label', array('tag'=>'td') ), 
						));
			//var_dump( $subForm->getElement('document')->getDecorators() );
			/*			
			$subForm->addElement('FilteringSelect', 'locktype', array(
                    //'label' => tra('Lock type'),
                    'autocomplete' => false,
                    'value' => 'simple',
                    'multiOptions' => array(
						'simple'=>tra('Simple'),
						'history'=>tra('For history'),
					),
			));
			$subForm->getElement('locktype')->setDecorators( array(
				array('DijitElement'),
				array('Errors'),
				//array('Description', array('tag'=>'p', 'class'=>'description')),
				//array('Label', array()),
				array('HtmlTag', array('tag'=>'td')),
			));
			//var_dump( $subForm->getElement('locktype')->getDecorators() );
			*/
			
			$subForm->addElement('hidden', 'document_id', array('value'=>$document_id ));
			$form->getElement('document_id')->addMultiOption($document_id, $document_id);
			$form->addSubForm($subForm, $document_id);
		}

		$form->addElement('FilteringSelect', 'locktype', array(
				'label' => tra('Lock type'),
				'autocomplete' => false,
				'value' => 'simple',
				'order' => 999,
				'multiOptions' => array(
					'simple'=>tra('Simple'),
					'history'=>tra('For history'),
				),
		));
		$form->getElement('locktype')->setDecorators( array(
			array('DijitElement'),
			array('Errors'),
			//array('Description', array('tag'=>'p', 'class'=>'description')),
			array('Label', array()),
			array('HtmlTag', array('tag'=>'td')),
		));
		
		$form->addElement('hidden', 'space', array('value'=>$this->space_name ));
		$form->addElement('hidden', 'ticket', array('value'=>RbView_Flood::getTicket() ));
		
		if ( $this->getRequest ()->isPost() && $isValidate ) {
			if ($form->isValid($this->getRequest ()->getPost())) {
				if (! RbView_Flood::checkFlood ( $this->ticket ))
					return $this->_cancel ();
				//var_dump( $this->getRequest ()->getParams() );die;
				foreach($this->document_ids as $document_id){
					$document = & Rb_Document::get ( $this->space_name, $document_id );
					//Check if access is free
					$access = $document->checkAccess ();
					if ($access > 0) {
						Ranchbe::getError()->error( tra ( 'You cant lock document %element%' ), array ('element' => $document->getName () ) );
						continue;
					} else {
						$locktype = $form->getValue('locktype');
						//var_dump($locktype);die;
						switch($formValues){
							case 'history':
								$this->_lockdoc ( $document, 15 );
								break;
							case 'simple':
								$this->_lockdoc ( $document, 11 );
								break;
						}
					}
				}
				return $this->_successForward ();
			}
		}
		
		$this->view->form = $form;
		Ranchbe::getError()->checkErrors ();
		$this->render ( 'default/editform', null, true );
		
		//$this->_successForward ();
		return true;
	} //End of method

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function resetdocAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			$document->cancelCheckOut ();
		}
		
		$this->_successForward ();
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function updatedocAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			$document->checkInAndKeep ();
		}
		
		$this->_successForward ();
		
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function putinwsAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			if (Ranchbe::checkPerm ( 'document_get', $document, false )) {
				$document->putInWildspace ();
			}
		}
		$this->_successForward ();
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function freezeAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		$freeze = new Rb_Freeze ( );
		
		foreach ( $this->document_ids as $document_id ) {
			$freeze->freeze ( Rb_Document::get ( $this->space_name, $document_id ) );
		}
		
		return $this->_successForward ();
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function resetdoctypeAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			if ($docfile = & $document->getDocfile ( 'main' )) { //Get main docfile of the document
				$file_extension = $docfile->getProperty ( 'file_extension' ); //Get the property of the main docfile
				$file_type = $docfile->getProperty ( 'file_type' ); //Get the property of the main docfile
			} else {
				$file_extension = NULL;
				$file_type = 'nofile';
			}
			if ($doctype = $document->setDocType ( $file_extension, $file_type ))
				$document->save ();
		}
		$this->_successForward ();
		return true;
	} //End of method
		

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function assocfileAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		
		$this->_helper->viewRenderer->setNoRender(true);
		$document = & Rb_Document::get ( $this->space_name, $this->document_ids [0] );
		Ranchbe::checkPerm ( 'document_edit', $document, true );
		
		$file_names = $this->getRequest ()->get ( 'file_name' );
		
		if (is_null ( $file_names )) {
			$params = array ('offset' => 0, 'maxRecords' => 99999 );
			if (Ranchbe::getConfig()->document->assocfile->checkname) {
				$params ['find'] ['file_name'] = $document->getNumber ();
			}
			$list = Rb_Directory::getDatas (
				Rb_User::getCurrentUser ()->getWildspace ()->getPath(), 
				$params, 
				false );
			if (is_array ( $list )) {
				foreach ( $list as $infos ) {
					$selectSet [$infos ['file_name']] = $infos ['file_name'];
				}
			} else {
				$selectSet = array ();
			}
			return $this->_selectfile ( 'assocfile', sprintf ( tra ( 'Associate file to %s' ), $document->getName () ), $document, $selectSet );
		}
		
		if (RbView_Flood::checkFlood ( $this->ticket ))
			foreach ( $file_names as $file_name ) {
				$file = Rb_User::getCurrentUser ()->getWildspace ()->getPath () . '/' . $file_name;
				if (is_file ( $file )) {
					$document->associateFile ( $file ); //Assoc file "file_name" to document "document_id"
				}
			}
		return $this->_successForward ();
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function assocattachmentAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		
		$this->_helper->viewRenderer->setNoRender(true);
		
		$document = & Rb_Document::get ( $this->space_name, $this->document_ids [0] );
		
		Ranchbe::checkPerm ( 'document_edit', $document, true );
		
		$vfile_name = $this->getRequest ()->get ( 'vfile_name' );
		$pfile_name = $this->getRequest ()->get ( 'pfile_name' );
		
		$path = Rb_User::getCurrentUser ()->getWildspace ()->getPath ();
		
		//Construct form to select file
		if (! $pfile_name && ! $vfile_name) {
			//extension for pictures
			$pregex = '(.jpg|.gif|.png|.bmp)$';
			//extension for visu
			//$vext = $document->getDoctype()->getProperty('visu_file_extension');
			//$vregex = '('.$vext.')$';
			$vregex = '(' . implode ( '|', Ranchbe::getVisuFileExtensions () ) . ')$';
			$selectPicure = array ();
			$selectVisu = array ();
			if ($handle = opendir ( $path )) {
				while ( $file = @readdir ( $handle ) ) {
					if (preg_match ( '/' . $pregex . '/i', $file ))
						$selectPicure [$file] = $file;
						//if($vext)
					if (preg_match ( '/' . $vregex . '/i', $file ))
						$selectVisu [$file] = $file;
				}
				@closedir ( $handle );
			}
			
			require_once ('RbView/Smarty/Form/ElementCreators.php'); //Librairy to easily create forms
			$form = & new RbView_Pear_Html_QuickForm ( 'select_file', 'POST', $this->actionUrl ( 'assocattachment' ) ); //Construct the form with QuickForm lib
			$this->_initFormForward ( $form ); //init forward urls
			

			$form->setWidth ( '550px' );
			$form->addElement ( 'header', 'title1', sprintf ( tra ( 'Associate visualisation file to %s' ), $document->getName () ) );
			
			$form->addElement ( 'static', 'title2', tra ( 'assoc_visu_help1' ) );
			
			if (count ( $selectVisu ) > 0) {
				$select = & $form->addElement ( 'select', 'vfile_name', tra ( 'Select visualisation file' ), $selectVisu, array ('id' => 'vfile_name' ) );
				$select->setSize ( 5 );
				$select->setMultiple ( false );
			}
			
			if (count ( $selectPicure ) > 0) {
				$select = & $form->addElement ( 'select', 'pfile_name', tra ( 'Select picture file' ), $selectPicure, array ('id' => 'pfile_name' ) );
				$select->setSize ( 5 );
				$select->setMultiple ( false );
			}
			
			if (count ( $selectPicure ) == 0 && count ( $selectVisu ) == 0) {
				$html = '<p><b><i>' . tra ( 'no files to select' ) . '</i></b></p>';
				$form->addElement ( 'static', '', $html );
			} else {
				$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			}
			
			//Add hidden
			$form->addElement ( 'hidden', 'document_id', $document->getId () );
			$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
			$form->addElement ( 'hidden', 'space', $this->space_name );
			
			//Add submit
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			$form->display ();
			return;
		}
		
		if (RbView_Flood::checkFlood ( $this->ticket )) {
			//Rename file to a normalize name from document number
			if ($pfile_name) {
				$pfile = $path . '/' . $document->getNumber () . '.' . uniqid () . '.picture' . substr ( $pfile_name, strrpos ( $pfile_name, '.' ) );
				rename ( $path . '/' . $pfile_name, $pfile );
			}
			
			if ($vfile_name) {
				$vfile = $path . '/' . $document->getNumber () . '.' . uniqid () . '.visu' . substr ( $vfile_name, strrpos ( $vfile_name, '.' ) );
				rename ( $path . '/' . $vfile_name, $vfile );
			}
			
			if (is_file ( $pfile )) {
				$document->associateFile ( $pfile, false, 2 );
				$thumbnail = new Rb_Document_Thumbnail ( $document );
				$thumbnail->generate ();
			}
			
			if (is_file ( $vfile )) {
				$document->associateFile ( $vfile, false, 1 );
			}
		}
		
		$this->_successForward ();
	} //End of method
	

	//-----------------------------------------------------------------
	/**
	 * @return boolean
	 * 
	 */
	public function updatethumbnailAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		$document = & Rb_Document::get ( $this->space_name, $this->document_id );
		$thumbnail = new Rb_Document_Thumbnail ( $document );
		$thumbnail->generate ();
		return $this->_successForward ();
	} //End of method
	

	//---------------------------------------------------------------------
	/** Add comments about document
	 * @return void
	 * 
	 * 
	 */
	public function addcommentAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		$document = & Rb_Document::get ( $this->space_name, $this->document_id );
		Ranchbe::checkPerm ( 'document_edit', $document, true );
		
		$comment = trim ( $this->getRequest ()->get ( 'comment' ) );
		
		if (! empty ( $comment )) {
			$ocomment = new Rb_Document_Comment ( $this->space );
			$ocomment->add ( $this->document_id, $comment );
		}
		
		return $this->_successForward ();
	} //End of method
	

	//---------------------------------------------------------------------
	/** Delete the comment
	 * @return void
	 * 
	 */
	public function delcommentAction() {
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		$comment_id = $this->getRequest ()->get ( 'comment_id' );
		
		$ocomment = new Rb_Document_Comment ( $this->space );
		$ocomment->Suppress ( $comment_id );
		
		return $this->_successForward ();
	} //End of method
	

	//---------------------------------------------------------------------
	/** Move document to other container
	 * 
	 * @return void
	 */
	public function movedocumentAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		
		$this->_helper->viewRenderer->setNoRender(true);
		Ranchbe::getLayout()->setLayout('popup');
		
		$validate = $this->getRequest ()->get ( 'validate' );
		$target_container_id = $this->getRequest ()->get ( 'target_container_id' );
		
		require_once ('RbView/Smarty/Form/ElementCreators.php'); //Librairy to easily create forms
		$form = & new RbView_Pear_Html_QuickForm ( 'moveDocument', 
							'POST', $this->actionUrl ( 'movedocument' ) );
		$form->setWidth ( '800px' );
		
		$form->defaultRenderer ()->setElementTemplate ( '<tr><td class="formcolor">
            {label}<!-- BEGIN required --><span style="color: #ff0000">*</span><!-- END required -->
            {element}<!-- BEGIN error --><span style="color: #ff0000">{error}</span><br /><!-- END error -->
          </td></tr>' );
		
		$form->addElement ( 'header', null, '<i>' . tra ( 'Move document' ) . '</i>' );
		
		$html = '<ul>';
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			if (Ranchbe::checkPerm ( 'document_edit', $document, false )) {
				$form->addElement ( 'hidden', 'document_id[]', $document_id );
				$form->addElement ( 'hidden', 'page_id', 'movedocument' );
				$form->addElement ( 'hidden', 'space', $this->space_name );
				$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
				$html .= '<li><b>' . $document->getName () . '</b></li>';
			} else {
				$html .= '<li><b>' . tra ( 'you have not permission to move' ) . ' ' . $document->getName () . '</b></li>';
			}
		}
		$html .= '</ul>';
		$form->addElement ( 'static', null, $html );
		
		//Get list of containers
		$p = array ('property_name' => 'target_container_id', 'property_description' => tra ( 'To' ), 'default_value' => Ranchbe::getContext ()->getProperty ( 'container_id' ), 'is_multiple' => false, 'return_name' => false, 'is_required' => true, 'property_length' => 5 );
		construct_select_container ( $p, $form, Rb_Container::get ( $this->space_name ), 'client' );
		
		if ($validate && $target_container_id) { //Validate the form
			if ($form->validate ()) { //Validate the form
				$form->freeze (); //and freeze it
				if (RbView_Flood::checkFlood ( $this->ticket ))
					foreach ( $this->document_ids as $document_id ) {
						//@TODO: Check user right on target container
						//Update the document informations and move the associated files
						$document = & Rb_Document::get ( $this->space_name, $document_id );
						if (Ranchbe::checkPerm ( 'document_edit', $document, false )) {
							$document->moveDocument ( Rb_Container::get ( $this->space_name, $target_container_id ) );
						}
					}
			}
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		}
		$form->applyFilter ( '__ALL__', 'trim' );
		
		$form->display ();
		Ranchbe::getError()->checkErrors ( array ('close_button' => true ) );
		return true;
	} //End of method
	
	
	//---------------------------------------------------------------------
	/** Copy document
	 * @return void
	 * 
	 */
	public function copy2documentAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();

		$this->view->dojo()->enable();
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		
		$validate = $this->getRequest ()->get ( 'validate' );
		$target_container_id = $this->getRequest ()->get ( 'target_container_id' );
		$newName = $this->getRequest ()->get ( 'newName' );
		$indice_id = $this->getRequest ()->get ( 'indice_id' );
		
		$form = new ZendRb_Form();
		$form->setAction( $this->_helper->url('copydocument') );
		$form->addElement ( 'Hidden', 'space', array('value'=>$this->space_name) );
		$form->addElement ( 'Hidden', 'ticket', array('value'=>RbView_Flood::getTicket() ) );
		//var_dump( $form->getDecorators() );
		//$form->setDecorators(array('FormElements', array('HtmlTag',array('tag'=>'table')), 'ContentPane'));
		
		$this->view->PageTitle = tra ( 'Copy documents' );
		
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			$subform = new ZendRb_SubForm();
			$subform->setDecorators(array('FormElements', array('HtmlTag',array('tag'=>'tr')), 'DijitForm'));
			
			//$subform->addElement ( 'StaticString', 'sstring', array('value'=>$document->getNumber () ));
			
			$subform->addElement ( 'TextBox', 'newName', array(
								'value'=>'copy_of_' . $document->getNumber (),
								'label'=> tra ( 'New name' ),
								'size' =>50,
								'required'=> true,
								'trim'  => true,
								) );
								
			$subform->addElement ( 'Selectindice', 'indice_id', array(
								'value'=>1,
								'label'=> tra ( 'Indice' ),
								'required'=> true,
								'allowEmpty'=> false,
								'returnName'=> false,
								'advSelect'=> false,
								'multiple' => false,
								'optionsList'=> 0,
								) );
			$subform->addElement ( 'Selectcontainer', 'target_container_id', array(
								'value'=>$document->getContainer ()->getId (),
								'label'=> tra ( 'Container' ),
								'required'=> true,
								'allowEmpty'=> false,
								'returnName'=> false,
								'advSelect'=> false,
								'multiple' => false,
								'source' => Rb_Container::get ( $this->space_name )
								) );
								
			$subform->getElement('newName')->addDecorator('HtmlTag',array('tag'=>'td'))->removeDecorator('Label');
			$subform->getElement('indice_id')->addDecorator('HtmlTag',array('tag'=>'td'))->removeDecorator('Label');
			$subform->getElement('target_container_id')->addDecorator('HtmlTag',array('tag'=>'td'))->removeDecorator('Label');
			
			$form->addSubForm($subform, $document_id);
		}
		
		//var_dump($this->getRequest ()->getPost());die;		
		
		if ($validate && $form->isValid($this->getRequest ()->getPost())) {
			if (RbView_Flood::checkFlood ( $this->ticket ))
			foreach ( $this->document_ids as $document_id ) {
				$document = & Rb_Document::get ( $this->space_name, $document_id );
				if (Ranchbe::checkPerm ( 'document_edit', $document, false )) {
					$to_container = & Rb_Container::get ( $this->space_name, $target_container_id [$document_id] );
					$document->copyDocumentToNumber ( $to_container, $newName [$document_id], $indice_id [$document_id], true );
				}
			}
		}
		
		$form->getElement('save')->addDecorator('HtmlTag',array('tag'=>'td'));
		
		$this->view->form = $form;
		$this->render ( 'default/editform', null, true );
		Ranchbe::getError()->checkErrors ();
		return;
		
	} //End of method
	
	

	//---------------------------------------------------------------------
	/** Copy document
	 * @return void
	 * 
	 */
	public function copydocumentAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		
		$this->_helper->viewRenderer->setNoRender(true);
		Ranchbe::getLayout()->setLayout('popup');
		
		$validate = $this->getRequest ()->get ( 'validate' );
		$target_container_id = $this->getRequest ()->get ( 'target_container_id' );
		$newName = $this->getRequest ()->get ( 'newName' );
		$indice_id = $this->getRequest ()->get ( 'indice_id' );
		
		require_once ('RbView/Smarty/Form/ElementCreators.php'); //Librairy to easily create forms
		$form = & new RbView_Pear_Html_QuickForm ( 'copyDocument', 'POST', $this->actionUrl ( 'copydocument' ) );
		$form->setWidth ( '800px' );
		
		$form->defaultRenderer ()->setElementTemplate ( '<td class="formcolor">
            {label}<!-- BEGIN required --><span style="color: #ff0000">*</span><!-- END required -->
            {element}<!-- BEGIN error --><span style="color: #ff0000">{error}</span><br /><!-- END error -->
          </td>' );
		
		$form->addElement ( 'header', null, tra ( 'Copy documents' ) . '<br />' );
		
		$indice_list = Rb_Indice::singleton ()->getIndices ( 0 );
		
		$container = & Rb_Container::get ( $this->space_name );
		$containers = $container->getAll ( array ('sort_field' => $container->getDao ()->getFieldName ( 'number' ), 'sort_order' => 'ASC', 'select' => array ($container->getDao ()->getFieldName ( 'number' ), $container->getDao ()->getFieldName ( 'id' ) ) ) );
		foreach ( $containers as $value ) {
			$container_list [$value [$container->getDao ()->getFieldName ( 'id' )]] = $value [$container->getDao ()->getFieldName ( 'number' )];
		}
		unset ( $containers );
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			$form->addElement ( 'static', '', '<tr><td><b>' . $document->getName () . '</b></td>' );
			$form->addElement ( 'hidden', 'document_id[' . $document_id . ']', $document_id );
			$form->addElement ( 'text', 'newName[' . $document_id . ']', tra ( 'New name' ), array ('value' => 'copy_of_' . $document->getNumber (), 'size' => 50 ) );
			$form->addRule ( 'newName[' . $document_id . ']', tra ( 'Required' ), 'required', null, 'client' );
			//Get list of indices
			$pindice_list = array ('property_name' => 'indice_id[' . $document_id . ']', 'property_description' => tra ( 'Indice' ), 'default_value' => 1, 'is_multiple' => false, 'return_name' => true, 'is_required' => true, 'property_length' => 1 );
			construct_select ( $indice_list, $pindice_list, $form, 'client' );
			
			$pcont_list = array ('property_name' => 'target_container_id[' . $document_id . ']', 'property_description' => tra ( 'Container' ), 'default_value' => $document->getContainer ()->getId (), 'is_multiple' => false, 'return_name' => true, 'is_required' => true, 'property_length' => 1 );
			construct_select ( $container_list, $pcont_list, $form, 'client' );
			$form->addElement ( 'static', '', '</tr>' );
		}
		$form->addElement ( 'static', '', '</tr></table>' );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
		
		if ($validate && $target_container_id) { //Validate the form
			if ($form->validate ()) { //Validate the form
				$form->freeze (); //and freeze it
				if (RbView_Flood::checkFlood ( $this->ticket ))
					foreach ( $this->document_ids as $document_id ) {
						$document = & Rb_Document::get ( $this->space_name, $document_id );
						if (Ranchbe::checkPerm ( 'document_edit', $document, false )) {
							$to_container = & Rb_Container::get ( $this->space_name, $target_container_id [$document_id] );
							$document->copyDocumentToNumber ( $to_container, $newName [$document_id], $indice_id [$document_id], true );
						}
					}
			}
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		}
		
		//$form->addElement('submit', 'cancel', tra('cancel') );
		//$form->addElement ( 'static', '', '<input onclick="javascript:window.close();return false;" name="close" value="' . tra ( Close ) . '" type="button" />' );
		$form->applyFilter ( '__ALL__', 'trim' );
		
		$form->display ();
		Ranchbe::getError()->checkErrors ();
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	/*
	public function multichangeindiceAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			if (Ranchbe::checkPerm ( 'document_edit', $document, false ))
				continue;
			$access = $document->checkAccess ();
			if ($access == 0) {
				$this->_lockdoc ( 11 );
			}
			$document->copyDocumentToVersion ( $document->getFather (), 0, true );
		}
		
		return $this->_successForward ();
	} //End of method
	*/
	

	//---------------------------------------------------------------------
	/** Create a new version of document
	 * 
	 * @return void
	 */
	public function createversionAction() {
		if (! $this->_initdocAction () )
			return $this->_cancel ();
		
		$validate = $this->getRequest ()->get ( 'validate' );
		$indice_id = $this->getRequest ()->get ( 'indice_id' ); //array
		
		if(Ranchbe::getConfig()->version->lock_previous == 'onchoice'){
			$makeHistory = $this->getRequest ()->get ( 'makeHistory' );
		}else if(Ranchbe::getConfig()->version->lock_previous == 'force'){
			$makeHistory = 1;
		}else{
			$makeHistory = 0;
		}
		
		require_once ('RbView/Smarty/Form/ElementCreators.php'); //Librairy to easily create forms
		$form = & new RbView_Pear_Html_QuickForm ( 'createversion', 'POST', $this->actionUrl ( 'createversion' ) );
		
		$this->view->PageTitle = tra ( 'Change indice of documents :' );
		$form->setWidth ( '500px' );
		
		$this->_initFormForward($form);
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			if (! Ranchbe::checkPerm ( 'document_edit', $document, false )) {
				$form->addElement ( 'static', '', $document->getName () . ' : ' . tra ( 'You are not authorized to process this request.' ) );
				continue;
			}
			$form->addElement ( 'hidden', 'document_id[' . $document_id . ']', $document_id );
			
			//Get list of indices
			$indices = Rb_Indice::singleton ()->getIndices ( $document->getProperty ( 'document_version' ) );
			$indices ['ignore'] = 'ignore';
			$select = & $form->addElement ( 'select', 'indice_id[' . $document_id . ']', '<b>' . $document->getNumber () . '</b>', $indices );
			$select->setSize ( 1 );
			$select->setMultiple ( false );
			$form->addRule ( 'indice', tra ( 'is required' ), 'required' );
		}
		if(Ranchbe::getConfig()->version->lock_previous == 'onchoice'){
			$attrib = array();
			if(Ranchbe::getConfig()->version->lock_previous_default == 1){
				$attrib = array('checked'=>'checked');
			}
			$form->addElement ( 'checkbox', 'makeHistory', tra('Mark previous as history'), 'Yes' , $attrib );
		}
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
		
		if ($validate && $indice_id) { //Validate the form
			if ($form->validate ()) { //Validate the form
				$form->freeze (); //and freeze it
				if (RbView_Flood::checkFlood ( $this->ticket ))
					foreach ( $this->document_ids as $document_id ) {
						if ($indice_id [$document_id] == 'ignore')
							continue;
						$document = & Rb_Document::get ( $this->space_name, $document_id );
						//Check if the update indice is permit
						$access = $document->checkAccess ();
						if ($access == 0) {
							$this->_lockdoc ( $document, 11 );
						} else if (! ($access > 9 && $access < 20)) {
							Ranchbe::getError()->push ( Rb_Error::ERROR, array ('element' => $document->getNumber () ), tra ( 'You cant change indice of %element%: is not locked' ) );
							continue;
						}
						$version = $document->copyDocumentToVersion ( $document->getFather (), $indice_id [$document_id], true );
						//Lock the previous indice for prevent change indice on
						if ($makeHistory) {
							if (! $document->lockDocument ( 15, true )) {
								Ranchbe::getError()->push ( Rb_Error::ERROR, array ('name' => $document->getNumber () ), 'cant lock the document %name%' );
								continue;
							}
						}
					}
			}
			$form->addElement ( 'submit', 'cancel', tra ( 'end' ) );
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		}
		$form->applyFilter ( '__ALL__', 'trim' );
		
		Ranchbe::getError()->checkErrors ();
		$this->view->form = $form->toHtml ();
		$this->render ( 'default/editform', null, true );
		return true;
	} //End of method
	
	//---------------------------------------------------------------------
	/**
	 * @return void
	 * 
	 */
	public function updatereadAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		foreach ( $this->document_ids as $document_id ) {
			$document = & Rb_Document::get ( $this->space_name, $document_id );
			$container = & $document->getFather();
			$reads = Rb_Reposit_Read::getReadReposit($container);
			$docfiles = $document->getDocfiles();
			foreach( $docfiles as $docfile ){
				foreach($reads as $read){
					$readlink = Rb_Reposit_Read::getReadLink($read, $docfile);
					$readlink->save();
				}
			}
		}
		$this->_successForward ();
		return true;
	} //End of method
	
	//---------------------------------------------------------------------
	/**
	 * @return void
	 * 
	 */
	public function convertAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
			
		$this->_helper->viewRenderer->setNoRender(true);
			
		$document = & Rb_Document::get ( $this->space_name, $this->document_ids [0] );
		
		$validate = $this->getRequest ()->get ( 'validate' );
		$toType = $this->getRequest ()->get ( 'toType' );
		
		$maindocfile = $document->getDocfile ( 'main' );
		$fromType = trim ( strtolower ( $maindocfile->getProperty ( 'file_extension' ) ), '.' );
		
		//select format
		require_once ('RbView/Smarty/Form/ElementCreators.php'); //Librairy to easily create forms
		$form = & new RbView_Pear_Html_QuickForm ( 'convert', 'POST', $this->actionUrl ( 'convert' ) );
		
		$form->addElement ( 'header', 'infos', sprintf ( tra ( 'Convert document %s' ), $document->getName () ) );
		$form->setWidth ( '500px' );
		
		$form->addElement ( 'hidden', 'document_id', $document->getId () );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
		
		//Select toType
		$conf = new Zend_Config_Ini ( Ranchbe::getConfig()->converter->inifile, Ranchbe::getConfig()->converter->inisection );
		
		$select = array ();
		if ($conf->application->$fromType) {
			foreach ( $conf->application->$fromType as $type => $application ) {
				$select [$type] = $type . '/' . $application;
			}
		}
		construct_select ( $select, array ('property_name' => 'toType', 'property_description' => tra ( 'Convert in type' ), 'is_multiple' => false, 'return_name' => false, 'is_required' => true, 'property_length' => 1 ), $form, 'client' );
		
		if ($validate && $toType) { //Validate the form
			if ($form->validate ()) { //Validate the form
				$cvtclient = new Rb_Converter_Request ( $maindocfile->getProperty ( 'file' ), $toType );
				//$cvtclient->setTransport(new rb_converter_transport_soap($cvtclient));
				

				$res = $cvtclient->convert ();
				if ($res ['error'])
					Ranchbe::getError()->push ( Rb_Error::ERROR, array (), $res ['error'] );
				else if ($res ['data']) {
					//send content of file to user
					//var_dump($res);die;
					$content = base64_decode ( $res ['data'] );
					$fileName = basename ( $res ['output_file'] );
					header ( "Content-disposition: attachment; filename=$fileName" );
					header ( "Content-Type: application/data" );
					header ( "Content-Transfer-Encoding: $fileName\n" ); // Surtout ne pas enlever le \n
					header ( "Content-Length: " . strlen ( $content ) );
					header ( "Pragma: no-cache" );
					header ( "Cache-Control: must-revalidate, post-check=0, pre-check=0, public" );
					header ( "Expires: 0" );
					echo $content;
					die ();
					
		/*
        $file = new Rb_File( $res['result_file'] );
        $file->downloadFile();
        unlink( $res['result_file'] );
        die;
        */
				}
			}
			return $this->_cancel ();
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		}
		
		$form->applyFilter ( '__ALL__', 'trim' );
		$form->display ();
		Ranchbe::getError()->checkErrors ();
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	/**
	 * @return void
	 * 
	 */
	public function exportAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		
		$this->_helper->viewRenderer->setNoRender(true);
		$document = & Rb_Document::get ( $this->space_name, $this->document_ids [0] );
		
		$validate = $this->getRequest ()->get ( 'validate' );
		$toType = $this->getRequest ()->get ( 'toType' );
		
		//select format
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = & new RbView_Pear_Html_QuickForm ( 'export', 
									'POST', $this->actionUrl ( 'export' ) );
		
		$form->addElement ( 'header', 'infos', sprintf ( tra ( 'Export document %s' ), $document->getName () ) );
		$form->setWidth ( '500px' );
		
		$form->addElement ( 'hidden', 'document_id', $document->getId () );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
		
		$select = array (1 => 'toXml', 2 => 'toCsv' );
		construct_select ( $select, array ('property_name' => 'toType', 'property_description' => tra ( 'Export format' ), 'is_multiple' => false, 'return_name' => false, 'is_required' => true, 'property_length' => 1 ), $form, 'client' );
		
		if ($validate && $toType) { //Validate the form
			if ($form->validate ()) { //Validate the form
				$xml = new XMLWriter ( );
				$xml->openMemory ();
				$xml->startDocument ( '1.0', 'ISO-8859-1' );
				$xml->setIndent ( true );
				Rb_Document_Serialize::toXml ( $document, $xml );
				$xml->endDocument ();
				$res = $xml->flush ();
				if ($res) {
					//send content of file to user
					$content = $res;
					$fileName = $document->getNumber () . '.export.xml';
					header ( "Content-disposition: attachment; filename=$fileName" );
					header ( "Content-Type: application/data" );
					header ( "Content-Transfer-Encoding: $fileName\n" ); // Surtout ne pas enlever le \n
					header ( "Content-Length: " . strlen ( $content ) );
					header ( "Pragma: no-cache" );
					header ( "Cache-Control: must-revalidate, post-check=0, pre-check=0, public" );
					header ( "Expires: 0" );
					echo $content;
					die ();
				}
			}
			return $this->_cancel ();
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		}
		
		$form->applyFilter ( '__ALL__', 'trim' );
		$form->display ();
		Ranchbe::getError()->checkErrors ();
		return true;
	} //End of method

	//---------------------------------------------------------------------
	/**
	 * @return void
	 * 
	 */
	public function sendbymailAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		
		if(!Ranchbe::getConfig()->mail->enable)
			return $this->_cancel ();
		
		$document = Rb_Document::get ( $this->space_name, $this->document_id );
		
		if (!Ranchbe::checkPerm ( 'document_get', $document, false )) {
			return $this->_cancel ();
		}
		
		//Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->view->PageTitle = sprintf(tra ( 'Send document %s by mail' ), $document->getName() );
		require_once APPLICATION_PATH . '/Form/Document/Sendbymail.php';
		$this->view->form = new Form_Document_Sendbymail();
		$this->view->form->setAction( $this->_helper->url('sendbymail') );
		
		$this->view->form->addElement('hidden', 'document_id', array('value'=>$this->document_id ));
		$this->view->form->addElement('hidden', 'space', array('value'=>$this->space_name ));
		$this->view->form->addElement('hidden', 'ticket', array('value'=>RbView_Flood::getTicket() ));
		
		$this->view->form->setDefaults(array (
			'subject' => sprintf(tra( 'Document %s'), $document->getName() ),
			'message_body' => sprintf(tra("Hello,<br /> find here a copy document %s"), $document->getName() ),
		));
		
		$assocfiles = $document->getDocfiles();
		foreach($assocfiles as $docfile){
	    	$this->view->form->getElement('attachment')
	    		->addMultiOption($docfile->getId(), $docfile->getName());
		}
		
		$save = $this->getRequest()->getParam('save');
		
		if ( $this->getRequest ()->isPost() && $save) {
			if ($this->view->form->isValid($this->getRequest ()->getPost())) {
				if ($this->_sendbymail($this->view->form->getValues(), $document) )
					return $this->_successForward();
				else
					$this->view->failedMessage = tra('Send message has failed');
			}
		}
		
		Ranchbe::getError()->checkErrors ();
		$this->render ( 'default/editform', null, true );
		return;
	} //End of method
	
	
	/** Send files by mail
	 * 
	 * @param array
	 * @param Rb_Document
	 * @return boolean
	 */
	protected function _sendbymail($values, Rb_Document $document){
		//create mail
		$mail = new Rb_Mail();
		$mail->setBodyText( $values['message_body'] );
		$mail->setSubject( $values['subject'] );
		$mail->setFrom(Rb_User::getCurrentUser()->getProperty('email'));
		
		//add file to join
		foreach($values['attachment'] as $file_id){
			$docfile = Rb_Docfile::get($document->getSpace()->getName(), $file_id);
			if(!$docfile) continue;
			$fsdata = $docfile->getFsdata();
			if(!$fsdata) continue;
			//@TODO : add possibility for directory content or other datatype than plain file
			if(is_file($fsdata->getProperty('file'))){
				$at = $mail->createAttachment(file_get_contents($fsdata->getProperty('file')) );
				$at->filename = $docfile->getName();
			}
		}

		if($values['to']){
			if(is_array($values['to'])){
				foreach($values['to'] as $tomail){
					$mail->addTo($tomail, $tomail);
				}
			}else{
				$mail->addTo($values['to'], $values['to']);
			}
		}
		if($values['cc']){
			if(is_array($values['cc'])){
				foreach($values['cc'] as $ccmail){
					$mail->addCc($ccmail, $ccmail);
				}
			}else
				$mail->addCc($values['cc'], $values['cc']);
		}
		if($values['bcc']){
			if(is_array($values['bcc'])){
				foreach($values['bcc'] as $bccmail){
					$mail->addBcc($bccmail, $bccmail);
				}
			}else
				$mail->addCc($values['bcc'], $values['bcc']);
		}
		if (count ( $mail->getRecipients() ) == 0) {
			Ranchbe::getError()->error( tra ( 'ERROR: No valid users to send the mail' ) );
			return false;
		}
		$mail->send();
		return true;
	}
	
	/** Generate a mailto href
	 * 
	 * @return boolean
	 */
	public function mailtoAction(){
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		
		$document = Rb_Document::get ( $this->space_name, $this->document_id );
		
		if (!Ranchbe::checkPerm ( 'document_get', $document, false )) {
			return $this->_cancel ();
		}
		
		$body = array();
		$subject = $document->getName();
		$values['attachment'] = array(); //desactivate generation of link to files
		
		$body[] = tra('Hello,');
		$body[] = sprintf( tra('Find here links to files of document %s'), $document->getName() );
		
		$expiration_time = Ranchbe::getConfig()->security->ticket->expiration_time / 3600;
		$body[] = sprintf( tra('The following links will be valid for %s hours. Beyond this period it will be necessary for you to be identified on RanchBE.'), $expiration_time);

		//generate link to document
		$ticket = Rb_Ticket::setTicket( $document->getId() );
		if(!$ticket) return $this->_cancel ();
		$body[] = tra('Link to document :');
		$body[] = '<a href= ' . ROOT_URL . 'get/file/document_id/' . $document->getId()
					. '/ticket/' . $ticket
					. '/space/' . $document->getSpace()->getName() . '>'
					. $document->getName() . '</a>';
		
		
		//$mm = false;
		//add file to join
		if( count($values['attachment']) > 0 ){
			$body[] = tra('Link to files :');
			//$mm = new Zend_Mime_Message();
			foreach($values['attachment'] as $file_id){
				$docfile = Rb_Docfile::get($document->getSpace()->getName(), $file_id);
				if(!$docfile) continue;
				$ticket = Rb_Ticket::setTicket( $file_id );
				$body[] = '<a href= ' . ROOT_URL . 'get/file/file_id/' . $file_id
							. '/ticket/' . $ticket
							. '/space/' . $document->getSpace()->getName()
							. 'object_class/docfile >'
							. $docfile->getName() . '</a>';
				/*
				$docfile = Rb_Docfile::get($document->getSpace()->getName(), $file_id);
				if(!$docfile) continue;
				$fsdata = $docfile->getFsdata();
				if(!$fsdata) continue;
				//@TODO : add possibility for directory content or other datatype than plain file
				if(is_file($fsdata->getProperty('file'))){
			        $mp = new Zend_Mime_Part( file_get_contents($fsdata->getProperty('file')) );
			        $mp->encoding = Zend_Mime::ENCODING_BASE64;
			        $mp->type = Zend_Mime::TYPE_OCTETSTREAM;
			        $mp->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
			        $mp->filename = $docfile->getName();
			        $mm->addPart($mp);
				}
				*/
			}
		}

		/*
		if($values['to']){
			if(is_array($values['to'])){
				$to = implode(',', $values['to']);
			}else{
				$to = $values['to'];
			}
		}
		if($values['cc']){
			if(is_array($values['cc'])){
				$cc = implode(',', $values['cc']);
			}else
				$cc = $values['cc'];
		}
		if($values['bcc']){
			if(is_array($values['bcc'])){
				$bcc = implode(',', $values['bcc']);
			}else
				$cc = $values['bcc'];
		}
		*/

		/*
		$mime = new Zend_Mime();
        $body .= $mime->boundaryLine();
        $body .= $mp->getHeaders();
		$body .= $mm->generateMessage();
        $body .= $mime->mimeEnd();
        */
        
		//$body = urlencode($body);
		//if($mm) $body .= $mm->generateMessage();
		//var_dump($body);
		
		$body = implode('<br />', $body);
		$mailto = "mailto:$to?cc=$cc&bcc=$bcc&body=$body&subject=$subject";
		
		header ("location:$mailto");
		//$this->_redirect('index');
		echo "<a href=_blank onClick='javascript:window.close();return;'>Close this window</a>";
		die;
		return true;
	}
	

} //End of class
