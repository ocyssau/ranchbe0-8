<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/document/abstract.php');
class Document_RelationController extends controllers_document_abstract {
	protected $page_id = 'document'; //(string)
	protected $ifSuccessForward = array ('module' => 'document', 'controller' => 'relation', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'document', 'controller' => 'relation', 'action' => 'index' );
	
	public function init() {
		parent::init ();
		$this->view->assign ( 'document_id', $this->document_id );
		$contextSwitch = $this->_helper->getHelper ( 'contextSwitch' );
		$contextSwitch->addActionContext ( 'getdocumenttree', 'xml' )->initContext ();
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	/*! \brief Common code for all actions
	*/
	protected function _initdocAction() {
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return false;
		//if(!$this->check_flood) return false;
		if (empty ( $this->document_ids )) {
			$this->error_stack->push ( Rb_Error::ERROR, array (), 'You must select at least one item' );
			return false;
		}
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	/*Create an array from string content in id from dthtmlxtree js lib
	* $id is a string form like this : key__value--otherkey__othervalue--...etc
	*/
	protected function _dthtmlxtreeIdToArray($id) {
		$id_string = explode ( '--', $id );
		foreach ( $id_string as $keyvalue ) {
			$keyvalue_explode = explode ( '__', $keyvalue );
			$return [$keyvalue_explode [0]] = $keyvalue_explode [1];
		}
		return $return;
	}
	
	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->getAction ();
	} //End of method
	

	//---------------------------------------------------------------------
	//Alias for getdocument
	public function getAction() {
		return $this->_forward ( 'getdetailwindow', 'detail', 'document' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function addsonAction() {
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$document = Rb_Document::get($this->space_name, $this->document_id);
		Ranchbe::checkPerm ( 'document_edit', $document, true );
		$l_selected = $this->getRequest ()->getParam ( 'l_selected' );
		if (! $l_selected) {
			$this->view->assign ( 'actionName', 'AddSon' );
			$this->_helper->viewRenderer->setNoController(true);
			$this->view->headScript()->appendFile(Ranchbe::getConfig()->js->baseurl.'dhtmlxTree/js/dhtmlXCommon.js');
			$this->view->headScript()->appendFile(Ranchbe::getConfig()->js->baseurl.'dhtmlxTree/js/dhtmlXTree.js');
			//$this->view->request_url = ROOT_URL.'dthtmlxtree_generateXml1.php';
			$this->view->request_url = $this->view->baseUrl('document/relation/getdocumenttree/format/xml');
			$this->render ( 'document/select', null, true );
			return;
		} else {
			$docrel = new Rb_Doclink ( $document );
			foreach ( $l_selected as $dthtmlxtreeId ) {
				$son_doc = $this->_dthtmlxtreeIdToArray ( $dthtmlxtreeId );
				$docrel->addSon ( $son_doc ['document_id'] );
			}
		}
		return $this->_forward ( $this->ifSuccessForward ['action'], 
								$this->ifSuccessForward ['controller'], 
								$this->ifSuccessForward ['module'], 
								$this->ifSuccessForward ['params'] );
		
	/*
    if(!$check_flood) break;
    if(!isset($_REQUEST['l_selected'])){
      $this->view->assign('actionName', 'AddSon');
      $this->view->display('selectDocument.tpl');
      die;
    }else{
      $docrel = new Rb_Doclink($odocument);
      foreach ($_REQUEST['l_selected'] as $dthtmlxtreeId) {
        $son_doc = DthtmlxtreeIdToArray($dthtmlxtreeId);
        $docrel->AddSon( $son_doc['document_id'] );
      }
    }
    $this->view->assign('content1display', 'none');
    $this->view->assign('content3display', 'block');
  */
	
	//----------DOC RELATION SHIP MANAGEMENT :
	/*
  case ('AddSon'):
    if(!$check_flood) break;
    if(!isset($_REQUEST['l_selected'])){
      $this->view->assign('actionName', 'AddSon');
      $this->view->display('selectDocument.tpl');
      die;
    }else{
      require_once('class/productStructure/relationship.php');
      $docrel = new relationship($odocument);
      foreach ($_REQUEST['l_selected'] as $dthtmlxtreeId) {
        $son_doc = DthtmlxtreeIdToArray($dthtmlxtreeId);
        $docrel->AddSon($son_doc['document_class'],$son_doc['space']); //$son_doc[1] = value of document class
      }
    }
  break;
  */
	} //End of method
	

	//---------------------------------------------------------------------
	public function suppresssonAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		$document = Rb_Document::get($this->space_name, $this->document_id);
		Ranchbe::checkPerm ( 'document_edit', $document, true );
		$link_ids = $this->getRequest ()->getParam ( 'dr_link_id' );
		if (! is_array ( $link_ids ))
			$link_ids = array ($link_ids );
		if (! empty ( $link_ids )) {
			$docrel = new Rb_Doclink ( $document );
			foreach ( $link_ids as $link_id ) {
				$docrel->suppressSon ( $link_id );
			}
		}
		return $this->_forward ( $this->ifSuccessForward ['action'], 
								$this->ifSuccessForward ['controller'], 
								$this->ifSuccessForward ['module'], 
								$this->ifSuccessForward ['params'] );
		
	/*
  //----------DOC RELATION SHIP MANAGEMENT :
  /*
  //Remove a son from document
  case ('SuppressSon'):
    if(!$check_flood) break;
    if (!empty($_REQUEST["rs_link_id"])){
      require_once('class/productStructure/relationship.php');
      $docrel = new relationship($odocument);
      foreach ($_REQUEST["rs_link_id"] as $link_id){
        $docrel->SuppressSon($link_id);
      }
    }
  break;
  */
	} //End of method
	

	//---------------------------------------------------------------------
	public function getfathersAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		//$this->_helper->viewRenderer->setNoRender(true);
		if (! $this->_initdocAction ())
			return $this->_cancel ();
		$document = Rb_Document::get($this->space_name, $this->document_id);
		Ranchbe::checkPerm ( 'document_get', $document, true );
		$docrel = new Rb_Doclink ( $document );
		$this->view->father_documents = $docrel->getFathers ();
		$this->view->document_name = $document->getName ();

	//----------DOC RELATION SHIP MANAGEMENT :
	/*
  //View father doclink
  case ('ViewFatherDoc'):
    if (!empty($_REQUEST["document_id"])){
      require_once('class/productStructure/relationship.php');
      $docrel = new relationship($odocument);
      $father_documents = $docrel->getFathers();
      $this->view->assign_by_ref('father_documents', $father_documents);
    }
  break;
  */
	
	} //End of method
	

	//----------DOCUMENT ALTERNATIVES - NOT IN USE FOR THE MOMENT:
	/*
  case ('AddAlternative'):
    if(!$check_flood) break;
    if(!isset($_REQUEST['l_selected'])){
      $this->view->assign('actionName', 'AddAlternative');
      $this->view->assign('alternative_group', $_REQUEST['alternative_group']);
      $this->view->display('selectDocument.tpl');
      die;
    }else{
      require_once('class/productStructure/alternative.php');
      $alternative = new alternative($odocument);
      foreach ($_REQUEST['l_selected'] as $dthtmlxtreeId){
        $alt_doc = DthtmlxtreeIdToArray($dthtmlxtreeId);
        $alternative->AddAlter($alt_doc['document_class'],$alt_doc['space'],$_REQUEST['alternative_group']);
      }
    }
  break;
  
  //----------DOCUMENT ALTERNATIVES - NOT IN USE FOR THE MOMENT:
  //Remove an alternative from document
  case ('SuppressAlter'):
    if(!$check_flood) break;
    if (!empty($_REQUEST['alt_link_id'])){
      require_once('class/productStructure/alternative.php');
      $alternative = new alternative($odocument);
      foreach ($_REQUEST['alt_link_id'] as $link_id){
        $alternative->SuppressAlter($link_id);
      }
    }
  break;
  */
	
	//---------------------------------------------------------------------
	/*! \brief return xml tree of documents
	*/
	public function getdocumenttreeAction() {
		$this->view->id = $this->getRequest ()->getParam ( 'id' );
		$space_id = $this->getRequest ()->getParam ( 'space_id' );
		$container_id = $this->getRequest ()->getParam ( 'container_id' );
		$category_id  = $this->getRequest ()->getParam ( 'category_id' );
		$document_id  = $this->getRequest ()->getParam ( 'document_id' );
		
		/*Create an array from string content in id from dthtmlxtree js lib
		* $id is a string form like this : key__value--otherkey__othervalue--...etc
		*/
		$id_string = explode ( '--', $this->view->id );
		foreach ( $id_string as $keyvalue ) {
			$keyvalue_explode = explode ( '__', $keyvalue );
			$keyvalue_explode [0] = $keyvalue_explode [1];
		}
		
		if (!$space_id) {
			$this->view->spacelist = array (
									'space__bookshop'=>'bookshop',
									'space__cadlib'=>'cadlib', 
									'space__mockup'=>'mockup', 
									'space__workitem'=>'workitem' );
		}
		
		
		//Get container list
		if ($space_id && ! $container_id) {
			$this->view->containerlist = Rb_Container::get ( $this->space_name )->getAll ();
		}
		
		//Get categories list
		if ($container_id  && ! $category_id ) {
			$container = Rb_Container::get ( $this->space_name, $container_id ); //Create new manager
			$params ['sort_field'] = 'category_number';
			$params ['sort_order'] = 'ASC';
			$category = new Rb_Category ();
			$this->view->categorieslist = $category->getAll ( $params );
			$params ['where'] [] = 'category_id IS NULL';
			$params ['sort_field'] = 'document_number';
			$params ['sort_order'] = 'ASC';
			$this->view->documentlist = $container->getContents ( $params );
		}
		
		//If get documents
		if ($category_id && ! $document_id) {
			$container = Rb_Container::get ( $this->space_name, $container_id ); //Create new manager
			$params ['exact_find'] ['category_id'] = $category_id;
			$params ['sort_field'] = 'document_number';
			$params ['sort_order'] = 'ASC';
			$this->view->documentlist = $container->getContents ( $params );
		}
	} //End of method

} //End of class
