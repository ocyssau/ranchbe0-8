<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


class Document_WorkflowController extends controllers_document_abstract {
	protected $page_id = 'documentDetail'; //(string)
	protected $ifSuccessForward = array ('module' => 'document', 'controller' => 'workflow', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'document', 'controller' => 'workflow', 'action' => 'index' );
	
	public function init() {
		parent::init ();
		$this->view->odocument =& $this->document ; //register object to use in smarty plugins
		$this->view->document_id = $this->document_id ;
		return true;
	} //End of method
	

	function indexAction() {
		$this->getAction ();
	} //End of method
	

	//---------------------------------------------------------------------
	function getAction() {
		$this->_getinstances ();
		//$this->_getinstancedetail();
		//$this->_getactivitydetail();
		
		//To hide/show the tabs
		$this->view->assign ( 'content1display', 'block' );
		$this->view->assign ( 'content2display', 'none' );
		$this->view->assign ( 'content3display', 'none' );
		$this->view->assign ( 'content4display', 'none' );
		
		$this->error_stack->checkErrors ();
		$this->view->display ( 'document/WF/workflowLayout.tpl' );
	} //End of method

	
	//---------------------------------------------------------------------
	function getinstancesAction() {
		$this->_getinstances ();
		$this->error_stack->checkErrors ();
		$this->view->display ( 'documentManager/documentDetailLayout.tpl' );
	} //End of method
	

	//---------------------------------------------------------------------
	/* Monitor Instances
	*/
	protected function _getinstances() {
		//require_once('conf/galaxia_setup.php');
		//include_once (GALAXIA_LIBRARY . '/src/ProcessMonitor/ProcessMonitor.php');
		$processMonitor = new ProcessMonitor ( Ranchbe::getDb () );
		$popup = '1';
		$wheres [] = "gi.name='" . $this->document->getProperty ( 'normalized_name' ) . "'";
		$where = implode ( ' and ', $wheres );
		$offset = 0;
		$maxRecords = 100;
		$sort_mode = 'instanceId_asc';
		$find = '';
		$items = $processMonitor->monitor_list_instances ( $offset, $maxRecords, $sort_mode, $find, $where );
		$this->view->items = $items ['data'];
		//var_dump ( $items );
		//$this->view->assign ( 'documentInstances', $this->view->fetch ( 'documentManager/WF/Instances.tpl' ) );
	} //End of method
	

	//---------------------------------------------------------------------
	function getinstancedetailAction() {
		$this->_getinstancedetail ();
		$this->error_stack->checkErrors ();
		$this->view->display ( 'document/documentDetailLayout.tpl' );
	} //End of method
	

	//---------------------------------------------------------------------
	/* Details of Instance
	*/
	protected function _getinstancedetail() {
		//require_once ('conf/galaxia_setup.php');
		/*
		require_once('configs/galaxia_setup.php');
		require_once('Galaxia/src/ProcessMonitor/ProcessMonitor.php');
		$processMonitor = new ProcessMonitor($dbGalaxia);
		*/
		if (isset ( $_REQUEST ['iid'] )) {
			$this->view->iid = $iid;
			$popup = '1';
			$smarty = & $this->view;
			include 'galaxia_admin_instance.php';
			$this->view->documentInstanceDetail = 
				$this->view->fetch ( 'document/WF/InstanceDetail.tpl' );
		}
	} //End of method
	

	//---------------------------------------------------------------------
	function getactivitydetailAction() {
		$this->_getactivitydetail ();
		$this->error_stack->checkErrors ();
		$this->view->display ( 'document/documentDetailLayout.tpl' );
	} //End of method
	

	//---------------------------------------------------------------------
	/* Details of Instance
	*/
	protected function _getactivitydetail() {
		//require_once ('conf/galaxia_setup.php');
		/*
		require_once('configs/galaxia_setup.php');
		require_once('Galaxia/src/ProcessMonitor/ProcessMonitor.php');
		$processMonitor = new ProcessMonitor($dbGalaxia);
		*/
		if (isset ( $_REQUEST ['aid'] )) {
			$popup = '1';
			$smarty = & $this->view;
			include 'galaxia_admin_instance_activity.php';
			$this->view->assign ( 'documentInstanceDetail', $this->view->fetch ( 'documentManager/WF/Activities.tpl' ) );
		}
	} //End of method

} //End of class
