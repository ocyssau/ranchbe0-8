<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/content/abstract.php');
class Content_IndexController extends controllers_content_abstract {
	protected $page_id = 'contentManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'content', 'controller' => 'get', 'action' => 'document' );
	protected $ifFailedForward = array ('module' => 'content', 'controller' => 'get', 'action' => 'document' );
	
	public function init() {
		parent::init ();
		return true;
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function freezeallAction() {
		if (! Ranchbe::checkPerm ( 'document_get', $this->container, false )) {
			$this->error_stack->checkErrors ();
			return $this->_forward ( 'index', 'index', 'containerManager' );
		}
		
		$freeze = new Rb_Freeze ( );
		
		//GetDocument
		$list = Rb_Container::get ( $this->space_name, $this->container_id )->getContents ( array (), false );
		
		foreach ( $list as $docInfos ) {
			$freeze->freeze ( Rb_Document::get ( $this->space_name, $docInfos ['document_id'] ) );
		}
		
		$this->_successForward ();
		
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	public function getlistfileAction() {
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		if (! Ranchbe::checkPerm ( 'document_get', $this->container, false )) {
			$this->error_stack->checkErrors ();
			return $this->_forward ();
		}
		
		//Get list of scripts
		$form = & new RbView_Pear_Html_QuickForm ( 'scriptsList', 'post', $this->actionUrl ( 'getlistfile' ) );
		
		$form->addElement ( 'header', 'editheader', tra ( 'Select a render to use' ) );
		
		$form->setDefaults ( array ('result_format' => 'xls', 'result_extend' => 'filter' ) );
		
		//Select result format (xls or csv)
		$form->addElement ( 'radio', 'result_format', 'csv', '', 'csv' );
		$form->addElement ( 'radio', 'result_format', 'excel', '', 'xls' );
		
		//Select xls render
		$path_queries_scripts = DEFAULT_XLSRENDER_SCRIPTS_DIR . '/documentManager';
		$scriptfiles = array ();
		foreach ( glob ( $path_queries_scripts . '/*.php' ) as $scriptfile ) {
			$scriptfiles [] = basename ( $scriptfile );
		}
		$scriptfiles = array_combine ( $scriptfiles, $scriptfiles );
		$select = & $form->addElement ( 'select', 'xls_render', tra ( 'Render' ), $scriptfiles );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		//Select extend
		$form->addElement ( 'radio', 'result_extend', 'all', '', 'all' );
		$form->addElement ( 'radio', 'result_extend', 'current filter', '', 'filter' );
		
		//Submit
		$form->addElement ( 'submit', 'action', 'downloadListing' );
		$form->addElement ( 'submit', 'cancel', 'Cancel' );
		
		$form->addElement ( 'hidden', 'container_id', $this->container_id );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		
		// Tries to validate the form
		if ($form->validate ()) {
			$result_format = $this->getRequest ()->getParam ( 'result_format' );
			$xls_render = $this->getRequest ()->getParam ( 'xls_render' );
			
			$container = & Rb_Container::get ( $this->space_name, $this->container_id );
			
			$params = array ();
			$params ['offset'] = 0; //desactive pagination
			$params ['numrows'] = 99999; //desactive limit of display
			if ($result_format === 'csv') {
				//get all documents
				$list = $container->getContents ( $params, true );
				$this->smarty->assign_by_ref ( 'list', $list );
				header ( "Content-type: application/csv " );
				header ( "Content-Disposition: attachment; filename=" . $container->getName () . '_content.csv' );
				$this->smarty->display ( './documentManager/getcsv.tpl' );
				die ();
			} else if ($result_format === 'xls') {
				require_once ($path_queries_scripts . '/' . $xls_render);
				excel_render ( $container, $params );
				die ();
			}
		}
		$form->display ();
		die ();
	} //End of method


} //End of class

