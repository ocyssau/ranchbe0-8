<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

abstract class controllers_content_abstract extends controllers_abstract{
	protected $container; //container object
	protected $container_id; //(int)
	protected $container_name; //(string)
	protected $ticket_id; //(string)

	public function init(){
		$this->error_stack =& Ranchbe::getError();

		$this->space_name = $this->getRequest()->getParam('space');
		$this->page_id = $this->space_name.$this->page_id;
		$this->ticket = $this->getRequest()->getParam('ticket');

		$this->container_id = $this->getRequest()->getParam('container_id');

		if( empty( $this->space_name ) && Ranchbe::getContext()->getProperty('space_name') )
		$this->space_name = Ranchbe::getContext()->getProperty('space_name');

		if( empty( $this->container_id ) && Ranchbe::getContext()->getProperty('container_id') )
		$this->container_id = Ranchbe::getContext()->getProperty('container_id');

		if( ( empty($this->container_id) ) || ( empty( $this->space_name ) )
		){ //if none selected container redirect to accueil
			Ranchbe::getError()->warning('None active container');
			Ranchbe::getError()->checkErrors();
			die;
		}

		$this->container =& Rb_Container::get($this->space_name , $this->container_id); //Create new manager
		$this->space =& $this->container->getSpace();

		//Assign name to particular fields
		$this->view->assign('container_id', $this->container_id);
		$this->view->assign('SPACE_NAME', $this->space_name);
		$this->view->assign('CONTAINER_NUMBER' , $this->space_name . '_number');
		$this->view->assign('CONTAINER_DESCRIPTION' , $this->space_name . '_description');
		$this->view->assign('CONTAINER_STATE' , $this->space_name . '_state');
		$this->view->assign('CONTAINER_ID' , $this->space_name . '_id');

		/*
		if($this->container){
			if($this->container->getProperty('read_id')){
				$this->view->assign('reposit_dir', Rb_Reposit_Read::getReadReposit($this->container)
				->getProperty('url'));
				$this->view->assign('thumbs_dir', Rb_Reposit_Read::getReadReposit($this->container)
				->getProperty('reposit_url').'/__attachments/_thumbs');
				$this->view->assign('thumbs_extension', '.png');
			}
		}
		*/

		//Zend_Registry::set('space',$this->space);
		//Zend_Registry::set('container',$this->container);

		$this->_initForward();
	} //End of method

} //End of class
