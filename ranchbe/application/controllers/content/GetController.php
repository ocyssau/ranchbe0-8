<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

class Content_GetController extends controllers_content_abstract {
	protected $page_id = 'contentManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'content', 
										'controller' => 'get', 
										'action' => 'document' );
	protected $ifFailedForward = array ('module' => 'content', 
										'controller' => 'get', 
										'action' => 'document' );
	
	public function init() {
		return parent::init ();
	} //End of method

	//----------------------------------------------------------------------------------------------------
	public function documentAction() {
		$this->view->dojo()->enable();
		if (! Ranchbe::checkPerm ( 'document_get', $this->container, false )) {
			Ranchbe::getError()->checkErrors ();
			return $this->_forward ( 'index', 'index', 'container' );
		}
		$search = new Rb_Search_Db ( Rb_Document::get ( $this->space_name )->getDao () );
		$search->setSelect ( '*' );
		$filter = new RbView_Helper_Docfilter ( $this->getRequest (), $search, 'document' );
		$filter->setDefault ( 'sort_field', 'document_number' );
		$filter->setDefault ( 'sort_order', 'ASC' );
		$filter->finish ();

		//set the union for get extends properties
		$filter->setWithExtends ( Rb_Document::get ( $this->space_name )->getDao () );
		$this->view->list = $this->container->getContents ( $filter->getParams (), 
											$filter->getOption ( 'displayHistory' ) );
		
		$pagination = new RbView_Helper_Pagination($filter);
	    $pagination->setPagination( count($this->view->list) );
	    $pagination->setRequest( $this->getRequest() );
	    $this->view->views_helper_pagination = $pagination->fetchForm( $this->view->getEngine () );

		//Show or hide the docfile manager tab
		$hideDocfileManage = ( int ) $this->getRequest ()->getParam ( 'hideDocfileManage' );
		if ($hideDocfileManage)
			$_SESSION ['DisplayDocfileTab'] = false; //reset session var to hide the file manager tab. See tabs.tpl and mySpaceTabs.tpl
		
		// Display the template
		RbView_Menu::get ()->getMySpace ();
		RbView_Tab::get ( 'documentTab' )->activate ();
		$this->view->assign ( 'views_helper_filter_form', $filter->fetchForm ( $this->view->getEngine (), $this->container ) ); //generate code for the filter form
		$this->view->assign ( 'object_class', 'document' );
		Ranchbe::getError()->checkErrors ();
		$this->_helper->actionStack ( 'toolbar' , 'index', 'document' );
		return;
	} //End of method
	
	//----------------------------------------------------------------------------------------------------
	public function docfileAction() {
		if (! Ranchbe::checkPerm ( 'document_get', $this->container, false )) {
			Ranchbe::getError()->checkErrors ();
			return $this->_forward ( 'index', 'index', 'container' );
		}
		
		$docfile = & Rb_Docfile::get ( $this->space_name );
		$search = new Rb_Search_Db ( $docfile->getDao () );
		$filter = new RbView_Helper_Recordfilefilter ( $this->getRequest (), $search, 'docfilefilter' );
		$filter->setDefault ( 'sort_field', 'file_name' );
		$filter->setDefault ( 'sort_order', 'ASC' );
		$filter->setTemplate('recordfile/searchBar.tpl');
		$filter->finish ();
		
		//get all files
		/*
		$space_name = & $this->space_name;
		$params = array();
		$params ['with'] [] = array ('type' => 'INNER', 'table' => $space_name . '_documents', 'col' => 'document_id' );
		$params ['select'] = array ($space_name . '_documents.document_number', $space_name . '_documents.document_state', $space_name . '_documents.document_iteration', $space_name . '_documents.document_version', $space_name . '_doc_files.*' );
		$params ['exact_find'] [$space_name . '_id'] = $this->container->getId ();
		$params ['where'] [] = 'file_life_stage < 3';
		*/
		$this->view->list = Rb_Docfile::get ( $this->space_name )->getAll ( $params );
		
		$pagination = new RbView_Helper_Pagination($filter);
	    $pagination->setPagination( count($this->view->list) );
	    $pagination->setRequest( $this->getRequest() );
	    $this->view->views_helper_pagination = $pagination->fetchForm( $this->view->getEngine () );
				
		Ranchbe::getError()->checkErrors ();
		
		//Show or hide the docfile manager tab
		$_SESSION ['DisplayDocfileTab'] = true; //reset session var to hide the file manager tab. See tabs.tpl and mySpaceTabs.tpl
		
		// Display the template
		RbView_Menu::get ()->getMySpace ();
		RbView_Tab::get ( 'documentfileTab' )->activate ();
		$this->view->assign('views_helper_filter_form' , $filter->fetchForm($this->view->getEngine(), $this->container) );
		$this->view->assign ( 'object_class', 'docfile' );
		$this->_helper->actionStack ( 'toolbar' , 'index', 'docfile' );
	} //End of method

	//----------------------------------------------------------------------------------------------------
	public function recordfileAction() {
		if (! Ranchbe::checkPerm ( 'document_get', $this->container, false )) {
			Ranchbe::getError()->checkErrors ();
			return $this->_forward ( 'index', 'index', 'container' );
		}
		
		$recordfile = & Rb_Recordfile::get ( $this->space_name );
		$search = new Rb_Search_Db ( $recordfile->getDao () );
		$filter = new RbView_Helper_Recordfilefilter ( $this->getRequest (), $search, 'recordfilefilter' );
		$filter->setDefault ( 'sort_field', 'file_name' );
		$filter->setDefault ( 'sort_order', 'ASC' );
		$filter->setTemplate('recordfile/searchBar.tpl');
		$filter->finish ();
		
		//get all files
		$this->view->list = $recordfile->getAll ( $filter->getParams (), $this->container_id );
		
		$pagination = new RbView_Helper_Pagination($filter);
	    $pagination->setPagination( count($this->view->list) );
	    $pagination->setRequest( $this->getRequest() );
	    $this->view->views_helper_pagination = $pagination->fetchForm( $this->view->getEngine () );
		
		Ranchbe::getError()->checkErrors ();
		
		//Show or hide the docfile manager tab
		if ($_REQUEST ['HideDocfileManage'])
			$_SESSION ['DisplayDocfileTab'] = false; //reset session var to hide the file manager tab. See tabs.tpl and mySpaceTabs.tpl
		
		// Display the template
		RbView_Menu::get ()->getMySpace ();
		RbView_Tab::get ( 'documentTab' )->activate ();
		$this->view->assign ( 'views_helper_filter_form', $filter->fetchForm ( $this->view->getEngine(), $this->container ) );
		$this->view->assign ( 'object_class', 'recordfile' );
		$this->_helper->actionStack ( 'toolbar' , 'index', 'recordfile' );
	} //End of method

	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward ( 'document' );
	} //End of method
	
} //End of class

