<?php

class Training_DojoController extends Zend_Controller_Action {
	public function init() {
		/* Initialize action controller here */
	}
	
	public function indexAction(){
		//$this->view->layout ()->disableLayout ();
		//Ranchbe::getLayout ()->setLayout ( 'dojo' );
		Zend_Dojo::enableView($this->view);
		//Zend_Dojo_View_Helper_Dojo::setUseDeclarative();
		//Zend_Dojo_View_Helper_Dojo::setUseProgrammatic();
		$this->view->dojo()->setLocalPath('js/dojo/dojo.js');
//							->addStyleSheetModule('dijit.themes.tundra');
		
		$request = $this->getRequest();
		$form    = $this->_getForm();
		if ($this->getRequest()->isPost()) {
			if ($form->isValid($request->getPost())) {
				return $this->_helper->redirector('success');
			}
		}
		$this->view->form = $form;
		$this->view->getEngine()->assign_by_ref('dojo', $this->view->dojo());
	} //End of method

	
	public function simplelistAction(){
		Zend_Dojo::enableView($this->view);
		$this->view->dojo()->setLocalPath('js/dojo/dojo.js');
		$this->view->dojo()->requireModule("dojox.form.ListInput");
		$this->_helper->viewRenderer->setScriptAction ( 'index' );
		$this->view->getEngine()->assign_by_ref('dojo', $this->view->dojo());
	} //End of method
	
	
	protected function _getForm(){
		require_once APPLICATION_PATH . '/Form/Dojo.php';
		$form = new Form_Dojo();
		$form->setAction($this->_helper->url('index'));
		return $form;
	}
	
} //End of class
