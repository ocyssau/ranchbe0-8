<?php

class oo{
	public $var1;
}

class Training_IndexController extends Zend_Controller_Action {
	public function init() {
		/* Initialize action controller here */
	}

	public function dojoAction() {
		//Zend_Debug::dump($this->view);die;
		$this->_helper->actionStack ( 'header', 'index' );
		// action body
		$this->view->messages = 'test de message';
		$flashMessenger = $this->_helper->getHelper ( 'FlashMessenger' );
		$message = 'Nous avons fait quelquechose lors de la derni�re requ�te';
		$flashMessenger->addMessage ( $message );
		$this->view->placeholder ( 'foo' )->setPrefix ( "<ul>\n    <li>" )->setSeparator ( "</li><li>\n" )->setIndent ( 4 )->setPostfix ( "</li></ul>\n" )->set ( "Du texte pour plus tard" );
		if ($this->view->doctype ()->isXhtml ()) {
			echo $this->view->escape ( $this->view->doctype ()->getDoctype () );
		}
	}
	
	public function indexAction() {
		//Zend_Debug::dump($this->view);die;
		$this->_helper->actionStack ( 'header', 'index' );
		// action body
		$this->view->messages = 'test de message';
		$flashMessenger = $this->_helper->getHelper ( 'FlashMessenger' );
		$message = 'Nous avons fait quelquechose lors de la derni�re requ�te';
		$flashMessenger->addMessage ( $message );
		$this->view->placeholder ( 'foo' )
			->setPrefix ( "<ul>\n    <li>" )
			->setSeparator ( "</li><li>\n" )
			->setIndent ( 4 )
			->setPostfix ( "</li></ul>\n" )
			->set ( "Du texte pour plus tard" );
		if ($this->view->doctype ()->isXhtml ()) {
			echo $this->view->escape ( $this->view->doctype ()->getDoctype () );
		}
	}

	public function headerAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'header' );
		$this->view->titre = 'Create a mockup';
		$this->view->message = implode ( '<br />', $this->_helper->getHelper ( 'FlashMessenger' )->getMessages () );
	}

	public function optionsAction() {
		$application = Ranchbe::getApplication ();
		$bootstrap = $application->getBootstrap ();
		$options1 = $application->getOptions ();
		$options2 = $bootstrap->getOptions ();
		Zend_Debug::dump ( $options1 ['resources'] ['smarty'] ['view_suffix'] );
		Zend_Debug::dump ( $options2 ['resources'] ['smarty'] ['view_suffix'] );
		//$options1['resources']['smarty']['view_suffix'] = 'optionForApp';
		$options2 ['resources'] ['smarty'] ['view_suffix'] = 'optionForBoot';
		//$application->setOptions($options1);
		$application->setOptions ( $options2 );
		Zend_Debug::dump ( $options1 ['resources'] ['smarty'] ['view_suffix'] );
		Zend_Debug::dump ( $options2 ['resources'] ['smarty'] ['view_suffix'] );
		die ();
	}


	//---------------------------------------------------------------------
	public function uploadfromtxtAction() {
		$this->view->layout ()->disableLayout ();
		//$this->_helper->viewRenderer->setNoRender(true);
		$this->getResponse ()->setHeader ( 'Content-Type', 'application/csv' )->setHeader ( 'Content-Transfer-Encoding', Binary )->setHeader ( 'Content-Disposition', 'attachment; filename=export_partners.csv' );
		//->setHeader ( 'Content-Length', mb_strlen ( $data ) )
		$this->view->list = Rb_Partner::get ()->getAll ();
		//Envoi du fichier dont le chemin est pass� en param�tre
		$this->view->render ( 'partner/index/export.tpl' );
	} //End of method


	//---------------------------------------------------------------------
	public function standardsAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->view->layout ()->disableLayout ();
		$this->_helper->viewRenderer->setNoController(true);
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->viewRenderer->setNeverRender(true);
		$this->render ( 'reposit/edit', null, true );
		$this->getResponse ()->setHeader ( 'Content-Type', 'application/csv' )
							->setHeader ( 'Content-Transfer-Encoding', Binary )
							->setHeader ( 'Content-Disposition', 'attachment; filename=export_partners.csv' );
		$this->view->render ( 'partner/index/export.tpl' );
		$this->_helper->viewRenderer->setScriptAction ( 'edit' );
		$this->view->headLink()->offsetSetStylesheet ( $index=5, $stylesheet='styles/toto.css' );
		$this->view->headLink()->appendStylesheet ( $stylesheet='styles/toto.css' );
		$this->view->headScript()->appendFile(Ranchbe::getConfig()->js->baseurl.'lib.js');
    	$this->view->headScript()->appendFile(Ranchbe::getConfig()->js->baseurl.'qfamsHandler.js');
		
		$this->_helper->url('index');
		
		/*To execute in background
		 $mode = $this->getRequest ()->getParam ( 'mode' );
		 if ($mode == 'background') { //Tache execute en arriere plan
			session_write_close (); //To close the current session and unlock the Session. If not do that, its impossible to continue work with another scripts of ranchbe
			ignore_user_abort ( true ); //Continue execution after deconnection of user
			print (Rb_Date::formatDate ( $start_time ) . tra ( 'import_manager_notice1' ) . '<a href="javascript:window.close()">Close Window</a><br />') ;
			flush (); // On vide le buffer de sortie
			}
			*/

		//get controller name
		$this->getRequest()->getControllerName();
		//get action name
		$this->getRequest()->getActionName();
		//get module name
		$this->getRequest()->getModuleName();
		//get params
		$this->getRequest()->getParams();

	} //End of method


	//---------------------------------------------------------------------
	public function memoryAction() {
		$this->view->layout ()->disableLayout ();
		$this->_helper->viewRenderer->setNoRender(true);
		$stime = time();
		//$flist = glob('C:\Program Files\Zend\Apache2\htdocs\ranchbe0.8\userdatas_test\reposit\mockup\DEPOT1\__imported\*.txt');
		$list = Rb_Recordfile::get('mockup')->getAll(array(
														'select'=>array('file_id'),
													));
		$reg = array();
		$space = new Rb_Space('mockup');
		//var_dump($list);
		$i = 0;
		foreach ( $list as $file ) {
			$file_id = $file['file_id'];
			//$object = new Rb_Fsdata ( $file ); //Construct the fsdata object
			$object = new Rb_Recordfile($space , $file_id);
			//$object =& Rb_Recordfile::get('mockup' , $file_id);
			//$object = new oo();
			//$object->var = $list;
			//$reg[] = $object;
			//var_dump($object);
			//$fsdata = null;
			if(is_int($i / 100)){
				//Rb_Recordfile::flushRegistry();
				Ranchbe::getError()->flush(true);
				gc_collect_cycles();
				$ctime = time() - $stime;
				echo $file_id .'--'. $i .'--'. memory_get_usage()/1024 
									. 'Ko -- time :'. $ctime .'<br />';
			}
			if(is_int($i / 100)){
				$reg = array();
				echo 'by 100<br />';
			}
			//usleep(10000);
			//Ranchbe::getAcl ()->remove( $object );
			unset ($object);
			$i++;
		}
		
		echo 'Finished';
		
	} //End of method

	//---------------------------------------------------------------------
	public function repositAction() {		
		$space = new Rb_Space('mockup');
		$reposit = Rb_Reposit::getActiveReposit($space);
		var_dump(Rb_Dao_Abstract::$_lastQuery, $reposit->getName());
		
		die;
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function formAction() {		
		$techDoc = new Zend_Form_Element_Multiselect('technical_doc');
		$techDoc->setLabel('Technical Documents')
				->setMultiOptions(array('o1'=>'option1','o2'=>'option2','o3'=>'option3'));
		/*
				->setMultiOptions(array('Option Group 1' => array('option1','option2','option3'),
										'Option Group 2'=> array('option1','option2','option3')));
		*/
		$techDoc->size = 8;
		$techDoc->setValue(array('option1','option2'));
		//$techDoc->setOptions(array('option1','option2'));
		//var_dump($techDoc->getValue());die;
		$techDoc->setDecorators(array(
									'Advmultiselect', 
									'Errors', 
									array('HtmlTag', array()), 
									array('Label', array()),
									));
		//Zend_Debug::Dump($techDoc->getDecorators());die;
		$this->view->form->addElement($techDoc);
		
		$decorator = array('ViewHelper','Errors',
							    array('HtmlTag', array()),
							    array('Label', array())
				    );
		$ticket = new Zend_Form_Element_Hidden( 'ticket' );
		
	} //End of method
	
	//---------------------------------------------------------------------
	public function mimesAction() {		
		$docfile = Rb_Docfile::get('workitem', 20153);
		$file = $docfile->getProperty('file');
		$mm = new Zend_Mime_Message();
        $mp = new Zend_Mime_Part( file_get_contents($file) );
        $mp->encoding = Zend_Mime::ENCODING_BASE64;
        $mp->type = Zend_Mime::TYPE_OCTETSTREAM;
        $mp->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
        $mp->filename = $docfile->getName();
        $mm->addPart($mp);
        echo '<pre>';
		$mime = new Zend_Mime();
		$body = '';
        $body .= $mime->boundaryLine();
        $body .= $mp->getHeaders();
		$body .= $mm->generateMessage();
        $body .= $mime->mimeEnd();
        
        echo urlencode($body);
        
		die;
	} //End of method
	
	
	

} //End of class
