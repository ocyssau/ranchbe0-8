<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


require_once ('controllers/abstract.php');
abstract class controllers_perms_abstract extends controllers_abstract {
	protected $object_id; //(int)
	protected $resource_id; //(string)
	protected $role_id; //(string)
	protected $page_id = 'perms'; //(string)
	protected $ifSuccessForward = array ('module' => 'project', 'controller' => 'perms', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'project', 'controller' => 'perms', 'action' => 'index' );
	
	//---------------------------------------------------------------------
	public function indexAction() {
		$this->getAction ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_getgroups ();
		$this->_getrules ();
		$this->error_stack->checkErrors ();
		$this->_initDisplay();
		$this->render ( 'perms/get', null, true );
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _getgroups() {
		//Display the group list
		$groups = Rb_Group::getGroups ();
		foreach ( $groups as $group_id => $group ) {
			$group ['role_id'] = Rb_Group::getStaticRoleId ( $group_id );
			if (empty ( $this->role_id ))
				$this->role_id = $group ['role_id'];
			if ($group ['role_id'] == $this->role_id) { //memorize current group name to display to user
				$this->view->currentGroup = $group ['group_define_name'];
			}
			$groups [$group_id] = $group;
		}
		$this->view->groups = $groups;
		return $this->view->groups;
	} //End of method
	

	//---------------------------------------------------------------------
	//Get the users list with groups
	protected function _getusers() {
		foreach ( Rb_User::getUsers () as $user_id => $user ) {
			$user = Rb_User::get ( $user_id );
			$users [$user_id] = $user->getProperties ();
			$users [$user_id] ['role_id'] = $user->getRoleId ();
			foreach ( $user->getGroups () as $group_id ) {
				$users [$user_id] ['groups'] [$group_id] = Rb_Group::get ( $group_id )->getProperties ();
			}
		}
		return $users;
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _getrules() {
		if (empty ( $this->resource_id ))
			return false;
		if (empty ( $this->role_id ))
			return false;
			//Get all permissions
		$rules = Ranchbe::getAcl ()->getRules ( $this->resource_id, $this->role_id );
		$this->view->privileges = $rules;
	} //End of method
	

	//---------------------------------------------------------------------
	public function saveAction() {
		if (! Ranchbe::checkPerm ( 'admin_users', $this->resource_id, false )) {
			return $this->_cancel ();
		}
		
		$privileges = $this->getRequest ()->getParam ( 'privileges' );
		
		if (Ranchbe::getAcl ()->save ( $this->role_id, $this->resource_id, $privileges )) {
			$this->view->assign ( 'msg', 'Permissions updated successful' );
		} else {
			$this->view->assign ( 'msg', 'Permissions update failed' );
		}
		
		return $this->getAction ();
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _initDisplay() {
		$this->view->assign ( 'resource_id', $this->resource_id );
		$this->view->assign ( 'role_id', $this->role_id );
		$this->view->assign ( 'object_id', $this->object_id );
	} //End of method


} //End of class
