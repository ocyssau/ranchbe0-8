<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


require_once ('controllers/abstract.php');
abstract class Perms_abstract extends controllers_project_abstract {
	protected $object_id; //(int)
	protected $resource_id; //(string)
	protected $role_id; //(string)
	protected $page_id = 'projectLinks'; //(string)
	protected $ifSuccessForward = array ('module' => 'perms', 'controller' => 'perms', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'perms', 'controller' => 'perms', 'action' => 'index' );
	
	//---------------------------------------------------------------------
	public function indexAction() {
		$this->getgroupsAction ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		$this->_getgroups ();
		$this->_getperms ();
		$this->error_stack->checkErrors ();
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _getgroups() {
		//Display the group list
		$groups = Rb_Group::getGroups ();
		foreach ( $groups as $group_id => $group ) {
			$group ['role_id'] = Rb_Group::getStaticRoleId ( $group_id );
			if (empty ( $this->role_id ))
				$this->role_id = $group ['role_id'];
			if ($group ['role_id'] == $this->role_id) { //memorize current group name to display to user
				$this->smarty->assign ( 'currentGroup', $group ['group_define_name'] );
			}
			$groups [$group_id] = $group;
		}
		
		$this->smarty->assign_by_ref ( 'groups', $groups );
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _getperms() {
		if (empty ( $this->resource_id ))
			return false;
		if (empty ( $this->role_id ))
			return false;
			
		//Get all permissions
		$privileges = Ranchbe::getAcl ()->getRules ( $this->resource_id, $this->role_id );
		$this->smarty->assign_by_ref ( 'privileges', $privileges );
	
	} //End of method
	

	//---------------------------------------------------------------------
	public function saveAction() {
		$privileges = $this->getRequest ()->getParam ( 'privileges' );
		
		Ranchbe::getAcl ()->save ( $this->role_id, $this->resource_id, $privileges );
		
		$this->smarty->assign ( 'msg', 'Permissions updated successful' );
		return $this->getAction ();
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _display($template) {
		$this->smarty->assign ( 'resource_id', $this->resource_id );
		$this->smarty->assign ( 'role_id', $this->role_id );
		$this->smarty->assign ( 'object_id', $this->object_id );
		$this->smarty->display ( $template );
	} //End of method


} //End of class
