<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');
class Import_HistoryController extends controllers_abstract {
	protected $history; //(Rb_Import_History)
	protected $page_id = 'ImportHistory'; //(string)
	protected $ifSuccessForward = array ('module' => 'import', 'controller' => 'history', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'import', 'controller' => 'history', 'action' => 'index' );
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->space_name = $this->getRequest ()->getParam ( 'space' );
		$this->ticket = $this->view->ticket;
		if (! $this->space_name)
			$this->space_name = Ranchbe::getContext ()->space_name;
		
		if ($this->getRequest ()->getParam ( 'ifSuccessForward' )) {
			$this->ifSuccessForward = $this->getRequest ()->getParam ( 'ifSuccessForward' );
			$this->ifSuccessForward = $this->parseUrl ( $this->ifSuccessForward );
		}
		
		if ($this->getRequest ()->getParam ( 'ifFailedForward' )) {
			$this->ifSuccessForward = $this->getRequest ()->getParam ( 'ifFailedForward' );
			$this->ifSuccessForward = $this->parseUrl ( $this->ifFailedForward );
		}
		
		$cancel = $this->getRequest ()->getParam ( 'cancel' );
		if ($cancel)
			$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		
		$this->container_id = $this->getRequest ()->getParam ( 'container_id' );
		$this->container = & Rb_Container::get ( $this->space_name, $this->container_id ); //Create new manager
		$this->space = & $this->container->getSpace ();
		
		$this->history = new Rb_Import_History ( $this->space );
		
		//$this->check_flood = check_flood($_REQUEST['page_id']);
		//Zend_Registry::set ( 'space', $this->space );
		//Zend_Registry::set ( 'container', $this->container );
		

		//Assign name to particular fields
		$this->view->assign ( 'SPACE_NAME', $this->space_name );
		
		if ($this->container_id) {
			$this->view->assign ( 'container_id', $this->container_id );
			$this->view->assign ( 'container_number', $this->container->getProperty ( 'container_number' ) );
		}
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward ( 'get' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		Ranchbe::checkPerm ( 'get', $this->container, true );
		//Ranchbe::getLayout ()->setLayout ( 'popup' );
		

		$search = new Rb_Search_Db ( $this->history->getDao () );
		$filter = new RbView_Helper_Filter ( $this->getRequest (), $search, $this->page_id );
		$filter->setUrl ( './import/history/get' );
		$filter->setDefault ( 'sort_field', 'import_order' );
		$filter->setDefault ( 'sort_order', 'DESC' );
		$filter->finish ();
		
		//get infos on all package
		if ($this->container_id)
			$search->setFind ( $this->container_id, $this->container->getFieldName ( 'id' ), 'exact' );
		
		$this->view->list = $this->history->getAll ( $filter->getParams () );
		
		$pagination = new RbView_Helper_Pagination($filter);
	    $pagination->setPagination( count($this->view->list) );
	    $pagination->setRequest( $this->getRequest() );
	    $this->view->views_helper_pagination = $pagination->fetchForm( $this->view->getEngine () );
		
		$this->error_stack->checkErrors ();
		
		// Display the template
		RbView_Menu::get ()->getMySpace ();
		if( RbView_Tab::get ( 'documentTab' ) ){
			RbView_Tab::get ( 'documentTab' )->activate ();
		}
		$this->view->assign ( 'views_helper_filter_form', $filter->fetchForm ( $this->view->getEngine (), $this->container ) ); //generate code for the filter form
		$this->view->assign ( 'PageTitle', vsprintf ( tra ( 'Import history in %s' ), array (tra ( $this->space_name ) ) ) );
	} //End of method
	

	//---------------------------------------------------------------------
	/**! \brief Select the imported file of the package file.
	 *
	 */
	public function getimportedAction() { //List the imported files in container
		$import_order = $this->getRequest ()->getParam ( 'import_order' );
		$action = 'recordfile';
		$controlleur = 'get';
		$module = 'content';
		$params = array ('find_import_order' => $import_order, 'activatefilter' => 1 );
		return $this->_forward ( $action, $controlleur, $module, $params );
	} //End of method
	

	//---------------------------------------------------------------------
	public function suppressAction() {
		Ranchbe::checkPerm ( 'editer', $this->container, true );
		//if (!$check_flood) return false;
		$import_orders = $this->getRequest ()->getParam ( 'import_order' );
		if (! is_array ( $import_orders ))
			$import_orders = array ($import_orders );
		foreach ( $import_orders as $import_order ) {
			$history = Rb_Import_History::get ( $this->space_name, $import_order );
			$history->suppress ();
		}
		return $this->_forward ( 'get' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function cleanAction() { //Clean the history import
		Ranchbe::checkPerm ( 'editer', $this->container, true );
		//if (!$check_flood) return false;
		$this->history->cleanHistory ();
		return $this->_forward ( 'index' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function viewerrorAction() {
		Ranchbe::checkPerm ( 'get', $this->container, true );
		$import_order = $this->getRequest ()->getParam ( 'import_order' );
		$history = Rb_Import_History::get ( $this->space_name, $import_order );
		$logdir = Ranchbe::getConfig ()->path->reposit->import_log;
		$file = $logdir . '/' . $history->errors_report;
		if (! $this->_view ( $file )) {
			return $this->_forward ( 'index' );
		}
	} //End of method
	

	//---------------------------------------------------------------------
	public function viewlogAction() {
		Ranchbe::checkPerm ( 'get', $this->container, true );
		$import_order = $this->getRequest ()->getParam ( 'import_order' );
		$history = Rb_Import_History::get ( $this->space_name, $import_order );
		$logdir = Ranchbe::getConfig ()->path->reposit->import_log;
		$file = $logdir . '/' . $history->logfile;
		if (! $this->_view ( $file )) {
			return $this->_forward ( 'index' );
		}
	} //End of method
	

	//---------------------------------------------------------------------
	public function viewcontentAction() {
		Ranchbe::checkPerm ( 'get', $this->container, true );
		//if (!$check_flood) return false;
		$import_order = $this->getRequest ()->getParam ( 'import_order' );
		$history = Rb_Import_History::get ( $this->space_name, $import_order );
		$logdir = Ranchbe::getConfig ()->path->reposit->import_log;
		$file = $logdir . '/' . $history->listfile;
		if (! $this->_view ( $file )) {
			return $this->_forward ( 'index' );
		}
	} //End of method

	
	//---------------------------------------------------------------------
	public function detailAction() {
		//Ranchbe::checkPerm ( 'get', $this->container, true );
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		//if (!$check_flood) return false;
		$import_order = $this->getRequest ()->getParam ( 'id' );
		$history = Rb_Import_History::get ( $this->space_name, $import_order );
		$this->view->properties = $history->getProperties();
		$this->view->PageTitle = 'Detail of importation '.$history->getId().
								' <i>('.$history->getName().')</i>';
		$this->view->sameurl = 'import/history/detail';
	} //End of method
	

	//---------------------------------------------------------------------
	protected function _view($file) {
		$fsdata = new Rb_Fsdata ( $file );
		if ($fsdata->downloadFile ())
			die ();
		else {
			$this->error_stack->push ( Rb_Error::WARNING, array ('element' => $file ), tra ( 'The file \'%element%\' don\'t exist' ) );
			return false;
		}
	} //End of method


} //End of class

