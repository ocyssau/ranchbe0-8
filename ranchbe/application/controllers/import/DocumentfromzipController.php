<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('controllers/content/abstract.php');
class Import_DocumentfromzipController extends controllers_import_document_abstract{
	protected $page_id='docImportManager'; //(string)
	protected $ifSuccessForward = array('module'=>'import','controller'=>'document','action'=>'index');
	protected $ifFailedForward = array('module'=>'content','controller'=>'get','action'=>'document');
	
	/**
	 * Path to temporary zip file to import
	 * 
	 * @var string
	 */
	protected $zipfile = false;

	public function init(){
		parent::init();
		
		//Create a subForm to set the headers
		$form_header = new RbView_Smarty_Form_Sub('form_header', 'post');
		$form_header->addElement('header', '', tra('Batch Import of documents in').' '.
									$this->container->getNumber() );
		$form_header->addElement('header', '', tra('Selection review') );
		$this->form_collection->add( $form_header );
	} //End of method

	//---------------------------------------------------------------------
	public function indexAction(){
		Ranchbe::checkPerm( 'document_create' , $this->container );
		set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode
		$step = $this->getRequest()->getParam('step');

		if($step == 'refresh'){
			$this->zipfile = $this->getRequest()->getParam('zipfile');
			$this->form_collection->addElement('hidden', 'zipfile', $this->zipfile);
			$this->_getInputFromForm();
			$this->_setForm(false);
			$this->view->createForm = $this->form_collection->toHtml ();
		}else if($step == 'validate'){
			$this->zipfile = $this->getRequest()->getParam('zipfile');
			$this->form_collection->addElement('hidden', 'zipfile', $this->zipfile);
			$this->_getInputFromForm ();
			$this->_setForm(true);
			$this->view->createForm = $this->form_collection->toHtml ();
		}else if($step == 'cancel'){
			$this->_cancel();
		}else{
			$this->_getInput ();
			$this->_setForm(false);
			$this->view->createForm = $this->form_collection->toHtml ();
		}
		//var_dump($step, $this->csvfile, $this->view->createForm);die;
		$this->error_stack->checkErrors();
	} //End of method

	//---------------------------------------------------------------------
	/**
	 * Extract attached files from zip file and populate $this->input
	 * 
	 * @return array
	 */
	protected function _getInput(){
		if(!is_file($_FILES['importzipfile']['tmp_name'])){
			return $this->_cancel();
		}
		$wildspace = Rb_User::getCurrentUser()->getWildspace()->getPath();
		$tmpdir = $wildspace . '/.rbetmp';
		if(!is_dir($tmpdir))
			if(!mkdir($tmpdir)){
				print 'cant create tmpdir';
				return false;
			}
		$this->zipfile = $tmpdir .'/'. basename($_FILES['importzipfile']['name']);
		move_uploaded_file($_FILES['importzipfile']['tmp_name'], $this->zipfile);
		$this->form_collection->addElement('hidden', 'zipfile', $this->zipfile);
		
		$package = new Rb_Datatype_Package($this->zipfile);
		$result = $package->unpack ( $tmpdir );
		//return only files that are in root of archive :
		$pb_max = 0; //use by progress observer
		$unpack = array();
		$files = array();
		while ( $result->next () ) { //for each unpack file
			$unpack [] = $result->getFilename (); // = c:/mockup_dir/mockup/__imported/caddsPart/_pd
			$pb_max ++; //use by progress observer
			$fname = basename( $result->getFilename () );
			$this->input[] = array(
								'file'=>$fname,
								'document_number'=>$fname
			);
		}
		//var_dump($unpack, $this->input, $tmpdir, $this->zipfile);die;
		return $this->input;
	} //End of method

} //End of class
