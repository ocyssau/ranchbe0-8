<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('controllers/content/abstract.php');
class Import_DocumentfromcsvController extends controllers_import_document_abstract{
	protected $page_id='docImportManager'; //(string)
	protected $ifSuccessForward = array('module'=>'import','controller'=>'document','action'=>'index');
	protected $ifFailedForward = array('module'=>'content','controller'=>'get','action'=>'document');
	
	/**
	 * Path to temporary csv file to parse
	 * 
	 * @var string
	 */
	protected $csvfile = false;
	
	public function init(){
		parent::init();
		
		//Create a subForm to set the headers
		$form_header = new RbView_Smarty_Form_Sub('form_header', 'post');
		$form_header->addElement('header', '', tra('Batch Import of documents in').' '.
		$this->container->getNumber() );
		$form_header->addElement('header', '', tra('Selection review') );
		$this->form_collection->add( $form_header );
	} //End of method

	//---------------------------------------------------------------------
	public function indexAction(){
		Ranchbe::checkPerm( 'document_create' , $this->container );
		set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode
		$step = $this->getRequest()->getParam('step');
		if($step == 'refresh'){
			$this->csvfile = $this->getRequest()->getParam('csvfile');
			$this->form_collection->addElement('hidden', 'csvfile', $this->csvfile);
			$this->_getInputFromForm();
			$this->_setForm(false);
			$this->view->createForm = $this->form_collection->toHtml ();
		}
		else if($step == 'validate'){
			$this->csvfile = $this->getRequest()->getParam('csvfile');
			$this->form_collection->addElement('hidden', 'csvfile', $this->csvfile);
			$this->_getInputFromForm();
			$this->_setForm(true);
			$this->view->createForm = $this->form_collection->toHtml ();
		}
		else if($step == 'cancel'){
			$this->_cancel();
		}
		else{
			$this->_getInput();
			$this->_setForm(false);
			$this->view->createForm = $this->form_collection->toHtml ();
		}
		//var_dump($step, $this->csvfile, $this->view->createForm);die;
		$this->error_stack->checkErrors();
	} //End of method

	//---------------------------------------------------------------------
	/**
	 * Extract documents and properties from csv file and populate $this->input
	 * 
	 * @return array
	 */
	protected function _getInput(){
		if( !is_file($_FILES['importDocumentCsvList']['tmp_name']) ){
			return $this->_cancel();
		}
		$wildspace = Rb_User::getCurrentUser()->getWildspace()->getPath();
		$tmpdir = $wildspace . '/.rbetmp/';
		if(!is_dir($tmpdir))
			if(!mkdir($tmpdir)){
				print 'cant create tmpdir';
				return false;
			}
		$this->csvfile = $tmpdir . basename($_FILES['importDocumentCsvList']['tmp_name']);
		move_uploaded_file($_FILES['importDocumentCsvList']['tmp_name'], $this->csvfile);
		$this->form_collection->addElement('hidden', 'csvfile', $this->csvfile);
		$this->input = Rb_Datatype_Csv::parse($this->csvfile);
		return $this->input;
	} //End of method

} //End of class
