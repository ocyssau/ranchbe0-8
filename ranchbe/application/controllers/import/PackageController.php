<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


require_once ('controllers/abstract.php');
class Import_PackageController extends controllers_abstract {
	protected $_package = false; //(rb_import)
	protected $page_id = 'importPackageManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'import', 'controller' => 'package', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'import', 'controller' => 'package', 'action' => 'index' );
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->ticket = $this->getRequest ()->getParam ( 'ticket' );
		$space_name = $this->getRequest ()->getParam ( 'space' );
		if (! $space_name && Ranchbe::getContext ()->getProperty ( 'space_name' ))
			$space_name = Ranchbe::getContext ()->getProperty ( 'space_name' );
		$this->space = Rb_Space::get ( $space_name );
		$this->packages_reposit = realpath ( Ranchbe::getConfig ()->path->reposit->import );
		$this->default_target_dir = realpath ( Ranchbe::getConfig ()->path->reposit->unpack );
		return true;
	} //End of method
	

	//---------------------------------------------------------------------------
	/**! \brief Get package reposit in package reposit.
	 *   Return a array if no errors, else return false.
	 *   
	 */
	protected function _getDatas(array $params = array()) {
		$path = Ranchbe::getConfig ()->path->reposit->import;
		$return = array ();
		$fsdatas = Rb_Directory::getCollectionDatas ( $path, $params, '(\.zip)|(\.tar)|(\.z)|(\.gz)$' );
		if (! $fsdatas)
			return $return;
		foreach ( $fsdatas as $key => $fsdata ) {
			$return [$key] = $fsdata->getProperties ();
			$import_order = Rb_Import_History::getIdFromMd5 ( $this->space->getName (), $fsdata->getProperty ( 'file_md5' ) );
			if ($import_order) {
				$import = new Rb_Import_History ( $this->space, $import_order );
				$return [$key] = array_merge ( $return [$key], $import->getProperties () );
			} else {
				$return [$key] ['import_order'] = $fsdata->getProperty ( 'file_name' );
			}
		}
		return $return;
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	protected function _getImportForm() {
		$form = new Zend_Form ( );
		$option = array ();
		$target = new Zend_Form_Element_Radio ( 'target' );
		$target->setLabel ( tra ( 'Target' ) )
				->addMultiOption ( 'InTempdir', sprintf ( tra ( 'Import in temp dir %s' ), $this->default_target_dir ) )
				->addMultiOption ( 'InContainer', sprintf ( tra ( 'Import in current container %s' ), '' ) );
		$form->addElement ( $target );
		/*
		$mode = new Zend_Form_Element_Radio ( 'mode' );
		$mode->setLabel ( tra ( 'Running mode' ) )
				->addMultiOption ( 'background', 'In background' )
				->addMultiOption ( 'direct', 'Direct' );
		$form->addElement ( $mode );
		*/
		return $form;
	} //End of method
	
	
	//---------------------------------------------------------------------------
	/**! \brief Import the content of the package file in the container.
	 *  Return true or false.
	 *
	 * This method list the files that they were imported from the package $import_order.
	 * If a file has been imported twice, only the last package source file can be list.
    \param Rb_Datatype_Package
    \param 
    \param $lock(bool) If true, the files or the document will be locked after import.
	 */
	protected function _import(Rb_Datatype_Package &$package, $to, $lock = false) {
		//Check if the package is not in progress
		//Ranchbe::getError()->push(Rb_Error::ERROR, array('element'=>$packInfos['package_file_name'], 'debug'=>array()), 'this package %element% is in progress');
		
		//record package in history
		$history = new Rb_Import_History ( $this->space, 0 );
		$history->setProperty ( 'file_name', $package->getProperty ( 'file_name' ) );
		$history->setProperty ( 'file_extension', $package->getProperty ( 'file_extension' ) );
		$history->setProperty ( 'file_mtime', $package->getProperty ( 'file_mtime' ) );
		$history->setProperty ( 'file_size', $package->getProperty ( 'file_size' ) );
		$history->setProperty ( 'file_md5', $package->getProperty ( 'file_md5' ) );
		$history->setProperty ( 'state', 'InProgress' );
		
		$to = strtolower ( $to );
		if ($to == 'incontainer') {
			//Zend_Debug::dump(Ranchbe::getContext()->getProperties());die;
			$to_container = Rb_Container::get ( Ranchbe::getContext ()->space_name, Ranchbe::getContext ()->container_id );
			Ranchbe::checkPerm( 'edit' , $to_container , true);
			$this->_importToContainer ( $history, $package, $to_container, $lock );
		} else if ($to == 'intempdir') {
			$this->_importToDir ( $history, $package, $this->default_target_dir, $lock );
		}
		$history->save ();
		return $history;
	} //End of method
	

	//---------------------------------------------------------------------------
	/**! \brief Import the content of the package file in directory
	 *  @param Rb_Import_History
	 *  @param Rb_Datatype_Package
	 *  @param String
	 *  @param Bool /If true, the files or the document will be locked after import.
	 *  @return Rb_Import_History
	 */
	protected function _importToDir(Rb_Import_History &$history, Rb_Datatype_Package &$package, $to_dir, $lock = false) {
		if (! is_dir ( $to_dir )) {
			$history->setProperty ( 'state', 'error' );
			return $history;
		}
	    $adapter = new Zend_ProgressBar_Adapter_JsPush(array('updateMethodName' => 'Zend_ProgressBar_Update',
                                                             'finishMethodName' => 'Zend_ProgressBar_Finish'));
		$progressBar = new Zend_ProgressBar($adapter, 0, 10);
        $progressBar->update(1, $package->getProperty('file_name'));
		$result = $package->unpack ( $to_dir );
		if (! $result) {
			$history->setProperty ( 'state', 'error' );
			return $history;
		}
		//usleep(1000000);
		$progressBar->finish();
		$history->setProperty ( 'state', 'inTempDir' );
		$history->setProperty ( 'target_dir', $to_dir );
		return $history;
	} //End of method
	
	
	//---------------------------------------------------------------------------
	/**! \brief Import the content of the package file in the container.
	 *
	 *  @param Rb_Import_History
	 *  @param Rb_Datatype_Package
	 *  @param Rb_Container
	 *  @param Bool /If true, the files or the document will be locked after import.
	 *  @return Rb_Import_History
	 */
	protected function _importToContainer(Rb_Import_History &$history, Rb_Datatype_Package &$package, Rb_Container &$to_container, $lock = false) {
		//Init the log
		$logdir = Ranchbe::getConfig ()->path->reposit->import_log;
		if (! is_dir ( $logdir )) {
			trigger_error ( $logdir . ' is unreachable', E_USER_ERROR );
		}
		$log_id = uniqid ( 'log' );
		$log = new Rb_Import_Log ( $logdir . '/' . $log_id . '_log.txt' );
		$contentlog = new Rb_Import_Log ( $logdir . '/' . $log_id . '_content.txt' );
		$errorlog = new Rb_Import_Log ( $logdir . '/' . $log_id . '_errors.txt' );
		
		$history->setErrorLog($errorlog);
		
	    $adapter = new Zend_ProgressBar_Adapter_JsPush(array('updateMethodName' => 'Zend_ProgressBar_Update',
                                                             'finishMethodName' => 'Zend_ProgressBar_Finish'));
		$progressBar = new Zend_ProgressBar($adapter);
		
		$to_reposit = Rb_Reposit::getActiveReposit ( $to_container->getSpace () );
		
		if (! $to_reposit){
			$progressBar->finish();
			foreach(Ranchbe::getError()->getErrors(false) as $error)
				$errorlog->addContent( $error['message'] );
			$errorlog->save ();
			$history->setProperty ( 'errors_report', $errorlog->getProperty ( 'file_name' ) );
			$history->setProperty ( 'state', 'error' );
			return $history;
		}
		$to_dir = Rb_Reposit::getBasePath ( $to_reposit,  Ranchbe::getConfig()->reposit->suffix );
		
		$history->setContainer ( $to_container );
		$history->setReposit ( $to_reposit );

		$result = $package->unpack ( $to_dir );
		if (! $result) {
			$progressBar->finish();
			foreach(Ranchbe::getError()->getErrors(false) as $error)
				$errorlog->addContent( $error['message'] );
			$errorlog->save ();
			$history->setProperty ( 'errors_report', $errorlog->getProperty ( 'file_name' ) );
			$history->setProperty ( 'state', 'error' );
			return $history;
		}
		
		//return only files that are in root of archive :
		$pb_max = 0; //use by progress observer
		$unpack = array();
		while ( $result->next () ) { //for each unpack file
			//@todo : a refaire pour prendre en compte cadds. L'importation dans un sous repertoire de fichier est permis:
			// valide : ./reposit_dir/MAQ001/cgr/file.cgr
			// valide : ./reposit_dir/MAQ001/file.CATPart
			// les parts cadds devrons �tre vue comme une entit� au niveau du dossier cadds
			// valide : ./reposit_dir/MAQ001/caddspart
			// non valide : ./reposit_dir/MAQ001/caddspart/_pd
			$unpack [] = $result->getFilename (); // = c:/mockup_dir/mockup/__imported/caddsPart/_pd
			$pb_max ++; //use by progress observer
		}

		$progressBar = new Zend_ProgressBar($adapter, 0, $pb_max);
		$unpack_Root = array_unique ( $unpack );
		$history->save(); //save here to set history_id on recordfile
		$i = 1;
		$stime = time();
		$pb_step = (int) ($pb_max / 100);
		if($pb_step == 0) $pb_step = 10;
		foreach ( $unpack_Root as $file ) {
			$fsdata = new Rb_Fsdata ( $file ); //Construct the fsdata object
			if (! $fsdata->getProperty ( 'dataType' )) {
				$errorlog->addContent('Error during importing of : ' . $file . ' : file do not existing on filesystem');
				continue;
			}
			
			if(is_int($i / ($pb_step * 3) ) ){
				Ranchbe::getError()->flush(true);
				//Flush logs
				$log->save ();
				$contentlog->save ();
				$errorlog->save ();
			}
			if( is_int($i / $pb_step ) ){
				$ctime = time() - $stime;
				$message = basename($file) .'--'. $i .'--'
							. memory_get_usage()/1024 . 'Ko -- exectime :'. $ctime .'sec';
	        	$progressBar->update($i, $message );
				//echo $message;
			}
			$file_id = Rb_Recordfile::getFileId ( $to_container->getSpace ()->getName (), 
												  $fsdata->getProperty ( 'file_name' ) );
			//Test if file exist in this container
			if ($file_id) {
				$recordfile = new Rb_Recordfile( $to_container->getSpace (), $file_id );
				//if recordfile is in other reposit, suppress previous data and update record
				if( $recordfile->getProperty ( 'reposit_id' ) !=  $to_reposit->getId () ){
					$otherfile = $recordfile->getProperty('file');
					$recordfile->getFsdata()->suppress();
					$recordfile->setFsdata($fsdata);
					$log->addContent('delete file in other reposit ' . $otherfile );
				}
				if ($recordfile->getProperty ( 'file_md5' ) != $fsdata->getProperty ( 'file_md5' )) { // md5 are differents
					$recordfile->setProperty ( 'file_iteration', $recordfile->getProperty ( 'file_iteration' ) + 1 );
					$recordfile->setProperty ( 'file_update_date', time () );
					$recordfile->setProperty ( 'file_update_by', Rb_User::getCurrentUser ()->getId () );
					$recordfile->setFsdata($fsdata);
					$log->addContent('replace ' . $file . ' new version : ' . $recordfile->file_iteration);
				} else { // md5 are equal
					$log->addContent('The file ' . $file . ' is not updated : imported file is same that existing file.');
				}
				$recordfile->setProperty ( 'reposit_id', $to_reposit->getId () );
				$recordfile->setImportOrder ( $history->getId () );
				$recordfile->setFather ( $to_container );
				$recordfile->save ( false, false );
			//The file dont exist
			} else {
				$recordfile = new Rb_Recordfile ( $to_container->getSpace (), 0 ); //Construct new recordfile
				$recordfile->setImportOrder ( $history->getId () );
				$recordfile->setProperty ( 'reposit_id', $to_reposit->getId () );
				$recordfile->setFather ( $to_container );
				$recordfile->setFsdata ( $fsdata );
				$ok = $recordfile->save ();
				if (! $ok ) { //Record the file
					$errorlog->addContent('Error during record of ' . $file);
				} else {
					$log->addContent('create ' . $file);
				}
			}
			$contentlog->addContent( $file );
			unset ( $recordfile );
			$i++;
		} //end of foreach
		$progressBar->finish();

		$log->save ();
		$contentlog->save ();
		$errorlog->save ();
		$history->setProperty ( 'logfile', $log->getProperty ( 'file_name' ) );
		$history->setProperty ( 'listfile', $contentlog->getProperty ( 'file_name' ) );
		$history->setProperty ( 'errors_report', $errorlog->getProperty ( 'file_name' ) );
		$history->setProperty ( 'state', 'inContainer' );
		return $history;
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward ( 'get' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		
		$search = new Rb_Search_Filesystem ( );
		$filter = new RbView_Helper_Filefilter ( $this->getRequest (), $search, $this->page_id );
		$filter->setUrl ( './import/' . $this->space_name );
		$filter->setDefault ( 'sort_field', 'file_name' );
		$filter->setDefault ( 'sort_order', 'ASC' );
		$filter->setTemplate ( 'wildspace/searchBar.tpl' );
		$filter->finish ();
		
		//get infos on files
		$this->view->list = $this->_getDatas ( $filter->getParams () );
		$this->view->form = $this->_getImportForm ();
		$this->error_stack->checkErrors ();
		$this->view->PageTitle = vsprintf ( tra ( '%s import package manager' ), array (tra ( $this->space_name ) ) );
		$this->view->views_helper_filter_form = $filter->fetchForm ( $this->view->getEngine (), $this->container );
		
		$pagination = new RbView_Helper_Pagination($filter);
	    $pagination->setPagination( count($this->view->list) );
	    $pagination->setRequest( $this->getRequest() );
	    $this->view->views_helper_pagination = $pagination->fetchForm( $this->view->getEngine () );
		
		// Display the template
		RbView_Menu::get ()->getMySpace ();
		RbView_Tab::get ( 'documentTab' )->activate ();
		
		/*
	  	Ranchbe::checkPerm( 'get' , $this->container , true);
	    $search = new Rb_Search_Db($this->_package->getDao());
	    $filter = new views_helper_filter( $this->getRequest(), $search, $this->page_id);
	    $filter->setUrl('./importManager/'.$this->space_name);
	    $filter->setDefault('sort_field','import_order');
	    $filter->setDefault('sort_order','DESC');
	    $filter->finish();
	    set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode
	    $list = $this->_package->getAll($filter->getParams()); //get infos on all package in reposit directories
	    $this->view->assign_by_ref('list', $list);
	    $pagination = new views_helper_pagination($filter);
	    $pagination->setPagination( count($list) );
	    $this->error_stack->checkErrors();
	    // Display the template
	    RbView_Menu::get()->init()->getMySpace();
	    RbView_Tab::get('documentTab')->activate();
	    $this->view->assign('sameurl' , './importManager/package/get'); //important: is first assign
	    $this->view->assign('views_helper_filter_form' , $filter->fetchForm($this->view, $this->container) ); //generate code for the filter form
	    $this->view->assign('views_helper_pagination' , $pagination->fetchForm($this->view) ); //generate code for the filter form
	    $this->view->assign('PageTitle' , vsprintf(tra('%s import package manager'), array(tra($this->space_name))) );
	    $this->view->assign('mid', 'importManager/fileImport.tpl');
	    $this->view->display('ranchbe.tpl');
	    */
	} //End of method
	

	//---------------------------------------------------------------------
	public function uploadAction() {
		//Ranchbe::checkPerm( 'get' , $this->container , true);
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->indexAction ();
		$overwrite = $this->getRequest ()->getParam ( 'overwrite' );
		$file = $_FILES ['uploadFile'];
		Rb_File::uploadFile ( $file, $overwrite, $this->packages_reposit );
		$this->indexAction ();
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function suppressAction() {
		//Ranchbe::checkPerm( 'edit' , $this->container , true);
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->indexAction ();
		$files = $this->getRequest ()->getParam ( 'import_order' );
		$path = $this->packages_reposit;
		foreach ( $files as $file ) {
			$file_name = basename ( $file );
			$package = new Rb_Fsdata ( $path . '/' . $file_name );
			$package->suppress ();
		}
		return $this->_forward ( 'get' );
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function uncompressAction() {
		//Ranchbe::checkPerm( 'edit' , $this->container , true);
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->indexAction ();
		$files = $this->getRequest ()->getParam ( 'file_name' );
		$path = $this->packages_reposit;
		foreach ( $files as $file ) {
			$file_name = basename ( $file );
			$package = new Rb_Datatype_Package ( $path . '/' . $file_name );
			$package->uncompress ();
		}
		return $this->_forward ( 'get' );
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function importAction() {
		//FOR DEBUG:
		//return $this->importstartAction();
		
		//Ranchbe::getLayout ()->setLayout ( 'popup' );
		$files = $this->getRequest ()->getParam ( 'file_name' );
		$mode = $this->getRequest ()->getParam ( 'mode' );
		$target = $this->getRequest ()->getParam ( 'target' );
		$this->view->iframe_src = "import/package/importstart/file_name/"
									.implode('/file_name/', $files)
									."/target/$target"
									."/space/$space_name/ticket/$this->ticket";
		//var_dump($this->view->iframe_src);die;
		$this->view->successUrl = $this->view->baseUrl('import/package/importsuccess');
		$this->view->title = sprintf(tra('Import of %s in progress'), $files[0]);
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function importstartAction() {
		$this->view->layout ()->disableLayout ();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$files = $this->getRequest ()->getParam ( 'file_name' );
		if(!is_array($files)) $files = array($files);
		$target = $this->getRequest ()->getParam ( 'target' );
		$path = $this->packages_reposit;
		$flashMessenger = $this->_helper->getHelper ( 'FlashMessenger' );
		
		set_time_limit ( 24 * 3600 ); //To increase default TimeOut. No effect if php is in safe_mode
		//Zend_progressbar require a output_buffering value not too great. 1024 seems be a good value
		//You must adapt this value in your php.ini or in htaccess file
		//switch off output buffering, require by progress bar 
		ob_end_flush();
		
		$start_time = time ();
		foreach($files as $file){
			$file_name = basename ( $file );
			$package = new Rb_Datatype_Package ( $this->packages_reposit . '/' . $file_name );
			$history = $this->_import ( $package, $target, false );
			$flashMessenger->addMessage('<h1>'.sprintf(tra('End import of package %s'), $file_name).'</h1>');
			//@todo : revoir les objets import afin d'eviter les appels multiple a getPackageInfos
			if ($history->getProperty ( 'state' ) == 'error') {
				$body = '<b>' . tra ( 'Not Imported package' ) . ' : </b>' . $package->getProperty ( 'file_name' ) . '<br/>';
				$body .= '<b>' . tra ( 'An error is occured during import task' ) . '</b><br />';
				$body .= '<b>' . tra ( 'See the logfile' ) . '</b>: '. $history->getErrorLog()->getProperty('file') .'<br />';
				$body .= '<i>';
				foreach( $history->getErrorLog()->getContent() as $error)
					$body .= $error.'<br />';
				$body .= '</i>';
				$subject = vsprintf ( tra ( 'Error in import task of %s' ), array ($package->getProperty ( 'file_name' ) ) );
			} else {
				$body = '<b>' . tra ( 'Imported package' ) . ' : </b>' . $package->getProperty ( 'file_name' ) . '<br/>';
				$subject = tra ( 'End of import task of ' ) . $package->getProperty ( 'file_name' );
			}
			$end_time = time ();
			$duration = ($end_time - $start_time);

			if($history->getReposit ()->url)
				$body .= '<b>' . tra ( 'target dir' ) . ' : </b>' . $history->getReposit ()->url . '<br/>';
			else
				$body .= '<b>' . tra ( 'target dir' ) . ' : </b>' . $history->getProperty('target_dir') . '<br/>';
			if($history->getContainer () )
				$body .= '<b>' . tra ( 'target container' ) . ' : </b>' . $history->getContainer ()->getName () . '<br/>';
			$body .= '<b>' . tra ( 'By' ) . ' : </b>' . Rb_User::getCurrentUser ()->getUsername () . '<br/>';
			$body .= '<b>' . tra ( 'start to' ) . ' : </b>' . Rb_Date::formatDate ( $start_time ) . '<br/>';
			$body .= '<b>' . tra ( 'end to' ) . ' : </b>' . Rb_Date::formatDate ( $end_time ) . '<br/>';
			$body .= '<b>' . tra ( 'duration' ) . ' : </b>' . $duration . ' sec<br/>';
			
			$flashMessenger->addMessage ( $body );
			
			//Send message to user
			$message = new Rb_Message ( Ranchbe::getDb () );
			$to = Rb_User::getCurrentUser ()->getId ();
			$from = 'system';
			$cc = '';
			$body = str_replace ( '<br/>', "\n", $body );
			$priority = 3;
			$message->postMessage ( $to, $from, $to, $cc, $subject, $body, $priority );
			
		}
		$flashMessenger->addMessage ( '<hr />' );
		//return $this->_redirect('import/package/importsuccess');
		//return $this->_forward('importsuccess');
		//$this->getResponse()->clearAllHeaders();
		//$this->_redirector = $this->_helper->getHelper('Redirector');
		//$this->_redirector->gotoUrl('import/package/importsuccess');
	} //End of method
	
	
	//---------------------------------------------------------------------
	//List the content of a package
	public function importsuccessAction() {
		//$this->view->layout ()->setLayout ( 'popup' );
		$this->view->message = implode ( '<br />', $this->_helper->getHelper ( 'FlashMessenger' )->getMessages () );
		$this->error_stack->checkErrors ();
	} //End of method

	
	//---------------------------------------------------------------------
	//List the content of a package
	public function viewcontentAction() {
		Ranchbe::checkPerm ( 'get', $this->container, true );
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$file_name = basename ( $this->getRequest ()->getParam ( 'file_name' ) );
		$package = new Rb_Datatype_Package ( $this->packages_reposit . '/' . $file_name );
		$content = $package->getContent ( false );
		if ($content === false) {
			return $this->_forward ( 'get' );
		}
		$this->view->pageTitle = sprintf ( tra ( 'content of package %s' ), $package->getProperty ( 'file_name' ) );
		$count = 0;
		$list = array ();
		while ( $content->next () ) {
			$count ++;
			$stat = $content->getStat ();
			$list [$count] ['file_name'] = $content->getFilename ();
			$list [$count] ['mtime'] = $stat ['mtime'];
			$list [$count] ['file_size'] = $stat ['size'];
			$list [$count] ['gid'] = $stat ['gid'];
			$list [$count] ['uid'] = $stat ['uid'];
		}
		$this->view->list = $list;
		$this->view->count = $count;
	} //End of method


//---------------------------------------------------------------------
//add a description to this package
/*
	public function adddescriptionAction() {
		$cancel = $this->getRequest ()->getParam ( 'cancel' );
		if ($cancel || ! Ranchbe::checkPerm ( 'edit', $this->container, false )) {
			return $this->_cancel ();
		}
		
		$import_order = $this->getRequest ()->getParam ( 'import_order' );
		$description = $this->getRequest ()->getParam ( 'description' );
		if (! $import_order)
			die ( 'none package selected' );
		
		$form = & new RbView_Pear_Html_QuickForm ( 'addDescription', 'POST', $this->actionUrl ( 'adddescription' ) ); //Construct the form with QuickForm lib
		$form->addElement ( 'header', 'title1', tra ( 'Describe this package' ) );
		$form->addElement ( 'hidden', 'import_order', $import_order );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', $this->ticket );
		
		//Set defaults values of elements
		$form->setDefaults ( array ('description' => $this->_package->getPackageDescription ( $import_order ) ) );
		
		//Add fields for input informations
		$form->addElement ( 'textarea', 'description', tra ( 'Description' ), array ('rows' => 3, 'cols' => 20 ) );
		
		// Process the modify request
		if ($description && $import_order) {
			if (RbView_Flood::checkFlood ( $this->ticket )) {
				$this->_package->getImporthistory ()->addImportPackageDescription ( $description, $import_order );
			}
			$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
			return;
		}
		$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		$form->applyFilter ( '__ALL__', 'trim' );
		$form->display ();
	} //End of method
	*/

} //End of class
