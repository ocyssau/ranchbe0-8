<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('controllers/content/abstract.php');


/**
 * To use methods of this class, set the var input in require format
 * 	from derived class
 * 
 */
abstract class controllers_import_document_abstract extends controllers_content_abstract{
	protected $page_id='docImportManager'; //(string)
	protected $ifSuccessForward = array('module'=>'import','controller'=>'document','action'=>'index');
	protected $ifFailedForward = array('module'=>'content','controller'=>'get','action'=>'document');
	
	/**
	 * 
	 * @var Rb_Import
	 */
	protected $import;
	
	/**
	 * 
	 * @var RbView_Smarty_Form_Collection
	 */
	protected $form_collection;
	
	/**
	 * Array of array of property_name=>value foreach document to create
	 * The speciale key "file" define the file name to use
	 * i.e
	 * array(0=>array('document_number'=>'DOC001', 'file'=>'file001.ext', 'description'=>'A new document'));
	 * 
	 * @var array
	 */
	protected $input = array();

	/**
	 * Method to populate $this->input
	 * 
	 * @return array
	 */
	protected abstract function _getInput();
	
	/**
	 * (non-PHPdoc)
	 * @see application/controllers/content/controllers_content_abstract#init()
	 */
	public function init(){
		parent::init();
		//$this->import = new Rb_Import($this->container);
		//Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$this->form_collection = new RbView_Smarty_Form_Collection('form', 'post', $this->actionUrl('index') );

		//Set hidden field
		$this->form_collection->addElement('hidden', 'step', 'init', array('id'=>'step') ); //this prop is controled by submit or by choice of category
		$this->form_collection->addElement('hidden', 'redisplay', 0, array('id'=>'redisplay') ); //use to control validation or re-display of the form
		$this->form_collection->addElement('hidden', 'container_number', $this->container->getNumber() );
		$this->form_collection->addElement('hidden', 'container_id', $this->container->getId() );
		$this->form_collection->addElement('hidden', 'ticket', $this->ticket);

		//Add submit button
		//Add js to set the action and to reset the redisplay option state.
		//Redisplay hidden request option is used to prevent execution of validate the form when redisplay the form following change of category
		$this->form_collection->addElement('submit', 'check', tra('Check documents'),
		array('onClick'=>'javascript:getElementById(\'step\').value=\'refresh\';')
		);

		$this->form_collection->addElement('submit', 'validate', tra('Validate'),
		array('onClick'=>'javascript:getElementById(\'step\').value=\'validate\';')
		);

		$this->form_collection->addElement('submit', 'cancel', tra('cancel'),
		array('onClick'=>'javascript:getElementById(\'step\').value=\'cancel\';')
		);
	} //End of method
	
	
	//---------------------------------------------------------------------
	/*
	 * Get the user input in form and format in tdp convention :
	 * return no assoc array
	 * foreach key is a assoc array where ket is property name and value value of property
	 */
	protected function _getInputFromForm(){
		$i=0;
		$fields = $this->getRequest()->getParam('fields');
		if(is_array($fields))
		foreach($fields as $loop_id=>$properties_name){
			foreach($properties_name as $property_name){
				$property_val = $this->getRequest()->getParam($property_name);
				$this->input[$i][$property_name] = $property_val[$loop_id];
			}
			//add the property file if necessary
			$property_val = $this->getRequest()->getParam('file_name');
			if($property_val[$loop_id])
				$this->input[$i]['file'] = $property_val[$loop_id];
			$i++;
		}
		return $this->input;
	} //End of method

	//---------------------------------------------------------------------
	protected function _setForm($validate = false){
		if( !$this->input ){
			return $this->_forward('index','document', 'import');
		}
		$docaction = $this->getRequest()->getParam('docaction');
		$list =& $this->input;
		foreach($list[0] as $key=>$val) //Construct a array with the header of each cols
			$fields[] = $key;
		$tmp_list = array();
		foreach($list as $i=>$tdpInput){
			$smartStore = new RbView_SmartImport($this->space_name);
			$i = md5($i); //to dont use integer for loop id
			$isDocfile = false; //init var
			$isDocument = false; //init var
			$doctype = false; //init var
			$category_id = false; //init var
			
			//---------- The TDP specify a file
			if( !empty($tdpInput['file']) ){
				$file_name = $tdpInput['file'];
				//---------- Check if file exist in wildspace
				$data =& $smartStore->testFile($file_name, Rb_User::getCurrentUser()->getWildspace()->getPath());
				if( !$data ){//file dont exist so return error message to user
					$smartStore = new RbView_SmartStoreForm($this->space_name);
					$smartStore->setError($file_name.' is not in your Wildspace.');
					$this->form_collection->add( $smartStore->setForm($i) );
					continue;
				}

				//---------- Check if file is recorded in database and try to init document and docfile
				$isDocfile = $smartStore->isDocfile($data);

				if( !$isDocfile ){
					if( $tdpInput['document_number'] ){ //A document number is set in TDP
						$document_number = $tdpInput['document_number'];
					}else{ //set document number from file_name
						$document_number = $data->getProperty('doc_name');
					}
					$isDocument = $smartStore->isDocument( $data->getProperty('doc_name') );
				}

			}else     //---------- The TDP specifie a document number
			if( !empty($tdpInput['document_number']) ){
				$document_number = $tdpInput['document_number'];
				//---------- Check if document is recorded in database and try to init document and docfile
				$isDocument = $smartStore->isDocument( $document_number );
			}

			//---------- if is not a docfile or document init a new document and a new docfile
			//---------- Check the doctype for the new document
			if( !$isDocfile && !$isDocument ){
				$smartStore->initNewDocument($this->container, $data, $document_number);
				$doctype =& $smartStore->setDoctype();
			}

			//---------- Set the category
			if( $tdpInput['category_id'] ){
				if(!ctype_digit($tdpInput['category_id'])){
					$smartStore->setError('category_id of '. $document_number . ' is not a integer');
					$category_id = false;
				}else{
					$category_id = (int) $tdpInput['category_id'];
				}
			}
			$smartStore->setCategory( $doctype, $category_id );

			//---------- Set the document_version
			if( $tdpInput['document_version'] ){
				if(!ctype_digit($tdpInput['document_version'])){
					$smartStore->setError('document_version of '. $document_number . ' is not a integer');
				}
			}

			//---------- Check double documents
			if( in_array($smartStore->getDocument()->getNumber() , $tmp_list) ){
				$smartStore->setError($smartStore->getDocument()->getNumber().
                                              ' is not unique in this package');
			}else{
				$tmp_list[] = $smartStore->getDocument()->getNumber();
			}

			$this->form_collection->add(
			$smartStore->setForm($i, $docaction[$i], $validate,  $tdpInput)
			);

		} //End of foreach

		$this->form_collection->addElement('hidden', 'loop', $i);
		return $this->form_collection;
	} //End of method

} //End of class
