<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Ranchbe is distributed in the hope that it will be useful,                |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Ranchbe; if not, write to the Free Software                    |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once ('controllers/abstract.php');
class Home_IndexController extends controllers_abstract {
	protected $page_id = 'home'; //(string)
	protected $ifSuccessForward = array ();
	protected $ifFailedForward = array ();
	protected $user; //(string) current user name
	
	public function init() {
		$this->user = Rb_User::getCurrentUser ()->getUsername ();
		if ($this->getRequest ()->getParam ( 'ifSuccessForward' )) {
			$this->ifSuccessForward = $this->getRequest ()->getParam ( 'ifSuccessForward' );
			$this->ifSuccessForward = $this->parseUrl ( $this->ifSuccessForward );
		}
		
		if ($this->getRequest ()->getParam ( 'ifFailedForward' )) {
			$this->ifSuccessForward = $this->getRequest ()->getParam ( 'ifFailedForward' );
			$this->ifSuccessForward = $this->parseUrl ( $this->ifFailedForward );
		}
		
		$cancel = $this->getRequest ()->getParam ( 'cancel' );
		if ($cancel)
			$this->_forward ( $this->ifSuccessForward ['action'], 
							  $this->ifSuccessForward ['controller'],
							  $this->ifSuccessForward ['module'],
							  $this->ifSuccessForward ['params'] );
		$this->ticket = $this->view->ticket;
	} //End of method
	
	//------------------------------------------------------------------------------
	public function indexAction() {
		$this->_getUnreadMessages ();
		//$this->_getTasks ();
		RbView_Menu::get ();
		RbView_Tab::get ( 'homeTab' )->activate ();
		//$this->_helper->viewRenderer->setResponseSegment('section');
		Rb_Message_Notification::clear ( Rb_User::getCurrentUser ()->getId () );
		$this->_helper->actionStack ( 'toolbar' );
	} //End of method
	
	//------------------------------------------------------------------------------
	// Get unread messages
	protected function _getUnreadMessages() {
		$offset = 0;
		$maxRecords = 5;
		$sort_mode = 'date_desc';
		$find = '';
		$flag = 'isRead';
		$flagval = 'n';
		$orig_or_reply = "r";
		
		if (! isset ( $_REQUEST ['replyto'] ))
			$_REQUEST ['replyto'] = '';
		
		$messulib = new Rb_Message ( Ranchbe::getDb () );
		
		$items = $messulib->listUserMessages ( $this->user, 
												$offset, 
												$maxRecords, 
												$sort_mode, 
												$find, 
												$flag, 
												$flagval, 
												'', '', 
												$_REQUEST ['replyto'], 
												$orig_or_reply );
		$this->view->items = $items ['data'];
	} //End of method
	
	//------------------------------------------------------------------------------
	//Get list of received tasks
	protected function _getTasks() {
		$tasklib = new Rb_Task ( Ranchbe::getDb () );
		$sort_mode = 'priority_desc';
		$tasklist = $tasklib->list_tasks ( $this->user, 
											$offset, 
											$maxRecords, 
											$find, 
											$sort_mode, 
											$show_private = false, 
											$show_submitted = false, 
											$show_received = true, 
											$show_shared = false, 
											false, 
											null );
		$this->view->tasklist = $tasklist ['data'];
	} //End of method
	
} //End of class
