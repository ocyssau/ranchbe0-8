<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


class Tasks_IndexController extends Zend_Controller_Action {
	protected $page_id = 'tasks_index'; //(string)
	protected $ifSuccessForward = array ('module' => 'tasks', 'controller' => 'index', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'tasks', 'controller' => 'index', 'action' => 'index' );
	
	//------------------------------------------------------------------------------
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->user_id = Rb_User::getCurrentUser ()->getName ();
		
		$this->task = new Rb_Task ( Ranchbe::getDb () );
		$this->message = new Rb_Message ( Ranchbe::getDb () );
		
		$this->task_id = ( int ) $this->getRequest ()->getParam ( 'task_id' );
		$this->task_ids = ( array ) $this->getRequest ()->getParam ( 'task' );
		
		//default values for show options
		if (! isset ( $_SESSION ['show_trash'] ))
			$_SESSION ['show_trash'] = false;
		if (! isset ( $_SESSION ['show_completed'] ))
			$_SESSION ['show_completed'] = true;
		if (! isset ( $_SESSION ['show_private'] ))
			$_SESSION ['show_private'] = true;
		if (! isset ( $_SESSION ['show_received'] ))
			$_SESSION ['show_received'] = true;
		if (! isset ( $_SESSION ['show_submitted'] ))
			$_SESSION ['show_submitted'] = true;
		if (! isset ( $_SESSION ['show_shared'] ))
			$_SESSION ['show_shared'] = true;
		if (! isset ( $_SESSION ['show_admin'] ))
			$_SESSION ['show_admin'] = false;
		if (! isset ( $_SESSION ['admin_mode'] ))
			$_SESSION ['admin_mode'] = false;
		
		if ($task_admin and isset ( $_REQUEST ["admin_mode_off"] )) {
			$admin_mode = false;
			$_SESSION ['admin_mode'] = false;
		}
		
		if ($_SESSION ['admin_mode'] and $task_admin) {
			$admin_mode = true;
			$smarty->assign ( 'admin_mode', $admin_mode );
		} else {
			$admin_mode = false;
		}
		
		if ($_SESSION ['show_trash']) {
			$smarty->assign ( 'show_trash', true );
			$show_trash = true;
		} else {
			$show_trash = false;
		}
		
		if ($_SESSION ['show_completed']) {
			$smarty->assign ( 'show_completed', true );
			$show_completed = true;
		} else {
			$show_completed = false;
		}
		
		if ($_SESSION ['show_private']) {
			$smarty->assign ( 'show_private', true );
			$show_private = true;
		} else {
			$show_private = false;
		}
		
		if ($_SESSION ['show_received']) {
			$smarty->assign ( 'show_received', true );
			$show_received = true;
		} else {
			$show_received = false;
		}
		
		if ($_SESSION ['show_submitted']) {
			$smarty->assign ( 'show_submitted', true );
			$show_submitted = true;
		} else {
			$show_submitted = false;
		}
		
		if ($_SESSION ['show_shared']) {
			$smarty->assign ( 'show_shared', true );
			$show_shared = true;
		} else {
			$show_shared = false;
		}
		
		if ($_SESSION ['show_admin']) {
			$smarty->assign ( 'show_admin', true );
			if ($tiki_p_tasks_admin == 'y')
				$show_admin = true;
			else
				$show_admin = false;
		} else {
			$show_admin = false;
		}
		
		if ($_REQUEST ["show_form"] == 'y') {
			$show_form = true;
			$smarty->assign ( 'show_form', $show_form );
		} else {
			$show_form = false;
		}
		
		if ($task_admin and isset ( $_REQUEST ["admin_mode"] ) and $task_admin) {
			$admin_mode = true;
			$_SESSION ['admin_mode'] = true;
		}
		
		$maxRecords = 100;
		$smarty->assign ( 'tasks_maxRecords', $maxRecords );
	
	} //End of method
	

	//------------------------------------------------------------------------------
	public function indexAction() {
		$user_for_group_list = $this->user_id;
		
		$show_history = null;
		if (isset ( $_REQUEST ['show_history'] ))
			$show_history = $_REQUEST ['show_history'];
		
		if (($this->task_id) && ! isset ( $_REQUEST ['preview'] )) {
			$info = $this->task->get_task ( $this->user_id, $this->task_id, $show_history, $task_admin );
			if (! (isset ( $info ['user'] ))) {
				$smarty->assign ( 'msg', tra ( "Sorry this task does not exist or you have no rights to view this task" ) );
				$smarty->display ( "error.tpl" );
				die ();
			}
			if ($show_admin)
				$user_for_group_list = $info ['creator'];
			$show_form = true;
			$smarty->assign ( 'show_form', $show_form );
			$task_id = $info ['task_id'];
		} else {
			$info = $this->task->get_default_new_task ( $this->user_id );
		}
		
		/*
if (isset($_REQUEST['tiki_view_mode']) and $_REQUEST['tiki_view_mode'] == 'view') {
	$tiki_view_mode = 'view';
	$smarty->assign('tiki_view_mode', $tiki_view_mode);
    $show_view = true;
    $smarty->assign('show_view', $show_view);
	$show_form = false;
	$smarty->assign('show_form', $show_form);
	$info['parsed'] = $tikilib->parse_data($info['description']);
}

if (isset($_REQUEST['tiki_view_mode']) and $_REQUEST['tiki_view_mode'] == 'edit') {
	$show_form = true;
	$smarty->assign('show_form', $show_form);
}

if (isset($_REQUEST['preview'])) {
	$tiki_view_mode = 'preview';
	$smarty->assign('tiki_view_mode', $tiki_view_mode);
	$show_form = true;
	$smarty->assign('show_form', $show_form);
	$show_view = true;
	$smarty->assign('show_view', $show_view);
	$info = $this->task->get_default_new_task($this->user_id);
}
*/
		
		//var_dump($info);die;
		$smarty->assign ( 'task_id', $this->task_id );
		$smarty->assign ( 'info', $info );
		$smarty->assign ( 'created_Month', date ( 'm', $info ['created'] ) );
		$smarty->assign ( 'created_Day', date ( 'd', $info ['created'] ) );
		$smarty->assign ( 'created_Year', date ( 'Y', $info ['created'] ) );
		$smarty->assign ( 'created_Hour', date ( 'H', $info ['created'] ) );
		$smarty->assign ( 'created_Minute', date ( 'M', $info ['created'] ) );
		
		if ((! isset ( $info ['start'] )) || ($info ['start'] == null)) {
			$info ['start'] = date ( "U" );
			$smarty->assign ( 'start_date', $info ['start'] );
		} else {
			$smarty->assign ( 'start_date', $info ['start'] );
		}
		
		if ((! isset ( $info ['end'] )) || ($info ['end'] == null)) {
			$this->view->assign ( 'end_date', ($info ['start'] + 86400) );
		} else {
			$this->view->assign ( 'end_date', $info ['end'] );
		}
		
		if (! isset ( $_REQUEST ['sort_mode'] )) {
			$sort_mode = 'priority_desc';
		} else {
			$sort_mode = $_REQUEST ['sort_mode'];
		}
		
		if (! isset ( $_REQUEST ['offset'] )) {
			$offset = 0;
		} else {
			$offset = $_REQUEST ['offset'];
		}
		
		$this->view->assign_by_ref ( 'offset', $offset );
		
		if (isset ( $_REQUEST ['find'] )) {
			$find = $_REQUEST ['find'];
		} else {
			$find = null;
		}
		
		$this->view->assign ( 'find', $find );
		
		$this->view->assign ( 'sort_mode', $sort_mode );
		
		if (! $show_form) { //No show form
			

			//Get list of tasks
			$tasklist = $this->task->list_tasks ( $this->user_id, $offset, $maxRecords, $find, $sort_mode, $show_private, $show_submitted, $show_received, $show_shared,
									/*$use_show_shared_for_group*/ false, /*$show_shared_for_group*/ null );
			
			$this->view->assign ( 'tasklist', $tasklist ["data"] );
			
		//---------------------Pagination	
		

		//---------------------Pagination	
		

		} //End of no show form
		

		//$receive_groups = $this->task->userlib->get_groups();
		$receive_groups = Rb_Group::getGroups ();
		$this->view->assign ( 'receive_groups', $receive_groups );
		
		//	$receive_users = $this->task->get_user_with_permissions('tiki_p_tasks_receive');
		//	if(count($receive_users) < 100) $this->view->assign('receive_users', $receive_users);
		

		$percs = array ();
		
		for($i = 0; $i <= 100; $i += 10) {
			$percs [] = $i;
		}
		
		$this->view->assign ( 'percs', $percs );
		
		$this->view->assign ( 'user', $this->user_id );
		
		$img_accepted = "img/icons2/tick.gif";
		$img_accepted_height = "15";
		$img_accepted_width = "15";
		$img_me_waiting = "img/icons2/1.gif";
		$img_me_waiting_height = "16";
		$img_me_waiting_width = "13";
		$img_he_waiting = "img/icons2/icn_forum.gif";
		$img_he_waiting_height = "16";
		$img_he_waiting_width = "16";
		$img_not_accepted = "img/icons2/error.gif";
		$img_not_accepted_height = "14";
		$img_not_accepted_width = "14";
		
		$this->view->assign ( 'img_accepted', $img_accepted );
		$this->view->assign ( 'img_accepted_height', $img_accepted_height );
		$this->view->assign ( 'img_accepted_width', $img_accepted_width );
		$this->view->assign ( 'img_he_waiting', $img_he_waiting );
		$this->view->assign ( 'img_he_waiting_height', $img_he_waiting_height );
		$this->view->assign ( 'img_he_waiting_width', $img_he_waiting_width );
		$this->view->assign ( 'img_me_waiting', $img_me_waiting );
		$this->view->assign ( 'img_me_waiting_height', $img_me_waiting_height );
		$this->view->assign ( 'img_me_waiting_width', $img_me_waiting_width );
		$this->view->assign ( 'img_not_accepted', $img_not_accepted );
		$this->view->assign ( 'img_not_accepted_height', $img_not_accepted_height );
		$this->view->assign ( 'img_not_accepted_width', $img_not_accepted_width );
		
	/*
$this->view->assign('mid', 'tiki-user_tasks.tpl');
$this->view->display("tiki.tpl");
*/
	
	//$this->view->assign ( 'accueilTab', 'active' );
	//$this->view->assign ( 'mid', 'tasks/tiki-user_tasks.tpl' );
	//$this->view->display ( "ranchbe.tpl" );
	
	} //End of method
	

	//------------------------------------------------------------------------------
	public function putintrashAction() {
		$_REQUEST ['update_tasks'] = true;
		$_REQUEST ['action'] = 'move_marked_to_trash';
		$this->task_ids = array ($this->task_id );
	} //End of method
	

	//------------------------------------------------------------------------------
	public function outfromtrashAction() {
		$_REQUEST ['update_tasks'] = true;
		$_REQUEST ['action'] = 'remove_marked_from_trash';
		$this->task_ids = array ($this->task_id );
	} //End of method
	

	//------------------------------------------------------------------------------
	public function updatepercentageAction() {
		if (isset ( $_REQUEST ['update_percentage'] ) && isset ( $_REQUEST ['task_perc'] )) {
			foreach ( $_REQUEST ['task_perc'] as $task => $perc ) {
				if ($perc == 'w')
					$perc = NULL;
				$this->task->update_task_percentage ( $task, $this->user_id, $perc );
			}
		}
	} //End of method
	

	//------------------------------------------------------------------------------
	public function movemarkedtotrashAction() {
		if (($_REQUEST ['action'] == 'move_marked_to_trash') && isset ( $this->task_ids )) {
			foreach ( array_keys ( $this->task_ids ) as $task ) {
				$this->task->mark_task_as_trash ( $task, $this->user_id );
				$trashed_task = $this->task->get_task ( $this->user_id, $task );
				if ($trashed_task ['user'] == $this->user_id) {
					$msg_from = $this->user_id;
					$msg_to = $trashed_task ['creator'];
				} else {
					$msg_from = $this->user_id;
					$msg_to = $trashed_task ['user'];
				}
				
				/*
  			$msg_title = tra('Task'). ' "' . $trashed_task['title'] . '" '.tra('was moved into trash');
  			$msg_body  = '__'. tra('Task').':__';
  			$msg_body .= '^[tiki-user_tasks.php?task_id=' . $trashed_task['task_id']. "|".$trashed_task['title']."]^\n\n";
  			$msg_body .= tra('trashed by'). ': '.$this->user_id;
  
  			if(	$this->user_idlib->user_has_permission($msg_from,'tiki_p_messages') and 
  				$userlib->user_has_permission($msg_to,'tiki_p_messages'))
  			{
  				$this->message->postMessage($msg_to, $msg_from, $msg_to, '', $msg_title, $msg_body, $trashed_task['priority']);
  			}
        */
				
				$msg_title = tra ( 'Task' ) . ' "' . $trashed_task ['title'] . '" ' . tra ( 'was moved into trash' );
				$msg_body = tra ( 'Task' ) . ': ';
				$msg_body .= '^[tiki-user_tasks.php?task_id=' . $trashed_task ['task_id'] . "|" . $trashed_task ['title'] . "]^\n\n";
				$msg_body .= tra ( 'trashed by' ) . ': ' . $this->user_id;
				$this->message->postMessage ( $msg_to, $msg_from, $msg_to, '', $msg_title, $msg_body, $trashed_task ['priority'] );
			} //End of foreach
		} //End of move_marked_to_trash
	} //End of method
	

	//------------------------------------------------------------------------------
	public function openmarkedAction() {
		foreach ( array_keys ( $this->task_ids ) as $task ) {
			$this->task->open_task ( $task, $this->user_id );
			$trashed_task = $this->task->get_task ( $this->user_id, $task );
			if ($trashed_task ['user'] == $this->user_id) {
				$msg_from = $this->user_id;
				$msg_to = $trashed_task ['creator'];
			} else {
				$msg_from = $this->user_id;
				$msg_to = $trashed_task ['user'];
			}
			
			/*
		$msg_title = tra('Task'). ' "' . $trashed_task['title'] . '" '.tra('open / in process');
		$msg_body  = '__'. tra('Task').':__';
		$msg_body .= '^[tiki-user_tasks.php?task_id=' . $trashed_task['task_id']. "|".$trashed_task['title']."]^\n\n";
		$msg_body .= tra('open / in process'). ': '.$this->user_id;
		if(	$userlib->user_has_permission($msg_from,'tiki_p_messages') and 
			$userlib->user_has_permission($msg_to,'tiki_p_messages'))
		{
			$this->message->postMessage($msg_to, $msg_from, $msg_to, '', $msg_title, $msg_body, $trashed_task['priority']);
		}
    */
			
			$msg_title = tra ( 'Task' ) . ' "' . $trashed_task ['title'] . '" ' . tra ( 'open / in process' );
			$msg_body = '__' . tra ( 'Task' ) . ':__';
			$msg_body .= '^[tiki-user_tasks.php?task_id=' . $trashed_task ['task_id'] . "|" . $trashed_task ['title'] . "]^\n\n";
			$msg_body .= tra ( 'open / in process' ) . ': ' . $this->user_id;
			$this->message->postMessage ( $msg_to, $msg_from, $msg_to, '', $msg_title, $msg_body, $trashed_task ['priority'] );
		
		} //End of foreach
	} //End of method
	

	//------------------------------------------------------------------------------
	public function completemarkedAction() {
		foreach ( array_keys ( $this->task_ids ) as $task ) {
			$this->task->mark_complete_task ( $task, $this->user_id );
			$trashed_task = $this->task->get_task ( $this->user_id, $task );
			if ($trashed_task ['user'] == $this->user_id) {
				$msg_from = $this->user_id;
				$msg_to = $trashed_task ['creator'];
			} else {
				$msg_from = $this->user_id;
				$msg_to = $trashed_task ['user'];
			}
			/*
		$msg_title = tra('Task'). ' "' . $trashed_task['title'] . '" '.tra('completed (100%)');
		$msg_body  = '__'. tra('Task').':__';
		$msg_body .= '^[tiki-user_tasks.php?task_id=' . $trashed_task['task_id']. "|".$trashed_task['title']."]^\n\n";
		$msg_body .= tra('completed (100%)'). ': '.$this->user_id;
		if(	$userlib->user_has_permission($msg_from,'tiki_p_messages') and 
			$userlib->user_has_permission($msg_to,'tiki_p_messages'))
		{
			$this->message->postMessage($msg_to, $msg_from, $msg_to, '', $msg_title, $msg_body, $trashed_task['priority']);
		}
    */
			
			$msg_title = tra ( 'Task' ) . ' "' . $trashed_task ['title'] . '" ' . tra ( 'completed (100%)' );
			$msg_body = '__' . tra ( 'Task' ) . ':__';
			$msg_body .= '^[tiki-user_tasks.php?task_id=' . $trashed_task ['task_id'] . "|" . $trashed_task ['title'] . "]^\n\n";
			$msg_body .= tra ( 'completed (100%)' ) . ': ' . $this->user_id;
			$this->message->postMessage ( $msg_to, $msg_from, $msg_to, '', $msg_title, $msg_body, $trashed_task ['priority'] );
		
		} //End of foreach
	} //End of method
	

	//------------------------------------------------------------------------------
	public function removemarkedfromtrashAction() {
		Ranchbe::checkPerm ( 'task-undotrash', $area_id );
		foreach ( array_keys ( $this->task_ids ) as $task ) {
			$this->task->unmark_task_as_trash ( $task, $this->user_id );
		}
	} //End of method
	

	//------------------------------------------------------------------------------
	public function waitingmarkedAction() {
		foreach ( array_keys ( $this->task_ids ) as $task ) {
			$this->task->waiting_task ( $task, $this->user_id );
		}
	} //End of method
	

	//------------------------------------------------------------------------------
	public function reloadAction() {
		if (isset ( $_REQUEST ['reload'] )) {
			if (isset ( $_REQUEST ['show_trash'] )) {
				$_SESSION ['show_trash'] = true;
			} else {
				$_SESSION ['show_trash'] = false;
			}
			if (isset ( $_REQUEST ['show_completed'] )) {
				$_SESSION ['show_completed'] = true;
			} else {
				$_SESSION ['show_completed'] = false;
			}
			if (isset ( $_REQUEST ['show_private'] )) {
				$_SESSION ['show_private'] = true;
			} else {
				$_SESSION ['show_private'] = false;
			}
			if (isset ( $_REQUEST ['show_received'] )) {
				$_SESSION ['show_received'] = true;
			} else {
				$_SESSION ['show_received'] = false;
			}
			if (isset ( $_REQUEST ['show_submitted'] )) {
				$_SESSION ['show_submitted'] = true;
			} else {
				$_SESSION ['show_submitted'] = false;
			}
			if (isset ( $_REQUEST ['show_shared'] )) {
				$_SESSION ['show_shared'] = true;
			} else {
				$_SESSION ['show_shared'] = false;
			}
			if (isset ( $_REQUEST ['show_admin'] )) {
				$_SESSION ['show_admin'] = true;
			} else {
				$_SESSION ['show_admin'] = false;
			}
		
		} //End of reload
	} //End of method
	

	//------------------------------------------------------------------------------
	public function updatetasksAction() {
		if (isset ( $_REQUEST ['update_tasks'] )) {
		}
	} //End of method
	

	//------------------------------------------------------------------------------
	public function emtytrashAction() {
		if (isset ( $_REQUEST ['emty_trash'] )) {
			$this->task->emty_trash ( $this->user_id );
		}
	} //End of method
	

	//------------------------------------------------------------------------------
	protected function _preview() {
		if ((isset ( $_REQUEST ['save'] )) || (isset ( $_REQUEST ['preview'] ))) {
			//$dc = $tikilib->get_date_converter($this->user_id);
			$now = date ( "U" );
			$auto_accepted_status = true;
			$save = array ();
			$save_head = array ();
			$msg_body = '';
			$msg_title = '';
			
			if ($info ['task_id'] == 0) {
				$msg_changes_head = '__' . tra ( "Task entries:" ) . "__\n";
			} else {
				$msg_changes_head = '__' . tra ( "Changes:" ) . "__\n";
			}
			$msg_changes = '';
			
			if (isset ( $_REQUEST ['use_start_date'] ) and isset ( $_REQUEST ['start_Hour'] ) and isset ( $_REQUEST ['start_Minute'] ) and isset ( $_REQUEST ['start_Month'] ) and isset ( $_REQUEST ['start_Day'] ) and isset ( $_REQUEST ['start_Year'] )) {
				
				/*
				$start_date = $dc->getServerDateFromDisplayDate(mktime(	$_REQUEST['start_Hour'], 
																		$_REQUEST['start_Minute'], 
																		0, 
																		$_REQUEST['start_Month'], 
																		$_REQUEST['start_Day'], 
																		$_REQUEST['start_Year']));
				  */
				
				$start_date = Rb_Date::getTSFromDate ( $_REQUEST ['start_Hour'] );
			} else
				$start_date = null;
			
			if (isset ( $_REQUEST ['use_end_date'] ) and isset ( $_REQUEST ['end_Hour'] ) and isset ( $_REQUEST ['end_Minute'] ) and isset ( $_REQUEST ['end_Month'] ) and isset ( $_REQUEST ['end_Day'] ) and isset ( $_REQUEST ['end_Year'] )) {
				
				/*
			$end_date = $dc->getServerDateFromDisplayDate(mktime(	$_REQUEST['end_Hour'], 
																	$_REQUEST['end_Minute'], 
																	0, 
																	$_REQUEST['end_Month'], 
																	$_REQUEST['end_Day'], 
																	$_REQUEST['end_Year']));
*/
				
				$end_date = Rb_Date::getTSFromDate ( $_REQUEST ['end_Hour'] );
			} else
				$start_date = null;
			
			if (isset ( $_REQUEST ['task_user'] )) {
				$task_user = $_REQUEST ['task_user'];
				if ($info ['creator'] == $this->user_id and $info ['user'] != $_REQUEST ['task_user']) {
					$save_head ['user'] = $_REQUEST ['task_user'];
					$msg_changes .= tra ( 'Task user' ) . ': ' . $info ['user'] . ' --> ' . $save_head ['user'] . "\n";
				}
			} else {
				$task_user = $this->user_id;
			}
			if (isset ( $_REQUEST ['public_for_group'] )) {
				$public_for_group = $_REQUEST ['public_for_group'];
				if ($info ['creator'] == $this->user_id and $info ['public_for_group'] != $_REQUEST ['public_for_group']) {
					$save_head ['public_for_group'] = $_REQUEST ['public_for_group'];
					$msg_changes .= tra ( 'Public for group' ) . ': ' . $info ['public_for_group'] . ' --> ' . $save_head ['public_for_group'] . "\n";
				}
			} else {
				$public_for_group = null;
				if ($info ['creator'] == $this->user_id and $info ['public_for_group'] != null) {
					$save_head ['public_for_group'] = null;
					$msg_changes .= tra ( 'Public for group' ) . ': ' . $info ['public_for_group'] . ' --> ' . $save_head ['public_for_group'] . "\n";
				}
			}
			
			if (isset ( $_REQUEST ['rights_by_creator'] )) {
				$rights_by_creator = $_REQUEST ['rights_by_creator'] = 'y';
				if ($info ['creator'] == $this->user_id and $info ['rights_by_creator'] != $_REQUEST ['rights_by_creator']) {
					$save_head ['rights_by_creator'] = $_REQUEST ['rights_by_creator'];
					$msg_changes .= tra ( 'Rights by creator' ) . ': ' . $info ['rights_by_creator'] . ' --> ' . $save_head ['rights_by_creator'] . "\n";
				}
			} else {
				$rights_by_creator = null;
				if ($info ['creator'] == $this->user_id and $info ['rights_by_creator'] != null) {
					$save_head ['rights_by_creator'] = null;
					$msg_changes .= tra ( 'Rights by creator' ) . ': ' . $info ['rights_by_creator'] . ' --> ' . $save_head ['rights_by_creator'] . "\n";
				}
			}
			if (isset ( $_REQUEST ['public_for_group'] )) {
				$public_for_group = $_REQUEST ['public_for_group'];
			} else {
				$public_for_group = null;
			}
			if (isset ( $_REQUEST ['title'] ) and $info ['title'] != $_REQUEST ['title']) {
				$save ['title'] = $_REQUEST ['title'];
				$msg_changes .= tra ( 'Title' ) . ': ' . $info ['title'] . ' --> ' . $save ['title'] . "\n";
			}
			
			if (isset ( $_REQUEST ['description'] ) and $info ['description'] != $_REQUEST ['description']) {
				$save ['description'] = $_REQUEST ['description'];
				$msg_changes .= tra ( 'Description' ) . ': ' . $info ['description'] . ' --> ' . $save ['description'] . "\n";
			}
			
			if (isset ( $_REQUEST ['use_start_date'] ) and $info ['start'] != $start_date) {
				$save ['start'] = $start_date;
				$msg_changes .= tra ( 'Start' ) . ": ";
				if ($info ['start'] != null) {
					$msg_changes .= date ( "l dS of F Y H:i:s", $info ['start'] ) . ' --> ';
				}
				$msg_changes .= date ( "l dS of F Y H:i:s", $save ['start'] ) . "\n";
			}
			if (isset ( $_REQUEST ['use_end_date'] ) and $info ['end'] != $end_date) {
				$save ['end'] = $end_date;
				$msg_changes .= tra ( 'END' ) . ": ";
				if ($info ['end'] != null) {
					$msg_changes .= date ( "l dS of F Y H:i:s", $info ['end'] ) . ' --> ';
				}
				$msg_changes .= date ( "l dS of F Y H:i:s", $save ['end'] ) . "\n";
			}
			if (isset ( $_REQUEST ['priority'] ) and $info ['priority'] != $_REQUEST ['priority']) {
				$save ['priority'] = $_REQUEST ['priority'];
				$msg_changes .= tra ( 'Priority' ) . ': ' . $info ['priority'] . ' --> ' . $save ['priority'] . "\n";
			}
			if (isset ( $_REQUEST ['status'] )) {
				if ($_REQUEST ['status'] == 'w')
					$save ['status'] = null;
				else
					$save ['status'] = $_REQUEST ['status'];
				if ($info ['status'] != $save ['status']) {
					$msg_changes .= tra ( 'Status' ) . ': ' . $info ['status'] . ' --> ' . $save ['status'] . "\n";
					if ($save ['status'] == 'c') {
						$_REQUEST ['percentage'] = 100;
						$save ['completed'] = $now;
					}
				} else {
					unset ( $save ['status'] );
				}
			}
			if (isset ( $_REQUEST ['percentage'] )) {
				if ($_REQUEST ['percentage'] == 'w')
					$save ['percentage'] = null;
				else
					$save ['percentage'] = $_REQUEST ['percentage'];
				if ($info ['percentage'] != $save ['percentage']) {
					$msg_changes .= tra ( 'Percentage' ) . ': ' . $info ['percentage'] . ' --> ' . $save ['percentage'] . "\n";
				} else {
					unset ( $save ['percentage'] );
				}
			}
			
			if ($info ['task_id'] > 0 and $info ['creator'] != $info ['user']) {
				if (isset ( $_REQUEST ['task_accept'] )) {
					$auto_accepted_status = false;
					if ($this->user_id == $info ['creator'] and $info ['percentage'] != 'y') {
						$save ['accepted_creator'] = 'y';
						$msg_changes .= tra ( 'Task accepted by creator' ) . "\n";
					}
					if ($this->user_id == $info ['user'] and $info ['percentage'] != 'y') {
						$save ['accepted_user'] = 'y';
						$msg_changes .= tra ( 'Task accepted by task user' ) . "\n";
					}
				}
				
				if (isset ( $_REQUEST ['task_not_accept'] )) {
					$auto_accepted_status = false;
					if ($this->user_id == $info ['creator'] and $info ['percentage'] != 'n') {
						$save ['accepted_creator'] = 'n';
						$msg_changes .= tra ( 'Task NOT accepted by creator' ) . "\n";
					}
					if ($this->user_id == $info ['user'] and $info ['percentage'] != 'n') {
						$save ['accepted_user'] = 'n';
						$msg_changes .= tra ( 'Task NOT accepted by task user' ) . "\n";
					}
				}
			}
			
			if (isset ( $_REQUEST ['remove_from_trash'] )) {
				$this->task->unmark_task_as_trash ( $info ['task_id'], $this->user_id, $admin_mode );
			}
			
			if (isset ( $_REQUEST ['move_into_trash'] )) {
				$this->task->mark_task_as_trash ( $info ['task_id'], $this->user_id, $admin_mode );
			}
			
			if (isset ( $_REQUEST ['task_send_changes_message'] )) {
				$send_message = true;
			} else {
				$send_message = false;
			}
			
			if (isset ( $_REQUEST ['preview'] )) {
				$info = $save;
				$info ['task_id'] = $this->task_id;
				$info ['task_version'] = $_REQUEST ['task_version'];
				$info ['last_version'] = $_REQUEST ['last_version'];
				$info ['user'] = $_REQUEST ['task_user'];
				$info ['creator'] = $_REQUEST ['creator'];
				;
				$info ['public_for_group'] = $_REQUEST ['public_for_group'];
				$info ['rights_by_creator'] = (isset ( $_REQUEST ['rights_by_creator'] )) ? $_REQUEST ['rights_by_creator'] : NULL;
				//  $task['deleted'] = (isset($_REQUEST['deleted']))? $_REQUEST['deleted'] :NULL;
				$info ['created'] = (isset ( $_REQUEST ['created'] )) ? $_REQUEST ['created'] : date ( "U" );
				/*--history --*/
				//  $task['belongs_to'] = (isset($_REQUEST['belongs_to']))? :;
				//  $task['lasteditor'] = (isset($_REQUEST['lasteditor']))? :;
				//  $task['lastchanges'] = date("U");
				/*--*/
				$info ['percentage_null'] = ($_REQUEST ['percentage'] == 'w');
				
				if ((isset ( $_REQUEST ['status'] )) && ($_REQUEST ['status'] != 'w')) {
					$info ['status'] = $_REQUEST ['status'];
				} else {
					$info ['status'] = null;
				}
				$info ['info'] = (isset ( $_REQUEST ['task_info_message'] )) ? $_REQUEST ['task_info_message'] : '';
				$info ['parsed'] = (isset ( $info ['description'] )) ? $info ['description'] : '';
			}
		} //End of save or preview
	} //End of method
	

	//------------------------------------------------------------------------------
	protected function save() {
		if (isset ( $_REQUEST ['save'] )) {
			if (isset ( $_REQUEST ['task_info_message'] ) and strlen ( $_REQUEST ['task_info_message'] ) > 1) {
				$task_info_message = "\n__" . tra ( "Info message" ) . ":__\n";
				$task_info_message .= '^' . $_REQUEST ['task_info_message'] . "^\n\n";
			} else {
				$task_info_message = '';
			}
			
			if (isset ( $save ['title'] ) && strlen ( $save ['title'] ) < 3) {
				$smarty->assign ( 'msg', tra ( "The task title must have at least 3 characters" ) );
				$smarty->display ( "error.tpl" );
				die ();
			}
			
			if ($info ['task_id'] == 0) {
				//new task
				if ($_REQUEST ['task_user'] != $this->user_id) {
					$save ['accepted_creator'] = 'y';
					$msg_from = $this->user_id;
					$msg_to = $_REQUEST ['task_user'];
					$msg_title = tra ( "NEW Task" ) . ': "' . $save ['title'] . '"';
					$send_message = true;
				} else {
					$send_message = false;
				}
				
				/*
			if(	($_REQUEST['task_user'] == $this->user_id) or
				($userlib->user_has_permission($_REQUEST['task_user'],'tiki_p_tasks_receive') and 
				 $userlib->user_has_permission($this->user_id,'tiki_p_tasks_send')))
			{
				$task_id = $this->task->new_task($_REQUEST['task_user'], $this->user_id, $public_for_group, $rights_by_creator, $now, $save);
			}
      else{
        unset($this->task_id);
        $smarty->assign('msg', tra("Sorry you are not allowed to send tasks to other users, or the user is not allowed to receive tasks!"));
        $smarty->display("error.tpl");
        die;
  		}
*/
				
				$task_id = $this->task->new_task ( $_REQUEST ['task_user'], $this->user_id, $public_for_group, $rights_by_creator, $now, $save );
			
			} else {
				if ($auto_accepted_status) {
					if ($info ['user'] == $this->user_id) {
						$msg_to = $info ['creator'];
						$save ['accepted_user'] = 'y';
						$save ['accepted_creator'] = null;
					} else if ($info ['creator'] == $this->user_id) {
						$msg_to = $info ['user'];
						$save ['accepted_user'] = null;
						$save ['accepted_creator'] = 'y';
					} else {
						$msg_to = $info ['user'];
						$save ['accepted_user'] = null;
						$save ['accepted_creator'] = null;
					}
				}
				$msg_from = $this->user_id;
				$this->task->update_task ( $info ['task_id'], $this->user_id, $save, $save_head, $admin_mode );
				$task_id = $info ['task_id'];
				$msg_title = tra ( "Changes on Task" ) . ': "' . $info ['title'] . '" by ' . $this->user_id;
			}
			$info = $this->task->get_task ( $this->user_id, $task_id, null, $admin_mode );
			
			/*
	//send email to task user	
	if ((isset($_REQUEST['send_email_newtask'])) && $send_message && ($_REQUEST['task_user'] != $this->user_id)){
		$email = $userlib->get_user_email($msg_to);
 		$mail = new TikiMail($msg_to);
 		$mail->setSubject($msg_title);
		$mail_data =  tra("You received a new task")."\n\n".$info['title']."\n".tra("from")." :$this->user_id\n";
		$mail_data .= tra("The priority is").": ";

		switch($info['priority']){
			case 1: $mail_data .= tra("very low");
				break;
			case 2: $mail_data .= tra("low");
				break;
			case 3: $mail_data .= tra("normal");
				break;
			case 4: $mail_data .= tra("high");
				break;
			case 5: $mail_data .= tra("very high");
				break;
		}
		$mail_data .= ".\n\n";
		if ($info['start'] !== NULL){
			$mail_data .= tra("You've to start your work at least on").": ".date("j.M.Y - H:i",$info['end'])."\n";
		}
		if ($info['end'] !== NULL){
			$mail_data .= tra("You've to finish your work on").": ".date("j.M.Y - H:i",$info['end'])."\n";
		}

		$mail_data .= "\n".tra("Login and click the link below")."\n";
		$mail_data .= "http://".$_REQUEST['HTTP_HOST'].$_REQUEST['REQUEST_URI']."?tiki_view_mode=view&task_id=".$task_id."\n\n";
		$mail_data .= tra("Please read the task and work on it!");
   		$mail->setText($mail_data);
   		$mail->send(array($email));
	} //End of email user
*/
			
			if (! isset ( $info ['user'] )) {
				unset ( $this->task_id );
				$smarty->assign ( 'msg', tra ( "Sorry, there was an error while trying to write data into the database" ) );
				$smarty->display ( "error.tpl" );
				die ();
			}
			
			/*
	if(	$send_message and
		$userlib->user_has_permission($msg_from,'tiki_p_messages') and 
		$userlib->user_has_permission($msg_to,'tiki_p_messages'))
	{
*/
			
			if ($send_message) {
				/*
		$msg_body  = "__" . tra('Task') . ":__";
		$msg_body .= '^[tiki-user_tasks.php?task_id=' . $info['task_id']. "|".$info['title']."]^\n";
		
		$msg_body .= $task_info_message . $msg_changes_head . '^' . $msg_changes . '^';
		$this->message->postMessage($msg_to,	//user
					$msg_from,	//from
					$msg_to,	//to
					'',		//cc
					$msg_title,	//title
					$msg_body,	//body
					$info['priority']);//priority
*/
				$msg_body = tra ( 'Task' ) . ":";
				$msg_body .= '<a href="tiki-user_tasks.php?task_id=' . $info ['task_id'] . '">' . $info ['title'] . '</a>';
				$msg_body .= '<br />';
				$msg_body .= $task_info_message . '<br />' . $msg_changes_head . '<br />' . $msg_changes;
				$this->message->postMessage ( $msg_to, //user
												$msg_from, //from
												$msg_to, //to
												'', //cc
												$msg_title, //title
												$msg_body, //body
												$info ['priority'] ); //priority
			

			} //End of send message
			

			if ($show_admin)
				$user_for_group_list = $info ['creator'];
			
			$show_form = false;
			$smarty->assign ( 'show_form', $show_form );
			$show_view = true;
			$smarty->assign ( 'show_view', $show_view );
		} //End of save
	} //End of method

} //End of class
	


