<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once ('controllers/abstract.php');
class Galaxia_ProcessesController extends controllers_abstract {
	protected $page_id = 'galaxia_processes'; //(string)
	protected $ifSuccessForward = array ('module' => 'galaxia', 'controller' => 'processes', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'galaxia', 'controller' => 'processes', 'action' => 'index' );
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->dbGalaxia = & Ranchbe::getDb ();
//		include_once (GALAXIA_LIBRARY . '/src/API/Process.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/Instance.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/BaseActivity.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Activity.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/End.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Join.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Split.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Standalone.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Start.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/SwitchActivity.php');
//		include_once (GALAXIA_LIBRARY . '/src/Observers/Galaxia_Observer_Logger.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/ProcessManager.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Instance.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Role.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Activity.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/GraphViz.php');
		
		//Galaxia engine
		//require_once('Galaxia/Galaxia_Manager_Process.php'); ::>>
		
		/// $roleManager is the object that will be used to manipulate roles.
		$this->roleManager = new Galaxia_Manager_Role ( $this->dbGalaxia );
		/// $activityManager is the object that will be used to manipulate activities.
		$this->activityManager = new Galaxia_Manager_Activity ( $this->dbGalaxia );
		/// $processManager is the object that will be used to manipulate processes.
		$this->processManager = new Galaxia_Manager_Process ( $this->dbGalaxia );
		/// $instanceManager is the object that will be used to manipulate instances.
		$this->instanceManager = new Galaxia_Manager_Instance ( $this->dbGalaxia );
		
		if (GALAXIA_LOGFILE && GALAXIA_LOGFILE) {
			//include_once (GALAXIA_LIBRARY . '/src/Observers/Galaxia_Observer_Logger.php');
			$logger = new Galaxia_Observer_Logger ( GALAXIA_LOGFILE );
			$this->processManager->attach_all ( $logger );
			$this->activityManager->attach_all ( $logger );
			$this->roleManager->attach_all ( $logger );
			if(!is_file(GALAXIA_LOGFILE)){
			}
		}
		
		$this->view->is_active_help = tra ( 'indicates if the process is active. Invalid processes cant be active' );
		$this->pid = ( int ) $this->getRequest ()->getParam ( 'pid' );
		if ($this->pid) {
			$this->proc_info = $this->processManager->get_process ( $this->pid );
		} else {
			$this->proc_info = array (
								'name' => '', 
								'description' => '', 
								'version' => '1.0', 
								'isActive' => 'n', 
								'pId' => 0 );
		}
		$this->view->proc_info = $this->proc_info;
		$this->view->assign ( 'info', $this->proc_info ); //???
		$this->view->assign ( 'pid', $this->pid );
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
		
		/*
	  $where = '';
	  $wheres = array();
	  
	  if (isset($_REQUEST['filter'])) {
	  	if ($_REQUEST['filter_name']) {
	  		$wheres[] = " name='" . $_REQUEST['filter_name'] . "'";
	  	}
	  
	  	if ($_REQUEST['filter_active']) {
	  		$wheres[] = " isActive='" . $_REQUEST['filter_active'] . "'";
	  	}
	  
	  	$where = implode('and', $wheres);
	  }
	  
	  if (isset($_REQUEST['where'])) {
	  	$where = $_REQUEST['where'];
	  }
	  
	  if (!isset($_REQUEST["sort_mode"])) {
	  	$sort_mode = 'lastModif_desc';
	  } else {
	  	$sort_mode = $_REQUEST["sort_mode"];
	  }
	
	  if (!isset($_REQUEST["offset"])) {
	  	$offset = 0;
	  } else {
	  	$offset = $_REQUEST["offset"];
	  }
	  
	  $this->view->assign_by_ref('offset', $offset);
	  
	  if (isset($_REQUEST['find'])) {
	  	$find = $_REQUEST['find'];
	  } else {
	  	$find = '';
	  }
	
	  $this->view->assign('find', $find);
	  $this->view->assign('where', $where);
	  $this->view->assign_by_ref('sort_mode', $sort_mode);
	
	  //Veiller a ce que maxRecords soit bien d�finis
	  if(!$maxRecords) $maxRecords = 50;
	  */
			
			/*
	  $search = new Rb_Workflow_Galaxiasearch();
	  $filter = new RbView_helper_galaxiaProcessFilter( $this->getRequest(), $search, $this->page_id);
	  $filter->setUrl('./galaxia/processes/index');
	  $filter->setDefault('sort_field','lastModif');
	  $filter->setDefault('sort_order','DESC');
	  $filter->finish();
	  */
		
		$offset = 0;
		$maxRecords = 50000;
		$sort_mode = 'lastModif_desc';
		$find = '';
		$where = '';
		
		/*
		  $this->view->assign('cant', $items['cant']);
		  
		  $cant_pages = ceil($items["cant"] / $maxRecords);
		  $this->view->assign_by_ref('cant_pages', $cant_pages);
		  $this->view->assign('actual_page', 1 + ($offset / $maxRecords));
		  
		  if ($items["cant"] > ($offset + $maxRecords)) {
		  	$this->view->assign('next_offset', $offset + $maxRecords);
		  } else {
		  	$this->view->assign('next_offset', -1);
		  }
		  
		  if ($offset > 0) {
		  	$this->view->assign('prev_offset', $offset - $maxRecords);
		  } else {
		  	$this->view->assign('prev_offset', -1);
		  }
		  */
		
		if ($this->pid) {
			Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
			$valid = $this->activityManager->validate_process_activities ( $this->pid );
			$errors = array ();
			if (! $valid) {
				$this->processManager->deactivate_process ( $this->pid );
				$errors = $this->activityManager->get_error ();
			}
			$this->view->assign ( 'errors', $errors );
		}
		
		if (Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), false )) {
			$items = $this->processManager->list_processes ( $offset, $maxRecords, $sort_mode, $find, $where );
			$this->view->items = $items ['data'];
		}
		
		// Display the template
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'workflowTab' )->activate ();
		$this->error_stack->checkErrors ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function deleteAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$process = $this->getRequest ()->getParam ( 'process' );
		if (is_array ( $process )) {
			foreach ( array_keys ( $process ) as $pid ) {
				$this->processManager->remove_process ( $pid );
			}
		}
		return $this->_forward('index');
	} //End of method
	

	//---------------------------------------------------------------------
	public function newminorAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$this->processManager->new_process_version ( $this->pid, true );
		return $this->_forward('index');
	} //End of method
	

	//---------------------------------------------------------------------
	public function newmajorAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$this->processManager->new_process_version ( $this->pid, false );
		return $this->_forward('index');
	} //End of method
	

	//---------------------------------------------------------------------
	public function saveAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		
		$name = $this->getRequest ()->getParam ( 'name' );
		$name = str_replace ( array (' ', '/', '\\', '.' ), '', $name );
		$vars = array ('description' => $this->getRequest ()->getParam ( 'description' ), 
						'version' => $this->getRequest ()->getParam ( 'version' ), 
						'isActive' => $this->getRequest ()->getParam ( 'isActive' ) );
		
		if (! empty ( $name )) {
			$vars ['name'] = $name;
		} else {
			$this->error_stack->push ( Rb_Error::ERROR, array (), tra ( 'name is empty' ) );
			return $this->_forward ( 'index' );
		}
		
		if ($this->processManager->process_name_exists ( $vars ['name'], $vars ['version'] ) && $this->pid == 0) {
			$this->error_stack->push ( Rb_Error::ERROR, array (), tra ( 'Process already exists' ) );
			return $this->_forward ( 'index' );
		}
		
		if ($vars ['isActive'] == 'on') {
			$vars ['isActive'] = 'y';
		}
		
		$pid = $this->processManager->replace_process ( $this->pid, $vars );
		$valid = $this->activityManager->validate_process_activities ( $pid );
		if (! $valid) {
			$this->processManager->deactivate_process ( $pid );
		}
		
		$info = array ('name' => '', 
						'description' => '', 
						'version' => '1.0', 
						'isActive' => 'n', 
						'pId' => 0 );
		$this->view->assign ( 'info', $info );
		return $this->_forward('index');
	} //End of method
	

	//---------------------------------------------------------------------
	public function exportAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$this->view->layout ()->disableLayout ();
		$in_charset = iconv_get_encoding (); //Get current encoding option
		$filename = 'processExport_' . $this->proc_info ['normalized_name'] . '.xml';
		$rawdata = $this->processManager->serialize_process ( $this->pid );
		$this->view->xmlcontent = iconv ( $in_charset ['output_encoding'], "ISO-8859-1//TRANSLIT", $rawdata );
		$this->getResponse ()
				->setHeader ( 'Content-Type', 'text/xml; charset=utf-8' )
				->setHeader ( 'Content-Transfer-Encoding', Binary )
				->setHeader ( 'Content-Disposition', "attachment; filename=$filename" )
				->setHeader ( 'Pragma', 'no-cache' )
				->setHeader ( 'Cache-Control', 'must-revalidate, post-check=0, pre-check=0, public' )
				->setHeader ( 'Expires', 0 );
		//$this->render ( 'galaxia/xml/exportProcess', null, true );
		//$data = $this->view->fetch ( 'galaxia/xml/exportProcess.xml' );
		//header ( "Content-disposition: attachment; filename=$filename" );
		//header ( 'Content-type: text/xml; charset=utf-8' );
		//header ( "Content-Transfer-Encoding: binary\n" ); // Surtout ne pas enlever le \n
		//header ( "Content-Length: " . mb_strlen ( $data ) );
		//header ( "Pragma: no-cache" );
		//header ( "Cache-Control: must-revalidate, post-check=0, pre-check=0, public" );
		//header ( "Expires: 0" );
		//echo $data;
		//die;
	} //End of method
	

	//---------------------------------------------------------------------
	public function importAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		//Check here for an uploaded process
		if (isset ( $_FILES ['userfile1'] ) && is_uploaded_file ( $_FILES ['userfile1'] ['tmp_name'] )) {
			$fp = fopen ( $_FILES ['userfile1'] ['tmp_name'], "rb" );
			
			$data = '';
			$fhash = '';
			
			while ( ! feof ( $fp ) ) {
				$rawdata .= fread ( $fp, 8192 * 16 );
			}
			
			$data = iconv ( 'ISO-8859-1', 'ISO-8859-1//TRANSLIT', $rawdata ); //Add By RanchBE To clean input

			fclose ( $fp );
			$size = $_FILES ['userfile1'] ['size'];
			$name = $_FILES ['userfile1'] ['name'];
			$type = $_FILES ['userfile1'] ['type'];
			
			$process_data = $this->processManager->unserialize_process ( $data, 'ISO-8859-1' ); //second parameter add by RanchBE
			

			if ($process_data) {
				if ($this->processManager->process_name_exists ( $process_data ['name'], $process_data ['version'] )) {
					$this->error_stack->push ( Rb_Error::ERROR, array (), tra ( 'The process name already exists' ) );
					return $this->_forward ( 'index' );
				} else {
					$this->processManager->import_process ( $process_data );
				}
			}
		}
		return $this->_forward ( 'index' );
	} //End of method


} //End of class
