<?php

// $Header: /cvsroot/ranchbe/ranchbe0.7/web/galaxia_view_workitem.php,v 1.1 2009/06/05 17:06:37 ranchbe Exp $

// Copyright (c) 2002-2005, Luis Argerich, Garland Foster, Eduardo Polidor, et. al.
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.

require_once('conf/galaxia_setup.php');
include_once('lib/Galaxia/ProcessMonitor.php');

if (!isset($_REQUEST['itemId'])) {
	$smarty->assign('msg', tra("No item indicated"));

	$smarty->display("error.tpl");
	die;
}

$wi = $processMonitor->monitor_get_workitem($_REQUEST['itemId']);
$smarty->assign_by_ref('wi', $wi);

$smarty->assign('stats', $processMonitor->monitor_stats());

$sameurl_elements = array(
	'offset',
	'sort_mode',
	'where',
	'find',
	'itemId'
);

//Display manager
$template = 'galaxia/view_workitem.tpl';
include('galaxia.php');

?>
