<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

/*
// Load configuration of the Galaxia Workflow Engine
//require_once('conf/galaxia_setup.php');
//include_once(GALAXIA_LIBRARY.'/src/ProcessManager/ProcessManager.php');
//include_once(GALAXIA_LIBRARY.'/src/ProcessManager/InstanceManager.php');
//include_once(GALAXIA_LIBRARY.'/src/ProcessManager/RoleManager.php');
//include_once(GALAXIA_LIBRARY.'/src/ProcessManager/ActivityManager.php');
//include_once(GALAXIA_LIBRARY.'/src/ProcessManager/GraphViz.php');

global $dbGalaxia;
/// $roleManager is the object that will be used to manipulate roles.
$roleManager = new RoleManager($dbGalaxia);
/// $activityManager is the object that will be used to manipulate activities.
$activityManager = new ActivityManager($dbGalaxia);
/// $processManager is the object that will be used to manipulate processes.
$processManager = new ProcessManager($dbGalaxia);
/// $instanceManager is the object that will be used to manipulate instances.
$instanceManager = new InstanceManager($dbGalaxia);

if (defined('GALAXIA_LOGFILE') && GALAXIA_LOGFILE) {
    //include_once (GALAXIA_LIBRARY.'/src/Observers/Galaxia_Observer_Logger.php');
    $logger = new Galaxia_Observer_Logger(GALAXIA_LOGFILE);
    $processManager->attach_all($logger);
    $activityManager->attach_all($logger);
    $roleManager->attach_all($logger);
}
*/

require_once ('controllers/abstract.php');
class Galaxia_InstanceController extends controllers_abstract {
	protected $page_id = 'galaxia_processes'; //(string)
	protected $ifSuccessForward = array ('module' => 'galaxia', 'controller' => 'processes', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'galaxia', 'controller' => 'processes', 'action' => 'index' );
	protected $processMonitor = false;
	protected $processManager = false;
	protected $activityManager = false;
	protected $roleManager = false;
	protected $instanceManager = false;
	protected $instance = false;
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->dbGalaxia = & Ranchbe::getDb ();
		if (GALAXIA_LOGFILE && GALAXIA_LOGFILE) {
			$logger = new Galaxia_Observer_Logger ( GALAXIA_LOGFILE );
			$this->processManager->attach_all ( $logger );
			$this->activityManager->attach_all ( $logger );
			$this->roleManager->attach_all ( $logger );
		}
	} //End of method
	

	//---------------------------------------------------------------------
	/* Get activities of instance
	 * 
	 */
	public function getactivitiesAction() {
		$this->view->iid = ( int ) $this->getRequest ()->getParam ( 'iid' );
		$iid = & $this->view->iid;
		$this->view->aid = ( int ) $this->getRequest ()->getParam ( 'aid' );
		$aid = & $this->view->aid;
		$owner = $this->getRequest ()->getParam ( 'owner' );
		$save = $this->getRequest ()->getParam ( 'save' );
		$unsetprop = $this->getRequest ()->getParam ( 'unsetprop' );
		$__removecomment = $this->getRequest ()->getParam ( '__removecomment' );
		$__cid = $this->getRequest ()->getParam ( '__cid' );
		$__title = $this->getRequest ()->getParam ( '__title' );
		$__post = $this->getRequest ()->getParam ( '__post' );
		$user = Rb_User::getCurrentUser ()->getName ();
		if ($save) {
			$this->instanceManager->set_instance_user ( $iid, $aid, $owner );
		}
		$this->view->ins_info = $this->instanceManager->get_instance ( $iid );
		$this->view->proc_info = $this->processManager->get_instance ( $this->view->ins_info ['pId'] );
		$this->view->users = Ranchbe::getAuthAdapter ()->getUsers ();
		if ($this->view->users === false)
			return false;
		if ($unsetprop) {
			unset ( $props [$unsetprop] );
			$this->instanceManager->set_instance_properties ( $iid, $props );
		}
		$this->view->acts = $this->instanceManager->get_instance_activities ( $iid, $aid );
		$acts = & $this->view->acts;
		$this->instance->getInstance ( $iid );
		// Process comments
		if ($__removecomment) {
			//Ranchbe::checkPerm ( 'g-admin-instance', $g_area_id );
			$this->view->__comment = $this->instance->get_instance_comment ( $__removecomment );
			if ($this->view->__comment ['user'] == $user) {
				$this->instance->remove_instance_comment ( $__removecomment );
			}
		}
		//$smarty->assign_by_ref ( '__comments', $__comments );
		if (! $__cid) {
			$__cid = 0;
		}
		if ($__post) {
			//Ranchbe::checkPerm ( 'g-admin-instance', $g_area_id );
			$this->instance->replace_instance_comment ( $__cid, $aid, '', $user, $__title, $this->view->__comment );
		}
		$__comments = $this->instance->get_instance_comments ( $aid );
		$this->view->__comments = $__comments;
	} //End of method
	

	//---------------------------------------------------------------------
	/* Get instances
	 * 
	 */
	public function updateAction() {
		//Ranchbe::checkPerm('g-monitor-instances' , $g_area_id);
		$update_status = $this->getRequest ()->getParam ( 'update_status' );
		$update_actstatus = $this->getRequest ()->getParam ( 'update_actstatus' );
		foreach ( $update_status as $key => $val ) {
			$this->processMonitor->update_instance_status ( $key, $val );
		}
		foreach ( $update_actstatus as $key => $val ) {
			$parts = explode ( ':', $val );
			$this->processMonitor->update_instance_activity_status ( $key, $parts [1], $parts [0] );
		}
	} //End of method
	

	//---------------------------------------------------------------------
	/* Delete instance
	 * 
	 */
	public function deleteAction() {
		//Ranchbe::checkPerm('g-monitor-instances' , $g_area_id);
		$inst = $this->getRequest ()->getParam ( 'inst' );
		foreach ( array_keys ( $inst ) as $ins ) {
			$this->processMonitor->remove_instance ( $ins );
		}
	} //End of method
	

	//---------------------------------------------------------------------
	/* Delete aborted
	 * 
	 */
	public function deleteabortedAction() {
		//Ranchbe::checkPerm('g-monitor-instances' , $g_area_id);
		$this->processMonitor->remove_aborted ();
	} //End of method
	

	//---------------------------------------------------------------------
	/* Remove all
	 * 
	 */
	public function deleteallAction() {
		//Ranchbe::checkPerm('g-monitor-instances' , $g_area_id);
		$filter_process = $this->getRequest ()->getParam ( 'filter_process' );
		$this->processMonitor->remove_all ( $filter_process );
	} //End of method
	

	//---------------------------------------------------------------------
	/* Send instance
	 * 
	 */
	public function sendinstanceAction() {
		$sendInstance = $this->getRequest ()->getParam ( 'sendInstance' );
		$activityId = $this->getRequest ()->getParam ( 'activityId' );
		//Ranchbe::checkPerm('g-monitor-instances' , $g_area_id);
		//activityId indicates the activity where the instance was
		//and we have to send it to some activity to be determined
		//include_once ('Galaxia/src/API/Galaxia_Instance.php');
		$instance = new Galaxia_Instance ( Ranchbe::getDb () );
		$instance->getInstance ( $sendInstance );
		// Do not add a workitem since the instance must be already completed!
		$instance->complete ( $activityId, true, false );
	} //End of method
	

	//---------------------------------------------------------------------
	/* Send instance
	 * 
	 */
	public function indexAction() {
		$maxRecords = 50;
		if (isset ( $_REQUEST ['filter_status'] ) && $_REQUEST ['filter_status'])
			$wheres [] = "gi.status='" . $_REQUEST ['filter_status'] . "'";
		
		if (isset ( $_REQUEST ['filter_process'] ) && $_REQUEST ['filter_process'])
			$wheres [] = "gi.pId=" . $_REQUEST ['filter_process'] . "";
		
		if (isset ( $_REQUEST ['filter_instanceName'] ) && $_REQUEST ['filter_instanceName'])
			$wheres [] = "gi.name='" . $_REQUEST ['filter_instanceName'] . "'";
		
		if (isset ( $_REQUEST ['filter_owner'] ) && $_REQUEST ['filter_owner'])
			$wheres [] = "owner='" . $_REQUEST ['filter_owner'] . "'";
		
		$where = implode ( ' and ', $wheres );
		
		if (! isset ( $_REQUEST ["sort_mode"] )) {
			$sort_mode = 'instanceId_asc';
		} else {
			$sort_mode = $_REQUEST ["sort_mode"];
		}
		
		if (! isset ( $_REQUEST ["offset"] )) {
			$offset = 0;
		} else {
			$offset = $_REQUEST ["offset"];
		}
		
		$this->view->offset = $offset;
		
		if (isset ( $_REQUEST ["find"] )) {
			$find = $_REQUEST ["find"];
		} else {
			$find = '';
		}
		
		$this->view->assign ( 'find', $find );
		$this->view->assign ( 'where', $where );
		$this->view->sort_mode = $sort_mode;
		$items = $this->processMonitor->monitor_list_instances ( $offset, $maxRecords, $sort_mode, $find, $where );
		$this->view->assign ( 'cant', $items ['cant'] );
		
		$cant_pages = ceil ( $items ["cant"] / $maxRecords );
		$this->view->cant_pages = $cant_pages;
		$this->view->assign ( 'actual_page', 1 + ($offset / $maxRecords) );
		
		if ($items ["cant"] > ($offset + $maxRecords)) {
			$this->view->assign ( 'next_offset', $offset + $maxRecords );
		} else {
			$this->view->assign ( 'next_offset', - 1 );
		}
		
		if ($offset > 0) {
			$this->view->assign ( 'prev_offset', $offset - $maxRecords );
		} else {
			$this->view->assign ( 'prev_offset', - 1 );
		}
		
		$this->view->items = $items ['data'];
		
		$all_procs = $items = $this->processMonitor->monitor_list_processes ( 0, - 1, 'name_desc', '', '' );
		$this->view->all_procs = $all_procs ['data'];
		
		if (isset ( $_REQUEST ['filter_process'] ) && $_REQUEST ['filter_process']) {
			$where = ' pId=' . $_REQUEST ['filter_process'];
		} else {
			$where = '';
		}
		
		$all_acts = $this->processMonitor->monitor_list_activities ( 0, - 1, 'name_desc', '', $where );
		$this->view->all_acts = $all_acts ['data'];
		
		$types = $this->processMonitor->monitor_list_activity_types ();
		$this->view->assign_by_ref ( 'types', $types );
		$this->view->types = $types;
		
		$names = $this->processMonitor->monitor_list_instances_names ();
		$this->view->names = $names;
		
		$this->view->assign ( 'stats', $this->processMonitor->monitor_stats () );
		
		$all_statuses = array ('aborted', 'active', 'completed', 'exception' );
		
		$this->view->assign ( 'all_statuses', $all_statuses );
		
		$sameurl_elements = array ('offset', 'sort_mode', 'where', 'find', 'filter_user', 'filter_status', 'filter_act_status', 'filter_type', 'pId', 'filter_process', 'filter_owner', 'filter_activity' );
		
		$this->view->assign ( 'statuses', $this->processMonitor->monitor_list_statuses () );
		$this->view->assign ( 'users', $this->processMonitor->monitor_list_users () );
		$this->view->assign ( 'owners', $this->processMonitor->monitor_list_owners () );
		
		//Display manager
		//$template = 'galaxia/monitor_instances.tpl';
		//include ('galaxia.php');
	} //End of method


} //End of class	

