<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/galaxia/abstract.php');
class Galaxia_ActivitiesController extends controllers_galaxia_abstract {
	protected $page_id = 'galaxia_graph'; //(string)
	protected $ifSuccessForward = array ('module' => 'galaxia', 'controller' => 'activities', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'galaxia', 'controller' => 'activities', 'action' => 'index' );
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->dbGalaxia = & Ranchbe::getDb ();

//		include_once (GALAXIA_LIBRARY . '/src/API/Process.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/Instance.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/BaseActivity.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Activity.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/End.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Join.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Split.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Standalone.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Start.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/SwitchActivity.php');
//		include_once (GALAXIA_LIBRARY . '/src/Observers/Galaxia_Observer_Logger.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/ProcessManager.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Instance.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Role.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Activity.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/GraphViz.php');
		
		//Galaxia engine
		//require_once('Galaxia/Galaxia_Manager_Process.php'); ::>>

		/// $roleManager is the object that will be used to manipulate roles.
		$this->roleManager = new Galaxia_Manager_Role ( $this->dbGalaxia );
		/// $activityManager is the object that will be used to manipulate activities.
		$this->activityManager = new Galaxia_Manager_Activity ( $this->dbGalaxia );
		/// $processManager is the object that will be used to manipulate processes.
		$this->processManager = new Galaxia_Manager_Process ( $this->dbGalaxia );
		/// $instanceManager is the object that will be used to manipulate instances.
		$this->instanceManager = new Galaxia_Manager_Instance ( $this->dbGalaxia );
		
		if (defined ( 'GALAXIA_LOGFILE' ) && GALAXIA_LOGFILE) {
			//include_once (GALAXIA_LIBRARY . '/src/Observers/Galaxia_Observer_Logger.php');
			$logger = new Galaxia_Observer_Logger ( GALAXIA_LOGFILE );
			$this->processManager->attach_all ( $logger );
			$this->activityManager->attach_all ( $logger );
			$this->roleManager->attach_all ( $logger );
		}
		
		$this->pid = ( int ) $this->getRequest ()->getParam ( 'pid' );
		
		if ($this->pid) {
			$this->proc_info = $this->processManager->get_process ( $this->pid );
		} else {
			$this->proc_info = array ('name' => '', 'description' => '', 'version' => '1.0', 'isActive' => 'n', 'pId' => 0 );
		}
		
		$this->activityId = ( int ) $this->getRequest ()->getParam ( 'activityId' );
		
		// Retrieve activity info if we are editing, assign to 
		// default values when creating a new activity

		if ($this->activityId) {
			$this->act_info = $this->activityManager->get_activity ( $this->pid, $this->activityId );
			$time = $this->activityManager->get_expiration_members ( $this->act_info ['expirationTime'] );
			$this->act_info ['year'] = $time ['year'];
			$this->act_info ['month'] = $time ['month'];
			$this->act_info ['day'] = $time ['day'];
			$this->act_info ['hour'] = $time ['hour'];
			$this->act_info ['minute'] = $time ['minute'];
		} else {
			$this->act_info = array (
					'name' => '', 
					'description' => '', 
					'activityId' => 0, 
					'isInteractive' => 'n', 
					'isAutoRouted' => 'y', 
					'isAutomatic' => 'n', 
					'isComment' => 'y', 
					'type' => 'activity', 
					'month' => 0, 'day' => 0, 'hour' => 0, 'minute' => 0, 
					'expirationTime' => 0 );
		}
		$this->view->assign ( 'activityId', $this->activityId );
		$this->view->assign ( 'act_info', $this->act_info );
		$this->view->assign ( 'proc_info', $this->proc_info );
		$this->view->assign ( 'pid', $this->pid );
		$this->view->assign ( 'is_active_help', tra ( 'indicates if the process is active. Invalid processes cant be active' ) );
	} //End of method
	

	//---------------------------------------------------------------------
	//get admin activities page
	public function indexAction() {
		// Get all the process roles
		$all_roles = $this->roleManager->list_roles ( $this->pid, 0, - 1, 'name_asc', '' );
		$this->view->all_roles = $all_roles ['data'];
		
		// Get activity roles
		if ($this->activityId) {
			$roles = $this->activityManager->get_activity_roles ( $this->activityId );
		} else {
			$roles = array ();
		}
		$this->view->assign ( 'roles', $roles );
		
		$this->view->transitions = $this->activityManager->get_process_transitions ( $this->pid, '' );
		
		$offset = 0;
		$maxRecords = 50000;
		$sort_mode = 'flowNum_asc';
		$find = '';
		$where = '';
		
		//Now information for activities in this process
		$activities = $this->activityManager->list_activities ( $this->pid, 0, - 1, $sort_mode, $find, $where );
		
		//Now check if the activity is or not part of a transition
		if ($this->activityId) {
			$temp_max = count ( $activities ['data'] );
			for($i = 0; $i < $temp_max; $i ++) {
				$id = $activities ['data'] [$i] ['activityId'];
				$activities ['data'] [$i] ['to'] = $this->activityManager->transition_exists ( $this->pid, $this->activityId, $id ) ? 'y' : 'n';
				$activities ['data'] [$i] ['from'] = $this->activityManager->transition_exists ( $this->pid, $id, $this->activityId ) ? 'y' : 'n';
			}
		}
		
		$maxExpirationTime = array ('years' => 5, 'months' => 11, 'days' => 30, 'hours' => 23, 'minutes' => 59 );
		$arYears = array ();
		$arMonths = array ();
		$arDays = array ();
		$arHours = array ();
		$arminutes = array ();
		for($i = 0; $i <= $maxExpirationTime ['months']; $i ++)
			$arMonths [$i] = $i;
		for($i = 0; $i <= $maxExpirationTime ['years']; $i ++)
			$arYears [$i] = $i;
		for($i = 0; $i <= $maxExpirationTime ['days']; $i ++)
			$arDays ["$i"] = $i;
		for($i = 0; $i <= $maxExpirationTime ['hours']; $i ++)
			$arHours ["$i"] = $i;
		for($i = 0; $i <= $maxExpirationTime ['minutes']; $i ++)
			$arminutes ["$i"] = $i;
		$this->view->assign ( 'years', $arYears );
		$this->view->assign ( 'months', $arMonths );
		$this->view->assign ( 'days', $arDays );
		$this->view->assign ( 'hours', $arHours );
		$this->view->assign ( 'minutes', $arminutes );
		
		$this->view->items = $activities ['data'];
		
		if ($this->pid) {
			Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
			$this->activityManager->build_process_graph ( $this->pid );
			$valid = $this->activityManager->validate_process_activities ( $this->pid );
			$errors = array ();
			if (! $valid) {
				$this->processManager->deactivate_process ( $this->pid );
				$errors = $this->activityManager->get_error ();
			}
			$this->view->assign ( 'errors', $errors );
		}
		
		$this->view->proc_info = $this->_getGraph ( $this->pid, $this->proc_info );
		
		// Display the template
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'workflowTab' )->activate ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function removeroleAction() {
		$role_id = $this->getRequest ()->getParam ( 'role_id' );
		$this->activityManager->remove_activity_role ( $this->activityId, $role_id );
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$process = $this->getRequest ()->getParam ( 'process' );
		if (is_array ( $process )) {
			foreach ( array_keys ( $process ) as $pid ) {
				$this->processManager->remove_process ( $pid );
			}
		}
		return $this->_successForward();
	} //End of method
	

	//---------------------------------------------------------------------
/*
	public function addroleAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$isInteractive = (isset ( $_REQUEST ['isInteractive'] ) && $_REQUEST ['isInteractive'] == 'on') ? 'y' : 'n';
		$isAutoRouted = (isset ( $_REQUEST ['isAutoRouted'] ) && $_REQUEST ['isAutoRouted'] == 'on') ? 'y' : 'n';
		$isAutomatic = (isset ( $_REQUEST ['isAutomatic'] ) && $_REQUEST ['isAutomatic'] == 'on') ? 'y' : 'n';
		$isComment = (isset ( $_REQUEST ['isComment'] ) && $_REQUEST ['isComment'] == 'on') ? 'y' : 'n';
		
		$info = array (
					'name' => $_REQUEST ['name'], 
					'description' => $_REQUEST ['description'], 
					'activityId' => $_REQUEST ['activityId'], 
					'isInteractive' => $isInteractive, 
					'isAutoRouted' => $isAutoRouted, 
					'isAutomatic' => $isAutomatic, 
					'isComment' => $isComment, 
					'type' => $_REQUEST ['type'], 
					'month' => 0, 'day' => 0, 'hour' => 0, 'minute' => 0, 
					'expirationTime' => 0 );
		
		if (empty ( $_REQUEST ['rolename'] )) {
			$this->view->assign ( 'msg', tra ( "Role name cannot be empty" ) );
			$this->view->display ( "error.tpl" );
			die ();
		}
		
		$vars = array ('name' => $_REQUEST ['rolename'], 'description' => '' );
		
		if (isset ( $_REQUEST ["userole"] ) && $_REQUEST ["userole"]) {
			if ($_REQUEST ['activityId']) {
				$activityManager->add_activity_role ( $_REQUEST ['activityId'], $_REQUEST ["userole"] );
			}
		} else {
			$rid = $roleManager->replace_role ( $_REQUEST ['pid'], 0, $vars );
			
			if ($_REQUEST ['activityId']) {
				$activityManager->add_activity_role ( $_REQUEST ['activityId'], $rid );
			}
		}
	} //End of method
*/

	//---------------------------------------------------------------------
	//delete activity
	public function deleteactAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$activities = ( array ) $this->getRequest ()->getParam ( 'activity' );
		foreach ( array_keys ( $activities ) as $item ) {
			$this->activityManager->remove_activity ( $this->pid, $item );
		}
		return $this->_successForward();
	} //End of method
	

	//---------------------------------------------------------------------
	// If we are adding an activity then add it!
	public function saveactAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		
		$isInteractive = $this->getRequest ()->getParam ( 'isInteractive' );
		$isAutomatic = $this->getRequest ()->getParam ( 'isAutomatic' );
		$isComment = $this->getRequest ()->getParam ( 'isComment' );
		
		//A activity is always AutoRouted in ranchbe, else, process is dead and can be reactivated except for standalone activity
		//$isAutoRouted =  $this->getRequest()->getParam('isAutoRouted');
		//$isAutoRouted = ($isAutoRouted) ? 'y' : 'n';
		$isAutoRouted = 'y';
		
		$name = $this->getRequest ()->getParam ( 'name' );
		$description = $this->getRequest ()->getParam ( 'description' );
		$type = $this->getRequest ()->getParam ( 'type' );
		$userole = ( int ) $this->getRequest ()->getParam ( 'userole' );
		$rolename = $this->getRequest ()->getParam ( 'rolename' );
		$add_tran_from = ( array ) $this->getRequest ()->getParam ( 'add_tran_from' );
		$add_tran_to = ( array ) $this->getRequest ()->getParam ( 'add_tran_to' );
		
		$year = ( int ) $this->getRequest ()->getParam ( 'year' );
		$month = ( int ) $this->getRequest ()->getParam ( 'month' );
		$day = ( int ) $this->getRequest ()->getParam ( 'day' );
		$hour = ( int ) $this->getRequest ()->getParam ( 'hour' );
		$minute = ( int ) $this->getRequest ()->getParam ( 'minute' );
		$expirationTime = $year * 535680 + $month * 44640 + $day * 1440 + $hour * 60 + $minute;
		
		$vars = array (
					'activityId' => $this->activityId, 
					'name' => $name, 
					'description' => $description, 
					'isInteractive' => $isInteractive, 
					'isAutoRouted' => $isAutoRouted, 
					'isAutomatic' => $isAutomatic, 
					'isComment' => $isComment, 
					'type' => $type, 
					'expirationTime' => $expirationTime );
		
		if (empty ( $name )) {
			$this->error_stack->push ( Rb_Error::ERROR, array (), tra ( 'Activity name cannot be empty' ) );
			return $this->_successForward();
		}
		
		if ($this->activityManager->activity_name_exists ( $this->pid, $name ) && $this->activityId == 0) {
			$this->error_stack->push ( Rb_Error::ERROR, array (), tra ( 'Activity name already exists' ) );
			return $this->_successForward();
		}
		
		$newaid = $this->activityManager->replace_activity ( $this->pid, $this->activityId, $vars );
		$rid = 0;
		
		if ($userole)
			$rid = $userole;
		
		if (! empty ( $rolename )) {
			$vars = array ('name' => $rolename, 'description' => '' );
			$rid = $this->roleManager->replace_role ( $this->pid, 0, $vars );
		}
		
		if ($rid) {
			$this->activityManager->add_activity_role ( $newaid, $rid );
		}
		
		$this->act_info = array (
							'name' => '', 
							'description' => '', 
							'activityId' => 0, 
							'isInteractive' => 'y', 
							'isAutoRouted' => 'n', 
							'isAutomatic' => 'n', 
							'isComment' => 'y', 
							'type' => 'activity' );
		
		$this->activityId = 0;
		//$smarty->assign('info', $info); //unusefull because is yet assign by ref

		// remove transitions
		$this->activityManager->remove_activity_transitions ( $this->pid, $newaid );		
		if ($add_tran_from) {
			foreach ( $add_tran_from as $actfrom ) {
				$this->activityManager->add_transition ( $this->pid, $actfrom, $newaid );
			}
		}
		if ($add_tran_to) {
			foreach ( $add_tran_to as $actto ) {
				$this->activityManager->add_transition ( $this->pid, $newaid, $actto );
			}
		}
		return $this->_successForward();
	} //End of method
	

	//---------------------------------------------------------------------
	//get admin role page
	public function updateactAction() {
		
		$isInteractive = ( array ) $this->getRequest ()->getParam ( 'isInteractive' );
		$isAutoRouted = ( array ) $this->getRequest ()->getParam ( 'isAutoRouted' );
		$isAutomatic = ( array ) $this->getRequest ()->getParam ( 'isAutomatic' );
		$isComment = ( array ) $this->getRequest ()->getParam ( 'isComment' );
		$activities_ids = ( array ) $this->getRequest ()->getParam ( 'items' );
		
		//var_dump($isInteractive);
		//var_dump($isAutoRouted);
		//var_dump($isAutomatic);
		//var_dump($isComment);
		//var_dump($activities_ids);

		for($i = 0; $i < count ( $activities_ids ); $i ++) {
			$id = $activities_ids [$i];
			
			if ($isInteractive [$id]) {
				$this->activityManager->set_interactivity ( $this->pid, $id, 'y' );
			} else {
				$this->activityManager->set_interactivity ( $this->pid, $id, 'n' );
			}
			
			if ($isAutoRouted [$id]) {
				$this->activityManager->set_autorouting ( $this->pid, $id, 'y' );
			} else {
				$this->activityManager->set_autorouting ( $this->pid, $id, 'n' );
			}
			
			if ($isAutomatic [$id]) {
				$this->activityManager->set_automatic ( $this->pid, $id, 'y' );
			} else {
				$this->activityManager->set_automatic ( $this->pid, $id, 'n' );
			}
			
			if ($isComment [$id]) {
				$this->activityManager->set_commented ( $this->pid, $id, 'y' );
			} else {
				$this->activityManager->set_commented ( $this->pid, $id, 'n' );
			}
		}
		return $this->_successForward();
	} //End of method
	

	//---------------------------------------------------------------------
	//delete transition
	public function deletetransAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		
		$transitions = ( array ) $this->getRequest ()->getParam ( 'transition' );
		
		foreach ( array_keys ( $transitions ) as $item ) {
			$parts = explode ( "_", $item );
			$this->activityManager->remove_transition ( $parts [0], $parts [1] );
		}
		return $this->_successForward();
	} //End of method
	

	//---------------------------------------------------------------------
	//add transition
	public function addtransAction() {
		$actFromId = ( int ) $this->getRequest ()->getParam ( 'actFromId' );
		$actToId = ( int ) $this->getRequest ()->getParam ( 'actToId' );
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$this->activityManager->add_transition ( $this->pid, $actFromId, $actToId );
		return $this->_successForward();
	} //End of method
	

	//---------------------------------------------------------------------
	//activate the process
	public function activateprocAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$this->processManager->activate_process ( $this->pid );
		return $this->_successForward ( 'index', 'processes' );
	} //End of method
	

	//---------------------------------------------------------------------
	//desactivate the process
	public function deactivateprocAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$this->processManager->deactivate_process ( $this->pid );
		return $this->_successForward ( 'index', 'processes' );
	} //End of method
	

	//---------------------------------------------------------------------
	//get graph
	protected function updateGraphAction() {
		$this->activityManager->build_process_graph ( $this->pid );
		return $this->_successForward();
	} //End of method
	
	//---------------------------------------------------------------------
	//run activity
	public function runAction() {
		$__activity_completed = false;
		$activityId = $this->getRequest ()->getParam ( 'activityId' );
		$__post = $this->getRequest ()->getParam ( '__post' );
		$name = $this->getRequest ()->getParam ( 'name' );
		$__removecomment = $this->getRequest ()->getParam ( '__removecomment' );
		$__cid = $this->getRequest ()->getParam ( '__cid' );
		$this->view->iid = ( int ) $this->getRequest ()->getParam ( 'iid' );
		$iid = & $this->view->iid;
		$__title =  $this->getRequest ()->getParam ( '__title' );
		$__comment = & $this->getRequest ()->getParam ( '__comment' );
		$auto = $this->getRequest ()->getParam ( 'auto' );
		$start_page = $this->getRequest ()->getParam ( 'start_page' );
		$CnextUsers = $this->getRequest ()->getParam ( 'CnextUsers' );
		
		// Determine the activity using the activityId request
		// parameter and get the activity information
		// load then the compiled version of the activity

		if (empty ( $_REQUEST ['activityId'] )) {
			$this->view->assign ( 'msg', tra ( "No activity indicated" ) );
			$this->view->display ( "error_simple.tpl" );
			die ();
		}
		
		// When $user is null, it means an anonymous user is trying to use Galaxia
		//$user = is_null($user) ? "Anonymous" : $user;
		//$user = is_null($user) ? "Anonymous" : $user;
		
		$activity = $baseActivity->getActivity ( $activityId );
		$process->getProcess ( $activity->getProcessId () );
		
		// Get user roles
		// Get activity roles
		$act_roles = $activity->getRoles ();
		$user_roles = $activity->getUserRoles ( $user );
		
		// Only run a start activity if a name is set for the instance
		if ($activity->getType () == 'start' && ! $__post) {
			if (empty ( $name )) {
				$this->view->assign ( 'msg', tra ( "A start activity requires a name for the instance" ) );
				$this->view->display ( "error_simple.tpl" );
				die ();
			}
		}
		
		// Only check roles if this is an interactive activity
		if ($activity->isInteractive () == 'y') {
			if (! count ( array_intersect ( $act_roles, $user_roles ) )) {
				$this->view->assign ( 'msg', tra ( "You cant execute this activity" ) );
				$this->view->display ( "error_simple.tpl" );
				die ();
			}
		}
		
		$act_role_names = $activity->getActivityRoleNames ( $user );
		
		foreach ( $act_role_names as $role ) {
			$name = $role ['name'];
			if (in_array ( $role ['roleId'], $user_roles )) {
				$this->view->assign ( $name, 'y' );
				$$name = 'y';
			} else {
				$this->view->assign ( $name, 'n' );
				$$name = 'n';
			}
		}
		
		if (! isset ( $_REQUEST ['__post'] )) {
			// Existing variables here:
			// $process, $activity, $instance (if not standalone)
			// Get paths for original and compiled activity code
			$origcode = GALAXIA_PROCESSES 
						. '/' . $process->getNormalizedName () 
						. '/code/activities/' 
						. $activity->getNormalizedName () . '.php';
			$compcode = GALAXIA_PROCESSES 
						. '/' . $process->getNormalizedName () 
						. '/compiled/' 
						. $activity->getNormalizedName () . '.php';
			
			// Now get paths for original and compiled template
			$origtmpl = GALAXIA_PROCESSES 
						. '/' . $process->getNormalizedName () 
						. '/code/templates/' 
						. $activity->getNormalizedName () . '.tpl';
			$comptmpl = 'templates/' . $process->getNormalizedName () 
						. '/' . $activity->getNormalizedName () . '.tpl';
			
			$recomplile = false; //var init to choose recompile or not
			//If activity is not interactive, they are not template file
			if ($activity->isInteractive ()) {
				if (filemtime ( $origtmpl ) > filemtime ( $comptmpl ))
					$recomplile = true;
			}
			if ((filemtime ( $origcode ) > filemtime ( $compcode ))) {
				$recomplile = true;
			}
			
			// Check whether the activity code or template are newer than their compiled counterparts,
			// i.e. check if the source code or template were changed; if so, we need to recompile
			if ($recomplile) {
				// Recompile the activity
				//include_once ('Galaxia/Galaxia_Manager_Process.php');
				$activityManager->compile_activity ( $activity->getProcessId (), $activity->getActivityId () );
			}
			
			// Get paths for shared code and activity
			$shared = GALAXIA_PROCESSES . '/' . $process->getNormalizedName () . '/code/shared.php';
			$source = $compcode;
			
			// Include the shared code
			//include_once ($shared);
			
			// Now do whatever you have to do in the activity
			//include_once ($source);
		}
		
		if (! $__cid )
			$__cid = 0;
			
		//Update instance comment after validation of post operations
		if ( $__post ) {
			$instance->getInstance ( $iid );
			$instance->replace_instance_comment ( $__cid, 
				$activity->getActivityId (), 
				$activity->getName (), 
				$user, $__title, $__comment );
		} //End of post operation

		$__comments = $instance->get_instance_comments ( $activity->getActivityId () );
		
		// This goes to the end part of all activities
		// If this activity is interactive then we have to display the template
		if (! $auto 
			&& $__activity_completed 
			&& ! $__post) {
			//Display form for input comments
			$this->view->assign ( 'procname', $process->getName () );
			$this->view->assign ( 'procversion', $process->getVersion () );
			$this->view->assign ( 'actname', $activity->getName () );
			$this->view->assign ( 'actid', $activity->getActivityId () );
			$this->view->assign ( 'post', 'n' );
			$this->view->assign ( 'iid', $instance->instanceId );
			$this->view->assign ( 'CnextUsers', implode ( '%3B', $instance->CnextUsers ) );
			$this->view->assign ( 'start_page', $start_page ); //Store URL to return to this page from next step pages
			//$this->view->display ( "galaxia/activity_completed.tpl" );
		} 

		elseif (! $auto && $__post ) {
			$this->view->assign ( 'procversion', $process->getVersion () );
			$this->view->assign ( 'actname', $activity->getName () );
			$this->view->assign ( 'actid', $activity->getActivityId () );
			$this->view->assign ( 'title', $__title );
			$this->view->assign ( 'comment', $__comment );
			$this->view->assign ( 'post', 'y' );
			$this->view->assign ( 'mid', 'galaxia/activity_completed.tpl' );
			$this->view->assign ( 'start_page', $start_page ); //Store URL to return to this page from next step pages
			//$this->view->display ( "galaxia/activity_completed.tpl" );
			
			//Send comments to next user
			$CnextUsers = explode ( '%3B', $CnextUsers );
			$message = new Rb_Message ( $this->db );
			$from = $user;
			$cc = '';
			$subject = $__title;
			$body = 'Process :' . $process->getVersion () . '<br>' 
					. 'Activity :' . $activity->getName () 
					. '<br>' . 'Subject :' 
					. $__title 
					. '<br>' . 'Comment :' 
					. $__comment . '<br>';
			$priority = 3;
			
			foreach ( $CnextUsers as $to ) {
				$message->postMessage ( $to, $from, $to, $cc, $subject, $body, $priority );
			}
		} else {
			//Display template of the activity
			if (! isset ( $_REQUEST ['auto'] ) && $activity->isInteractive ()) {
				$template = $activity->getNormalizedName () . '.tpl';
				$this->view->display ( $process->getNormalizedName () . '/' . $template );
			}
		} //End of displaying template

	} //End of method

} //end of class
