<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');
abstract class controllers_galaxia_abstract extends controllers_abstract {
	
	//---------------------------------------------------------------------
	//Get graph
	protected function _getGraph($pid, $proc_info) {
		$this->activityManager->build_process_graph ( $pid );
		$proc_info ['graph'] = GALAXIA_PROCESSES . '/' . $proc_info ['normalized_name'] . '/graph/' . $proc_info ['normalized_name'] . '.png';
		$mapfile = GALAXIA_PROCESSES . '/' . $proc_info ['normalized_name'] . '/graph/' . $proc_info ['normalized_name'] . '.map';
		if (file_exists ( $proc_info ['graph'] ) && file_exists ( $mapfile )) {
			//$map = join('' , file($mapfile));
			$url = $this->view->baseUrl('galaxia/activities/index/pid/' . $pid);
			$proc_info ['map'] = implode ( "\n", str_replace ( 'foourl?activityId=', $url . '/activityId/', file ( $mapfile ) ) );
		} else {
			$proc_info ['graph'] = '';
		}
		return $proc_info;
	} //End of method


}//End of class

