<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');
class Galaxia_RolesController extends controllers_abstract {
	
	protected $page_id = 'galaxia_roles'; //(string)
	protected $ifSuccessForward = array ('module' => 'galaxia', 'controller' => 'roles', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'galaxia', 'controller' => 'roles', 'action' => 'index' );
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->dbGalaxia = & Ranchbe::getDb ();
		
//		include_once (GALAXIA_LIBRARY . '/src/API/Process.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/Instance.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/BaseActivity.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Activity.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/End.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Join.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Split.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Standalone.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Start.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/SwitchActivity.php');
//		include_once (GALAXIA_LIBRARY . '/src/Observers/Galaxia_Observer_Logger.php');
//		
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/ProcessManager.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Instance.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Role.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Activity.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/GraphViz.php');
		
		//Galaxia engine
		//require_once('Galaxia/Galaxia_Manager_Process.php'); ::>>
		

		/// $roleManager is the object that will be used to manipulate roles.
		$this->roleManager = new Galaxia_Manager_Role ( $this->dbGalaxia );
		/// $activityManager is the object that will be used to manipulate activities.
		$this->activityManager = new Galaxia_Manager_Activity ( $this->dbGalaxia );
		/// $processManager is the object that will be used to manipulate processes.
		$this->processManager = new Galaxia_Manager_Process ( $this->dbGalaxia );
		/// $instanceManager is the object that will be used to manipulate instances.
		$this->instanceManager = new Galaxia_Manager_Instance ( $this->dbGalaxia );
		
		if (defined ( 'GALAXIA_LOGFILE' ) && GALAXIA_LOGFILE) {
			//include_once (GALAXIA_LIBRARY . '/src/Observers/Galaxia_Observer_Logger.php');
			$logger = new Galaxia_Observer_Logger ( GALAXIA_LOGFILE );
			$this->processManager->attach_all ( $logger );
			$this->activityManager->attach_all ( $logger );
			$this->roleManager->attach_all ( $logger );
		}
		
		$this->pid = ( int ) $this->getRequest ()->getParam ( 'pid' );
		
		if ($this->pid) {
			$this->proc_info = $this->processManager->get_process ( $this->pid );
		}
		
		$this->view->proc_info = $this->proc_info;
		$this->view->assign ( 'pid', $this->pid );
		
		$this->roleId = ( int ) $this->getRequest ()->getParam ( 'roleId' );
		
		if ($this->roleId) {
			$this->role_info = $this->roleManager->get_role ( $this->pid, $this->roleId );
		} else {
			$this->roleId = 0;
			$this->role_info = array ('name' => '', 'description' => '', 'roleId' => 0 );
		}
		
		$this->view->assign ( 'roleId', $this->roleId );
		$this->view->assign ( 'info', $this->role_info );
	
	} //End of method
	

	//---------------------------------------------------------------------
	//get admin role page
	public function indexAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		
		$offset = 0;
		$maxRecords = 50000;
		$sort_mode = 'lastModif_desc';
		$find = '';
		$where = '';
		
		$mapitems = $this->roleManager->list_mappings ( $this->pid, $offset, $maxRecords, $sort_mode, $find );
		$this->view->mapitems = $mapitems ['data'];
		
		$all_roles = $this->roleManager->list_roles ( $this->pid, 0, - 1, 'name_asc', '' );
		$this->view->items = $all_roles ['data'];
		$this->view->roles = $all_roles ['data'];
		
		$valid = $this->activityManager->validate_process_activities ( $this->pid );
		$this->proc_info ['isValid'] = $valid ? 'y' : 'n';
		$errors = array ();
		
		if (! $valid) {
			$errors = $this->activityManager->get_error ();
		}
		
		// MAPIING
		$users = Ranchbe::getAuthAdapter ()->getUsers ();
		if ($users === false) {
			echo '<p>List_user: error on line: ' . __LINE__ . '</p>';
		}
		$this->view->users = $users;
		
		$groups = Rb_Group::getGroups (); //var_dump($groups);
		$this->view->groups = $groups;
		
		//$roles = $this->roleManager->list_roles($this->pid, 0, -1, 'name_asc', '');//???
		//$this->view->roles = $roles['data'];
		

		$this->view->assign ( 'errors', $errors );
		
		// Display the template
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'workflowTab' )->activate ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function deleteAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$role = $this->getRequest ()->getParam ( 'role' );
		foreach ( array_keys ( $role ) as $item ) {
			$this->roleManager->remove_role ( $this->pid, $item );
		}
		return $this->_forward('index');
	} //End of method
	

	//---------------------------------------------------------------------
	//save new role
	public function saveAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$vars = array ('name' => $this->getRequest ()->getParam ( 'name' ), 'description' => $this->getRequest ()->getParam ( 'description' ) );
		$this->roleManager->replace_role ( $this->pid, $this->roleId, $vars );
		$this->role_info = array ('name' => '', 'description' => '', 'roleId' => 0 );
		$this->roleId = 0;
		$this->view->assign ( 'info', $this->role_info );
		return $this->_forward ( 'index' );
	} //End of method
	

	//---------------------------------------------------------------------
	//Map users to roles
	public function savemapAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$users = ( array ) $this->getRequest ()->getParam ( 'user' ); //var_dump($users);
		$role_ids = $this->getRequest ()->getParam ( 'role' );
		if ($users && $role_ids) {
			foreach ( $users as $user ) {
				foreach ( $role_ids as $role_id ) {
					$this->roleManager->map_user_to_role ( $this->pid, $user, $role_id );
				}
			}
		}
		return $this->_forward ( 'index' );
	} //End of method
	

	//---------------------------------------------------------------------
	//Map group to role
	public function mapgAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$op = $this->getRequest ()->getParam ( 'op' );
		$group_id = ( int ) $this->getRequest ()->getParam ( 'group' ); //var_dump($group_id);
		$role_id = ( int ) $this->getRequest ()->getParam ( 'role' );
		$users = ( array ) Rb_Group::get ( $group_id )->getUsers (); //var_dump($users);die;
		if ($op == 'add') {
			foreach ( $users as $user ) {
				$this->roleManager->map_user_to_role ( $this->pid, $user ['handle'], $role_id );
			}
		} else {
			foreach ( $users as $user ) {
				$this->roleManager->remove_mapping ( $user ['handle'], $role_id );
			}
		}
		return $this->_forward('index');
	} //End of method
	

	//---------------------------------------------------------------------
	public function deletemapAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$map = $this->getRequest ()->getParam ( 'map' );
		foreach ( array_keys ( $map ) as $item ) {
			$parts = explode ( ':::', $item );
			$this->roleManager->remove_mapping ( $parts [0], $parts [1] );
		}
		return $this->_forward('index');
	} //End of method


} //end of class
