<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/galaxia/abstract.php');
class Galaxia_GraphController extends controllers_galaxia_abstract {
	
	protected $page_id = 'galaxia_graph'; //(string)
	protected $ifSuccessForward = array ('module' => 'galaxia', 'controller' => 'graph', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'galaxia', 'controller' => 'graph', 'action' => 'index' );
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->dbGalaxia = & Ranchbe::getDb ();
		
//		include_once (GALAXIA_LIBRARY . '/src/API/Process.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/Instance.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/BaseActivity.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Activity.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/End.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Join.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Split.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Standalone.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Start.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/SwitchActivity.php');
//		include_once (GALAXIA_LIBRARY . '/src/Observers/Galaxia_Observer_Logger.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/ProcessManager.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Instance.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Role.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Activity.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/GraphViz.php');
		
		//Galaxia engine
		//require_once('Galaxia/Galaxia_Manager_Process.php'); ::>>

		/// $roleManager is the object that will be used to manipulate roles.
		$this->roleManager = new Galaxia_Manager_Role ( $this->dbGalaxia );
		/// $activityManager is the object that will be used to manipulate activities.
		$this->activityManager = new Galaxia_Manager_Activity ( $this->dbGalaxia );
		/// $processManager is the object that will be used to manipulate processes.
		$this->processManager = new Galaxia_Manager_Process ( $this->dbGalaxia );
		/// $instanceManager is the object that will be used to manipulate instances.
		$this->instanceManager = new Galaxia_Manager_Instance ( $this->dbGalaxia );
		
		if (defined ( 'GALAXIA_LOGFILE' ) && GALAXIA_LOGFILE) {
			//include_once (GALAXIA_LIBRARY . '/src/Observers/Galaxia_Observer_Logger.php');
			$logger = new Galaxia_Observer_Logger ( GALAXIA_LOGFILE );
			$this->processManager->attach_all ( $logger );
			$this->activityManager->attach_all ( $logger );
			$this->roleManager->attach_all ( $logger );
		}
		
		$this->pid = ( int ) $this->getRequest ()->getParam ( 'pid' );
		
		if ($this->pid) {
			$this->activityManager->build_process_graph ( $this->pid );
			$this->view->proc_info = $this->_getGraph ( $this->pid, $this->processManager->get_process ( $this->pid ) );
		} else {
			$this->view->proc_info = array (
										'name' => '', 
										'description' => '', 
										'version' => '1.0', 
										'isActive' => 'n', 
										'pId' => 0 );
		}
		//var_dump($this->view->proc_info);
		$this->view->assign ( 'pid', $this->pid );
		$this->view->assign ( 'is_active_help',
			tra ( 'indicates if the process is active. Invalid processes cant be active' ) );
	
	} //End of method
	

	//---------------------------------------------------------------------
	//get admin role page
	public function indexAction() {
		$offset = 0;
		$maxRecords = 50000;
		$sort_mode = 'lastModif_desc';
		$find = '';
		$where = '';
		$items = $this->processManager->list_processes ( $offset, $maxRecords, $sort_mode, $find, $where );
		$this->view->items = $items ['data'];
		if ($this->pid) {
			Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
			$valid = $this->activityManager->validate_process_activities ( $this->pid );
			$errors = array ();
			if (! $valid) {
				$this->processManager->deactivate_process ( $this->pid );
				$errors = $this->activityManager->get_error ();
			}
			$this->view->assign ( 'errors', $errors );
		}
		// Display the template
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'workflowTab' )->activate ();
	} //End of method


} //end of class
