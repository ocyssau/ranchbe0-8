<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');
class Galaxia_SharedsourceController extends controllers_abstract {
	
	protected $page_id = 'galaxia_sharedsource'; //(string)
	protected $ifSuccessForward = array ('module' => 'galaxia', 'controller' => 'sharedsource', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'galaxia', 'controller' => 'sharedsource', 'action' => 'index' );
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->dbGalaxia = & Ranchbe::getDb ();
//		include_once (GALAXIA_LIBRARY . '/src/API/Process.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/Instance.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/BaseActivity.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Activity.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/End.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Join.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Split.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Standalone.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/Start.php');
//		include_once (GALAXIA_LIBRARY . '/src/API/activities/SwitchActivity.php');
//		include_once (GALAXIA_LIBRARY . '/src/Observers/Galaxia_Observer_Logger.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/ProcessManager.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Instance.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Role.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/Galaxia_Manager_Activity.php');
//		include_once (GALAXIA_LIBRARY . '/src/Galaxia_Manager_Process/GraphViz.php');
		
		//Galaxia engine
		//require_once('Galaxia/Galaxia_Manager_Process.php'); ::>>

		/// $roleManager is the object that will be used to manipulate roles.
		$this->roleManager = new Galaxia_Manager_Role ( $this->dbGalaxia );
		/// $activityManager is the object that will be used to manipulate activities.
		$this->activityManager = new Galaxia_Manager_Activity ( $this->dbGalaxia );
		/// $processManager is the object that will be used to manipulate processes.
		$this->processManager = new Galaxia_Manager_Process ( $this->dbGalaxia );
		/// $instanceManager is the object that will be used to manipulate instances.
		$this->instanceManager = new Galaxia_Manager_Instance ( $this->dbGalaxia );
		
		if (defined ( 'GALAXIA_LOGFILE' ) && GALAXIA_LOGFILE) {
			//include_once (GALAXIA_LIBRARY . '/src/Observers/Galaxia_Observer_Logger.php');
			$logger = new Galaxia_Observer_Logger ( GALAXIA_LOGFILE );
			$this->processManager->attach_all ( $logger );
			$this->activityManager->attach_all ( $logger );
			$this->roleManager->attach_all ( $logger );
		}
		
		$this->pid = ( int ) $this->getRequest ()->getParam ( 'pid' );
		
		if ($this->pid) {
			$this->proc_info = $this->processManager->get_process ( $this->pid );
		} else {
			$this->error_stack->push ( Rb_Error::ERROR, array (), tra ( 'No process indicated' ) );
			return $this->_forward ( 'index', 'processes' );
		}
		
		$this->activityId = ( int ) $this->getRequest ()->getParam ( 'activityId' );
		
		// Retrieve activity info
		if ($this->activityId) {
			$this->act_info = $this->activityManager->get_activity ( $this->pid, $this->activityId );
		}
		
		$this->view->template = $this->getRequest ()->getParam ( 'template' );
		$this->view->assign ( 'activityId', $this->activityId );
		$this->view->act_info = $this->act_info;
		$this->view->proc_info = $this->proc_info;
		$this->view->assign ( 'pid', $this->pid );
		$this->view->assign ( 'is_active_help', tra ( 'indicates if the process is active. Invalid processes cant be active' ) );
	} //End of method
	

	//---------------------------------------------------------------------
	//get admin activities page
	public function indexAction() {
		if (! $this->activityId) {
			$this->getsharedAction ();
		} else {
			if ($this->view->template == 'y') {
				$this->gettemplateAction ();
			} else {
				$this->getcodeAction ();
			}
		}
	} //End of method
	

	//---------------------------------------------------------------------
	//get source datas
	protected function _getSourceDatas() {
		$fp = fopen ( $this->source, "r" );
		if (filesize ( $this->source ) > 0) //Ranchbe 0.5
			$data = fread ( $fp, filesize ( $this->source ) );
		fclose ( $fp );
		$this->view->assign ( 'data', $data );
		return $data;
	} //End of method
	

	//---------------------------------------------------------------------
	//get source datas
	protected function _display() {
		$valid = $this->activityManager->validate_process_activities ( $this->pid );
		$errors = array ();
		if (! $valid) {
			$errors = $this->activityManager->get_error ();
			$this->proc_info ['isValid'] = 'n';
		} else {
			$this->proc_info ['isValid'] = 'y';
		}
		$this->view->assign ( 'errors', $errors );
		$activities = $this->activityManager->list_activities ( $this->pid, 0, - 1, 'name_asc', '' );
		$this->view->items = $activities ['data'];
		// Display the template
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'workflowTab' )->activate ();
	} //End of method
	

	//---------------------------------------------------------------------
	//get code
	public function getcodeAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$this->_helper->viewRenderer->setScriptAction ( 'index' );
		$this->view->template = 'n';
		$this->_initCode ();
		$this->view->assign ( 'source_file', $this->source_file );
		$this->view->assign ( 'source_type', 'code' );
		$this->_getSourceDatas ();
		return $this->_display ();
	} //End of method
	

	//---------------------------------------------------------------------
	//get template
	public function gettemplateAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$this->_helper->viewRenderer->setScriptAction ( 'index' );
		if ($this->act_info ['isInteractive'] == 'n') {
			$this->view->template = 'n';
			return $this->getcodeAction ();
		}
		$this->view->template = 'y';
		$this->_initTemplate ();
		$this->view->source_file = $this->source_file;
		$this->view->source_type = template;
		$this->_getSourceDatas ();
		return $this->_display ();
	} //End of method
	

	//---------------------------------------------------------------------
	//get shared code
	public function getsharedAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$this->_helper->viewRenderer->setScriptAction ( 'index' );
		$this->_initShared ();
		$this->view->assign ( 'source_file', $this->source_file );
		$this->view->assign ( 'source_type', 'shared' );
		$this->_getSourceDatas ();
		return $this->_display ();
	} //End of method
	

	//---------------------------------------------------------------------
	//get code
	protected function _initCode() {
		$this->source_file = $this->act_info ['normalized_name'] . '.php';
		$this->source_path = GALAXIA_PROCESSES . '/' . $this->proc_info ['normalized_name'] . '/code/activities';
		$this->source = $this->source_path . '/' . $this->source_file;
	} //End of method
	

	//---------------------------------------------------------------------
	//get code
	protected function _initTemplate() {
		$this->source_file = $this->act_info ['normalized_name'] . '.tpl';
		$this->source_path = GALAXIA_PROCESSES . '/' . $this->proc_info ['normalized_name'] . '/code/templates';
		$this->source = $this->source_path . '/' . $this->source_file;
	} //End of method
	

	//---------------------------------------------------------------------
	//get code
	protected function _initShared() {
		$this->source_file = 'shared.php';
		$this->source_path = GALAXIA_PROCESSES . '/' . $this->proc_info ['normalized_name'] . '/code';
		$this->source = $this->source_path . '/' . $this->source_file;
	} //End of method
	

	//---------------------------------------------------------------------
	//save source
	public function saveAction() {
		Ranchbe::checkPerm ( 'admin_process', Ranchbe::getAcl ()->getRootResource (), true );
		$source_type = $this->getRequest ()->getParam ( 'source_type' );
		$source = $this->getRequest ()->getParam ( 'source' );
		switch ($source_type) {
			case 'code' :
				$this->_initCode ();
				break;
			case 'template' :
				$this->_initTemplate ();
				break;
			case 'shared' :
				$this->_initShared ();
				break;
		}
		$fp = fopen ( $this->source, "w" );
		fwrite ( $fp, stripslashes ( $source ) );
		fclose ( $fp );
		if ($this->activityId) {
			$this->activityManager->compile_activity ( $this->pid, $this->activityId );
		}
		return $this->_forward('index');
	} //End of method


} //end of class
