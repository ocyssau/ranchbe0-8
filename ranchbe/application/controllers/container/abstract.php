<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('controllers/abstract.php');

class controllers_container_abstract extends controllers_abstract{
	protected $container; //container object
	protected $container_id; //(int)
	protected $page_id = 'containerManager'; //(string)
	protected $ifSuccessForward = array('module'=>'container','controller'=>'index','action'=>'index');
	protected $ifFailedForward = array('module'=>'container','controller'=>'index','action'=>'index');
	protected $ticket=''; //(String)

	public function init(){
		$this->error_stack =& Ranchbe::getError();

		$this->ticket = $this->getRequest()->getParam('ticket');
		$this->space_name = $this->getRequest()->getParam('space');
		if( empty( $this->space_name ) )
		$this->space_name = Ranchbe::getContext()->getProperty( 'space_name' );
		$this->page_id = $this->space_name.$this->page_id;

		$this->container_id = $this->getRequest()->getParam('container_id');
		if(is_array($this->container_id)){
			$this->container_ids = $this->container_id;
			$this->container_id  = -1; //to get a not initialized container object
		}else{
			$this->container_ids = array($this->container_id);
		}

		//$this->container = rb_container::get($this->space_name, $this->container_id); //Create no initialized instance
		$this->space =& Rb_Space::get($this->space_name);

		//Assign name to particular fields
		$this->view->assign('SPACE_NAME', $this->space_name);
		$this->view->assign('CONTAINER_NUMBER' , $this->space_name.'_number');
		$this->view->assign('CONTAINER_DESCRIPTION' , $this->space_name.'_description');
		$this->view->assign('CONTAINER_STATE' , $this->space_name.'_state');
		$this->view->assign('CONTAINER_ID' , $this->space_name.'_id');

		if($this->container_id){
			$this->view->assign('container_id', $this->container_id);
		}

		$this->_initForward();

	} //End of method

} //End of class
