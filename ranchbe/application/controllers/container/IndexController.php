<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once ('controllers/container/abstract.php');
class Container_IndexController extends controllers_container_abstract {
	protected $page_id = 'containerManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'container', 
										'controller' => 'index', 
										'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'container', 
										'controller' => 'index', 
										'action' => 'index' );

	public $elementDecorators = array(
        'ViewHelper',
        'Errors',
	    array('Description', array('tag' => 'p', 'class' => 'description')),
	    array('HtmlTag', array('tag' => 'td')),
	    array('Label', array('tag' => 'td')),
	    array(array('tr' => 'HtmlTag'), array('tag' => 'tr'))
   );
    
    public $elementDecorators2 = array(
        'ViewHelper',
        'Errors',
   		array( 'HtmlTag', array('tag' => 'label', 'class' => 'element'))
   		);
	
	//----------------------------------------------------------------------------------------------------
	protected function _getForm() {
		$this->view->dojo()->enable();
		
		$form = new ZendRb_Form();
		//$this->view->headScript()->appendFile(Ranchbe::getConfig()->js->baseurl.'qfamsHandler.js');

		$space_name = $this->space_name;
		$element = $form->addElement('ValidationTextBox','container_number', array(
				'label' => tra ( 'Number' ),
				'size' =>50,
				'value' => '',
				'required' => true,
				'parseOnLoad'=>true,
				'getDisplayedValue'=> true,
				'regExp' => Ranchbe::getConfig ()->$space_name->mask,
				'invalidMessage' => utf8_encode(Ranchbe::getConfig ()->$space_name->maskhelp),
				'filters' => array('StringTrim', 'StringToUpper')
				)
		);
		//$form->getElement('container_number')->addDecorators($this->elementDecorators );
		//var_dump($form->getElement('container_number')->getDecorators());die;

		
		
		$form->addElement('Date','forseen_close_date',array(
                    'label' => tra('forseen_close_date'),
                    'required'  => true,
					)
		);
		
		$form->addElement('Textarea', 'container_description', array(
					'label' => tra ( 'Description' ),
					'value' => 'New container',
					'trim'  => true,
					'required' => true,
					'style' => 'width: 400px;',
					'filters' => array('StringTrim', 'StringToLower'),
					'validators' => array(
									    array('NotEmpty', true),
									    array('stringLength', false, array(0,255)),
									),
					));
		
		if ($this->space_name == 'workitem') {
			$form->addElement('Selectproject','project_id', array(
					'label' => tra ( 'Project' ),
					'value' => '',
					'allowEmpty'=> false,
					'returnName'=>false,
					'advSelect'=>false,
					'required' => true,
					'multiple' => false,
					'source'=> '',
					)
			);
		}
		
		$form->addElement('Selectread','default_read_id', array(
				'label' => tra ( 'default_read_id' ),
				'value' => '',
				'allowEmpty'=> true,
				'returnName'=>false,
				'advSelect'=>false,
				'required' => false,
				'multiple' => false,
				'source'=> $this->space,
				)
		);
		
		$form->addElement('Selectprocess','default_process_id', array(
				'label' => tra ( 'default_process_id' ),
				'value' => '',
				'allowEmpty' => true,
				'returnName' => false,
				'advSelect' => false,
				'required' => false,
				'multiple' => false,
				'autocomplete' => true,
				'source'=> Rb_Container::get('workitem'),
				)
		);
		
		$form->addElement('hidden', 'container_id', array('value'=>$this->container_id ));
		$form->addElement('hidden', 'space', array('value'=>$this->space_name ));
		$form->addElement('hidden', 'ticket', array('value'=>RbView_Flood::getTicket() ));
		
		//Get fields for custom metadata
		$extendProperties = $this->container->getMetadatas ();
		foreach ( $extendProperties as $property ) {
			$element = ZendRb_Form_Element_Creator::getElement( $property );
			$form->addElement( $element );
		}
		$this->view->optionalFields = $optionalFields;
		
		return $form;		
	} //End of method
	
	
	//----------------------------------------------------------------------------------------------------
	protected function _create($values) {
		if (! is_numeric ( $values ['forseen_close_date'] ))
			$values ['forseen_close_date'] = Rb_Date::makeTimestamp ( $values ['forseen_close_date'] );
		foreach ( $values as $property => $value ) {
			$this->extend->setProperty ( $property, $value );
		}
		return $this->extend->save ();
	} //End of method
	
	//----------------------------------------------------------------------------------------------------
	protected function _edit($values) {
		if( $this->extend->getId() < 1 ){
			Ranchbe::getError()->error(tra('object is not set, you can not edit this object'));
		}
		if (! is_numeric ( $values ['forseen_close_date'] ))
			$values ['forseen_close_date'] = Rb_Date::makeTimestamp ( $values ['forseen_close_date'] );
		foreach ( $values as $property => $value ) {
			$this->extend->setProperty ( $property, $value );
		}
		return  $this->extend->save ();
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function indexAction() {
		$this->view->dojo()->enable();
		return $this->_forward ( 'get' );
		 
	} //End of method
	
	
	//---------------------------------------------------------------------
	/*!\brief
	 * Record selected container id/number/type and eventualy his project id/number in session.<br>
	 *    
	 *  Create var in session :<br>
	 *  SelectedContainer : id of the current container<br>
	 *  SelectedContainerNum : Number of the current container<br>
	 *  SelectedContainerType : Type of the current container(workitem, bookshop, cadlib, mockup)<br>
	 *  SelectedProject : id of the father project if current container is a workitem<br>
	 *  SelectedProjectNum : number of the father project if current container is a workitem<br>
	 *  objectList : List of the objects linked to current project(only if selected container is type workitem)<br>
	 *  projectInfos : Infos about current project(only if selected container is type workitem)<br>
	*/
	//    See ranchbe_setup too for assign the container num to smarty.
	public function activatecontainerAction() {
		$container = & Rb_Container::get ( $this->space_name, $this->container_id );
		$this->view->dojo()->enable();
		if (! Ranchbe::checkPerm ( 'document_get', $container, false )) {
			return $this->_cancel ();
		}
		$context = & Ranchbe::getContext ();
		//if space change re-init all context
		if ($context->getProperty ( 'space_name' ) != $this->space_name) { //init the space name
			$context->init ();
		}
		/* if select a other container that current container, re-init links
	    */
		if (! empty ( $this->container_id ) && $this->container_id != $context->container_id) {
			$context->setProperty ( 'space_name', $this->space_name );
			$context->setProperty ( 'container_id', $container->getId () );
			$context->setProperty ( 'container_number', $container->getNumber () );
			$context->setProperty ( 'container_properties', $container->getProperties () );
			
			if ($project =& $container->getFather ()) //get father
				if ($project->getId ()) { //try to init project
					//Get infos about project
					$context->setProperty ( 'project_id', $project->getId () );
					$context->setProperty ( 'project_number', $project->getNumber () );
					$context->setProperty ( 'project_properties', $project->getProperties () );
					//Get object linked to project with detail on childs objects
					$objectList = $project->getChilds ();
					if ($objectList) {
						foreach ( $objectList as $t ) {
							$object [$t ['class_id']] [] = $t;
						}
						$context->setProperty ( 'links', $object );
					}
				}
		} else {
			if (! $context->getProperty ( 'container_id' )) {
				Ranchbe::getError()->error('none selected container');
				$this->_forward ( $this->ifFailedForward ['action'], $this->ifFailedForward ['controller'], $this->ifFailedForward ['module'], $this->ifFailedForward ['params'] );
			}
		}		
		return $this->_successForward ();
	} //End of method
	
	//---------------------------------------------------------------------
	public function getAction() {
		$container = & Rb_Container::get ( $this->space_name );
		$container_view = & new Rb_Container_Alias_View ( $this->space );
		if (! Ranchbe::checkPerm ( 'get', $container, false )) {
			Ranchbe::getError()->checkErrors ();
			return $this->_forward ( 'index', 'index', 'index' );
		}
		$search = new Rb_Search_Db ( $container->getDao () );
		$filter = new RbView_Helper_Containerfilter ( $this->getRequest (), $search, $this->page_id );
		$filter->setUrl ( './container/' . $this->space_name );
		$filter->setDefault ( 'sort_field', $this->space_name . '_number' );
		$filter->setDefault ( 'sort_order', 'ASC' );
		$filter->finish ();
		//set the union for get extends properties
		$filter->setWithExtends ( $container->getDao () );
		$this->view->list = $container_view->getAll ( $filter->getParams () );
		//var_dump($container->getAllExtend());
		
		/* @TODO : les permissions pour voir nécessite de construire les objects de chaque container
		  et de verifier les permissions individuels sur chacun. Ceci peut-être couteux en processeur donc
		  on s'en passera ici pour les besoins de base.
		*/
		//Filter to display only authorized container
		/*
		  if(is_array($list_t))
		  foreach($list_t as $element ){
		    $container =& Rb_Container::get($this->space_name, $element[$this->container->getFieldName('id')]);
		    if( Ranchbe::checkPerm( 'container_document_get' , $container->AREA_ID , false) ){
		      $list[] = $element;
		    }
		  }
		  unset($list_t);
		*/
		//var_dump($list);die;
		
		$pagination = new RbView_Helper_Pagination($filter);
	    $pagination->setPagination( count($this->view->list) );
	    $pagination->setRequest( $this->getRequest() );
	    $this->view->views_helper_pagination = $pagination->fetchForm( $this->view->getEngine () );
		
		Ranchbe::getError()->checkErrors ();
		
		$this->_helper->actionStack ( 'toolbar' );
		
		// Display the template
		RbView_Menu::get ();
		RbView_Tab::get ( $this->space_name . 'Tab' )->activate ();
		$this->view->assign ( 'views_helper_filter_form', 
						$filter->fetchForm ( $this->view->getEngine (), $container ) );
		$this->view->assign ( 'PageTitle', $this->space_name . ' manager' );
	} //End of method
	
	
	//----------------------------------------------------------------------------------------------------
	public function suppressAction() {
		if (RbView_Flood::checkFlood ( $this->ticket ))
			foreach ( $this->container_ids as $container_id ) {
				$container = & Rb_Container::get ( $this->space_name, $container_id );
				if (! Ranchbe::checkPerm ( 'suppress', $container, false ))
					continue;
				else
					$container->suppress ();
			}
		return $this->_successForward ();
	} //suppress
	
	
	//----------------------------------------------------------------------------------------------------
	public function createAction() {
		Ranchbe::checkPerm ( 'create', Rb_Container::get ( $this->space_name, - 1 ), true );
		$this->_helper->viewRenderer->setNoRender(true);
		
		$this->container = Rb_Container::get ( $this->space_name, 0 ); //Create a new instance
		$this->extend = new Rb_Object_Extend ( $this->container ); //get extend container
		
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->view->PageTitle = tra ( 'Create a ' . $this->space_name );
		$this->view->form = $this->_getForm();
		$this->view->form->setAction( $this->_helper->url('create') );
		
		if ($this->space_name == 'workitem' || $this->space_name == 'bookshop') {
			$default_manager_type = 0;
		} else {
			$default_manager_type = 1;
		}
		
		$file_only = new Zend_Form_Element_Radio('file_only', array(
				'label' => tra ( 'Manager type' ),
				'order' => 0,
				'value' => $default_manager_type,
				'multiOptions'  => array(
										1=>tra ( 'File Manager' ),
										0=>tra ( 'Documents manager' )
										),
			)
		);
		$this->view->form->addElement($file_only);
		
		$this->view->form->setDefaults(array (
			'container_description' => 'New ' . $this->space_name, 
			'file_only' => $default_manager_type ,
			'forseen_close_date' => Rb_Date::formatDate( time () + Ranchbe::getConfig()->date->object->lifetime, 'dojo' ),
		));
		
		if ( $this->getRequest ()->isPost() ) {
			if ($this->view->form->isValid($this->getRequest ()->getPost())) {
				if (!RbView_Flood::checkFlood ( $this->ticket )) {return false;}
				if( $this->_create($this->view->form->getValues()) ){
					$this->view->successMessage = tra('Save is successfull');
					$this->view->form = null;
				}else{
					$this->view->failedMessage = tra('Save has failed');
				}
			} else {
				$this->view->failedMessage = tra('Save has failed');
			}
		}
		Ranchbe::getError()->checkErrors ();
		$this->render ( 'default/editform', null, true );
		return;
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function editAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		//$this->_helper->viewRenderer->setScriptAction('edit');
		
		$this->container = & Rb_Container::get ( $this->space_name, $this->container_id );
		$this->extend = new Rb_Object_Extend ( $this->container ); //get extend container
		
		//if (!$check_flood) return false;
		Ranchbe::checkPerm ( 'edit', $this->container, true );

		$this->view->PageTitle = tra ( 'Modify a ' . $this->space_name );
		$this->view->form = $this->_getForm();
		$this->view->form->setAction( $this->_helper->url('edit') );
		
		$this->view->form->setDefaults ( array (
			'container_number' => $this->container->getProperty ( 'container_number' ), 
			'container_description' => $this->container->getProperty ( 'container_description' ), 
			'project_id' => $this->container->getProperty ( 'project_id' ), 
			'default_read_id' => $this->container->getProperty ( 'default_read_id' ),
			'default_process_id' => $this->container->getProperty ( 'default_process_id' ),
			'forseen_close_date' => Rb_Date::formatDate($this->container->getProperty ( 'forseen_close_date' ) , 'dojo' ),
		));
		
		if ( $this->getRequest ()->isPost() ) {
			if ($this->view->form->isValid($this->getRequest ()->getPost())) {
				if (!RbView_Flood::checkFlood ( $this->ticket )) {return false;}
				if( $this->_edit($this->view->form->getValues()) ){
					$this->view->successMessage = tra('Save is successfull');
				}else{
					$this->view->failedMessage = tra('Save has failed');
				}
			} else {
				$this->view->failedMessage = tra('Save has failed');
			}
		}
		Ranchbe::getError()->checkErrors ();
		$this->render ( 'default/editform', null, true );
		return;
	} //End of method

} //End of class
