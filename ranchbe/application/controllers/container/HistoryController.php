<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


class Container_HistoryController extends controllers_history_abstract {
	protected $page_id = 'containerHistory'; //(string)
	protected $ifSuccessForward = array ('module' => 'container', 'controller' => 'history', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'container', 'controller' => 'history', 'action' => 'index' );
	
	public function init() {
		parent::init ();
		$this->space_name = $this->getRequest ()->getParam ( 'space' );
		if (empty ( $this->space_name ) && Ranchbe::getContext ()->getProperty ( 'space_name' ))
			$this->space_name = Ranchbe::getContext ()->getProperty ( 'space_name' );
		$this->object = & Rb_Container::get ( $this->space_name ); //Create new manager
		$this->history = new Rb_History ( $this->object );
		if (! $this->object_id)
			$this->object_id = $this->getRequest ()->getParam ( 'container_id' );
		$this->view->assign ( 'PageTitle', $this->object->getSpaceName () . ' history' );
	} //End of method
} //End of class

//@TODO: REVOIR FILTRE: Lors du reset, affiche historique de tous plutot que l'historique du conteneur selectionne
