<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('controllers/perms/abstract.php');
class Container_PermsController extends controllers_perms_abstract{
	protected $page_id='containerPerms'; //(string)
	protected $ifSuccessForward = array('module'=>'container','controller'=>'perms','action'=>'index');
	protected $ifFailedForward = array('module'=>'container','controller'=>'perms','action'=>'index');

	public function init(){
		$this->error_stack =& Ranchbe::getError();

		$this->ticket = $this->view->ticket;
		$this->space_name = $this->getRequest()->getParam('space');
		if( empty( $this->space_name ) )
			$this->space_name = Ranchbe::getContext()->getProperty( 'space_name' );
		$this->page_id = $this->space_name.$this->page_id;

		$this->_initForward();

		$this->object_id = $this->getRequest()->getParam('object_id');

		$container = Rb_Container::get($this->space_name, $this->object_id);

		$this->role_id = $this->getRequest()->getParam('role_id');
		
		$this->resource_id = $this->getRequest()->getParam('resource_id');
		if($this->resource_id == 3){
			switch($this->space_name){
				case 'bookshop':
					$this->resource_id = 3;
					break;
				case 'cadlib':
					$this->resource_id = 4;
					break;
				case 'mockup':
					$this->resource_id = 5;
					break;
				case 'workitem':
					$this->resource_id = 6;
					break;
			}
		}
		
		if( empty($this->resource_id) ){
			$this->resource_id = $container->getResourceId();
		}
		if(empty($this->resource_id)) {
			print 'none ressource<br />';die;
		}

		if($this->object_id){
			$this->view->resource_name = $container->getNumber();
		}else{
			$this->view->resource_name = tra( $this->space_name . 's' );
		}

		$this->view->SPACE_NAME = $this->space_name;
		$this->view->samecontroller = './container/perms'; //important: is first assign
	} //End of method

} //End of class
