<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('controllers/container/abstract.php');
class Container_ConsultController extends controllers_container_abstract{

protected $page_id='consultContainer'; //(string)
protected $ifSuccessForward = array('module'=>'container','controller'=>'consult','action'=>'index');
protected $ifFailedForward = array('module'=>'container','controller'=>'consult','action'=>'index');

public function init(){
  return parent::init();
} //End of method

//---------------------------------------------------------------------
public function indexAction(){
  $this->getAction();
} //End of method

//---------------------------------------------------------------------
public function getAction(){
} //End of class

} //End of class
