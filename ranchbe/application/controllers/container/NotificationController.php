<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');
class Container_NotificationController extends controllers_abstract {
	
	protected $page_id = 'containerNotification'; //(string)
	protected $ifSuccessForward = array ('module' => 'container', 'controller' => 'notification', 'action' => 'get' );
	protected $ifFailedForward = array ('module' => 'container', 'controller' => 'notification', 'action' => 'get' );
	
	public function init() {
		//$this->error_stack = & Ranchbe::getError();
		
		$this->ticket = $this->getRequest ()->getParam ( 'ticket' );
		$this->space_name = $this->getRequest ()->getParam ( 'space' );
		$this->container_id = $this->getRequest ()->getParam ( 'container_id' );
		$this->space = & Rb_Space::get ( $this->space_name );
		$this->notification_id = $this->getRequest ()->getParam ( 'notification_id' );
		if (is_array ( $this->notification_id )) {
			$this->notification_ids = $this->notification_id;
			$this->notification_id = - 1; //to get a not initialized container object
		} else if ($this->notification_id) {
			$this->notification_ids = array ($this->notification_id );
		} else {
			$this->notification_ids = array ();
		}
		
		//Assign name to particular fields
		$this->view->assign ( 'SPACE_NAME', $this->space_name );
		$this->view->assign ( 'CONTAINER_ID', $this->space_name . '_id' );
		
		if ($this->notification_id) {
			$this->view->assign ( 'notification_id', $this->notification_id );
		}
		$this->view->assign ( 'container_id', $this->container_id );
		$this->_initForward ();
		$cancel = $this->getRequest ()->getParam ( 'cancel' );
		if ($cancel)
			$this->_cancel ();
		Ranchbe::checkPerm ( 'get', Rb_Container::get ( $this->space_name, $this->container_id ), true );
		
		//scripts for quickform_advmultiselect
    	$this->view->headScript()->appendFile(Ranchbe::getConfig()->js->baseurl.'qfamsHandler.js');
		return true;
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		Ranchbe::getLayout()->setLayout('popup');
		$this->view->list = Rb_Container_Notification::get ( $this->space_name )
							->getAll ( $this->container_id, 
								Rb_User::getCurrentUser ()->getId () );
		$i = 0;
		if ($list)
			foreach ( $list as $notification ) {
				if (! $notification ['Rb_Condition']) {
					$i ++;
					continue;
				}
				$condition = unserialize ( $notification ['Rb_Condition'] );
				$tests = $condition->getTests ();
				foreach ( $tests as $test ) {
					$list [$i] ['test'] [] = $test;
				}
				$i ++;
			}
		Ranchbe::getError()->checkErrors ();
		$this->view->sameurl = './container/notification/get';
		$this->view->PageTitle = tra ( 'My notifications' );
		return true;
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function editAction() {
		$this->notification = Rb_Container_Notification::get ( $this->space_name, $this->notification_id );
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		//$form = new HTML_QuickForm('createNotification', 'post', $this->actionUrl('create') );
		$form = new RbView_Pear_Html_QuickForm ( 'editNotification', 'post', $this->actionUrl ( 'edit' ) );
		$this->_initFormForward ( $form ); //init the hidden param to set the forward parameter
		
		$form->addElement ( 'header', 'editheader', tra ( 'Edit notification' ) );
		//Set defaults values of elements if modify request
		$Infos = Rb_Container_Notification::get ( $this->space_name, $this->notification_id )->getProperties ();		
		$condition = & Rb_Container_Notification::get ( $this->space_name, $this->notification_id )->getCondition ();
		
		if ($condition)
			foreach ( $condition->getTests () as $test ) {
				if ($test [1] == 'category_id')
					$category_ids [] = $test [2];
				else if ($test [1] == 'doctype_id')
					$doctype_ids [] = $test [2];
			}
		
		$form->setDefaults ( array (
						'submit' => 'modify', 
						'event' => $Infos ['event'], 
						'category_id' => $category_ids, 
						'doctype_id' => $doctype_ids ) );
		
		//Add hidden fields
		$form->addElement ( 'hidden', 'notification_id', $this->notification_id );
		$this->container_id = $this->notification->getProperty ( 'container_id' );
		/*can not edit the event
	  	$form->addElement('select', 'event', tra('On event'), 
                    array(  'doc_post_update'=>tra('document update'), 
                            'doc_post_store'=>tra('document creation'), 
                            'doc_post_checkout'=>tra('document checkout'),
                            'doc_post_checkin'=>tra('document checkin'),
                            'doc_post_reset'=>tra('document checkout reset'),
                            'doc_post_suppress'=>tra('document suppression'),
                            'doc_post_move'=>tra('document move')
                    ));
		  */
		return $this->_finishForm ( $form, 'edit' );
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function createAction() {
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'createNotification', 'post', $this->actionUrl ( 'create' ) );
		$this->_initFormForward ( $form ); //init the hidden param to set the forward parameter
		$form->addElement ( 'header', 'editheader', tra ( 'Define a new notification' ) );
		
		$e = Rb_Container_Notification::get ( $this->space_name )
				->getAll ( $this->container_id, Rb_User::getCurrentUser ()->getId () );
		$myEvents = array ();
		foreach ( $e as $event ) {
			$myEvents [$event ['event']] = tra ( $event ['event'] );
		}
		
		$events = array (
			'doc_post_update' => tra ( 'doc_post_update' ), 
			'doc_post_store' => tra ( 'doc_post_store' ), 
			'doc_post_checkout' => tra ( 'doc_post_checkout' ), 
			'doc_post_checkin' => tra ( 'doc_post_checkin' ), 
			'doc_post_reset' => tra ( 'doc_post_reset' ), 
			'doc_post_suppress' => tra ( 'doc_post_suppress' ), 
			'doc_post_move' => tra ( 'doc_post_move' ) );
		$p = array ('property_name' => 'event', 
					'property_description' => tra ( 'On event' ), 
					'default_value' => array (), 
					'is_multiple' => true, 
					'return_name' => false, 
					'property_length' => 5, 
					'adv_select' => true );
		construct_select ( array_diff ( $events, $myEvents ), $p, $form );
		return $this->_finishForm ( $form, 'create' );
	} //End of method
	
	
	//----------------------------------------------------------------------------------------------------
	public function suppressAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		if (RbView_Flood::checkFlood ( $this->ticket ))
			foreach ( $this->notification_ids as $notification_id ) {
				$notification = & Rb_Container_Notification::get ( $this->space_name, $notification_id );
				if (Rb_User::getCurrentUser ()->getId () == $notification->getProperty ( 'user_id' )) {
					$notification->suppress ();
				} else
					continue;
			}
		return $this->_successForward();
	} //suppress
	

	//----------------------------------------------------------------------------------------------------
	public function _create($values) {
		if ($values ['category_id']) {
			$condition = new Rb_Condition ( 'or' );
			foreach ( $values ['category_id'] as $category_id ) {
				$condition->setTest ( 'category_id', '==', $category_id );
			}
			//$this->notification->setCondition($condition);
		}
		
		if ($values ['doctype_id']) {
			if (! $condition)
				$condition = new Rb_Condition ( 'or' );
			foreach ( $values ['doctype_id'] as $doctype_id ) {
				$condition->setTest ( 'doctype_id', '==', $doctype_id );
			}
			//$this->notification->setCondition($condition);
		}

		if(is_array($values ['event']))
		foreach ( $values ['event'] as $event ) {
			if (empty ( $event ))
				continue;
			$data ['event'] = $event;
			$notification = new Rb_Container_Notification ( $this->space, 0 );
			if ($condition)
				$notification->setCondition ( $condition );
			$data ['container_id'] = $values ['container_id'];
			$data ['user_id'] = Rb_User::getCurrentUser ()->getId ();
			foreach ( $data as $property => $value ) {
				$notification->setProperty ( $property, $value );
			}
			$notification->save ();
		}
	
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function _modify($values) {
		if ($values ['category_id']) {
			$condition = new Rb_Condition ( 'or' );
			foreach ( $values ['category_id'] as $category_id ) {
				$condition->setTest ( 'category_id', '==', $category_id );
			}
			$this->notification->setCondition ( $condition );
		} else {
			$this->notification->unsetCondition ();
		}
		
		if ($values ['doctype_id']) {
			if (! $condition)
				$condition = new Rb_Condition ( 'or' );
			foreach ( $values ['doctype_id'] as $doctype_id ) {
				$condition->setTest ( 'doctype_id', '==', $doctype_id );
			}
			$this->notification->setCondition ( $condition );
		}
		
		$data ['event'] = $values ['event'];
		foreach ( $data as $property => $value ) {
			$this->notification->setProperty ( $property, $value );
		}
		
		return $this->notification->save ();
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	protected function _finishForm(HTML_QuickForm &$form, $mode) {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_quickFormRendererSet ( $form );
		$this->_helper->viewRenderer->setNoRender(true);
		
		if ($advOptions) {
			$p = array ('property_name' => 'category_id', 'property_description' => tra ( 'For selected categories only<br />(Let blank for all categories)' ), 'default_value' => array (), 'is_multiple' => true, 'return_name' => false, 'property_length' => 5, 'adv_select' => true );
			construct_select_category ( $p, $form, Rb_Container::get ( $this->space_name ) );
			
			$p = array ('property_name' => 'doctype_id', 'property_description' => tra ( 'For selected document type only<br />(Let blank for all doctypes)' ), 'default_value' => array (), 'is_multiple' => true, 'return_name' => false, 'property_length' => 5, 'adv_select' => true );
			construct_select_doctype ( $p, $form, Rb_Doctype::get () );
		}
		
		/*
		  //Get extends properties
		  $metadatas = Rb_Container::get($this->space_name, $this->container_id)->getMetadatas();
		  $selectMeta = array('document_number'=>tra('document_number'));
		  foreach($metadatas as $metadata ){
		    $selectMeta[$metadata['property_id']] = $metadata['property_description'];
		  }
		  $cond = null;
		  $cond[] = &HTML_QuickForm::createElement('select', null, null, $selectMeta);
		  $cond[] = &HTML_QuickForm::createElement('select', null, null, 
		                                array('1'=>tra('equal to'),
		                                      '2'=>tra('contains'),
		                                      '3'=>tra('superior to'),
		                                      '4'=>tra('inferior to')
		                                ));
		
		  $cond[] = &HTML_QuickForm::createElement('text', null, null );
		  $form->addGroup($cond, 'condition1', tra('Other condition').' 1');
		  $form->addGroup($cond, 'condition2', tra('Other condition').' 2');
		  $form->addGroup($cond, 'condition3', tra('Other condition').' 3');
		  */
		
		//Add hidden fields
		$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'container_id', $this->container_id );
		
		$form->addElement ( 'submit', 'submit', tra ( 'Validate' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		
		// Try to validate the form
		if ($form->validate ()) {
			//$form->freeze(); //and freeze it
			// Form is validated, then processes the create request
			if ($mode == 'create') {
				if (RbView_Flood::checkFlood ( $this->ticket ))
					$form->process ( array ($this, '_create' ), true );
			} else if ($mode == 'edit') {
				$form->process ( array ($this, '_modify' ), true );
			}
			return $this->_successForward();
		} //End of validate form
		
		/*
		  //Set the renderer for display QuickForm form in a smarty template
		  $this->_quickFormRendererSet($form);
		  */
		
		$form->display ();
	} //End of method


} //End of class
