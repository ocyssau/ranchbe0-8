<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/metadatas/abstract.php');
class Container_MetadatasController extends Metadatas_abstract {
	protected $page_id = 'contmetadataManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'container', 'controller' => 'metadatas', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'container', 'controller' => 'metadatas', 'action' => 'index' );
	
	public function init() {
		parent::init ();
		$this->aclobject = Rb_Container::get ( $this->space_name ); //Create new manager
		$this->space = & $this->aclobject->getSpace ();
		$this->metadata = new Rb_Metadata_Dictionary ();
		$this->metadata->setExtendTable ( Rb_Object_Extend::getExtendTable ( $this->aclobject->getDao () ) );
		$this->view->module = 'container';
		Ranchbe::getLayout ()->setLayout ( 'popup' );
	} //End of method
	
	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward ( 'get' );
	} //End of method
	
} //End of class

