<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once ('controllers/container/abstract.php');
class Container_CategoriesController extends controllers_container_abstract {
	protected $container; //container object
	protected $container_id; //(int)
	protected $page_id = 'containerManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'container', 'controller' => 'categories', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'container', 'controller' => 'categories', 'action' => 'index' );
	
	public function init() {
		parent::init ();
		$this->category = new Rb_Category ( );
		$this->categoryDoctypeLink = new Rb_Category_Relation_Doctype ( );
		$this->categoryMetadataLink = new Rb_Category_Relation_Metadata ( );
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		//scripts for quickform_advmultiselect
    	$this->view->headScript()->appendFile(Ranchbe::getConfig()->js->baseurl.'qfamsHandler.js');
	} //End of method
	
	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward ( 'get' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		$properties = $this->categoryMetadataLink->getMetadatas ( 0, 
			array ('select' => 
				array ('link_id', 
						'parent_id', 
						'class_id', 
						'property_id', 
						'property_name', 
						'property_description' ) ) );
		
		$doctypes = $this->categoryDoctypeLink->getDoctypes ( 0, 
			array ('select' => 
				array ('link_id', 
						'parent_id', 
						'class_id', 
						'doctype_id', 
						'doctype_number', 
						'doctype_description' ) ) );
		
		$categories = Rb_Category::get ()->getAll ();
		
		//Associate both array $cat_prop_list and $cat_doctype_list in one $list multi-dimensional array
		$i = 0;
		$list = array ();
		foreach ( $categories as $category ) {
			$category_id = $category ['category_id'];
			$list [$i] = $category;
			foreach ( $properties as $property ) {
				if ($property ['parent_id'] == $category_id)
					$list [$i] ['properties'] [] = $property;
			}
			foreach ( $doctypes as $doctype ) {
				if ($doctype ['parent_id'] == $category_id)
					$list [$i] ['doctypes'] [] = $doctype;
			}
			$i ++;
		}
		
		$this->view->list = $list;
		$this->error_stack->checkErrors ();
		
		// Display the template
		RbView_Menu::get ();
		RbView_Tab::get ( $this->space_name . 'Tab' )->activate ();
		$this->view->assign ( 'sameurl', './container/categories/get' ); //important: is first assign
		//$this->view->assign('views_helper_filter_form' , $filter->fetchForm($this->view, $this->container) ); //generate code for the filter form
		//$this->view->assign('views_helper_pagination' , $pagination->fetchForm($this->view) ); //generate code for the filter form
		$this->view->assign ( 'PageTitle', 'Categories' );
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function suppressAction() {
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		if (RbView_Flood::checkFlood ( $this->ticket )) {
			$this->category_ids = $this->getRequest ()->getParam ( 'category_id' );
			if (! empty ( $this->category_ids )) {
				foreach ( $this->category_ids as $id ) {
					$categorie = Rb_Category::get ( $id )->suppress ();
				}
			}
		}
		
		$this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
	
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function linkpropertyAction() {
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$property_ids = $this->getRequest ()->getParam ( 'property_id' );
		$this->category_id = $this->getRequest ()->getParam ( 'category_id' );
		$this->category = & Rb_Category::get ( $this->category_id );
		$this->_helper->viewRenderer->setNoRender();
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'selectMetadata', 
										'post', $this->actionUrl ( 'linkproperty' ) );
		$form->addElement ( 'header', 'title1', tra ( 'Add a property to category' ) . 
								': ' . $this->category->getProperty ( 'category_number' ) );
		
		$form->addElement ( 'hidden', 'category_id', $this->category_id );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', $this->view->ticket);
		
		//Get the current property
		$_default = $this->categoryMetadataLink->getMetadatas ( $this->category_id );
		foreach ( $_default as $d ) {
			$default [] = $d ['property_id'];
		}
		unset ( $_default );
		$p = array ('property_name' => 'property_id', 'property_description' => tra ( 'Select property to add' ), 'default_value' => $default, 'is_multiple' => true, 'return_name' => false, 'property_length' => 5, 'adv_select' => true );
		construct_select_property ( $p, $form, Rb_Document::get ( $this->space_name )->getDao () );
		
		if (isset ( $validate )) {
			if ($form->validate ()) { //Validate the form...
				//Add the property if necessary
				if (is_array ( $property_ids ))
					foreach ( $property_ids as $property_id ) {
						if (is_array ( $default )) { //Previous selection exist
							if (! in_array ( $property_id, $default )) {
								//$this->categoryMetadataLink->addLink( array('category_id'=>$this->category_id, 'property_id'=>$property_id ) );
								$link = new Rb_Object_Relation ( 0 );
								$link->setParent ( $this->category_id );
								$link->setChild ( $property_id );
								$link->save ();
							}
						} else { //there is no previous selection, add all
							//$this->categoryMetadataLink->addLink( array('category_id'=>$this->category_id, 'property_id'=>$property_id ) );
							$link = new Rb_Object_Relation ( 0 );
							$link->setParent ( $this->category_id );
							$link->setChild ( $property_id );
							$link->save ();
						}
					}
					//Suppress the properties if necessary
				if (is_array ( $default ))
					foreach ( $default as $property_id ) {
						if (is_array ( $property_ids )) { //Selection exist
							if (! in_array ( $property_id, $property_ids )) {
								//$this->categoryMetadataLink->suppressLink( $this->categoryMetadataLink->getLinkId($this->category_id,$property_id) );
								$this->categoryMetadataLink->suppressLink ( $this->category_id, $property_id );
							}
						} else { //its a empty selection, suppress all
							//$this->categoryMetadataLink->suppressLink( $this->categoryMetadataLink->getLinkId($this->category_id,$property_id) );
							$this->categoryMetadataLink->suppressLink ( $this->category_id, $property_id );
						}
					}
			}
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			$form->display ();
			return;
		}
		$this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
	} //End of method

	//----------------------------------------------------------------------------------------------------
	public function unlinkpropertyAction() {
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		$link_id = $this->getRequest ()->getParam ( 'link_id' );
		
		if ($link_id) {
			$relation = new Rb_Object_Relation ( $link_id );
			$relation->suppress ();
		}
		
		$this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
	} //End of method

	//----------------------------------------------------------------------------------------------------
	public function linkdoctypeAction() {
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$doctype_ids = $this->getRequest ()->getParam ( 'doctype_id' );
		$this->category_id = $this->getRequest ()->getParam ( 'category_id' );
		$this->category = & Rb_Category::get ( $this->category_id );
		$this->_helper->viewRenderer->setNoRender();
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = & new RbView_Pear_Html_QuickForm ( 'selectDoctype', 'POST', $this->actionUrl ( 'linkdoctype' ) ); //Construct the form with QuickForm lib
		$form->addElement ( 'header', 'title1', tra ( 'Add a doctype to category' ) . ': ' . $this->category->getProperty ( 'category_number' ) );
		
		$form->addElement ( 'hidden', 'category_id', $this->category_id );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', $this->view->ticket );
		
		//Get the current doctypes
		//$default_doctypes = $this->categoryDoctypeLink->getLinks($this->category_id);
		$_default = $this->categoryDoctypeLink->getDoctypes ( $this->category_id );
		if (is_array ( $_default ))
			foreach ( $_default as $d ) {
				$default [] = $d ['doctype_id'];
			}
		unset ( $_default );
		$p = array (
			'property_name' => 'doctype_id', 
			'property_description' => tra ( 'Select property to add' ), 
			'default_value' => $default, 
			'is_multiple' => true, 
			'return_name' => false, 
			'property_length' => 5, 
			'adv_select' => true );
		construct_select_doctype ( $p, $form, Rb_Doctype::get ( 0 ) );
		
		if ($validate) {
			if ($form->validate ()) { //Validate the form...
				//Add the doctypes if necessary
				if (is_array ( $doctype_ids ))
					foreach ( $doctype_ids as $doctype_id ) {
						if (is_array ( $default )) { //Previous selection exist
							if (! in_array ( $doctype_id, $default )) {
								//$this->categoryDoctypeLink->addLink( array('category_id'=>$this->category_id, 'doctype_id'=>$doctype_id ) );
								$link = new Rb_Object_Relation ( 0 );
								$link->setParent ( $this->category_id );
								$link->setChild ( $doctype_id );
								$link->save ();
							}
						} else { //there is no previous selection, add all
							//$this->categoryDoctypeLink->addLink( array('category_id'=>$this->category_id, 'doctype_id'=>$doctype_id ) );
							$link = new Rb_Object_Relation ( 0 );
							$link->setParent ( $this->category_id );
							$link->setChild ( $doctype_id );
							$link->save ();
						}
					}
					//Suppress the doctypes if necessary
				if (is_array ( $default ))
					foreach ( $default as $doctype_id ) {
						if (is_array ( $doctype_ids )) { //Selection exist
							if (! in_array ( $doctype_id, $doctype_ids )) {
								//$this->categoryDoctypeLink->suppressLink( $this->categoryDoctypeLink->getLinkId($this->category_id,$doctype_id) );
								$this->categoryDoctypeLink->suppressLink ( $this->category_id, $doctype_id );
							}
						} else { //its a empty selection, suppress all
							//$this->categoryDoctypeLink->suppressLink( $this->categoryDoctypeLink->getLinkId($this->category_id,$doctype_id) );
							$this->categoryDoctypeLink->suppressLink ( $this->category_id, $doctype_id );
						}
					}
			}
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			$form->display ();
			return;
		}
		
		$this->_forward ( $this->ifSuccessForward ['action'], 
						$this->ifSuccessForward ['controller'], 
						$this->ifSuccessForward ['module'], 
						$this->ifSuccessForward ['params'] );
	} //End of method

	//----------------------------------------------------------------------------------------------------
	public function unlinkdoctypeAction() {
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		$link_id = $this->getRequest ()->getParam ( 'link_id' );
		
		if ($link_id) {
			$relation = new Rb_Object_Relation ( $link_id );
			$relation->suppress ();
		}
		
		$this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
	} //End of method

	//----------------------------------------------------------------------------------------------------
	public function createAction() {
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
			
		//Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'createCategory', 
											'post', $this->actionUrl ( 'create' ) );
		$form->addElement ( 'header', 'createheader', tra ( 'Create a category' ) );
		$form->setDefaults ( array ('action' => 'create' ) );
		$this->_finishEditForm ( $form, array(), 'create' );
	} //End of method
	
	//----------------------------------------------------------------------------------------------------
	public function editAction() {
		
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		$this->category_id = $this->getRequest ()->getParam ( 'category_id' );
		$this->category = & Rb_Category::get ( $this->category_id );
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'editCategory', 'post', $this->actionUrl ( 'edit' ) );
		$form->addElement ( 'header', 'editheader', tra ( 'Modify a category' ) );
		
		//Set defaults values of elements if modify request
		$Infos = $this->category->getProperties ();
		
		$form->setDefaults ( array ('category_number' => $Infos ['category_number'], 'category_description' => $Infos ['category_description'], 'action' => 'modify' ) );
		
		//Add hidden fields
		$form->addElement ( 'hidden', 'category_id', $this->category_id );
		
		$this->_finishEditForm ( $form, $Infos, 'edit' );
	} //End of method
	
	//----------------------------------------------------------------------------------------------------
	protected function _finishEditForm(&$form, $Infos, $mode) {
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
		
		//Add form elements
		$form->addElement ( 'text', 'category_number', tra ( 'Name' ) );
		$form->addElement ( 'text', 'category_description', tra ( 'Description' ) );
		
		//Add submit button
		$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		
		//Add validation rules to check input data
		$form->addRule ( 'category_number', tra ( 'Name is required' ), 'required' );
		$form->addRule ( 'category_description', tra ( 'Description is required' ), 'required' );
		
		// applies new filters to the element values
		$form->applyFilter ( '__ALL__', 'trim' );
		
		// Try to validate the form
		//if ($_REQUEST['action'] == 'create' || $_REQUEST['action'] == 'modify')
		if ($form->validate ()) {
			//$form->freeze(); //and freeze it
			
			//Re-set value for display correct date in freeze form
			$form->setConstants ( array (
				'fsCloseDate' => $form->getSubmitValue ( 'fsCloseDate' ) ) );
			
			// Form is validated, then processes the create request
			if (RbView_Flood::checkFlood ( $this->ticket )) {
				if ($mode == 'create') {
					$form->process ( array ($this, '_create' ), true );
				} else if ($mode == 'edit') {
					$form->process ( array ($this, '_modify' ), true );
				}
			}
			
			$this->_forward ( $this->ifSuccessForward ['action'], 
								$this->ifSuccessForward ['controller'], 
								$this->ifSuccessForward ['module'], 
								$this->ifSuccessForward ['params'] );
			return;
		} //End of validate form
		$this->_helper->viewRenderer->setNoRender();
		$this->error_stack->checkErrors ();
		$form->display ();
	} //End of method

	//----------------------------------------------------------------------------------------------------
	public function _create($values) {
		$categorie = new Rb_Category ( 0 );
		foreach ( $values as $name => $val ) {
			$categorie->setProperty ( $name, $val );
		}
		$categorie->save ();
	} //End of function

	//----------------------------------------------------------------------------------------------------
	public function _modify($values) {
		foreach ( $values as $name => $val ) {
			$this->category->setProperty ( $name, $val );
		}
		$this->category->save ();
	} //End of function

} //End of class
