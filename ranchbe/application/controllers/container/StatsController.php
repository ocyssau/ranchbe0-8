<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Ranchbe is distributed in the hope that it will be useful,                |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/container/abstract.php');
class Container_StatsController extends controllers_container_abstract {
	protected $page_id = 'containerStats'; //(string)
	protected $ifSuccessForward = array ('module' => 'container', 
									'controller' => 'index', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'container', 
									'controller' => 'index', 'action' => 'index' );
	public $space; //public and use by stats sctipts
	public $container;
	public $container_id; //public and use by stats sctipts
	public $container_ids; //public and use by stats sctipts

	public function init() {
		parent::init ();
		Zend_Registry::set ( 'space', $this->space );
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
		$this->_forward ('get');
	} //End of method
	

	//-------------------------------------------------------------------------
	public function getAction() {
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();

		//construct the list of stats scrpits
		$path_queries_scripts = Ranchbe::getConfig()->path->scripts->stats;
		
		
		$container = Rb_Container::get($this->space_name, $this->container_id);
		
		//Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'selectMetadata', 
													'post', $this->actionUrl ( 'get' ) );
		$this->_initFormForward ( $form ); //init the hidden param to set the forward parameter
		$form->addElement ( 'header', 'PageTitle', tra ( 'Stats of ' ) . $container->getName() );
		
		$form->addElement ( 'hidden', 'container_id', $this->container_id );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', $this->view->ticket );
		
		$files = glob ( $path_queries_scripts . '/stats_query_*.php' );
		foreach ( $files as $filename )
			$list [basename ( $filename )] = substr ( basename ( basename ( $filename ), '.php' ), 12 );
		$select = & $form->addElement ( 'select', 'statistics_queries', tra ( 'Queries' ), $list );
		$select->setSize ( 10 );
		$select->setMultiple ( true );
		
		if ($form->validate ()) { //Validate the form...
			//if(!$check_flood) break;
			$graphs = array ();
			foreach ( $form->getElementValue ( 'statistics_queries' ) as $script ) {
				require $path_queries_scripts . '/' . basename($script);
				$result = call_user_func ( basename ( $script, '.php' ), $container );
				if (is_array ( $result ))
					$graphs = array_merge ( $result, $graphs );
			}
		}
		
		$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		$form->applyFilter ( '__ALL__', 'trim' );
		$this->view->assign ( 'select_form', $form->toHtml () );
		
		//Assign name to particular fields
		$this->view->assign ( 'graphs', $graphs );
		
		// Active the tab
		RbView_Menu::get ();
		RbView_Tab::get ( $this->space_name . 'Tab' )->activate ();
		return;
	} //End of function


} //End of class
