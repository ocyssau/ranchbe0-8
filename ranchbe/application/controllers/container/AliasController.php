<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/container/abstract.php');
class Container_AliasController extends controllers_container_abstract {
	protected $container; //container object
	protected $aliasdao;
	protected $container_id; //(int)
	protected $page_id = 'aliascontainerManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'container', 'controller' => 'index', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'container', 'controller' => 'index', 'action' => 'index' );

	//----------------------------------------------------------------------------------------------------
	protected function _getForm() {
		$this->view->dojo()->enable();
		$form = new ZendRb_Form();
		$form->addElement ( 'hidden', 'ticket', array('value'=>RbView_Flood::getTicket () ));
		$form->addElement ( 'hidden', 'page_id', array('value'=>$this->pageId) );
		$form->addElement ( 'hidden', 'space', array('value'=>$this->space_name) );
		
		$space_name = $this->space_name;
		$form->addElement('ValidationTextBox','container_number', array(
				'label' => tra ( 'Number' ),
				'size' =>50,
				'value' => '',
				'required' => true,
				'propercase' => true,
				'regExp' => Ranchbe::getConfig ()->$space_name->mask,
				'invalidMessage' => utf8_encode(Ranchbe::getConfig ()->$space_name->maskhelp),
				'filters' => array('StringTrim', 'StringToUpper'),
				)
		);
		
		$form->addElement('Textarea', 'container_description', array(
					'label' => tra ( 'Description' ),
					'value' => 'New alias',
					'trim'  => true,
					'required' => true,
					'style' => 'width: 400px;',
					'filters' => array('StringTrim', 'StringToLower'),
					'validators' => array(
									    array('NotEmpty', true),
									    array('stringLength', false, array(0,255)),
									),
					));
		
		$form->addElement('SelectContainer','container_id', array(
					'label' => tra ( 'Alias of' ),
					'value' => $this->container_id,
					'allowEmpty'=> false,
					'returnName'=>false,
					'advSelect'=>false,
					'required' => true,
					'multiple' => false,
					'source'=> Rb_Container::get ( $this->space_name ),
					)
			);
		
		return $form;
	} //End of method
	
	//----------------------------------------------------------------------------------------------------
	protected function _create($values) {
		$values ['container_number'] = $values ['container_number'] . '_alias';
		foreach ( $values as $property => $value ) {
			$this->aliasdao->setProperty ( $property, $value );
		}
		return $this->aliasdao->save ();
	} //End of method
	
	//----------------------------------------------------------------------------------------------------
	protected function _edit($values) {
		foreach ( $values as $property => $value ) {
			$this->aliasdao->setProperty ( $property, $value );
		}
		return $this->aliasdao->save ();
	} //End of method
	
	//----------------------------------------------------------------------------------------------------
	public function init() {
		parent::init ();
		Ranchbe::getLayout ()->setLayout ( 'popup' );
	} //End of method

	//---------------------------------------------------------------------
	public function suppressAction() {
		$this->alias_ids = $this->getRequest ()->getParam ( 'alias_id' );
		if (! is_array ( $this->alias_ids ))
			$this->alias_ids = array ($this->alias_ids );
		if (empty ( $this->alias_ids )) {
			$this->error_stack->push ( Rb_Error::ERROR, array (), 'You must select at least one item' );
		} else {
			if (RbView_Flood::checkFlood ( $this->ticket ))
				foreach ( $this->alias_ids as $alias_id ) {
					$aliasdao = new Rb_Container_Alias_Dao ( $this->space, $alias_id );
					$container_id = $aliasdao->getProperty ( 'container_id' );
					$container = & Rb_Container::get ( $this->space_name, $container_id ); //Create new manager
					if (! Ranchbe::checkPerm ( 'admin_container_alias', $container->getFather () )) {
						$this->error_stack->push ( Rb_Error::ERROR, array (), 'You must select at least one item' );
						continue;
					}
					$aliasdao->suppress ( $alias_id );
				} //loop
		}
		$this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
	} //End of method

	//---------------------------------------------------------------------
	public function editAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		
		$this->alias_id = $this->getRequest ()->getParam ( 'alias_id' );
		if (empty ( $this->alias_id )) {
			Ranchbe::getError()->error('You must select at least one item');
			return $this->_cancel ();
		}
		
		$this->aliasdao = new Rb_Container_Alias_Dao ( $this->space, $this->alias_id );
		$this->container_id = $this->aliasdao->getProperty ( 'container_id' );
		$this->container = & Rb_Container::get ( $this->space_name, $this->container_id );
		if (! Ranchbe::checkPerm ( 'admin_container_alias', $this->container->getFather () )) {
			return $this->_cancel ();
		}
		
		$this->view->PageTitle = tra ( 'Edit container alias' ) . ' ' . $this->container->getName();
		
		//Create the form
		$this->view->form = $this->_getForm();
		$this->view->form->setAction( $this->_helper->url('edit') );
		$this->view->form->addElement( 'hidden', 'alias_id', array('value'=>$this->alias_id ) );
		
		$this->view->form->setDefaults ( array (
			'container_number' => $this->aliasdao->getProperty ( 'container_number' ), 
			'container_description' => $this->aliasdao->getProperty ( 'container_description' ), 
			'container_id' => $this->container_id ) );
		
		if ( $this->getRequest ()->isPost() ) {
			if ($this->view->form->isValid($this->getRequest ()->getPost())) {
				if (!RbView_Flood::checkFlood ( $this->ticket )) {return false;}
				if( $this->_edit($this->view->form->getValues()) ){
					$this->view->successMessage = tra('Save is successfull');
				}else{
					$this->view->failedMessage = tra('Save has failed');
				}
			} else {
				$this->view->failedMessage = tra('Save has failed');
			}
		}
		
		$this->render ( 'default/editform', null, true );
		Ranchbe::getError()->checkErrors ();
		return;
		
	} //End of method

	//---------------------------------------------------------------------
	public function createAction() {
		$this->aliasdao = new Rb_Container_Alias_Dao ( $this->space, 0 );
		$this->container_id = $this->getRequest ()->getParam ( container_id );
		$this->container = & Rb_Container::get ( $this->space_name, $this->container_id );
		
		$this->view->PageTitle = tra ( 'Create a container alias' );
		
		//Create the form
		if ($this->container_id) {
			if (! Ranchbe::checkPerm ( 'admin_container_alias', $this->container->getFather () )) {
				return $this->_cancel ();
			}
		}
		
		$this->view->form = $this->_getForm();
		$this->view->form->setAction( $this->_helper->url('create') );
		$this->view->form->addElement( 'hidden', 'action', array('value'=>'createAlias' ) );
		
		if ( $this->getRequest ()->isPost() ) {
			if ($this->view->form->isValid($this->getRequest ()->getPost())) {
				if (!RbView_Flood::checkFlood ( $this->ticket )) {return false;}
				if( $this->_create($this->view->form->getValues()) ){
					$this->view->successMessage = tra('Save is successfull');
					$this->view->form = null;
				}else{
					$this->view->failedMessage = tra('Save has failed');
				}
			} else {
				$this->view->failedMessage = tra('Save has failed');
			}
		}

		Ranchbe::getError()->checkErrors ();
		$this->render ( 'default/editform', null, true );
		return;
	} //End of method

} //End of class
