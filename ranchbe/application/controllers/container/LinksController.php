<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


require_once ('controllers/container/abstract.php');
class Container_LinksController extends controllers_container_abstract {
	
	protected $page_id = 'containerLinks'; //(string)
	protected $ifSuccessForward = array ('module' => 'container', 'controller' => 'links', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'container', 'controller' => 'links', 'action' => 'index' );
	
	public function init() {
		parent::init ();
		$this->container = Rb_Container::get ( $this->space_name, $this->container_id ); //Create no initialized instance
		$this->containerDocmetadataLink = Rb_Container_Relation_Docmetadata::get ();
		$this->doctypeProcessLink = Rb_Container_Relation_Doctype::get ();
		// Active the tab
		$this->view->assign ( 'documentManage', 'active' ); //Assign variable to smarty
		$this->view->assign ( 'sameModule', $this->ifSuccessForward ['module'] );
		$this->view->assign ( 'sameController', $this->ifSuccessForward ['controller'] );
		$this->view->assign ( 'sameAction', $this->ifSuccessForward ['action'] );
		//scripts for quickform_advmultiselect
    	$this->view->headScript()->appendFile(Ranchbe::getConfig()->js->baseurl.'qfamsHandler.js');
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
		$this->error_stack->push ( WARNING, 'Info', array (), 'None default action is defined' );
		$this->error_stack->checkErrors ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function getmetadatasAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		if (! empty ( $_REQUEST ['sort_field'] )) {
			$params ['sort_field'] = $_REQUEST ['sort_field'];
			$params ['sort_order'] = $_REQUEST ['sort_order'];
		} else
			$params = array ();
		$this->view->list = $this->containerDocmetadataLink->getMetadatas ( $this->container_id, $params );
		RanchBE::getError()->checkErrors ();
		$this->view->assign ( 'container_number', $this->container->getNumber () );
		$this->view->assign ( 'sameurl', './container/links/getmetadatas' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function getdoctypesAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		if (! empty ( $_REQUEST ['sort_field'] )) {
			$params ['sort_field'] = $_REQUEST ['sort_field'];
			$params ['sort_order'] = $_REQUEST ['sort_order'];
		}
		$this->view->list = Rb_Container_Relation_Doctype::get ()->getDoctypes ( NULL, NULL, $this->container_id, $params );
		RanchBE::getError()->checkErrors ();
		$this->view->assign ( 'PageTitle', vsprintf ( tra ( 'Linked doctypes to %s' ), array ($this->container->getNumber () ) ) );
		$this->view->assign ( 'sameurl', './container/links/getdoctypes' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function getcategoriesAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		if (! empty ( $_REQUEST ['sort_field'] )) {
			$params ['sort_field'] = $_REQUEST ['sort_field'];
			$params ['sort_order'] = $_REQUEST ['sort_order'];
		} else
			$params = array ();
		$this->view->list = Rb_Container_Relation_Category::get ()->getCategories ( $this->container_id, $params );
		RanchBE::getError()->checkErrors ();
		$this->view->assign ( 'container_number', $this->container->getNumber () );
		$this->view->assign ( 'sameurl', './container/links/getdoctypes' );
	} //End of method
	
	//---------------------------------------------------------------------
	public function getreadsAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		if (! empty ( $_REQUEST ['sort_field'] )) {
			$params ['sort_field'] = $_REQUEST ['sort_field'];
			$params ['sort_order'] = $_REQUEST ['sort_order'];
		} else
			$params = array ();
		$this->view->list = Rb_Container_Relation_Read::get ()->getReads ( $this->container_id, $params );
		RanchBE::getError()->checkErrors ();
		$this->view->container_number = $this->container->getNumber ();
		$this->view->sameurl = './container/links/getreads';
	} //End of method
	
	//---------------------------------------------------------------------
	public function linkdoctypeAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$doctypes_ids = $this->getRequest ()->getParam ( 'doctype_id' );
		$this->ifFailedForward ['action'] = 'getdoctypes';
		$this->ifSuccessForward ['action'] = 'getdoctypes';
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'selectDoctype', 'post', $this->actionUrl ( 'linkdoctype' ) );
		$this->_initFormForward ( $form ); //init the hidden param to set the forward parameter
		$form->addElement ( 'header', 'title1', tra ( 'Doctypes list' ) );
		
		$form->addElement ( 'hidden', 'container_id', $this->container_id );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', $this->view->ticket);
		
		//Get the current doctypes
		//$_default = $this->container->getDoctypes(NULL , NULL);
		$_default = $this->doctypeProcessLink->getDoctypes ( NULL, NULL, $this->container_id );
		$default = array ();
		if (is_array ( $_default ))
			foreach ( $_default as $d ) {
				$default [] = $d ['doctype_id'];
			}
		unset ( $_default );
		$p = array ('property_name' => 'doctype_id', 
					'property_description' => tra ( 'Select doctype to add' ), 
					'default_value' => $default, 
					'is_multiple' => true, 
					'return_name' => false, 
					'property_length' => 5, 
					'adv_select' => true );
		construct_select_doctype ( $p, $form, Rb_Doctype::get ( 0 ) );
		
		if (isset ( $validate )) {
			if ($form->validate ()) { //Validate the form...
				//if(!$check_flood) break;
				//Add the property if necessary
				if (is_array ( $doctypes_ids ))
					foreach ( $doctypes_ids as $doctype_id ) {
						if (is_array ( $default )) { //Previous selection exist
							if (! in_array ( $doctype_id, $default )) {
								//$this->doctypeProcessLink->addLink( array('container_id'=>$this->container_id, 'doctype_id'=>$doctype_id ) );
								$link = new Rb_Object_Relation ( 0 );
								$link->setParent ( $this->container_id );
								$link->setChild ( $doctype_id );
								$link->save ();
							}
						} else { //there is no previous selection, add all
							//$this->doctypeProcessLink->addLink( array('container_id'=>$this->container_id, 'doctype_id'=>$doctype_id ) );
							$link = new Rb_Object_Relation ( 0 );
							$link->setParent ( $this->container_id );
							$link->setChild ( $doctype_id );
							$link->save ();
						}
					}
					//Suppress the properties if necessary
				if (is_array ( $default ))
					foreach ( $default as $doctypes_id ) {
						if (is_array ( $doctypes_ids )) { //Selection exist
							if (! in_array ( $doctypes_id, $doctypes_ids )) {
								//$this->doctypeProcessLink->suppressDoctypeContainerLink( $doctypes_id, $this->container_id );
								$this->doctypeProcessLink->suppressLink ( $this->container_id, $doctypes_id );
							}
						} else { //its a empty selection, suppress all
							//$this->doctypeProcessLink->suppressDoctypeContainerLink( $doctypes_id, $this->container_id );
							$this->doctypeProcessLink->suppressLink ( $this->container_id, $doctypes_id );
						}
					}
			}
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			return $form->display ();
		}
		$this->_forward ( $this->ifSuccessForward ['action'], 
						$this->ifSuccessForward ['controller'], 
						$this->ifSuccessForward ['module'], 
						$this->ifSuccessForward ['params'] );
	} //End of method
	

	//---------------------------------------------------------------------
	public function unlinkdoctypeAction() {
		
		$link_ids = $this->getRequest ()->getParam ( 'link_id' );
		$this->ifFailedForward ['action'] = 'getdoctypes';
		$this->ifSuccessForward ['action'] = 'getdoctypes';
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		if (empty ( $link_ids )) {
			$this->error_stack->push ( Rb_Error::WARNING, array (), tra ( 'You must select at least one item' ) );
			return $this->_cancel ();
		}
		
		foreach ( $link_ids as $link_id ) {
			//$this->doctypeProcessLink->suppressLink($link_id);
			$link = new Rb_Object_Relation ( $link_id );
			$link->suppress ();
		}
		$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
	} //End of method
	

	//---------------------------------------------------------------------
	public function linkprocessAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$link_ids = $this->getRequest ()->getParam ( 'link_id' );
		$this->ifFailedForward ['action'] = 'getdoctypes';
		$this->ifSuccessForward ['action'] = 'getdoctypes';
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		if (empty ( $link_ids )) {
			$this->error_stack->push ( Rb_Error::WARNING, array (), tra ( 'You must select at least one item' ) );
			return $this->_cancel ();
		}
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'selectProcess', 'post', $this->actionUrl ( 'linkprocess' ) );
		$this->_initFormForward ( $form ); //init the hidden param to set the forward parameter
		$form->addElement ( 'header', 'title1', tra ( 'Process list' ) );
		
		$form->addElement ( 'hidden', 'container_id', $this->container_id );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', $this->view->ticket );
		foreach ( $link_ids as $link_id ) {
			$form->addElement ( 'hidden', 'link_id[]', $link_id );
		}
		
		//Get list
		$p = array ('property_name' => 'process_id', 'property_description' => tra ( 'Select process to add' ), 'is_multiple' => false, 'return_name' => false, 'property_length' => 5, 'adv_select' => false );
		construct_select_process ( $p, $form );
		
		if (isset ( $validate )) {
			$process_id = $this->getRequest ()->getParam ( 'process_id' );
			if ($form->validate ()) { //Validate the form...
				foreach ( $link_ids as $link_id ) {
					//$this->doctypeProcessLink->modifyLink($link_id, array('process_id'=>$process_id ) );
					$link = new Rb_Object_Relation ( $link_id );
					$link->setExtend ( $process_id );
					$link->save ();
				}
			}
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			$form->display ();
			die ();
		}
		$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
	
	} //End of method
	

	//---------------------------------------------------------------------
	public function unlinkprocessAction() {
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$link_ids = $this->getRequest ()->getParam ( 'link_id' );
		$this->ifSuccessForward ['action'] = 'getdoctypes';
		$this->ifFailedForward ['action'] = 'getdoctypes';
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		if (empty ( $link_ids )) {
			$this->error_stack->push ( Rb_Error::WARNING, array (), tra ( 'You must select at least one item' ) );
			return $this->_cancel ();
		}
		
		foreach ( $link_ids as $link_id ) {
			//$this->doctypeProcessLink->modifyLink($link_id, array('process_id'=>'') );
			$link = new Rb_Object_Relation ( $link_id );
			$link->setExtend ( null );
			$link->save ();
		}
		$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
	} //End of method
	

	//---------------------------------------------------------------------
	public function linkpropertyAction() {
		return $this->_forward('linkmetadata');
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function linkmetadataAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$property_ids = $this->getRequest ()->getParam ( 'property_id' );
		$this->ifFailedForward ['action'] = 'getmetadatas';
		$this->ifSuccessForward ['action'] = 'getmetadatas';
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
			
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'selectMetadata', 'post', $this->actionUrl ( 'linkproperty' ) );
		$this->_initFormForward ( $form ); //init the hidden param to set the forward parameter
		$form->addElement ( 'header', 'title1', tra ( 'Metadatas list' ) );
		
		$form->addElement ( 'hidden', 'container_id', $this->container_id );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', $this->view->ticket);
		
		//Get the current property
		//Get list of metadatas
		$_default = $this->containerDocmetadataLink->getMetadatas ( $this->container_id, array () );
		foreach ( $_default as $d ) {
			$default [] = $d ['property_id'];
		}
		unset ( $_default );
		$p = array ('property_name' => 'property_id', 'property_description' => tra ( 'Select property to add' ), 'default_value' => $default, 'is_multiple' => true, 'return_name' => false, 'property_length' => 5, 'adv_select' => true );
		construct_select_property ( $p, $form, Rb_Document::get ( $this->space_name )->getDao () );
		
		if (isset ( $validate )) {
			if ($form->validate ()) { //Validate the form...
				//Add the property if necessary
				if (is_array ( $property_ids ))
					foreach ( $property_ids as $property_id ) {
						if (is_array ( $default )) { //Previous selection exist
							if (! in_array ( $property_id, $default )) {
								$link = new Rb_Object_Relation ( 0 );
								$link->setParent ( $this->container_id );
								$link->setChild ( $property_id );
								$link->save ();
							}
						} else { //there is no previous selection, add all
							$link = new Rb_Object_Relation ( 0 );
							$link->setParent ( $this->container_id );
							$link->setChild ( $property_id );
							$link->save ();
						}
					}
					//Suppress the properties if necessary
				if (is_array ( $default ))
					foreach ( $default as $property_id ) {
						if (is_array ( $property_ids )) { //Selection exist
							if (! in_array ( $property_id, $property_ids )) {
								$this->containerDocmetadataLink->suppressLink ( $this->container_id, $property_id );
							}
						} else { //its a empty selection, suppress all
							$this->containerDocmetadataLink->suppressLink ( $this->container_id, $property_id );
						}
					}
			}
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			return $form->display ();
		}
		$this->_forward ( $this->ifSuccessForward ['action'], 
						$this->ifSuccessForward ['controller'], 
						$this->ifSuccessForward ['module'], 
						$this->ifSuccessForward ['params'] );
	} //End of method
	

	//---------------------------------------------------------------------
	public function unlinkmetadataAction() {
		$link_ids = $this->getRequest ()->getParam ( 'link_id' );
		$this->ifFailedForward ['action'] = 'getmetadatas';
		$this->ifSuccessForward ['action'] = 'getmetadatas';
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		if (empty ( $link_ids )) {
			$this->error_stack->push ( Rb_Error::WARNING, array (), tra ( 'You must select at least one item' ) );
			return $this->_cancel ();
		}
		
		foreach ( $link_ids as $link_id ) {
			$link = new Rb_Object_Relation ( $link_id );
			$link->suppress ();
			//$this->containerDocmetadataLink->suppress();
		}
		$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
	} //End of method
	

	//---------------------------------------------------------------------
	public function linkcategoryAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$category_ids = $this->getRequest ()->getParam ( 'category_id' );
		$this->ifFailedForward ['action'] = 'getcategories';
		$this->ifSuccessForward ['action'] = 'getcategories';
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
			
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'selectCategory', 'post', $this->actionUrl ( 'linkcategory' ) );
		$this->_initFormForward ( $form ); //init the hidden param to set the forward parameter
		$form->addElement ( 'header', 'title1', tra ( 'Categories list' ) );
		
		$form->addElement ( 'hidden', 'container_id', $this->container_id );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
		
		//Get the current doctypes
		//$containerCategoryLink = new rb_container_categoryLink($this->container);
		$containerCategoryLink = new Rb_Container_Relation_Category ( );
		
		$default = $containerCategoryLink->getCategories ( $this->container_id, array () );
		foreach ( $default as $d ) {
			$default_ids [] = $d ['category_id'];
		}
		unset ( $default );
		$p = array ('property_name' => 'category_id', 'property_description' => tra ( 'Select category to add' ), 'default_value' => $default_ids, 'is_multiple' => true, 'return_name' => false, 'adv_select' => true );
		$category = new Rb_Category ( );
		construct_select_category ( $p, $form, $category );
		
		if (isset ( $validate )) {
			if ($form->validate ()) { //Validate the form...
				//if(!$check_flood) break;
				//Add the property if necessary
				if (is_array ( $category_ids ))
					foreach ( $category_ids as $category_id ) {
						if (is_array ( $default_ids )) { //Previous selection exist
							if (! in_array ( $category_id, $default_ids )) {
								//$containerCategoryLink->addLink( array('container_id'=>$this->container->getId(), 'category_id'=>$category_id ) );
								$link = new Rb_Object_Relation ( 0 );
								$link->setParent ( $this->container_id );
								$link->setChild ( $category_id );
								$link->save ();
							}
						} else { //there is no previous selection, add all
							//$containerCategoryLink->addLink( array('container_id'=>$this->container->getId(), 'category_id'=>$category_id ) );
							$link = new Rb_Object_Relation ( 0 );
							$link->setParent ( $this->container_id );
							$link->setChild ( $category_id );
							$link->save ();
						}
					}
				
				//Suppress the properties if necessary
				if (is_array ( $default_ids ))
					foreach ( $default_ids as $category_id ) {
						if (is_array ( $category_ids )) { //Selection exist
							if (! in_array ( $category_id, $category_ids )) {
								//$containerCategoryLink->suppressLink( $containerCategoryLink->getLinkId( $this->container_id, $category_id ) );
								$containerCategoryLink->suppressLink ( $this->container_id, $category_id );
							}
						} else { //its a empty selection, suppress all
							//$containerCategoryLink->suppressLink( $containerCategoryLink->getLinkId( $this->container_id, $category_id ) );
							$containerCategoryLink->suppressLink ( $this->container_id, $category_id );
						}
					}
			}
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			return $form->display ();
		}
		$this->_forward ( $this->ifSuccessForward ['action'], 
						$this->ifSuccessForward ['controller'], 
						$this->ifSuccessForward ['module'], 
						$this->ifSuccessForward ['params'] );
	
	} //End of method
	
	//---------------------------------------------------------------------
	public function linkreadAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		$validate = $this->getRequest ()->getParam ( 'save' );
		$read_ids = $this->getRequest ()->getParam ( 'read_id' );
		$this->ifFailedForward ['action'] = 'getreads';
		$this->ifSuccessForward ['action'] = 'getreads';
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		//-- Construct the form
		//$this->view->dojo()->enable();
		$form = new ZendRb_Form();
		$form->addElement('submit', 'cancel', array(
											'label' => 'Cancel', 
											'order'=>1001));
		$this->view->PageTitle = tra ( 'Select reads' );
		$form->setAction( $this->_helper->url('linkread') );
		
		$form->addElement('hidden', 'container_id', array('value'=>$this->container_id ));
		$form->addElement('hidden', 'space', array('value'=>$this->space_name ));
		$form->addElement('hidden', 'ticket', array('value'=>RbView_Flood::getTicket() ));
		$this->_initFormForward ( $form ); //init the hidden param to set the forward parameter
		
		//Get the reads
		$containerReadLink = new Rb_Container_Relation_Read ();
		$default = $containerReadLink->getReads ( $this->container_id, array () );
		foreach ( $default as $d ) {
			$default_ids [] = $d ['reposit_id'];
		}
		unset ( $default );
		
		$form->addElement('SelectRead','read_id', array(
				'label' => tra ( 'Select read to add' ),
				'value' => $default_ids,
				'allowEmpty'=> false,
				'returnName'=>false,
				'advSelect'=>true,
				'required' => false,
				'multiple' => true,
				'source'=> $this->space,
				)
		);
		
		if ( $this->getRequest ()->isPost() && $validate ) {
			if (!RbView_Flood::checkFlood ( $this->ticket )) {return false;}
			//Add the property if necessary
			if (is_array ( $read_ids ))
				foreach ( $read_ids as $read_id ) {
					if (is_array ( $default_ids )) { //Previous selection exist
						if (! in_array ( $read_id, $default_ids )) {
							//$containerCategoryLink->addLink( array('container_id'=>$this->container->getId(), 'category_id'=>$category_id ) );
							$link = new Rb_Object_Relation ( 0 );
							$link->setParent ( $this->container_id );
							$link->setChild ( $read_id );
							$link->save ();
						}
					} else { //there is no previous selection, add all
						$link = new Rb_Object_Relation ( 0 );
						$link->setParent ( $this->container_id );
						$link->setChild ( $read_id );
						$link->save ();
					}
				}
			
			//Suppress the properties if necessary
			if (is_array ( $default_ids ))
				foreach ( $default_ids as $read_id ) {
					if (is_array ( $read_ids )) { //Selection exist
						if (! in_array ( $read_id, $read_ids )) {
							$containerReadLink->suppressLink ( $this->container_id, $read_id );
						}
					} else { //its a empty selection, suppress all
						$containerReadLink->suppressLink ( $this->container_id, $read_id );
					}
				}
		return $this->_successForward();
		}

		Ranchbe::getError()->checkErrors ();
		$this->view->form = $form;
		$this->render ( 'default/editform', null, true );
		return;
	} //End of method
	
	//---------------------------------------------------------------------
	public function unlinkcategoryAction() {
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$link_ids = $this->getRequest ()->getParam ( 'link_id' );
		$this->ifSuccessForward ['action'] = 'getcategories';
		$this->ifFailedForward ['action'] = 'getcategories';
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		if (empty ( $link_ids )) {
			$this->error_stack->push ( Rb_Error::WARNING, array (), tra ( 'You must select at least one item' ) );
			return $this->_cancel ();
		}
		
		//$containerCategoryLink = new rb_container_categoryLink($this->container);
		foreach ( $link_ids as $link_id ) {
			//$containerCategoryLink->suppressLink($link_id);
			$link = new Rb_Object_Relation ( $link_id );
			$link->suppress ();
		}
		return $this->_successForward();
	} //End of method

	//---------------------------------------------------------------------
	public function unlinkreadAction() {
		$validate = $this->getRequest ()->getParam ( 'save' );
		$link_ids = $this->getRequest ()->getParam ( 'link_id' );
		$this->ifSuccessForward ['action'] = 'getreads';
		$this->ifFailedForward ['action'] = 'getreads';
		
		if (! Ranchbe::checkPerm ( 'admin', $this->container, false ))
			return $this->_cancel ();
		
		if (empty ( $link_ids )) {
			Ranchbe::getError()->warning ( tra ( 'You must select at least one item' ) );
			return $this->_cancel ();
		}
		
		foreach ( $link_ids as $link_id ) {
			$link = new Rb_Object_Relation ( $link_id );
			$link->suppress ();
		}
		return $this->_successForward();
	} //End of method
	
} //End of class
