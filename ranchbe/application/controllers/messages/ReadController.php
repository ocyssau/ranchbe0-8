<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


class Messages_ReadController extends Zend_Controller_Action {
	
	protected $page_id = 'message_read'; //(string)
	protected $ifSuccessForward = array ('module' => 'messages', 'controller' => 'read', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'messages', 'controller' => 'read', 'action' => 'index' );
	
	//------------------------------------------------------------------------------
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->msg_ids = ( array ) $this->getRequest ()->getParam ( 'msg' );
		$this->msg_id = ( int ) $this->getRequest ()->getParam ( 'msgId' );
		$this->view->assign ( 'msgId', $this->msg_id );
		$this->user_id = Rb_User::getCurrentUser ()->getName ();
		$this->dbsource = $this->getRequest ()->getParam ( 'dbsource' ); //messages||archive||sent
		if (empty ( $this->dbsource ))
			$this->dbsource = 'messages';
		$this->message = new Rb_Message ( Ranchbe::getDb () );
	} //End of method
	

	//------------------------------------------------------------------------------
	public function indexAction() {
		$priority = $this->getRequest ()->getParam ( 'priority' );
		$sort_mode = $this->getRequest ()->getParam ( 'sort_mode' );
		if (! $sort_mode)
			$sort_mode = 'date_desc';
		$offset = ( int ) $this->getRequest ()->getParam ( 'offset' );
		$find = $this->getRequest ()->getParam ( 'find' );
		
		$this->view->flag = $flag;
		$this->view->priority = $priority;
		$this->view->flagval = $flagval;
		$this->view->offset = $offset;
		$this->view->sort_mode = $sort_mode;
		$this->view->find = $find;
		$this->view->legend = '';
		$this->view->dbsource = $this->dbsource;
		
		if (! $this->msg_id) {
			$this->view->legend = tra ( 'No more messages' );
			return;
		}
		
		// Using the sort_mode, flag, flagval and find get the next and prev messages
		$next = $this->message->getNextMessage ( $this->user_id, $this->msg_id, $sort_mode, $find, $flag, $flagval, $priority, $this->dbsource );
		$prev = $this->message->getPrevMessage ( $this->user_id, $this->msg_id, $sort_mode, $find, $flag, $flagval, $priority, $this->dbsource );
		$this->view->assign ( 'next', $next );
		$this->view->assign ( 'prev', $prev );
		
		// Mark the message as read in the receivers mailbox
		$this->message->flagMessage ( $this->user_id, $this->msg_id, 'isRead', 'y' );
		
		// Get the message and assign its data to template vars
		$msg = $this->message->getMessage ( $this->user_id, $this->msg_id, $this->dbsource );
		$this->view->assign ( 'msg', $msg );
		
		// which quote format should tiki use?
		$quote_format = 'simple';
		$this->view->assign ( 'quote_format', $quote_format );
		
		// Mark the message as read in the senders sent box:
		$this->message->flagMessage ( $msg ['user_from'], $this->msg_id, 'isRead', 'y', 'sent' );
		
		RbView_Menu::get ();
		RbView_Tab::get ( 'homeTab' )->activate ();
	} //End of method
	

	//------------------------------------------------------------------------------
	// Delete messages if the delete button was pressed
	public function deleteAction() {
		$msgdel = $this->getRequest ()->getParam ( 'msgdel' );
		$this->message->deleteMessage ( $this->user_id, $msgdel, $this->dbsource );
		return $this->_forward ('index');
	} //End of method


} //end of class

