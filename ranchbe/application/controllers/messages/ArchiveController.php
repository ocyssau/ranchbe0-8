<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


class Messages_ArchiveController extends Zend_Controller_Action {
	
	protected $page_id = 'message_archive'; //(string)
	protected $ifSuccessForward = array ('module' => 'messages', 'controller' => 'archive', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'messages', 'controller' => 'archive', 'action' => 'index' );
	
	//------------------------------------------------------------------------------
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->msg_ids = ( array ) $this->getRequest ()->getParam ( 'msg' );
		$this->user_id = Rb_User::getCurrentUser ()->getName ();
		$this->message = new Rb_Message ( Ranchbe::getDb () );
	} //End of method
	

	//------------------------------------------------------------------------------
	public function indexAction() {
		
		$priority = $this->getRequest ()->getParam ( 'priority' );
		$flags = $this->getRequest ()->getParam ( 'flags' );
		if ($flags != '') {
			$parts = explode ( '_', $flags );
			$flag = $parts [0];
			$flagval = $parts [1];
		} else {
			$flag = $this->getRequest ()->getParam ( 'flag' );
			$flagval = $this->getRequest ()->getParam ( 'flagval' );
		}
		
		$sort_mode = $this->getRequest ()->getParam ( 'sort_mode' );
		if (! $sort_mode)
			$sort_mode = 'date_desc';
		$offset = $this->getRequest ()->getParam ( 'offset' );
		if (! $offset)
			$offset = 0;
		$find = $this->getRequest ()->getParam ( 'find' );
		if (! $find)
			$find = '';
		
		$this->view->flag = $flag;
		$this->view->priority = $priority;
		$this->view->flagval = $flagval;
		$this->view->offset = $offset;
		$this->view->sort_mode = $sort_mode;
		$this->view->find = $find;
		
		$maxRecords = 100;
		
		// What are we paginating: items
		$items = $this->message->listUserMessages ( $this->user_id, $offset, $maxRecords, $sort_mode, $find, $flag, $flagval, $priority, 'archive' );
		
		$cant_pages = ceil ( $items ['cant'] / $maxRecords );
		$this->view->cant_pages = $cant_pages;
		$this->view->assign ( 'actual_page', 1 + ($offset / $maxRecords) );
		
		if ($items ['cant'] > ($offset + $maxRecords)) {
			$this->view->assign ( 'next_offset', $offset + $maxRecords );
		} else {
			$this->view->assign ( 'next_offset', - 1 );
		}
		
		if ($offset > 0) {
			$this->view->assign ( 'prev_offset', $offset - $maxRecords );
		} else {
			$this->view->assign ( 'prev_offset', - 1 );
		}
		
		$this->view->items = $items ['data'];
		
		$cellsize = 200;
		$percentage = 1;
		$mailbox_maxsize = Ranchbe::getConfig()->mailbox->maxsize;
		if ($mailbox_maxsize > 0) {
			$current_number = $this->message->countMessages ( $this->user_id, 'archive' );
			$this->view->assign ( 'messu_archive_number', $current_number );
			$this->view->assign ( 'messu_archive_size', $mailbox_maxsize );
			$percentage = ($current_number / $mailbox_maxsize) * 100;
			$cellsize = round ( $percentage / 100 * 200 );
			if ($current_number > $mailbox_maxsize)
				$cellsize = 200;
			if ($cellsize < 1)
				$cellsize = 1;
			$percentage = round ( $percentage );
		}
		$this->view->assign ( 'cellsize', $cellsize );
		$this->view->assign ( 'percentage', $percentage );
		
		RbView_Menu::get ();
		RbView_Tab::get ( 'homeTab' )->activate ();
	} //End of method
	

	//------------------------------------------------------------------------------
	// Delete messages if the delete button was pressed
	public function deleteAction() {
		if ($this->msg_ids) {
			foreach ( array_keys ( $this->msg_ids ) as $msg ) {
				$this->message->deleteMessage ( $this->user_id, $msg, 'archive' );
			}
		}
		return $this->_forward ('index');
	} //End of method
	

	//------------------------------------------------------------------------------
	// Archive messages if the archive button was pressed
	public function autodeleteAction() {
		$days = ( int ) $this->getRequest ()->getParam ( 'days' );
		$this->message->deleteMessages ( $this->user_id, $days, 'archive' );
		return $this->_forward ('index');
	} //End of method
	

	//------------------------------------------------------------------------------
	// Download messages if the download button was pressed
	public function downloadAction() {
		// if message ids are handed over, use them:
		if (isset ( $this->msg_ids )) {
			foreach ( array_keys ( $this->msg_ids ) as $msg ) {
				$tmp = $this->message->getMessage ( $this->user_id, $msg, 'archive' );
				$items [] = $tmp;
			}
		} else {
			$items = $this->message->getMessages ( $this->user_id, 'archive', '', '', '' );
		}
		$this->view->items = $items;
		$this->view->layout()->disableLayout();
		$this->getResponse ()->setHeader ( 'Content-Type', 'application/txt' )
							->setHeader ( 'Content-Transfer-Encoding', Binary )
							->setHeader ( 'Content-Disposition', 'attachment; filename=rb-msg-archive-' . time ( "U" ) . '.txt ' );
	} //End of method


} //End of class

