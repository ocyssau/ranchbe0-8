<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


class Messages_ComposeController extends Zend_Controller_Action {
	protected $page_id = 'message_compose'; //(string)
	protected $ifSuccessForward = array ('module' => 'messages', 'controller' => 'compose', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'messages', 'controller' => 'compose', 'action' => 'index' );
	
	protected $message;
	protected $user_id;
	protected $msg_ids;
	
	//------------------------------------------------------------------------------
	public function init() {
		//$this->error_stack = & Ranchbe::getError();
		$this->msg_ids = ( array ) $this->getRequest ()->getParam ( 'msg' );
		$this->user_id = Rb_User::getCurrentUser ()->getName ();
		$this->message = new Rb_Message ( Ranchbe::getDb () );
		
		$lang = Ranchbe::getConfig()->ressources->translate->lang;
		$this->view->headLink ()->appendStylesheet ( ROOT_URL . 'styles/htmlarea/htmlarea.css' );
		$this->view->headScript ()->appendFile ( Ranchbe::getConfig()->js->baseurl.'htmlarea/htmlarea.js' );
		$this->view->headScript ()->appendFile ( Ranchbe::getConfig()->js->baseurl.'htmlarea/htmlarea-lang-'.$lang.'.js' );
	} //End of method
	

	//------------------------------------------------------------------------------
	public function indexAction() {

		$this->view->subject = $this->getRequest ()->getParam('subject');
		$this->view->message_body = $this->getRequest ()->getParam('message_body');
		$this->view->to_all = $this->getRequest ()->getParam('to_all');
		$this->view->priority = $this->getRequest ()->getParam('priority');
		$this->view->by_message = $this->getRequest ()->getParam('by_message');
		$this->view->by_mail = $this->getRequest ()->getParam('by_mail');
		$this->view->replyto_hash = $this->getRequest ()->getParam('replyto_hash');
		
		$this->view->to = $this->getRequest ()->getParam('to');
		$this->view->bc = $this->getRequest ()->getParam('bc');
		$this->view->bcc = $this->getRequest ()->getParam('bcc');
		
		$by_message = $this->getRequest ()->getParam('by_message');
		$by_mail = $this->getRequest ()->getParam('by_mail');
		
		$this->view->by_message_is_checked = 'checked';
		if($by_mail)
			$this->view->by_mail_is_checked = 'checked';
		
		if ( $this->view->to_all ) {
			$this->view->to = '';
			$all_users = Rb_User::getUsers ();
			foreach ( $all_users as $values ) {
				$this->view->to .= $values ['handle'] . ' ';
			}
		}
		
		//Clean the body text
		$this->view->message_body = $this->message->cleanPutText ( $this->view->message_body );
		
		foreach ( array ('to', 'cc', 'bcc' ) as $dest )
			if (is_array ( $this->view->$dest ))
				$this->view->$dest = implode ( ', ', array_filter ( $this->view->$dest, 'ctype_alnum' ) );
		
		$this->view->activatemail = Ranchbe::getConfig()->mail->message->enable;
		$this->view->sent = 0;
		
		RbView_Menu::get ();
		RbView_Tab::get ( 'homeTab' )->activate ();
	} //End of method
	

	//------------------------------------------------------------------------------
	// Strip Re:Re:Re: from subject
	public function replyAction() {
		$msgId = $this->getRequest ()->getParam('msgId');
		//$subject = $this->getRequest ()->getParam('subject');
		//$body = $this->getRequest ()->getParam('message_body');
		$reply = $this->getRequest ()->getParam('reply');
		$replyall = $this->getRequest ()->getParam('replyall');
		
		$msg = $this->message->getMessage ( $this->user_id, $msgId );
		$subject = $msg ['subject'];
		$body = $msg ['body'];
		
		// Strip Re:Re:Re: from subject
		if ($reply || $replyall) {
			$subject = tra ( "Re:" ) . ereg_replace ( "^(" . tra ( "Re:" ) . ")+", "", $subject );
		}
	} //End of method
	
	//------------------------------------------------------------------------------
	public function sendAction() {
		$subject = $this->getRequest ()->getParam('subject');
		$body = $this->getRequest ()->getParam('message_body');
		$to_all = $this->getRequest ()->getParam('to_all');
		$to = $this->getRequest ()->getParam('to');
		$bc = $this->getRequest ()->getParam('bc');
		$bcc = $this->getRequest ()->getParam('bcc');
		$priority = $this->getRequest ()->getParam('priority');
		$by_message = $this->getRequest ()->getParam('by_message');
		$by_mail = $this->getRequest ()->getParam('by_mail');
		
		if(!$by_message && !$by_mail){
			$this->view->feedback = tra ( 'ERROR: You must choose a method for send' ) . '<br />';
			return $this->_forward('index');
		}
		
		$this->view->sent = 1;
		
		// Validation:
		// must have a subject or body non-empty (or both)
		if (empty ( $subject ) && empty ( $body )) {
			$this->view->feedback = tra ( 'ERROR: Either the subject or body must be non-empty' ) . '<br />';
		}
		
		// Parse the to, cc and bcc fields into an array
		$to = preg_split ( '/\s*(,|\s)\s*/', $to );
		$cc = preg_split ( '/\s*(,|\s)\s*/', $cc );
		$bcc = preg_split ( '/\s*(,|\s)\s*/', $bcc );
		
		if ($to_all) {
			$to = array();
			foreach ( Rb_User::getUsers () as $values ) {
				$to[] = $values ['handle'];
			}
		}
		if(!is_array($to)) $to = array();
		
		if($by_message){
			$to_cc_ccc = $to;
			if(is_array($cc))  $to_cc_ccc = array_merge ( $to_cc_ccc, $cc );
			if(is_array($bcc)) $to_cc_ccc = array_merge ( $to_cc_ccc, $bcc );
			$this->_sendMessage($subject, $body, $to_cc_ccc, $priority);
		}
		if($by_mail){
			if(!is_array($cc))  $cc = array();
			if(!is_array($bcc)) $bcc = array();
			$this->_sendMail($subject, $body, $to, $cc, $bcc, $priority);
		}
		
		return $this->_forward('index');
	} //End of method
	
	//------------------------------------------------------------------------------
	/** Send a rb message
	 * 
	 * @param string $subject
	 * @param string $body
	 * @param array $to
	 * @param integer $priority
	 * @return void
	 */
	protected function _sendMessage($subject, $body, array $to, $priority) {
		$replyto_hash = $this->getRequest ()->getParam('replyto_hash');
		
		// Remove invalid users from the to, cc and bcc fields
		$users = array ();
		
		foreach ( $to as $a_user ) {
			if (! empty ( $a_user )) {
				if ($this->message->userExists ( $a_user )) {
					// only send mail if nox mailbox size is defined or not reached yet
					if (($this->message->countMessages ( $a_user ) < $messu_mailbox_size) || ($messu_mailbox_size == 0)) {
						$users [] = $a_user;
					} else {
						$this->view->feedback .= sprintf ( tra ( 'User %s can not receive messages, mailbox is full' ), $a_user ) . '<br />';
					}
				} else {
					$this->view->feedback .= sprintf ( tra ( "Invalid user: %s" ), $a_user ) . "<br />";
				}
			}
		}
		$to = array_unique ( $users );
		unset($users);
		
		// Validation: either to, cc or bcc must have a valid user
		if (count ( $to ) > 0) {
			$this->view->feedback .= tra ( "Message will be sent to: " ) . implode ( ',', $to ) . '<br />';
		} else {
			$this->view->feedback .= tra ( 'ERROR: No valid users to send the message' ) . '<br />';
		}
		
		// Insert the message in the inboxes of each user
		foreach ( $to as $user ) {
			$this->message->postMessage ( $user, $this->user_id, $user, '', $subject, $body, $priority, $replyto_hash );
			// if this is a reply flag the original messages replied to
			if ($replyto_hash != '') {
				$this->message->markReplied ( $user, $replyto_hash );
			}
		}
		
		// Insert a copy of the message in the sent box of the sender
		$this->message->saveSentMessage ( $this->user_id, $this->user_id, $to, $cc, $subject, $body, $priority, $replyto_hash );
	
	} //End of method

	//------------------------------------------------------------------------------
	/** Send a mail
	 * 
	 * @param string $subject
	 * @param string $body
	 * @param array $to		array of recipient user name
	 * @param array $bc		array of copy user name
	 * @param array $bcc
	 * @param integer $priority
	 * @return void
	 */
	protected function _sendMail($subject, $body, array $to, array $cc, array $bcc, $priority) {
		
		$mail = new Rb_Mail();
		$mail->setBodyText( $body );
		$mail->setSubject( $subject );
		$mail->setFrom(Rb_User::getCurrentUser()->getProperty('email'), 'RBuser '.Rb_User::getCurrentUser()->getName());
		
		// Insert the message in the inboxes of each user
		foreach ( $to as $username ) {
			$user_id = Ranchbe::getAuthAdapter()->getUserIdFromName($username);
			if(!$user_id) continue;
			$user = Rb_User::get($user_id);
			if(!$user->getProperty('email')){
				$this->view->feedback .= sprintf(tra ( 'ERROR: %s has no mail ' ), $user->getName() ) . '<br />';
				continue;
			}
			$mail->addTo($user->getProperty('email'), $user->getName());
		}
		foreach ( $cc as $username ) {
			$user_id = Ranchbe::getAuthAdapter()->getUserIdFromName($username);
			if(!$user_id) continue;
			$user = Rb_User::get($user_id);
			if(!$user->getProperty('email')){
				$this->view->feedback .= sprintf(tra ( 'ERROR: %s has no mail ' ), $user->getName() ) . '<br />';
				continue;
			}
			$mail->addCc($user->getProperty('email'), $user->getName());
		}
		foreach ( $bcc as $username ) {
			$user_id = Ranchbe::getAuthAdapter()->getUserIdFromName($username);
			if(!$user_id) continue;
			$user = Rb_User::get($user_id);
			if(!$user->getProperty('email')){
				$this->view->feedback .= sprintf(tra ( 'ERROR: %s has no mail ' ), $user->getName() ) . '<br />';
				continue;
			}
			$mail->addBcc($user->getProperty('email'), $user->getName());
		}
		
		if (count ( $mail->getRecipients() ) == 0) {
			$this->view->feedback .= tra ( 'ERROR: No valid users to send the mail' ) . '<br />';
			return;
		}
		
		$mail->send();
		
	} //End of method
	
} //End of class

