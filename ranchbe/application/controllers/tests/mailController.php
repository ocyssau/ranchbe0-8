<?php
/** Zend_Controller_Action */

class Tests_MailController extends Zend_Controller_Action{
  public function sendAction()
  {
    $tr = new Zend_Mail_Transport_Smtp('smtp.free.fr');
    $mail = new Zend_Mail();
    $mail->setDefaultTransport( new Zend_Mail_Transport_Smtp('smtp.free.fr') );
    $mail->setBodyText('Ceci est le texte du message.');
    $mail->setFrom('rororo@free.fr', 'moi');
    $mail->addTo('ocyssau@free.fr', 'un destinataire');
    $mail->setSubject('Sujet de test');
    $mail->send();
  }
} //End of class
