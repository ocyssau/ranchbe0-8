<?php
/** Zend_Controller_Action */

class Tests_IndexController extends Zend_Controller_Action{

  public function zenddbAction()
  {
    $db = new Zend_Db_Adapter_Pdo_Mysql(array(
        'host'     => 'localhost',
        'username' => 'root',
        'password' => '',
        'dbname'   => 'ranchbe0-7'
    ));
    $sql = 'SELECT * FROM workitems';
    $result = $db->fetchAll($sql);
    var_dump($result);
	
    $config = new Zend_Config(
        array(
            'database' => array(
                'adapter' => 'Mysqli',
                'params' => array(
                    'host'     => 'localhost',
                    'dbname' => 'ranchbe',
                    'username' => 'root',
                    'password' => '',
                )
            )
        )
    );
    $db2 = Zend_Db::factory($config->database);
    $result2 = $db2->fetchAll($sql);
    var_dump($result2);
  }

  public function thumbAction()
  {
    $document = Rb_Document::get('workitem', 74);
    $thumbnail = new Rb_Document_Thumbnail($document);
    $thumbnail->generate();
  }

} //End of class
