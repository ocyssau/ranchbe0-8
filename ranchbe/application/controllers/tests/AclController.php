<?php
/** Zend_Controller_Action */

class Tests_AclController extends Zend_Controller_Action{

  public function indexAction()
  {
    $acl = new Zend_Acl();
    
    $acl->addRole(new Zend_Acl_Role('invite'))
        ->addRole(new Zend_Acl_Role('membre'))
        ->addRole(new Zend_Acl_Role('admin'))
        ->addRole(new Zend_Acl_Role('unUtilisateur'), array('invite', 'membre', 'admin'));
    
    $acl->add( new Zend_Acl_Resource('project_1') );
    //$acl->add( new Zend_Acl_Resource('container_1'), 'project_1' );
    $acl->add( new Zend_Acl_Resource('container_1') );
    $acl->add( new Zend_Acl_Resource('document_1'), 'container_1' );
    
    $acl->deny('invite', 'project_1');
    $acl->allow('membre', 'project_1');
    //$acl->deny('unUtilisateur', 'project_1');
    
    echo $acl->isAllowed('unUtilisateur', 'container_1') ? 'autoris�' : 'refus�';
    echo '<br />';
    echo $acl->isAllowed('unUtilisateur', 'project_1') ? 'autoris�' : 'refus�';

    echo '<br />';
    echo $acl->isAllowed('membre', 'project_1') ? 'autoris�' : 'refus�';
    echo '<br />';
    echo $acl->isAllowed('membre', 'container_1') ? 'autoris�' : 'refus�';
    echo '<br />';
    echo $acl->isAllowed('membre', 'document_1') ? 'autoris�' : 'refus�';
  }

  public function test2Action()
  {
    $acl = new Zend_Acl();

    $acl->add( new Zend_Acl_Resource('project_1') );
    $acl->add( new Zend_Acl_Resource('container_1'), 'project_1' );
    $acl->add( new Zend_Acl_Resource('document_1'), 'container_1' );
    
    $acl->addRole(new Zend_Acl_Role('invite'));
    $acl->addRole(new Zend_Acl_Role('staff'), 'invite');
    $acl->addRole(new Zend_Acl_Role('concepteur'), 'staff');
    $acl->addRole(new Zend_Acl_Role('calculateur'), 'staff');
    $acl->addRole(new Zend_Acl_Role('groupManager'), 'staff');
    $acl->addRole(new Zend_Acl_Role('administrateur'));
    
    // Invit� peut uniquement voir le contenu de toutes les ressources
    $acl->allow('invite', null, 'read');
    
    // Staff h�rite des privil�ges de Invit�, mais a aussi ses propres
    // privil�ges
    $acl->allow('groupManager', 'project_1', array('create', 'edit', 'read') );
    $acl->allow('groupManager', 'container_1', array('create', 'edit', 'read') );
    $acl->allow('staff', 'document_1', array('create', 'edit', 'read') );

    echo $acl->isAllowed('groupManager', 'container_1', 'edit') ?
         "autoris�" : "refus�";
    echo '<br />';

    die;

    $acl->allow('concepteur', 'project_1', array('create', 'edit', 'read') );
    
    // concepteur h�rite les privil�ges voir, modifier, soumettre,
    // et relire de Staff, mais a aussi besoin de certains privil�ges
    $acl->allow('concepteur', null, array('publier', 'archiver', 'supprimer'));
    
    // Administrateur h�rite de rien, mais re�oit tous les privil�ges
    $acl->allow('administrateur');

    echo $acl->isAllowed('staff', 'container_1', 'edit') ?
         "autoris�" : "refus�";
    echo '<br />';

    echo $acl->isAllowed('invit�', null, 'voir') ?
         "autoris�" : "refus�";
    echo '<br />';
    
    echo $acl->isAllowed('staff', null, 'publier') ?
         "autoris�" : "refus�";
    echo '<br />';
    
    echo $acl->isAllowed('staff', null, 'relire') ?
         "autoris�" : "refus�";
    echo '<br />';
        
    echo $acl->isAllowed('administrateur', null, 'voir') ?
         "autoris�" : "refus�";
    echo '<br />';
    
    echo $acl->isAllowed('administrateur') ?
         "autoris�" : "refus�";
    // autoris� car administrateur est autoris� pour tout
    echo '<br />';
    
    echo $acl->isAllowed('administrateur', null, 'modifier') ?
         "autoris�" : "refus�";
    // autoris� car administrateur est autoris� pour tout
  } //End of method




  public function test3Action()
  {
    Rb_Container::get('workitem' , 1);
    Ranchbe::getAcl()->init();
  } //End of method


  public function test4Action()
  {
    $acl = new Zend_Acl();

    $acl->add( new Zend_Acl_Resource('project_1') );
    $acl->add( new Zend_Acl_Resource('container_1'), 'project_1' );
    $acl->add( new Zend_Acl_Resource('document_1'), 'container_1' );
    
    $acl->addRole(new Zend_Acl_Role('invite'));
    $acl->addRole(new Zend_Acl_Role('staff'), 'invite');
    $acl->addRole(new Zend_Acl_Role('concepteur'), 'staff');
    $acl->addRole(new Zend_Acl_Role('calculateur'), 'staff');
    $acl->addRole(new Zend_Acl_Role('groupManager'), 'staff');
    $acl->addRole(new Zend_Acl_Role('administrateur'));
    
    // Invit� peut uniquement voir le contenu de toutes les ressources
    $acl->allow('invite', null, 'read');
    
    // Staff h�rite des privil�ges de Invit�, mais a aussi ses propres
    // privil�ges
    $acl->allow('groupManager', 'project_1', array('create', 'edit', 'read') );
    $acl->allow('groupManager', 'container_1', array('create', 'edit', 'read') );
    $acl->allow('staff', 'document_1', array('create', 'edit', 'read') );
    
  } //End of method

} //End of class
