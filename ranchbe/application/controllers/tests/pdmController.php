<?php
/** Zend_Controller_Action */

class Tests_PdmController extends Zend_Controller_Action{

  /*
  public function indexAction(){
  }

  public function createpcAction(){
    $productConcept = new Rb_Pdm_Productconcept();
    $productConcept->setProperty('number', 'toy01');
    $productConcept->setProperty('name', 'chopper toy');
    $productConcept->setProperty('description', 
                                    'a wood chopper toy for childrens from 1 to 99 years');
    $productConcept->save();
    echo 'new product concept id= '.$productConcept->getId().' has been created';
  }

  public function createrootnodeAction(){
    $productConcept = Rb_Pdm_Productconcept::get(1);
    $rootItem = new rb_pdm_item_root();
    $rootItem->setProperty( 'number', 'root_toy01_basic_def');
    $rootItem->setProperty( 'name', 'toy chopper basic definition');
    $rootItem->setProperty( 'description', 'root item of toy chopper');
    $rootItem->setProductConcept( $productConcept );
    $rootItem->save();
    $productConcept->setRootItem($rootItem);
    $productConcept->save();
    echo 'new root node id= '.$rootItem->getId().' has been created';
  }

  public function addchildAction(){
    $item =& rb_pdm_item::get(10);
    $additem = new rb_pdm_item();
    $document =& Rb_Document::get('workitem', 74);
    $additem->setDocument( $document );
    $additem->setProperty('number', $document->getNumber() );
    $additem->setQuantity(1, 'discret' );
    $additem->save();
    $relationship =& $additem->getRelationship();
    $relationship->setFather($item);
    $relationship->save();
    echo 'new node id= '.$additem->getId().' has been created';
  }

  public function getfirstlevelAction(){
    $productConcept = Rb_Pdm_Productconcept::get(1);
    $rootItem =& $productConcept->getRootItem();
    $childs = $rootItem->getRelationship()->getSons(); //expand level 1 of structure
    var_dump($childs);
  }

  public function getchildsAction(){
    $item =& rb_pdm_item::get(3);
    $document = $item->getDocument();
    if($document)
      var_dump( $document->getProperties() );
    $childs = $this->_getchilds($item, 3);
    var_dump($childs);
  }

  protected function _getchilds($item, $level){
    $childs = $item->getRelationship()->getSons();
    if($level > 0 && $childs)
    foreach($childs as $child){
      $item =& rb_pdm_item::get($child['item_id']);
      $childs = $this->_getchilds($item, $level - 1);
      var_dump( $level, $item->getProperties() );
    }
  }
  */

//==============================================================================
//Actions from methods based on STEP

  public function createproductAction(){
    //create basic definition
    $product =& Rb_Pdm_Product::get(0);
    $product->setProperty('number', 'product1'); //number must be uniq, if empty or not set, number wwill be automaticly set
    $product->setProperty('name', 'product 1');
    $product->setProperty('description', 'une part de test');
    $id = $product->save();
    echo 'new product id= '.$product->getId().' has been created';
    var_dump( $product->getProperties(), $product->getId() );
    Ranchbe::getError()->checkErrors();
  }

  public function createproductversionAction(){
    //create basic definition
    $product =& Rb_Pdm_Product::get(1);
    //create version
    $version = Rb_Pdm_Product_Version::get(0);
    $version->setProperty('version_id', 3);
    $version->setProduct($product);
    //associate a existing document to this version
    $document =& Rb_Document::get('workitem', 74);
    $version->setDocument($document);
    //record version in database
    $version->save();
    echo 'new product version id= '.$version->getId().' has been created';
    var_dump( $version->getProperties(), $version->getId() );
    Ranchbe::getError()->checkErrors();
  }

  //Create a product definition
  public function createcontextAction(){
    $context = Rb_Pdm_Context_Productdefinition::get(0);
    $context->setProperty('name', 'asDesign');
    $context->setProperty('application', 'mechanical design');
    $context->setProperty('life_cycle_stage', 'asDesign');
    $context->save();
    echo 'new context id= '.$context->getId().' has been created';
    var_dump( $context->getProperties(), $context->getId() );
    Ranchbe::getError()->checkErrors();
  }
  
  //Create a product definition
  public function createdefinitionAction(){
    $product =& Rb_Pdm_Product_Version::get(3);
    $context1 = Rb_Pdm_Context_Productdefinition::get(1);
    $definition =& Rb_Pdm_Product_Definition::get(0);
    $definition->setProductVersion($product);
    $definition->setContext($context1); //Set the primary context
    $definition->save();
    echo 'new product definition id= '.$definition->getId().' has been created';
    var_dump( $definition->getProperties(), $definition->getId() );
    Ranchbe::getError()->checkErrors();
  }

  //define a child for product 1
  public function setusageAction(){
    //get a product definition
    $product1 =& Rb_Pdm_Product_Definition::get(1);
    $product2 =& Rb_Pdm_Product_Definition::get(2);
    //instanciate Rb_Pdm_Usage
    $usage = Rb_Pdm_Usage::get(0);
    //Set the parent
    $usage->setParent( $product1 );
    //Set the child
    $usage->setChild( $product2 );
    //set the properties
    $usage->setProperty('number','usage1');
    $usage->setProperty('name','usage 1');
    $usage->setProperty('description','un usage');
    $usage->save();
    echo 'new usage id= '.$usage->getId().' has been created';
    var_dump($usage->getProperties(), $usage->getId() );
    Ranchbe::getError()->checkErrors();
  }

  /** Get childs of the product_definition
  */
  public function getchildsAction(){
    //get the product
    $product =& Rb_Pdm_Product::get(1);
    //get the version
    $version =& $product->getVersion(1);
    //get the definition for context id 1
    $context_id = 1;
    $definition =& Rb_Pdm_Product_Definition::getFromVersionContext($version->getId(), 
                                                                    $context_id );
    //get usage of definition
    $p['exact_find']['parent_id'] = $definition->getId();
    $childs = Rb_Pdm_Usage::get()->getAll($p);
    var_dump($definition->getProperties(), $definition->getId() );
    var_dump( $childs );
    Ranchbe::getError()->checkErrors();
  }

  /** Get all product_definition from a context without usage relations
  */
  public function getproductsincontextAction(){
    //get the context
    $context = Rb_Pdm_Context_Productdefinition::get(1);
    $p['exact_find']['ct_id'] = 1;
    $products = Rb_Pdm_Product_Definition::get()->getAll($p);
    var_dump( $context->getProperties(), $products );
  }

  /** Get all product_definition on first level of the context
  */
  public function getrootproductAction(){
    $context_id = 1;
    $roots = Rb_Pdm_Usage::get()->getRoot($context_id);
    var_dump($roots);
  }

  /** Set a association between a product_definition and a context
  */
  public function createassociationAction(){
    $context = Rb_Pdm_Context_Productdefinition::get(1);
    $definition =& Rb_Pdm_Product_Definition::get(13);
    $association = Rb_Pdm_Context_Association::get(0);
    $association->setContext($context);
    $association->setDefinition($definition);
    $association->save();
    echo 'new association id= '.$association->getId().' has been created';
    var_dump($association->getProperties(), $association->getId() );
    Ranchbe::getError()->checkErrors();
  }

  /** Get the associations
  */
  public function getassociationAction(){
    $association = Rb_Pdm_Context_Association::get(1);
    $assocs = $association->getAll();
    var_dump($assocs);
  }

//==============================================================================
//Configuration

  /** Create a product_concept_context
  */
  public function createconfcontextAction(){
    $context = Rb_Pdm_Configuration_Context::get(0);
    $context->setProperty('name', 'pcc2_name');
    $context->setProperty('application', '');
    $context->setProperty('market_segment_type', '');
    $context->save();
    echo 'new product concept id= '.$context->getId().' has been created';
    var_dump($context->getProperties(), $context->getId() );
    Ranchbe::getError()->checkErrors();
  }

  /** Create a product_concept
  */
  public function createpcAction(){
    $productConcept = new rb_pdm_configuration_concept(0);
    $productConcept->setProperty('number', 'pc_02');
    $productConcept->setProperty('name', 'pc 02');
    $productConcept->setProperty('description', 'un product concept');
    $context = Rb_Pdm_Configuration_Context::get(1);
    $productConcept->setContext($context);
    $productConcept->save();
    echo 'new product concept id= '.$productConcept->getId().' has been created';
    var_dump( $productConcept->getProperties(), $productConcept->getId() );
    Ranchbe::getError()->checkErrors();
  }

  /** Create a configuration item
  */
  public function createciAction(){
    $configurationItem = Rb_Pdm_Configuration_Item::get(0);
    $configurationItem->setProperty('number', 'ci02');
    $configurationItem->setProperty('name', 'ci 2');
    $configurationItem->setProperty('description', 'un autre configuration item');
    $configurationItem->setProperty('purpose', 'tester le ci');
    $concept = rb_pdm_configuration_concept::get(1);
    $configurationItem->setConcept($concept);
    $configurationItem->save();
    echo 'new ci id= '.$configurationItem->getId().' has been created';
    var_dump( $configurationItem->getProperties(), $configurationItem->getId() );
    Ranchbe::getError()->checkErrors();
  }

  /** Create a configuration design
  */
  public function createeffectivityAction(){
    $effectivity = Rb_Pdm_Effectivity_Configuration::get(0);
    $effectivity->setProperty('number', 'effectivity_02');
    $effectivity->setProperty('name', 'effectivity 02');
    $effectivity->setProperty('description', 'effectivity du design 1');
    //$usage = Rb_Pdm_Usage::get(1);
    //$effectivity->setUsage($usage);
    $design = Rb_Pdm_Configuration_Design::get(1);
    $effectivity->setDesign($design);
    $effectivity->save();
    echo 'new effectivity id= '.$effectivity->getId().' has been created';
    var_dump( $effectivity->getProperties(), $effectivity->getId() );
    Ranchbe::getError()->checkErrors();
  }

  /** Set a validity
  */
  public function setvalidityAction(){
    $effectivity = Rb_Pdm_Effectivity_Configuration::get(1);
    $validity = new Rb_Pdm_Effectivity_Dated($effectivity);
    $validity->setProperty('start_date', time() );
    $validity->setProperty('end_date', time() );
    $effectivity->save();
    var_dump( $validity->getProperties(), $validity->getId() );
    var_dump( $effectivity->getProperties(), $effectivity->getId() );
    Ranchbe::getError()->checkErrors();
  }

  public function getproductsAction(){
    $products = Rb_Pdm_Product_viewa::get()->getAll();
    //var_dump( $products );
    $varname = $this->getRequest()->getParam('varname');
    $json=json_encode($products);
    $data[$varname] = $products;
    echo json_encode($data);
  }
  
} //End of class
