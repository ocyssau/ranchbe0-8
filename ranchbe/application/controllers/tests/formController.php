<?php
/** Zend_Controller_Action */

class Form_IndexController extends Zend_Controller_Action{

  public function getForm()
  {
    $form = new Zend_Form();
    $form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/tests/index/login')->setMethod('post');
    
    // Create and configure username element:
    $username = $form->createElement('text', 'username');
    $username->addValidator('alnum')
             ->addValidator('regex', false, array('/^[a-z]+/'))
             ->addValidator('stringLength', false, array(6, 20))
             ->setRequired(true)
             ->addFilter('StringToLower');
    
    // Create and configure password element:
    $password = $form->createElement('password', 'password');
    $password->addValidator('StringLength', false, array(6))
             ->setRequired(true);
    
    // Add elements to form:
    $form->addElement($username)
         ->addElement($password)
         ->addElement('submit', 'login', array('label' => 'Login'));

     return $form;
  }

  public function indexAction()
  {
    // Disable the ViewRenderer helper to use smarty:
    Zend_Controller_Front::getInstance()->setParam('noViewRenderer', false);
    // Disable the ErrorHandler plugin:
    Zend_Controller_Front::getInstance()->setParam('noErrorHandler', false);

    // render user/form.phtml
    $this->view->form = $this->getForm();
    $this->render('form');
//var_dump( Zend_Controller_Front::getInstance()->getBaseUrl() );
  }

  public function loginAction()
  {
    if (!$this->getRequest()->isPost()) {
        return $this->_forward('index');
    }
    $form = $this->getForm();
    if (!$form->isValid($_POST)) {
        // Failed validation; redisplay form
        $this->view->form = $form;
        return $this->render('form');
    }

    $values = $form->getValues();
    // now try and authenticate....
  }

} //End of class
