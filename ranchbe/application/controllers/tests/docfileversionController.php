<?php

class Tests_DocfileversionController extends Zend_Controller_Action{

  public function indexAction(){
  }

  public function createAction(){
    $docfile = Rb_Docfile::get('workitem',1);
    $iteration = Rb_Docfile_Iteration::keepIteration($docfile);
    var_dump($iteration);
  }

  public function linkAction(){
    $source = 'c:/tmp/test.doc'; // Ceci est le fichier qui existe actuellement
    $dest = 'c:/tmp/linktest.doc';  // Ceci sera le nom du fichier que vous voulez lier
    link($source, $dest);
  }
  
} //End of class
