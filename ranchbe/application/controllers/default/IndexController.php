<?php

class IndexController extends Zend_Controller_Action {
	
	public function init() {
		/* Initialize action controller here */
	}
	
	public function indexAction() {
		return $this->_forward('index','index','home');
	}
	
	public function headerAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'header' );
	}
	
	public function mainmenuAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'mainMenu' );
	}
	
	public function newmessagenotifyAction() {
		$count_message = Rb_Message_Notification::hasMessage( Rb_User::getCurrentUser()->getId() );
		if( $count_message ){
			$this->_helper->viewRenderer->setResponseSegment ( 'newmessagenotify' );
			$this->view->message_notification = sprintf( tra('you have %s new messages'), $count_message);
			return;
		}else{
			$this->_helper->viewRenderer->setNoRender(true);
			return;
		}
	}
	
	public function footerAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'footer' );
	}

	public function toolbarAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'toolBar' );
	}

	public function aboutAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->view->version = Ranchbe::getVersion();
	}

	public function getlicenceAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$file = APPLICATION_PATH
				 . '/licences/'
				 .	Ranchbe::getConfig()->resources->translate->lang
				 . '/GPL.txt';
		echo '<pre>';
		echo file_get_contents($file);
		echo '</pre>';
	}
	
}
