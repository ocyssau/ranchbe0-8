<?php
/**
 * GraphController
 * 
 * @author
 * @version 
 */

require_once 'Artichow/BarPlot.class.php';
require_once 'Zend/Controller/Action.php';
class GraphController extends Zend_Controller_Action {
	/**
	 * The default action - show the home page
	 */
	public function initAction() {
		$this->values = @unserialize ( urldecode ( stripslashes ( $this->request->getParam ( 'values' ) ) ) );
		$this->legend = @unserialize ( urldecode ( stripslashes ( $this->request->getParam ( 'legend' ) ) ) );
		$this->title = $this->request->getParam ( 'title' );
		// Check data
		if (! is_array ( $this->values ) || ! is_array ( $legend ))
			exit ();
		if (count ( $this->values ) == 0)
			exit ();
	}
	
	/**
	 * The default action - show the home page
	 */
	public function barAction() {
		$graph = new Graph ( 400, 300 ); //width, height px
		//$graph->setAntiAliasing(true);
		$graph->title->set ( $title ); //Title definition
		//$graph->title->setFont(new Font3(26)); //Font of title
		//$graph->title->border->show(); //Border around title
		//$graph->title->setBackgroundColor(new Color(255, 255, 255, 25)); //Background color back title
		//$graph->title->setPadding(4, 4, 4, 4); //Margin of title
		$graph->title->move ( 0, - 5 ); //Position of title x,y
		$plot = new BarPlot ( $values );
		$plot->setSize ( 1, 0.9 ); //Change la largeur $width et la hauteur $height du composant. Les nouvelles valeurs doivent �tre comprises entre 0 et 1 et correspondent � une fraction des largeur et hauteur de l'image � laquelle le composant appartient.
		$plot->setAbsPosition ( 0, 0 ); //Change la position du composant sur l'image. Contrairement � setCenter(), cette m�thode ne place pas le composant par rapport � son centre, mais par rapport � son coin haut-gauche. Les positions $left � gauche et $top pour la hauteur doivent �tre donn�es en pixels. Attention, la position 0 pour $top place le composant en haut de l'image.
		$plot->xAxis->setLabelText ( $legend );
		$plot->xAxis->label->setAngle ( 90 );
		//$plot->xAxis->title->set("Axe des X");
		//$plot->xAxis->title->setFont(new TuffyBold(10));
		//$plot->xAxis->setTitleAlignment(Label::RIGHT);
		$plot->yAxis->title->set ( "Count" );
		$plot->yAxis->title->setFont ( new TuffyBold ( 10 ) );
		$plot->yAxis->title->move ( - 4, 0 );
		$plot->yAxis->setTitleAlignment ( Label::TOP );
		$plot->setBarColor ( new Color ( 250, 255, 0 ) );
		//$plot->setSpace(20, 20, 20, 40); //public setSpace(int $left := NULL, int $right := NULL, int $top := NULL, int $bottom := NULL)
		/*
		$plot->barShadow->setSize(3);
		$plot->barShadow->setPosition(Shadow::RIGHT_TOP);
		$plot->barShadow->setColor(new Color(180, 180, 180, 10));
		$plot->barShadow->smooth(TRUE);
		*/
		$plot->setBackgroundGradient ( new LinearGradient ( new Color ( 230, 230, 230 ), new Color ( 255, 255, 255 ), 0 ) );
		$plot->barBorder->setColor ( new Color ( 0, 0, 150, 20 ) );
		$plot->setBarGradient ( new LinearGradient ( new Color ( 150, 150, 210, 0 ), new Color ( 230, 230, 255, 30 ), 0 ) );
		$plot->setBarPadding ( 0.3, 0.3 ); //Width of bars
		//$plot->setBarSpace(10);
		$graph->add ( $plot );
		$graph->draw ();
	}
}

