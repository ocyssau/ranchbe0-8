<?php

class GetController extends Zend_Controller_Action {
	
	/**
	 * (non-PHPdoc)
	 * @see external/Zend/Controller/Zend_Controller_Action#init()
	 */
	public function init() {
		/* Initialize action controller here */
		$this->view->layout ()->disableLayout ();
		$this->_helper->viewRenderer->setNoRender(true);
	}
	
	
	//---------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see external/Zend/Controller/Zend_Controller_Action#preDispatch()
	 */
	function preDispatch() {
		if (! Ranchbe::getAuth ()->hasIdentity ()) {
			return $this->_checkTicket();
		}
	}
	
	//---------------------------------------------------------------------
	/** Ticket use here is a md5 hash security code to compare with code record in db.
	 * 	This ticket is use to give access to file to user without valid access to ranchbe.
	 *  This ticket has limited time validity.
	 *  
	 * @return unknown_type
	 */
	protected function _checkTicket() {
		$ticket = $this->getRequest ()->getParam ( 'ticket' );
		$id = $this->getRequest ()->getParam ( 'document_id' );
		if(!$id) $id = $this->getRequest ()->getParam ( 'file_id' );
		if(!$ticket || !$id) return controllers_abstract::preDispatch();
		//clean tickets
		Rb_Ticket::cleanExpired();
		//get ticket in db
		if(! Rb_Ticket::isValid($ticket, $id) ){
			return controllers_abstract::preDispatch();
		}else{
			return true;
		}
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function indexAction() {
		return $this->_forward ( 'get', 'index' );
	}

	/**
	 * 
	 * @return unknown_type
	 */
	public function fileAction() {
		$space_name = $this->getRequest ()->getParam ( 'space' );
		$document_id = $this->getRequest ()->getParam ( 'document_id' );
		$file_id = $this->getRequest ()->getParam ( 'file_id' );
		$object_class = $this->getRequest ()->getParam ( 'object_class' );
		if ($document_id) {
			Rb_Document::get ( $space_name, $document_id )->viewDocument ();
			Ranchbe::getError()->checkErrors ();
		}
		if (! $file_id)
			return;
		
		switch ($object_class) {
			case 'docfile_iteration' :
			case 'docfile' :
				$recordfile = & Rb_Docfile::get ( $space_name, $file_id );
				break;
			case 'recordfile' :
				$recordfile = & Rb_Recordfile::get ( $space_name, $file_id );
				break;
			default :
				Ranchbe::getError()->push ( Rb_Error::WARNING, array (), tra ( 'object_class is not set. can not display' ) );
				Ranchbe::getError()->checkErrors ();
				return;
				break;
		}
		$fsdata = & $recordfile->getFsdata (); //Get the fsdata from the record file
		if ($fsdata) {
			$viewer = new Rb_Viewer ( ); //Create a new viewer
			$viewer->init ( $fsdata ); //link the fsdata to the viewer and set property of the viewer
			$viewer->push (); //Get the viewable file
			Ranchbe::getError()->checkErrors ();
			return;
		} else {
			Ranchbe::getError()->push ( Rb_Error::WARNING, array ('datapath' => $recordfile->getProperty('file') ), 'file %datapath% is not reachable' );
			Ranchbe::getError()->checkErrors ();
			return;
		}
	} //End of method
} //End of class

