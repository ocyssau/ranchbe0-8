<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');

abstract class Metadatas_abstract extends controllers_abstract {
	protected $page_id = ''; //(string)
	protected $ifSuccessForward = array ();
	protected $ifFailedForward = array ();
	protected $field_regex_access;
	protected $field_list_access;
	protected $selectdb_where_access;
	protected $property_length_access;
	protected $field_name_access;
	protected $metadata;
	protected $prefix = '';
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		
		$this->space_name = $this->getRequest ()->getParam ( 'space' );
		$this->ticket = $this->getRequest ()->getParam ( 'ticket' );
		
		if (empty ( $this->space_name ) && ! empty ( $_SESSION ['SelectedContainerType'] ))
			$this->space_name = $_SESSION ['SelectedContainerType'];
		
		if ($this->getRequest ()->getParam ( 'ifSuccessForward' )) {
			$this->ifSuccessForward = $this->getRequest ()->getParam ( 'ifSuccessForward' );
			$this->ifSuccessForward = $this->parseUrl ( $this->ifSuccessForward );
		}
		
		if ($this->getRequest ()->getParam ( 'ifFailedForward' )) {
			$this->ifSuccessForward = $this->getRequest ()->getParam ( 'ifFailedForward' );
			$this->ifSuccessForward = $this->parseUrl ( $this->ifFailedForward );
		}
		
		//Assign name to particular fields
		$this->view->assign ( 'SPACE_NAME', $this->space_name );
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		$this->view->list = $this->metadata->getMetadatas ( $this->aclobject->getDao () );
		$this->error_stack->checkErrors ();
		// Display the template
		RbView_Menu::get ();
		RbView_Tab::get ( $this->space_name . 'Tab' )->activate ();
		//$this->view->assign('views_helper_filter_form' , $filter->fetchForm($this->view, $this->aclobject) ); //generate code for the filter form
		//$this->view->assign('views_helper_pagination' , $pagination->fetchForm($this->view) ); //generate code for the filter form
		$this->view->assign ( 'PageTitle', tra ( $this->prefix . ' metadatas manager' ) );
		$this->_helper->viewRenderer->setNoController(true);
		$this->_helper->viewRenderer->setScriptAction('metadatas/get');
		return;
	} //End of method

	//----------------------------------------------------------------------------------------------------
	public function suppressAction() {
		if (! Ranchbe::checkPerm ( 'admin', $this->aclobject, false ))
			return $this->_cancel ();
		
		if (RbView_Flood::checkFlood ( $this->ticket )) {
			$property_ids = $this->getRequest ()->getParam ( 'property_id' );
			if (! empty ( $property_ids )) {
				foreach ( $property_ids as $id ) {
					$metadata = & Rb_Metadata_Dictionary::get ( $id );
					$metadata->suppress ();
				}
			}
		}
		
		$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
	
	} //End of method
	
	
	//----------------------------------------------------------------------------------------------------
	public function createAction() {
		if (! Ranchbe::checkPerm ( 'admin', $this->aclobject, false ))
			return $this->_cancel ();
		//Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new HTML_QuickForm ( 'editForm', 'post', $this->actionUrl ( 'create' ) ); //editForm name is used by js, so dont change it
		$form->addElement ( 'header', 'editheader', tra ( 'Create a metadata' ) );
		$form->addElement ( 'text', 'property_fieldname', 
							tra ( 'property_fieldname' ), 
							array ('size' => 10, $this->field_name_access ) );
		$Infos = array();
		$this->_finishForm ( $form, $Infos, 'create' );
	} //End of method
	
	
	//----------------------------------------------------------------------------------------------------
	public function editAction() {
		$this->property_id = $this->getRequest ()->getParam ( 'property_id' );
		$this->metadata = & Rb_Metadata_Dictionary::get ( $this->property_id );
		if (! Ranchbe::checkPerm ( 'admin', $this->aclobject, false ))
			return $this->_cancel ();
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new HTML_QuickForm ( 'editForm', 'post', $this->actionUrl ( 'edit' ) ); //editForm name is used by js, so dont change it
		$form->addElement ( 'header', 'editheader', tra ( 'Modify metadata' ) );
		//Set defaults values of elements if modify request
		$form->addElement ( 'hidden', 'property_id', $this->property_id );
		$Infos = $this->metadata->getProperties ();
		$form->setDefaults ( array ('property_fieldname' => $Infos ['property_fieldname'], 'property_name' => $Infos ['property_name'], 'property_description' => $Infos ['property_description'], 'property_type' => $Infos ['property_type'], 'property_length' => $Infos ['property_length'], 'regex' => $Infos ['regex'], 'is_required' => $Infos ['is_required'], 'is_multiple' => $Infos ['is_multiple'], 'return_name' => $Infos ['return_name'], 'select_list' => $Infos ['select_list'], 'selectdb_where' => $Infos ['selectdb_where'], 'selectdb_table' => $Infos ['selectdb_table'], 'selectdb_field_for_value' => $Infos ['selectdb_field_for_value'], 'selectdb_field_for_display' => $Infos ['selectdb_field_for_display'], 'date_format' => $Infos ['date_format'], 'display_both' => $Infos ['display_both'] ) );
		$form->addElement ( 'text', 'property_fieldname', tra ( 'property_fieldname' ), array ('size' => 10, 'readonly' ) );
		$this->_finishForm ( $form, $Infos, 'edit' );
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	protected function _finishForm(&$form, $Infos, $mode) {
		if ($this->getRequest ()->getParam ( 'cancel' ) )
			$this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
		
		$form->addElement ( 'hidden', 'space', $this->space_name );
		
		//Add form elements
		$form->addElement ( 'text', 'property_name', tra ( 'property_name' ), array ('size' => 10 ) );
		$form->addElement ( 'text', 'property_description', tra ( 'property_description' ) );
		
		$SelectSet = array_combine ( $this->metadata->accept_property_type, $this->metadata->accept_property_type );
		$select = & $form->addElement ( 'select', 
										'property_type', 
										tra ( 'property_type' ), 
										$SelectSet, 
										array ('onChange' => 'afficherAutre()' ) );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		
		$form->addElement ( 'textarea', 'regex', tra ( 'regex' ), array ('cols' => 40, $this->field_regex_access ) );
		$form->addElement ( 'textarea', 'select_list', tra ( 'select_list' ), array ('cols' => 40, $this->field_list_access ) );
		$form->addElement ( 'checkbox', 'is_required', tra ( 'is_required' ) );
		$form->addElement ( 'checkbox', 'is_multiple', tra ( 'is_multiple' ) );
		$form->addElement ( 'checkbox', 'return_name', tra ( 'return_name' ) );
		$form->addElement ( 'checkbox', 'display_both', tra ( 'display_both' ) );
		$form->addElement ( 'text', 'selectdb_where', tra ( 'selectdb_where' ), array ('size' => 80, $this->selectdb_where_access ) );
		$form->addElement ( 'text', 'property_length', tra ( 'property_length' ), array ('size' => 10, $this->property_length_access ) );
		$form->addElement ( 'text', 'selectdb_table', tra ( 'selectdb_table' ), array ('cols' => 40 ) );
		$form->addElement ( 'text', 'selectdb_field_for_value', tra ( 'selectdb_field_for_value' ), array ('cols' => 40 ) );
		$form->addElement ( 'text', 'selectdb_field_for_display', tra ( 'selectdb_field_for_display' ), array ('cols' => 40 ) );
		$form->addElement ( 'text', 'date_format', tra ( 'date_format' ), array ('cols' => 20 ) );
		
		$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		
		//Add submit button
		$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		
		//Add validation rules to check input data
		$form->addRule ( 'property_size', 'should be numeric', 'numeric', null, 'server' );
		$form->addRule ( 'property_fieldname', tra ( 'is required' ), 'required' );
		$form->addRule ( 'property_fieldname', tra ( 'should contains lower letters and/or _ only' ), 'regex', '/^[a-z_]+$/', 'server' );
		$form->addRule ( 'property_description', tra ( 'is required' ), 'required' );
		$form->addRule ( 'property_type', tra ( 'is required' ), 'required' );
		
		// applies new filters to the element values
		$form->applyFilter ( '__ALL__', 'trim' );
		
		// Try to validate the form
		if ($form->validate ()) {
			//$form->freeze(); //and freeze it
			// Form is validated, then processes the create request
			if (RbView_Flood::checkFlood ( $this->ticket )) {
				if ($mode == 'create') {
					$form->process ( array ($this, '_create' ), true );
				} else if ($mode == 'edit') {
					$form->process ( array ($this, '_modify' ), true );
				}
			}
			$this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
			return;
		}
		//Set the renderer for display QuickForm form in a smarty template
		$this->_quickFormRendererSet ( $form );
		$this->error_stack->checkErrors ();
		
		// Display the template
		$this->_helper->viewRenderer->setNoController(true);
		$this->_helper->viewRenderer->setScriptAction('metadatas/edit');
	} //End of method
	

	public function _create($values) {
		$this->metadata->setType ( $values ['property_type'] );
		foreach ( $values as $name => $value ) {
			$this->metadata->setProperty ( $name, $value );
		}
		$this->metadata->save ();
	} //End of method
	

	public function _modify($values) {
		if (! isset ( $values ['is_required'] ))
			$values ['is_required'] = 0;
		if (! isset ( $values ['is_multiple'] ))
			$values ['is_multiple'] = 0;
		if (! isset ( $values ['return_name'] ))
			$values ['return_name'] = 0;
		unset ( $values ['property_fieldname'] ); //can not modify fieldname
		foreach ( $values as $prop => $value ) {
			$this->metadata->setProperty ( $prop, $value );
		}
		$this->metadata->save ();
	} //End of method


} //End of class

