<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/reposit/abstract.php');
class Reposit_IndexController extends controllers_reposit_abstract {
	
	protected $page_id = 'repositManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'reposit', 
								'controller' => 'index', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'reposit', 
								'controller' => 'index', 'action' => 'index' );
	
	public function init() {
		//Zend_Registry::set ( 'controller', $this );
		$this->view->dojo()->enable();
		return parent::init ();
	} //End of method
	
	
	//----------------------------------------------------------------------------------------------------
	protected function _finishEditForm(&$form, $Infos, $mode) {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$set = array (tra ( 'very low' ), tra ( 'low' ), tra ( 'medium' ), tra ( 'high' ), tra ( 'very high' ) );
		$select = & $form->addElement ( 'select', 'priority', tra ( 'Priority' ), $set, array () );
		$select->setSize ( 1 );
		$select->setMultiple ( false );
		return controllers_reposit_abstract::_finishEditForm ( $form, $Infos, $mode );
	} //End of method
	
	
	//----------------------------------------------------------------------------------------------------
	public function _create($values) {
		switch ($values ['for_space']) {
			case 10 :
				$values ['reposit_url'] = Ranchbe::getConfig()->path->reposit->bookshop . '/' . $values ['reposit_number'];
				$values ['space_id'] = 10;
				break;
			case 15 :
				$values ['reposit_url'] = Ranchbe::getConfig()->path->reposit->cadlib . '/' . $values ['reposit_number'];
				$values ['space_id'] = 15;
				break;
			case 20 :
				$values ['reposit_url'] = Ranchbe::getConfig()->path->reposit->mockup . '/' . $values ['reposit_number'];
				$values ['space_id'] = 20;
				break;
			case 25 :
				$values ['reposit_url'] = Ranchbe::getConfig()->path->reposit->workitem . '/' . $values ['reposit_number'];
				$values ['space_id'] = 25;
				break;
		}
		unset ( $values ['for_space'] );
		if (! isset ( $values ['is_active'] ))
			$values ['is_active'] = 0;
		foreach ( $values as $property => $value ) {
			$this->reposit->setProperty ( $property, $value );
		}
		return $this->reposit->save ( true );
	} //End of method
	
	
	//----------------------------------------------------------------------------------------------------
	public function _modify($values) {
		unset ( $values ['reposit_url'] );
		unset ( $values ['reposit_number'] );
		$values['is_active'] = $values['is_active'];
		foreach ( $values as $property => $value ) {
			$this->reposit->setProperty ( $property, $value );
		}
		return $this->reposit->save ();
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward ( 'get' );
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function getAction() {
		
		Ranchbe::getLayout ()->setLayout ( 'reposit' );
		$reposit = & Rb_Reposit::get ();
		$search = new Rb_Search_Db ( $reposit->getDao () );
		
		$search->setFind ( 1, 'reposit_type', 'exact' );
		
		
		$filter = new RbView_Helper_Repositfilter( $this->getRequest(), $search, $this->page_id );
		$filter->setUrl('reposit/index/get');
		$filter->setDefault('sort_field', 'reposit_number');
		
		$filter->setDefault('sort_order', 'ASC');
		$filter->finish();
		//var_dump($reposit->getAll($filter->getParams () ));die;
		$this->view->list = $reposit->getAll ( $filter->getParams () );
		
		//$pagination = new RbView_Helper_Pagination($filter);
		//$pagination->setPagination( count($this->view->list) );
		$this->error_stack->checkErrors ();
		// Display the template
		$this->view->controller = 'index';
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'repositTab' )->activate ();
		$this->view->sameurl = './reposit/index/get'; //important: is first assign
		$this->view->views_helper_filter_form = $filter->fetchForm($this->view->getEngine(), $reposit); //generate code for the filter form
		//$this->view->assign('views_helper_pagination' , $pagination->fetchForm($this->view->getEngine()) ); //generate code for the filter form
		$this->view->PageTitle = 'Reposit manager';
		$this->_helper->actionStack ( 'toolbar' );
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function suppressAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = & new RbView_Pear_Html_QuickForm ( 'confirm_suppress_reposit', 'POST', $this->actionUrl ( 'validatesuppress' ) );
		$form->setWidth ( '550px' );
		$form->addElement ( 'header', 'title1', tra ( 'Are you sure that you want suppress this reposit' ) );
		
		foreach ( $this->reposit_ids as $reposit_id ) {
			$reposit = & Rb_Reposit::get ( $reposit_id );
			if (! Ranchbe::checkPerm ( 'suppress', $reposit, false ))
				continue;
			
			$checkedlabel = '<b>' . $reposit->getNumber () . '</b> ' . '<i>' . $reposit->getProperty ( 'description' ) . '</i>';
			$checkElement = '<input type="checkbox" name="document_id[]" value="' . $reposit_id . '" id="checkbox_tbl_' . $reposit_id . '" checked="checked" />';
			$checkElement .= '<label for="checkbox_tbl_' . $reposit_id . '">';
			$checkElement .= $checkedlabel;
			$checkElement .= '</label>';
			$form->addElement ( 'static', '', $checkElement );
		}
		$this->serialize_request_post ( $form, array (
							'reposit_id', 
							'ifSuccessModule', 
							'ifSuccessAction', 
							'ifSuccessController', 
							'ifFailedModule', 
							'ifFailedAction', 
							'ifFailedController' ) );
		$form->addElement ( 'hidden', 'ticket', RbView_Flood::getTicket () );
		if (is_a ( $form->getElement ( 'ticket' ), 'HTML_QuickForm_element' ))
			$form->getElement ( 'ticket' )->setValue ( RbView_Flood::getTicket () );
		
		$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		$form->applyFilter ( '__ALL__', 'trim' );
		$form->display ();
	} //End of method
	
	
	//----------------------------------------------------------------------------------------------------
	public function validatesuppressAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		$reposits = array();
		foreach ( $this->reposit_ids as $reposit_id ) {
			$reposit = & Rb_Reposit::get ( $reposit_id );
			if (! Ranchbe::checkPerm ( 'suppress', $reposit, false )){
				continue;
			}else{
				$reposits[] = $reposit->getProperties();
				$reposit->suppress ();
			}
		}
		$this->view->reposits = $reposits;
	} //End of method
	
	
	//----------------------------------------------------------------------------------------------------
	public function createAction() {
		Ranchbe::checkPerm ( 'create', Rb_Reposit::get (), true );
		$this->reposit = new Rb_Reposit ( 0 );
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new HTML_QuickForm ( 'createreposit', 'post', $this->actionUrl ( 'create' ) );
		$form->addElement ( 'header', 'editheader', tra ( 'Create a reposit' ) );

		$form->addElement ( 'text', 'reposit_number', tra ( 'Number' ) );
		$this->view->assign ( 'number_help', Ranchbe::getConfig()->reposit->maskhelp );
		$mask = Ranchbe::getConfig()->reposit->mask;
		$form->addRule ( 'reposit_number', 'This number is not valid', 'regex', "/$mask/", 'server' );
		$form->addRule ( 'reposit_number', tra ( 'is required' ), 'required', null, 'server' );

		$p = array ('property_name' => 'for_space', 
					'default_value' => '', 
					'property_length' => 1, 
					'is_multiple' => false, 
					'is_required' => true );
		$space_list = array (10 => tra ( 'Bookshop' ), 15 => tra ( 'Cadlib' ), 20 => tra ( 'Mockup' ), 25 => tra ( 'Workitem' ) );
		construct_select ( $space_list, $p, $form );

		// Creates a radio buttons group
		$radio [] = &HTML_QuickForm::createElement ( 'radio', null, null, tra ( 'Reposit' ), 1 );
		$radio [] = &HTML_QuickForm::createElement ( 'radio', null, null, tra ( 'Read' ), 2 );
		$form->addGroup ( $radio, 'reposit_type', tra ( 'Reposit type' ) );
		//Set defaults values of elements if create request
		$form->setDefaults ( array ('reposit_description' => 'New reposit', 
									'reposit_type' => 1 ) );
		$this->_helper->viewRenderer->setScriptAction('edit');
		return $this->_finishEditForm ( $form, array (), 'create' );
	} //End of method
	

	//----------------------------------------------------------------------------------------------------
	public function editAction() {
		$this->reposit = & Rb_Reposit::get ( $this->reposit_id );
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		
		//if (!$check_flood) return false;
		Ranchbe::checkPerm ( 'edit', $this->reposit, true );
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new HTML_QuickForm ( 'editreposit', 'post', $this->actionUrl ( 'edit' ) );
		$form->addElement ( 'header', 'editheader', sprintf ( tra ( 'Edit reposit %s' ), $this->reposit->getNumber () ) );
		//Set defaults values of elements if modify request
		$Infos = $this->reposit->getProperties ();
		$form->setDefaults ( array (
			'reposit_number' => $this->reposit->getProperty ( 'reposit_number' ),
			'reposit_name' => $this->reposit->getProperty ( 'reposit_name' ),
			'reposit_description' => $this->reposit->getProperty ( 'reposit_description' ),
			'is_active' => $this->reposit->getProperty ( 'is_active' ),
			'priority' => $this->reposit->getProperty ( 'priority' ),
			'reposit_mode' => $this->reposit->getProperty ( 'reposit_mode' ),
			'submit' => 'modify' ) );
		
		//Add hidden fields
		$form->addElement ( 'hidden', 'reposit_id', $this->reposit_id );
		$this->_finishEditForm ( $form, $Infos, 'edit' );
		return;
	} //End of method


} //End of class
