<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/reposit/abstract.php');
class Reposit_DetailController extends controllers_reposit_abstract {
	
	protected $page_id = 'repositDetailManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'reposit', 'controller' => 'detail', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'reposit', 'controller' => 'detail', 'action' => 'index' );
	
	public function init() {
		//Zend_Registry::set ( 'controller', $this );
		return parent::init ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward('get');
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$reposit = & Rb_Reposit::get ( $this->reposit_id );
		$this->view->reposit = $reposit->getProperties ();
		$this->error_stack->checkErrors ();
		// Display the template
		//RbView_Menu::get()->getAdmin();
		//RbView_Tab::get('repositTab')->activate();
		$this->view->assign ( 'sameurl', './repositManager/detail/get' ); //important: is first assign
		$this->view->assign ( 'PageTitle', 'Reposit detail' );
	} //End of method


} //End of class
