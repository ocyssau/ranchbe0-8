<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/reposit/IndexController.php');
class Reposit_ReadController extends Reposit_IndexController {
	protected $page_id = 'readManager'; //(string)
	protected $ifSuccessForward = array ('module' => 'reposit', 
											'controller' => 'read', 
											'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'reposit', 
											'controller' => 'read', 
											'action' => 'index' );

	//---------------------------------------------------------------------
	protected function _finishEditForm(&$form, $Infos, $mode) {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		// Creates a radio buttons group
		$radio [] = &HTML_QuickForm::createElement ( 'radio', null, null, tra ( 'By symlink' ), 1 );
		$radio [] = &HTML_QuickForm::createElement ( 'radio', null, null, tra ( 'By hardlink' ), 3 );
		$radio [] = &HTML_QuickForm::createElement ( 'radio', null, null, tra ( 'By copy' ), 2 );
		$form->addGroup ( $radio, 'reposit_mode', tra ( 'Read mode' ) );
		controllers_reposit_abstract::_finishEditForm ( $form, $Infos, $mode );
		$this->render('reposit/index/edit', null, true);
	} //End of method
	

	//---------------------------------------------------------------------
	public function _create($values) {
		switch ($values ['for_space']) {
			case 10 :
				$values ['reposit_url'] = Ranchbe::getConfig ()->path->read->bookshop
													 . '/' . $values ['reposit_number'];
				$values ['space_id'] = 10;
				break;
			case 15 :
				$values ['reposit_url'] = Ranchbe::getConfig ()->path->read->cadlib
													 . '/' . $values ['reposit_number'];
				$values ['space_id'] = 15;
				break;
			case 20 :
				$values ['reposit_url'] = Ranchbe::getConfig ()->path->read->mockup
													 . '/' . $values ['reposit_number'];
				$values ['space_id'] = 20;
				break;
			case 25 :
				$values ['reposit_url'] = Ranchbe::getConfig ()->path->read->workitem
													 . '/' . $values ['reposit_number'];
				$values ['space_id'] = 25;
				break;
		}
		unset ( $values ['for_space'] );
		if (! isset ( $values ['is_active'] ))
			$values ['is_active'] = 0;
		foreach ( $values as $property => $value ) {
			$this->reposit->setProperty ( $property, $value );
		}
		return $this->reposit->save ( true );
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		$reposit = & Rb_Reposit_Read::get ();
		$search = new Rb_Search_Db ( $reposit->getDao () );
		$search->setFind ( 2, 'reposit_type', 'exact' );
		$filter = new RbView_Helper_Repositfilter( $this->getRequest(), $search, $this->page_id );
		$filter->setUrl('./reposit/read/get');
		$filter->setDefault('sort_field', 'reposit_number');
		$filter->setDefault('sort_order', 'ASC');
		$filter->finish();
		$this->view->list = $reposit->getAll ( $filter->getParams () );
		$this->error_stack->checkErrors ();
		// Display the template
    	$this->view->controller = 'read';
		RbView_Menu::get ()->getAdmin ();
		RbView_Tab::get ( 'readrepositTab' )->activate ();
		$this->view->sameurl = 'reposit/read/get'; //important: is first assign
		$this->view->PageTitle = 'Read reposit manager';
		$this->view->views_helper_filter_form = $filter->fetchForm($this->view->getEngine(), $reposit); //generate code for the filter form
		$this->render('reposit/index/get', null, true);
		$this->_helper->actionStack ( 'toolbar');
		Ranchbe::getLayout ()->setLayout ( 'reposit' );
	} //End of method

	
	//---------------------------------------------------------------------
	public function createAction() {
		Ranchbe::checkPerm ( 'create', Rb_Reposit::get ( $this->space_name, - 1 ), true );
		$this->reposit = Rb_Reposit_Read::get ( $this->space_name, 0 ); //Create a new instance
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new HTML_QuickForm ( 'createreadreposit', 'post', $this->actionUrl ( 'create' ) );
		$form->addElement ( 'header', 'editheader', tra ( 'Create a read reposit' ) );
		
		$form->addElement ( 'text', 'reposit_number', tra ( 'Number' ) );
		$this->view->assign ( 'number_help', Ranchbe::getConfig()->reposit->maskhelp );
		$mask = Ranchbe::getConfig()->reposit->mask;
		$form->addRule ( 'reposit_number', 'This number is not valid', 'regex', "/$mask/", 'server' );
		$form->addRule ( 'reposit_number', tra ( 'is required' ), 'required', null, 'server' );
		
		$p = array (
				'property_name' => 'for_space', 
				'default_value' => '', 
				'property_length' => 1, 
				'is_multiple' => false, 
				'is_required' => true );
		$space_list = array (
			10 => tra ( 'Bookshop' ), 
			15 => tra ( 'Cadlib' ), 
			20 => tra ( 'Mockup' ), 
			25 => tra ( 'Workitem' ) );
		construct_select ( $space_list, $p, $form );
		
		//Set defaults values of elements if create request
		$form->setDefaults ( array ('reposit_description' => 'New read' ) );
		$this->_helper->viewRenderer->setNoController(true);
		$this->_helper->viewRenderer->setScriptAction('edit');
		return $this->_finishEditForm ( $form, array (), 'create' );
	} //End of method

	//---------------------------------------------------------------------
	public function rebuildAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->reposit = & Rb_Reposit_Read::get ( $this->reposit_id );
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		if (! Ranchbe::checkPerm ( 'edit', $this->reposit, false ))
			return $this->_cancel ();
			//if(!RbView_Flood::checkFlood($this->ticket)) return $this->_cancel();

		$validate = $this->getRequest ()->get ( 'save' );
		$rebuildMode = $this->getRequest ()->get ( 'rebuildMode' );
		
		//-- Construct the form
		$this->view->dojo()->enable();
		$this->view->PageTitle = sprintf ( tra ( 'Rebuild read reposit %s' ), $this->reposit->getNumber () );
		$form = new ZendRb_Form();
		$form->setAction( $this->_helper->url('rebuild') );
		$form->getElement('save')->setLabel(tra('rebuild'));
		
		//Add hidden fields
		$form->addElement('hidden', 'reposit_id', array('value'=>$this->reposit_id ));
		$form->addElement('hidden', 'ticket', array('value'=>RbView_Flood::getTicket() ));
		
		$form->addElement('Multiselect','rebuildMode', array(
				'label' => tra ( 'Mode of rebuild' ),
				'required' => true,
				'multiple' => false,
				'multiOptions' => array(
								1 => tra ( 'clean and rebuild' ),
								2 => tra ( 'rebuild' ),
								3 => tra ( 'clean' ),
				)));

		switch($rebuildMode){
			case 1:
			case 2:
				$successMessage = tra('Rebuild is successfull');
				$failedMessage = tra('Rebuild has failed');
				break;
			case 3:
				$successMessage = tra('Clean up is successfull');
				$failedMessage = tra('Clean up has failed');
				break;
		}
		
		if ( $this->getRequest ()->isPost() && $validate && $rebuildMode ) {
			if ($form->isValid($this->getRequest ()->getPost())) {
				if (!RbView_Flood::checkFlood ( $this->ticket )) {return false;}
				if( $this->reposit->rebuild ( $rebuildMode ) ){
					$this->view->successMessage = $successMessage;
					//$form = null;
				}else{
					$this->view->failedMessage = $failedMessage;
				}
			} else {
				$this->view->failedMessage = $failedMessage;
			}
		}
		
		$this->view->form = $form;

		Ranchbe::getError()->checkErrors ();
		$this->render ( 'default/editform', null, true );
		return;
		
		/*		
		$this->serialize_request_post ( $form, array (
												'reposit_id', 
												'ifSuccessModule', 
												'ifSuccessAction', 
												'ifSuccessController', 
												'ifFailedModule', 
												'ifFailedAction', 
												'ifFailedController' ) );
		if (is_a ( $form->getElement ( 'ticket' ), 'HTML_QuickForm_element' ))
			$form->getElement ( 'ticket' )->setValue ( RbView_Flood::getTicket () );
		*/
	} //End of method


} //End of class
