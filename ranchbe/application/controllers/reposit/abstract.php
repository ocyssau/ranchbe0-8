<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('controllers/abstract.php');

class controllers_reposit_abstract extends controllers_abstract{
protected $reposit; //reposit object
protected $reposit_id; //(int)
protected $page_id='repositManager'; //(string)
protected $ifSuccessForward = array('module'=>'reposit',
                                    'controller'=>'index',
                                    'action'=>'index');
protected $ifFailedForward = array('module'=>'reposit',
                                   'controller'=>'index',
                                   'action'=>'index');
protected $ticket=''; //(String)

public function init(){
  $this->error_stack =& Ranchbe::getError();

  $this->ticket = $this->getRequest()->getParam('ticket');
  $this->reposit_id = $this->getRequest()->getParam('reposit_id');

  if(is_array($this->reposit_id)){
    $this->reposit_ids = $this->reposit_id;
    $this->reposit_id  = -1; //to get a not initialized reposit object
  }else{
    $this->reposit_ids = array($this->reposit_id);
  }

  if($this->reposit_id){
    $this->view->assign('reposit_id', $this->reposit_id);
  }

  $this->_initForward();
} //End of method

//----------------------------------------------------------------------------------------------------
protected function _finishEditForm(&$form, $Infos, $mode){
  //Add fields for input informations in all case
  $form->addElement('text', 'reposit_name', tra('Name'));
  $form->addElement('textarea', 'reposit_description', tra('Description'), array('size'=>40));
  $form->addElement('checkbox', 'is_active', tra('Activate'), null, array());

  //Add hidden fields
  $form->addElement('hidden', 'ticket', RbView_Flood::getTicket());

  $form->addElement('reset', 'reset', 'reset');
  $form->addElement('submit', 'submit', tra('Validate') );

  // Try to validate the form
  if ( $form->validate() ){
    $form->freeze(); //and freeze it
    // Form is validated, then processes the create request
    if(RbView_Flood::checkFlood($this->ticket)){
      if($mode == 'create'){
        $form->process(array($this, '_create'), true);
      }else
      if($mode == 'edit'){
        $form->process(array($this, '_modify'), true);
      }
    }
  } //End of validate form
  
  //Set the renderer for display QuickForm form in a smarty template
  $this->_quickFormRendererSet($form);
  
  $this->error_stack->checkErrors();
  
} //End of method

} //End of class
