<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');
class Docfile_IndexController extends controllers_abstract {
	protected $page_id = 'docfileManager_index'; //(string)
	protected $ifSuccessForward = array ('module' => 'content', 'controller' => 'get', 'action' => 'docfile' );
	protected $ifFailedForward = array ('module' => 'content', 'controller' => 'get', 'action' => 'docfile' );
	
	
	public function init() {
		$this->space_name = $this->getRequest ()->getParam ( 'space' );
		$this->page_id = $this->space_name . $this->page_id;
		$this->ticket = $this->view->ticket;
		//id of the version file
		$this->file_id = $this->getRequest ()->getParam ( 'file_id' );
		if (is_array ( $this->file_id )) {
			$this->file_ids = $this->file_id;
			$this->file_id = 0; //to get a not initialized container object
		} else if (! is_null ( $this->file_id )) {
			$this->file_id = ( int ) $this->file_id;
			$this->file_ids = array ($this->file_id );
		} else {
			$this->file_id = 0; //to get a not initialized container object
			$this->file_ids = array ();
		}
		$this->document_id = $this->getRequest ()->getParam ( 'document_id' );
		if($this->space_name)
			$this->document = & Rb_Document::get ( $this->space_name, $this->document_id );
		$this->view->document_id = $this->document_id;
		$this->_initForward ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function suppressAction() {
		if (! Ranchbe::checkPerm ( 'document_suppress', $this->document, false ))
			return $this->_cancel ();
		foreach ( $this->file_ids as $file_id ) {
			Rb_Docfile::get ( $this->space_name, $file_id )->removeFile ( true, false );
		}
		$this->_successForward ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function putinwsAction() {
		if (! Ranchbe::checkPerm ( 'document_get', $this->document, false ))
			return $this->_cancel ();
		foreach ( $this->file_ids as $file_id ) {
			Rb_Docfile::get ( $this->space_name, $file_id )->putFileInWildspace ();
		}
		$this->_successForward ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function resetAction() {
		if (! Ranchbe::checkPerm ( 'document_edit', $this->document, false ))
			return $this->_cancel ();
		foreach ( $this->file_ids as $file_id ) {
			Rb_Docfile::get ( $this->space_name, $file_id )->resetFile ();
		}
		$this->_successForward ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function setroleAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$this->_helper->viewRenderer->setNoRender(true);
		if (! $this->file_ids)
			return $this->_cancel ();
		if (! Ranchbe::checkPerm ( 'document_edit', $this->document, false ))
			return $this->_cancel ();
		
		if ($this->getRequest ()->getParam ( 'cancel' ))
			return $this->_cancel ();
		
		$validate = $this->getRequest ()->getParam ( 'validate' );
		$role_ids = $this->getRequest ()->getParam ( 'role_id' );
		
		/* This unset is necessary for dont interfer with Quick_form because :
		 * Quick_Form use the content of POST to populate the field, here file_id
		 * Quick_Form ignore the value enter in element definition if a value is send be POST!!@�$%%�$�!!
		 */
		unset ( $_POST ['file_id'] );
		
		$default_roles = array ();
		$file_id = current ( $this->file_ids );
		$docfile = Rb_Docfile::get ( $this->space_name, $file_id );
		$docfile_roles = Rb_Docfile_Role::getRoles ( $docfile );
		foreach ( $docfile_roles as $role_id ) {
			$default_ids [] = $role_id;
		}
		
		//-- Construct the form with QuickForm lib
		require_once ('RbView/Smarty/Form/ElementCreators.php');
		$form = new RbView_Pear_Html_QuickForm ( 'setdocfilerole', 'post', $this->actionUrl ( 'setrole' ) );
		$this->_initFormForward ( $form ); //init the hidden param to set the forward parameter

		$form->addElement ( 'header', 'editheader', sprintf ( tra ( 'Set role for file %s' ), $docfile->getNumber () ) );
		$form->setDefaults ( array ('role_id' => $default_ids, 'file_id' => $file_id ) );
		
		//construct select role
		$params = array ('property_name' => 'role_id', 'property_description' => 'Select roles for this docfile', 'adv_select' => true, 'return_name' => false, 'is_required' => false );
		$roles = array ('MAIN', 'VISU', 'PICTURE', 'ANNEX' );
		construct_select ( $roles, $params, $form );
		
		//Add hidden fields
		$form->addElement ( 'hidden', 'file_id', $file_id );
		$form->addElement ( 'hidden', 'document_id', $this->document->getId() );
		$form->addElement ( 'hidden', 'space', $this->space_name );
		
		if (isset ( $validate )) {
			if ($form->validate ()) { //Validate the form...
				//if(!$check_flood) break;
				//Add the property if necessary
				if (is_array ( $role_ids ))
					foreach ( $role_ids as $role_id ) {
						if (is_array ( $default_ids )) { //Previous selection exist, check if link exist
							if (! in_array ( $role_id, $default_ids )) {
								$role = Rb_Docfile_Role::get ( $this->space_name, 0 );
								$role->setFile ( $file_id );
								$role->setRole ( $role_id );
								$role->save ();
							}
						} else { //there is no previous selection, add all
							$role = Rb_Docfile_Role::get ( $this->space_name, 0 );
							$role->setFile ( $file_id );
							$role->setRole ( $role_id );
							$role->save ();
						}
					}
				
				//Suppress the properties if necessary
				if (is_array ( $default_ids ))
					foreach ( $default_ids as $role_id ) {
						if (is_array ( $role_ids )) { //Selection exist
							if (! in_array ( $role_id, $role_ids )) {
								//suppress this role
								Rb_Docfile_Role::get ( $this->space_name )->suppress ( $file_id, $role_id );
							}
						} else { //its a empty selection, suppress all
							Rb_Docfile_Role::get ( $this->space_name )->suppress ( $file_id, $role_id );
						}
					}
			}
		} else {
			$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
			$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
			$form->applyFilter ( '__ALL__', 'trim' );
			return $form->display ();
		}
		$this->_successForward ();
	} //End of method


} //End of class
