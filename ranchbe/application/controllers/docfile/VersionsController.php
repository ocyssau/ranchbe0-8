<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/content/abstract.php');
class Docfile_VersionsController extends controllers_content_abstract {
	protected $page_id = 'recordfile_versions'; //(string)
	protected $ifSuccessForward = array ('module' => 'docfile', 'controller' => 'versions', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'docfile', 'controller' => 'versions', 'action' => 'index' );
	
	public function init() {
		parent::init ();
		
		//id of the version file
		$this->file_id = $this->getRequest ()->getParam ( 'file_id' );
		
		if (is_array ( $this->file_id )) {
			$this->file_ids = $this->file_id;
			$this->file_id = 0; //to get a not initialized container object
		} else if (! is_null ( $this->file_id )) {
			$this->file_id = ( int ) $this->file_id;
			$this->file_ids = array ($this->file_id );
		} else {
			$this->file_id = 0; //to get a not initialized container object
			$this->file_ids = array ();
		}
		
		$this->docfile_id = ( int ) $this->getRequest ()->getParam ( 'docfile_id' );
		$this->view->docfile_id = $this->docfile_id;
		
		//init the father document to set acls
		$this->docfile = & Rb_Docfile::get ( $this->space_name, $this->docfile_id );
		$this->document = & $this->docfile->getFather ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward ('get');
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		Ranchbe::checkPerm ( 'document_get', $this->document, true );
		//get all file versions
		$this->view->list = Rb_Docfile_Iteration::getIterations ( $this->docfile, false );
		$this->view->file_name = $this->docfile->getName();
		// Display the template
		$this->error_stack->checkErrors ();
		$this->view->assign ( 'object_class', 'docfile_iteration' ); //used to view document from template
	} //End of method
	

	//---------------------------------------------------------------------
	public function suppressAction() {
		if (! Ranchbe::checkPerm ( 'admin', $this->document, false ))
			return $this->_cancel ();
		
		foreach ( $this->file_ids as $file_id ) {
			Rb_Docfile_Iteration::get ( $this->space_name, $file_id )->removeFile ( true );
		}
		return $this->_forward ( 'get' );
	} //End of method
	

	//---------------------------------------------------------------------
	public function putinwsAction() {
		if (! Ranchbe::checkPerm ( 'document_get', $this->document, false ))
			return $this->_cancel ();
		
		foreach ( $this->file_ids as $file_id ) {
			Rb_Docfile_Iteration::get ( $this->space_name, $file_id )->putFileInWildspace ();
		}
		return $this->_forward ( 'get' );
	} //End of method


} //End of class
