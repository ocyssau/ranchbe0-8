<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once ('controllers/abstract.php');
class Wildspace_IndexController extends controllers_abstract {
	protected $page_id = 'wildspace'; //(string)
	protected $ticket = ''; //(string)
	protected $ifSuccessForward = array ('module' => 'wildspace', 
											'controller' => 'index', 
											'action' => 'get' );
	protected $ifFailedForward = array ('module' => 'wildspace', 
										'controller' => 'index', 
										'action' => 'get' );
	
	public function init() {
		$this->view->dojo()->enable();
		$this->error_stack = & Ranchbe::getError();
		$this->ticket = $this->getRequest ()->getParam ( 'ticket' );
		$this->wildspace = & Rb_User::getCurrentUser ()->getWildspace ();
		$this->_putContextInView ();
	} //End of method
	
	
	//---------------------------------------------------------------------
	public function indexAction() {
		return $this->_forward ( 'get' );
	} //End of method
	
	
	//---------------------------------------------------------------------
	function _getDatas($params, $displayMd5) {
		$path = $this->wildspace->getPath ();
		if (! is_dir ( $path )) {
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('element' => $path ), 'wildspace %element% is not reachable' );
			return false;
		}
		if (is_file ( $path . '/_db' )) { //add ranchbe 0.6 for cadds datas
			Ranchbe::getError()->push ( Rb_Error::ERROR, array ('element' => $path ), 'forbidden presence of _db file in wildspace %element%. Delete it and retry.' );
			return false;
		}
		$fsdatas = Rb_Directory::getCollectionDatas ( $path, $params );
		
		$return = array ();
		if ($fsdatas)
			foreach ( $fsdatas as $key => $fsdata ) { //get infos from checkout index
				$return [$key] = $fsdata->getProperties ( $displayMd5 );
				if ($getStoredInfos) {
					$checkout_infos = Rb_CheckoutIndex::getCheckoutIndex ( $fsdata->getProperty ( 'file_name' ) );
					$return [$key] ['container_type'] = $checkout_infos ['container_type'];
					$return [$key] ['container_id'] = $checkout_infos ['container_id'];
					$return [$key] ['container_number'] = $checkout_infos ['container_number'];
					$return [$key] ['document_id'] = $checkout_infos ['document_id'];
					$return [$key] ['check_out_by'] = $checkout_infos ['check_out_by'];
					$return [$key] ['description'] = $checkout_infos ['description'];
				}
			}
		return $return;
	} //End of method
	

	//---------------------------------------------------------------------
	public function getAction() {
		$displayMd5 = $this->getRequest ()->getParam ( 'displayMd5' );
		$search = new Rb_Search_Filesystem ( );
		$filter = new RbView_Helper_Filefilter ( $this->getRequest (), $search, $this->page_id );
		$filter->setUrl ( './wildspace/index/get' );
		$filter->setDefault ( 'sort_field', 'file_name' );
		$filter->setDefault ( 'sort_order', 'ASC' );
		$filter->setTemplate ( 'wildspace/searchBar.tpl' );
		$filter->finish ();
		//get infos on files
		$this->view->list = $this->_getDatas ( $filter->getParams (), $displayMd5 );
		// Display the template
		RbView_Menu::get ()->getMySpace ();
		RbView_Tab::get ( 'wildspaceTab' )->activate ();
		$this->view->assign ( 'sameurl', './wildspace/index/get' ); //important: is first assign
		$this->view->assign ( 'views_helper_filter_form', 
				$filter->fetchForm ( $this->view->getEngine(), $this->container ) ); //generate code for the filter form
		//$this->view->assign('views_helper_pagination' , $pagination->fetchForm($this->view->getEngine()) ); //generate code for the filter form
		$this->error_stack->checkErrors ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function suppressfileAction() {
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		$file_names = $this->getRequest ()->getParam ( 'file_name' );
		if (! $file_names)
			return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		
		if (! is_array ( $file_names )) { //generate array if parameter is string
			$file_names = array ($file_names );
		}
		
		foreach ( $file_names as $file_name ) {
			//@todo : Add confirmation
			if ($this->wildspace->suppressWildspaceFile ( $file_name ))
				$this->error_stack->push ( INFO, 'Info', array ('element' => $file_name ), '<em>' . tra ( 'file %element% is suppressed' ) . '</em><br />' );
		}
		
		return $this->_forward ( $this->ifSuccessForward ['action'], 
								$this->ifSuccessForward ['controller'], 
								$this->ifSuccessForward ['module'], 
								$this->ifSuccessForward ['params'] );
	
	} //End of method
	

	//---------------------------------------------------------------------
	public function renamefileAction() {
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		$file_name = $this->getRequest ()->getParam ( 'file_name' );
		$newName = $this->getRequest ()->getParam ( 'newName' );
		
		if (! $newName || ! $file_name)
			$this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
		
		$ofile = new Rb_File ( $this->wildspace->getPath () . '/' . $file_name );
		$ofile->rename ( $this->wildspace->getPath () . '/' . $newName );
		
		return $this->_forward ( $this->ifSuccessForward ['action'], 
							$this->ifSuccessForward ['controller'], 
							$this->ifSuccessForward ['module'], 
							$this->ifSuccessForward ['params'] );
	} //End of method
	

	//---------------------------------------------------------------------
	public function copyfileAction() {
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		$file_name = $this->getRequest ()->getParam ( 'file_name' );
		$newName = $this->getRequest ()->getParam ( 'newName' );
		
		if (! $newName || ! $file_name)
			$this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		
		$ofile = new Rb_File ( $this->wildspace->getPath () . '/' . $file_name );
		$ofile->copy ( $this->wildspace->getPath () . '/' . $newName, 0666, false );
		
		return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
	} //End of method
	

	//---------------------------------------------------------------------
	public function uploadAction() {
		$this->wildspace->uploadFile ( $_FILES ['uploadFile'], $_REQUEST ['overwrite'] );
		return $this->_forward ( $this->ifSuccessForward ['action'], 
								$this->ifSuccessForward ['controller'], 
								$this->ifSuccessForward ['module'], 
								$this->ifSuccessForward ['params'] );
	} //End of method

	//---------------------------------------------------------------------
	public function downloadAction() {
		$file_name = $this->getRequest ()->getParam ( 'file_name' );
		if (! $file_name)
			return $this->_forward ( $this->ifSuccessForward ['action'], 
						$this->ifSuccessForward ['controller'], 
						$this->ifSuccessForward ['module'], 
						$this->ifSuccessForward ['params'] );
		$viewer = new Rb_Viewer ( );
		$viewer->init ( $this->wildspace->getPath () . '/' . $file_name );
		$viewer->push ();
	} //End of method
	

	//---------------------------------------------------------------------
	// Download multiples files in a zip
	public function zipdownloadAction() {
		$file_names = $this->getRequest ()->getParam ( 'file_name' );
		if (! $file_names)
			return $this->_forward ( $this->ifSuccessForward ['action'], 
									$this->ifSuccessForward ['controller'], 
									$this->ifSuccessForward ['module'], 
									$this->ifSuccessForward ['params'] );
		
		if (! is_array ( $file_names )) { //generate array if parameter is string
			$file_names = array ($file_names );
		}
		
		require_once ('File/Archive.php');
		File_Archive::setOption ( 'zipCompressionLevel', 9 );
		foreach ( $file_names as $file_name ) {
			$multiread [] = File_Archive::read ( $this->wildspace->getPath () 
												. '/' . $file_name, $file_name );
		}
		
		$reader = File_Archive::readMulti ( $multiread );
		$writer = File_Archive::toArchive ( 'wildspace.zip', File_Archive::toOutput () );
		File_Archive::extract ( $reader, $writer );
		die ();
	} //End of method
	

	//---------------------------------------------------------------------
	public function uncompressAction() {
		if (! RbView_Flood::checkFlood ( $this->ticket ))
			return $this->_cancel ();
		
		$file_names = $this->getRequest ()->getParam ( 'file_name' );
		if (! $file_names)
			return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
		
		if (! is_array ( $file_names )) { //generate array if parameter is string
			$file_names = array ($file_names );
		}
		
		foreach ( $file_names as $package ) {
			if (! is_file ( $this->wildspace->getPath () . '/' . $package ))
				continue; //To prevent lost of data
			$extension = substr ( $package, strrpos ( $package, '.' ) );
			
			if ($extension == '.Z') {
				if (! exec ( UNZIPCMD . " $package" )) {
					$this->error_stack->push ( Rb_Error::ERROR, array ('element' => $package, 'debug' => array () ), 'cant uncompress this file %element%' );
					return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
				}
			}
			
			if ($extension == '.adraw') {
				$zip = new ZipArchive ( );
				if ($zip->open ( $this->wildspace->getPath () . '/' . $package ) === TRUE) {
					$zip->extractTo ( $this->wildspace->getPath () );
					$zip->close ();
				} else {
					$this->error_stack->push ( Rb_Error::ERROR, array ('element' => $package ), 'WARNING : can not uncompress file : %file%' );
				}
				return $this->_forward ( $this->ifSuccessForward ['action'], $this->ifSuccessForward ['controller'], $this->ifSuccessForward ['module'], $this->ifSuccessForward ['params'] );
			}
			
			require_once ('File/Archive.php'); //File_Archive PEAR package extension
			$objSource = File_Archive::read ( $this->wildspace->getPath () . '/' . $package . '/', $this->wildspace->getPath () . '/' );
			if (PEAR::isError ( $objSource )) {
				$this->error_stack->push ( Rb_Error::ERROR, array (), $objSource->getMessage () . '<br />' . $objSource->getUserInfo () );
				continue;
			}
			$objDest = File_Archive::toFiles ();
			//var_dump($result);
			while ( $objSource->next () ) { //for each unpack file
				$strThisFile = $objSource->getFilename (); // Get the filename
				if (is_file ( $strThisFile ) || is_dir ( $strThisFile )) { //Get the name of the unpack file
					$this->error_stack->push ( INFO, 'Info', array (), 'FILE EXISTS;IS NOT REPLACED: ' . $strThisFile );
					continue;
				}
				$this->error_stack->push ( INFO, 'Info', array (), 'EXTRACTED: ' . $strThisFile );
				//echo $strThisFile .'<br>';
				$objDest->newFile ( $strThisFile ); //Create the file
				$objSource->sendData ( $objDest ); //output the source data to it
			}
			$objSource->close (); //Now close the reader
			$objDest->close (); //Now close the writer
		}
		return $this->_forward ( $this->ifSuccessForward ['action'], 
								$this->ifSuccessForward ['controller'], 
								$this->ifSuccessForward ['module'], 
								$this->ifSuccessForward ['params'] );
	} //End of method
	
	//---------------------------------------------------------------------
	// Get the UUID of a CATIA file
	public function checkuuidAction() {
		require_once ('CheckUUID.php');
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$file_names = $this->getRequest ()->getParam ( 'file_name' );
		if (! $file_names) {
			$this->error_stack->push ( Rb_Error::ERROR, array (), 'You must select at least one item' );
			$this->error_stack->checkErrors ( array ('close_button' => true ) );
			die ();
		}
		
		if (! is_array ( $file_names )) { //generate array if parameter is string
			$file_names = array ($file_names );
		}
		
		set_time_limit ( 30 * 60 ); //To increase default TimeOut.No effect if php is in safe_mode
		foreach ( $file_names as $filename ) {
			$file = $this->wildspace->getPath () . '/' . $filename;
			$this->view->UUID = checkUUID ( $file );
			$this->view->file_name = $file;
			$this->render('checkuuid');
		}
		return;
	} //End of method


} //End of class
