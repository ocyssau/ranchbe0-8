<?php
/** Zend_Controller_Action */
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


require_once ('controllers/abstract.php');

class Search_IndexController extends controllers_abstract 
{
	
	protected $page_id = 'search'; //(string)
	protected $ifSuccessForward = array ('module' => 'search', 'controller' => 'index', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'search', 'controller' => 'index', 'action' => 'index' );
	
	public function init() {
		$this->error_stack = & Ranchbe::getError();
		$this->_initForward ();
	} //End of method

	//---------------------------------------------------------------------
	//Display form
	public function indexAction() {
		$filter_options ['find_name'] = $this->getRequest ()->getParam ( 'find_name' );
		$content = new Rb_Content ( );
		$search = new Rb_Search_Db ( $content );
		$sort_field = $this->getRequest ()->getParam ( 'sort_field' );
		$sort_order = $this->getRequest ()->getParam ( 'sort_order' );
		if (! $sort_field)
			$sort_field = 'name';
		if (! $sort_order)
			$sort_order = 'ASC';
		$search->setOrder ( $sort_field, $sort_order );
		if ($filter_options ['find_name']) {
			$search->setFind ( $filter_options ['find_name'], 'name', 'anywhere' );
		}
		$this->view->list = $content->getAll ( $search->getParams () );
		$this->view->assign ( 'filter_options', $filter_options ); //Defined element for select action search
		// Display the template
		RbView_Menu::get ();
		//RbView_Tab::get('documentTab')->activate();
		//$this->view->assign('views_helper_filter_form' , $filter->fetchForm($this->view, $this->container) ); //generate code for the filter form
		//$this->view->assign('views_helper_pagination' , $pagination->fetchForm($this->view) ); //generate code for the filter form
	} //End of method

} //End of class
