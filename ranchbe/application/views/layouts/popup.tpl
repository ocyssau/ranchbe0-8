{*Smarty template*}
{$this->doctype()}
<html>
<head>
{$this->headTitle()}
<base href="{$root_url}">
{$this->headMeta()}
{$this->headLink()}
{$this->headStyle()}
{enable_dojo view=$this}
{$this->headScript()}

<noscript>{tr}BE CAREFUL : You must enable javascript for use RanchBE and enable pop-up{/tr}</noscript>
</head>

<body class="tundra">

<div id="tiki-main">
	<div id="tiki-top"></div>
	<div id="tiki-mid">
		<div id="tiki-center">
			{$layout->contenu}
			<p>
			<form name="close">
			  <input type="button" onclick="window.close();return false;" value="{tr}Close{/tr}">
			</form>
			</p>
		</div>
	</div>
</div>

{*
{php}
// output summary of SQL logging results
if( PROFILING_DISPLAY ){
  $perf = NewPerfMonitor( ranchbe::getDb() );
  echo $perf->SuspiciousSQL();
  echo $perf->ExpensiveSQL();
}
{/php}
*}


</body>
</html>


