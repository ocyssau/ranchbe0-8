{*Smarty template*}
{$this->doctype()}
<html>
<head>
{$this->headTitle()}
<base href="{$root_url}">
{$this->headMeta()}
{$this->headLink()}
{$this->headStyle()}
{$this->dojo()}
{$this->headScript()}

<noscript>{tr}BE CAREFUL : You must enable javascript for use RanchBE and enable pop-up{/tr}</noscript>
</head>

<body>
<div id="tiki-main">
	<div id="tiki-top"></div>
	<div id="tiki-mid">
		<div id="tiki-center">
			{$layout->contenu}
			<p>
			<form name="close">
			  <input type="button" onclick="window.close()" value="{tr}Close{/tr}">
			</form>
			</p>
		</div>
	</div>
</div>
</body>
</html>




