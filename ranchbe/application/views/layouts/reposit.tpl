{*Smarty template*}
{$this->doctype()}
<html>
<head>
{$this->headTitle()}
<base href="{$root_url}">
{$this->headMeta()}
{$this->headLink()}
{$this->headStyle()}
{enable_dojo view=$this}
{$this->headScript()}


<noscript>{tr}BE CAREFUL : You must enable javascript for use RanchBE and enable pop-up{/tr}</noscript>
</head>

<body class="tundra">
<div id="tiki-main">
	<div id="tiki-top">{$this->action('header', 'index')}</div>
	<div id="tiki-mid">
		{$this->action('mainmenu','index')}
		<div id="toolBar">{$layout->toolBar}</div>
		{$layout->newMessageNotification}
		<div id="tiki-center">
			{$layout->contenu}
		</div>
	</div>
</div>
</body>
</html>
