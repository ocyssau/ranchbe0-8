{*Smarty template*}
{$this->doctype()}
<html>
<head>
	{$this->headTitle()}
	<base href="{$root_url}">
	{$this->headMeta()}
	{$this->headLink()}
	{$this->headStyle()}
	{enable_dojo view=$this}
	{$this->headScript()}
</head>
<body class="tundra">


<noscript>{tr}BE CAREFUL : You must enable javascript for use RanchBE and enable pop-up{/tr}</noscript>
<div id="tiki-main">
	<div id="tiki-top">{$this->action('header', 'index')}</div>
	<div id="tiki-mid">
		{$this->action('mainmenu','index')}
		<div id="toolBar">{$layout->toolBar}</div>
		{$this->action('newmessagenotify', 'index')}
		<div id="tiki-center">
			{$layout->contenu}
		</div>
	</div>
	<div id="tiki-bot">{$this->action('footer','index')}</div>
</div>


{php}
/*not with pdo
// output summary of SQL logging results
if( PROFILING_DISPLAY ){
  $perf = NewPerfMonitor( Ranchbe::getDb() );
  echo $perf->SuspiciousSQL();
  echo $perf->ExpensiveSQL();
}
*/
{/php}



</body>
</html>
