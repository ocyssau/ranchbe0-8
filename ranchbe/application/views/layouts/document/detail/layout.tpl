{*Smarty template*}
{$this->doctype()}
<html>
<head>
{$this->headTitle()}
<base href="{$root_url}">
{$this->headMeta()}
{$this->headLink()}
{$this->headScript()}
{$this->headStyle()}

<noscript>{tr}BE CAREFUL : You must enable javascript for use RanchBE and enable pop-up{/tr}</noscript>
</head>

<body>
<div id="tiki-main">
	<div id="tiki-top"></div>
	<div id="tiki-mid">
		<div id="tiki-center">

<div id="page-bar">
  <span id="tab1" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(1,5);">{tr}Detail of the document{/tr}</a></span>
  <span id="tab2" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(2,5);">{tr}Associated files to{/tr}</a></span>
  <span id="tab3" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(3,5);">{tr}Document relations{/tr}</a></span>
  <span id="tab4" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(4,5);">{tr}Workflows{/tr}</a></span>
</div>

{*================================= DETAIL OF DOC ===================================================*}
<a name="1" ></a>
<div id="content1" class="tabcontent" style="display: {$content1display};">
{$layout->document_detail}
</div>

{*================================= ASSOCIATED FILES ===================================================*}
<a name="2" ></a>
<div id="content2" class="tabcontent" style="display: {$content2display};">
{$layout->associated_files}
</div>
 
{*===================== DOCUMENT STRUCTURE ==============================================================*}
<a name="3"></a>
<div id="content3" class="tabcontent" style="display: {$content3display};">
{$layout->structure}
</div>

{*===================== DOCUMENT ALTERNATIVE ==============================================================*}
{*=================================LISTING OF INSTANCES===============================================*}
<a name="4" ></a>
<div id="content4" class="tabcontent" style="display: {$content4display};">
{$layout->instances}
</div>

			<p>
			<form name="close">
			  <input type="button" onclick="window.close()" value="{tr}Close{/tr}">
			</form>
			</p>
		</div>
	</div>
</div>
</body>
</html>
