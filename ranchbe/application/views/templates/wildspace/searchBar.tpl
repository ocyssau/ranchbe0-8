{*Smarty template*}

{*--------------------Search Bar defintion--------------------------*}
<div id="sitesearchbar">
<fieldset>
<form id="filterf" action="{$sameurl}" method="post">
<input type="hidden" name="activatefilter" value="1" />

  <label for="find_file_name"><small>{tr}file_name{/tr}</small></label>
  <input size="16" type="text" name="find_file_name" value="{$filter_options.find_file_name|escape}" />

  <label for="find_file_type"><small>{tr}file_extension{/tr}</small></label>
  <input size="5" type="text" name="find_file_type" value="{$filter_options.find_file_type|escape}" />

  <label for="displayMd5"><small>{tr}Display md5{/tr}</small></label>
  <input type="hidden" name="displayMd5" value="0" />
  <input type="checkbox" name="displayMd5" value="1" {if $filter_options.displayMd5}"checked"{/if} onChange='javascript:getElementById("filterf").submit()'; />

  <input type="submit" name="filter" value="{tr}filter{/tr}" />
  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />
</fieldset>

</span>

</form>

</div>
