{*Smarty template*}

{*================================= DETAIL OF DOC ===================================================*}
<h3>{tr}Detail of reposit{/tr} {$reposit.reposit_number} (<i>{$reposit.reposit_name}</i>)</h3>

<table><tr>
<td width="50%">
  <table class="normal">
    {cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Basic properties{/tr}</td></tr>
    <tr class="{cycle}"><td>{tr}Number{/tr}</td>
    <td class="thin">
    {$reposit.reposit_number}

    </td></tr>
    <tr class="{cycle}"><td>{tr}Name{/tr}</td><td class="thin">{$reposit.reposit_name}</td></tr>
    <tr class="{cycle}"><td>{tr}Description{/tr}</td><td class="thin">{$reposit.reposit_description}</td></tr>
    <tr class="{cycle}"><td>{tr}reposit_type{/tr}</td><td class="thin">{$reposit.reposit_type|reposit_type}</td></tr>
    <tr class="{cycle}"><td>{tr}reposit_mode{/tr}</td><td class="thin">{$reposit.reposit_mode|reposit_mode}</td></tr>
    <tr class="{cycle}"><td>{tr}space{/tr}</td><td class="thin">{$reposit.space_id|space}</td></tr>
    <tr class="{cycle}"><td>{tr}reposit_url{/tr}</td><td class="thin">{$reposit.reposit_url}</td></tr>

    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Life cycle{/tr}</td></tr>
    <tr class="{cycle}"><td>{tr}Is active{/tr}</td><td class="thin">{$reposit.is_active|yesorno}</td></tr>
    <tr class="{cycle}"><td>{tr}Priority{/tr}</td><td class="thin">{$reposit.priority}</td></tr>

    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Date and time{/tr}</td></tr>
    <tr class="{cycle}"><td>{tr}Create{/tr}</td><td class="thin">{tr}By{/tr} {$reposit.open_by|username} {tr}at{/tr} {$reposit.open_date|date_format}</td></tr>

    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Other properties{/tr}</td></tr>
    <tr class="{cycle}"><td>{tr}reposit_id{/tr}</td><td class="thin">{$reposit.reposit_id}</td></tr>
  </table>
</td>
</tr>
</table>

{reposit_action_button 
		infos=$reposit
		ticket=$ticket 
		displayText = true
		mode='button'
		reposit_type=$reposit.reposit_type
		ifSuccessModule='reposit'
		ifSuccessController='detail'
		ifSuccessAction='index'
		ifFailedModule='reposit'
		ifFailedController='detail'
		ifFailedAction='index'}

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="window.location.href=
   'reposit/detail/get/reposit_id/{$reposit.reposit_id}';
   return false;">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" 
                  alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>
