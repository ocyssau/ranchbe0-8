{*Smarty template*}

{$form.javascript}

<form {$form.attributes}>
{$form.hidden}

<h2>{$form.header.editheader}</h2>

<table class="normal">

  {if $form.for_space.label}
  <tr>
    <td class="formcolor">{$form.for_space.label}:</td>
    <td class="formcolor">{$form.for_space.html}</td>
  </tr>
  {/if}
  {if $form.reposit_number.label}
  <tr>
    <td class="formcolor">{$form.reposit_number.label}:</td>
    <td class="formcolor">
      {$form.reposit_number.html}
      <br /><i>{$number_help}</i>
    </td>
  </tr>
  {/if}
  <tr>
    <td class="formcolor">{$form.reposit_name.label}:</td>
    <td class="formcolor">{$form.reposit_name.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{$form.reposit_description.label}:</td>
    <td class="formcolor">{$form.reposit_description.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{$form.is_active.label}:</td>
    <td class="formcolor">{$form.is_active.html}</td>
  </tr>
  {if $form.reposit_mode.label}
  <tr>
    <td class="formcolor">{$form.reposit_mode.label}:</td>
    <td class="formcolor">{$form.reposit_mode.html}</td>
  </tr>
  {/if}
  {if $form.priority.label}
  <tr>
    <td class="formcolor">{$form.priority.label}:</td>
    <td class="formcolor">{$form.priority.html}</td>
  </tr>
  {/if}

 {if not $form.frozen}
  <tr>
    <td class="formcolor"> </td>
    <td class="formcolor">{$form.submit.html}&nbsp;{$form.reset.html}
  </tr>
  <tr>
    <td class="formcolor">{$form.requirednote}</td>
  </tr>
 {/if}

</table>
</form>

{if $form.errors}
<b>Collected Errors:</b><br />
{foreach key=name item=error from=$form.errors}
  <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
{/if}

