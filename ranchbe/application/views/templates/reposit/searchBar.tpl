{*Smarty template*}
{*--------------------Search Bar defintion--------------------------*}
<div id="sitesearchbar">
<fieldset>
<form id="filterf" action="{$formurl}" method="post">
<input type="hidden" name="activatefilter" value="1" />
<input type="hidden" name="f_adv_search_cb" value="1" />
  <label>
  <input size="4" type="text" name="numrows" value="{$filter_options.numrows|escape}" />
  <small>{tr}rows to display{/tr}</small></label>
<br />

  <label for="find_number"><small>{tr}Number{/tr}</small></label>
  <input size="20" type="text" name="find_number" value="{$filter_options.find_number|escape}" />

  <label for="find_space"><small>{tr}Space{/tr}</small></label>
	<select name="find_space" onchange='javascript:getElementById("filterf").submit();'>
  	<option {if 10 eq $filter_options.find_space}selected="selected"{/if} value=10>{tr}bookshop{/tr}</option>
  	<option {if 15 eq $filter_options.find_space}selected="selected"{/if} value=15>{tr}cadlib{/tr}</option>
  	<option {if 20 eq $filter_options.find_space}selected="selected"{/if} value=20>{tr}mockup{/tr}</option>
  	<option {if 25 eq $filter_options.find_space}selected="selected"{/if} value=25>{tr}workitem{/tr}</option>
	</select>


  <input type="submit" name="filter" value="{tr}filter{/tr}" />
  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />

<br />
{literal}
<script language="JavaScript">
function displayOption(cb,span){
	if(document.getElementById(cb)!=null ){
	    var a,b;
	    var a = document.getElementById(cb);
	    var b = document.getElementById(span);
	    if(a.checked==true)
	        b.style.display = "block";
	    else b.style.display = "none";
   }
}

function initOption(){
displayOption('f_adv_search_cb', 'f_adv_search_span');
displayOption('f_dateAndTime_cb', 'f_dateAndTime_span');
}

</script>
{/literal}

{*Advanced search===========================================*}
<input type="hidden" name="f_adv_search_cb" value="0" />
<label><small>{tr}Advanced filter{/tr}</small>
  <input type="checkbox" name="f_adv_search_cb" value="1" {if $filter_options.f_adv_search_cb}"checked"{/if} id="f_adv_search_cb" onClick="displayOption('f_adv_search_cb', 'f_adv_search_span');" />
</label>
<span id=f_adv_search_span style="display: none">
<fieldset>

  <label for="find"><small>{tr}find{/tr}</small></label>
  <input size="16" type="text" name="find" value="{$filter_options.find|escape}" />

  <label for="find_field"><small>{tr}In{/tr}</small></label>
	<select name="find_field" onchange='javascript:getElementById("filterf").submit();'>
	<option {if '' eq $filter_options.find_field}selected="selected"{/if} value=""></option>
	{foreach from=$find_elements item=name key=field}
   <option {if $field eq $filter_options.find_field}selected="selected"{/if} value="{$field|escape}">{$name}</option>
	{/foreach}
	</select>

</span>

</form>

</div>

{literal}
<script language="JavaScript">
  initOption();
</script>
{/literal}
