{*Smarty template*}



{*--------------------list body---------------------------*}

<div >
	<div class="wrapper1" >
		<div class="wrapper">
			<div class="nav-wrapper">
				<div class="nav-left"></div>
				<div class="nav">
					<ul id="navigation">
				   		<li class="active" id="i0">
							<a href="javascript:change()">
								<span class="menu-left"></span>
								<span class="menu-mid">reposit active</span>
								<span class="menu-right"></span>
							</a>
						</li>
				   		<li class="" id="i1">
							<a href="javascript:change()">
								<span class="menu-left"></span>
								<span class="menu-mid">reposit unactive</span>
								<span class="menu-right"></span>
							</a>
						</li>
				   	</ul>
				</div>
				<div class="nav-right"></div>
			</div>
			<div class="content">
				<div class="buttons" id="ind0">
					{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
					  {section name=list loop=$list}
					        {if $list[list].is_active}
					        	<a href="javascript:popupP('reposit/detail/get/reposit_id/{$list[list].reposit_id}','detailReposit', 400 , 520)" class="positive">
							        	<h1 style="color:#6F6F6F">{$list[list].reposit_name}</h1>
										{$list[list].reposit_number}
								    	{tr}priority{/tr} {$list[list].priority}
							    </a>
					        {/if}
					  {sectionelse}
					    {tr}No containers to display{/tr}
					    {$list}
					  {/section}
				</div>
				
				<div class="cacher" id="ind1">
					{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
					  {section name=list loop=$list}
					        {if !$list[list].is_active}
					          	<a href="javascript:popupP('reposit/detail/get/reposit_id/{$list[list].reposit_id}','detailReposit', 400 , 520)" class="negative">
							        	<h1 style="color:#6F6F6F">{$list[list].reposit_name}</h1>
										{$list[list].reposit_number}
								    	{tr}priority{/tr} {$list[list].priority}
							    </a>
					        {/if}
					  {sectionelse}
					    {tr}No containers to display{/tr}
					    {$list}
					  {/section}
				</div>
			</div>
			<div class="content-bottom"></div>
		</div>
	</div>
</div>


{* -------------------Pagination------------------------ *}
{*$views_helper_pagination*}

<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
</form>

















{literal}
<script type="text/javascript">
var ind = 0;
function change(){
	if(ind==0){
	  dojo.removeClass('i0',"active");
	  dojo.addClass('i1',"active");
	  dojo.addClass('ind0',"cacher");
	  dojo.removeClass('ind1',"cacher");
	  dojo.addClass('ind1',"buttons");
	  ind=1;
	}else{
	  dojo.removeClass('i1',"active");
	  dojo.addClass('i0',"active");
	  dojo.addClass('ind1',"cacher");
	  dojo.removeClass('ind0',"cacher");
	  dojo.addClass('ind0',"buttons");
	  ind=0;
	}
	}
</script>
{/literal}

