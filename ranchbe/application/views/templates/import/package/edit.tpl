{*Smarty template*}

{include file="header.tpl"}

<form {$form.attributes}>
{$form.hidden}

<h2>{tr}Add a description{/tr}</h2>

<table class="normal">
  <tr class="formcolor">
    <td>{tr}{$form.description.label}{/tr}:</td>
    <td>{$form.description.html}</td>
  </tr>

 {if not $form.frozen}
  <tr class="formcolor">
    <td> </td>
    <td>{$form.reset.html}&nbsp;{$form.submit.html}</td>
  </tr>
 {/if}

  <tr>
    <td>{tr}{$form.requirednote}{/tr}</td>
  </tr>

</table>
<input type="hidden" name="ticket" value="{$ticket}" />
</form>

<form name="close">
  <input type="button" onclick="window.close()" value="{tr}Close{/tr}">
</form>

<br />

<b>Collected Errors:</b><br />
  {foreach key=name item=error from=$form.errors}
      <font color="red">{$error}</font> in element [{$name}]<br />
  {/foreach}
