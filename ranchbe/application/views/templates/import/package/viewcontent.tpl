{*Smarty template*}

<h1><b>{$pageTitle}</b></h1>
<table border=1>
	<table class="normal">
		<tr>
			<th class="heading"><b>file_name</b></th>
			<th class="heading"><b>mtime</b></th>
			<th class="heading"><b>size</b></th>
			<th class="heading"><b>gid</b></th>
			<th class="heading"><b>uid</b></th>
		</tr>
		{*--------------------list body---------------------------*}
		{cycle print=false values="even,odd"} 
		{section name=list loop=$list}
		<tr class="{cycle}">
			<td class="thin">{$list[list].file_name}</td>
			<td class="thin">{$list[list].mtime|date_format}</td>
			<td class="thin">{$list[list].file_size|filesize_format}</td>
			<td class="thin">{$list[list].gid}</td>
			<td class="thin">{$list[list].uid}</td>
		</tr>
		{/section}
	</table>
	<b> {$count} elements in this package </b>
	
		