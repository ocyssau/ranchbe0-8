{*Smarty template*}

{literal}
    <style type="text/css">
        iframe {
            position: absolute;
            left: -100px;
            top: -100px;
            width: 10px;
            height: 10px;
            overflow: hidden;
        }

        #progressbar {
        	margin-left: auto;
        	margin-right: auto;
        	width: 600px
        }

        .pg-progressbar {
            position: relative;
            width: 500px;
            height: 24px;
            overflow: hidden;
            border: 1px solid #c6c6c6;
        }

        .pg-progress {
            z-index: 150;
            position: absolute;
            left: 0;
            top: 0;
            width: 0;
            height: 24px;
            overflow: hidden;
        }

        .pg-progressstyle {
            height: 22px;
            border: 1px solid #748a9e;
            background-image: url('img/progressbar/animation.gif');
        }

        .pg-text,
        .pg-invertedtext {
            position: absolute;
            left: 0;
            top: 4px;
            width: 500px;
            text-align: center;
            font-family: sans-serif;
            font-size: 12px;
        }

        .pg-invertedtext {
            color: #ffffff;
        }

        .pg-text {
            z-index: 100;
            color: #000000;
        }
    </style>

<script language="JavaScript">
function Zend_ProgressBar_Update(data)
{
    document.getElementById('pg-percent').style.width = data.percent + '%';
    document.getElementById('pg-text-1').innerHTML = data.text;
    document.getElementById('pg-text-2').innerHTML = data.text;
}

function Zend_ProgressBar_Finish()
{
    document.getElementById('pg-percent').style.width = '100%';
    document.getElementById('pg-text-1').innerHTML = 'Done';
    document.getElementById('pg-text-2').innerHTML = 'Done';
    window.location.replace("{/literal}{$successUrl}{literal}");
}
</script>
{/literal}

<h1>{$title}</h1>
<br />

<iframe src="{$iframe_src}" id="iframe"></iframe>
<div id="progressbar">
    <div class="pg-progressbar">
        <div class="pg-progress" id="pg-percent">
            <div class="pg-progressstyle"></div>
            <div class="pg-invertedtext" id="pg-text-1"></div>
        </div>
        <div class="pg-text" id="pg-text-2"></div>
    </div>
</div>
<div id="progressBar"><div id="progressDone"></div></div>

