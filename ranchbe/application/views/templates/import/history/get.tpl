{*Smarty template*}

{include file = 'recordfile/_actions.tpl'}

<p class="info_bulle" alt="help" width="10px">
  <a href="javascript:done();" 
  		style= "color:#9F9F9F;" 
  		onmouseover="return overlib('{"import_history_help1"|tr|escape:"quotes"}');" 
  		onmouseout="nd();" >
  		<h1 class="pagetitle">{$PageTitle}</h1>
  		</a>
</p> 


{*--------------------Search Bar defintion--------------------------*}
{*assign var="histoOption" value="1"*}
{$views_helper_filter_form|toggleanim}


{literal}
<script type="text/javascript">
	initTbody();
	function done (){{/literal}
		return "{tr}import_history_help1{/tr}";{literal}
	}
</script>
{/literal}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">
{include file='import/history/_list.tpl'}

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>
<button class="mult_submit" type="submit" name="action" value="suppressHistory" title="{tr}Suppress{/tr}"
onclick="if(confirm('{tr}Do you want really suppress this history item{/tr}')){ldelim}
document.checkform.action='./import/history/suppress';
pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="cleanHistory" title="{tr}Clean history{/tr}"
onclick="if(confirm('{tr}Suppress all not imported record{/tr}')){ldelim}
document.checkform.action='./import/history/clean';
pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/history_clean.png" title="{tr}Clean history{/tr}" alt="{tr}Clean history{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="getImportHistory" title="{tr}Refresh{/tr}"
onclick="document.checkform.action='./import/history/get'; return false;{rdelim}">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="space" value="{$SPACE_NAME}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
</form>


