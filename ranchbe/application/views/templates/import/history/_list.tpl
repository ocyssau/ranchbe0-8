{*Smarty template*}
{* -------------------Pagination------------------------ *}
{$views_helper_pagination}

<table class="normal">
 <tr>
  <th class="heading"><input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxes(this.form,'import_order[]',this.checked)" /></th>
  <th class="heading auto"></th>

  <th class="heading">{column_header sort_order='INV' sort_field='import_order'}</th>
  <th class="heading" nowrap>
    {column_header sort_order='INV' sort_field='file_name' displayMd5=$displayMd5}.
    {column_header sort_order='INV' sort_field='file_extension' displayMd5=$displayMd5}
    ({column_header sort_order='INV' sort_field='file_type' displayMd5=$displayMd5})
    <br />
    <i>{column_header sort_order='INV' sort_field='description' displayMd5=$displayMd5}</i>
  </th>
  <th class="heading">{column_header sort_order='INV' sort_field='container_id' title='Container'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='state'}</th>
  <th class="heading">
    {tr}imported{/tr}
    {column_header sort_order='INV' sort_field='import_by' title='by'} - 
    {column_header sort_order='INV' sort_field='import_date' title='date'}
  </th>
  <th class="heading">
    {tr}imported{/tr}
    {column_header sort_order='INV' sort_field='logfile'}<br />
    {column_header sort_order='INV' sort_field='errors_report'}
  </th>
  <th class="heading">
    {column_header sort_order='INV' sort_field='file_path'}<br />
    {column_header sort_order='INV' sort_field='file_mtime'}<br />
    {column_header sort_order='INV' sort_field='file_size'}<br />
    {column_header sort_order='INV' sort_field='file_md5'}
  </th>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin">
    <input type="checkbox" name="import_order[]" value="{$list[list].import_order}" /></td>
    </td>

    <td class="thin">
      <a class="link" href="import/history/viewcontent/import_order/{$list[list].import_order}/space/{$SPACE_NAME}" title="{tr}See the contents{/tr}">
      <img border="0" alt="{tr}ViewContents{/tr}" src="img/icons/eye.png" />
      </a>
    </td>

    <td class="thin">
      <a class="link" href="import/history/getimported/import_order/{$list[list].import_order}/space/{$SPACE_NAME}" 
      	title="{tr}Get files in reposit from this package{/tr}">
      {$list[list].import_order}
      </a>
    </td>

    <td class="thin">
      <a class="link" href="import/history/viewcontent/import_order/{$list[list].import_order}/space/{$SPACE_NAME}" title="{tr}Content{/tr}">
      {$list[list].file_name}</a><br />
      <i>{$list[list].description}</i>
    </td>
    <td class="thin">{$list[list].container_id|container}</a></td>
    <td class="thin">{$list[list].state}</td>
    <td class="thin">
      {$list[list].import_by|username} - {$list[list].import_date|date_format}<br />
    </td>

    <td class="thin">
    {if $list[list].listfile}
      <a class="link" href="import/history/viewcontent/import_order/{$list[list].import_order}/space/{$SPACE_NAME}" title="{tr}Content{/tr}">
      ViewContent</a><br />
    {/if}
    {if $list[list].logfile}
      <a class="link" href="import/history/viewlog/import_order/{$list[list].import_order}/space/{$SPACE_NAME}" title="{tr}Log{/tr}">
      ViewLog</a><br />
    {/if}
    {if $list[list].errors_report}
      <a class="link" href="import/history/viewerror/import_order/{$list[list].import_order}/space/{$SPACE_NAME}" title="{tr}Errors{/tr}">
      ViewErrors</a>
    {/if}
    </td>

    <td class="thin">
    <ul>
      <li>{tr}file_mtime{/tr} : {$list[list].file_mtime|date_format}</li>
      <li>{tr}file_size{/tr} : {$list[list].file_size|filesize_format}</li>
      <li>{tr}file_md5{/tr} : {$list[list].file_md5}</li>
    </ul>
    </td>
   </tr>
  {/section}
  </table>
  {* -------------------Pagination------------------------ *}
  {$views_helper_pagination}
  {* -------------------checkall box------------------------ *}
  <input name="switcher" id="clickall2" type="checkbox" onclick="switchCheckboxes(this.form,'import_order[]',this.checked)" />
  <label for="clickall2">{tr}select all{/tr}</label>
