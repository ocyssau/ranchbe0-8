{*Smarty template*}

<table><tr>
<td width="50%">
  <table class="normal">
    {cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Basic properties{/tr}</td></tr>
    <tr class="{cycle}"><td>{tr}Number{/tr}</td><td class="thin">{$properties.import_order}</td></tr>
    <tr class="{cycle}"><td>{tr}Name{/tr}</td><td class="thin">{$properties.file_name}</td></tr>
    <tr class="{cycle}"><td>{tr}Description{/tr}</td><td class="thin">{$properties.description}</td></tr>
    <tr class="{cycle}"><td>{tr}Container{/tr}</td><td class="thin">{$properties.container_id|container}</td></tr>
    <tr class="{cycle}"><td>{tr}file_extension{/tr}</td><td class="thin">{$properties.file_extension}</td></tr>
    <tr class="{cycle}"><td>{tr}file_mtime{/tr}</td><td class="thin">{$properties.file_mtime|date_format}</td></tr>
    <tr class="{cycle}"><td>{tr}file_size{/tr}</td><td class="thin">{$properties.file_size|filesize_format}</td></tr>
    <tr class="{cycle}"><td>{tr}file_md5{/tr}</td><td class="thin">{$properties.file_md5}</td></tr>

    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Life cycle{/tr}</td></tr>
    <tr class="{cycle}"><td>{tr}State{/tr}</td><td class="thin">{$properties.state}</td></tr>

    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Date and time{/tr}</td></tr>
    <tr class="{cycle}"><td>{tr}Imported{/tr}</td><td class="thin">{tr}By{/tr} {$properties.import_by|username}
																   {tr}at{/tr} {$properties.import_date|date_format}</td></tr>

    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Other properties{/tr}</td></tr>
    <tr class="{cycle}"><td>{tr}Log{/tr}</td><td class="thin">
	    <a href="import/history/viewlog/import_order/{$list[list].import_order}/space/{$SPACE_NAME}" title="{tr}Log{/tr}">
	    {$properties.logfile}</a></td></tr>
    <tr class="{cycle}"><td>{tr}Error log{/tr}</td><td class="thin">
      	<a href="import/history/viewerror/import_order/{$list[list].import_order}/space/{$SPACE_NAME}" title="{tr}Errors{/tr}">
	    {$properties.errors_report}</a></td></tr>
    <tr class="{cycle}"><td>{tr}Contents log{/tr}</td><td class="thin">
      	<a href="import/history/viewcontent/import_order/{$list[list].import_order}/space/{$SPACE_NAME}" title="{tr}Errors{/tr}">
	    {$properties.listfile}</a></td></tr>
  </table>
</td>
</tr>
</table>

