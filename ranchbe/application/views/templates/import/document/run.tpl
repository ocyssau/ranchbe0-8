{*Smarty template*}

{include file="header.tpl"}

<form name="step3" method="post" action="{$smarty.server.PHP_SELF}">
<table>

<tr><td>{tr}Running mode{/tr} :<br></td></tr>
<tr class="formcolor"><td>
  <input type="texte" name="maxcsvrow" value="{$maxcsvrow}" id="maxcsvrow" readonly disabled>
   <label for="maxcsvrow">{tr}Max number of rows to import from a csv file{/tr}</label></td></tr>

<tr><td>{tr}Running mode{/tr} :<br></td></tr>
<tr class="formcolor"><td>
  <input type="radio" name="background" value="1" id="background1">
   <label for="background1">{tr}Launch Update in hidden mode{/tr}</label></td></tr>

<tr class="formcolor"><td>
  <input type="radio" name="background" value="0" id="background2">
   <label for="background2">{tr}Direct update{/tr}</label></td></tr>

<tr class="formcolor"><td>
  <input type="checkbox" name="create_category" value="1" id="create_category">
   <label for="create_category">{tr}Create category if not exist{/tr}</label></td></tr>

<tr class="formcolor"><td>
  <input type="checkbox" name="lock" value="1" id="lock">
   <label for="lock">{tr}Lock document after store{/tr}</label></td></tr>

<tr><td>
  <input type="submit" value="run" name="action" />
  <input type="submit" value="cancel" name="action" />
</td>

</tr>
</table>
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
<input type="hidden" value="{$cvsfile}" name="cvsfile" />
</form>
