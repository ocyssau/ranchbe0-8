{*Smarty template*}

<h2 class="pagetitle">{tr}Select a csv file{/tr}</h2>

<i>
The cvs file must be construct in accordance to this rules :<br />
<ul>
<li>The first line (header) define the name of the fields to import in the database.</li>
<li>If the header define a field 'file', this file will be store in the vault. This file must be in the wildspace</li>
<li>Be careful : the header define the field name and not the description show to the user</li>
<li>The next lines defines the value of each field.</li>
<li>You can define the header 'category_name' if you prefer set the usual name of the category in place to the id</li>
</ul>
</i>

<form action="import/documentfromcsv/index" method="post" name="step1" enctype="multipart/form-data">
<table class="normal">
  <tr>
   <td>
   {assign var='helphead' value='<b>document_number;file;natif_issue;category_id;document_supplier;document_version;description</b><br />'}
   {assign var='helpcore' value='number;file name;issue;category;supplier;indice;description'}
   {assign var='helphead' value='<b>document_number;file;natif_issue;category_id;document_supplier;document_version;description</b><br />'}
   {assign var='helpcore' value='numero;nom du fichier;issue;id de la categorie;fournisseur;id de l\'indice;description'}
    {tr}Batch upload{/tr} (CSV file<a {popup text="$helphead $helpcore"}><img src="img/icons/help.png" border="0" height="16" width="16" alt='{tr}help{/tr}' /></a>):
   </td>
   <td>
    <input type="file" name="importDocumentCsvList"/>
    <input type="submit" value="visucvs" name="submit" />
    <input type="hidden" name="container_id" value="{$container_id}" />
    <input type="hidden" name="space" value="{$SPACE_NAME}" />
   </td>
  </tr>
</table>
</form>


<h2 class="pagetitle">{tr}Or select a zip file{/tr}</h2>
<form action="import/documentfromzip/index" method="post" name="step1" enctype="multipart/form-data">
<table class="normal">
  <tr>
   <td>
    {assign var='helpheadzip' value='The zip file must contains the files to import without subdirectory<br />'}
    {assign var='helpcorezip' value=''}
    {tr}Batch upload{/tr}(zip file<a {popup text="$helpheadzip $helpcorezip"}><img src="img/icons/help.png" border="0" height="16" width="16" alt='{tr}help{/tr}' /></a>):
   </td>
   <td>
    <input type="file" name="importzipfile"/>
    <input type="submit" value="visuzipfile" name="submit" />
    <input type="hidden" name="container_id" value="{$container_id}" />
    <input type="hidden" name="space" value="{$SPACE_NAME}" />
   </td>
  </tr>
</table>
</form>


<!-- 
<h2 class="pagetitle">{tr}Or select the 'transfert data package(TDP)' file{/tr}</h1>
<form action="{$smarty.server.PHP_SELF}" method="post" name="step1" enctype="multipart/form-data">
<table class="normal">
  <tr>
   <td>
    {assign var='helpheadtdp' value='The tdp must be in accordance to the ECSS-M-50B<br />'}
    {assign var='helpcoretdp' value=''}
    {tr}Batch upload{/tr} (CSV file<a {popup text="$helpheadtdp $helpcoretdp"}><img src="img/icons/help.png" border="0" height="16" width="16" alt='{tr}help{/tr}' /></a>):
   </td>
   <td>
    <input type="file" name="importtdpfile"/>
    <input type="submit" value="visutdp" name="action" />
    <input type="hidden" name="container_id" value="{$container_id}" />
   </td>
  </tr>
</table>
</form>
-->
