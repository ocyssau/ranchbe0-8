{*Smarty template*}
{if $displayHeader eq 1}
{include file='header.tpl'}
{$form.javascript}

<div id="tiki-main">

<div id="tiki-top">
{include file="top_bar.tpl"}
</div>

<div id="tiki-mid">
{include file="tabs/tabs.tpl"}
<br>
<div id="tiki-center">

{*--------------------list header----------------------------------*}
<form name="step2" method="post" action="{$smarty.server.PHP_SELF}">

  {foreach item=property_name from=$fields}
    <input type="hidden" name="fields[]" value="{$property_name}" />
  {/foreach}

  <table class="normal">
    <tr>
      <th>{tr}{$form.document_number[$loop_id].label}{/tr}:<hr />
      {tr}{$form.file[$loop_id].label}{/tr}:</th>
      <th>{tr}{$form.description[$loop_id].label}{/tr}:</th>
      <th>{tr}{$form.document_version[$loop_id].label}{/tr}:</th>
      <th>{tr}{$form.category_id[$loop_id].label}{/tr}:</th>
      {section name=of loop=$optionalFields}
        {assign var="fn" value=$optionalFields[of].property_name}
        <th>{tr}{$form.$fn[$loop_id].label}{/tr}:</th>
      {/section}
      <th class="heading">
      {tr}Result and Actions{/tr}</a></th>
    </tr>
{/if}

{if $displayFooter eq 0}
  <tr>
    <td>{$form.document_number[$loop_id].html} <br />
        {$form.file[$loop_id].html}</td>
    <td>{$form.description[$loop_id].html}</td>
    <td>{$form.document_version[$loop_id].html}</td>
    <td>{$form.category_id[$loop_id].html}</td>
    <!-- Display optionnals fields -->
    <!-- TODO : revoir boucle imbriqu�e -->
    {section name=of loop=$optionalFields}
      {assign var="fn" value=$optionalFields[of].property_name}
      <td>{$form.$fn[$loop_id].html}</td>
    {/section}

    <td>
      <br />
      <font color="brown"><i> <!--Errors-->
        {foreach item=error from=$document.errors}
          - {$error}<br />
        {/foreach}</i>
      </font>
      {if $document.doctype.doctype_number}<!--Doctype-->
      <font color="green"><i>Recognize doctype : <b>{$document.doctype.doctype_number}</b></i><br /></font>{/if}
      <!--Actions-->
      <br />
        {$form.docaction[$loop_id].label}
        {$form.docaction[$loop_id].html}
      <br /><br />
   </td>

  </tr>
   <input type="hidden" name="loop" value="{$loop_id}" />
   <input type="hidden" name="document_id[{$loop_id}]" value="{$document.document_id}" />
   <input type="hidden" name="file_id[{$loop_id}]" value="{$document.file_id}" />

{/if}

{if $displayFooter eq 1}

  </table>

  <input type="submit" value="validatecsv" name="action" />
  <input type="submit" value="cancel" name="action" />

  <input type="hidden" name="page_id" value="documentImportZipfile" />

  </form>

  </div>
</div>

<div id="tiki-bot">
  {include file="bottom_bar.tpl"}
</div>

</div>

{/if}
