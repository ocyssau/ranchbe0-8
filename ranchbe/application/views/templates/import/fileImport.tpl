{*Smarty template*}

<div id="page-bar">
  <span class="button2"><a class="linkbut" href="javascript:popupP('importManager/history/get/space/{$SPACE_NAME}','{$randWindowName}',800 , 1200)">{tr}History import{/tr}</a></span>
</div>

<h1 class="pagetitle">{$PageTitle}</h1>

{*--------------------list header----------------------------------*}

{*--------------------Search Bar defintion--------------------------*}
<a id='choix' href="javascript:funcchoice()" style="text-decoration: none;"> <div class="hautboxScroll" style="text-align: center;">
<img id="imageFiltre" type="image" style='margin-top: 5px;' name="photo" src="./img/galaxia/icons/mini_inv_triangle.gif" value="cacher" />&nbsp;&nbsp;<b id="labelFiltre"style="color:#08639C;" >Afficher le filtre</b> </div></a>
<div id="englob" class="cacher"><div id="animDiv" class='boxScroll'>
{$views_helper_filter_form}
</div>
</div>


{literal}
<script type="text/javascript">
	initTbody();
	document.getElementById('choix').name = 0;
	var cptChoixToggle = 0;
</script>
{/literal}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">
{include file='import/_list.tpl'}

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>
<button class="mult_submit" type="submit" name="action" value="suppressPackage" title="{tr}Suppress{/tr}"
onclick="if(confirm('{tr}Do you want really suppress this file{/tr}')){ldelim}
document.checkform.action='./importManager/index/suppress';
pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="uncompressPackage" title="{tr}Uncompress{/tr}"
onclick="document.checkform.action='./importManager/index/uncompress';
pop_no(checkform);">
<img class="icon" src="./img/icons/package/package_uncompress.png" title="{tr}Uncompress{/tr}" alt="{tr}Uncompress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="update" title="{tr}Refresh{/tr}"
onclick="document.checkform.action='./importManager/index/get';
pop_no(checkform);">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p></p>

<table>
<tr><td>{tr}Data type{/tr} :<br></td></tr>
<tr class="formcolor"><td>
  <input type="radio" name="target" value="InTempdir" id="datatype1">
   <label for="datatype1">{tr}To put in a temp dir{/tr}</label></td></tr>

<tr class="formcolor"><td>
  <input type="radio" name="target" value="InContainer" id="datatype2">
   <label for="datatype2">{tr}To put in{/tr} {$SPACE_NAME}</label></td></tr>

<tr><td>{tr}Running mode{/tr} :<br></td></tr>
<tr class="formcolor"><td>
  <input type="radio" name="background" value="1" id=background1>
   <label for="background1">{tr}Launch Update in hidden mode{/tr}</label></td></tr>

<tr class="formcolor"><td>
  <input type="radio" name="background" value="0" id=background2>
   <label for="background2">{tr}Direct update{/tr}</label></td></tr>

<tr><td>
  <button class="mult_submit" type="submit" name="action" value="unpack" title="{tr}Update mockup{/tr}" id="01"
  onclick="document.checkform.action='./importManager/index/import';
  pop_it(checkform);">
  <img class="icon" src="./img/icons/tick.png" title="{tr}Update mockup{/tr}" alt="{tr}Update mockup{/tr}" width="16" height="16" />
  </button>
 </td>

</tr>
</table>

<p></p>

<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
<input type="hidden" name="ticket" value="{$ticket}" />
</form>


{* ------------------- Upload section ------------------------ *}
<form action="./importManager/index/upload" method="post" name="upload" enctype="multipart/form-data">
  <fieldset>
    <legend align="top"><i>{tr}Add a package to the package reposit dir{/tr}</i></legend>
    <input type="file" name="uploadFile"/>
    <input type="submit" value="upload" name="action" /><br />
    {tr}Overwrite{/tr}:
    <input type="checkbox" name="overwrite" />
  </fieldset>

  <input type="hidden" name="container_id" value="{$container_id}" />
  <input type="hidden" name="space" value="{$SPACE_NAME}" />
  <input type="hidden" name="ticket" value="{$ticket}" />
</form>
