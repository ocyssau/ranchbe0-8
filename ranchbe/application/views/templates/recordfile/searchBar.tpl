{*Smarty template*}

{*--------------------Search Bar defintion--------------------------*}
<div id="sitesearchbar">
<fieldset>
<form id="filterf" action="{$sameurl}" method="post">
<input type="hidden" name="activatefilter" value="1" />

<br />
  <label>
  <input size="4" type="text" name="numrows" value="{$filter_options.numrows|escape}" />
  <small>{tr}rows to display{/tr}</small></label>
<br />

  <label for="find_file_name"><small>{tr}file_name{/tr}</small></label>
  <input size="16" type="text" name="find_file_name" value="{$filter_options.find_file_name|escape}" />

  <label for="find_file_type"><small>{tr}file_extension{/tr}</small></label>
  <input size="5" type="text" name="find_file_type" value="{$filter_options.find_file_type|escape}" />

  <label for="find_import_order"><small>{tr}import_order{/tr}</small></label>
  <input size="5" type="text" name="find_import_order" value="{$filter_options.find_import_order|escape}" />

  <input type="submit" name="filter" value="{tr}filter{/tr}" />
  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />

<br />

{literal}
<script language="JavaScript">
function displayOption(cb,span){
    var a,b;
    var a = document.getElementById(cb);
    var b = document.getElementById(span);
    if(a.checked==true)
        b.style.display = "block";
    else b.style.display = "none";
}

function initOption(){
	displayOption('f_adv_search_cb', 'f_adv_search_span');
	displayOption('f_dateAndTime_cb', 'f_dateAndTime_span');
}
</script>
{/literal}

<input type="hidden" name="f_adv_search_cb" value="0" />
<label><small>{tr}Advanced filter{/tr}</small>
  <input type="checkbox" name="f_adv_search_cb" value="1" {if $filter_options.f_adv_search_cb}"checked"{/if} id="f_adv_search_cb" onClick="displayOption('f_adv_search_cb', 'f_adv_search_span');" />
</label>
<span id=f_adv_search_span style="display: none">
<fieldset>


  <label for="find"><small>{tr}find{/tr}</small></label>
  <input size="16" type="text" name="find" value="{$filter_options.find|escape}" />

  <label for="find_field"><small>{tr}In{/tr}</small></label>
	<select name="find_field" onchange='javascript:getElementById("filterf").submit();'>
	<option {if '' eq $find_field}selected="selected"{/if} value=""></option>
	{foreach from=$find_elements item=name key=field}
   <option {if $field eq $filter_options.find_field}selected="selected"{/if} value="{$field|escape}">{tr}{$name}{/tr}</option>
	{/foreach}
	</select>

  <label for="f_action_field"><small>{tr}Action{/tr}</small></label>
	<select name="f_action_field">
	<option {if $filter_options.f_action_field eq ''}selected="selected"{/if} value="-"></option>
	{foreach from=$user_elements item=name key=field}
   <option value="{$name}" {if $filter_options.f_action_field eq $name}selected="selected"{/if}>{tr}{$name}{/tr}</option>
	{/foreach}
	</select>

  <label for="f_action_user_name"><small>{tr}By{/tr}</small></label>
	<select name="f_action_user_name" size="1">
	<option {if $filter_options.f_action_user_name eq ''}selected="selected"{/if} value=""></option>
	{foreach from=$user_list item=user key=user_id}
    <option value="{$user_id}" {if $filter_options.f_action_user_name eq $user_id}selected="selected"{/if}>{$user.handle}</option>
	{/foreach}
	</select>


<label><small>{tr}Date and time{/tr}</small>
<input type="checkbox" name="f_dateAndTime_cb" value="1" id="f_dateAndTime_cb" onClick="displayOption('f_dateAndTime_cb', 'f_dateAndTime_span');" />
</label>
<span id=f_dateAndTime_span style="display: none">
<fieldset>

{literal}
<script type="text/javascript">
//<![CDATA[
    dojo.require("dijit.form.DateTextBox");
</script>
{/literal}

{foreach from=$date_elements item=name}
<div>
  <label><small>{tr}{$name}{/tr}</small>
  <input type="checkbox" name="{$name}_sel" value="1" id="{$name}_sel" onClick="displayOption('{$name}_sel','{$name}_span');" />
  </label>

  <span id={$name}_span style="display: none">
	  <fieldset>
	  <label><input type="radio" name="{$name}_cond" "checked" value=">"/>
	  <small>{tr}Superior to{/tr}</small></label>
	
	  <label><input type="radio" name="{$name}_cond" value="<" />
	  <small>{tr}Inferior to{/tr}</small></label>
	  <input id="{$name}" name="{$name}" value="" 
	  	type="text" dojoType="dijit.form.DateTextBox" style="width:150px" />
	  </fieldset>
  </span>
</div>
{/foreach}


</fieldset>
</fieldset>
</fieldset>
</fieldset>

</span>
</span>

</form>

</div>

{literal}
<script language="JavaScript">
  initOption();
</script>
{/literal}
