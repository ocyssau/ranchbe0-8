{*Smarty template*}

<table><tr>
<td width="50%">
  <table class="normal">
    {cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Basic properties{/tr}</td></tr>
    <tr class="{cycle}"><td>{tr}file_name{/tr}</td><td class="thin">{$fileinfos.file_name}</td></tr>
    <tr class="{cycle}"><td>{tr}file_path{/tr}</td><td class="thin">{$fileinfos.file_path}</td></tr>
    <tr class="{cycle}"><td>{tr}reposit_id{/tr}</td><td class="thin">{$fileinfos.reposit_id}</td></tr>
    <tr class="{cycle}"><td>{tr}file_size{/tr}</td><td class="thin">{$fileinfos.file_size|filesize_format}</td></tr>
    <tr class="{cycle}"><td>{tr}file_mtime{/tr}</td><td class="thin">{$fileinfos.file_mtime|date_format}</td></tr>
    <tr class="{cycle}"><td>{tr}file_md5{/tr}</td><td class="thin">{$fileinfos.file_md5}</td></tr>
    <tr class="{cycle}"><td>{tr}file_type{/tr}</td><td class="thin">{$fileinfos.file_type}</td></tr>

    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Life cycle{/tr}</td></tr>
    <tr class="{cycle}"><td>{tr}access{/tr}</td><td class="thin">{$fileinfos.file_access_code|access_code}</td></tr>
    <tr class="{cycle}"><td>{tr}State{/tr}</td><td class="thin">{$fileinfos.file_state}</td></tr>

    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Date and time{/tr}</td></tr>
    {if $fileinfos.check_out_by}

    <tr class="{cycle}"><td>{tr}CheckOut{/tr}</td><td class="thin">{tr}By{/tr} 
    {$fileinfos.file_checkout_by|username} 
    {tr}at{/tr} {$docinfos.file_checkout_date|date_format}</td></tr>
    {/if}

    <tr class="{cycle}"><td>{tr}Create{/tr}</td><td class="thin">{tr}By{/tr} 
    {$fileinfos.file_open_by|username} 
    {tr}at{/tr} {$fileinfos.file_open_date|date_format}</td></tr>

    <tr class="{cycle}"><td>{tr}Last Update{/tr}</td><td class="thin">{tr}By{/tr} 
    {$fileinfos.file_update_by|username} 
    {tr}at{/tr} {$fileinfos.file_update_date|date_format}</td></tr>

    {if $fileinfos.document_id}
    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Other properties{/tr}</td></tr>
    <tr class="{cycle}">
      <td>{tr}document_id{/tr}</td>
      <td class="thin">{$fileinfos.document_id}
       <a class="link" href="javascript:popupP('document/detail/getdetailwindow/document_id/{$fileinfos.document_id}/space/{$SPACE_NAME}',
                    'documentDetailWindow', 600 , 1024)">{tr}See details{/tr}</a>
      </td>
    </tr>
    {/if}

  </table>
</td>
</tr>
</table>
