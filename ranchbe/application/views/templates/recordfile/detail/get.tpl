{*Smarty template*}

{*================================= DETAIL OF FILE ===================================================*}
<h3>{tr}Detail of recordfile{/tr} {$fileinfos.file_name} - {$fileinfos.file_iteration}</h3>

<form name="checkform" method="post" action="{$sameurl}">

{include file="recordfile/_detail.tpl"}

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
onclick="if(confirm('{tr}Do you want really suppress this file{/tr}'))
{ldelim}document.checkform.action='./recordfile/index/suppress';
pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="putInWs" title="{tr}Put in wildspace{/tr}"
onclick="document.checkform.action='./recordfile/index/putinws' ; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_read.png" title="{tr}Put in wildspace{/tr}" alt="{tr}Put in wildspace{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="viewImported" value="viewImported" title="{tr}See package details{/tr}"
 onclick="document.checkform.action='./import/history/get/space/{$SPACE_NAME}/import_order/{$fileinfos.import_order}';
 pop_it(checkform)">
{tr}See package details{/tr}
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="document.checkform.action='{$sameurl}'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="file_id" value="{$file_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />

<input type="hidden" name="ifSuccessModule" value="recordfile" />
<input type="hidden" name="ifSuccessController" value="detail" />
<input type="hidden" name="ifSuccessAction" value="get" />

</form>

