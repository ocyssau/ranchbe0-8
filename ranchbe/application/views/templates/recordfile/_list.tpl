{*Smarty template*}

<div class="scroll" id="scroll">
<table class="scroll">
<thead class="fixedHeader">
<tr class="fixedHeader" >
    {* -------------------checkall box------------------------ *}
    <th class="heading">
    <input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxes(this.form,'file_id[]',this.checked)" />
    {$smarty.capture.menu}
    </th>

    {if $list.0.document_number}
    <th class="heading">
      {column_header sort_order='INV' sort_field='document_number' title='Document number'}
    </th>
    {/if}
  
    <th class="heading">
      {column_header sort_order='INV' sort_field='file_name'} . 
      {column_header sort_order='INV' sort_field='file_extension'} - 
      {column_header sort_order='INV' sort_field='file_iteration'}<br />
      (<i>{column_header sort_order='INV' sort_field='file_type'}</i>)
      {*(<i>{column_header sort_order='INV' sort_field='file_class'}</i>)*}<br />
      {column_header sort_order='INV' sort_field='file_access_code'} -
      {column_header sort_order='INV' sort_field='file_state'}
    </th>
  
    {if $list.0.import_order}
    <th class="heading">
      {column_header sort_order='INV' sort_field='import_order'}
    </th>
    </th>
    {/if}
  
    <th class="heading">
      {column_header sort_order='INV' sort_field='file_check_out_by'}/
      {column_header sort_order='INV' sort_field='file_check_out_date'}
      <br />
      {column_header sort_order='INV' sort_field='file_open_by'}/
      {column_header sort_order='INV' sort_field='file_open_date'}
      <br />
      {column_header sort_order='INV' sort_field='file_update_by'}/
      {column_header sort_order='INV' sort_field='file_update_date'}
    </th>
    <th class="heading">
      {column_header sort_order='INV' sort_field='file_path'}
      {column_header sort_order='INV' sort_field='file_size'}
      {column_header sort_order='INV' sort_field='file_mtime'}
      {column_header sort_order='INV' sort_field='file_md5'}
    </th>
  </tr>
</thead>
<tbody class="scrollContent" id="contentTbody">
{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="file_id[]" value="{$list[list].file_id}" /></td>

    {if $list.0.document_number}
    <td class="thin">
      <a  class="link" 
      href="javascript:popupP('document/detail/index/document_id/{$list[list].document_id}/space/{$SPACE_NAME}','documentDetailWindow', 600 , 1024)">
      {$list[list].document_number} - 
      {$list[list].document_version|indice}.
      {$list[list].document_iteration}</a>
    </td>
    {/if}

    <td class="thin">
      <a class="link" 
      	href="get/file/file_id/{$list[list].file_id}/space/{$SPACE_NAME}/object_class/{$object_class}/ticket/{$ticket}"
		title="{tr}View file{/tr}">
      {file_icon extension=$list[list].file_extension icondir=$file_icons_dir icontype='.gif'}
      {if $list[list].file_version}
      {$list[list].file_name} - {$list[list].file_version|indice}.{$list[list].file_iteration}
      {else}
      {$list[list].file_name}.{$list[list].file_iteration}
      {/if}
      </a>

      {if $list[list].file_iteration > 1 && 
         $object_class != 'recordfile' 
          && $object_class != 'docfile_iteration' 
      }<!--is version is used to hide this option when display version files for example-->
      <a href="javascript:popup('docfile/versions/get/docfile_id/{$list[list].file_id}/space/{$SPACE_NAME}','{$SPACE_NAME}Versions')" title="{tr}Versions{/tr}">
      {tr}See old versions{/tr}</a>
      {/if}

      <br />
      <i>({$list[list].file_type})</i>
      <br />
      {$list[list].file_access_code|access_code} - {$list[list].file_state}
      {if $list[list].file_mode == 1}
      <font color="red"><b>- {tr}AUTO_GENERATED{/tr}</b></font>
      {/if}
    </td>

    {if $list[list].import_order}
    <td class="thin" align="center">
      <a href="javascript:popupP('import/history/detail/id/{$list[list].import_order}/space/{$SPACE_NAME}','viewImported', 600 , 1024)" title="{tr}See package details{/tr}">
      <b><font size="2">{$list[list].import_order}</font></b></a><br />
      <font size="-3">(<i>{tr}Package{/tr} : {$list[list].import_order|import_package_name}</i>)</font>
    </td>
    {/if}

    <td class="thin">
      {if !empty($list[list].file_checkout_by)}
      <b>{tr}Checkout{/tr} : </b>{$list[list].file_checkout_by|username} - {$list[list].file_checkout_date|date_format}<br />
      {/if}
      <b>{tr}Created{/tr} : </b>{$list[list].file_open_by|username} - {$list[list].file_open_date|date_format}<br />
      <b>{tr}Updated{/tr} : </b>{$list[list].file_update_by|username} - {$list[list].file_update_date|date_format}<br />
    </td>

    <td class="thin">
      {*<b>{tr}file_path{/tr}:</b> {$list[list].file_path}<br />*}

    <b>{tr}in reposit{/tr}:</b>
    <a class="link" 
       href="javascript:popupP('reposit/detail/get/reposit_id/{$list[list].reposit_id}',
              'detailReposit', 400 , 520)">
    {$list[list].reposit_id|reposit}</a><br />

      <b>{tr}file_size{/tr}:</b> {$list[list].file_size|filesize_format}<br />
      <b>{tr}file_mtime{/tr}:</b> {$list[list].file_mtime|date_format}<br />
      <b>{tr}file_md5{/tr}:</b> {$list[list].file_md5}<br />
    </td>

   </tr>
  {/section}
  </tbody>
  </table></div>
  
  
{literal}
<script type="text/javascript">
	initTbody();
</script>
{/literal}
  
