{*Smarty template*}

<FIELDSET>
<LEGEND><a href="#" class="pagetitle">{tr}{$PageTitle}{/tr}</a></LEGEND>
<table border=0>
<tr>
<td><img src="{$logo}">
<br />
<i>RANCHBE</i>&#169; {$ranchbe_version} {$ranchbe_build}
</td>

<td>
</td><td>
<form method="post" action="{$this->baseUrl('auth/auth/login')}"> {*logout=0 else next request failed because keep logout=true in session*}
    <table style="border:1px; margin-left: 12px;">
        <tr>
            <td>login:</td>
            <td><input name="username" type="text" size="15" maxlength="15" value="{$username}" /></td>
        </tr>

        <tr>
            <td>password:</td>
            <td><input name="passwd" type="password" size="15" maxlength="15" /></td>
        </tr>

        <tr>
            <td colspan="2"><input type="submit" value="Login" /></td>
        </tr>

        <tr><td colspan="10">
          {foreach item=message from=$messages}
            <i>{$message}</i>
          {/foreach}
        </td></tr>

    </table>
<input type="hidden" name="forwardUrl" value="{$forwardUrl}">
<input type="hidden" name="logout" value="0">{*if not after click on logout re-connexion becomes impossible (except close mozilla or bidouiller the url)*}
{$hidden_serialized_post}
</form>

</tr>
</table>
</FIELDSET>
