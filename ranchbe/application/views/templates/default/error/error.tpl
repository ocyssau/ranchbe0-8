<div id="tiki-main">
  <div id="tiki-mid"> <br />
    <div class="cbox">
      <div class="cbox-title">
        {tr}{$message}{/tr}
      </div>
      <div class="cbox-data"> <br />
      	<h3>{tr}Exception information{/tr}:</h3>
      	{$exception->getMessage()} <br />
      	<h3>{tr}Stack trace{/tr}:</h3>
		<pre>{$exception->getTraceAsString()}</pre>
		<h3>{tr}Request Parameters{/tr}:</h3>
		<pre>{$parameters}</pre>
        <br /><br />
        <a href="javascript:history.back()" class="linkmenu">{tr}Go back{/tr}</a> |
        <a href="{$this->baseUrl('/home')}" class="linkmenu">{tr}Return to home page{/tr}</a> |
        <br /><br />
      </div>
    </div>
  </div>
</div>

