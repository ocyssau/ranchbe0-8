{*Smarty template*}

{literal}
<style type="text/css">

  /* messages box */
  div.messu_notification {
    overflow: hidden;
  	position: absolute;
    text-align: center ;
  	top:   10px;
  	right:   10px;
  	height:   80px;
  	width:   80px;
  	z-index:   1000;
  	background-image: url(./img/splash_star.png);
    background-repeat: no-repeat;
    background-position: center center;
  	margin:   0px;
  	border:   0px;
  	padding:   0px;
  }

  div.center {
    position: relative;
  	top:   0px;
  	right:   0px;
    overflow: hidden;
  	top: 20%;
  	z-index:   1001;
  }

</style>
{/literal}

<div class="messu_notification" id="messu_notification">
  <div class="center">
    <b>
    <a class="link" href="./messages/mailbox/index">{$message_notification}</a>
    </b>
  </div>
</div>


