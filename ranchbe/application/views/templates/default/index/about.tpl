
<table border=0>
<tr>
  <td><img src="img/ranchBe_logo_190__.gif" alt="ranchbe"/></td>
  <td><h1 class="pagetitle">About RanchBE</h1>
  <b>RanchBE</b> {$version.ranchbe_copyright} {$version.ranchbe_ver} {$version.ranchbe_build} <a href="http://www.ranchbe.com">www.ranchbe.com</a>
  <p>
    <b>RanchBE&#169;</b> is free software, you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    any later version.
  </p>
  <p>
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY, without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  </p>
  <p>
	<a href="{$this->baseUrl('index/getlicence')}" title="licence">See the GNU General Public License for more details.</a>
  </p>  

</td></tr>
</table>
<div id="power" style="text-align: center">
	<a href="http://framework.zend.com/" title="ZEND"><img style="border: 0; vertical-align: middle" alt="Powered by ZEND" src="img/PoweredBy_ZF_4LightBG.png" /></a>
	<a href="http://www.php.net/" title="PHP"><img style="border: 0; vertical-align: middle" alt="Powered by PHP" src="img/php.png" /></a>
	<a href="http://smarty.php.net/" title="Smarty"><img style="border: 0; vertical-align: middle" alt="Powered by Smarty" src="img/smarty.gif"  /></a>
	<a href="http://adodb.sourceforge.net/" title="ADOdb"><img style="border: 0; vertical-align: middle" alt="Powered by ADOdb" src="img/adodb.png" /></a>
	<a href="http://www.w3.org/Style/CSS/" title="CSS"><img style="border: 0; vertical-align: middle" alt="Made with CSS" src="img/css1.png" /></a>
  <br />
	<a href="http://tikiwiki.org/" title="TikiWiki">This software reused code from TikiWiki. Thanks to TikiWiki staff</a>
  <br />
</div>

