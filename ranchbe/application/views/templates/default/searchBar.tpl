{*Smarty template*}

{*--------------------Search Bar defintion--------------------------*}
<div id="sitesearchbar">
<fieldset>
<form id="filterf" action="{$sameurl}" method="post">
<input type="hidden" name="activatefilter" value="1" />
<input type="hidden" name="f_adv_search_cb" value="1" />

<br />
  <label>
  <input size="4" type="text" name="numrows" value="{$filter_options.numrows|escape}" />
  <small>{tr}rows to display{/tr}</small></label>
<br />

  <label for="find_file_name"><small>{tr}file_name{/tr}</small></label>
  <input size="16" type="text" name="find_file_name" value="{$filter_options.find_file_name|escape}" />

  <label for="find_file_type"><small>{tr}file_extension{/tr}</small></label>
  <input size="5" type="text" name="find_file_type" value="{$filter_options.find_file_type|escape}" />

  <label for="find"><small>{tr}find{/tr}</small></label>
  <input size="16" type="text" name="find" value="{$filter_options.find|escape}" />

  <label for="find_field"><small>{tr}In{/tr}</small></label>
	<select name="find_field" onchange='javascript:getElementById("filterf").submit();'>
	<option {if '' eq $filter_options.find_field}selected="selected"{/if} value=""></option>
	{foreach from=$find_elements item=name key=field}
   <option {if $field eq $filter_options.find_field}selected="selected"{/if} value="{$field|escape}">{$name}</option>
	{/foreach}
	</select>

  <label for="f_action_field"><small>{tr}Action{/tr}</small></label>
	<select name="f_action_field">
	<option {if $filter_options.f_action_field eq ''}selected="selected"{/if} value=""></option>
	{foreach from=$user_elements item=name key=field}
   <option value="{$name}" {if $filter_options.f_action_field eq $name}selected="selected"{/if}>{$name}</option>
	{/foreach}
	</select>

  <label for="f_action_user_name"><small>{tr}By{/tr}</small></label>
	<select name="f_action_user_name">
	<option {if $filter_options.f_action_user_name eq ''}selected="selected"{/if} value=""></option>
	{foreach from=$user_list item=name}
   <option value="{$name.auth_user_id}" {if $filter_options.f_action_user_name eq $name.auth_user_id}selected="selected"{/if}>{$name.handle}</option>
	{/foreach}
	</select>

  <input type="submit" name="filter" value="{tr}filter{/tr}" />
  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />

<br />

{literal}
<script language="JavaScript">
function displayOption(cb,span){

    var a,b;

    var a = document.getElementById(cb);
    var b = document.getElementById(span);
    if(a.checked==true)
        b.style.display = "block";
    else b.style.display = "none";

}
</script>
{/literal}

<label><small>{tr}Date and time{/tr}</small>
<input type="checkbox" name="f_dateAndTime_cb" value="1" id="f_dateAndTime_cb" onClick="displayOption('f_dateAndTime_cb', 'f_dateAndTime_span');" />
</label>
<span id=f_dateAndTime_span style="display: none">
<fieldset>

{*include_css file1="jscalendar/calendar-win2k-1.css"*}
{*include_js file1="jscalendar/calendar.js" file2="jscalendar/lang/calendar-fr.js" file3="jscalendar/calendar-setup.js"*}

{literal}
<script type="text/javascript">
function setupcal(inputfield, displayarea){
  Calendar.setup({
      inputField     :    inputfield,     // id of the input field
      displayArea    :    displayarea,       // ID of the span where the date is to be shown
      ifFormat       :    "%s",     // format of the input field (even if hidden, this format will be honored)
      daFormat       :    "%Y/%m/%d %H:%M:%S",// format of the displayed date
      showsTime      :    true, // display hours 
      singleClick    :    false
  });
}
</script>
{/literal}

{foreach from=$date_elements item=name}
  <label><small>{tr}{$name}{/tr}</small>
  <input type="checkbox" name="{$name}_sel" value="1" id="{$name}_sel" onClick="displayOption('{$name}_sel','{$name}_span');" />
  </label>

  <span id={$name}_span style="display: none">
  <fieldset>
  <input type="hidden" name="{$name}" value="" id="{$name}"/>

  <label><input type="radio" name="{$name}_cond" "checked" value=">"/>
  <small>{tr}Superior to{/tr}</small></label>

  <label><input type="radio" name="{$name}_cond" value="<" />
  <small>{tr}Inferior to{/tr}</small></label>
  <span style="background-color: rgb(255, 255, 136);" id="show_{$name}">Click</span>
  </fieldset>
  </span>

  {literal}
  <script type="text/javascript">
  setupcal('{/literal}{$name}{literal}', 'show_{/literal}{$name}{literal}');
  </script>
  {/literal}
<br />
{/foreach}



<br />

</fieldset>
</fieldset>

</span>

</form>

</div>
