{*Smarty template*}

<h1>{$PageTitle}</h1>

{if $successMessage}
	<p style="color:green;">{$successMessage}</p>
	
	{literal}
	<script type="text/javascript">
		document.onload = window.parent.opener.location.reload();
	</script>
	{/literal}
	
{/if}
{if $failedMessage}
	<p style="color:red;">{$failedMessage}</p>
{/if}

{$form}
