{*Smarty template*}

<h1 class="pagetitle">{tr}Configuration{/tr}</h1>


<table>
<tr><td>

<div class="menubg flt">
<img width="252" height="4"  class="top flt" />
	<ul class="menu flt">
		<li class=""><a href="tools/index/editconfig?indice_file=lang" >{tr}Language{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=phpsettings" >{tr}PHP Settings Config{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=adodb" >{tr}Data base{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=log" >{tr}Log config{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=smarty" >{tr}Smarty config{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=layout" >{tr}Layout config{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=date" >{tr}Date config{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=path" >{tr}Reposits paths config{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=icons" >{tr}Icons and visu config{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=menu" >{tr}Menu config{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=mail" >{tr}Mail config{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=externaltools" >{tr}External tools{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=galaxia" >{tr}Galaxia workflow engine config{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=mask" >{tr}Objects basic configs{/tr}</a></li>
		<li class=""><a href="tools/index/editconfig?indice_file=security" >{tr}Security rules{/tr}</a></li>
	</ul>	
	<img  width="252" height="3" class="bot flt" />
</div>

</td></tr>
</table>
