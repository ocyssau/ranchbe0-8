{*Smarty template*}

<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<h1 class="pagetitle">{tr}Files in trash{/tr}</h1>

{*--------------------list header----------------------------------*}
<table class="normal">
 <tr>
  <th class="heading auto"></th>

  <th class="heading">{tr}original_file{/tr}</th>
  <th class="heading">{tr}file_name{/tr}</th>
  <th class="heading">{tr}file_path{/tr}</th>
  <th class="heading">{tr}file_size{/tr}</th>
  <th class="heading">{tr}file_mtime{/tr}</th>

 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].file_name}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>
    <td class="thin">{$list[list].original_file}</td>
    <td class="thin">{$list[list].file_name}</td>
    <td class="thin">{$list[list].file_path}</td>
    <td class="thin">{$list[list].file_size|filesize_format}</td>
    <td class="thin">{$list[list].file_mtime|date_format}</td>
   </tr>
  {/section}
  </table>

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>
        <br>

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

</form>
