{*Smarty template*}

<h1 class="pagetitle">{tr}Update doctype icon{/tr}</h1>

<ul>
{section name=success loop=$success}
	<li>
	{tr}compile icon {$success[success].icon} for doctype {$success[success].doctype_number}{/tr}
	</li>
{/section}
</ul>
<ul>
{section name=failed loop=$failed}
	<li>
	{tr}COMPILE FAILED for icon {$failed[failed].icon} for doctype {$failed[failed].doctype_number}{/tr}
	</li>
{/section}
</ul>
