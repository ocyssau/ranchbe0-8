{*Smarty template*}

<h1 class="pagetitle">{tr}Update thumbnails{/tr}</h1>

{section name=result loop=$results}
	<ul><li>
	{tr}Update thumbnail of document {$results[result].document_number} ({$results[result].document_id}){/tr}
	</li></ul>
{sectionelse}
    {tr}None document{/tr}
{/section}
  
  