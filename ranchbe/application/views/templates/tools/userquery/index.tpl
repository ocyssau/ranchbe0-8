{*Smarty template*}

<h1 class="pagetitle">{tr}User queries{/tr} {$checked}</h1>

{*--------------------Search Bar defintion--------------------------*}
{$views_helper_filter_form|toggleanim}

<form id="checkform" name="checkform" method="post" action="{$sameurl}">
	{* -------------------Pagination------------------------ *}
	{$views_helper_pagination}
	
	{*--------------------list header----------------------------------*}
	<div class="scroll" id="scroll">
	<table class="scroll" onLoad="load();">
	<thead class="fixedHeader">
	 <tr class="fixedHeader" >
	  <th class="scroll" width="50px">
	  	  <div dojoType="dijit.form.DropDownButton" iconClass="noteIcon" height="width=0px" onLoad="load(this.id);" class="cacher" id="dojodiv">
				  <span></span>
				  <div dojoType="dijit.Menu">
					    <div dojoType='dijit.MenuItem'  onClick="document.checkform.action='./tools/userquery/zipdownload'; pop_no(document.checkform)" >
					    	<img border='0'  src="./img/icons/document/document_zip.png" />{tr}Download zip{/tr}
					    </div>
					    
						<div dojoType='dijit.MenuItem'  onClick="document.checkform.action='./tools/userquery/uncompress'; pop_no(document.checkform)" >
					    	<img border='0'   src='./img/icons/package/package_uncompress.png' />{tr}Uncompress{/tr}
					    </div>
					    
					    <div dojoType='dijit.MenuSeparator'></div>
					    
					    <div dojoType='dijit.MenuItem'  onClick="document.checkform.action='./tools/userquery/index'; pop_no(document.checkform)" >
					    	<img border='0'  src='./img/icons/refresh.png' />{tr}Refresh{/tr}
					    </div>
					    
					    <div dojoType='dijit.MenuItem'  onClick="if(confirm('{tr}Do you want really suppress this files{/tr}')){ldelim}document.checkform.action='./tools/userquery/suppressfile';pop_no(document.checkform){rdelim}else {ldelim}return false;{rdelim}" >
					        <img border='0'  src='./img/icons/trash.png' />{tr}Suppress{/tr}
					    </div>
					    
				  </div>
			</div>
	  </th>
	
	  {*-Specifics fields-*}
	  <th class="heading" nowrap>
	    {column_header sort_order='INV' sort_field='file_name' displayMd5=$displayMd5}.
	    {column_header sort_order='INV' sort_field='file_extension' displayMd5=$displayMd5}
	    ({column_header sort_order='INV' sort_field='file_type' displayMd5=$displayMd5})
	  </th>
	
	  <th class="heading">
	    {column_header sort_order='INV' sort_field='file_mtime' displayMd5=$displayMd5}
	  </th>
	
	  <th class="heading">
	    {column_header sort_order='INV' sort_field='file_size' displayMd5=$displayMd5}
	  </th>
	
	  {if $filter_options.displayMd5}
	  <th class="heading">
	    {column_header sort_order='INV' sort_field='file_md5' displayMd5=$displayMd5}
	  </th>
	  {/if}
	 </tr>
	 </thead>
	 
	 <tbody class="scrollContent" id="contentTbody">
	 
	{*--------------------list body---------------------------*}
	{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
	  {section name=list loop=$list}
	   <tr class="{cycle}">
	   
	    <td class="thin" style="text-align: center; width:50px;">
	    <input type="checkbox" name="file_name[]" value="{$list[list].file_name}" 
	    {if $list[list].checked eq 'y'}checked="checked" {/if}/>
	    </td>
	    
	    {*-Specifics fields-*}
	    <td nowrap>
	    
	    <div>&nbsp;{file_icon extension=$list[list].file_extension}&nbsp;&nbsp;
	    <a class="link" href="tools/userquery/execute/file_name/{$list[list].file_name}" 
	    title="{tr}Execute{/tr}">
	    
	    {$list[list].natif_file_name}</a>
	    ({$list[list].file_type})
	    
	    <div class="cacher"  dojoType='dijit.form.DropDownButton' iconClass='noteIcon' onclick="menuAjax(this.id,{$list[projects].project_id});" onLoad="load(this.id);">
	  		<span>Actions</span>
			  <div dojoType='dijit.Menu'>
	
				<div dojoType='dijit.MenuItem'
					onclick="document.location.href='./tools/userquery/execute/file_name/{$list[list].file_name}'" >
			        {file_icon extension=$list[list].file_extension}
			        {tr}Execute{/tr}
			    </div>

				<div dojoType='dijit.MenuItem'  
					onclick="document.location.href='tools/userquery/download/file_name/{$list[list].file_name}'" >
			        {file_icon extension=$list[list].file_extension}
			        {tr}Download{/tr}
			    </div>
			    
			    <div dojoType='dijit.MenuItem' 
			    	title="{tr}Rename{/tr}"
	         		onClick="newNameInput('tools/userquery/renamefile/file_name/{$list[list].file_name}/ticket/{$ticket}','{$list[list].file_name}','{tr}Rename: input the new file name{/tr}');return false;">
			        <img border='0'  src='img/icons/refresh.png' />{tr}Rename{/tr}
			    </div>
			    
			    <div dojoType='dijit.MenuItem' 
			    	title="{tr}Copy{/tr}"
	         		onClick="newNameInput('tools/userquery/copyfile/file_name/{$list[list].file_name}/ticket/{$ticket}','{$list[list].file_name}','{tr}Copy: input the new file name{/tr}');return false;">
			        <img border='0'  src='img/icons/copy.gif' />{tr}Copy{/tr}
			    </div>

			    <div dojoType='dijit.MenuItem' title="{tr}Edit{/tr}"
					onclick="document.location.href='tools/userquery/edit/file_name/{$list[list].file_name}'" >
			        <img border='0'  src='img/icons/edit.png' />{tr}Edit{/tr}
			    </div>

		  </div>
		</div>
	    </td>
	    
	    <td class="thin">{$list[list].file_mtime|date_format}</td>
	    <td class="thin">{$list[list].file_size|filesize_format}</td>
	    {if $list[list].file_md5}<td class="thin">{$list[list].file_md5}</td>{/if}
	   </tr>
	  {/section}
	  </tboby>
	  </table></div>
	<input type="hidden" name="ticket" value="{$ticket}" />
</form>



{* ------------------- Upload section ------------------------ *}
<fieldset>
<legend align="top"><i>{tr}Add a query file{/tr}</i></legend>
<form action="tools/userquery/upload" method="post" name="upload" enctype="multipart/form-data">
    <input type="file" name="uploadFile"/>
    <input type="submit" value="upload" name="action" />
    <br />{tr}Overwrite{/tr}: <input type="checkbox" name="overwrite" />
</form>
</fieldset>



{literal}
<script type="text/javascript">
initTbody();

function newNameInput(monlien,file_name,title){
  var newName = prompt(title, file_name );
  if(newName == null){
    return false;
  }
  document.location.href = monlien + '/newName/' + newName;
  return true;
}
 
</script>
{/literal}



