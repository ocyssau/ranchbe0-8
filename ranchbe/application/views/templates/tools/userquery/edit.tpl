{*Smarty template*}

<h1 class="pagetitle">{tr}User sql query edit{/tr}</h1>

<form id="edit_query" action="./tools/userquery/save" method="post">
  <input type="hidden" name="file_name" value={$file_name} />
  <textarea cols="128" rows="32" name="sql_query" >{$sql_query}</textarea>
  <br />
  <input type="submit" name="save" value="{tr}save{/tr}" />
  <input type="submit" name="cancel" value="{tr}cancel{/tr}" />
</form>
