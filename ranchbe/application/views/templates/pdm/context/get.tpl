{*Smarty template*}

{*--------------------Actions defintion--------------------------*}
{include file='pdm/context/_actions.tpl'}

<br />

<h1 class="pagetitle">{$PageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
{*assign var="histoOption" value="1"*}
<a id='choix' href="javascript:funcchoice()" style="text-decoration: none;"> <div class="hautboxScroll" style="text-align: left;">
<img id="imageFiltre" type="image" style='margin-top: 5px;' name="photo" src="./img/galaxia/icons/mini_inv_triangle.gif" value="cacher" />&nbsp;&nbsp;<b id="labelFiltre"style="color:#08639C;" >Afficher le filtre</b> </div></a>
<div id="englob" class="cacher"><div id="animDiv" class='boxScroll'>
{$views_helper_filter_form}
</div>
</div>


{literal}
<script type="text/javascript">
	initTbody();
	document.getElementById('choix').name = 0;
	var cptChoixToggle = 0;
</script>
{/literal}

{* -------------------Pagination------------------------ *}
{*$views_helper_pagination*}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="./pdm/context/index">
<table class="normal">
 <tr>
  <th class="heading auto">
    <input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxes(this.form,'cta_id[]',this.checked)" />
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field='name' title='name'}
  </th>
  <th class="heading">
  {column_header sort_order='INV' sort_field='application' title='application'}
  </th>
  <th class="heading">
  {column_header sort_order='INV' sort_field='life_cycle_stage' title='life_cycle_stage'}
  </th>
  <th class="heading">
  {column_header sort_order='INV' sort_field='context_application_id' title='context_application_id'}
  </th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="cta_id[]" 
                            value="{$list[list].cta_id}"/>
    </td>
    
    <td nowrap>{$list[list].name}</td>
    <td nowrap>{$list[list].application}</td>
    <td nowrap>{$list[list].life_cycle_stage}</td>
    <td nowrap>{$list[list].context_application_id}</td>
   </tr>
  {sectionelse}
    <tr><td colspan="6">{tr}Nothing to display{/tr}</td></tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}
{*$views_helper_pagination*}

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
onclick="if(confirm('{tr}Do you want really suppress this{/tr}'))
{ldelim}document.checkform.action='./pdm/context/suppress';
pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
onclick="document.checkform.action='./pdm/context/index'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="ticket" value="{$ticket}" />
</form>
