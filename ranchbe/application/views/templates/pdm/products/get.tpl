{*Smarty template*}

{*--------------------Actions defintion--------------------------*}
{include file='pdm/products/_actions.tpl'}

<br />

<h1 class="pagetitle">{$PageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
{*assign var="histoOption" value="1"*}
<a id='choix' href="javascript:funcchoice()" style="text-decoration: none;"> <div class="hautboxScroll" style="text-align: left;">
<img id="imageFiltre" type="image" style='margin-top: 5px;' name="photo" src="./img/galaxia/icons/mini_inv_triangle.gif" value="cacher" />&nbsp;&nbsp;<b id="labelFiltre"style="color:#08639C;" >Afficher le filtre</b> </div></a>
<div id="englob" class="cacher"><div id="animDiv" class='boxScroll'>
{$views_helper_filter_form}
</div>
</div>


{literal}
<script type="text/javascript">
    initTbody();
	document.getElementById('choix').name = 0;
	var cptChoixToggle = 0;
</script>
{/literal}

{* -------------------Pagination------------------------ *}
{*$views_helper_pagination*}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="./pdm/context/index">
<table class="normal">
 <tr>
  <th class="heading auto">
    <input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxes(this.form,'cta_id[]',this.checked)" />
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field='p_number' title='p_number'}
  .{column_header sort_order='INV' sort_field='version_id' title='version_id'}
  <br />{column_header sort_order='INV' sort_field='p_name' title='p_name'}
  <br />{column_header sort_order='INV' sort_field='p_description' title='p_description'}
  </th>
  <th class="heading">
  {column_header sort_order='INV' sort_field='document_id' title='document'}
  </th>
  <th class="heading">
  {column_header sort_order='INV' sort_field='ct_id' title='context'}
  </th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="pd_id[]" 
                            value="{$list[list].pd_id}"/>
    </td>
    
    <td nowrap>
      {$list[list].p_number}.{$list[list].version_id}({$list[list].p_name})<br />
      <i>{$list[list].p_description}</i>
      
      <br /><br />

      <span class="button2"
              onclick="toggleLine('{$list[list].pd_id}','imgtoggle_{$list[list].pd_id}')">
        {tr}Actions{/tr}
        <img src="img/icons/untoggle.png" id="imgtoggle_{$list[list].pd_id}">
      </span>
      <!-- Action hide menu -->
      <ul class="actionlist" 
          style="display:none;overflow:hidden;height:0px;margin:5;padding:5;"
          id="{$list[list].pd_id}">
          {product_action_button
            infos=$list[list]
            ticket=$ticket
            displayText = true
            mode='list'
            ifSuccessModule='pdm'
            ifSuccessController='products'
            ifSuccessAction='index'
            ifFailedModule='pdm'
            ifFailedController='products'
            ifFailedAction='index'
          }
      </ul>
    </td>
    <td nowrap>{$list[list].document_id}</td>
    <td nowrap>{$list[list].ct_id|context}</td>
   </tr>
  {sectionelse}
    <tr><td colspan="6">{tr}Nothing to display{/tr}</td></tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}
{*$views_helper_pagination*}

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
onclick="document.checkform.action='./pdm/products/index'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="ticket" value="{$ticket}" />
</form>
