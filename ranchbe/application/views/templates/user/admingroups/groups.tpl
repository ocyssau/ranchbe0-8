{*Smarty template*}
<div id="groups_list" style="
  height: auto;
	width: 600px;
">

{*--------------------Group list header----------------*}
<table class="normal" >
			<tr >
			  <th class="heading">
				<div id="displaySelectedRowCount"></div>
			  </th>
			  <th class="heading">{tr}groups{/tr}</th>
			  <th class="heading">{tr}members{/tr}</th>
			</tr>
			{*--------------------Group list body----------------*}
			{cycle values="even,odd" print=false}
			{foreach key=group_id item=group from=$groups}
			 <tr class="{cycle}" >
			
			    <td style="text-align: center;">
			     <input type="checkbox" name="group_id[]"
			            value="{$group_id}" {if $users[user].checked eq 'y'}checked="checked" {/if}/>
			    </td>
			    <td>
			      <a class="link"
			         href="user/admingroups/edit/group_id/{$group_id}">
			         <img border="0" 
			              alt="{tr}Edit{/tr}"
			              title="{tr}edit account settings for{/tr}: {$group.group_define_name}"
			              src="img/icons/edit.png" />
			      </a>
			      <a class="link"
			          href="user/admingroups/assigngroup/group_id/{$group_id}">
			          <img border="0" 
			              alt="{tr}Assign Group{/tr}"
			              title="{tr}Assign Group{/tr}"
			              src="img/icons/group_assign.png" />
			      </a>
			      <a class="link"
			          href="user/admingroups/suppress/group_id/{$group_id}">
			          <img border="0" 
			              alt="{tr}delete{/tr}"
			              title="{tr}delete{/tr}"
			              src="img/icons/trash.png" />
			      </a>
			      {$group.group_define_name} ({$group.group_description})
			    </td>
			    <td>
			      {foreach from=$group.groups item=group key=group_id}
			        <img border="0" alt="" src="img/icons/group_link.png" />
			        {$group.group_define_name}
			      {/foreach}
			    </td>
			 </tr>
		{/foreach}
</table>
</div>

