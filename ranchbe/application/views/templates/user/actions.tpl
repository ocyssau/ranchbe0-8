{*Smarty template*}

<table>
<tr><td>
	<a href="user/admingroups/get" class="button">
		<span class="group">{tr}Admin groups{/tr}</span>
	</a>
</td><td>
	<a href="user/adminusers/get" class="button">
		<span class="user">{tr}Admin users{/tr}</span>
	</a>
</td><td>
	<a href="user/adminusers/create" class="button">
		<span class="adduser">{tr}New user{/tr}</span>
	</a>
</td><td>
	<a href="user/admingroups/create" class="button">
		<span class="addgroup">{tr}New group{/tr}</span>
	</a>
</td><td>
	<a href="javascript:popupP('user/perms/get/resource_id/1','userPerms',550,650 )" class="button">
		<span class="key">{tr}Set permissions{/tr}</span>
	</a>
</td></tr>
</table>