{capture assign=content}
  {section name=errors loop=$errors}
    {if $errors[errors].level == 'critical' || $errors[errors].level == 'alert' || $errors[errors].level == 'emergency'}
      <p class="fatal-text">
      <img border="0" alt="FATAL" src="img/fatal60.png" width=30px />
      {$errors[errors].message}</p>
    {elseif $errors[errors].level == 'error'}
      <p class="error-text">
      <img border="0" alt="ERROR" src="img/error60.png" width=30px />
      {$errors[errors].message}</p>
    {elseif $errors[errors].level == 'warning' || $errors[errors].level == 'notice'}
      <p class="warning-text">
      <img border="0" alt="WARNING" src="img/warning60.png" width=30px />
      {$errors[errors].message}</p>
    {else}
      <p class="info-text">
      <img border="0" alt="INFO" src="img/info60.png" width=30px />
      {$errors[errors].message}</p>
    {/if}
    {if $debug}
      <p class="debug-text">
      <b>Error level</b> :  {$errors[errors].level}<br />
      <b>Code</b> :         {$errors[errors].code}<br />
      <b>For package</b> :  {$errors[errors].package}<br />
      <b>In class</b> :     {$errors[errors].context.class}<br />
      <b>In function</b> :  {$errors[errors].context.function}<br />
      <b>In file</b> :      {$errors[errors].context.file}<br />
      <b>In line</b> :      {$errors[errors].context.line}<br />
      {if $errors[errors].params.query}
      <b>SQL Request</b> :  {$errors[errors].params.query}<br />
      {/if}
      {if $errors[errors].debug}
        <b>Debug</b> : 
        {foreach from=$errors[errors].params.debug key=key item=item }
          {$key}={$item}<br />
        {/foreach}
      <br />
      {/if}
      </p>
    {/if}
  {/section}
  {if $close_button}
    <input type="button" onclick="window.close()" value="{tr}Close Window{/tr}" title="{tr}Close Window{/tr}">
  {/if}
  {if $back_button}
    <input type="button" onclick="javascript:history.back()" value="{tr}Go back{/tr}" title="{tr}Go back{/tr}">
  {/if}
  {if $home_button}
    <form action="{$this->baseUrl('home/index/index')}"><input type="submit" value="{tr}Return to home page{/tr}" title="{tr}Return to home page{/tr}"></form>
  {/if}
{/capture}

{*
<!-- <script type="text/javascript" src="js/dojo/dojo.js"></script> -->
*}

{literal}
<script type="text/javascript">
  dojo.require("dojox.layout.ResizeHandle");
  dojo.require("dojo.dnd.Mover");
  dojo.require("dojo.dnd.Moveable");
  dojo.require("dojo.dnd.move");

  var m1;
  var initDND = function(){
    //Doing movable error win
    m1 = new dojo.dnd.Moveable("executionReport", {handle: "moveHandle"} ); //click in "moveHandle" to move window
    //Doing resizable error win
		var handle = document.createElement('div'); //Create a picture handle in the bottom right corner
		dojo.byId("executionReportFoot").appendChild(handle);
		var resizer = new dojox.layout.ResizeHandle({
			targetId: "executionReport",
			activeResize: true
		},handle);
		resizer.startup();
  };
  dojo.addOnLoad(initDND);

  function closeMbox(id){
    element = document.getElementById(id);
    element.style.visibility = 'hidden';
    return true;
  }

</script>

<style type="text/css">
	
  .dojoxResizeHandle
  {
    position: absolute;
    right: 2px;
    bottom: 2px;
    width: 13px;
    height: 13px;
    z-index: 20;
    background-image: url(img/icons/resize.png);
    line-height: 0px;
  }

  .dojoxResizeNW
  {
  cursor: nw-resize;
  }

</style>
{/literal}

<div class="mbox" id="executionReport">
  <div class="mbox-title" id="moveHandle">{tr}Execution report{/tr}</div>
  <img class="mbox-closebutton" src="img/icons/cross.png" onClick="closeMbox('executionReport');" title="{tr}Close{/tr}">
  <div class="mbox-data" id="executionReportData">
  {$content}
  </div>
  <div class="mbox-foot" id="executionReportFoot"></div>
</div>

