{*Smarty template*}
<div id="groups_list" style="
  height: auto;
	width: 600px;
">

{*--------------------Group list header----------------*}
<table class="normal">
<tr>
  <th class="heading">{tr}groups{/tr}</th>
</tr>
{*--------------------Group list body----------------*}
{cycle values="even,odd" print=false}
{foreach key=group_id item=group from=$groups}
 <tr class="{cycle}"
    {if $role_id eq $group.role_id} style="background-color:#ECE9D8;text-color:white;"; {/if}>
    <td>
    {if $resource_id}
     <a class="link"
      {if $role_id eq $group.role_id}
      style="text-decoration:underline";
      {/if}
      href="{$samecontroller}/get/resource_id/{$resource_id}/role_id/{$group.role_id}/object_id/{$object_id}{if $SPACE_NAME}/space/{$SPACE_NAME}{/if}"
      title="{tr}permissions{/tr}">
      <img border="0" alt="" src="img/icons/group.png" />
      {$group.group_define_name} ({$group.group_description})
     </a>
    {else}
      {$group.group_define_name} ({$group.group_description})
    {/if}
    </td>
 </tr>
{/foreach}
</table>
</div>
