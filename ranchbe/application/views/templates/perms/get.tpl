{*Smarty template*}

<div id="perms" style="
  height: auto;
	width: 600px;
	margin:   0px;
	padding:   5px;
	border:   2px solid white;
">

<h1 class="pagetitle">{$PageTitle}</h1>

{*----------------groups-------------------------*}
{include file="perms/groups.tpl"}

{*----------------permissions-------------------------*}
{if $resource_id}
<h3 class="pagetitle">{tr}permissions for{/tr} {$resource_name}::{$currentGroup}</h3>
<i>{$msg}</i><br />
{include file="perms/perms.tpl"}
{/if}

</div>

