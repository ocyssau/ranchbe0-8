{*Smarty template*}

<div  id="users_list"
      style="height: auto; width: auto;">

{*--------------------Group list header----------------*}
<table class="normal">
<tr>
  <th class="heading">
	<div id="displaySelectedRowCount"></div>
  </th>
  <th class="heading">{tr}users{/tr}</th>
  <th class="heading">{tr}mail{/tr}</th>
  <th class="heading">{tr}groups{/tr}</th>
</tr>
{*--------------------Group list body----------------*}
{cycle values="even,odd" print=false}

{foreach key=user_id item=user from=$users}
 <tr class="{cycle}">
    <td>
     <input type="checkbox" name="user_id[]"
            value="{$user_id}" {if $users[user].checked eq 'y'}checked="checked" {/if}/>
    </td>

    <td>
      <a class="link"
         href="user/adminusers/edit/user_id/{$user_id}">
         <img border="0" 
              alt="{tr}Edit{/tr}"
              title="{tr}edit account settings for{/tr}: {$user.handle}"
              src="img/icons/edit.png" />
      </a>

      <a class="link"
          href="user/adminusers/assigngroup/user_id/{$user_id}">
          <img border="0" 
              alt="{tr}Assign Group{/tr}"
              title="{tr}Assign Group{/tr}"
              src="img/icons/group_assign.png" />
      </a>

      <a class="link"
          href="user/adminusers/suppressuser/user_id/{$user_id}">
          <img border="0" 
              alt="{tr}delete{/tr}"
              title="{tr}delete{/tr}"
              src="img/icons/trash.png" />
      </a>

        <b>{$user.handle}</b></td>
    <td>{$user.email}</td>
    <td>
      {foreach from=$user.groups item=group key=group_id}
        <img border="0" alt="" src="img/icons/group_link.png" />
        {$group.group_define_name}
      {/foreach}
    </td>

 </tr>
{/foreach}
</table>
</div>
