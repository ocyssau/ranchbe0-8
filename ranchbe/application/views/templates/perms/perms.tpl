{*Smarty template*}

<div id="perms_list" style="
  height: auto;
	width: 400px;
">

<form method="POST" action="{$samecontroller}/save" name="assignPerm">

{*--------------------Group list header----------------*}
<table class="normal">
<tr>
  <th class="heading">{tr}privilege{/tr}</th>
  <th class="heading">{tr}deny{/tr}</th>
  <th class="heading">{tr}allow{/tr}</th>
  <th class="heading">{tr}inherit{/tr}</th>
</tr>

{*--------------------Group list body----------------*}
{foreach key=privilege item=item from=$privileges}
 <tr>
    <td>{tr}{$privilege|privilege_name}{/tr}</td>
    <td>
      <!--TYPE_DENY-->
      <input type="radio" name="privileges[{$privilege}]" value="TYPE_DENY" 
      {if $item.type eq 'TYPE_DENY'}checked="checked"{/if} />
    </td><td>
      <!--TYPE_ALLOW-->
      <input type="radio" name="privileges[{$privilege}]" value="TYPE_ALLOW" 
      {if $item.type eq 'TYPE_ALLOW'}checked="checked"{/if} />
    </td><td>
      <!--INHERIT-->
      <input type="radio" name="privileges[{$privilege}]" value="INHERIT" 
      {if $item.type eq 'INHERIT'}checked="checked"{/if} />
    </td>
 </tr>
{/foreach}
</table>

<input type="hidden" name="resource_id" value="{$resource_id}">
<input type="hidden" name="role_id" value="{$role_id}">
<input type="hidden" name="object_id" value="{$object_id}">
<input type="hidden" name="space" value="{$SPACE_NAME}">

<input name="submit" value="{tr}save{/tr}" type="submit">&nbsp;
<input name="reset" value="reset" type="reset">
</form>

</div>
