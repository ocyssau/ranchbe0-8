{*Smarty template*}

{*--------------------Search Bar defintion--------------------------*}

<div id="rapidSearch">
<fieldset>
{include file="search/searchBar.tpl"}
</fieldset>
</div>

{*--------------------list header----------------------------------*}
<table class="normal">
 <tr>
  <th class="heading">{column_header sort_order='INV' sort_field='id'}</th>
  <th class="heading">
  {column_header sort_order='INV' sort_field='name'}
  <br />
  {column_header sort_order='INV' sort_field='description'}
  </th>
 </tr>

{*--------------------list Body----------------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">

    <td class="thin">
    {$list[list].id}
    </td>

    <td class="thin" nowrap>
    {if $list[list].class_name == 'document'}
    <a class="link" 
    href="javascript:popupP('document/detail/get/document_id/{$list[list].id}/space/{$list[list].space_name}'
    ,'documentDetailWindow', 600 , 1024)">
    {elseif $list[list].class_name == 'docfile'}
    <a class="link" 
    href="javascript:popupP('docfile/detail/get/file_id/{$list[list].id}/space/{$list[list].space_name}'
    ,'docfileDetailWindow', 600 , 1024)">
    {elseif $list[list].class_name == 'recordfile'}
    <a class="link" 
    href="javascript:popupP('recordfile/detail/get/file_id/{$list[list].id}/space/{$list[list].space_name}'
    ,'recordfileDetailWindow', 600 , 1024)">
    {/if}
    {if $list[list].class_name == 'document'}
    {$list[list].name} - {$list[list].indice|indice}.{$list[list].version}
    {else}
    {$list[list].name} - {$list[list].version}
    {/if}
    </a>
    <br />
    <font size="2"><i>{$list[list].description}</i></font>
    <br />
    {tr}{$list[list].class_name}{/tr} of {tr}{$list[list].space_name}{/tr}
    </td>

  {/section}
  </table>
