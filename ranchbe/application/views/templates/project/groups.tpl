{*Smarty template*}

{include file="header.tpl"}

<div id="tiki-center">

<form name="close"><input type="button" onclick="window.close()" value="{tr}Close{/tr}"></form>

<h2>{$PageTitle}</h2>

{*--------------------Group list header----------------*}
<table class="normal">
<tr>
    <th class="heading">{tr}name{/tr}</th>
    <th class="heading">{tr}desc{/tr}</th>
    <th class="heading">{tr}Permissions{/tr}</th>
</tr>

{*--------------------Group list body----------------*}
{cycle values="even,odd" print=false} {* ---SmartyCode to alternate colors of rows---*}
  {section name=user loop=$list}
   <tr class="{cycle}">
      <td>{$list[user].group_define_name}</td>
      <td>{tr}{$list[user].group_description}{/tr}</td>
      <td>
       <a class="link" 
          href="projectManager/perms/getperms/project_id/{$project_id}/group_id/{$list[user].group_id}"
          title="{tr}permissions{/tr}">
          <img border="0" alt="{tr}permissions{/tr}" src="img/icons/key.png" />
       </a>
      </td>
   </tr>
  {/section}
</table>

<form name="close"><input type="button" onclick="window.close()" value="{tr}Close{/tr}"></form>

</div>
