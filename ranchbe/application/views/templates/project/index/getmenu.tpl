<div  dojoType='dijit.form.DropDownButton' iconClass='noteIcon' >
  		<span>Actions</span>
		  <div dojoType='dijit.Menu'>
		    <div dojoType='dijit.PopupMenuItem' iconClass='dijitEditorIcon dijitEditorIconCopy'>
		      <span>Documents</span>
		      <div dojoType='dijit.Menu'>
		        <div dojoType='dijit.MenuItem' onClick='javascript:popupP('./project/links/getdoctypes/project_id/{$list[projects].project_id}/space/project/ticket/{$ticket}',900,450,'Doctypes')'>
		        <img border='0' alt='' src='img/icons/link_edit.png' />{tr}Doctypes{/tr}
		        </div>
		        <div dojoType='dijit.MenuItem' onClick='javascript:popupP('./project/links/getmockups/project_id/{$list[projects].project_id}/space/project/ticket/{$ticket}',900,450,'Mockups')' >
		        <img border='0' alt='' src='img/icons/link_edit.png' />{tr}Mockups{/tr}
		        </div>
		        <div dojoType='dijit.MenuItem' onClick='javascript:popupP('./project/links/getcadlibs/project_id/{$list[projects].project_id}/space/project/ticket/{$ticket}',900,450,'Mockups')' >
		        <img border='0' src='img/icons/link_edit.png' />{tr}Cadlibs{/tr}
		        </div>
		        <div dojoType='dijit.MenuItem' onClick='javascript:popupP('./project/links/getbookshops/project_id/{$list[projects].project_id}/space/project/ticket/{$ticket}',900,450,'Mockups')' >
		        <img border='0' alt='' src='img/icons/link_edit.png' />{tr}Bookshops{/tr}
		        </div>
		      </div>
		    </div>
		    <div dojoType='dijit.MenuSeparator'></div>
			<div dojoType='dijit.MenuItem'  onClick='javascript:popupP('project/perms/get/object_id/{$list[projects].project_id}','projectPerms',550,650 )' >
		        <img border='0' alt='' src='img/icons/key.png' />{tr}permissions{/tr}
		    </div>
		    <div dojoType='dijit.MenuItem'  onClick='project/index/suppress/project_id/{$list[projects].project_id}/ticket/{$ticket}'>
		        <img border='0' alt='' src='img/icons/trash.png' />{tr}Suppress project{/tr}
		    </div>
		    <div dojoType='dijit.MenuItem'  onClick='javascript:popup('project/index/edit/project_id/{$list[projects].project_id}','editproject')'>
		        <img border='0' alt='{tr}edit project{/tr}' src='img/icons/edit.png' />{tr}edit{/tr}
		    </div>
		    <div dojoType='dijit.MenuSeparator'></div>
		    <div dojoType='dijit.MenuItem' onClick='javascript:popupP('project/history/get/project_id/{$list[projects].project_id}','projetHistory', 1180, 1180)'>
		        <img border='0' alt='' src='img/icons/history.png' />{tr}Get history{/tr}
		    </div>
		  </div>
		</div>
		
		