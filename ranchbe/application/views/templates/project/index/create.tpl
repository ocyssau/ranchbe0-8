{*Smarty template*}

{$form.javascript}

<div id="tiki-center">

<form {$form.attributes}>
{$form.hidden}

<h2>{$form.header.editheader}</h2>

<table class="normal">
 
  <tr>
    <td class="formcolor">{$form.project_number.label}:</td>
    <td class="formcolor">{$form.project_number.html}
      <br /><i>{$number_help}</i>
    </td>
  </tr>
 
  <tr>
    <td class="formcolor">{$form.project_description.label}:</td>
    <td class="formcolor">{$form.project_description.html}</td>
  </tr>
  
  <tr>
    <td class="formcolor">{$form.forseen_close_date.label}:</td>
    <td class="formcolor">{$form.forseen_close_date.html}</td>
  </tr>

 {if not $form.frozen}
  <tr>
    <td class="formcolor"> </td>
    <td class="formcolor">{$form.submit.html}&nbsp;{$form.reset.html}&nbsp;<input type="button" onclick="window.close()" value="{tr}Close{/tr}"></td>
  </tr>
  <tr>
    <td class="formcolor">{$form.requirednote}</td>
  </tr>
 {/if}

</table>
</form>

<br />

{if $form.errors}
<b>Collected Errors:</b><br />
{foreach key=name item=error from=$form.errors}
  <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
<br />
{/if}

</div>
