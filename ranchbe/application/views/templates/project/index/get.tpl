{*Smarty template*}
<h1 class="pagetitle">{tr}{$PageTitle}{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{$views_helper_filter_form|toggleanim}

{* -------------------Pagination------------------------ *}
{$views_helper_pagination}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$this->baseUrl('project/index/index')}">
<div class="scroll" >
<table class="normal" id="scroll">
<thead class="fixedHeader">
 <tr class="fixedHeader" >
    <th width="50px">
		{project_action_menu 
		  space=$SPACE_NAME 
		  ticket=$ticket
		  displayText = true
		  mode='main'
		  formId = 'checkform'
		  ifSuccessModule='project'
		  ifSuccessController='index'
		  ifSuccessAction='get'
		  ifFailedModule='project'
		  ifFailedController='index'
		  ifFailedAction='get'
		}
	<input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxes(this.form, 'project_id[]', this.checked);">
	<div id="displaySelectedRowCount"></div>
 	</th>
  

  <th class="heading">
    {column_header sort_order='INV' sort_field='project_number'}
    <br />
    {column_header sort_order='INV' sort_field='project_description'}
    <br />
    {column_header sort_order='INV' sort_field='project_state'}
  </th>

  <th class="heading" >
	  {tr}created{/tr}
	  {column_header sort_order='INV' sort_field='open_by' title='by'}
	   - 
	  {column_header sort_order='INV' sort_field='open_date' title='date'}
	  <br />
	  {tr}closed{/tr}
	  {column_header sort_order='INV' sort_field='close_by' title='by'}
	   - 
	  {column_header sort_order='INV' sort_field='close_date' title='date'}
	  <br />
	  {column_header sort_order='INV' sort_field='forseen_close_date'}
  </th>

 </tr>

</thead>
<tbody class="scrollContent" id="contentTbody">

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=projects loop=$list}
   <tr class="{cycle}">
    <td class="thin" style="text-align: center;">
	    <input type="checkbox" name="project_id[]" value="{$list[projects].project_id}" {if $projects[projects].checked eq 'y'}checked="checked"{/if} />
    </td>
    
	<td nowrap >
    <a class="link">{$list[projects].project_number}</a>
    <br /><i>{$list[projects].project_description}</i>
    <br />{$list[projects].project_state}
	  {project_action_menu 
	  	  infos = $list[projects]
		  space=$SPACE_NAME 
		  ticket=$ticket
		  displayText = true
		  mode='contextual'
		  formId = 'checkform'
		  ifSuccessModule='project'
		  ifSuccessController='index'
		  ifSuccessAction='get'
		  ifFailedModule='project'
		  ifFailedController='index'
		  ifFailedAction='get'
		}
	</td>
	
    <td >
	    <b>{tr}created{/tr} : </b>{$list[projects].open_date|date_format} - {$list[projects].open_by|username}<br />
	    {if $list[projects].close_date}
		    <b>{tr}closed{/tr} : </b>{$list[projects].close_date|date_format} - {$list[projects].close_by|username}<br />
	    {/if}
	    <b>{tr}forseen_close_date{/tr} : </b>{$list[projects].forseen_close_date|date_format}
    </td>

   </tr>
   
  {sectionelse}
    <tr><td  colspan="6">{tr}No projects to display{/tr}</td></tr>
  {/section}

  
  </tbody>
  </table></div>

{*Multiselection select action form *}
<input type="hidden" name="ticket" value="{$ticket}" />
</form>

{literal}
<script type="text/javascript">
	initTbody();
</script>
{/literal}


