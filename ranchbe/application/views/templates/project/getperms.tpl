{*Smarty template*}

<h1 class="pagetitle">{tr}Permissions of group{/tr} {$group_info.group_define_name}</h1>
<a href="projectExplore.php?project_id={$project_id}&action=listGroups" class="linkbut">{tr}Back to groups{/tr}</a><br />

{$msg}<br />

{*----------------Permissions list header--------------------------*}
    <form name="assignPerm" action="{$root_url}/projectManager/perms/update" method="post">
    <input type="hidden" name="group_id" value="{$group_id|escape}" />
    <input type="hidden" name="project_id" value="{$project_id|escape}" />
    <table class="normal">

{*----------------Permissions list body-------------------------*}
    {cycle values="odd,even" print=false}
    {section name=user loop=$perms}
      <input type="hidden" name="right_ids[{$perms[user].right_id}]" value="{$perms[user].right_id}" />
      <tr>
        <td class="{cycle advance=false}">
        <input type="checkbox" name="right_id[{$perms[user].right_id}]" value="{$perms[user].right_id}"
        {section name=group_perms loop=$group_perms}
         {if $group_perms[group_perms].right_id eq $perms[user].right_id}checked="checked"{/if}
        {/section}
        />
        </td>
        <td class="{cycle}">{tr}{$perms[user].right_description}{/tr}</td>
      </tr>
    {/section}
  </table>
  <input type="submit" name="update" value="{tr}validate{/tr}" />
</form>
<br />
