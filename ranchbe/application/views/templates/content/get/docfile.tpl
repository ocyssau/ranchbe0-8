{*Smarty template*}

<h1 class="pagetitle">{tr}Documents files manager{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{$views_helper_filter_form|toggleanim}

{literal}
<script type="text/javascript">
	initTbody();
</script>
{/literal}


{* -------------------Pagination------------------------ *}
{$views_helper_pagination}

{*--------------------list----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">
{capture name=menu}
	  {docfile_action_menu 
		  space=$SPACE_NAME 
		  ticket=$ticket
		  displayText = true
		  mode='main'
		  formId = 'checkform'
		  ifSuccessModule='content'
		  ifSuccessController='get'
		  ifSuccessAction='docfile'
		  ifFailedModule='content'
		  ifFailedController='get'
		  ifFailedAction='docfile'
		}
{/capture}

{include file="recordfile/_list.tpl"}

{* -------------------Pagination------------------------ *}
{$views_helper_pagination}

<input type="hidden" name="{$CONTAINER_ID}" value="{$container_id}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />

</form>
