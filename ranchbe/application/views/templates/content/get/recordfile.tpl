{*Smarty template*}

<h1 class="pagetitle">{$PageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
{$views_helper_filter_form|toggleanim}


{literal}
<script type="text/javascript">
	initTbody();
</script>
{/literal}

{* -------------------Pagination------------------------ *}
{$views_helper_pagination}

{*--------------------list definition----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">
{include file='recordfile/_list.tpl'}

{* -------------------Pagination------------------------ *}
{$views_helper_pagination}

<div id="action-menu">

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}" id="01"
onclick="if(confirm('{tr}Do you want really suppress this file{/tr}')){ldelim}
document.checkform.action='./recordfile/index/suppress';
pop_no(checkform){rdelim}else {ldelim}return false;
{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="putInWs" title="{tr}Put in wildspace{/tr}" id="06"
 onclick="document.checkform.action='./recordfile/index/putinws'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_read.png" title="{tr}Put in wildspace{/tr}" alt="{tr}Put in wildspace{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="document.checkform.action='{$sameurl}'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>
</div>

<p></p>

<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
</form>
