{*Smarty template*}

<div id="page-title">
  <h1 class="pagetitle">{tr}Documents manager{/tr}</h1>
</div>

{*--------------------Search Bar defintion--------------------------*}
{$views_helper_filter_form|toggleanim}

{* -------------------Pagination------------------------ *}
{$views_helper_pagination}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
{include file='document/_list.tpl'}
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
</form>



{literal}
<script type="text/javascript">
	initTbody();
</script>
{/literal}
