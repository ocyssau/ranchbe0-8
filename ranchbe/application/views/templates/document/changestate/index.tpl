{*Smarty template*}

{$changestateForm}

<br />

{if $graphUrl}
<table class="normal">
<tr>
	<td colspan="6" align="center">
	<h3>{tr}Graph of current process{/tr}</h3>
  </td>
</tr>
<tr>
	<td colspan="6" align="center">
  <img src="{$graphUrl}" title="{tr}Graph{/tr}" alt="{tr}Graph is not accessible{/tr}"/>
</td>
</tr>	
</table>
{/if}
