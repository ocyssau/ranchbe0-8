{*Smarty template*}
{*========================== DETAIL OF ACTIVITY ======================================================*}
<h3>{tr}Detail activity{/tr} : {$aid}</h3>
<h3>{tr}Comments{/tr}</h3>
{section name=ix loop=$comments}
<table class="email">
        <tr>
	    	<td class="heading">{tr}From{/tr}:</td>
		<td class="formcolor">{$comments[ix].user|capitalize:true}</td>
      	        <td class="closeButton">
		    <form method="POST" target="email" action="galaxia_view_comment.php">
		    <input type="hidden" name="__user" value="{$comments[ix].user}" />
			  <input type="hidden" name="__title" value="{$comments[ix].title}" />
			  <input type="hidden" name="__comment" value="{$comments[ix].comment}" />
			  <input type="hidden" name="__timestamp" value="{$comments[ix].timestamp}" />
	      	    	  <input type="submit" name="view" title="{tr}Pop-up{/tr}" galaxia_view_comment.php','email','HEIGHT=400,width=400,resizable=0,menubar=no,location=no,scrollbars=1')" value="&#164">
		    </form>
		</td>
		<td class="closeButton">    
	      	    <form method="POST" action="galaxia_admin_instance_activity.php?iid={$iid}&aid={$aid}">
		    	  <input type="hidden" name="__removecomment" value="{$comments[ix].cId}" />
	      	    	  <input type="submit" name="eraser" value="X" title="{tr}erase{/tr}">
	      	    </form>
		</td>
	</tr>
	<tr>
	    	<td class="heading">{tr}Date{/tr}:</td>
		<td class="formcolor" colspan="3">{$comments[ix].timestamp|date_format:"%A %e de %B, %Y %H:%M:%S"|capitalize:true}</td>
	</tr>
	<tr>
		<td class="heading">{tr}Subject{/tr}:</td>
		<td class="formcolor" colspan="3">{$comments[ix].title}</td>
	</tr>
	<tr>
		{*<td class="body">{tr}Body{/tr}:</td>*}
		<td class="body" colspan="4">{$comments[ix].comment}</td>
	</tr>
</table>
{/section}
