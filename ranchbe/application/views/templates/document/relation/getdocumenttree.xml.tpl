<?xml version="1.0" encoding="UTF-8"?>


{if $spacelist}
<tree id='0'>
{foreach from=$spacelist key=space_id item=space}
	<item child='1' id='{$space_id}' text='{$space}'>
	</item>
{/foreach}
{/if}

{if $containerlist}
<tree id='{$id}'>
{foreach from=$containerlist item=container}
	<item child='1' id='{$id}--container_id__{$container_id}' text='{$container.container_number}'>
	</item>
{/foreach}
{/if}

{if $categorylist}
<tree id='{$id}'>
{foreach from=$categorieslist item=category}
	<item child='1' id='{$id}--category_id__{$category.category_id}' text='{$category.category_number}'>
	</item>
{/foreach}
{/if}

{if $documentlist}
<tree id='{$id}'>
{foreach from=$documentlist item=document}
	<item child='0' id='{$id}--document_class__{$document.document_number}--document_id__{$document.document_id}' text='{$document.document_number}'>
	</item>
{/foreach}
{/if}

</tree>


