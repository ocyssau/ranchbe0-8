{*Smarty template*}

{*===================== DOCUMENT STRUCTURE ==============================================================*}
<h3>{tr}Father documents of{/tr} : {$document_name}</h3>

<table class="normal">
 <tr>
  <th class="heading"></th>
  <th class="heading">{tr}Number{/tr}</th>

  <th width="50" class="heading" nowrap>{tr}description{/tr}</th>
  <th class="heading">{tr}Indice{/tr} - {tr}Version{/tr}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$father_documents}
   <tr class="{cycle}">
    <td class="thin"></td>
    <td class="thin">
    <a class="link" 
    href="javascript:popupP('documentManager/detail/getdetailwindow/document_id/{$father_documents[list].document_id}/space/{$father_documents[list].doc_space}'
    ,'documentDetailWindow', 600 , 1024)">{$father_documents[list].document_number}</a></td>
    <td class="thin">{$father_documents[list].description}</td>
    <td class="thin">{$father_documents[list].document_version|indice}.{$father_documents[list].document_iteration}</td>
   </tr>

  {sectionelse}
  <tr>
  	<td class="{cycle advance=false}" colspan="7">
  	{tr}No document is linked to this document{/tr}
  	</td>
  </tr>	
  {/section}
</table>
