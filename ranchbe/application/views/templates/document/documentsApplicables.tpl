{*Smarty template*}

{include file='header.tpl'}

<h1 class="pagetitle">{tr}{$PageTitle}{/tr}</h1>

<form {$form.attributes}>
{$form.hidden}

<table>
<tr>
<td>{$form.domaines.label}</td><!--<td>{$form.motscles.label}</td><td>{$form.numrows.label}</td>-->
</tr><tr>
<td>{$form.domaines.html}</td><!--<td>{$form.motscles.html}</td><td>{$form.numrows.html}</td>-->
</tr>
</table>

{if not $form.frozen}
<tr>
  <td class="formcolor">{$form.reset.html}&nbsp;{$form.submit.html}</td>
</tr>
{/if}

</table>
</form>

{*--------------------list header----------------------------------*}

<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
<tr>
  <th class="heading auto"></th>
  {section name=header loop=$select}
    {if $select[header] != 'doctype_id'}
    <th class="heading">{tr}{$select[header]}{/tr}</th>{/if}
  {/section}
</tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><a href="{$smarty.server.PHP_SELF}?action=view&document_id={$list[list].document_id}&container_type={$container_type}" title="{tr}Voir le document{/tr}">
    <img border="0" alt="no icon" src="{$icons_dir}/C/{$list[list].doctype_id}.gif" /></td>
      {foreach key=name item=body from=$select}
        {if $body != 'doctype_id'}
        {assign var="filtre" value='indice'}
        <td class="thin">{$list[list].$body}</a></td>{/if}
      {/foreach}
   </tr>
  {/section}
  </table>
</form>
