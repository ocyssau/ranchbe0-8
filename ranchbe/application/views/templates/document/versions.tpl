{*Smarty template*}

<h1 class="pagetitle">{tr}Versions of file{/tr} : {$file_name}</h1>

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">
    {tr}file_name{/tr} .
    {tr}file_extension{/tr} - 
    {tr}file_version{/tr}<br />
    {tr}file_type{/tr}<br />
    {tr}file_access_code{/tr} - {tr}file_state{/tr}
  </th>
  <th class="heading">
  {tr}Action - user - date{/tr}
  </th>
  <th class="heading">
  {tr}infos{/tr}
  {tr}file_path{/tr} - 
  {tr}file_size{/tr} - 
  {tr}file_mtime{/tr} - 
  {tr}file_md5{/tr}
  </th>
 </tr>

<!--
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">
    <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_name">
    {tr}file_name{/tr}</a>  -
    <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_version">
    {tr}file_version{/tr}</a>
    <br />
    <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_type">
    {tr}file_type{/tr}</a>
  </th>
  <th class="heading">
    <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_path">
    {tr}file_path{/tr}</a> - 
    <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_size">
    {tr}file_size{/tr}</a> - 
    <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_mtime">
    {tr}file_mtime{/tr}</a> - 
    <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_md5">
    {tr}file_md5{/tr}</a>
  </th>
 </tr>
-->

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].file_id}" /></td>

    <td class="thin">
      <a class="link"
		href="get/file/file_id/{$list[list].file_id}/space/{$CONTAINER_TYPE}/object_class/recordfileVersion/ticket/{$ticket}"
		title="{tr}View file{/tr}">
      {file_icon extension=$list[list].file_extension icondir=$file_icons_dir icontype='.gif'}{$list[list].file_name} - V{$list[list].file_version}</a>
      <br />{$list[list].file_type}
      <br />{$list[list].file_access_code|access_code} - {$list[list].file_state}
    </td>

    <td class="thin">
      {if !empty($list[list].file_check_out_by)}
      <b>{tr}Checkout{/tr} : </b>{$list[list].file_check_out_by|username} - {$list[list].file_check_out_date|date_format}
      <br />{/if}
      <b>{tr}Last Update{/tr} : </b>{$list[list].file_update_by|username} - {$list[list].file_update_date|date_format}
      <br />
      <b>{tr}Create{/tr} : </b>{$list[list].file_open_by|username} - {$list[list].file_open_date|date_format}
    </td>

    <td class="thin">
      <i>{tr}file_path{/tr}:</i> {$list[list].file_path}<br />
      <i>{tr}file_size{/tr}:</i> {$list[list].file_size|filesize_format}<br />
      <i>{tr}file_mtime{/tr}:</i> {$list[list].file_mtime|date_format}<br />
      <i>{tr}file_md5{/tr}:</i> {$list[list].file_md5}<br />
    </td>

   </tr>
  {/section}
  </table>

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>
        <br>

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="SuppressVer" title="{tr}Suppress{/tr}" id="01" pop_no(checkform)">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="putInWs" title="{tr}Put in wildspace{/tr}" id="06" pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_read.png" title="{tr}Put in wildspace{/tr}" alt="{tr}Put in wildspace{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="file_id" value="{$file_id|escape}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>
