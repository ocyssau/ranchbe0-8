{*Smarty template*}

{$form.javascript}

<form {$form.attributes}>
{$form.hidden}

<h2>{$form.header.createdoc_header}</h2>

<table class="normal">

 {if $form.document_number}
  <tr>
    <td>{$form.document_number.label}:</td>
    <td>{$form.document_number.html}
    	<br /><i>{$number_help}</i></td>
  </tr>
 {/if}

 {if $form.description}
  <tr>
    <td>{$form.description.label}:</td>
    <td>{$form.description.html}</td>
  </tr>
 {/if}

 {if $form.document_version}
  <tr>
    <td>{$form.document_version.label}:</td>
    <td>{$form.document_version.html}</td>
  </tr>
 {/if}

 {if $form.category_id}
  <tr>
    <td>{$form.category_id.label}:</td>
    <td>{$form.category_id.html}</td>
  </tr>
 {/if}

  <!-- Display optionnals fields -->
  <!-- TODO : revoir boucle imbriqu�e -->
  {section name=of loop=$optionalFields}
  {assign var="fn" value=$optionalFields[of].property_fieldname}
  <tr class="formcolor">
    <td>{$form.$fn.label}:</td>
    <td>{$form.$fn.html}</td>
  </tr>
  {/section}

 {if $form.lock}
  <tr>
    <td>{$form.lock.label}:</td>
    <td>{$form.lock.html}</td>
  </tr>
 {/if}

 
  <tr>
    <td> </td>
    <td>{if not $form.frozen}{$form.validate.html}&nbsp;{$form.reset.html}&nbsp;{/if}{$form.close.html}</td>
  </tr>

  <tr>
    <td>{$form.requirednote}</td>
  </tr>

</table>

</form>

<br />
