{*Smarty template*}

<br>

<h1 class="pagetitle">{tr}Document history{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
<a id='choix' href="javascript:funcchoice()" style="text-decoration: none;"> 
<div class="hautboxScroll" style="text-align: left;">
<img id="imageFiltre" type="image" style='margin-top: 5px;' name="photo" src="./img/galaxia/icons/mini_inv_triangle.gif" value="cacher" />&nbsp;&nbsp;<b id="labelFiltre"style="color:#08639C;" >Afficher le filtre</b> </div></a>
<div id="englob" class="cacher">
<div id="animDiv" class='boxScroll'>
{include file='searchBar_simple.tpl'}
<form id="resetf" action="{$smarty.server.PHP_SELF}" method="post">
  <input type="hidden" name="document_id" value="{$document_id}" />
  <input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />
</form>
</div>
</div>

{literal}
<script type="text/javascript">
	initTbody();
	document.getElementById('choix').name = 0;
	var cptChoixToggle = 0;
</script>
{/literal}
{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  <th class="heading">
  {column_header document_id=$document_id sort_order='INV' sort_field='histo_order'}
  {column_header document_id=$document_id sort_order='INV' sort_field='action_name'}
  {column_header document_id=$document_id sort_order='INV' sort_field='action_by'}
  {column_header document_id=$document_id sort_order='INV' sort_field='action_date'}
  {column_header document_id=$document_id sort_order='INV' sort_field='comment'}
  {column_header document_id=$document_id sort_order='INV' sort_field='from_document'}
  </th>

  <th class="heading" nowrap>
  {column_header document_id=$document_id sort_order='INV' sort_field='document_number'} - 
  {column_header document_id=$document_id sort_order='INV' sort_field='document_version'}.
  {column_header document_id=$document_id sort_order='INV' sort_field='document_iteration'} - 
  {column_header document_id=$document_id sort_order='INV' sort_field=$container_map_id title='container' } - 
  <br />
  {column_header document_id=$document_id sort_order='INV' sort_field='description' }
  <br />
  {column_header document_id=$document_id sort_order='INV' sort_field='doctype_id' }
  <br />
  {column_header document_id=$document_id sort_order='INV' sort_field='document_state' }
  {column_header document_id=$document_id sort_order='INV' sort_field='document_access_code' }
  </th>

  <th class="heading">
  {column_header document_id=$document_id sort_order='INV' sort_field='activity_id' }.
  {column_header document_id=$document_id sort_order='INV' sort_field='instance_id' }.
  </th>

 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="histo_order[]" value="{$list[list].histo_order}" /></td>

    <td class="thin"><a href="DocHistory.php?action=suppress&histo_order[]={$list[list].histo_order}&document_id={$document_id}&space={$CONTAINER_TYPE}" title="{tr}Suppress{/tr}: {$list[list].histo_order}"
                      onclick="return confirm('{tr}Do you want really suppress{/tr} history {$list[list].histo_order}')">
                     <img border="0" alt="{tr}Suppress{/tr}: {$list[list].histo_order}" src="img/icons/trash.png" />
                     </a>
    </td>

    <td class="thin"><a href="javascript:popup('DocHistory.php?action=getComment&activity_id={$list[list].activity_id}&instance_id={$list[list].instance_id}&space={$CONTAINER_TYPE}','comments')" title="{tr}Get comment{/tr}">
                     <img border="0" alt="{tr}Get comment{/tr}" src="img/icons/comment.gif" />
                     </a>
    </td>

    <td class="thin">
    {$list[list].histo_order}<br />
    <b>{$list[list].action_name}</b> <i>by</i> <b>{$list[list].action_by}</b><br />
    {$list[list].action_date|date_format}
    {if !empty($list[list].comment)}
      <hr />
      <em>{tr}Comment{/tr} :<br />
      <b>{$list[list].comment}</b></em>
    {/if}
    {if !empty($list[list].from_document)}
      <hr />
      <a href="javascript:popupP('DocManage.php?documentDetail=1&document_id={$list[list].from_document}&space={$CONTAINER_TYPE}','documentDetailWindow',600,1024);">
        <em>{tr}from_document{/tr} :
        <b>{$list[list].from_document}</b></em>
      </a>
    {/if}
    </td>



    <td class="thin">
    <a class="link" href="javascript:popupP('DocManage.php?documentDetail=1&document_id={$list[list].document_id}&space={$CONTAINER_TYPE}','documentDetailWindow', 600 , 1024)">
    {$list[list].document_number} - {$list[list].document_version|indice}.{$list[list].document_iteration} - in {$list[list].$CONTAINER_ID|container}</a>
    <br />
    <i>{$list[list].description}</i>
    <br />
    {$list[list].doctype_id|doctype}
    <br />
    {$list[list].document_access_code|access_code} - {$list[list].document_state}
    </td>


    <td class="thin">
    {$list[list].activity_id}.{$list[list].instance_id}
    </td>

   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'histo_order[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>
      <br>

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<br />

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}" 
 onclick="if(confirm('{tr}Do you want really suppress this history{/tr}')){ldelim}pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p>
</p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="document_id" value="{$document_id}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />

</form>
