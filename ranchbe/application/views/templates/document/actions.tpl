{*Smarty template*}
<table>
<tr><td>
	<a href="content/get/docfile/space/{$SPACE_NAME}/container_id/{$container_id}" class="button">
		<span class="exp">{tr}Files manager{/tr}</span>
	</a>
</td><td>
	<a href="javascript:popupP('document/history/get/{$SPACE_NAME}','history', 600 , 1200)" class="button">
		<span class="histo">{tr}History of all{/tr}</span>
	</a>
</td><td>
	<a href="import/document/index/space/{$SPACE_NAME}/container_id/{$container_id}/step/step1" class="button">
		<span class="imp">{tr}Import{/tr}</span>
	</a>
</td><td>
	<a href="export/document/index/space/{$SPACE_NAME}/container_id/{$container_id}/step/step1" class="button">
		<span class="exp">{tr}Export{/tr}</span>
	</a>
</td><td>
	<a href="javascript:popupP('container/notification/get/container_id/{$container_id}/space/{$SPACE_NAME}/ticket/{$ticket}','Notification', 450 , 900)" class="button">
		<span class="noty">{tr}Notification{/tr}</span>
	</a>
</td><td>
	<a href="container/stats/get/container_id/{$container_id}/space/{$SPACE_NAME}/ticket/{$ticket}/ifFailedModule/content/ifFailedController/get/ifFailedAction/document"  class="button">
		<span class="stat">{tr}Statistics{/tr}</span>
	</a>
</td><td>
	<a href=href="content/index/freezeall/space/{$SPACE_NAME}/ticket/{$ticket}"   onclick="FreezeConfirm(this,'{tr}Do want really freeze all documents{/tr}');return false;"  class="button">
		<span class="delete">{tr}Freeze all{/tr}</span>
	</a>
</td></tr>
</table>
    