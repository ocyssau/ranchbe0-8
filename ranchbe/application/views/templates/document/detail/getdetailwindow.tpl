{*Smarty template*}

<div id="page-bar">
  <span id="tab1" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(1,4);">{tr}Detail of the document{/tr}</a></span>
  <span id="tab2" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(2,4);">{tr}Associated files to{/tr}</a></span>
  <span id="tab3" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(3,4);">{tr}Document relations{/tr}</a></span>
  <span id="tab4" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(4,4);">{tr}Workflows{/tr}</a></span>
</div>

{*================================= DETAIL OF DOC ===================================================*}
<a name="1" ></a>
<div id="content1" class="tabcontent" style="display: {$content1display};">
{$document_detail}
</div>

{*================================= ASSOCIATED FILES ===================================================*}
<a name="2" ></a>
<div id="content2" class="tabcontent" style="display: {$content2display};">
{$documentAssociatedFiles}
</div>
 
{*===================== DOCUMENT STRUCTURE ==============================================================*}
<a name="3"></a>
<div id="content3" class="tabcontent" style="display: {$content3display};">
{$documentStructure}
</div>

{*===================== DOCUMENT ALTERNATIVE ==============================================================*}

{*=================================LISTING OF INSTANCES===============================================*}
<div id="content4" class="tabcontent" style="display: {$content4display};">
  <a name="4" ></a>
  {$documentInstances}
  {*=================================DETAILS OF INSTANCES===============================================*}
  <a name="5" ></a>
  {$documentInstanceDetail}
  {*========================== DETAIL OF ACTIVITY ======================================================*}
  <a name="6" ></a>
  {$documentActivityDetail}
</div>
