{*Smarty template*}

{*===================== DOCUMENT ALTERNATIVE ==============================================================*}
<h3>{tr}Alternatives of{/tr} : {$document_name}</h3>

{*--------------------list header----------------------------------*}
<form name="linkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">{tr}Number{/tr}</th>

  <th width="50" class="heading">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{tr}Designation{/tr}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
  <th class="heading">{tr}Indice{/tr} - {tr}Version{/tr}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$alternative_list}
   <tr class="{cycle}">
    <td class="thin">{$alternative_list[list].alt_link_id}<input type="checkbox"
      name="alt_link_id[]"
      value="{$alternative_list[list].alt_link_id}" {if $alternative_list[list].alt_link_id eq 'y'}checked="checked" {/if}/>
    </td>
    <td class="thin"><a class="link" href="javascript:popupP('DocManage.php?documentDetail=1&document_id={$alternative_list[list].document_id}&space={$alternative_list[list].doc_space}','documentDetailWindow', 600 , 1024)">{$alternative_list[list].document_number}</a></td>
    <td class="thin">{$alternative_list[list].designation}</td>
    <td class="thin">{$alternative_list[list].document_indice_id|document_indice}.{$alternative_list[list].document_version}</td>
   </tr>
  {sectionelse}
  <tr>
  	<td class="{cycle advance=false}" colspan="7">
  	{tr}No alternatives for this document{/tr}
  	</td>
  </tr>	
  {/section}
</table>

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="AddAlternative" title="{tr}Add alternative{/tr}" id="01"
 onclick="pop_no(linkform)">
<img class="icon" src="./img/icons/doclink/addson.png" title="{tr}Add alternative{/tr}" alt="{tr}Add alternative{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="SuppressAlter" title="{tr}Suppress alternative{/tr}" id="01"
 onclick="pop_no(linkform)">
<img class="icon" src="./img/icons/doclink/delson.png" title="{tr}Suppress alternative{/tr}" alt="{tr}Suppress alternative{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="document_id" value="{$document_id|escape}" />
<input type="hidden" name="documentDetail" value="1" />
<input type="hidden" name="frompage" value="{$smarty.template}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="page_id" value="detailWindow" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>
