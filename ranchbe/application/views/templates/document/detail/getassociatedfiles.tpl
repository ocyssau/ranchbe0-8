{*Smarty template*}
{*================================= ASSOCIATED FILES ===================================================*}

<h3>{tr}Associated files to{/tr} : {$document_name}</h3>

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">

{*--------------------list of files----------------------------------*}
{include file="recordfile/_list.tpl"}

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
onclick="if(confirm('{tr}Do you want really suppress this file{/tr}'))
{ldelim}document.checkform.action='./docfile/index/suppress';
pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="ResetFile" title="{tr}Reset{/tr}" id="03"
onclick="document.checkform.action='./docfile/index/reset' ; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_cancel.png" title="{tr}Reset{/tr}" alt="{tr}Reset{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="putInWs" title="{tr}Put in wildspace{/tr}"
onclick="document.checkform.action='./docfile/index/putinws' ; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_read.png" title="{tr}Put in wildspace{/tr}" alt="{tr}Put in wildspace{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="setRole" title="{tr}Set docfile role{/tr}"
onclick="document.checkform.action='./docfile/index/setrole' ; pop_no(checkform)">
<img class="icon" src="./img/icons/docfile/role.png" title="{tr}Set docfile role{/tr}" alt="{tr}Set role{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="document.checkform.action='{$sameurl}'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="document_id" value="{$document_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="page_id" value="detailWindow" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />

<input type="hidden" name="ifSuccessModule" value="{$ifSuccessModule}" />
<input type="hidden" name="ifSuccessController" value="{$ifSuccessController}" />
<input type="hidden" name="ifSuccessAction" value="{$ifSuccessAction}" />

<input type="hidden" name="ifFailedModule" value="{$ifFailedModule}" />
<input type="hidden" name="ifFailedController" value="{$ifFailedController}" />
<input type="hidden" name="ifFailedAction" value="{$ifFailedAction}" />

</form>

{*================================= EMBEDED VIEWER ===================================================*}

{if $embededViewer}
  {$embededViewer}
{else}
  <b>{tr}documentDetailWindow_message1{/tr}</b><br />
{/if}
