{*Smarty template*}

{*===================== DOCUMENT STRUCTURE ==============================================================*}
<h3>{tr}Son documents of{/tr} : {$document_name}</h3>

{*--------------------list header----------------------------------*}
<form name="linkform" method="post" action="">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">{tr}Number{/tr}</th>

  <th width="50" class="heading" nowrap>{tr}description{/tr}</th>
  <th class="heading">{tr}Indice{/tr} - {tr}Version{/tr}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$doclinked_list}
   <tr class="{cycle}">
   <!--{*
    <td class="thin">{$doclinked_list[list].rs_link_id}<input type="checkbox" 
                            name="rs_link_id[]"
                            value="{$doclinked_list[list].rs_link_id}" {if $doclinked_list[list].rs_link_id eq 'y'}checked="checked" {/if}/>
    </td>
    <td class="thin"><a class="link" href="javascript:popupP('DocManage.php?documentDetail=1&document_id={$doclinked_list[list].document_id}&space={$doclinked_list[list].doc_space}','documentDetailWindow', 600 , 1024)">{$doclinked_list[list].document_number}</a></td>
    <td class="thin">{$doclinked_list[list].description}</td>
    <td class="thin">{$doclinked_list[list].document_version|indice}.{$doclinked_list[list].document_iteration}
    {if $doclinked_list[list].rs_access_code > 14}<img class="icon" src="./img/icons/lock.png" />{/if}
    </td>
    *}-->

    <td class="thin">{$doclinked_list[list].dr_link_id}<input type="checkbox" 
                            name="dr_link_id[]"
                            value="{$doclinked_list[list].dr_link_id}" {if $doclinked_list[list].dr_link_id eq 'y'}checked="checked" {/if}/>
    </td>
    <td class="thin"><a class="link" href="javascript:popupP('document/detail/get/document_id/{$doclinked_list[list].document_id}/space/{$doclinked_list[list].space}'
                                          ,'documentDetailWindow', 600 , 1024)">
                                {$doclinked_list[list].document_number}</a></td>
    <td class="thin" nowrap>{$doclinked_list[list].description}</td>
    <td class="thin">{$doclinked_list[list].document_version|indice}
                      .{$doclinked_list[list].document_iteration}
    {if $doclinked_list[list].dr_access_code > 14}
        <img class="icon" src="./img/icons/lock.png" />{/if}
    </td>


   </tr>

  {sectionelse}
  <tr>
  	<td class="{cycle advance=false}" colspan="7">
  	{tr}No document is linked to this document{/tr}
  	</td>
  </tr>	
  {/section}
</table>

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="AddSon" title="{tr}Add son{/tr}" id="01"
 onclick="document.linkform.action='./document/relation/addson';
 pop_no(linkform)">
<img class="icon" src="./img/icons/doclink/addson.png" title="{tr}Add son{/tr}" alt="{tr}Add son{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="SuppressSon" title="{tr}Suppress son{/tr}" id="01"
 onclick="document.linkform.action='./document/relation/suppressson';
 pop_no(linkform)">
<img class="icon" src="./img/icons/doclink/delson.png" title="{tr}Suppress son{/tr}" alt="{tr}Suppress son{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="ViewFatherDoc" title="{tr}View father document{/tr}" id="01"
 onclick="document.linkform.action='./document/relation/getfathers';
 pop_it(linkform)">
<img class="icon" src="./img/icons/doclink/document_father.png" title="{tr}View father document{/tr}" alt="{tr}View father document{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="document_id" value="{$document_id|escape}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
<input type="hidden" name="ticket" value="{$ticket}" />

<input type="hidden" name="ifSuccessModule" value="{$ifSuccessModule}" />
<input type="hidden" name="ifSuccessController" value="{$ifSuccessController}" />
<input type="hidden" name="ifSuccessAction" value="{$ifSuccessAction}" />

<input type="hidden" name="ifFailedModule" value="{$ifFailedModule}" />
<input type="hidden" name="ifFailedController" value="{$ifFailedController}" />
<input type="hidden" name="ifFailedAction" value="{$ifFailedAction}" />

</form>
