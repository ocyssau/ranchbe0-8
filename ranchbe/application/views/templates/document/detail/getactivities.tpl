{*Smarty template*}
{*========================= DETAIL OF INSTANCES ======================================================*}

<h3>{tr}Detail instance{/tr} : {$iid}</h3>

<table class="normal">
<tr><td>
</td></tr>
<tr>
	<td class="formcolor" colspan="2">
		{if count($acts)}
		<table class="normal">
			<tr>
				<td class="formcolor">{tr}Name{/tr}</td>
				<td class="formcolor">{tr}Started{/tr}</td>
				<td class="formcolor">{tr}Act status{/tr}</td>
				<td class="formcolor">{tr}Expiration Date{/tr}</td>
				<td class="formcolor">{tr}Ended{/tr}</td>
				<td class="formcolor">{tr}User{/tr}</td>
			</tr>
		{section name=ix loop=$acts}
			<tr>
				
        {*<td class="{cycle values='odd,even' advance=false}"><a href="galaxia_admin_instance_activity.php?iid={$iid}&aid={$acts[ix].activityId}">{$acts[ix].name}</a></td>*}
				
        <td class="{cycle values='odd,even' advance=false}"><a href="{$smarty.server.PHP_SELF}?iid={$iid}&document_id={$document_id}&aid={$acts[ix].activityId}&documentDetail=1#3">{$acts[ix].name}</a></td>
				<td class="{cycle advance=false}">{$acts[ix].iaStarted|date_format:"%b %e, %Y - %H:%M"|capitalize}</td>
				<td class="{cycle advance=false}">{$acts[ix].actstatus}</td>
				<td class="{cycle advance=false}">{if $acts[ix].exptime eq 0 && $acts[ix].type eq 'activity' && $acts[ix].isInteractive eq 'y'}{tr}Not Defined{/tr}{elseif $acts[ix].type != 'activity'}&lt;{$acts[ix].type}&gt;{elseif $acts[ix].isInteractive eq 'n'}{tr}Not Interactive{/tr}{else}{$acts[ix].exptime|date_format:"%b %e, %Y - %H:%M"|capitalize}{/if}</td>
				<td class="{cycle advance=false}">{if $acts[ix].ended eq 0}{tr}Not Ended{/tr}{else}{$acts[ix].ended|date_format:"%b %e, %Y - %H:%M"|capitalize}{/if}</td>
				<td class="{cycle values='odd,even'}">{$acts[ix].user}</td>
		{/section}
		{else}
			none instance to show
		{/if}
		</table>
	</td>
</tr>	
</tr>	
</table>
