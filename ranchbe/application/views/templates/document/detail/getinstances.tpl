{*Smarty template*}

{*=================================LISTING OF INSTANCES===============================================*}
<h3>{tr}Monitor instances of process{/tr} : {$proc_info.name} {$proc_info.version}</h3>
<br />

{*LISTING*}
<form action="document/detail/getinstances" method="post">
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="where" value="{$where|escape}" />
<input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
<table class="normal">
<tr>
<th class="heading" >{tr}Id{/tr}</a></th>
{*<th class="heading" >{tr}Activity{/tr}</a></th>*}
<th class="heading" >{tr}Name{/tr}</a></th>
<th class="heading" >{tr}Process{/tr}</a></th>
<th class="heading" >{tr}Started{/tr}</a></th>
<th class="heading" >{tr}Status{/tr}</a></th>
<th class="heading" >{tr}Ended{/tr}</a></th>
<th class="heading" >{tr}Creator{/tr}</a></th>
</tr>
{cycle values="odd,even" print=false}
{foreach from=$instances item=proc}
<tr>
	<td class="{cycle advance=false}">
	  	{$proc.instanceId}
	</td>

	<td class="{cycle advance=false}" style="text-align:center;">
		<a class="link" href="{$smarty.server.PHP_SELF}?iid={$proc.instanceId}&document_id={$document_id}&documentDetail=1#2">{$proc.insName}
	</td>

	<td class="{cycle advance=false}" style="text-align:center;">
		{tr}{$proc.name}{/tr}
	</td>

	<td class="{cycle advance=false}" style="text-align:center;">
		{$proc.started|date_format}
	</td>

  <td class="{cycle advance=false}" style="text-align:center;">
		{$proc.status}
	</td>
  
  <td class="{cycle advance=false}" style="text-align:center;">
		{if $proc.ended eq 0} {tr}Not ended{/tr} {else} {$proc.ended|date_format} {/if}
	</td>

	<td class="{cycle advance=false}" style="text-align:center;">
	{$proc.owner}
	</td>
</tr>
{foreachelse}
<tr>
	<td class="{cycle advance=false}" colspan="7">
	{tr}No instances created yet{/tr}
	</td>
</tr>	
{/foreach}
</table>
</form>
{* END OF LISTING OF INSTANCES *}
