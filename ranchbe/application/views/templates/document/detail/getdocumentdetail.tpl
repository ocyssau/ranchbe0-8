{*Smarty template*}
{*================================= DETAIL OF DOC ===================================================*}
<h3>{$docinfos.document_number} - {$docinfos.document_version|indice}.{$docinfos.document_iteration}<br />
 <i>{$docinfos.description}</i></h3>

<table><tr>
<td width="50%">
  <table class="normal">
    {cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Basic properties{/tr}</td></tr>
    <tr class="{cycle}"><td>{tr}Number{/tr}</td>
    <td class="thin">
    {$docinfos.document_number}
    {*get_doc_postit document_id=$docinfos.document_id space=$SPACE_NAME
      ifSuccessModule=document
      ifSuccessController=detail
      ifSuccessAction=index
    *}

    </td></tr>
    <tr class="{cycle}"><td>{tr}Description{/tr}</td><td class="thin">{$docinfos.description}</td></tr>
    <tr class="{cycle}"><td>{tr}Indice{/tr}.{tr}Iteration{/tr}</b></td><td class="thin">{$docinfos.document_version|indice}.{$docinfos.document_iteration}</td></tr>
    <tr class="{cycle}"><td>{tr}Category{/tr}</td><td class="thin">{$docinfos.category_id|category}</td></tr>
    <tr class="{cycle}"><td>{tr}Doctype{/tr}</td><td class="thin">{$docinfos.doctype_id|doctype}</td></tr>

    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Life cycle{/tr}</td></tr>
    <tr class="{cycle}"><td>{tr}access{/tr}</td><td class="thin">{$docinfos.document_access_code|access_code}</td></tr>
    <tr class="{cycle}"><td>{tr}State{/tr}</td><td class="thin">{$docinfos.document_state}</td></tr>
    <tr class="{cycle}"><td>{tr}default_process{/tr}</td><td class="thin"> {$docinfos.default_process_id}</td></tr>

    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Date and time{/tr}</td></tr>
    {if $docinfos.check_out_by}
    <tr class="{cycle}"><td>{tr}CheckOut{/tr}</td><td class="thin">{tr}By{/tr} {$docinfos.check_out_by|username} {tr}at{/tr} {$docinfos.check_out_date|date_format}</td></tr>
    {/if}
    <tr class="{cycle}"><td>{tr}Create{/tr}</td><td class="thin">{tr}By{/tr} {$docinfos.open_by|username} {tr}at{/tr} {$docinfos.open_date|date_format}</td></tr>
    <tr class="{cycle}"><td>{tr}Last Update{/tr}</td><td class="thin">{tr}By{/tr} {$docinfos.update_by|username} {tr}at{/tr} {$docinfos.update_date|date_format}</td></tr>

    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Other properties{/tr}</td></tr>
    <tr class="{cycle}"><td>{tr}document_id{/tr}</td><td class="thin">{$docinfos.document_id}</td></tr>
    {if $docinfos.from_document}
    <tr class="{cycle}">
      <td>{tr}from_document{/tr}</td>
      <td> {$docinfos.from_document}
      <a class="link" href="javascript:popupP('document/detail/getdetailwindow/document_id/{$docinfos.from_document}/space/{$SPACE_NAME}',
                  'documentDetailWindow', 600 , 1024)">{tr}See details{/tr}</a>
      </td>
    </tr>
    {/if}
  </table>
</td>
<td width="50%"> <!-- Display optionnals fields -->
  <table class="normal">
    <tr class="{cycle}"><td class="heading" colspan="2">{tr}Extends properties{/tr}</td></tr>
    {foreach item=type from=$optionalFields}
    {assign var="fn" value=$type.property_name}
    <tr class="{cycle}">
      <td>{tr}{$type.property_description}{/tr}</td>
      <td class="thin">{filter_select id=$docinfos.$fn type=$type.property_type}</td>
    </tr>
    {/foreach}
  </table>  
</td>
<td width="50%"> <!-- Display optionnals fields -->
  <table class="normal">
    <tr>
      <td>
      <!--{if $documentVisu}{$documentVisu}{else}{$documentPicture}{/if}-->
      {get_visu space=$SPACE_NAME document_id=$docinfos.document_id}
      </td>
    </tr>
  </table>
</td>
</tr>
</table>

{doc_action_menu 
docinfos=$docinfos 
space=$SPACE_NAME 
ticket=$ticket 
displayText = true
mode='button'
ifSuccessModule='document'
ifSuccessController='detail'
ifSuccessAction='index'
ifFailedModule='document'
ifFailedController='detail'
ifFailedAction='index'}


<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="document.location.href='document/detail/get/document_id/{$docinfos.document_id}/space/{$SPACE_NAME}';
   return false;">
<img class="icon" src="img/icons/refresh.png" title="{tr}Refresh{/tr}" 
                  alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

