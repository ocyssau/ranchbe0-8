{*Smarty template*}

{*--------------------list header----------------------------------*}
<table class="normal" id="scroll">
<thead class="fixedHeader">
 <tr class="fixedHeader">
  <th class="heading auto" width="50px">
  
			         {doc_action_menu 
				        space=$SPACE_NAME 
				        ticket=$ticket
				        displayText = true
				        mode='main'
				        formId = 'checkform'
				        ifSuccessModule='content'
				        ifSuccessController='get'
				        ifSuccessAction='document'
				        ifFailedModule='content'
				        ifFailedController='get'
				        ifFailedAction='document'
				      }
	<input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxes(this.form, 'document_id[]', this.checked);">
	<div id="displaySelectedRowCount"></div>
  </th>
  <th class="heading auto">

  {column_header sort_order='INV' sort_field='doctype_id'}
  <br />
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field='document_number'}
  - 
  {column_header sort_order='INV' sort_field='document_version'}
  .
  {column_header sort_order='INV' sort_field='document_iteration'}
  <br />
  <i>{column_header sort_order='INV' sort_field='description'}</i>
  <br />
  <a {popup text='<b>Access code legend</b> :<ul><li><b>0</b>:All access free</li><li><b>1</b>:Modification in progress, checkout forbidden</li><li><b>5</b>:workflow in progress, checkout forbidden</li><li><b>10</b>:Document validate, checkout and workflow request forbidden</li><li><b>11</b>:Lock by user, checkout and workflow request forbidden</li><li><b>15</b>:Indice is upgraded, all action forbidden</li></ul>'}class="tableheading" href="{sameurl sort_order='INV' sort_field='document_access_code'}">
  {tr}Access{/tr}</a> -
  {column_header sort_order='INV' sort_field='document_state'}
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field='category_id'}
  </th>

  <th class="heading">
  {tr}CheckOut{/tr}
  {column_header sort_order='INV' sort_field='check_out_by' title='by'}
   - 
  {column_header sort_order='INV' sort_field='check_out_date' title='date'}
  <br />
  {tr}Last Update{/tr}
  {column_header sort_order='INV' sort_field='update_by' title='by'}
   - 
  {column_header sort_order='INV' sort_field='update_date' title='date'}
  <br />
  {tr}Created{/tr}
  {column_header sort_order='INV' sort_field='open_by' title='by'}
   - 
  {column_header sort_order='INV' sort_field='open_date' title='date'}
  </th>

  <!-- Display optionnals fields -->
  {section name=of loop=$optionalFields}
    <th class="heading">
    {column_header  sort_order='INV' 
                    sort_field=$optionalFields[of].property_fieldname
                    title=$optionalFields[of].property_name}
    </th>
  {/section}
 </tr>
</thead>

{*--------------------list Body----------------------------------*}
<tbody class="scrollContent" id="contentTbody">
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
  {if $list[list].document_access_code > 14}
   <tr class="historical">
  {else}
   <tr class="{cycle}">{/if}
    <td class="thin">
    <input type="checkbox" name="document_id[]" value="{$list[list].document_id}" id="checkbox_tbl_{$smarty.section.list.index}" {if $list[list].checked eq 'y'}checked="checked" {/if} /><br />
    {$list[list].document_access_code|access_code_concis}</td>

    <td class="thin">
    <a href="get/file/document_id/{$list[list].document_id}/ticket/{$ticket}/space/{$SPACE_NAME}" title="{tr}View document{/tr}">
    {doctype_icon doctype_id = $list[list].doctype_id}
    {if $filter_options.displayThumbs}
      {get_thumbnail document_id=$list[list].document_id space=$SPACE_NAME}
    {/if}
    </a>
    <font size="-3"><br /><i>{$list[list].doctype_id|doctype}</i></font>
    </td>

    <td class="thin" nowrap>
    <a class="link" 
    	href="get/file/document_id/{$list[list].document_id}/ticket/{$ticket}/space/{$SPACE_NAME}"
    	title="{tr}View document{/tr}">
    {$list[list].document_number} - {$list[list].document_version|indice}.{$list[list].document_iteration}</a>
    {get_doc_postit document_id=$list[list].document_id space=$SPACE_NAME}
     <a href="javascript:popupP('document/detail/getdetailwindow/document_id/{$list[list].document_id}/space/{$SPACE_NAME}','documentDetailWindow', 600 , 1024)">
     {*<a href="document/detail/getdetailwindow/document_id/{$list[list].document_id}/space/{$SPACE_NAME}">*}
    {tr}See details{/tr}
    </a>
    <br />
    <i>{$list[list].description}</i>
    <br />
    {$list[list].document_access_code|access_code} - 
    {$list[list].document_state}
			         {doc_action_menu 
				        infos=$list[list]
				        space=$SPACE_NAME 
				        ticket=$ticket
				        displayText = true
				        mode='contextual'
				        ifSuccessModule='content'
				        ifSuccessController='get'
				        ifSuccessAction='document'
				        ifFailedModule='content'
				        ifFailedController='get'
				        ifFailedAction='document'
				      }
    </td>
    <td class="thin">{$list[list].category_id|category}</td>
    <td class="thin" nowrap>
      {if !empty($list[list].check_out_by)}
      <b>{tr}Checkout{/tr} : </b>{$list[list].check_out_by|username} - {$list[list].check_out_date|date_format}
      <br />
      {/if}
      <b>{tr}Last Update{/tr} : </b>{$list[list].update_by|username} - {$list[list].update_date|date_format}
      <br />
      <b>{tr}Created{/tr} : </b>{$list[list].open_by|username} - {$list[list].open_date|date_format}
    </td>
    <!-- Display optionnals fields -->
    {foreach key=key item=type from=$optionalFields}
      {assign var="fn" value=$type.property_fieldname}
      <td class="thin">{filter_select id=$list[list].$fn type=$type.property_type}</td>
    {/foreach}
   </tr>
  {/section}
</tbody> 
</table>


