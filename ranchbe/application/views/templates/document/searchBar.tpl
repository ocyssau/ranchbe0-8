{*Smarty template*}

{*--------------------Search Bar defintion--------------------------*}
<div id="searchbar">
<fieldset>
<form name="docfilterform" id="docfilterform" action="{$sameurl}" method="post">
<input type="hidden" name="activatefilter" value="1" />

{*sameurlpost*}
  <input type="hidden" name="displayHistory" value="0" />
  <label><input type="checkbox" name="displayHistory" value="1" {if $filter_options.displayHistory}"checked"{/if} onChange='javascript:getElementById("docfilterform").submit()';/>
  <small>{tr}Display history{/tr}</small></label>

  <input type="hidden" name="onlyMy" value="0" />
  <label><input type="checkbox" name="onlyMy" value="1" {if $filter_options.onlyMy}"checked"{/if} onChange='javascript:getElementById("docfilterform").submit()';/>
  <small>{tr}Only me{/tr}</small></label>

  <input type="hidden" name="checkByMe" value="0" />
  <label><input type="checkbox" name="checkByMe" value="1" {if $filter_options.checkByMe}"checked"{/if} onChange='javascript:getElementById("docfilterform").submit()';/>
  <small>{tr}Only check by me{/tr}</small></label>

  <input type="hidden" name="displayThumbs" value="0" />
  <label><input type="checkbox" name="displayThumbs" value="1" {if $filter_options.displayThumbs}"checked"{/if} onChange='javascript:getElementById("docfilterform").submit()';/>
  <small>{tr}Display thumbnails{/tr}</small></label>

  <label>
  <input size="4" type="text" name="numrows" value="{$filter_options.numrows|escape}" />
  <small>{tr}rows to display{/tr}</small></label>
<br />

  {capture name='help1'}{tr}searchBar_help_1{/tr}{/capture}
  <img border="0" alt="help" src="img/icons/help.png" 
    {popup text=$smarty.capture.help1}
  />

  <label for="find_document_number"><small>{tr}Number{/tr}</small></label>
  <input size="16" type="text" name="find_document_number" value="{$filter_options.find_document_number}" />

  <label for="find_document_description"><small>{tr}description{/tr}</small></label>
  <input size="16" type="text" name="find_document_description" value="{$filter_options.find_document_description}" />

  <label for="find_doctype"><small>{tr}Doctype{/tr}</small></label>
  <input size="8" type="text" name="find_doctype" value="{$filter_options.find_doctype|escape}" />

  <label for="find_category"><small>{tr}Category{/tr}</small></label>
	<select name="find_category">
	<option {if '' eq $find_field}selected="selected"{/if} value=""></option>
	{foreach from=$category_list item=name key=field}
   <option {if $field eq $filter_options.find_category}selected="selected"{/if} value="{$field|escape}">{$name}</option>
	{/foreach}
	</select>

  <label for="find_document_access_code"><small>{tr}Access{/tr}</small></label>
	<select name="find_document_access_code">
	   <option value=""></option>
	   <option {if $filter_options.find_document_access_code eq 'free'}selected="selected"{/if} value="free">{tr}Free{/tr}</option>
	   <option {if $filter_options.find_document_access_code eq 1}selected="selected"{/if}value="1">{tr}CheckedOut{/tr}</option>
	   <option {if $filter_options.find_document_access_code eq 5}selected="selected"{/if} value="5">{tr}InWorkflow{/tr}</option>
	   <option {if $filter_options.find_document_access_code eq 10}selected="selected"{/if} value="10">{tr}Validated{/tr}</option>
	   <option {if $filter_options.find_document_access_code eq 11}selected="selected"{/if} value="11">{tr}Locked{/tr}</option>
	   <option {if $filter_options.find_document_access_code eq 12}selected="selected"{/if} value="12">{tr}Marked to suppress{/tr}</option>
	   <option {if $filter_options.find_document_access_code eq 15}selected="selected"{/if} value="15">{tr}Historical{/tr}</option>
  </select>

  <label for="find_document_state"><small>{tr}State{/tr}</small></label>
  <input size="8" type="text" name="find_document_state" value="{$filter_options.find_document_state|escape}" />

  <input type="hidden" name="space" value={$SPACE_NAME} />
  <input type="hidden" name="filter" value=1 />
  <input type="submit" name="filter" value="{tr}filter{/tr}" />
  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />

<br />

{literal}
<script language="JavaScript">
function displayOption(cb,span){

    var a,b;

    var a = document.getElementById(cb);
    var b = document.getElementById(span);
    if(a.checked==true)
        b.style.display = "block";
    else b.style.display = "none";
    return true;

}

function initOption(){
displayOption('f_adv_search_cb', 'f_adv_search_span');
displayOption('f_dateAndTime_cb', 'f_dateAndTime_span');
}

</script>
{/literal}

<input type="hidden" name="f_adv_search_cb" value="0" />
<label><small>{tr}Advanced filter{/tr}</small>
  <input type="checkbox" name="f_adv_search_cb" value="1" {if $filter_options.f_adv_search_cb}"checked"{/if} id="f_adv_search_cb" onClick="displayOption('f_adv_search_cb', 'f_adv_search_span');" />
</label>
<span id=f_adv_search_span style="display: none">
<fieldset>

  <label for="find"><small>{tr}find{/tr}</small></label>
  <input size="16" type="text" name="find" value="{$filter_options.find|escape}" />

  <label for="find_field"><small>{tr}In{/tr}</small></label>
	<select name="find_field">
	<option {if '' eq $filter_options.find_field}selected="selected"{/if} value=""></option>
	{foreach from=$find_elements item=name key=field}
   <option {if $field eq $filter_options.find_field}selected="selected"{/if} value="{$field|escape}">{$name}</option>
	{/foreach}
	</select>

  <label for="f_action_field"><small>{tr}Action{/tr}</small></label>
	<select name="f_action_field">
	<option {if $filter_options.f_action_field eq ''}selected="selected"{/if} value=""></option>
	{foreach from=$user_elements item=name key=field}
   <option value="{$name}" {if $filter_options.f_action_field eq $name}selected="selected"{/if}>{tr}{$name}{/tr}</option>
	{/foreach}
	</select>

  <label for="f_action_user_name"><small>{tr}By{/tr}</small></label>
	<select name="f_action_user_name">
	<option {if $filter_options.f_action_user_name eq ''}selected="selected"{/if} value=""></option>
	{foreach from=$user_list item=name key=id}
   <option value="{$id}" {if $filter_options.f_action_user_name eq $id}selected="selected"{/if}>
    {$name.handle}
   </option>
	{/foreach}
	</select>

<label><small>{tr}Date and time{/tr}</small>
<input type="checkbox" name="f_dateAndTime_cb" value="1" id="f_dateAndTime_cb" {if $filter_options.f_dateAndTime_cb}"checked"{/if} onClick="displayOption('f_dateAndTime_cb', 'f_dateAndTime_span');" />
</label>
<span id=f_dateAndTime_span style="display: none">
<fieldset>

{literal}
<script type="text/javascript">
//<![CDATA[
    dojo.require("dijit.form.DateTextBox");
</script>
{/literal}

{foreach from=$date_elements item=name}
<div>
  <label><small>{tr}{$name}{/tr}</small>
  <input type="checkbox" name="{$name}_sel" value="1" id="{$name}_sel" onClick="displayOption('{$name}_sel','{$name}_span');" />
  </label>

  <span id={$name}_span style="display: none">
	  <fieldset>
	  <label><input type="radio" name="{$name}_cond" "checked" value=">"/>
	  <small>{tr}Superior to{/tr}</small></label>
	
	  <label><input type="radio" name="{$name}_cond" value="<" />
	  <small>{tr}Inferior to{/tr}</small></label>
	  <input id="{$name}" name="{$name}" value="" 
	  	type="text" dojoType="dijit.form.DateTextBox" style="width:150px" />
	  </fieldset>
  </span>
</div>
{/foreach}

</fieldset>

</span>

</fieldset>
</span>

</fieldset>

</form>

</div>

{literal}
<script language="JavaScript">
  initOption();
</script>
{/literal}
