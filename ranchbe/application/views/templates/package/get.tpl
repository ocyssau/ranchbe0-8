{*Smarty template*}

<div id="page-bar">
  <span class="button2"><a class="linkbut" href="javascript:popupP('importManager/history/get/space/{$SPACE_NAME}','{$randWindowName}',800 , 1200)">{tr}History import{/tr}</a></span>
</div>

<h1 class="pagetitle">{$PageTitle}</h1>

{*--------------------list header----------------------------------*}

{*--------------------Search Bar defintion--------------------------*}
{*assign var="histoOption" value="1"*}
{$views_helper_filter_form|toggleanim}

{literal}
<script type="text/javascript">
	initTbody();
</script>
{/literal}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">
{include file='import/package/_list.tpl'}

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>
<button class="mult_submit" type="submit" name="action" value="suppressPackage" title="{tr}Suppress{/tr}"
onclick="if(confirm('{tr}Do you want really suppress this file{/tr}')){ldelim}
document.checkform.action='./importManager/package/suppress';
pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="uncompressPackage" title="{tr}Uncompress{/tr}"
onclick="document.checkform.action='./importManager/package/uncompress';
pop_no(checkform);">
<img class="icon" src="./img/icons/package/package_uncompress.png" title="{tr}Uncompress{/tr}" alt="{tr}Uncompress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="update" title="{tr}Refresh{/tr}"
onclick="document.checkform.action='./importManager/package/get';
pop_no(checkform);">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p></p>

<fieldset>
<legend align="top"><i>{tr}Import control{/tr}</i></legend>
<table>

  {if $form.target}
  <tr>
    <td class="formcolor">{$form.target.label}:</td>
    <td class="formcolor">{$form.target.html}</td>
  </tr>
  {/if}

  {if $form.background}
  <tr>
    <td class="formcolor">{$form.background.label}:</td>
    <td class="formcolor">{$form.background.html}</td>
  </tr>
  {/if}

<tr><td>
  <button class="mult_submit" type="submit" name="action" value="unpack" title="{tr}Update mockup{/tr}" id="01"
  onclick="document.checkform.action='./importManager/package/import';
  pop_it(checkform);">
  <img class="icon" src="./img/icons/tick.png" title="{tr}Update mockup{/tr}" alt="{tr}Update mockup{/tr}" width="16" height="16" />
  </button>
 </td>

</tr>
</table>
  </fieldset>

<p></p>

<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
<input type="hidden" name="ticket" value="{$ticket}" />
</form>


{* ------------------- Upload section ------------------------ *}
<form action="./importManager/package/upload" method="post" name="upload" enctype="multipart/form-data">
  <fieldset>
    <legend align="top"><i>{tr}Add a package to the package reposit dir{/tr}</i></legend>
    <input type="file" name="uploadFile"/>
    <input type="submit" value="upload" name="action" /><br />
    {tr}Overwrite{/tr}:
    <input type="checkbox" name="overwrite" />
  </fieldset>

  <input type="hidden" name="container_id" value="{$container_id}" />
  <input type="hidden" name="space" value="{$SPACE_NAME}" />
  <input type="hidden" name="ticket" value="{$ticket}" />
</form>
