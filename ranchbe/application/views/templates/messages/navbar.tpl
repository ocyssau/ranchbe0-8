{*Smarty template*}
<table>
<tr><td>
	<a href="messages/mailbox/index" class="button">
		<span class="exp">{tr}Mailbox{/tr}</span>
	</a>
</td><td>
	<a href="messages/compose/index" class="button">
		<span class="edit">{tr}Compose{/tr}</span>
	</a>
</td>
{if $tiki_p_broadcast eq 'y'}
<td>
	<a href="messages/broadcast/index" class="button">
		<span class="info">{tr}Broadcast{/tr}</span>
	</a>
</td>
{/if}
<td>
	<a href="messages/sent/index" class="button">
		<span class="send">{tr}Sent{/tr}</span>
	</a>
</td><td>
	<a href="messages/archive/index" class="button">
		<span class="folder">{tr}Archive{/tr}</span>
	</a>
</td><td>
	{if $mess_archiveAfter>0}({tr}Auto-archive age for read messages:{/tr}
    {$mess_archiveAfter} {tr}days{/tr}){/if}
</td></tr>
</table>
    