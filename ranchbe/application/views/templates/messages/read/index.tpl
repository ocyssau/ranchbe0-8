<h1><a class="pagetitle" href="messages/read/index/msgId/{$msgId}/dbsource/{$dbsource}">{tr}Read message{/tr}</a></h1>

{include file="messages/navbar.tpl"}

<br />
{if $prev}<a class="readlink" href="messages/read/index/msgId/{$prev}/dbsource/{$dbsource}/offset/{$offset}/sort_mode/{$sort_mode}/find/{$find}/flag/{$flag}/priority/{$priority}/flagval/{$flagval}">{tr}Prev{/tr}</a>{/if} 
{if $next}<a class="readlink" href="messages/read/index/msgId/{$next}/dbsource/{$dbsource}/offset/{$offset}/sort_mode/{$sort_mode}/find/{$find}/flag/{$flag}/priority/{$priority}/flagval/{$flagval}">{tr}Next{/tr}</a>{/if} 

<br /><br />

{if $legend}
  {$legend}
{else}
  <table>
  <tr><td>
    <form method="post" action="./messages/read/delete">
    <input type="hidden" name="offset" value="{$offset|escape}" />
    <input type="hidden" name="find" value="{$find|escape}" />
    <input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
    <input type="hidden" name="flag" value="{$flag|escape}" />
    <input type="hidden" name="flagval" value="{$flagval|escape}" />
    <input type="hidden" name="priority" value="{$priority|escape}" />
    <input type="hidden" name="msgdel" value="{$msgId|escape}" />
    <input type="hidden" name="dbsource" value="{$dbsource|escape}" />
    {if $next}
    <input type="hidden" name="msgId" value="{$next|escape}" />
    {elseif $prev}
    <input type="hidden" name="msgId" value="{$prev|escape}" />
    {else}
    <input type="hidden" name="msgId" value="" />
    {/if}
    <input type="submit" name="delete" value="{tr}delete{/tr}" />
    </form>
  </td>
  <td>
    <form method="post" action="./messages/compose/index">
    <input type="hidden" name="offset" value="{$offset|escape}" />
    <input type="hidden" name="msgId" value="{$msgId|escape}" />
    <input type="hidden" name="find" value="{$find|escape}" />
    <input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
    <input type="hidden" name="flag" value="{$flag|escape}" />
    <input type="hidden" name="priority" value="{$priority|escape}" />
    <input type="hidden" name="flagval" value="{$flagval|escape}" />
    <input type="hidden" name="to" value="{$msg.user_from|escape}" />
    <input type="hidden" name="msgId" value="{$msgId|escape}" />
    <input type="hidden" name="replyto_hash" value="{$msg.hash}" />
    <input type="submit" name="reply" value="{tr}reply{/tr}" />
    </form>
  </td>
  <td>
    <form method="post" action="./messages/compose/index">
    <input type="hidden" name="offset" value="{$offset|escape}" />
    <input type="hidden" name="find" value="{$find|escape}" />
    <input type="hidden" name="msgId" value="{$msgId|escape}" />
    <input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
    <input type="hidden" name="flag" value="{$flag|escape}" />
    <input type="hidden" name="priority" value="{$priority|escape}" />
    <input type="hidden" name="flagval" value="{$flagval|escape}" />
    <input type="hidden" name="to" value="{$msg.user_from|escape},{$msg.user_cc},{$msg.user_to}" />
    <input type="hidden" name="msgId" value="{$msgId|escape}" />
    <input type="hidden" name="replyto_hash" value="{$msg.hash}" />
    <input type="submit" name="replyall" value="{tr}replyall{/tr}" />
    </form>
  </td></tr>
  </table>

  <div class="messureadflag">
  {if $msg.isFlagged eq 'y'}
  <img alt="flag" src="img/icons/flagged.png" /><a class="link" href="messages/read/index/dbsource/{$dbsource}/offset/{$offset}/action/isFlagged/actionval/n/msgId/{$msgId}/sort_mode/{$sort_mode}/find/{$find}/flag/{$flag}/priority/{$priority}/flagval/{$flagval}">{tr}Unflag{/tr}</a>
  {else}
  <a class="link" href="messages/read/index/dbsource/{$dbsource}/offset/{$offset}/action/isFlagged/actionval/y/msgId/{$msgId}/sort_mode/{$sort_mode}/find/{$find}/flag/{$flag}/priority/{$priority}/flagval/{$flagval}">{tr}Flag this message{/tr}</a>
  {/if}
  </div>
  <div class="messureadhead">
  <table>
    <tr><td style="font-weight:bold;">{tr}From{/tr}:</td><td>{$msg.user_from}</td></tr>
    <tr><td style="font-weight:bold;">{tr}To{/tr}:</td><td>{$msg.user_to}</td></tr>
    <tr><td style="font-weight:bold;">{tr}Cc{/tr}:</td><td>{$msg.user_cc}</td></tr>
    <tr><td style="font-weight:bold;">{tr}Subject{/tr}:</td><td>{$msg.subject}</td></tr>
    <tr><td style="font-weight:bold;">{tr}Date{/tr}:</td><td>{$msg.date|date_format}</td></tr>
  </table>
  </div>
  <div class="messureadbody">
  {$msg.parsed}
  </div>
{/if}
