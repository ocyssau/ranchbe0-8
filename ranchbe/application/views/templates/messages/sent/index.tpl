{*Smarty template*}
<h1><a class="pagetitle" href="messages/sent/index">{tr}Sent Messages{/tr}</a></h1>

{include file="messages/navbar.tpl"}

{if $messu_sent_size gt '0'}
<br />
<table border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td>
			<table border='0' height='20' cellpadding='0' cellspacing='0'
			       width='200' style='background-color:#666666;'>
				<tr>
					<td style='background-color:red;' width='{$cellsize}'>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
		<td><small>{$percentage}%</small></td>
	</tr>
</table>
[{$messu_sent_number} / {$messu_sent_size}] {tr}messages{/tr}. {if $messu_sent_number ge $messu_sent_size}{tr}Sent box is full. Archive or delete some sent messages first if you want to send more messages.{/tr}{/if}
{/if}

<br /><br />
<form name="messagesentfilter" action="./messages/sent/index" method="get">
<label for="mess-mailmessages">{tr}Messages{/tr}:</label>

<select name="flags" id="mess-mailmessages">
<option value="isReplied_y" {if $flag eq 'isRead' and $flagval eq 'y'}selected="selected"{/if}>{tr}Replied{/tr}</option>
<option value="isReplied_n" {if $flag eq 'isRead' and $flagval eq 'n'}selected="selected"{/if}>{tr}Not replied{/tr}</option>
<option value="" {if $flag eq ''}selected="selected"{/if}>{tr}All{/tr}</option>
</select>

<label for="mess-mailprio">{tr}Priority{/tr}:</label>
<select name="priority" id="mess-mailprio">
<option value="" {if $priority eq ''}selected="selected"{/if}>{tr}All{/tr}</option>
<option value="1" {if $priority eq 1}selected="selected"{/if}>{tr}1{/tr}</option>
<option value="2" {if $priority eq 2}selected="selected"{/if}>{tr}2{/tr}</option>
<option value="3" {if $priority eq 3}selected="selected"{/if}>{tr}3{/tr}</option>
<option value="4" {if $priority eq 4}selected="selected"{/if}>{tr}4{/tr}</option>
<option value="5" {if $priority eq 5}selected="selected"{/if}>{tr}5{/tr}</option>
</select>
<label for="mess-mailcont">{tr}Containing{/tr}:</label>
<input type="text" name="find" id="mess-mailcont" value="{$find|escape}" />
<input type="submit" name="filter" value="{tr}filter{/tr}" />
</form>
<br />

<form name="messagesentlist" action="./messages/sent/index" method="post">
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
<input type="hidden" name="flag" value="{$flag|escape}" />
<input type="hidden" name="flagval" value="{$flagval|escape}" />
<input type="hidden" name="priority" value="{$priority|escape}" />

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
onclick="document.messagesentlist.action='./messages/sent/delete';pop_no(messagesentlist);return false;">
{tr}Suppress{/tr}</button>

{*
<button class="mult_submit" type="submit" name="action" value="archive" title="{tr}move to archive{/tr}"
onclick="document.messagesentlist.action='./messages/sent/archive';pop_no(messagesentlist);return false;">
{tr}move to archive{/tr}</button>
*}

<button class="mult_submit" type="submit" name="action" value="download" title="{tr}download{/tr}"
onclick="document.messagesentlist.action='./messages/sent/download';pop_no(messagesentlist);return false;">
{tr}download{/tr}</button>


<table class="normal" >
  <tr>
    <td class="heading" ><input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxe(this.form, 'checked', this.checked)"/></td>
    <td class="heading" >&nbsp;</td>

    <td class="heading" ><a class="tableheading" href="messages/sent/index/flag/{$flag}/priority/{$priority}/flagval/{$flagval}/find/{$find}/offset/{$offset}/sort_mode/{if $sort_mode eq 'user_to_desc'}user_to_asc{else}user_to_desc{/if}">{tr}receiver{/tr}</a></td>
    <td class="heading" ><a class="tableheading" href="messages/sent/index/flag/{$flag}/priority/{$priority}/flagval/{$flagval}/find/{$find}/offset/{$offset}/sort_mode/{if $sort_mode eq 'subject_desc'}subject_asc{else}subject_desc{/if}">{tr}subject{/tr}</a></td>
    <td class="heading" ><a class="tableheading" href="messages/sent/index/flag/{$flag}/priority/{$priority}/flagval/{$flagval}/find/{$find}/offset/{$offset}/sort_mode/{if $sort_mode eq 'date_desc'}date_asc{else}date_desc{/if}">{tr}date{/tr}</a></td>
    <td class="heading" ><a class="tableheading" href="messages/sent/index/flag/{$flag}/priority/{$priority}/flagval/{$flagval}/find/{$find}/offset/{$offset}/sort_mode/{if $sort_mode eq 'isReplied_desc'}isReplied_asc{else}isReplied_desc{/if}">{tr}replies{/tr}</a></td>
    <td style="text-align:right;" class="heading" >{tr}size{/tr}</td>
  </tr>
  {cycle values="odd,even" print=false}
  {section name=user loop=$items}
  <tr>
    <td class="prio{$items[user].priority}"><input type="checkbox" name="msg[{$items[user].msgId}]" /></td>
    <td class="prio{$items[user].priority}">{if $items[user].isFlagged eq 'y'}<img src="img/icons/flagged.png" border="0" width="16" height="16" alt='{tr}flagged{/tr}' />{/if}</td>
    <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">{$items[user].user_to}</td>
    <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">
    <a class="readlink" href="messages/read/index/dbsource/sent/offset/{$offset}/flag/{$flag}/priority/{$items[user].priority}/flagval/{$flagval}/sort_mode/{$sort_mode}/find/{$find}/msgId/{$items[user].msgId}">{$items[user].subject}</a>
    </td>
    <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">{$items[user].date|date_format}</td>

	<td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">

    {if $items[user].isReplied eq 'n'}
    {tr}no{/tr}
    {else}
      <a class="readlink" href="messages/mailbox/index/replyto={$items[user].hash}">
        <img src="img/icons/email/email_go.png" alt='{tr}replied{/tr}' border='1'/></a>&nbsp;
      <a href="tiki-user_information.php?view_user={$items[user].user_from}">
        {$items[user].user_from}</a>
    {/if}
  </td>

    <td  style="text-align:right;{if $items[user].isRead eq 'n'}font-weight:bold;{/if}" class="prio{$items[user].priority}">{$items[user].len}</td>
  </tr>
  {sectionelse}
  <tr><td colspan="6">{tr}No messages to display{/tr}<td></tr>
  {/section}
</table>

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
{literal} <script language='Javascript' type='text/javascript'>
<!--
	  function switchCheckboxe(tform, elements_id, state) {
	  	// checkboxes need to have the same name elements_id
	  	// e.g. <input type="checkbox" name="my_ename[]">, will arrive as Array in php.
	  	for (var i = 0; i < tform.length; i++) {
	  		if (tform.elements[i].id == elements_id) {
	  			tform.elements[i].checked = state
	  		}
	  	}
	  	return true;
	  }
	  initTbody();
 {/literal}
 //-->                     
 </script>

</form>
<div align="center">
<div class="mini">
{if $prev_offset >= 0}
[<a class="prevnext" href="messages/sent/index/find/{$find}/offset/{$prev_offset}/sort_mode/{$sort_mode}/priority/{$priority}/flag/{$flag}/flagval/{$flagval}">{tr}prev{/tr}</a>]
{/if}
{tr}Page{/tr}: {$actual_page}/{$cant_pages}
{if $next_offset >= 0}
[<a class="prevnext" href="messages/sent/index/find/{$find}/offset/{$next_offset}/sort_mode/{$sort_mode}/priority/{$priority}/flag/{$flag}/flagval/{$flagval}">{tr}next{/tr}</a>]
{/if}
</div>
</div>
