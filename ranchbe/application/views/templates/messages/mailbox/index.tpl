{*Smarty template*}
{include file="messages/navbar.tpl"}
<h1><a class="pagetitle" href="messages/mailbox/index">{tr}Messages{/tr}</a></h1>

{*-------------------- Filter ----------------------------------*}
<form name="mailboxfilter" action="./messages/mailbox/index" method="get">
	<label for="mess-mailmessages">{tr}Messages{/tr}:</label>
	<select name="flags" id="mess-mailmessages">
		<option value="isRead_y" {if $flag eq 'isRead' and $flagval eq 'y'}selected="selected"{/if}>{tr}IsRead{/tr}</option>
		<option value="isRead_n" {if $flag eq 'isRead' and $flagval eq 'n'}selected="selected"{/if}>{tr}IsUnread{/tr}</option>
		<option value="isFlagged_y" {if $flag eq 'isFlagged' and $flagval eq 'y'}selected="selected"{/if}>{tr}Flagged{/tr}</option>
		<option value="isFlagged_n" {if $flag eq 'isflagged' and $flagval eq 'n'}selected="selected"{/if}>{tr}Unflagged{/tr}</option>
		<option value="" {if $flag eq ''}selected="selected"{/if}>{tr}All{/tr}</option>
	</select>
	<label for="mess-mailprio">{tr}Priority{/tr}:</label>
	<select name="priority" id="mess-mailprio">
		<option value="" {if $priority eq ''}selected="selected"{/if}>{tr}All{/tr}</option>
		<option value="1" {if $priority eq 1}selected="selected"{/if}>{tr}1{/tr}</option>
		<option value="2" {if $priority eq 2}selected="selected"{/if}>{tr}2{/tr}</option>
		<option value="3" {if $priority eq 3}selected="selected"{/if}>{tr}3{/tr}</option>
		<option value="4" {if $priority eq 4}selected="selected"{/if}>{tr}4{/tr}</option>
		<option value="5" {if $priority eq 5}selected="selected"{/if}>{tr}5{/tr}</option>
	</select>
	<label for="mess-mailcont">{tr}Containing{/tr}:</label>
	<input type="text" name="find" id="mess-mailcont" value="{$find|escape}" />
	<input type="submit" name="filter" value="{tr}filter{/tr}" />
</form>
	
{if $messu_mailbox_size gt '0'}
<br />
<div>
</div>

<table border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td>
			<table border='0' height='20' cellpadding='0' cellspacing='0'
			       width='200' style='background-color:white;'>
				<tr>
					<td style='background-color:red;' width='{$cellsize}'>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
		<td><small>{$percentage}%</small></td>
	</tr>
</table>
[{$messu_mailbox_number} / {$messu_mailbox_size}] {tr}messages{/tr}. {if $messu_mailbox_number ge $messu_mailbox_size}{tr}Mailbox is full! Delete or archive some messages if you want to receive more messages.{/tr}{/if}
{/if}

<br /><br />



{*-------------------- list header ----------------------------------*}
<form name="mailboxlist" action="./messages/mailbox/index" method="post">
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
<input type="hidden" name="flag" value="{$flag|escape}" />
<input type="hidden" name="flagval" value="{$flagval|escape}" />
<input type="hidden" name="priority" value="{$priority|escape}" />

<button class="mult_submit" type="submit" name="suppress" value="suppress" title="{tr}Suppress{/tr}"
onclick="document.mailboxlist.action='./messages/mailbox/delete';pop_no(mailboxlist);return false;">
{tr}Suppress{/tr}</button>

<button class="mult_submit" type="submit" name="archive" value="archive" title="{tr}move to archive{/tr}"
onclick="document.mailboxlist.action='./messages/mailbox/archive';pop_no(mailboxlist);return false;">
{tr}move to archive{/tr}</button>

<button class="mult_submit" type="submit" name="download" value="download" title="{tr}download{/tr}"
onclick="document.mailboxlist.action='./messages/mailbox/download';pop_no(mailboxlist);return false;">
{tr}download{/tr}</button>

<select name="markas">
<option value="isRead_y">{tr}Mark as read{/tr}</option>
<option value="isRead_n">{tr}Mark as unread{/tr}</option>
<option value="isFlagged_y">{tr}Mark as flagged{/tr}</option>
<option value="isFlagged_n">{tr}Mark as unflagged{/tr}</option>
</select>
<button class="mult_submit" type="submit" name="mark" value="mark" title="{tr}mark{/tr}"
onclick="document.mailboxlist.action='./messages/mailbox/mark';pop_no(mailboxlist);return false;">
{tr}mark{/tr}</button>

{tr}archive messages older than{/tr} 
<select name="days">
<option value="5">5 {tr}days{/tr}</option>
<option value="10">10 {tr}days{/tr}</option>
<option value="20">20 {tr}days{/tr}</option>
<option value="40">40 {tr}days{/tr}</option>
<option value="60">60 {tr}days{/tr}</option>
<option value="80">80 {tr}days{/tr}</option>
<option value="100">100 {tr}days{/tr}</option>
</select>
<button class="mult_submit" type="submit" name="autoarchive" value="autoarchive" title="{tr}autoarchive{/tr}"
onclick="document.mailboxlist.action='./messages/mailbox/autoarchive';pop_no(mailboxlist);return false;">
{tr}autoarchive{/tr}</button>


<table class="normal" id="scroll">
<thead class="fixedHeader">
  <tr>
    <td class="heading" ><input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxe(this.form, 'checked', this.checked)"/></td>
    <td class="heading" >&nbsp;</td>
    <td class="heading" ><a class="tableheading" href="messages/mailbox/index/flag/{$flag}/priority/{$priority}/flagval/{$flagval}/find/{$find}/offset/{$offset}/sort_mode/{if $sort_mode eq 'user_from_desc'}user_from_asc{else}user_from_desc{/if}">
    {tr}sender{/tr}</a></td>
    <td class="heading" ><a class="tableheading" href="messages/mailbox/index/flag/{$flag}/priority/{$priority}/flagval/{$flagval}/find/{$find}/offset/{$offset}/sort_mode/{if $sort_mode eq 'subject_desc'}subject_asc{else}subject_desc{/if}">
    {tr}subject{/tr}</a></td>
    <td class="heading" ><a class="tableheading" href="messages/mailbox/index/flag/{$flag}/priority/{$priority}/flagval/{$flagval}/find/{$find}/offset/{$offset}/sort_mode/{if $sort_mode eq 'date_desc'}date_asc{else}date_desc{/if}">
    {tr}date{/tr}</a></td>
    <td class="heading" >
    {tr}reply to{/tr}</td>
    <td style="text-align:right;" class="heading" >
    {tr}size{/tr}</td>
  </tr>
</thead>
<tbody class="scrollContent" id="contentTbody">

{*-------------------- list body ----------------------------------*}
  {cycle values="odd,even" print=false}
  {section name=user loop=$items}
  <tr>
    <td class="prio{$items[user].priority}">
    <input type="checkbox" name="msg[{$items[user].msgId}]" id="checked" value="{$items[user].msgId}" /></td>
    <td class="prio{$items[user].priority}">{if $items[user].isFlagged eq 'y'}<img src="img/icons/flagged.png" border="0" width="16" height="16" alt='{tr}flagged{/tr}' />{/if}</td>
    <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">{$items[user].user_from}</td>
    <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">
    <a class="readlink" href="messages/read/index/offset/{$offset}/flag/{$flag}/priority/{$items[user].priority}/flagval/{$flagval}/sort_mode/{$sort_mode}/find/{$find}/msgId/{$items[user].msgId}">{$items[user].subject}</a>
    </td>
    <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">{$items[user].date|date_format}</td>
		<td class="prio{$items[user].priority}">
		{if $items[user].replyto_hash eq ""}&nbsp;{else}
			<a class="readlink" href="messages/mailbox/index?origto={$items[user].replyto_hash}">
		    <img src="img/icons/arrow_up.png" alt='{tr}find replied message{/tr}' border='0'/>
			</a>
		{/if}
		</td>
    <td  style="text-align:right;{if $items[user].isRead eq 'n'}font-weight:bold;{/if}" class="prio{$items[user].priority}">{$items[user].len}</td>
  </tr>
  {sectionelse}
  <tr><td colspan="6">{tr}No messages to display{/tr}<td></tr>
  {/section}
  </tbody>
</table>

  {* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
  {literal}
  <script language='Javascript' type='text/javascript'>
  <!--
  
	  function switchCheckboxe(tform, elements_id, state) {
	  	// checkboxes need to have the same name elements_id
	  	// e.g. <input type="checkbox" name="my_ename[]">, will arrive as Array in php.
	  	for (var i = 0; i < tform.length; i++) {
	  		if (tform.elements[i].id == elements_id) {
	  			tform.elements[i].checked = state
	  		}
	  	}
	  	return true;
	  }  
	initTbody();
  {/literal}
  //-->                     
  </script>

</form>

<div align="center" class="mini">
{if $prev_offset >= 0}
[<a class="prevnext" href="./messages/mailbox/index/find/{$find}/offset/{$prev_offset}/sort_mode/{$sort_mode}/priority/{$priority}/flag/{$flag}/flagval/{$flagval}">{tr}prev{/tr}</a>]
{/if}
{tr}Page{/tr}: {$actual_page}/{$cant_pages}
{if $next_offset >= 0}
[<a class="prevnext" href="messages/mailbox/index/find/{$find}/offset/{$next_offset}/sort_mode/{$sort_mode}/priority/{$priority}/flag/{$flag}/flagval/{$flagval}">{tr}next{/tr}</a>]
{/if}
</div>

  