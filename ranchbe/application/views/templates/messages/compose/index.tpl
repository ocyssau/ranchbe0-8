<h1><a class="pagetitle" href="messages/compose/index">{tr}Compose message{/tr}</a>
</h1>

{include file="messages/navbar.tpl"}

<br /><br />

{if $feedback}
<div class="simplebox highlight">{$feedback}</div>
{/if}

<form name="composeform" action="./messages/compose/index" method="post">
<table class="normal" >
  <tr>
    <td class="formcolor"><label for="mess-composeto">{tr}To{/tr}:</label></td>
    <td class="formcolor"><input type="text" name="to" id="mess-composeto" value="{$to|escape}" />
    
    <label for="mess-composeto_all">{tr}To all users of ranchbe{/tr}:</label>
    <input type="checkbox" name="to_all" id="mess-composeto_all" value="1" />
    <br />
    <label for="mess-bymessage">{tr}Send a rb message{/tr}:</label>
    <input type="checkbox" name="by_message" id="mess-bymessage" value="1" {$by_message_is_checked} />
    {if $activatemail}
    <br />
    <label for="mess-bymail">{tr}Send a mail{/tr}:</label>
    <input type="checkbox" name="by_mail" id="mess-bymail" value="1" {$by_mail_is_checked} />
    {/if}
    
	<input type="hidden" name="replyto_hash" value="{$replyto_hash}" />

    <button class="mult_submit" type="submit" name="action" value="send" title="{tr}send{/tr}"
    onclick="document.composeform.action='./messages/compose/send';pop_no(composeform);return false;">
    {tr}send{/tr}</button>

		</td>
  </tr>
  <tr>
    <td class="formcolor"><label for="mess-composecc">{tr}CC{/tr}:</label></td><td class="formcolor"><input type="text" name="cc" id="mess-composecc" value="{$cc|escape}" /></td>
  </tr>
  <tr>
    <td class="formcolor"><label for="mess-composebcc">{tr}BCC{/tr}:</label></td><td class="formcolor"><input type="text" name="bcc" id="mess-composebcc" value="{$bcc|escape}" /></td>
  </tr>
  <tr>
    <td class="formcolor"><label for="mess-prio">{tr}Priority{/tr}:</label></td><td class="formcolor">
    <select name="priority" id="mess-prio">
      <option value="1" {if $priority eq 1}selected="selected"{/if}>1 -{tr}Lowest{/tr}-</option>
      <option value="2" {if $priority eq 2}selected="selected"{/if}>2 -{tr}Low{/tr}-</option>
      <option value="3" {if $priority eq 3}selected="selected"{/if}>3 -{tr}Normal{/tr}-</option>
      <option value="4" {if $priority eq 4}selected="selected"{/if}>4 -{tr}High{/tr}-</option>
      <option value="5" {if $priority eq 5}selected="selected"{/if}>5 -{tr}Very High{/tr}-</option>
    </select>
    </td>
  </tr>
  <tr>
    <td class="formcolor"><label for="mess-subj">{tr}Subject{/tr}:</label></td><td class="formcolor"><input type="text" name="subject" id="mess-subj" value="{$subject|escape}" size="80" maxlength="255"/></td>
  </tr>
</table>
<br />

<table class="normal" >
  <tr>
    <td style="text-align: left;" class="formcolor">
      <textarea rows="40" cols="80" name="message_body" id="message_body_id">{$message_body}</textarea>
    </td>
  </tr>
</table>

{*
<script type="text/javascript">
var html_areaEditor = new HTMLArea('message_body_id');
var html_areaConfig = html_areaEditor.config;
html_areaConfig.width = '1000px';
html_areaConfig.height = '400px';
html_areaConfig.imgURL = 'img/htmlarea/';
html_areaConfig.popupURL = 'js/htmlarea/popups/';
html_areaConfig.statusBar = false;
html_areaConfig.hideSomeButtons(' popupeditor ');
html_areaEditor.generate();
</script>
*}
</form>
<br />
