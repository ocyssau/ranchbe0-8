{*Smarty template*}

{include file="header.tpl"}

{*--------------------Search Bar defintion--------------------------*}
{include file='container/_actions.tpl'}

<br />

<h1 class="pagetitle">{tr}{$PageTitle}{/tr}</h1>

<br />
<hr />
<form {$form.attributes}>
{$form.hidden}
{$form.statistics_queries.label}:<br />
{$form.statistics_queries.html}<br />
{$form.reset.html}{$form.submit.html}
</form>

<hr />
<br />

<table class="normal">
<tr><td>
  {section name=graphs loop=$graphs}
  {$graphs[graphs].image}<br />
  {/section}
</td>
<td>
  {section name=text loop=$graphs}
  {$graphs[text].text}<br />
  {/section}
</td></tr>
</table>
