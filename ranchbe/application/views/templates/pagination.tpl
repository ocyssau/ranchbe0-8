{*Smarty template*}

<div class="mini">
<div align="center">

{foreach item=page from=$prev_pages}
  <a class="prevnext" href="{$sameurl}/offset/{math equation="(x * y) - y" x=$page y=$numrows}">{$page}</a>
{/foreach}
...

{if $prev_offset >= 0}
  <a class="prevnext" href="{$sameurl}/offset/{$prev_offset}">{tr}prev{/tr}</a>
{/if}

{tr}Page{/tr}: {$actual_page}
{if $cant_pages == 1}
  {if $next_offset}
    <a class="prevnext" href="{$sameurl}/offset/{$next_offset}">{tr}next{/tr}</a>
  {/if}
{/if}

</div>
</div>
