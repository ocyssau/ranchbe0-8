{*Smarty template*}

<h1>{tr}Admin process activities{/tr} ({$proc_info.name}:{$proc_info.version})</h1>

{include file=galaxia/admin/_actions.tpl}
<br />

{if count($errors) > 0}
<div class="wikitext">
Errors:<br />
{section name=ix loop=$errors}
<small>{$errors[ix]}</small><br />
{/section}
</div>
{/if}

<table>
    <tr>
      <td><center>

{if $proc_info.graph}
  <table class="normal">
    <tr>
      <td>
        <center>
          {if $proc_info.map}
            {rbhtml_image file=$proc_info.graph alt=$proc_info.name border=0 map=$proc_info.map}
          {else}
            {rbhtml_image file=$proc_info.graph alt=$proc_info.name border=0}
          {/if}
        </center>
      </td>
    </tr>
  </table>
{else}
  {tr}No process graph is available. Either the process still contains errors, the graph is not generated yet, or <a href="http://www.research.att.com/sw/tools/graphviz/">GraphViz</a> is not properly installed.{/tr}
{/if}

      </center></td>
      <td><center>





<h2>{tr}Add or edit an activity{/tr} 
<a class="link" href="galaxia/activities/index/pid/{$pid}/activityId/0">{tr}new{/tr}</a></h2>

<ul>
<li>
Interactive actvities require some kind of user interaction, normally a form is
presented to the user and the user can fill/change values submitting the form as
many times as needed until the activity decides that the activity is completed.
Else activites don�t require user interaction and are excuted by the workflow
engine.
</li>
<li>
Auto-executed activity is directly executed when instance is sent to. So execution is not
submit by a user request. Use on join, end or split activities for example.
</li>
<li>
If commented, a form will be displayed to input a comment for this activities.
</li>

</ul>


<form action="galaxia/activities/saveact" method="post">
<input type="hidden" name="pid" value="{$pid|escape}" />
<input type="hidden" name="activityId" value="{$act_info.activityId|escape}" />

<table class="normal">
<tr>
  <td class="formcolor">{tr}name{/tr}</td>
  <td class="formcolor"><input type="text" name="name" value="{$act_info.name|escape}" /></td>
</tr>
<tr>
  <td class="formcolor">{tr}description{/tr}</td>
  <td class="formcolor"><textarea name="description" rows="4" cols="60">{$act_info.description|escape}</textarea></td>
</tr>
<tr>  
  <td class="formcolor">{tr}type{/tr}</td>
  <td class="formcolor">
  <select name="type">
  <option value="start" {if $act_info.type eq 'start'}selected="selected"{/if}>{tr}start{/tr}</option>
  <option value="end" {if $act_info.type eq 'end'}selected="selected"{/if}>{tr}end{/tr}</option>		  
  <option value="activity" {if $act_info.type eq 'activity'}selected="selected"{/if}>{tr}activity{/tr}</option>		  
  <option value="switch" {if $act_info.type eq 'switch'}selected="selected"{/if}>{tr}switch{/tr}</option>		  
  <option value="split" {if $act_info.type eq 'split'}selected="selected"{/if}>{tr}split{/tr}</option>		  
  <option value="join" {if $act_info.type eq 'join'}selected="selected"{/if}>{tr}join{/tr}</option>		  
  <option value="standalone" {if $act_info.type eq 'standalone'}selected="selected"{/if}>{tr}standalone{/tr}</option>		  
  </select>
  <input type="checkbox" name="isInteractive" {if $act_info.isInteractive eq 'y'}checked="checked"{/if} />
  {tr}interactive{/tr} |
  <!--
  <input type="checkbox" name="isAutoRouted" {if $act_info.isAutoRouted eq 'y'}checked="checked"{/if} />
  {tr}auto routed{/tr} |
  -->
  <input type="checkbox" name="isAutomatic" {if $act_info.isAutomatic eq 'y'}checked="checked"{/if} />
  {tr}auto executed{/tr} |
  <input type="checkbox" name="isComment" {if $act_info.isComment eq 'y'}checked="checked"{/if} />
  {tr}commented{/tr}
  </td>
</tr>
<tr>
  <td class="formcolor">{tr}Expiration Time{/tr} </td>
  <td class="formcolor">
  {tr}Years{/tr}:
  <SELECT name="year" size ="1">
  	{html_options options=$years selected=$act_info.year}
  </SELECT>
  {tr}Months{/tr}:
  <SELECT name="month" size="1">
  	{html_options options=$months selected=$act_info.month}
  </SELECT>
  {tr}Days{/tr}:
  <SELECT name="day" size="1">
  	{html_options options=$days selected=$act_info.day}
  </SELECT>
  {tr}Hours{/tr}:
  <SELECT name="hour" size="1">
  	{html_options options=$hours selected=$act_info.hour}
  </SELECT>
  {tr}Minutes{/tr}:
  <SELECT name="minute" size="1">
  	{html_options options=$minutes selected=$act_info.minute}
  </SELECT>
  </td>
</tr>
<tr>
  <td class="formcolor">{tr}Add transitions{/tr}</td>
  <td class="formcolor">
    <table class="normal">
		<tr>
			<td class="formcolor">
				{tr}Add transition from:{/tr}<br />
				<select name="add_tran_from[]" multiple="multiple" size="5">
				{section name=ix loop=$items}
				<option value="{$items[ix].activityId|escape}" {if $items[ix].from eq 'y'}selected="selected"{/if}>{$items[ix].name|adjust:30}</option>
				{/section}			
				</select>
			</td>
			<td class="formcolor">
				{tr}Add transition to:{/tr}<br />
				<select name="add_tran_to[]" multiple="multiple" size="5">
				{section name=ix loop=$items}
				<option value="{$items[ix].activityId|escape}" {if $items[ix].to eq 'y'}selected="selected"{/if}>{$items[ix].name|adjust:30}</option>
				{/section}			
				</select>
			</td>
		</tr>    
    </table>
  </td>
</tr>

<tr>
  <td class="formcolor">{tr}roles{/tr}</td>
  <td class="formcolor">
  {section name=ix loop=$roles}
  {$roles[ix].name}[<a class="link" href="galaxia/activities/removerole/activityId/{$act_info.activityId}/pid/{$pid}/role_id/{$roles[ix].roleId}">x</a>]<br />
  {sectionelse}
  {tr}No roles associated to this activity{/tr}
  {/section}
  </td>
</tr>
<tr>
  <td class="formcolor">{tr}Add role{/tr}</td>
  <td class="formcolor">
  {if count($all_roles)}
  <select name="userole">
  <option value="">{tr}add new{/tr}</option>
  {section loop=$all_roles name=ix}
  <option value="{$all_roles[ix].roleId|escape}">{$all_roles[ix].name}</option>
  {/section}
  </select>
  {/if}
  <input type="text" name="rolename" />
  </td>
</tr>
<tr>
  <td class="formcolor">&nbsp;</td>
  <td class="formcolor"><input type="submit" name="save_act" value="{tr}save{/tr}" /> </td>
</tr>

</table>
</form>





<h2>{tr}Process Transitions{/tr}</h2>
<table class="normal">
<tr>
	<td >
		<h3>{tr}List of transitions{/tr}</h3>

      <!--{*
			<form action="galaxia/activities/index" method="post" id='filtran'>
			<input type="hidden" name="pid" value="{$pid|escape}" />
			<input type="hidden" name="activityId" value="{$act_info.activityId|escape}" />
			{tr}From:{/tr}<select name="filter_tran_name" onchange="javascript:document.getElementById('filtran').submit();">
			<option value="" {if $filter_tran_name eq ''}selected="selected"{/if}>{tr}all{/tr}</option>
			{section name=ix loop=$items}
			<option value="{$items[ix].activityId|escape}" {if $filter_tran_name eq $items[ix].activityId}selected="selected"{/if}>{$items[ix].name}</option>
			{/section}
			</select>
			</form>
			*}-->
			
			<form action="galaxia/activities/deletetrans" method="post">
			<input type="hidden" name="pid" value="{$pid|escape}" />
			<input type="hidden" name="activityId" value="{$act_info.activityId|escape}" />
			<table class="normal">
			<tr>
  			<td class="heading"><input type="submit" name="delete_tran" value="{tr}x{/tr} " /></td>
  			<td class="heading"><a class="tableheading">{tr}Origin{/tr}</td>
			</tr>
			{cycle values="odd,even" print=false}
			{section name=ix loop=$transitions}
			<tr>
				<td class="{cycle advance=false}">
					<input type="checkbox" name="transition[{$transitions[ix].actFromId}_{$transitions[ix].actToId}]" />
				</td>
				<td class="{cycle advance=false}">
					<a class="link" href="galaxia/activities/index/pid/{$pid}/activityId/{$transitions[ix].actFromId}">{$transitions[ix].actFromName}</a>
					<img src='./img/galaxia/icons/next.gif' alt='to' />
					<a class="link" href="galaxia/activities/index/pid/{$pid}/activityId/{$transitions[ix].actToId}">{$transitions[ix].actToName}</a>
				</td>
			</tr>
			{sectionelse}
			<tr>
				<td class="{cycle advance=false}" colspan="3">
				{tr}No transitions defined yet{/tr}
				</td>
			</tr>
			{/section}
			</table>
			</form>		
	</td>

	<td class="formcolor" >
		<h3>{tr}Add a transition{/tr}</h3>
		<form action="galaxia/activities/addtrans" method="post">
		<input type="hidden" name="pid" value="{$pid|escape}" />
		<input type="hidden" name="activityId" value="{$act_info.activityId|escape}" />
		<table class="normal">
		<tr>
		  <td class="formcolor">
		  From:
		  </td>
		  <td>
		  <select name="actFromId">
		  {section name=ix loop=$items}
		  <option value="{$items[ix].activityId|escape}">{$items[ix].name}</option>
		  {/section}
		  </select>
		  </td>
		</tr>
		<tr>
		  <td class="formcolor">
		  To: 
		  </td>
		  <td>
		   <select name="actToId">
		  {section name=ix loop=$items}
		  <option value="{$items[ix].activityId|escape}">{$items[ix].name}</option>
		  {/section}
		  </select>
		  </td>
		</tr>
		<tr>
		  <td class="formcolor">&nbsp;</td>
		  <td class="formcolor">
		    <input type="submit" name="add_trans" value="{tr}add{/tr}" />
		  </td>
		</tr>
		</table>	
		</form>
	</td>
</tr>
</table>	






      </center></td>
    </tr>
</table>





<h2>{tr}Process activities{/tr}</h2>
	
<form name="listactivities" action="galaxia/activities/updateact" method="post">
<input type="hidden" name="pid" value="{$pid|escape}" />
<input type="hidden" name="activityId" value="{$act_info.activityId|escape}" />
<table class="normal">
<tr>

<!--suppress activity-->
<td style="text-align:center;" class="heading">
  <button class="mult_submit" type="submit" name="deleteact" value="deleteact" title="{tr}Suppress{/tr}" 
   onclick="if(confirm('{tr}Do you want really suppress this activities{/tr}'))
   {ldelim}document.listactivities.action='./galaxia/activities/deleteact';
    pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
  <img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
  </button>
</td>

<td  class="heading" >{tr}#{/tr}</td>
<td  class="heading" >{tr}Name{/tr}</td>
<td  class="heading" >{tr}Type{/tr}</td>
<td  class="heading" >{tr}inter{/tr}</td>
<td  class="heading" >{tr}route{/tr}</td>
<td  class="heading" >{tr}auto{/tr}</td>
<td  class="heading" >{tr}comment{/tr}</td>
<td  class="heading" >{tr}Action{/tr}</td>
</tr>
{cycle values="odd,even" print=false}
{section name=ix loop=$items}
<tr>
	<td style="text-align:center;" class="{cycle advance=false}">
		<input type="checkbox" name="activity[{$items[ix].activityId}]" value="{$items[ix].activityId}" />
	</td>
	<td style="text-align:right;" class="{cycle advance=false}">
	  {$items[ix].flowNum}
	</td>

	<td class="{cycle advance=false}">
	  <a class="link" href="galaxia/activities/index/pid/{$pid}/activityId/{$items[ix].activityId}">{$items[ix].name}</a>
	  {if $items[ix].roles < 1}
		<small>{tr}(no roles){/tr}</small>
	  {/if}
	</td>
	<td style="text-align:center;" class="{cycle advance=false}">
	  {$items[ix].type|act_icon:$items[ix].isInteractive}
	</td>
	<td style="text-align:center;" class="{cycle advance=false}">
	  <input type="checkbox" name="isInteractive[{$items[ix].activityId}]" value="1"
    {if $items[ix].isInteractive eq 'y'}checked="checked"{/if} />
	</td>
    <td style="text-align:center;" class="{cycle advance=false}">
	  <input type="checkbox" name="isAutoRouted[{$items[ix].activityId}]" value="1"
    {if $items[ix].isAutoRouted eq 'y'}checked="checked"{/if} />
	</td>
    <td style="text-align:center;" class="{cycle advance=false}">
	  <input type="checkbox" name="isAutomatic[{$items[ix].activityId}]" value="1"
    {if $items[ix].isAutomatic eq 'y'}checked="checked"{/if} />
	</td>
    <td style="text-align:center;" class="{cycle advance=false}">
	  <input type="checkbox" name="isComment[{$items[ix].activityId}]" value="1"
    {if $items[ix].isComment eq 'y'}checked="checked"{/if} />
	</td>

	<td class="{cycle}">
	<a class="link" href="galaxia/sharedsource/getcode/pid/{$pid}/activityId/{$items[ix].activityId}">{tr}code{/tr}</a>
	{if $items[ix].isInteractive eq 'y'}
	<br /><a class="link" href="galaxia/sharedsource/gettemplate/pid/{$pid}/activityId/{$items[ix].activityId}">{tr}template{/tr}</a>
	{/if}
	</td>
</tr>
<input type="hidden" name="items[]" value="{$items[ix].activityId}" />
{sectionelse}
<tr>
	<td class="{cycle advance=false}" colspan="6">
	{tr}No activities defined yet{/tr}
	</td>
</tr>	
{/section}
<tr>
<td class="heading" colspan="9" style="text-align:center;">
<input type="submit" name="updateact" value="{tr}update{/tr}" />
</td>
</tr>
</table>
</form>	


