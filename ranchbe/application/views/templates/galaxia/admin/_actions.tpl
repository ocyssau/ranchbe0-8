{*Smarty template*}

<div id="BoiteActions">
  <span class="button2">
  	{if $proc_info.isValid eq 'y'}
  		<img border='0' 
           src='img/galaxia/icons/green_dot.gif'
           title='{tr}valid{/tr}' />
  	{else}
  		<img border='0'
           src='img/galaxia/icons/red_dot.gif'
           title='{tr}invalid{/tr}' />
  	{/if}
  </span>

  <span class="button2">
		{if $proc_info.isActive eq 'y'}
    <a href="galaxia/activities/deactivateproc/pid/{$pid}" class="linkbut">
    <img border="0" src="img/galaxia/icons/stop.gif"
                    title="{tr}stop{/tr}"/>
    {tr}stop{/tr}</a>
		{else}
		<a class="link" href="galaxia/activities/activateproc/pid/{$pid}">
    <img border='0' src='./img/galaxia/icons/refresh2.gif' 
                    title='{tr}activate{/tr}' />
    {tr}activate{/tr}</a>
		{/if}
  </span>

  <span class="button2">
    <a href="galaxia/processes/index/pid/{$pid}" class="linkbut">
    <img border="0" src="img/galaxia/icons/Process.gif"
                    title="{tr}processes{/tr}"/>
    {tr}processes{/tr}</a>
  </span>

  <span class="button2">
    <a href="galaxia/activities/index/pid/{$pid}" class="linkbut">
    <img border="0" src="img/galaxia/icons/Activity.gif"
                    title="{tr}activities{/tr}"/>
    {tr}activities{/tr}</a>
  </span>

  <span class="button2">
    <a href="galaxia/sharedsource/index/pid/{$pid}" class="linkbut">
    <img border="0" src="img/galaxia/icons/book.gif"
                    title="{tr}code{/tr}"/>
    {tr}code{/tr}</a>
  </span>

  <span class="button2">
    <a href="galaxia/graph/index/pid/{$pid}" class="linkbut">
    <img border="0" src="img/galaxia/icons/mode_tree.gif"
                    title="{tr}graph{/tr}"/>
    {tr}graph{/tr}</a>
  </span>

  <span class="button2">
    <a href="galaxia/roles/index/pid/{$pid}" class="linkbut">
    <img border="0" src="img/galaxia/icons/myinfo.gif"
                    title="{tr}roles{/tr}"/>
    {tr}roles{/tr}</a>
  </span>

  <span class="button2">
    <a href="galaxia/processes/index/pid/{$pid}" class="linkbut">
    <img border="0" src="img/galaxia/icons/change.gif"
                    title="{tr}edit{/tr}"/>
    {tr}edit{/tr}</a>
  </span>

  <span class="button2">
    <a href="galaxia/processes/export/pid/{$pid}" class="linkbut">
    <img border="0" src="img/galaxia/icons/export.gif"
                    title="{tr}export{/tr}"/>
    {tr}export{/tr}</a>
  </span>

</div>
