<table cellpadding='0' cellspacing='0'>
<tr>
 <td>
  <div style="height:20px; padding-top:2px; padding-bottom:2px;border:1px solid black; background-color:white; color:black; font-size:10px;">
    <table  cellpadding='0' cellspacing='0'>
      <tr>
  	   <td><b>{$proc_info.name}:{$proc_info.version}</b>
      	{if $proc_info.isValid eq 'y'}
      		<img border='0' src='./img/galaxia/icons/green_dot.gif' alt='{tr}valid{/tr}' title='{tr}valid{/tr}' />	
      	{else}
      		<img border='0' src='./img/galaxia/icons/red_dot.gif' alt='{tr}invalid{/tr}' title='{tr}invalid{/tr}' />
      	{/if}
      	</td>
      	<td align='right'>
      		<table cellpadding='0' cellspacing='2'>
        		<tr>
        			{if $proc_info.isActive eq 'y'}
        			<td><a class="link" href="galaxia/activities/deactivateproc/pid/{$pid}"><img border='0' src='./img/galaxia/icons/stop.gif' alt='{tr}stop{/tr}' title='{tr}stop{/tr}' /></a></td>
        			{else}
        			{if $proc_info.isValid eq 'y'}
        			<td><a class="link" href="galaxia/activities/activateproc/pid/{$pid}"><img border='0' src='./img/galaxia/icons/refresh2.gif' alt='{tr}activate{/tr}' title='{tr}activate{/tr}' /></a></td>
        			{/if}
        			{/if}
        			<td><a class="link" href="galaxia/processes/index/pid/{$pid}">
              <img border='0' src='./img/galaxia/icons/Process.gif'
              alt='{tr}processes{/tr}'
              title='{tr}processes{/tr}' /></a></td>

        			<td><a class="link" href="galaxia/activities/index/pid/{$pid}">
              <img border='0' src='./img/galaxia/icons/Activity.gif'
              alt='{tr}activities{/tr}'
              title='{tr}activities{/tr}' /></a></td>

        			<td><a class="link" href="galaxia/sharedsource/index/pid/{$pid}">
              <img border='0' src='./img/galaxia/icons/book.gif'
              alt='{tr}code{/tr}' title='{tr}code{/tr}' /></a></td>

        			<td><a class="link" href="galaxia/graph/index/pid/{$pid}">
              <img border='0' src='./img/galaxia/icons/mode_tree.gif'
              alt='{tr}graph{/tr} 'title='{tr}graph{/tr}' /></a></td>

        			<td><a class="link" href="galaxia/roles/index/pid/{$pid}">
              <img border='0' src='./img/galaxia/icons/myinfo.gif'
              alt='{tr}roles{/tr}' title='{tr}roles{/tr}' /></a></td>

        			<td><a class="link" href="galaxia/processes/index/pid/{$pid}">
              <img border='0' src='./img/galaxia/icons/change.gif'
              alt='{tr}edit{/tr}' title='{tr}edit{/tr}' /></a></td>

        			<td><a class="link" href="galaxia/processes/export/pid/{$pid}">
              <img border='0' src='./img/galaxia/icons/export.gif'
              alt='{tr}export{/tr}' title='{tr}export{/tr}' /></a></td>
            </tr>
  		    </table>
       </td>	
      </tr>
    </table>
    </div>
 </td>

</tr>
</table>
