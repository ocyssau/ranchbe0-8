{*Smarty template*}

<h1>{tr}Process Graph{/tr} ({$proc_info.name}:{$proc_info.version})</h1>

{include file=galaxia/admin/_actions.tpl}
<br />

{if count($errors)}
<div class="wikitext">
 {tr}This process is invalid{/tr}:<br />
 {section name=ix loop=$errors}
  <small>{$errors[ix]}</small><br />
 {/section}
</div>
{/if}

{if $proc_info.graph}
  <table class="normal">
    <tr>
      <td>
        <center>
          {if $proc_info.map}
            {rbhtml_image file=$proc_info.graph alt=$proc_info.name border=0 map=$proc_info.map}
          {else}
            {rbhtml_image file=$proc_info.graph alt=$proc_info.name border=0}
          {/if}
        </center>
      </td>
    </tr>
  </table>
{else}
  {tr}No process graph is available. Either the process still contains errors, the graph is not generated yet, or <a href="http://www.research.att.com/sw/tools/graphviz/">GraphViz</a> is not properly installed.{/tr}
{/if}
<br /><br />

