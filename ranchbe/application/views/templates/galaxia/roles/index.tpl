{*Smarty template*}

<h1>{tr}Admin process roles{/tr} ({$proc_info.name}:{$proc_info.version})</h1>

{include file=galaxia/admin/_actions.tpl}

{if count($errors) > 0}
<div class="wikitext">
Errors:<br />
{section name=ix loop=$errors}
<small>{$errors[ix]}</small><br />
{/section}
</div>
{/if}

<h2>{tr}Add or edit a role{/tr} <a class="link" href="./galaxia/roles/index/pid/{$pid}">{tr}new{/tr}</a></h2>
<form action="./galaxia/roles/save" method="post">
<input type="hidden" name="pid" value="{$pid|escape}" />
<input type="hidden" name="roleId" value="{$info.roleId|escape}" />
<table class="normal">
<tr>
  <td class="formcolor">{tr}name{/tr}</td>
  <td class="formcolor"><input type="text" name="name" value="{$info.name|escape}" /></td>
</tr>
<tr>
  <td class="formcolor">{tr}description{/tr}</td>
  <td class="formcolor"><textarea name="description" rows="4" cols="60">{$info.description|escape}</textarea></td>
</tr>
<tr>
  <td class="formcolor">&nbsp;</td>
  <td class="formcolor"><input type="submit" name="save" value="{tr}save{/tr}" /> </td>
</tr>
</table>
</form>



<h2>{tr}Process roles{/tr}</h2>

<form action="./galaxia/roles/delete" method="post">
<input type="hidden" name="pid" value="{$pid|escape}" />
<input type="hidden" name="roleId" value="{$info.roleId|escape}" />
<table class="normal">
<tr>
<td  class="heading"><input type="submit" name="delete" value="{tr}x{/tr} " /></td>
<td  class="heading" >{column_header sort_field='name' sort_order='INV'}</td>
<td  class="heading" >{column_header sort_field='description' sort_order='INV'}</td>
</tr>

{cycle values="odd,even" print=false}
{section name=ix loop=$items}
<tr>
	<td class="{cycle advance=false}">
		<input type="checkbox" name="role[{$items[ix].roleId}]" value="{$items[ix].roleId}" />
	</td>
	<td class="{cycle advance=false}">
	  <a class="link" href="galaxia/roles/index/pid/{$pid}/roleId/{$items[ix].roleId}">{$items[ix].name}</a>
	</td>
	<td class="{cycle}">
	  {$items[ix].description}
	</td>
</tr>
{sectionelse}
<tr>
	<td class="{cycle advance=false}" colspan="3">
	{tr}No roles defined yet{/tr}
	</td>
</tr>	
{/section}
</table>
</form>	


{* MAP USERS *}
{if count($roles) > 0}
	<h2>{tr}Map users to roles{/tr}</h2>
	<form method="post" action="./galaxia/roles/savemap">
	<input type="hidden" name="pid" value="{$pid|escape}" />
	<table class="normal">
	<tr>
		<td class="formcolor">{tr}Map{/tr}</td>
		<td class="formcolor">
		  <table border="1" >
		  	<tr>
		  		<td class="formcolor" >
		  		{tr}Users{/tr}:
				<input type="text" size="10" name="find_users" value="{$find_users|escape}" />
				<input type="submit" name="findusers" value="{tr}filter{/tr}" />
		  		</td>
		  		<td class="formcolor" >
	  			{tr}Roles{/tr}:<br />		  		
		  		</td>
		  	</tr>
		  	<tr>
		  		<td class="formcolor" >
					<select name="user[]" multiple="multiple" size="10">
					{*<option value="Anonymous">"{tr}Anonymous{/tr}"</option>*}
					{foreach key=id item=user from=$users}
					<option value="{$user.handle|escape}">{$user.handle|adjust:30}</option>
					{/foreach}
					</select>
		  		</td>
		  		<td class="formcolor" >

					<select name="role[]" multiple="multiple" size="10">
					{section name=ix loop=$roles}
					<option value="{$roles[ix].roleId|escape}">{$roles[ix].name|adjust:30}</option>
					{/section}
					</select>	  		
		  		</td>
		  	</tr>
		  </table>
		</td>
	</tr>
	
	<tr>
		<td class="formcolor">&nbsp;</td>
		<td style="text-align:center;" class="formcolor">
			<input type="submit" name="save_map" value="{tr}map{/tr}" />
		</td>
	</tr>
	</table>
	</form>
	




	{* MAP GROUPS *}
	<h2>{tr}Map groups to roles{/tr}</h2>
	<form method="post" action="./galaxia/roles/mapg">
	<input type="hidden" name="pid" value="{$pid|escape}" />
	<table class="normal">
	<tr>
		<td class="formcolor">
		{tr}Operation{/tr}
		</td>
		<td class="formcolor">
		{tr}Group{/tr}
		</td>
		<td class="formcolor">
		{tr}Role{/tr}
		</td>
		<td class="formcolor">
		&nbsp;
		</td>
	</tr>
	<tr>
		<td class="formcolor">
			<select name="op">
			<option value="add">{tr}add{/tr}</option>
			<option value="remove">{tr}remove{/tr}</option>
			</select>
		</td>

		<td class="formcolor">
			<select name="group">
			{foreach item=group key=group_id from=$groups}
        <option value="{$group_id|escape}">{$group.group_define_name}</option>
			{/foreach}
			</select>
		</td>

		<td class="formcolor">
			<select name="role">
			{section name=ix loop=$roles}
			<option value="{$roles[ix].roleId|escape}">{$roles[ix].name|adjust:30}</option>
			{/section}
			</select>	  		
		</td>
		<td class="formcolor">
			<input type="submit" name="mapg" value="{tr}go{/tr}" />
		</td>

	</tr>
	</table>
	</form>

{else}
	<h2>{tr}Warning{/tr}</h2>
	{tr}No roles are defined yet so no roles can be mapped{/tr}<br />
{/if}




<h2>{tr}List of mappings{/tr}</h2>
<form action="./galaxia/roles/index" method="post">
<input type="hidden" name="pid" value="{$pid|escape}" />
{tr}Find{/tr}:<input size="8" type="text" name="find" value="{$find|escape}" />
<input type="submit" name="filter" value="{tr}find{/tr}" />
</form>

<form action="./galaxia/roles/deletemap" method="post">
<input type="hidden" name="pid" value="{$pid|escape}" />

<table class="normal">
<tr>
<td  class="heading"><input type="submit" name="delete_map" value="{tr}x{/tr} " /></td>
<td  class="heading" ><a class="tableheading" href="galaxia_admin_roles.php?pid={$pid}&amp;find={$find}&amp;offset={$offset}&amp;sort_mode={if $sort_mode eq 'name_desc'}name_asc{else}name_desc{/if}">{tr}Role{/tr}</a></td>
<td  class="heading" ><a class="tableheading" href="galaxia_admin_roles.php?pid={$pid}&amp;find={$find}&amp;offset={$offset}&amp;sort_mode={if $sort_mode eq 'user_desc'}user_asc{else}user_desc{/if}">{tr}User{/tr}</a></td>
</tr>
{cycle values="odd,even" print=false}
{section name=ix loop=$mapitems}
<tr>
	<td class="{cycle advance=false}">
		<input type="checkbox" name="map[{$mapitems[ix].user}:::{$mapitems[ix].roleId}]" />
	</td>
	<td class="{cycle advance=false}">
	  {$mapitems[ix].name}
	</td>
	<td class="{cycle}">
	  {if $mapitems[ix].user eq ''}
	  "{tr}Anonymous{/tr}"
	  {else}
	  {$mapitems[ix].user}
	  {/if}
	</td>
</tr>
{sectionelse}
<tr>
	<td class="{cycle advance=false}" colspan="3">
	{tr}No mappings defined yet{/tr}
	</td>
</tr>	
{/section}
</table>
</form>

