{*Smarty template*}

{*******************************************************************************}
{*form to create a process*}
<h1>{tr}Add or edit a process{/tr}</h1>

{if $pid > 0}
  {include file=galaxia/admin/_actions.tpl}
  <br />
{/if}

{if $pid > 0 and count($errors)}
<div class="wikitext">
{tr}This process is invalid{/tr}:<br />
{section name=ix loop=$errors}
<small>{$errors[ix]}</small><br />
{/section}
</div>
{/if}

<form name="createproc" action="./galaxia/processes/save" method="post">
<input type="hidden" name="version" value="{$info.version|escape}" />
<input type="hidden" name="pid" value="{$info.pId|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="where" value="{$where|escape}" />
<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
<table class="normal">
	<tr>
		<td class="formcolor">{tr}Process Name{/tr}</td>
		<td class="formcolor"><input type="text" maxlength="80" name="name" value="{$info.name|escape}" /> {tr}ver:{/tr}{$info.version}</td>
	</tr>
	<tr>
	 	<td class="formcolor">{tr}Description{/tr}</td>
	 	<td class="formcolor"><textarea rows="5" cols="60" name="description">{$info.description|escape}</textarea></td>
	</tr>
	<tr>
		<td class="formcolor"><a {popup text="$is_active_help"}>{tr}is active?{/tr}</a></td>
		<td class="formcolor"><input type="checkbox" name="isActive" {if $info.isActive eq 'y'}checked="checked"{/if} /></td>
	</tr>
	<tr>
		<td class="formcolor">&nbsp;</td>
		<td class="formcolor">
    <input type="submit" name="save" value="{if $pid > 0}{tr}update{/tr}{else}{tr}create{/tr}{/if}" />
    </td>
	</tr>
</table>
</form>



{*******************************************************************************}
{*form to upload a process*}

<h2>{tr}Or upload a process using this form{/tr}</h2>
<form enctype="multipart/form-data" action="./galaxia/processes/import" method="post">
<table class="normal">
<tr>
  <td class="formcolor">{tr}Upload file{/tr}:</td>
  <td class="formcolor">
      <input type="hidden" name="MAX_FILE_SIZE" value="10000000000000" />
      <input size="16" name="userfile1" type="file" />
      <input style="font-size:9px;" type="submit" name="upload" value="{tr}upload{/tr}" />
  </td>
</tr>
</table>
</form>








<h2>{tr}List of processes{/tr} ({$cant})</h2>

{*******************************************************************************}
{*list*}
<form name="checkform" action="./galaxiaAdmin/processes/index" method="post">
  <input type="hidden" name="offset" value="{$offset|escape}" />
  <input type="hidden" name="find" value="{$find|escape}" />
  <input type="hidden" name="where" value="{$where|escape}" />
  <input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
    <table class="normal">
      <tr>
        <td style="text-align:center;"  class="heading">
        <button class="mult_submit" type="submit" name="delete" value="x" title="{tr}Delete{/tr}"
         onclick="document.checkform.action='./galaxia/processes/delete'; pop_no(checkform)">
        x
        </button>

        </td>
        <td  class="heading" >{column_header sort_field='name' sort_order='INV'}</td>
        <td  class="heading" >{column_header sort_field='version' sort_order='INV'}</td>
        <td  class="heading" >{column_header sort_field='isActive' sort_order='INV'}</td>
        <td  class="heading" >{column_header sort_field='isValid' sort_order='INV'}</td>
        <td  class="heading" >{tr}Action{/tr}</td>
      </tr>

      {cycle values="odd,even" print=false}
      {section name=ix loop=$items}
      <tr>
      	<td style="text-align:center;" class="{cycle advance=false}">
      		{if $items[ix].isActive eq 'y'}
           *
      		{else}
      		 <input type="checkbox" name="process[{$items[ix].pId}]" value="{$items[ix].pId}" />
      		{/if}
      	</td>
      	<td class="{cycle advance=false}">
      	  <a class="link" href="galaxia/processes/index/pid/{$items[ix].pId}">{$items[ix].name}</a>
      	</td>
      	<td style="text-align:right;" class="{cycle advance=false}">
      	  {$items[ix].version}
      	</td>
      		<td class="{cycle advance=false}" style="text-align:center;">
      	  {if $items[ix].isActive eq 'y'}
      	  <img src='./img/galaxia/icons/refresh2.gif' alt=' ({tr}active{/tr}) ' title='{tr}active process{/tr}' />
      	  {else}
      	  &nbsp;
      	  {/if}
      	</td>
      	<td class="{cycle advance=false}" style="text-align:center;">
      	  {if $items[ix].isValid eq 'n'}
      	  <img src='./img/galaxia/icons/red_dot.gif' alt=' ({tr}invalid{/tr}) ' title='{tr}invalid process{/tr}' />
      	  {else}
      	  <img src='./img/galaxia/icons/green_dot.gif' alt=' ({tr}valid{/tr}) ' title='{tr}valid process{/tr}' />
      	  {/if}
      	</td>
      
      	<td class="{cycle}">
      	  <a class="link" href="galaxia/activities/index/pid/{$items[ix].pId}">{tr}activities{/tr}</a><br />
      	  <a class="link" href="galaxia/sharedsource/index/pid/{$items[ix].pId}">{tr}code{/tr}</a><br />
      	  <a class="link" href="galaxia/graph/index/pid/{$items[ix].pId}">{tr}graph{/tr}</a><br />
      	  <a class="link" href="galaxia/roles/index/pid/{$items[ix].pId}">{tr}roles{/tr}</a><br />
      	  <a class="link" href="galaxia/processes/export/pid/{$items[ix].pId}">{tr}export{/tr}</a><br />
      	  <a class="link" href="galaxia/processes/newminor/pid/{$items[ix].pId}">{tr}new minor{/tr}</a><br />
      	  <a class="link" href="galaxia/processes/newmajor/pid/{$items[ix].pId}">{tr}new major{/tr}</a><br />
      	</td>
      </tr>
      {sectionelse}
      <tr>
      	<td class="{cycle advance=false}" colspan="15">
      	{tr}No processes defined yet{/tr}
      	</td>
      </tr>	
      {/section}
  </table>
</form>


{if $cant > 0}
<div class="wikitext">
*Note: It is not possible to delete an active process. To delete a process, deactivate it first.
</div>
{/if}

{*******************************************************************************}
{*pagination*}
