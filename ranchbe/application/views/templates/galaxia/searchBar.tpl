{*Smarty template*}

{*******************************************************************************}
{*filters*}
<form action="./galaxia/processes/index" method="post">
    <input type="hidden" name="offset" value="{$offset|escape}" />
    <input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
    
    {tr}Find{/tr}:
    <input size="8" type="text" name="find" value="{$find|escape}" />
    
    {tr}Process{/tr}:
    <select name="filter_name">
     <option value="">{tr}All{/tr}</option>
      {section loop=$all_procs name=ix}
        <option  value="{$all_procs[ix].name|escape}">{$all_procs[ix].name}</option>
      {/section}
    </select>
    
    {tr}Status{/tr}:
    <select name="filter_active">
     <option value="">{tr}All{/tr}</option>
     <option value="y">{tr}Active{/tr}</option>
     <option value="n">{tr}Inactive{/tr}</option>
    </select>
    
    <input type="submit" name="filter" value="{tr}filter{/tr}" />
</form>
