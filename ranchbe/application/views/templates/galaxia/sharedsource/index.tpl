{*Smarty template*}

<h1>{tr}Admin process sources{/tr}({$proc_info.name}:{$proc_info.version})</h1>

{include file=galaxia/admin/_actions.tpl}
<br />

{if count($errors) > 0}
<div class="wikitext">
Errors:<br />
{section name=ix loop=$errors}
<small>{$errors[ix]}</small><br />
{/section}
</div>
{/if}

<form name="editsource" id='editsource' action="galaxia/sharedsource/index" method="post">
<input type="hidden" name="pid" value="{$pid|escape}" />
<input type="hidden" name="source_file" value="{$source_file|escape}" />
<input type="hidden" id="source_type" name="source_type" value="{$source_type|escape}" />
<input type="hidden" name="template" value="{$template|escape}" />

<table class="normal">
<tr>
  <td class="formcolor">{tr}select source{/tr}</td>
  <td class="formcolor">
		<select name="activityId" onchange="document.getElementById('editsource').submit();">
		<option value="" {if $activityId eq 0}selected="selected"{/if}>{tr}Shared code{/tr}</option>
		{section loop=$items name=ix}
		<option value="{$items[ix].activityId|escape}" {if $activityId eq $items[ix].activityId}selected="selected"{/if}>{$items[ix].name}</option>
		{/section}
		</select>
  </td>

  <td class="formcolor">
    {if $activityId > 0 and $act_info.isInteractive eq 'y' and $template eq 'n'}
    <button class="mult_submit" type="submit" name="template" value="template" title="{tr}template{/tr}" id="08"
     onclick="document.editsource.action='./galaxia/sharedsource/index/template/y'; pop_no(editsource)">
    {tr}template{/tr}</button>
    {/if}

    {if $activityId > 0 and $act_info.isInteractive eq 'y' and $template eq 'y'}
      <button class="mult_submit" type="submit" name="code" value="code" title="{tr}template{/tr}" id="08"
       onclick="document.editsource.action='./galaxia/sharedsource/index/template/n'; pop_no(editsource)">
      {tr}code{/tr}</button>

      <button class="mult_submit" type="submit" name="save" value="save" title="{tr}save{/tr}" id="09"
       onclick="document.editsource.action='./galaxia/sharedsource/save'; pop_no(editsource)">
      {tr}save{/tr}</button>
    {else}
      <button class="mult_submit" type="submit" name="save" value="save" title="{tr}save{/tr}" id="09"
       onclick="document.editsource.action='./galaxia/sharedsource/save'; pop_no(editsource)">
      {tr}save{/tr}</button>
    {/if}

    <button class="mult_submit" type="submit" name="cancel" value="cancel" title="{tr}cancel{/tr}" id="09"
     onclick="document.editsource.action='./galaxia/sharedsource/index'; pop_no(editsource)">
    {tr}cancel{/tr}</button>

  </td>
</tr>
<tr>
  <td class="formcolor" colspan="4">
    <table>
    <tr>
    <td>
  	<textarea id='src' name="source" rows="40" cols="160">{$data|escape}</textarea>
  	</td>
  	<td>
  	{if $template eq 'y'}
      <a class="link" href="javascript:setSomeElement('src','\n{ldelim}if{rdelim}\n{ldelim}elseif{rdelim}\n{ldelim}else{rdelim}\n{ldelim}/if{rdelim}\n');">{ldelim}if{rdelim}{ldelim}/if{rdelim}</a><hr />
      <a class="link" href="javascript:setSomeElement('src','\n{ldelim}section name= loop={rdelim}\n{ldelim}sectionelse{rdelim}\n{ldelim}/section{rdelim}\n');">{ldelim}section{rdelim}{ldelim}/section{rdelim}</a><hr />
      <a class="link" href="javascript:setSomeElement('src','\n{ldelim}foreach from= item={rdelim}\n{ldelim}foreachelse{rdelim}\n{ldelim}/foreach{rdelim}\n');">{ldelim}foreach{rdelim}{ldelim}/foreach{rdelim}</a><hr />
      <a class="link" href="javascript:setSomeElement('src','\n{ldelim}tr{rdelim}{ldelim}/tr{rdelim}\n');">{ldelim}tr{rdelim}{ldelim}/tr{rdelim}</a><hr />
      <a class="link" href="javascript:setSomeElement('src','\n{ldelim}literal{rdelim}\n{ldelim}/literal{rdelim}\n');">{ldelim}literal{rdelim}{ldelim}/literal{rdelim}</a><hr />
      <a class="link" href="javascript:setSomeElement('src','\n{ldelim}* *{rdelim}\n');">{ldelim}* *{rdelim}</a><hr />
      <a class="link" href="javascript:setSomeElement('src','\n{ldelim}{literal}strip{/literal}{rdelim}{ldelim}{literal}/strip{/literal}{rdelim}\n');">{ldelim}{literal}strip{/literal}{rdelim}{ldelim}{literal}/strip{/literal}{rdelim}</a><hr />
      <a class="link" href="javascript:setSomeElement('src','\n{ldelim}include file={rdelim}\n');">{ldelim}include{rdelim}</a><hr />

      <a class="link" href="javascript:setSomeElement('src',
                            '\n<INPUT TYPE=text NAME=$name VALUE=$value SIZE=20 MAXLENGTH=40>' );">
                            {tr}form:input text{/tr}</a><hr />

      <a class="link" href="javascript:setSomeElement('src','\n<TEXTAREA NAME=$name VALUE=$value COLS=20 ROWS=5>' );">
                            {tr}form:input textarea{/tr}</a><hr />

      <a class="link" href="javascript:setSomeElement('src',
                            '\n<INPUT TYPE=checkbox NAME=$name VALUE=$value CHECKED>' );">
                            {tr}form:input checkbox{/tr}</a><hr />

      <a class="link" href="javascript:setSomeElement('src',
                            '\n<INPUT TYPE=radio NAME=$name VALUE=$value CHECKED>' );">
                            {tr}form:input radio{/tr}</a><hr />

      <a class="link" href="javascript:setSomeElement('src',
                            '\n<INPUT TYPE=submit NAME=$name VALUE=$value>' );">
                            {tr}form:input submit{/tr}</a><hr />

      <a class="link" href="javascript:setSomeElement('src',
                            '\n<INPUT TYPE=file NAME=$name ACCEPT=$mediatype>' );">
                            {tr}form:input file{/tr}</a><hr />

      <a class="link" href="javascript:setSomeElement('src',
                            '\n<INPUT TYPE=button NAME=$name VALUE=$value>' );">
                            {tr}form:input file{/tr}</a><hr />

      <a class="link" href="javascript:setSomeElement('src',
                            '\n<SELECT NAME=sections SIZE=1 MULTIPLE>
                            \n<OPTION>option1</OPTION>
                            \n<OPTION>option2</OPTION>
                            \n<OPTION>option3</OPTION>
                            \n</SELECT>' );">
                            {tr}form:select{/tr}</a><hr />

  	{else}
  		{literal}
  		<a class="link" href="javascript:setSomeElement('src','$instance->setNextUser(\'\');');">{/literal}{tr}Set next user{/tr}{literal}</a><hr />
  		<a class="link" href="javascript:setSomeElement('src','$instance->get(\'\');');">{/literal}{tr}Get property{/tr}{literal}</a><hr />
  		<a class="link" href="javascript:setSomeElement('src','$instance->set(\'\',\'\');');">{/literal}{tr}Set property{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$document;');">
      {/literal}{tr}Current document object{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$document->getId();');">
      {/literal}{tr}Id of document{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$document->getProperty($property_name);');">
      {/literal}{tr}Current document property{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$document->setProperty($property_name,$val);');">
      {/literal}{tr}Set a document property{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$document->setProperty(\'document_state\',$state);');">
      {/literal}{tr}Change state of document{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$document->setProperty(\'document_access_code\', 5);');">
      {/literal}{tr}Lock document{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$document->copyAssociatedFiles($target_dir, $replace=false);');">
      {/literal}{tr}Copy of associated file to{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$document->getDocfiles();');">
      {/literal}{tr}Get docfiles{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$document->getDocfile(\'main\');');">
      {/literal}{tr}Get main docfiles{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$document->getContainer();');">
      {/literal}{tr}Get container of document{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$this->sendMessage($to ,$subject, $body);');">
      {/literal}{tr}Send a message{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$this->setMessageToNextUsers($subject, $body);');">
      {/literal}{tr}Send a message to next user{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          'rb_user::getCurrentUser()->getName();');">
      {/literal}{tr}Current user name{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$docflow->resetProcess();');">
      {/literal}{tr}Cancel process{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$document->setProperty('instance_id', NULL);');">
      {/literal}{tr}Unset process document{/tr}{literal}</a><hr />

  		<a class="link" href="javascript:setSomeElement('src',
                          '$this->error_stack->push(ERROR, \'Warning\', array(), $msg);');">
      {/literal}{tr}Create a feedback message{/tr}{literal}</a><hr />

		  {/literal}

  		{if $act_info.isInteractive eq 'y'}
			{literal}
  			<a class="link" href="javascript:setSomeElement('src','$instance->complete();');">{/literal}{tr}Complete{/tr}{literal}</a><hr />
  			<a class="link" href="javascript:setSomeElement('src','if(isset($_REQUEST[\'save\'])){\n  $instance->complete();\n}');">{/literal}{tr}Process form{/tr}{literal}</a><hr />
			{/literal}
  		{/if}
  		{if $act_info.type eq 'switch'}
 			{literal}
			<a class="link" href="javascript:setSomeElement('src','$instance->setNextActivity(\'\');');">{/literal}{tr}Set Next act{/tr}{literal}</a><hr />
			<a class="link" href="javascript:setSomeElement('src','if() {\n  $instance->setNextActivity(\'\');\n}');">{/literal}{tr}If:SetNextact{/tr}{literal}</a><hr />
			<a class="link" href="javascript:setSomeElement('src','switch($instance->get(\'\')){\n  case:\'\':\n  $instance->setNextActivity(\'\');\n  break;\n}');">{/literal}{tr}Switch construct{/tr}{literal}</a><hr />
			{/literal}
  		{/if}
  	{/if}
  	
  	</td>
  	</tr>
  	</table>
  </td>
</tr>
</table>  
</form>
