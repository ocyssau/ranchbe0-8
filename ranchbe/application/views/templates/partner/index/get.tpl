{*Smarty template*}

<h1 class="pagetitle">{$PageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
{*assign var="histoOption" value="1"*}
{$views_helper_filter_form|toggleanim}

{* -------------------Pagination------------------------ *}
{$views_helper_pagination}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">
<div class="scroll" id="scroll">
	<table class="scroll" onLoad="load();" >
		<thead class="fixedHeader">
			<tr class="fixedHeader" >
				<th class="heading auto" width="50px">
				  {partner_action_menu 
					  space=$SPACE_NAME 
					  ticket=$ticket
					  displayText = true
					  mode='main'
					  formId = 'checkform'
					  ifSuccessModule='partner'
					  ifSuccessController='index'
					  ifSuccessAction='get'
					  ifFailedModule='partner'
					  ifFailedController='index'
					  ifFailedAction='get'
					}
				<input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxes(this.form, 'partner_id[]', this.checked);">
				<div id="displaySelectedRowCount"></div>
				</th>
				<th class="heading auto"></th>
				<th class="heading">{column_header sort_order='INV' sort_field='partner_type'}</th>
				<th class="heading">{column_header sort_order='INV' sort_field='partner_number'}</th>
				<th class="heading">{column_header sort_order='INV' sort_field='first_name'}</th>
				<th class="heading">{column_header sort_order='INV' sort_field='last_name'}</th>
				<th class="heading">{column_header sort_order='INV' sort_field='adress'}</th>
				<th class="heading">{column_header sort_order='INV' sort_field='city'}</th>
				<th class="heading">{column_header sort_order='INV' sort_field='zip_code'}</th>
				<th class="heading">{column_header sort_order='INV' sort_field='phone'}</th>
				<th class="heading">{column_header sort_order='INV' sort_field='cell_phone'}</th>
				<th class="heading">{column_header sort_order='INV' sort_field='mail'}</th>
				<th class="heading">{column_header sort_order='INV' sort_field='web_site'}</th>
				<th class="heading">{column_header sort_order='INV' sort_field='activity'}</th>
				<th class="heading">{column_header sort_order='INV' sort_field='company'}</th>
			</tr>
		</thead>
		<tbody class="scrollContent" id="contentTbody">
			{*--------------------list body---------------------------*}
			{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
			{section name=list loop=$list}
			 <tr class="{cycle}">
			  <td class="thin"><input type="checkbox" name="partner_id[]" value="{$list[list].partner_id}" {if $list[list].checked eq 'y'}checked="checked" {/if} /></td>
			  <td class="thin">
			  {partner_action_menu 
				  space=$SPACE_NAME 
				  ticket=$ticket
				  displayText = true
				  mode='contextual'
				  infos = $list[list]
				  formId = 'checkform'
				  ifSuccessModule='partner'
				  ifSuccessController='index'
				  ifSuccessAction='get'
				  ifFailedModule='partner'
				  ifFailedController='index'
				  ifFailedAction='get'
				}
			  </td>
			  <td class="thin">{tr}{$list[list].partner_type}{/tr}</td>
			  <td class="thin">{$list[list].partner_number}</td>
			  <td class="thin">{$list[list].first_name}</td>
			  <td class="thin">{$list[list].last_name}</td>
			  <td class="thin">{$list[list].adress}</td>
			  <td class="thin">{$list[list].city}</td>
			  <td class="thin">{$list[list].zip_code}</td>
			  <td class="thin">{$list[list].phone}</td>
			  <td class="thin">{$list[list].cell_phone}</td>
			  <td class="thin"><a href="mailto:{$list[list].mail}">{$list[list].mail}</a></td>
			  <td class="thin"><a href="http://{$list[list].web_site}">{$list[list].web_site}</a></td>
			  <!--<td class="thin"><a href="ftp://{$list[list].ftp_site}">{$list[list].ftp_site}</a></td>-->
			  <td class="thin">{$list[list].activity}</td>
			  <td class="thin">{$list[list].company}</td>
			 </tr>
			{/section}
		</tbody>
	</table>
</div>
{* -------------------Pagination------------------------ *}
{$views_helper_pagination}

<input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxes(this.form,'partner_id[]',this.checked)"/>
<label for="clickall">{tr}select all{/tr}</label>
</form>

{literal}

<script type="text/javascript" src="./js_dev/jQuery.js">
</script>

<script type="text/javascript">
initTbody();

	var cpt=0;
  function menuAjax(divId,projectId){
	  if(cpt==0){
	  cpt=1;
	  dojo.xhrGet({
	    url: "/ranchbe/project/index/getmenu",
	    load: function(responseObject, ioArgs){
	      dojo.byId(divId).innerHTML = responseObject;
	    },
	    error: function(response, ioArgs){
	      alert("erro");
	    }
	  });
	  }
	}
</script>
{/literal}
