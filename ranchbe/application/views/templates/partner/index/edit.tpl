{*Smarty template*}

{$form.javascript}

<div id="tiki-center">

<form {$form.attributes}>
{$form.hidden}

<h2>{$form.header.editheader}</h2>

<table class="normal">
  <tr>
    <td class="formcolor">{$form.partner_number.label}:</td>
    <td class="formcolor">{$form.partner_number.html}
      <br /><i>{$number_help}</i>
    </td>
  </tr>

  <tr>
    <td class="formcolor">{$form.partner_type.label}:</td>
    <td class="formcolor">{$form.partner_type.html}</td>
  </tr>

  <tr>
    <td class="formcolor">{$form.first_name.label}:</td>
    <td class="formcolor">{$form.first_name.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{$form.last_name.label}:</td>
    <td class="formcolor">{$form.last_name.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{$form.adress.label}:</td>
    <td class="formcolor">{$form.adress.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{$form.city.label}:</td>
    <td class="formcolor">{$form.city.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{$form.zip_code.label}:</td>
    <td class="formcolor">{$form.zip_code.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{$form.phone.label}:</td>
    <td class="formcolor">{$form.phone.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{$form.cell_phone.label}:</td>
    <td class="formcolor">{$form.cell_phone.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{$form.mail.label}:</td>
    <td class="formcolor">{$form.mail.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{$form.web_site.label}:</td>
    <td class="formcolor">{$form.web_site.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{$form.activity.label}:</td>
    <td class="formcolor">{$form.activity.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{$form.company.label}:</td>
    <td class="formcolor">{$form.company.html}</td>
  </tr>

 {if not $form.frozen}
  <tr>
    <td class="formcolor"> </td>
    <td class="formcolor">{$form.submit.html}&nbsp;{$form.reset.html}&nbsp;<input type="button" onclick="window.close()" value="{tr}Close{/tr}"></td>
  </tr>
  <tr>
    <td class="formcolor">{$form.requirednote}</td>
  </tr>

  {if $form.errors}
  <b>Collected Errors:</b><br />
  {foreach key=name item=error from=$form.errors}
    <font color="red">{$error}</font> in element [{$name}]<br />
  {/foreach}
  <br />
  {/if}
  {else}
    <input type="button" onclick="window.close()" value="{tr}Close{/tr}">
 {/if}

</table>
</form>

<br />

{if not $form.frozen}
<table class="normal">
<form action="./partnerManager/index/import" method="post" name="import" enctype="multipart/form-data">
  <tr>
   <td>
    {tr}Import csv{/tr} (CSV file<a {popup text='first_name;last_name;adress;city;zip_code;...<br />andre;malraux;panth�on;PARIS;75000;...'}><img src="img/icons/help.png" border="0" height="16" width="16" alt='{tr}help{/tr}' /></a>):
   </td>
   <td>
    <input type="file" name="csvlist"/>
    <input type="submit" value="csvimport" name="action" />
    <br />{tr}Overwrite{/tr}: <input type="checkbox" name="overwrite" checked="checked" />
   </td>
  </tr>
</form>
</table>
{/if}

</div>

