{*Smarty template*}
<h1 class="pagetitle">{tr}{$PageTitle}{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{$views_helper_filter_form|toggleanim}

{* -------------------Pagination------------------------ *}
{$views_helper_pagination}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$this->baseUrl('container/index/get')}">
<div class="scroll" >
<table class="normal" id="scroll">
<thead class="fixedHeader">
 <tr class="fixedHeader" >
    <th width="50px">
         {container_action_menu 
	        space=$SPACE_NAME 
	        ticket=$ticket
	        displayText = true
	        mode='main'
	        formId = 'checkform'
	        ifSuccessModule='container'
	        ifSuccessController='index'
	        ifSuccessAction='get'
	        ifFailedModule='container'
	        ifFailedController='index'
	        ifFailedAction='get'
	      }
	<input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxes(this.form, 'container_id[]', this.checked);">
	<div id="displaySelectedRowCount"></div>
 	</th>
  

  <th class="heading">
  {column_header  sort_order='INV' sort_field=$SPACE_NAME|cat:"_number" 
                  title='container_number' 
                  space=$SPACE_NAME}
  <br />
  {column_header  sort_order='INV' sort_field=$SPACE_NAME|cat:"_description" 
                  title='container_description' 
                  space=$SPACE_NAME}
  <br />
  {column_header  sort_order='INV' sort_field=$SPACE_NAME|cat:"_state" 
                  title='container_state' space=$SPACE_NAME}
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field=project_id space=$SPACE_NAME}
  </th>

  <!-- Display optionnals fields -->
  {section name=of loop=$optionalFields}
	  <th class="heading">
	  {column_header  sort_order='INV' 
	                  sort_field=$optionalFields[of].property_fieldname 
	                  title=$optionalFields[of].property_name
	                  space=$SPACE_NAME}
	  </th>
  {/section}

  <th class="heading">
  {tr}created{/tr}
  {column_header sort_order='INV' sort_field='open_by' title='by' space=$SPACE_NAME}
   - 
  {column_header sort_order='INV' sort_field='open_date' title='date' space=$SPACE_NAME}

  <br />
  {tr}closed{/tr}
  {column_header sort_order='INV' sort_field='close_by' title='by' space=$SPACE_NAME}
   - 
  {column_header sort_order='INV' sort_field='close_date' title='date' space=$SPACE_NAME}

  <br />
  {column_header sort_order='INV' sort_field='forseen_close_date' space=$SPACE_NAME}
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field='default_process_id' space=$SPACE_NAME}
  </th>
  
  <th class="heading">
  {column_header  sort_order='INV' sort_field='file_only'
                  title='Type' space=$SPACE_NAME}
  </th>
 </tr>

</thead>
<tbody class="scrollContent" id="contentTbody">

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    {if $list[list].alias_id }
    <td class="thin"></td>
    {else}
    <td class="thin" style="text-align: center;"><input type="checkbox" name="container_id[]" value="{$list[list].$CONTAINER_ID}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>
    {/if}

    <td nowrap>
      <a class="link" href="container/index/activatecontainer/space/{$SPACE_NAME}/container_id/{$list[list].$CONTAINER_ID}/ifSuccessModule/content/ifSuccessController/get/ifSuccessAction/{if $list[list].file_only}recordfile{else}document{/if}" >
      {$list[list].$CONTAINER_NUMBER}</a>
      <br /><i>{$list[list].$CONTAINER_DESCRIPTION}</i>
      <br />{$list[list].$CONTAINER_STATE}
         {container_action_menu 
         	infos = $list[list]
	        space=$SPACE_NAME 
	        ticket=$ticket
	        displayText = true
	        mode='contextual'
	        isalias = $list[list].alias_id
	        formId = 'checkform'
	        ifSuccessModule='container'
	        ifSuccessController='index'
	        ifSuccessAction='get'
	        ifFailedModule='container'
	        ifFailedController='index'
	        ifFailedAction='get'
	      }
    </td>

    <td>{$list[list].project_id|project}</td>

    <!-- Display optionnals fields -->
    {foreach key=key item=type from=$optionalFields}
      {assign var="fn" value=$type.property_fieldname}
      <td class="thin">{filter_select id=$list[list].$fn type=$type.property_type}</td>
    {/foreach}

    <td>
	    <b>{tr}created{/tr} : </b>{$list[list].open_date|date_format} - {$list[list].open_by|username}<br />
	    {if $list[list].close_date}
	    <b>{tr}closed{/tr} : </b>{$list[list].close_date|date_format} - {$list[list].close_by|username}<br />
	    {/if}
	    <b>{tr}forseen_close_date{/tr} : </b>{$list[list].forseen_close_date|date_format}
    </td>

    <td>{$list[list].default_process_id|process}</td>
    
    <td>{$list[list].file_only|container_type}</td>
    
   </tr>
  {sectionelse}
    <tr><td colspan="6">{tr}No containers to display{/tr}</td></tr>
  {/section}
  
  </tbody>
  </table></div>
{*Multiselection select action form *}
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
</form>

{literal}
<script type="text/javascript">
	initTbody();
</script>
{/literal}
    