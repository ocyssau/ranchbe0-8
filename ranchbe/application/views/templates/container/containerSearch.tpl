{*Smarty template*}

{*--------------------Actions defintion--------------------------*}
{include file='container/_actions.tpl'}

<br />

<h1 class="pagetitle">{tr}{$PageTitle}{/tr}</h1>

<i>Use the Ctrl key for multi-selection in the fields.</i>

<form method="post" name="form" action="{$smarty.server.PHP_SELF}?space={$CONTAINER_TYPE}">

{$form.hidden}

<fieldset>
<legend align="top"><i>Properties</i></legend>
<table>
  <tr><td class="formcolor">
    {$form.numrows.label}:<br />
    {$form.numrows.html}<br /></tr>
  <tr>
    <td class="formcolor">
    {$form.doctype_id.label}:<br />
    {$form.doctype_id.html}</td>
    <td class="formcolor">
    {$form.document_version.label}:<br />
    {$form.document_version.html}</td>
    <td class="formcolor">
    {$form.selectd.label}:<br />
    {$form.selectd.html}</td>
    <td class="formcolor">
    {$form.open_by.label}:<br />
    {$form.open_by.html}</td>
    <td class="formcolor">
    {$form.check_out_by.label}:<br />
    {$form.check_out_by.html}</td>
    <td class="formcolor">
    {$form.update_by.label}:<br />
    {$form.update_by.html}</td>
  </tr>
  <tr>
    <td class="formcolor">
    {$form.document_number.label}:<br />
    {$form.document_number.html}</td>
    <td class="formcolor">
    {$form.description.label}:<br />
    {$form.description.html}</td>
    <td class="formcolor">
    {$form.document_state.label}:<br />
    {$form.document_state.html}</td>
    <td class="formcolor">
    {$form.document_iteration.label}:<br />
    {$form.document_iteration.html}</td>
  </tr>
</table>
</fieldset>

<fieldset>
<legend align="top"><i>Metadatas</i></legend>
<table>
  <tr>
  <!-- Display optionnals fields -->
  <!-- TODO : revoir boucle imbriqu�e -->
  {section name=of loop=$optionalFields}
  {assign var="fn" value=$optionalFields[of].property_name}
    <td class="formcolor">
    {$form.$fn.label}:<br />
    {$form.$fn.html}</td>
  {/section}
  </tr>
</table>
</fieldset>


{literal}
<script language="JavaScript">
function displayOption(name){

    var a,b;

      var a = document.getElementById(name+"_sel");
      var b = document.getElementById('__'+name);
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";
}
</script>
{/literal}


<fieldset>
<legend align="top"><i>date and hours</i></legend>
  <label><input type="checkbox" name="open_date_min_sel" id="open_date_min_sel" value="1" onClick="displayOption('open_date_min');"/>
  {$form.open_date_min.label}:</label>
  <br />
  <span id=__open_date_min style="display:none">{$form.open_date_min.html}</span>

  <label><input type="checkbox" name="open_date_max_sel" id="open_date_max_sel" value="1" onClick="displayOption('open_date_max');"/>
  {$form.open_date_max.label}:</label>
  <br />
  <span id=__open_date_max style="display:none">{$form.open_date_max.html}</span>
  <hr />

  <label><input type="checkbox" name="checkout_date_min_sel" id="checkout_date_min_sel" value="1" onClick="displayOption('checkout_date_min');"/> 
  {$form.checkout_date_min.label}:</label>
  <br />
  <span id=__checkout_date_min style="display:none">{$form.checkout_date_min.html}</span>

  <label><input type="checkbox" name="checkout_date_max_sel" id="checkout_date_max_sel" value="1" onClick="displayOption('checkout_date_max');"/> 
  {$form.checkout_date_max.label}:</label>
  <br />
  <span id=__checkout_date_max style="display:none">{$form.checkout_date_max.html}</span>
  <hr />

  <label><input type="checkbox" name="update_date_min_sel" id="update_date_min_sel" value="1" onClick="displayOption('update_date_min');"/>
  {$form.update_date_min.label}:</label>
  <br />
  <span id=__update_date_min style="display:none">{$form.update_date_min.html}</span>

  <label><input type="checkbox" name="update_date_max_sel" id="update_date_max_sel" value="1" onClick="displayOption('update_date_max');"/>
  {$form.update_date_max.label}:</label>
  <br />
  <span id=__update_date_max style="display:none">{$form.update_date_max.html}</span>
  <br />
</fieldset>

<fieldset>
{if not $form.frozen}
{$form.reset.html}&nbsp;{$form.submit.html}
{/if}
</fieldset>

</form>


<a name="list" ></a>
{*--------------------Display messages------------------------------*}
<b><font color="red">{$search_return}</font></b><br />

{*--------------------list header----------------------------------*}

<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}?space={$CONTAINER_TYPE}">
<table class="normal">
<tr>
  <th class="heading auto"></th>
  {section name=header loop=$select}
  <th class="heading">{tr}{$select[header]}{/tr}</th>
  {/section}
</tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">

    <td class="thin"><a href="{$smarty.server.PHP_SELF}?action=downloadFile&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}" title="{tr}View document{/tr}">
    <img border="0" alt="no icon" src="{$icons_dir}/C/{$list[list].doctype_id}.gif" /></td>
    {foreach item=field from=$select}
      <!--<td class="thin">{$list[list].$field}</a></td>-->
      <td class="thin">{filter_select id=$list[list].$field type=$type.property_type property_name=$field}</td>
    {/foreach}

   </tr>
  {/section}
  </table>
</form>
