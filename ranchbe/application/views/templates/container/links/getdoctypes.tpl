{*Smarty template*}

<h2>{$PageTitle}</h2>
{* -------------------Pagination------------------------ *}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">{column_header sort_order='INV' sort_field='doctype_number'}</th>
  <th class="heading">{tr}Process{/tr}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="link_id[]" value="{$list[list].link_id}" /></td>
    <td class="thin">{$list[list].doctype_number}</td>
    <td class="thin">{$list[list].process_name} {$list[list].process_version}</td>
   </tr>
  {/section}
</table>

{* -------------------Pagination------------------------ *}

{*{include file='pagination.tpl'}*}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'link_id[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="linkDoctype" title="{tr}Link doctype{/tr}" id="02"
onclick="document.checkform.action='./{$sameModule}/links/linkdoctype'; pop_no(checkform)">
<img class="icon" src="./img/icons/doctype/doctype_link.png" title="{tr}Link doctype{/tr}" alt="{tr}Link doctype{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="unlinkDoctype" title="{tr}Unlink doctype{/tr}" id="01"
onclick="document.checkform.action='./{$sameModule}/links/unlinkdoctype'; pop_no(checkform)">
<img class="icon" src="./img/icons/doctype/doctype_unlink.png" title="{tr}Unlink doctype{/tr}" alt="{tr}Unlink doctype{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="linkProcess" title="{tr}Link process{/tr}" id="02"
onclick="document.checkform.action='./{$sameModule}/links/linkprocess'; pop_no(checkform)">
<img class="icon" src="./img/icons/process/process_link.png" title="{tr}Link process{/tr}" alt="{tr}Link process{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="unlinkDoctypeProcess" title="{tr}Unlink process{/tr}" id="02"
onclick="document.checkform.action='./{$sameModule}/links/unlinkprocess'; pop_no(checkform)">
<img class="icon" src="./img/icons/process/process_unlink.png" title="{tr}Unlink process{/tr}" alt="{tr}Unlink process{/tr}" width="16" height="16" />
</button>

<hr />

{*Submit checkform form*}

<p></p>

<input type="hidden" name="object_id" value="{$object_id}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="project_id" value="{$project_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
</form>
