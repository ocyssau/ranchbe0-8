{*Smarty template*}

<h2>{tr}Linked reads to{/tr} {$container_number}</h2>

{* -------------------Pagination------------------------ *}

{*--------------------list header----------------------------------*}

<form name="checkform" method="post" action="{$sameurl}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">{column_header sort_order='INV' sort_field='reposit_number'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='reposit_description'}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="link_id[]" value="{$list[list].link_id}" /></td>

    {*-Specifics fields-*}
    <td class="thin">{$list[list].reposit_number}</td>
    <td class="thin">{$list[list].reposit_description}</td>
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{*{include file='pagination.tpl'}*}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
<script language='Javascript' type='text/javascript'>
<!--
// check / uncheck all.
// in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
// for now those people just have to check every single box
document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'link_id[]',this.checked)\"/></td>");
document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
//-->                     
</script>

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="linkRead" title="{tr}Link read{/tr}" id="02"
onclick="document.checkform.action='./container/links/linkread'; pop_no(checkform)">
<img class="icon" src="./img/icons/read/link.png" title="{tr}Link category{/tr}" alt="{tr}Link read{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="unlinkRead" title="{tr}Unlink read{/tr}" id="01"
onclick="document.checkform.action='./container/links/unlinkread'; pop_no(checkform)">
<img class="icon" src="./img/icons/read/unlink.png" title="{tr}Unlink category{/tr}" alt="{tr}Unlink read{/tr}" width="16" height="16" />
</button>

<hr />

{*Submit checkform form*}

<p></p>

<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
</form>

