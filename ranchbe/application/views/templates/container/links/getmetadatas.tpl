{*Smarty template*}

<h2>{tr}Linked Metadatas to{/tr} {$container_number}</h2>

{* -------------------Pagination------------------------ *}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">{column_header sort_order='INV' sort_field='property_name'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='property_description'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='property_type'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='property_length'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='field_regex'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='is_required'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='is_multiple'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='return_name'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='selectdb_where'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='field_list'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='table_name'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='field_for_value'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='field_for_display'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='date_format'}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="link_id[]" value="{$list[list].link_id}" /></td>

    {*-Specifics fields-*}
    <td class="thin">{$list[list].property_name}</td>
    <td class="thin">{$list[list].property_description}</td>
    <td class="thin">{$list[list].property_type}</td>
    <td class="thin">{$list[list].property_length}</td>
    <td class="thin">{$list[list].field_regex}</td>
    <td class="thin">{$list[list].is_required|yesorno}</td>
    <td class="thin">{$list[list].is_multiple|yesorno}</td>
    <td class="thin">{$list[list].return_name|yesorno}</td>
    <td class="thin">{$list[list].selectdb_where}</td>
    <td class="thin">{$list[list].field_list}</td>
    <td class="thin">{$list[list].table_name}</td>
    <td class="thin">{$list[list].field_for_value}</td>
    <td class="thin">{$list[list].field_for_display}</td>
    <td class="thin">{$list[list].date_format}</td>
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
<script language='Javascript' type='text/javascript'>
<!--
// check / uncheck all.
// in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
// for now those people just have to check every single box
document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'link_id[]',this.checked)\"/></td>");
document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
//-->
</script>

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

</button>

<button class="mult_submit" type="submit" name="action" value="linkMetadata" title="{tr}Link metadata{/tr}" id="02"
onclick="document.checkform.action='./container/links/linkmetadata'; pop_no(checkform)">
<img class="icon" src="./img/icons/metadata/link.png" title="{tr}Link metadata{/tr}" alt="{tr}Link metadata{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="unlinkMetadata" title="{tr}Unlink metadata{/tr}" id="01"
onclick="document.checkform.action='./container/links/unlinkmetadata'; pop_no(checkform)">
<img class="icon" src="./img/icons/metadata/unlink.png" title="{tr}Unlink metadata{/tr}" alt="{tr}Unlink metadata{/tr}" width="16" height="16" />
</button>

<hr />

{*Submit checkform form*}

<p></p>

<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
</form>
