{*Smarty template*}

<div id="tiki-center">

<h1 class="pagetitle">{$PageTitle}</h1>

<div id="page-bar">
   <span class="button2"><a class="linkbut" 
   href="container/notification/create/space/{$SPACE_NAME}/container_id/{$container_id}">
   {tr}Create{/tr}
   </a></span>
</div>

<br />

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <!--
  <th class="heading auto"></th>
  -->
  <th class="heading">{tr}onEvent{/tr}</a></th>
  <th class="heading">{tr}onCondition{/tr}</a></th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
{section name=list loop=$list}
<tr class="{cycle}">
  <td class="thin"><input type="checkbox" name="notification_id[]" 
                                        value="{$list[list].notification_id}" />
  </td>

  <!--
  <td class="thin">
  <a href="container/notification/edit/notification_id/{$list[list].notification_id}/space/{$SPACE_NAME}"
      title="{tr}edit{/tr}">
      <img border="0" alt="{tr}edit{/tr}" src="img/icons/edit.png" /></a>
  </td>
  -->

  <td class="thin">{$list[list].event}</td>
  <td class="thin">
    {foreach from=$list[list].test item=curr_test}
      {*property operator value*}
      {$curr_test.1} {$curr_test.0} {$curr_test.2} <br />
    {/foreach}
  </td>
</tr>
{/section}
</table>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
onclick="if(confirm('{tr}Do you want really suppress this notification{/tr}'))
{ldelim}document.checkform.action='./container/notification/suppress';
pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
onclick="document.checkform.action='{$sameurl}'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>


<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
<input type="hidden" name="container_id" value="{$container_id}" />

</form>

</div>

