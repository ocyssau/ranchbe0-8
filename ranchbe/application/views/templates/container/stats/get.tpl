{*Smarty template*}

<h1 class="pagetitle">{tr}{$PageTitle}{/tr}</h1>

<br />
<hr />
{$select_form}
<hr />
<br />

<table class="normal">
<tr><td>
  {section name=graphs loop=$graphs}
  {$graphs[graphs].image}<br />
  {/section}
</td>
<td>
  {section name=text loop=$graphs}
  {$graphs[text].text}<br />
  {/section}
</td></tr>
{if empty($graphs)}
  <td><b>Nothing to display</b></td>
{/if}
</table>
