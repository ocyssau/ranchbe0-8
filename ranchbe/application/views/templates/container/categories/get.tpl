{*Smarty template*}

{$form.javascript}

<h1 class="pagetitle">{tr}{$PageTitle}{/tr}</h1>

<div id="page-help">
<p>{tr}categories_aide1{/tr}</p>
</div>

<div id="page-bar">
   <span class="button2"><a class="linkbut" href="container/categories/create/space/{$SPACE_NAME}">{tr}Create{/tr}</a></span>
</div>

<br />

{* -------------------Pagination------------------------ *}

{*--------------------list header----------------------------------*}

<form name="checkform" method="post" action="{$sameurl}">
<div class="scroll" >
<table class="normal" id="scroll">
<thead class="fixedHeader">
 <tr>
  <th class="heading auto">
  <input name="switcher" id="clickall" type="checkbox" 
            onclick="switchCheckboxes(this.form,'category_id[]',this.checked)" /></th>

  <th class="heading auto"></th>

  <th class="heading">{column_header sort_order='INV' sort_field='category_number'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='category_description'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='category_icon'}</th>
  <th class="heading">{tr}linked properties{/tr}</th>
  <th class="heading">{tr}linked doctypes{/tr}</th>
 </tr>
</thead>
<tbody class="scrollContent" id="contentTbody">

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="category_id[]" value="{$list[list].category_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    <td class="thin"><a href="container/categories/edit/space/{$SPACE_NAME}/category_id/{$list[list].category_id}" title="{tr}edit{/tr}">
                     <img border="0" alt="{tr}edit{/tr}" src="img/icons/edit.png" /></a>
    </td>

    {*-Specifics fields-*}
    <td class="thin">{$list[list].category_number}</td>
    <td class="thin">{$list[list].category_description}</td>
    <td class="thin">{$list[list].category_icon}</td>

    <td class="thin">
      <a href="container/categories/linkproperty/category_id/{$list[list].category_id}/space/{$SPACE_NAME}">
      <font size="-3"><i>{tr}Add/Remove{/tr}</i></font></a>
      {if $list[list].properties}
      <img src="img/icons/untoggle.png" title="{tr}toggle action{/tr}"
      id="properties_imgtoggle_{$list[list].category_id}"
      alt="{tr}actions{/tr}"
      onclick="toggleLine('properties_{$list[list].category_id}', 'properties_imgtoggle_{$list[list].category_id}')">
      <div  id="properties_{$list[list].category_id}" 
            style="display:none;overflow:hidden;height:10px;margin:0;padding:0;">
      <ul>
        {section name=prop loop=$list[list].properties}
          <li>
            <a href="javascript:popupP('document/metadata/edit/property_id/{$list[list].properties[prop].property_id}/space/{$SPACE_NAME}','EditMetadata',600,800)">
            {$list[list].properties[prop].property_description}</a>
            <a href="container/categories/unlinkproperty/link_id/{$list[list].properties[prop].link_id}/category_id/{$list[list].category_id}/space/{$SPACE_NAME}"
            onclick="return confirm('{tr}Do you want really unlink{/tr} {$list[list].properties[prop].property_description}')">
           <img border="0" alt="{tr}unlink it{/tr}" src="img/icons/trash.png" /></a>
         </li>
        {/section}
      </ul>
      {/if}
    </div>
    </td>

    <td class="thin">
      <a href="container/categories/linkDoctype/category_id/{$list[list].category_id}/space/{$SPACE_NAME}">
      <font size="-3"><i>{tr}Add/Remove{/tr}</i></font></a>
      {if $list[list].doctypes}
      <img src="img/icons/untoggle.png" title="{tr}toggle action{/tr}" alt=""
      id="doctypes_imgtoggle_{$list[list].category_id}"
      onclick="toggleLine('doctypes_{$list[list].category_id}','doctypes_imgtoggle_{$list[list].category_id}')">
      <div id="doctypes_{$list[list].category_id}" 
            style="display:none;overflow:hidden;height:10px;margin:0;padding:0;">
      <ul>
        {section name=doctype loop=$list[list].doctypes}
          <li>
            <a href="doctypesManager/edit/doctype_id/{$list[list].doctypes[doctype].doctype_id}">{$list[list].doctypes[doctype].doctype_number}</a>
            <a href="container/categories/unlinkdoctype/category_id/{$list[list].category_id}/link_id/{$list[list].doctypes[doctype].link_id}/space/{$SPACE_NAME}"
            onclick="return confirm('{tr}Do you want really unlink{/tr} {$list[list].properties[doctype].property_description}')">
           <img border="0" alt="{tr}unlink it{/tr}" src="img/icons/trash.png" /></a>
         </li>
        {/section}
      </ul>
      {/if}
    </div>
    </td>

   </tr>
  {/section}
  </tbody>
  </table></div>

{* -------------------Pagination------------------------ *}
{literal}
<script type="text/javascript">
	initTbody();
</script>
{/literal}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
<input name="switcher" id="clickall" type="checkbox" 
          onclick="switchCheckboxes(this.form,'category_id[]',this.checked)" />

<br>
{*Multiselection select action form *}
<img class="icon" src="img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppressCat" title="{tr}Suppress{/tr}" 
 onclick="if(confirm('{tr}Do you want really suppress this category{/tr}'))
 {ldelim}document.checkform.action='container/categories/suppress'; pop_no(checkform);
 {rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="space" value="{$SPACE_NAME}" />
<input type="hidden" name="ticket" value="{$ticket}" />
</form>

<hr />

