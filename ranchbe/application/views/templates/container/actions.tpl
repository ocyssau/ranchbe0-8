{*Smarty template*}
<table>
<tr><td>
	<a href="javascript:popupP('container/index/create/space/{$SPACE_NAME}','{$SPACE_NAME}Manage',550,800)" class="button">
		<span class="add">{tr}Create{/tr}</span>
	</a>
</td><td>
	<a href="javascript:popupP('container/alias/create/space/{$SPACE_NAME}','{$SPACE_NAME}Manage',1024,800)" class="button">
		<span class="add">{tr}Create alias{/tr}</span>
	</a>
</td><td>
	<a href="javascript:popupP('container/history/get/space/{$SPACE_NAME}','{$randWindowName}',1024,800)" class="button">
		<span class="histo">{tr}History{/tr}</span>
	</a>
</td><td>
	<a href="import/history/get/space/{$SPACE_NAME}" class="button">
		<span class="histo">{tr}History import{/tr}</span>
	</a>
</td><td>
	<a href="javascript:popupP('container/categories/get/space/{$SPACE_NAME}','{$CONTAINER_TYPE}Category',450,1200)" class="button">
		<span class="info">{tr}Categories{/tr}</span>
	</a>
</td><td>
	<a href="javascript:popupP('container/metadatas/get/space/{$SPACE_NAME}','{$CONTAINER_TYPE}MetaData',450,1200)"  class="button">
		<span class="doctyp">{tr}Container metadata{/tr}</span>
	</a>
</td><td>
	<a href="javascript:popupP('document/metadatas/get/space/{$SPACE_NAME}','{$CONTAINER_TYPE}MetaData',450,1200)" class="button">
		<span class="doctyp">{tr}Document metadata{/tr}</span>
	</a>
</td><td>
	<a href="container/stats/get/space/{$SPACE_NAME}/ticket/{$ticket}"  class="button">
		<span class="stat">{tr}Statistics{/tr}</span>
	</a>
</td><td>
	<a href="javascript:popupP('container/perms/get/resource_id/3/space/{$SPACE_NAME}','userPerms',550,650 )" class="button">
		<span class="key">{tr}Set permissions{/tr}</span>
	</a>
</td></tr>
</table>