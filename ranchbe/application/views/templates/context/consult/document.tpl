{*Smarty template*}

<div id="page-title">
  <h1 class="pagetitle">{tr}Containers consultation{/tr}</h1>
</div>

{*--------------------Search Bar defintion--------------------------*}
{*assign var="histoOption" value="1"*}
{$views_helper_filter_form|toggleanim}


{literal}
<script type="text/javascript">
	initTbody();
</script>
{/literal}


{$views_helper_pagination}


{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
{include file='document/_list.tpl'}

{$views_helper_pagination}

<div id="action-menu">
<button class="mult_submit" type="submit" name="action" value="putInWs" title="{tr}Put in wildspace{/tr}" id="06"
 onclick="document.checkform.action='containerConsult.php?space={$space}'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_read.png" title="{tr}Put in wildspace{/tr}" alt="{tr}Put in wildspace{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="document.checkform.action='./context/consult/consult'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>
</div>

</form>
