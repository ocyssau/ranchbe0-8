{*Smarty template*}

<div id="page-title">
  <h1 class="pagetitle">{tr}Containers consultation{/tr}</h1>
</div>

{*--------------------Search Bar defintion--------------------------*}
{*assign var="histoOption" value="1"*}
{$views_helper_filter_form|toggleanim}


{literal}
<script type="text/javascript">
	initTbody();
</script>
{/literal}

{*--------------------list definition----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">
{include file='recordfile/_list.tpl'}

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<div id="action-menu">
<button class="mult_submit" type="submit" name="action" value="putInWs" title="{tr}Put in wildspace{/tr}" id="06"
 onclick="document.checkform.action='./recordfile/index/putinws'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_read.png" title="{tr}Put in wildspace{/tr}" alt="{tr}Put in wildspace{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="document.checkform.action='{$sameurl}'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>
</div>

<p></p>

<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
</form>

