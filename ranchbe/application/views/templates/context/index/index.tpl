{*Smarty template*}

<h1 class="pagetitle">{tr}Context{/tr}</h1>

<div id="page-bar">
   <span class="button2"><a class="linkbut" href="context/index/generateCATIASearchOrder">{tr}Generate SearchOrder for CATIA{/tr}</a></span>
</div>

{if isset($searchOrder)}
  <br />
  <form action="./context/index/save" method="post" name="saveSearchOrder" enctype="multipart/form-data">
  <fieldset>
  <legend align="top"><i>{tr}Search order for CATIA{/tr} :</i></legend>
  <i>Generate for <b>{$CLIENT_OS}</b> system</i>
  <pre>{$searchOrder}</pre>
  <input type="submit" value="save" name="action" />
  <input type="hidden" name="searchOrder" value="{$searchOrder}" />
  </fieldset>
  </form>
{/if}

<br />

<table class="normal">
<tr>
  <th>Project</th><td class="odd">{$project_number}</td>
  <th>{$SPACE_NAME}</th><td class="odd">{$container_number}</td>
</tr>
</table>

<br />

{*--------------------list header----------------------------------*}
<table class="normal">
<tr>

<th class="heading">
{tr}{$HeaderCol1}{/tr}
</th>

<th class="heading">
{tr}{$HeaderCol2}{/tr}
</th>

<th class="heading">
{tr}{$HeaderCol3}{/tr}
</th>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
{section name=list loop=$list}
<tr class="{cycle}">

<td class="thin">{$list[list].$col1}</td>

<td class="thin">{$list[list].$col2}</td>

<td class="thin">{$list[list].$col3}</td>

</tr>
{/section}
</table>


