{*Smarty template*}

<table border=0>
<tr>
  <td><img src="{$logo}"></td>
  
  <td width="50%"style="text-align:left"><h1>{tr}Welcome{/tr} {$current_user_name}, <br />
  {tr}You are connected on{/tr} <i>RANCHBE</i>&#169; {$ranchbe_version} {$ranchbe_build}
  </h1>
  <h2> ...{tr}The design office ranch{/tr}</h2> 
  </td>

</tr>
</table>

<p></p>
<div class="rbbox">
  {include file="search/searchBar.tpl"}
</div>

<p></p>
<div class="rbbox">
  {include file='home/messages.tpl'}
</div>

{*
<p></p>
<div class="rbbox">
  {include file='home/tasks.tpl'}
</div>
*}
<p></p>
