{*Smarty template*}
<h2>{tr}New messages{/tr}</h2>
<table class="normal">
  <tr>
    <td class="heading" >&nbsp;</td>
    <td class="heading" >{tr}sender{/tr}</td>
    <td class="heading" >{tr}subject{/tr}</td>
    <td class="heading" >{tr}date{/tr}</td>
    <td class="heading" >{tr}size{/tr}</td>
  {cycle values="odd,even" print=false}
  {section name=user loop=$items}
  <tr>
    <td class="prio{$items[user].priority}">
    {if $items[user].isFlagged eq 'y'}
      <img src="img/icons/flagged.png" border="0" width="16" height="16" alt='{tr}flagged{/tr}' />
    {/if}
    </td>

    <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">{$items[user].user_from}</td>
    <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">
    <a class="readlink" href="messages/read/index/msgId/{$items[user].msgId}">{$items[user].subject}</a></td>
    <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">{$items[user].date|date_format}</td>
    <td  style="text-align:right;{if $items[user].isRead eq 'n'}font-weight:bold;{/if}" class="prio{$items[user].priority}">{$items[user].len}</td>
  </tr>
  {sectionelse}
  <tr><td colspan="6">{tr}No messages to display{/tr}</td></tr>
  {/section}
</table>
