{*Smarty template*}
<h2>{tr}New tasks{/tr}</h2>
<table class="normal">
<tr>
  <td class="heading" style="text-align:right;" >&nbsp;</td>
  <td class="heading" >{tr}title{/tr}</td>
  <td class="heading" >{tr}start{/tr}</td>
  <td class="heading" >{tr}end{/tr}</td>
  <td style="text-align:right;" class="heading" >{tr}priority{/tr}</td>
  <td style="text-align:right;" class="heading" >{tr}completed{/tr}</td>
</tr>

{cycle values="odd,even" print=false}
{section name=task_i loop=$tasklist}
<tr>
<td class="prio{$tasklist[task_i].priority}">

{if $tasklist[task_i].deleted} 
	{if $tasklist[task_i].creator ne $user}
		<img src='img/icons/trash.png' width='16' height='16' border='0' alt='{tr}delete{/tr}' />
	{else}
		<img src='img/icons/trash.gif' width='16' height='16' border='0' alt='{tr}trash{/tr}' />
	{/if}
{/if}

{if (($tasklist[task_i].creator eq $tasklist[task_i].user) and ($tasklist[task_i].user eq $user)) }
	{*private task*}
{else}
	{if ($tasklist[task_i].user eq $user) }
		{*recived task*}
		&gt;&gt;
		{if (($tasklist[task_i].accepted_creator eq 'n') or ($tasklist[task_i].accepted_user eq 'n')) }
			<img src="{$img_not_accepted}" height="{$img_not_accepted_height}" width="{$img_not_accepted_width}" title='{tr}not accepted by one user{/tr}'  border='0' alt='{tr}not accepted user{/tr}' />
		{else}
			{if ($tasklist[task_i].accepted_user eq '')}
				<img src="{$img_me_waiting}" height="{$img_me_waiting_height}" width="{$img_me_waiting_width}" title='{tr}waiting for me{/tr}' border='0' alt='{tr}waiting for me{/tr}' />
			{else}
				{if ($tasklist[task_i].accepted_creator eq 'y')} 
					<img src="{$img_accepted}" height="{$img_accepted_height}" width="{$img_accepted_width}" title='{tr}accepted by task user and creator{/tr}' border='0' alt='{tr}accepted by task user and creator{/tr}' />
				{else}
					<img src="{$img_he_waiting}"  height="{$img_he_waiting_height}" width="{$img_he_waiting_width}" border='0' alt='{tr}waiting for other user{/tr}' title='{tr}waiting for other user{/tr}' />
				{/if}
			{/if}
		{/if}

	{elseif ($tasklist[task_i].creator eq $user) }
		{*submitted task*}
		&lt;&lt;
		{if (($tasklist[task_i].accepted_creator eq 'n') or ($tasklist[task_i].accepted_user eq 'n')) }
			<img src="{$img_not_accepted}" height="{$img_not_accepted_height}" width="{$img_not_accepted_width}" title='{tr}not accepted by one user{/tr}'  border='0' alt='{tr}not accepted user{/tr}' />
		{else}
			{if ($tasklist[task_i].accepted_user eq '')}
				{if ($tasklist[task_i].accepted_creator eq 'y')}
					 <img src="{$img_he_waiting}"  height="{$img_he_waiting_height}" width="{$img_he_waiting_width}" border='0' alt='{tr}waiting for other user{/tr}' title='{tr}waiting for other user{/tr}' />
				{else}
					<img src="{$img_me_waiting}" height="{$img_me_waiting_height}" width="{$img_me_waiting_width}" title='{tr}waiting for me{/tr}' border='0' alt='{tr}waiting for me{/tr}' />
				{/if}
			{else}
				{if ($tasklist[task_i].accepted_creator eq 'y')}
					<img src="{$img_accepted}" height="{$img_accepted_height}" width="{$img_accepted_width}" title='{tr}accepted by task user and creator{/tr}' border='0' alt='{tr}accepted by task user and creator{/tr}' />
				{else}
					<img src="{$img_me_waiting}" height="{$img_me_waiting_height}" width="{$img_me_waiting_width}" title='{tr}waiting for me{/tr}' border='0' alt='{tr}waiting for me{/tr}' />
				{/if}
			{/if}
		{/if}
	{else}

		{*shared task*}
		&gt;&lt;
	{/if}
{/if}

	</td>
	<td class="prio{$tasklist[task_i].priority}">
<a {if $tasklist[task_i].status eq 'c'}style="text-decoration:line-through;"{/if} class="link" href="tiki-user_tasks.php?task_id={$tasklist[task_i].task_id}&amp;offset={$offset}&amp;sort_mode={$sort_mode}&amp;tiki_view_mode=view&amp;find={$find}">{$tasklist[task_i].title}</a></td>

<td {if $tasklist[task_i].status eq 'c'}style="text-decoration:line-through;"{/if} class="prio{$tasklist[task_i].priority}"><div class="mini">{$tasklist[task_i].start|date_format}</div></td>

<td {if $tasklist[task_i].status eq 'c'}style="text-decoration:line-through;"{/if} class="prio{$tasklist[task_i].priority}"><div class="mini">{$tasklist[task_i].end|date_format}</div></td>

<td style="text-align:right;{if $tasklist[task_i].status eq 'c'}text-decoration:line-through;{/if}" class="prio{$tasklist[task_i].priority}">{$tasklist[task_i].priority}</td>

<td style="text-align:right;{if $tasklist[task_i].status eq 'c'}text-decoration:line-through;{/if}" class="prio{$tasklist[task_i].priority}">
{if $tasklist[task_i].percentage_null }{tr}waiting{/tr}
{else}{$tasklist[task_i].percentage}%{/if}
</td>

</tr>

{sectionelse}
<tr>
	<td class="odd" colspan="6">{tr}No tasks entered{/tr}</td>
</tr>
{/section}
</table>
{* end ************ Task list ***************}
