{*Smarty template*}

<h1 class="pagetitle">{tr}Versions of file{/tr} : {$file_name}</h1>

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">

{*--------------------list of files----------------------------------*}
{assign var="isVersion" value=1}
{include file="recordfile/_list.tpl"}

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />
<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
onclick="if(confirm('{tr}Do you want really suppress this file{/tr}'))
{ldelim}document.checkform.action='./docfile/versions/suppress';
pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="putInWs" title="{tr}Put in wildspace{/tr}"
onclick="document.checkform.action='./docfile/versions/putinws' ; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_read.png" title="{tr}Put in wildspace{/tr}" alt="{tr}Put in wildspace{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="document.checkform.action='{$sameurl}'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="docfile_id" value="{$docfile_id|escape}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
</form>

