{*Smarty template*}

{*================================= DETAIL OF FILE ===================================================*}
<h3>{tr}Detail of docfile{/tr} {$fileinfos.file_name} - {$fileinfos.file_iteration}</h3>

{include file="recordfile/_detail.tpl"}

<form name="checkform" method="post" action="{$sameurl}">

	  {docfile_action_menu 
		  space=$SPACE_NAME 
		  ticket=$ticket
		  displayText = true
		  mode='main'
		  formId = 'checkform'
		  ifSuccessModule='docfile'
		  ifSuccessController='detail'
		  ifSuccessAction='get'
		  ifFailedModule='docfile'
		  ifFailedController='detail'
		  ifFailedAction='get'
		}

<p></p>

<input type="hidden" name="file_id" value="{$file_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />

<input type="hidden" name="ifSuccessModule" value="docfile" />
<input type="hidden" name="ifSuccessController" value="detail" />
<input type="hidden" name="ifSuccessAction" value="get" />

</form>
