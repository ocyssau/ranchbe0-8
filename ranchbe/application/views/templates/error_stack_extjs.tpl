{include file="header.tpl"}

{capture assign=content}
  {section name=errors loop=$errors}
    {if $errors[errors].level == Fatal}
      <p class="fatal-text">
      <img border="0" alt="FATAL" src="img/fatal60.png" width=30px />
      {$errors[errors].message}</p>
    {elseif $errors[errors].level == Error}
      <p class="error-text"><img border="0" alt="ERROR" src="img/error60.png" width=30px />{$errors[errors].message}</p>
    {elseif $errors[errors].level == Warning}
      <p class="warning-text">
      <img border="0" alt="WARNING" src="img/warning60.png" width=30px />
      {$errors[errors].message}</p>
    {else}
      <p class="info-text">
      <img border="0" alt="INFO" src="img/info60.png" width=30px />
      {$errors[errors].message}</p>
    {/if}
    {if $debug}
      <p class="debug-text">
      <b>Error level</b> :  {$errors[errors].level}<br />
      <b>Code</b> :         {$errors[errors].code}<br />
      <b>For package</b> :  {$errors[errors].package}<br />
      <b>In class</b> :     {$errors[errors].context.class}<br />
      <b>In function</b> :  {$errors[errors].context.function}<br />
      <b>In file</b> :      {$errors[errors].context.file}<br />
      <b>In line</b> :      {$errors[errors].context.line}<br />
      </p>
    {/if}
  {/section}
  {if $close_button}
    <input type="button" onclick="window.close()" value="{tr}Close Window{/tr}" title="{tr}Close Window{/tr}">
  {/if}
  {if $back_button}
    <input type="button" onclick="javascript:history.back()" value="{tr}Go back{/tr}" title="{tr}Go back{/tr}">
  {/if}
  {if $home_button}
    <form action="accueil.php"><input type="submit" value="{tr}Return to home page{/tr}" title="{tr}Return to home page{/tr}"></form>
  {/if}
{/capture}

{literal}
<link rel="stylesheet" type="text/css" href="javascripts/ext2js/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="javascripts/ext2js/resources/css/xtheme-default.css" />
<script type="text/javascript" src="javascripts/ext2js/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="javascripts/ext2js/ext-all.js"></script>

<script type="text/javascript">
Ext.onReady(function() {
    var title = '{/literal}{tr}Error report{/tr}{literal}';
    var html = '{/literal}{$content|strip|escape:"javascript"}{literal}';
    var window = MsgWindow(title, html);
    window.show();
});
function MsgWindow(title, content){
  return new Ext.Window({
                      title: title,
                      width: 500,
                      height: 300,
                      minWidth: 300,
                      minHeight: 200,
                      layout: 'fit',
                      plain: true,
                      bodyStyle: 'padding:5px;',
                      buttonAlign: 'center',
                      html: content,
                      autoScroll:true,
                    });
}
</script>
{/literal}
