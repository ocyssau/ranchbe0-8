{*Smarty template*}

<h1 class="pagetitle">{$PageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
{*assign var="histoOption" value="1"*}
{$views_helper_filter_form|toggleanim}

{* -------------------Pagination------------------------ *}
{$views_helper_pagination}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">
<div class="scroll" >
<table  id="scroll">
<thead class="fixedHeader">
 <tr class="fixedHeader" >
  <th class="heading auto">
	  <button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}" id="01"
		onclick="if(confirm('{tr}Do you want really suppress this doctypes{/tr}')){ldelim}
		document.checkform.action='doctype/index/suppress';
		pop_no(checkform){rdelim}else {ldelim}return false;{rdelim} ">
		<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
	  </button>
	<input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxes(this.form, 'doctype_id[]', this.checked);">
	<div id="displaySelectedRowCount"></div>
  </th>
  <th class="heading">{column_header sort_order='INV' sort_field='doctype_number'}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='doctype_description'}</th>

  <th class="heading" nowrap>
  {column_header sort_order='INV' sort_field='script_pre_store'}<br />
  {column_header sort_order='INV' sort_field='script_post_store'}<br />
  {column_header sort_order='INV' sort_field='script_pre_update'}<br />
  {column_header sort_order='INV' sort_field='script_post_update'}<br />
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field='recognition_regexp'}
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field='file_extension'}
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field='file_type'}
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field='icon'}
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field='visu_file_extension'}
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field='can_be_composite'}
  </th>

 </tr>
</thead>
<tbody class="scrollContent" id="contentTbody">

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin" style="text-align: center;"><input type="checkbox" name="doctype_id[]" value="{$list[list].doctype_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    <td class="thin">{$list[list].doctype_number}<br/>
    <div class="cacher"  dojoType='dijit.form.DropDownButton' iconClass='noteIcon' onclick="menuAjax(this.id,{$list[projects].project_id});" onLoad="load(this.id);">
	  		<span>Actions</span>
			  <div dojoType='dijit.Menu'>
				<div dojoType='dijit.MenuItem'  onClick="javascript:popup('./doctype/index/edit/doctype_id/{$list[list].doctype_id}','doctypeManage')" >
			        <img border="0" alt="" src="img/icons/edit.png" />{tr}edit{/tr}
			    </div>
			    <div dojoType='dijit.MenuItem'  href="doctype/index/suppress/doctype_id/{$list[list].doctype_id}" 
                      onclick="return confirm('{tr}Do you want really suppress{/tr} {$list[list].doctype_number}')">
			        <img border='0'  src='img/icons/trash.png' />{tr}Suppress{/tr}
			    </div>
			  </div>
		</div>
	</td>
    <td class="thin">{$list[list].doctype_description}</td>

    <td class="thin">
      {$list[list].script_pre_store}<br />
      {$list[list].script_post_store}<br />
      {$list[list].script_pre_update}<br />
      {$list[list].script_post_update}<br />
    </td>

    <td class="thin">{$list[list].recognition_regexp}</td>
    <td class="thin">{$list[list].file_extension}</td>
    <td class="thin">{$list[list].file_type}</td>
    <td class="thin">{*get_icon icon=$list[list].icon*}
    				{doctype_icon icon=$list[list].icon}</td>
    <td class="thin">{$list[list].visu_file_extension}</td>
    <td class="thin">{if $list[list].can_be_composite == 1}{tr}Yes{/tr}{else}{tr}No{/tr}{/if}</td>
   </tr>
  {/section}
  </tbody>
  </table></div>


<input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxes(this.form,'doctype_id[]',this.checked)"/>
<label for="clickall">{tr}select all{/tr}</label>

<br />


<p></p>

</form>






{literal}

<script type="text/javascript" src="./js_dev/jQuery.js">
</script>

<script type="text/javascript">

initTbody();

	var cpt=0;
  function menuAjax(divId,projectId){
	  if(cpt==0){
	  cpt=1;
	  dojo.xhrGet({
	    url: "/ranchbe/project/index/getmenu",
	    load: function(responseObject, ioArgs){
	      dojo.byId(divId).innerHTML = responseObject;
	    },
	    error: function(response, ioArgs){
	      alert("erro");
	    }
	  });
	  }
	}

  
</script>
{/literal}
