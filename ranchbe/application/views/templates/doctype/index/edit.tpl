{*Smarty template*}

{literal}
<script language="JavaScript">
function afficherExtensions(type) {
    if (type == 'file'){
      var a = document.getElementById("file_extension");
      if (a.style.display == "none")
          a.style.display = "block";
      else a.style.display = "none";
    }
}

function updateicon(select) {
  document.getElementById("showIcon").src = "./img/filetypes32/"+select.value;
  return false;
}
</script>
{/literal}

{$form.javascript}

<form {$form.attributes}>
{$form.hidden}

<h2>{$form.header.editheader}</h2>

<fieldset>
  <legend>Basic</legend>
  <ul>
    <label>{$form.doctype_number.label}:</label>
    {$form.doctype_number.html}
    <br /><i>{$number_help}</i><br />
    <label>{$form.doctype_description.label}:</label>
    {$form.doctype_description.html}
  </ul>
</fieldset>

<fieldset>
  <legend>Regex</legend>
  <ul>
    <label>{$form.recognition_regexp.label}:</label>
    {$form.recognition_regexp.html}<br />
    <a href="javascript:popup('testRegExpr.htm','testRegExpr')">{tr}Need help?{/tr}</a></p>
  </ul>
</fieldset>

<fieldset>
  <legend>Scripts</legend>
  <ul>
    <label>{$form.script_pre_store.label}:</label>
    {$form.script_pre_store.html}<br />
    
    <label>{$form.script_post_store.label}:</label>
    {$form.script_post_store.html}<br />
    
    <label>{$form.script_pre_update.label}:</label>
    {$form.script_pre_update.html}<br />
    
    <label>{$form.script_post_update.label}:</label>
    {$form.script_post_update.html}<br />
  </ul>
</fieldset>

<fieldset>
  <legend>File</legend>
  <ul>
    <label>{$form.file_type.label}:</label><br />
    {$form.file_type.html}
    <br />
    <span id=file_extension {if $fileselect}style="display: block"{else}style="display: none"{/if}>
      <label>{$form.file_extension.label}:</label>
      {$form.file_extension.html}
    </span>
    {$form.can_be_composite.label}:
    {$form.can_be_composite.html}
  </ul>
</fieldset>

<fieldset>
  <legend>Visualisation file</legend>
  <ul>
    <label>{$form.visu_file_extension.label}:</label>
    {$form.visu_file_extension.html}
  </ul>
</fieldset>


{*
{$form.file_type_ext.label}:
{$form.file_type_ext.html}
*}

<fieldset>
  <legend>Icon</legend>
  <ul>
    <label>{$form.icon.label}:</label>
    {$form.icon.html}
    <img src="" id="showIcon" alt="" />
  </ul>
</fieldset>

{if not $form.frozen}
  {$form.submit.html}&nbsp;{$form.reset.html}&nbsp;
  <br />
  {$form.requirednote}

  {if $form.errors}
  <br />
  <b>Collected Errors:</b><br />
  {foreach key=name item=error from=$form.errors}
    <font color="red">{$error}</font> in element [{$name}]<br />
  {/foreach}
  {/if}
{/if}

</form>


{if not $form.frozen}
<br />
<table class="normal">
<form action="./doctype/index/import" method="post" name="import" enctype="multipart/form-data">
  <tr>
   <td>
    {tr}Import csv{/tr} (CSV file<a {popup text='doctype_number;doctype_description;file_extension<br />doctype1;description1;.doc<br />doctype2;description2;.xls'}>
    <img src="img/icons/help.png" border="0" height="16" width="16" alt='{tr}help{/tr}' /></a>):
   </td>
   <td>
    <input type="file" name="csvlist"/>
    <input type="submit" value="csvimport" name="action" />
    <br />{tr}Overwrite{/tr}: <input type="checkbox" name="overwrite" checked="checked" />
   </td>
  </tr>
</form>
</table>
{/if}


{literal}
<script language="JavaScript">
  updateicon(document.getElementById("icon_select"));
</script>
{/literal}
