{*Smarty template*}

<div id="tiki-center">

<h1 class="pagetitle">{$PageTitle}</h1>

<div id="page-help">
<p>{tr}metadata_aide1{/tr}</p>
</div>

<div id="page-bar">
   <span class="button2"><a class="linkbut" href="{$module}/metadatas/create/space/{$SPACE_NAME}">{tr}Create{/tr}</a></span>
</div>

<br />

{*--------------------list header----------------------------------*}

<form name="checkform" method="post" action="{$sameurl}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  <th class="heading">{column_header sort_order='INV' sort_field='property_name' space=$SPACE_NAME}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='property_description' space=$SPACE_NAME}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='property_type' space=$SPACE_NAME}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='property_length' space=$SPACE_NAME}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='regex' space=$SPACE_NAME}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='is_required' space=$SPACE_NAME}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='is_multiple' space=$SPACE_NAME}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='return_name' space=$SPACE_NAME}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='selectdb_where' space=$SPACE_NAME}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='select_list' space=$SPACE_NAME}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='selectdb_table' space=$SPACE_NAME}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='selectdb_field_for_value' space=$SPACE_NAME}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='selectdb_field_for_display' space=$SPACE_NAME}</th>
  <th class="heading">{column_header sort_order='INV' sort_field='date_format' space=$SPACE_NAME}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="property_id[]" value="{$list[list].property_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    <td class="thin"><a href="{$module}/metadatas/edit/property_id/{$list[list].property_id}/space/{$SPACE_NAME}" title="{tr}edit{/tr}">
       <img border="0" alt="{tr}edit{/tr}" src="img/icons/edit.png" /></a>
    </td>

    {*-Specifics fields-*}
    <td class="thin">{$list[list].property_name}</td>
    <td class="thin">{$list[list].property_description}</td>
    <td class="thin">{$list[list].property_type}</td>
    <td class="thin">{$list[list].property_length}</td>
    <td class="thin">{$list[list].regex}</td>
    <td class="thin">{$list[list].is_required|yesorno}</td>
    <td class="thin">{$list[list].is_multiple|yesorno}</td>
    <td class="thin">{$list[list].return_name|yesorno}</td>
    <td class="thin">{$list[list].selectdb_where}</td>
    <td class="thin">{$list[list].select_list}</td>
    <td class="thin">{$list[list].selectdb_table}</td>
    <td class="thin">{$list[list].selectdb_field_for_value}</td>
    <td class="thin">{$list[list].selectdb_field_for_display}</td>
    <td class="thin">{$list[list].date_format}</td>
   </tr>
  {/section}
  </table>

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
<script language='Javascript' type='text/javascript'>
<!--
// check / uncheck all.
// in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
// for now those people just have to check every single box
document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'property_id[]',this.checked)\"/></td>");
document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr} : </label></td></tr>");
//-->                     
</script>

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
onclick="if(confirm('{tr}All data of this field will be lost. Are you sure?{/tr}'))
{ldelim}document.checkform.action='./{$module}/metadatas/suppress';
pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />
</form>
