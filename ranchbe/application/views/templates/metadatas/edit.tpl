{*Smarty template*}

{literal}
<script language="JavaScript">
function toggle(elementId1, elementId2) {
  element1 = document.getElementById(elementId1);
  element2 = document.getElementById(elementId2);
  actionInput = document.getElementById('propset_mode');
  if(actionInput.value == 'existing'){
    element1.style.visibility = 'visible';
    element1.style.display = 'block';
    element2.style.visibility = 'hidden';
    element2.style.display = 'none';
    actionInput.value ='create_new';
    //alert('le champ a pour valeur : '+actionInput.value);
  }else{
    element1.style.visibility = 'hidden';
    element1.style.display = 'none';
    element2.style.visibility = 'visible';
    element2.style.display = 'block';
    actionInput.value ='existing';
  }
  return true;
}

function afficherAutre() {

  var c = document.getElementById("size");
  c.style.display = "block";

  var a = document.getElementById("select_param");
  if (document.editForm.property_type.value == 'select'){
    if (a.style.display == "none")
        a.style.display = "block";
  }else{
        a.style.display = "none";
  }

  var a = document.getElementById("select_fromDB");
  if (document.editForm.property_type.value == 'selectFromDB' ||
      document.editForm.property_type.value == 'liveSearch')
  {
    if (a.style.display == "none")
        a.style.display = "block";
  }else{
        a.style.display = "none";
  }

  var a = document.getElementById("select_predef");
  if (document.editForm.property_type.value == 'partner' ||
      document.editForm.property_type.value == 'doctype' ||
      document.editForm.property_type.value == 'user' ||
      document.editForm.property_type.value == 'process' ||
      document.editForm.property_type.value == 'category' ||
      document.editForm.property_type.value == 'indice')
  {
    if (a.style.display == "none")
        a.style.display = "block";
  }else{
        a.style.display = "none";
  }

  var b = document.getElementById("select_common");
  if (document.editForm.property_type.value == 'partner' ||
      document.editForm.property_type.value == 'select'  ||
      document.editForm.property_type.value == 'doctype' ||
      document.editForm.property_type.value == 'user' ||
      document.editForm.property_type.value == 'process' ||
      document.editForm.property_type.value == 'category' ||
      document.editForm.property_type.value == 'selectFromDB' ||
      document.editForm.property_type.value == 'indice')
  {
    if (b.style.display == "none")
        b.style.display = "block";
  }else{
        b.style.display = "none";
  }

  var a = document.getElementById("text");
  if (document.editForm.property_type.value == 'text' ||
      document.editForm.property_type.value == 'long_text')
  {
    if (a.style.display == "none")
        a.style.display = "block";
  }else{
        a.style.display = "none";
  }

  if (document.editForm.property_type.value == 'integer' ||
      document.editForm.property_type.value == 'decimal')
  {
  }

  var a = document.getElementById("date");
  if (document.editForm.property_type.value == 'date')
  {
    if (a.style.display == "none")
        a.style.display = "block";
  }else{
        a.style.display = "none";
  }

}
</script>

{/literal}
{$form.javascript}

<h2 class="pagetitle">{$form.header.editheader}</h2>

{*Create form*}
<form {$form.attributes}>
{$form.hidden}

    <b>{$form.property_type.label}:</b><br />
    {$form.property_type.html}

    <br /><br />
    <b>{$form.property_fieldname.label}:</b><br />
    {$form.property_fieldname.html}

    <br /><br />
    <b>{$form.property_name.label}:</b><br />
    {$form.property_name.html}
  
    <br /><br />
    <b>{$form.property_description.label}:</b><br />
    {$form.property_description.html}

  <span id=size style="display: block">
    <br /><br />
    <b>{$form.property_length.label}:</b><br />
    <i>{tr}metadata_max_length{/tr}</i><br />
    {$form.property_length.html}
  </span>

  <span id=text style="display: block">
    <br />
    <b>{$form.regex.label}:</b><br />
    <i>{tr}metadata_regular_note{/tr}</i><br />
    {$form.regex.html}
    <a href="javascript:popup('testRegExpr.htm','testRegExpr')">{tr}Need help?{/tr}</a></p>
  </span>

    <br /><br />
    <b>{$form.is_required.label}:</b>
    {$form.is_required.html}<br />
    <i>Input is required</i>

  <span id=select_param style="display: none">
    <br />
    <b>{$form.select_list.label}:</b><br />
    <i>{tr}metadata_select_list_help1{/tr}</i><br />
    {$form.select_list.html}
  </span>

  <span id=select_common style="display: none">
    <br />
    <b>{$form.is_multiple.label}:</b>
    {$form.is_multiple.html}<br />
    <i>{tr}metadata_multiple_help{/tr}</i>

    <!--{*
    <br /><br />
    <b>{$form.adv_select.label}:</b>
    {$form.adv_select.html}<br />
    <i>{tr}Use the advanced multi select feature{/tr}</i>
    *}-->

    <br /><br />
    <b>{$form.return_name.label}:</b>
    {$form.return_name.html}<br />
    <i>{tr}Return the normalized name or return id{/tr}</i>
  </span>

  <span id=date style="display: none">
    <br />
    <b>{$form.date_format.label}:</b>
    {$form.date_format.html}<br />
    <i>{tr}metadata_date_format_help1{/tr}</i>
  </span>

  <span id=select_predef style="display: none">
    <br />
    <b>{$form.selectdb_where.label}:</b><br />
    <i>{tr}metadata_selectdb_help1{/tr}</i><br />
    {$form.selectdb_where.html}
  </span>

  <span id=select_fromDB style="display: none">
    <br />
    <b>{$form.selectdb_table.label}:</b>
    {$form.selectdb_table.html}<br />
    <i>{tr}Name of the table from where to gets datas{/tr}</i>

    <br /><br />
    <b>{$form.selectdb_field_for_value.label}:</b>
    {$form.selectdb_field_for_value.html}<br />
    <i>{tr}Name of the field used for set the value return by the select{/tr}</i>

    <br /><br />
    <b>{$form.selectdb_field_for_display.label}:</b>
    {$form.selectdb_field_for_display.html}<br />
    <i>{tr}Name of the field used for set the text to display{/tr}</i>
  </span>

  <br />
 {if not $form.frozen}
    {$form.validate.html}&nbsp;{$form.cancel.html}
 {/if}
  <br />

{$form.requirednote}

</form>

{literal}
<script language="JavaScript">
  afficherAutre();
</script>
{/literal}
