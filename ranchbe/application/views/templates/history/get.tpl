{*Smarty template*}

<h1 class="pagetitle">{tr}{$PageTitle}{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{*assign var="histoOption" value="1"*}
{$views_helper_filter_form|toggleanim}


{literal}
<script type="text/javascript">
	initTbody();
</script>
{/literal}

{* -------------------Pagination------------------------ *}
{$views_helper_pagination}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$sameurl}">
<div class="scroll" id="scroll">
<table class="scroll" >
<thead class="fixedHeader">
 <tr  class="fixedHeader" >
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  <th class="heading">
  {column_header sort_order='INV' sort_field='histo_order'}
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field='action_name'}<br />
  {column_header sort_order='INV' sort_field='action_by'}<br />
  {column_header sort_order='INV' sort_field='action_date'}
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field=$id_map title='Id'}
  </th>

  <th class="heading">
  {column_header sort_order='INV' sort_field=$number_map title='Number'} - 
  {column_header sort_order='INV' sort_field=$indice_map title='Indice'}.
  {column_header sort_order='INV' sort_field=$version_map title='Version'}<br />
  {column_header sort_order='INV' sort_field=$description_map title='Description'}<br />
  {column_header sort_order='INV' sort_field=$access_map title='Access'} - 
  {column_header sort_order='INV' sort_field=$state_map title='State'}
  </th>

  <th class="heading">
  {tr}Last Update{/tr}
  {column_header sort_order='INV' sort_field='update_by' title='by'}
   - 
  {column_header sort_order='INV' sort_field='update_date' title='date'}
  </th><th class="heading">
  {column_header sort_order='INV' sort_field='forseen_close_date'}
  </th><th class="heading">
  {column_header sort_order='INV' sort_field='close_date'}
  </th><th class="heading">
  {column_header sort_order='INV' sort_field=$father_map_id title='Father'}
  </th><th class="heading">
    {tr}Additional property{/tr}</th>

 </tr>
</thead>
<tbody class="scrollContent" id="contentTbody">
{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="histo_order[]" value="{$list[list].histo_order}" /></td>

    <td class="thin"><a href="{$samemodule}/history/suppress/histo_order/{$list[list].histo_order}/space/{$SPACE_NAME}/basic_id/{$basic_id}" title="{tr}Suppress{/tr}: {$list[list].histo_order}"
                      onclick="return confirm('{tr}Do you want really suppress{/tr} history {$list[list].histo_order}')">
                     <img border="0" alt="{tr}Suppress{/tr}: {$list[list].histo_order}" src="img/icons/trash.png" />
                     </a>
    </td>

    <td class="thin" class="link" >{$list[list].histo_order}</td>

    <td class="thin" class="link" NOWRAP>
    <b>{$list[list].action_name}</b><br />
    <i>by</i> <b>{$list[list].action_by}</b><br />
    <i>at</i> {$list[list].action_date|date_format}
    {if $list[list].comment}
    <br /><i>comment :</i> <b><font color="green">{$list[list].comment}</font></b>
    {/if}
    </td>

    <td class="thin" class="link" >{$list[list].$id_map}</td>

    <td class="thin" class="link" NOWRAP>
    {$list[list].$number_map} - {$list[list].$indice_map}.{$list[list].$version_map}<br />
    <i>{$list[list].$description_map}</i><br />
    {$list[list].document_access_code|access_code} - {$list[list].$state_map}<br />
    <font size="-3">{$list[list].doctype_id|doctype}</font>
    </td>

    <td class="thin" class="link" >
    {$list[list].update_by|username} - {$list[list].update_date|date_format}
    </td>
    <td class="thin" class="link" >{$list[list].forseen_close_date|date_format}</td>
    <td class="thin" class="link" >{$list[list].close_date|date_format}</td>
    <td class="thin" class="link" >{$list[list].$father_map_id}</td>

    <td class="thin" class="link" NOWRAP>
      {if $list[list].default_file_path}
      <i>default file path :</i>{$list[list].default_file_path}<br />
      {/if}

      {if $list[list].activity_id}
      <i>activityId :</i>{$list[list].activity_id}<br />
      <i>instanceId :</i>{$list[list].instance_id}<br />
      {/if}
    </td>
   </tr>
  {/section}</tbody>
  </table>

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
    <script language='Javascript' type='text/javascript'>
    <!--
    // check / uncheck all.
    // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
    // for now those people just have to check every single box
    document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'histo_order[]',this.checked)\"/></td>");
    document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
    //-->                     
    </script>

    <br>

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
 onclick="if(confirm('{tr}Do you want really suppress this history{/tr}'))
 {ldelim}document.checkform.action='./{$samemodule}/history/suppress';
 pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="document.checkform.action='./{$sameurl}';pop_no(checkform);return false;">
  <img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<input type="hidden" name="object_id" value="{$object_id}" />
<input type="hidden" name="space" value="{$SPACE_NAME}" />

</form>
