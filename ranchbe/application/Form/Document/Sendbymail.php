<?php
/**
 */
class Form_Document_Sendbymail extends ZendRb_Form{
	
    /**
     * Form initialization
     *
     * @return void
     */
    public function init(){
    	parent::init();
    	$emailRegexValidation = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}';
    	
		$this->addElement('TextBox','to', array(
				'label' => tra ( 'To' ),
				'allowEmpty'=> true,
				'required' => false,
				'multiple' => false,
				'autocomplete' => true,
				'returnName'=>true,
				'source'=> '',
				'regExp' => $emailRegexValidation,
				'invalidMessage' => tra('is not a valid mail'),
				'filters' => array('StringTrim', 'StringToLower'),
				'validators' => array(
								    array('emailAddress', true),
								    ),
				)
		);
		$this->addElement('TextBox','cc', array(
				'label' => tra ( 'Cc' ),
				'allowEmpty'=> true,
				'required' => false,
				'multiple' => false,
				'autocomplete' => true,
				'returnName'=>true,
				'source'=> '',
				'regExp' => $emailRegexValidation,
				'invalidMessage' => tra('is not a valid mail'),
				'filters' => array('StringTrim', 'StringToLower'),
				'validators' => array(
								    array('emailAddress', true),
								    ),
				)
		);
		$this->addElement('TextBox','bcc', array(
				'label' => tra ( 'Bcc' ),
				'allowEmpty'=> true,
				'required' => false,
				'multiple' => false,
				'autocomplete' => true,
				'returnName'=>true,
				'source'=> '',
				'regExp' => $emailRegexValidation,
				'invalidMessage' => tra('is not a valid mail'),
				'filters' => array('StringTrim', 'StringToLower'),
				'validators' => array(
								    array('emailAddress', true),
								    ),
				)
		);
		
    	
    	//Subject (text input)
        $this->addElement(
                'TextBox',
                'subject',
                array(
                    'label'      => tra('Subject'),
                    'trim'       => true,
                )
            );
            
    	//join files
        $this->addElement(
            'Multiselect',
            'attachment',
            array(
                'label'    => tra('Join files'),
            	'multiple' => true,
            	'returnname' => false,
            )
        );
        
    	//Body (editor)
        $this->addElement(
            'Editor',
            'message_body',
            array(
                'label'        => tra('Body'),
                'inheritWidth' => false,
            )
        );
        
    } //End of method
	
} //End of class

